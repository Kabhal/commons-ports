/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.message.smtp;

import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.TEXT_HTML;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.clearInvocations;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.mail.MailSenderAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.javamail.JavaMailSender;

import com.tinubu.commons.ddd2.domain.event.DomainEvent;
import com.tinubu.commons.ddd2.domain.event.DomainEventListener;
import com.tinubu.commons.lang.datetime.ApplicationClock;
import com.tinubu.commons.ports.message.domain.Message;
import com.tinubu.commons.ports.message.domain.Message.MessageBuilder;
import com.tinubu.commons.ports.message.domain.MessageAddress;
import com.tinubu.commons.ports.message.domain.MessageContent.MessageContentBuilder;
import com.tinubu.commons.ports.message.domain.MessageIOException;
import com.tinubu.commons.ports.message.domain.MessageService;
import com.tinubu.commons.ports.message.domain.testsuite.BaseMessageServiceTest;

@SpringBootTest(classes = SmtpMessageServiceFailureTest.class)
@ImportAutoConfiguration({
      MailSenderAutoConfiguration.class /* JavaMail initialization */ })
class SmtpMessageServiceFailureTest extends BaseMessageServiceTest {

   private static final ZonedDateTime FIXED_DATE =
         ZonedDateTime.of(LocalDateTime.of(2021, 11, 10, 14, 0, 0), ZoneId.of("UTC"));

   @Autowired
   JavaMailSender javaMailSender;

   private SmtpMessageService smtp;

   @BeforeEach
   public void configureSmtpMessageService() {
      smtp = new SmtpMessageService(javaMailSender);
   }

   @BeforeEach
   public void setApplicationClock() {
      ApplicationClock.setFixedClock(FIXED_DATE);
   }

   @Override
   protected MessageService messageService() {
      return smtp;
   }

   @Test
   void sendWhenSmtpConnectionFailure() {
      DomainEventListener<DomainEvent> domainEventListener = instrumentMessageServiceListeners();

      clearInvocations(domainEventListener);

      Message message = new MessageBuilder()
            .fromAddress(MessageAddress.of("from@domain.tld"))
            .toAddress(MessageAddress.of("to@domain.tld"))
            .replyToAddress(MessageAddress.of("reply-to@domain.tld"))
            .ccAddresses(MessageAddress.of("cc1@domain.tld"), MessageAddress.of("cc2@domain.tld"))
            .subject("subject")
            .content(MessageContentBuilder.of("<strong>content</strong>", TEXT_HTML))
            .build();

      assertThatExceptionOfType(MessageIOException.class).isThrownBy(() -> smtp.sendMessage(message));

      verifyNoMoreInteractions(domainEventListener);
   }

}
