/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.message.resilience4j;

import static org.mockito.Mockito.spy;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import com.tinubu.commons.ports.message.domain.AbstractMessageService;
import com.tinubu.commons.ports.message.domain.Message;
import com.tinubu.commons.ports.message.domain.MessageContent;
import com.tinubu.commons.ports.message.domain.MessageService;
import com.tinubu.commons.ports.message.domain.event.MessageSent;
import com.tinubu.commons.ports.message.domain.testsuite.BaseMessageServiceTest;
import com.tinubu.commons.ports.message.resilience4j.Resilience4jMessageConfig.Resilience4jMessageConfigBuilder;
import com.tinubu.commons.ports.resilience4j.Resilience4jConfig;
import com.tinubu.commons.ports.resilience4j.Resilience4jConfig.Resilience4jConfigBuilder;
import com.tinubu.commons.ports.resilience4j.event.Resilience4jEvent;

public abstract class BaseResilience4jMessageServiceTest extends BaseMessageServiceTest {

   private MessageService realMessageService;
   private MessageService messageService;

   protected MessageService realMessageService() {
      return realMessageService;
   }

   @BeforeEach
   public void configureMessageService() {
      this.realMessageService = spy(new AbstractMessageService() {
         @Override
         public void sendMessage(Message message, MessageContent... alternatives) {
            System.out.println(String.format("Sent message '%s'", message));

            publishEvent(new MessageSent(this, message));
         }
      });

      this.messageService = newResilience4jMessageService(realMessageService());

      this.messageService.registerEventLoggingListener(Resilience4jEvent.class);
   }

   @AfterEach
   public void closeDocumentRepository() {
      try {
         this.messageService.close();
      } finally {
         this.realMessageService.close();
      }
   }

   protected MessageService messageService() {
      return messageService;
   }

   private Resilience4jMessageService newResilience4jMessageService(MessageService realMessageService) {
      return new Resilience4jMessageService(new Resilience4jMessageConfigBuilder()
                                                  .resilience(resilience4jConfig())
                                                  .build(), realMessageService, "test");
   }

   protected Resilience4jConfig resilience4jConfig() {
      return new Resilience4jConfigBuilder().build();
   }

}
