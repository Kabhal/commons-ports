/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.message.domain;

import static com.tinubu.commons.ddd2.domain.type.support.DomainObjectSupport.checkInvariants;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.lang.beans.Getter;

/**
 * Generic message address.
 */
public class MessageAddress extends AbstractValue {

   protected final String address;
   protected final String personal;

   protected MessageAddress(String address) {
      this.address = address;
      this.personal = null;
   }

   protected MessageAddress(String address, String personal) {
      this.address = address;
      this.personal = personal;
   }

   @Override
   @SuppressWarnings("unchecked")
   public Fields<? extends MessageAddress> defineDomainFields() {
      return Fields
            .<MessageAddress>builder()
            .superFields((Fields<MessageAddress>) super.defineDomainFields())
            .field("address", v -> v.address, isNotBlank())
            .field("personal", v -> v.personal, isNull().orValue(isNotBlank()))
            .build();
   }

   public static MessageAddress of(String address) {
      return checkInvariants(new MessageAddress(address));
   }

   public static MessageAddress of(String address, String personal) {
      return checkInvariants(new MessageAddress(address, personal));
   }

   /**
    * Message originator or destination address. The format depends on the underlying implementation.
    *
    * @return A not blank string representation of the message address
    */
   @Getter
   public String address() {
      return address;
   }

   /**
    * An optional name by which the originator or the recipient's message is known.
    *
    * @return The originator or recipient's personal name
    */
   @Getter
   public String personal() {
      return personal;
   }
}
