/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.message.domain.metrology;

import java.util.Objects;
import java.util.StringJoiner;
import java.util.concurrent.atomic.AtomicLong;

import com.tinubu.commons.ddd2.domain.event.DomainEvent;
import com.tinubu.commons.lang.metrology.Counter;
import com.tinubu.commons.ports.message.domain.MessageService;
import com.tinubu.commons.ports.message.domain.event.MessageSent;

/**
 * Default metrology bean for all {@link MessageService}.
 */
public class DefaultMessageServiceMetrology implements MessageServiceMetrology {

   protected final AtomicLong messagesSent = new AtomicLong();
   protected final AtomicLong messageNotSent = new AtomicLong();

   public DefaultMessageServiceMetrology() {
   }

   public void incrementMessagesSent(int count) {
      messagesSent.addAndGet(count);
   }

   @Counter(description = "Messages sent counter")
   public long messagesSent() {
      return messagesSent.get();
   }

   @Override
   public void resetMetrics() {
      messagesSent.set(0);
   }

   @Override
   public void accept(DomainEvent event) {
      if (event instanceof MessageSent) {
         incrementMessagesSent(1);
      }
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      DefaultMessageServiceMetrology that = (DefaultMessageServiceMetrology) o;
      return Objects.equals(messagesSent.get(), that.messagesSent.get())
             && Objects.equals(messageNotSent.get(), that.messageNotSent.get());
   }

   @Override
   public int hashCode() {
      return Objects.hash(messagesSent, messageNotSent);
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", DefaultMessageServiceMetrology.class.getSimpleName() + "[", "]")
            .add("messagesSent=" + messagesSent)
            .add("messageNotSent=" + messageNotSent)
            .toString();
   }
}
