/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.message.domain;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.NullableUtils.nullableInstanceOf;
import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import com.tinubu.commons.ddd2.domain.ids.Uuid;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.beans.Setter;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.typed.TypedDocument;

/**
 * A document to attach to {@link Message}.
 * This representation adds support for a unique, normalized, document identifier,
 * that can be used to reference a document. If not specified, a random, normalized, UUID is generated for
 * the
 * identifier.
 * <p>
 * Correct usage for message documents is not to directly build an instance using the builder but to
 * reference an already built document using {@link #of(String, Document)}, e.g.:
 * {@code MessageDocument.of(attachmentId, TextPlainDocument.open(...));}
 *
 * @apiNote By design, any document, even attachments, can have a
 *       {@link MessageDocument#messageDocumentId} to support more backend implementations. Default
 *       normalization for this identifier is a URL-encoder, as in SMTP specification.
 */
public class MessageDocument extends TypedDocument<MessageDocument> {
   private final String messageDocumentId;

   protected MessageDocument(MessageDocumentBuilder builder) {
      super(builder);
      this.messageDocumentId = nullable(builder.messageDocumentId,
                                        MessageDocument::normalizeMessageDocumentId,
                                        defaultMessageDocumentId());
   }

   @Override
   @SuppressWarnings("unchecked")
   public Fields<? extends MessageDocument> defineDomainFields() {
      return Fields
            .<MessageDocument>builder()
            .superFields((Fields<MessageDocument>) super.defineDomainFields())
            .field("messageDocumentId", v -> v.messageDocumentId, isNotBlank())
            .build();
   }

   /**
    * Creates a new message document from existing document. Source document metadata are not changed.
    * If source document is a {@link MessageDocument}, its messageDocumentId will be overridden with specified
    * identifier.
    *
    * @param messageDocumentId message document identifier
    * @param document existing document to base on
    *
    * @return new message document
    */
   public static MessageDocument of(String messageDocumentId, Document document) {
      notBlank(messageDocumentId, "messageDocumentId");
      notNull(document, "document");

      return new MessageDocumentBuilder().reconstitute().copy(document)
            .messageDocumentId(messageDocumentId)
            .build();
   }

   /**
    * Creates a new message document from existing document. Source document metadata are not changed.
    * If source document is a {@link MessageDocument}, its messageDocumentId will be overridden with default
    * random identifier.
    *
    * @param document existing document to base on
    *
    * @return new message document
    */
   public static MessageDocument of(Document document) {
      notNull(document, "document");

      return new MessageDocumentBuilder().reconstitute().copy(document).messageDocumentId(null).build();
   }

   /**
    * Identifier for this document in the context of the message. By default, message document id is set to
    * document file name. Message document id is normalized (URL encoded).
    *
    * @return document identifier in message
    */
   @Getter
   public String messageDocumentId() {
      return messageDocumentId;
   }

   /**
    * Generates a default message document id from specified document. Generated document id is a random UUID
    * (UUID v4).
    *
    * @return message document id
    *
    * @implSpec Generated message document id must be normalized.
    */
   protected static String defaultMessageDocumentId() {
      return normalizeMessageDocumentId(Uuid.newRandomUuid().stringValue());
   }

   /**
    * Normalizes message document identifier.
    * Default normalization is to URL encode message document id using UTF-8 encoding, then lowercase the
    * output.
    *
    * @param messageDocumentId message document id to encode, must not be blank.
    *
    * @return normalized message document id
    */
   protected static String normalizeMessageDocumentId(String messageDocumentId) {
      validate(messageDocumentId, "messageDocumentId", isNotBlank()).orThrow();

      try {
         return URLEncoder.encode(messageDocumentId, UTF_8.name()).toLowerCase();
      } catch (UnsupportedEncodingException e) {
         throw new IllegalStateException(e);
      }
   }

   @Override
   protected String lineSeparator() {
      throw new UnsupportedOperationException("MessageDocument should not be used to write content");
   }

   @Override
   protected MessageDocumentBuilder documentBuilder() {
      return new MessageDocumentBuilder();
   }

   public static MessageDocumentBuilder reconstituteBuilder() {
      return new MessageDocumentBuilder().reconstitute();
   }

   public static class MessageDocumentBuilder
         extends TypedDocumentBuilder<MessageDocument, MessageDocumentBuilder> {
      private String messageDocumentId;

      /**
       * Copy constructor for {@link MessageDocument}
       *
       * @param document document to copy
       *
       * @return message document builder
       */
      public static MessageDocumentBuilder from(Document document) {
         notNull(document, "document");

         return new MessageDocumentBuilder().reconstitute().copy(document);
      }

      @Override
      protected MessageDocumentBuilder copy(Document document) {
         return super
               .copy(document)
               .optionalChain(nullableInstanceOf(document, MessageDocument.class),
                              (b, d) -> b.messageDocumentId(d.messageDocumentId));
      }

      @Setter
      public MessageDocumentBuilder messageDocumentId(String messageDocumentId) {
         this.messageDocumentId = messageDocumentId;
         return this;
      }

      @Override
      public MessageDocument buildDomainObject() {
         return new MessageDocument(this);
      }

   }

}
