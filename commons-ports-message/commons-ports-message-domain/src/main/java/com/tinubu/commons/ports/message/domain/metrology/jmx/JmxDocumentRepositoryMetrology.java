
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.message.domain.metrology.jmx;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.lang.util.ExceptionUtils.sneakyThrow;

import java.util.StringJoiner;

import javax.management.DynamicMBean;
import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanRegistrationException;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;

import com.tinubu.commons.ddd2.domain.event.DomainEvent;
import com.tinubu.commons.lang.metrology.jmx.JmxServer;
import com.tinubu.commons.lang.metrology.jmx.MetrologyMBean;
import com.tinubu.commons.ports.message.domain.MessageService;
import com.tinubu.commons.ports.message.domain.metrology.MessageServiceMetrology;

/**
 * JMX metrology bean for all {@link MessageService}.
 * <p>
 * This metrology implementation registers wrapped MBean in JMX, hence, it will propagate events to delegated
 * metrology but does not provide counter accessors by itself.
 * <p>
 * You can {@link MessageService#registerMetrology(MessageServiceMetrology) register} either :
 * <ul>
 *    <li>this class created with {@link #of(MessageServiceMetrology, String)}</li>
 *    <li>the delegated metrology returned from {@link #register(MessageServiceMetrology, String)}</li>
 * </ul>
 */
public class JmxDocumentRepositoryMetrology implements MessageServiceMetrology {

   private static final String JMX_DOMAIN = "com.tinubu.commons-ports.message";

   private final MessageServiceMetrology delegate;

   protected JmxDocumentRepositoryMetrology(MessageServiceMetrology delegate, String name)
         throws InstanceAlreadyExistsException, MBeanRegistrationException, MalformedObjectNameException {
      validate(this.delegate = delegate, "delegate", isNotNull())
            .and(validate(name, "name", isNotBlank()))
            .orThrow();

      registerMBean(delegate, name);
   }

   protected JmxDocumentRepositoryMetrology(MessageServiceMetrology delegate)
         throws InstanceAlreadyExistsException, MBeanRegistrationException {
      validate(this.delegate = delegate, "delegate", isNotNull()).orThrow();

      registerMBean(delegate);
   }

   public static JmxDocumentRepositoryMetrology of(MessageServiceMetrology delegate, String name)
         throws InstanceAlreadyExistsException, MBeanRegistrationException, MalformedObjectNameException {
      return new JmxDocumentRepositoryMetrology(delegate, name);
   }

   public static JmxDocumentRepositoryMetrology of(MessageServiceMetrology delegate)
         throws InstanceAlreadyExistsException, MBeanRegistrationException {
      return new JmxDocumentRepositoryMetrology(delegate);
   }

   public static <T extends MessageServiceMetrology> T register(T delegate, String name)
         throws InstanceAlreadyExistsException, MBeanRegistrationException, MalformedObjectNameException {
      registerMBean(delegate, name);

      return delegate;
   }

   public static <T extends MessageServiceMetrology> T register(T delegate)
         throws InstanceAlreadyExistsException, MBeanRegistrationException {
      registerMBean(delegate);

      return delegate;
   }

   public static <T extends MessageServiceMetrology> void unregister(Class<T> delegateType, String name)
         throws MBeanRegistrationException, MalformedObjectNameException, InstanceNotFoundException {
      unregisterMBean(delegateType, name);
   }

   public static <T extends MessageServiceMetrology> void unregister(Class<T> delegateType)
         throws MBeanRegistrationException, InstanceNotFoundException {
      unregisterMBean(delegateType);
   }

   protected static <T extends MessageServiceMetrology> void registerMBean(T delegate)
         throws InstanceAlreadyExistsException, MBeanRegistrationException {
      try {
         JmxServer.instance(JMX_DOMAIN)
               .registerMBean(dynamicMBean(delegate, delegate.getClass().getSimpleName()),
                              delegate.getClass().getSimpleName(),
                              delegate.getClass().getSimpleName());
      } catch (NotCompliantMBeanException | MalformedObjectNameException e) {
         throw sneakyThrow(e);
      }
   }

   protected static <T extends MessageServiceMetrology> void registerMBean(T delegate, String name)
         throws InstanceAlreadyExistsException, MBeanRegistrationException, MalformedObjectNameException {
      try {
         JmxServer
               .instance(JMX_DOMAIN)
               .registerMBean(dynamicMBean(delegate, name), delegate.getClass().getSimpleName(), name);
      } catch (NotCompliantMBeanException e) {
         throw sneakyThrow(e);
      }
   }

   protected static <T extends MessageServiceMetrology> void unregisterMBean(Class<? extends MessageServiceMetrology> delegateType,
                                                                             String name)
         throws MalformedObjectNameException, InstanceNotFoundException, MBeanRegistrationException {
      JmxServer.instance(JMX_DOMAIN).unregisterMBean(delegateType, name);
   }

   protected static <T extends MessageServiceMetrology> void unregisterMBean(Class<? extends MessageServiceMetrology> delegateType)
         throws InstanceNotFoundException, MBeanRegistrationException {
      JmxServer.instance(JMX_DOMAIN).unregisterMBean(delegateType);
   }

   private static <T extends MessageServiceMetrology> DynamicMBean dynamicMBean(T delegate, String name) {
      return new MetrologyMBean<MessageServiceMetrology>(delegate, name);
   }

   @Override
   public void accept(DomainEvent event) {
      delegate.accept(event);
   }

   @Override
   public void resetMetrics() {
      delegate.resetMetrics();
   }

   public String toString() {
      return new StringJoiner(", ", JmxDocumentRepositoryMetrology.class.getSimpleName() + "[", "]")
            .add("delegate=" + delegate)
            .toString();
   }

}
