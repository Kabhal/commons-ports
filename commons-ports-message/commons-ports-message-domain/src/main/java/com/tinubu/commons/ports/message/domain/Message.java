/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.message.domain;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.hasNoNullElements;
import static com.tinubu.commons.lang.util.CollectionUtils.immutable;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.ports.message.domain.rules.MessageRules.hasAtLeastOneAddress;
import static com.tinubu.commons.ports.message.domain.rules.MessageRules.hasUniqueMessageDocumentIds;
import static java.util.stream.Collectors.joining;

import java.util.List;
import java.util.Optional;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.Invariant;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.beans.Setter;

/**
 * Generic message representation.
 */
public class Message extends AbstractValue {

   protected final MessageAddress fromAddress;
   protected final MessageAddress replyToAddress;
   protected final List<MessageAddress> toAddresses;
   protected final List<MessageAddress> ccAddresses;
   protected final List<MessageAddress> bccAddresses;
   protected final String subject;
   protected final MessageContent content;
   protected final List<MessageDocument> attachments;

   protected Message(MessageBuilder builder) {
      this.fromAddress = builder.fromAddress;
      this.replyToAddress = builder.replyToAddress;
      this.toAddresses = immutable(list(builder.toAddresses));
      this.ccAddresses = immutable(list(builder.ccAddresses));
      this.bccAddresses = immutable(list(builder.bccAddresses));
      this.subject = builder.subject;
      this.content = builder.content;
      this.attachments = immutable(list(builder.attachments));
   }

   @Override
   @SuppressWarnings("unchecked")
   public Fields<? extends Message> defineDomainFields() {
      return Fields
            .<Message>builder()
            .superFields((Fields<Message>) super.defineDomainFields())
            .field("fromAddress", v -> v.fromAddress, isNotNull())
            .field("replyToAddress", v -> v.replyToAddress)
            .field("toAddresses", v -> v.toAddresses, hasNoNullElements())
            .field("ccAddresses", v -> v.ccAddresses, hasNoNullElements())
            .field("bccAddresses", v -> v.bccAddresses, hasNoNullElements())
            .field("subject", v -> v.subject)
            .field("content", v -> v.content)
            .field("attachments", v -> v.attachments, this::displayDocuments, hasNoNullElements())
            .invariants(Invariant.of(() -> this, hasAtLeastOneAddress()))
            .invariants(Invariant.of(() -> this, hasUniqueMessageDocumentIds()))
            .build();
   }

   /**
    * Sender of the message.
    *
    * @return message sender
    */
   @Getter
   public MessageAddress fromAddress() {
      return fromAddress;
   }

   /**
    * An optional reply address.
    *
    * @return optional message reply's address
    */
   @Getter
   public Optional<MessageAddress> replyToAddress() {
      return nullable(replyToAddress);
   }

   /**
    * Receivers of the message.
    *
    * @return message receivers, never empty
    */
   @Getter
   public List<MessageAddress> toAddresses() {
      return toAddresses;
   }

   /**
    * Optional Carbon Copy (CC) receivers of the message.
    *
    * @return message CC receivers or empty list
    */
   @Getter
   public List<MessageAddress> ccAddresses() {
      return ccAddresses;
   }

   /**
    * Optional Blind Carbon Copy (BCC) receivers of the message.
    *
    * @return message BCC receivers or empty list
    */
   @Getter
   public List<MessageAddress> bccAddresses() {
      return bccAddresses;
   }

   /**
    * Optional message subject.
    * Message subject can also be blank.
    *
    * @return message subject
    */
   @Getter
   public Optional<String> subject() {
      return nullable(subject);
   }

   /**
    * Optional message content.
    *
    * @return message content
    */
   @Getter
   public Optional<MessageContent> content() {
      return nullable(content);
   }

   /**
    * Optional document attachments.
    *
    * @return attachments or empty list
    */
   @Getter
   public List<MessageDocument> attachments() {
      return attachments;
   }

   protected Object displayDocuments(List<MessageDocument> documents) {
      return documents
            .stream().map(d -> d.messageDocumentId() + "->" + d.documentId())
            .collect(joining(",", "[", "]"));
   }

   public static MessageBuilder reconstituteBuilder() {
      return new MessageBuilder().reconstitute();
   }

   public static class MessageBuilder extends DomainBuilder<Message> {
      private MessageAddress fromAddress;
      private MessageAddress replyToAddress;
      private List<MessageAddress> toAddresses = list();
      private List<MessageAddress> ccAddresses = list();
      private List<MessageAddress> bccAddresses = list();
      private String subject;
      private MessageContent content;
      private List<MessageDocument> attachments = list();

      public static MessageBuilder from(Message message) {
         validate(message, "message", isNotNull()).orThrow();

         return new MessageBuilder()
               .<MessageBuilder>reconstitute()
               .fromAddress(message.fromAddress)
               .replyToAddress(message.replyToAddress)
               .toAddresses(message.toAddresses)
               .ccAddresses(message.ccAddresses)
               .bccAddresses(message.bccAddresses)
               .content(message.content)
               .subject(message.subject)
               .attachments(message.attachments);
      }

      @Setter
      public MessageBuilder fromAddress(MessageAddress from) {
         this.fromAddress = from;

         return this;
      }

      @Setter
      public MessageBuilder replyToAddress(MessageAddress replyTo) {
         this.replyToAddress = replyTo;

         return this;
      }

      @Setter
      public MessageBuilder toAddresses(List<MessageAddress> to) {
         this.toAddresses = list(to);

         return this;
      }

      public MessageBuilder toAddresses(MessageAddress... to) {
         return toAddresses(list(to));
      }

      public MessageBuilder toAddress(MessageAddress to) {
         this.toAddresses = list(to);

         return this;
      }

      public MessageBuilder addToAddresses(MessageAddress... to) {
         this.toAddresses.addAll(list(to));

         return this;
      }

      @Setter
      public MessageBuilder ccAddresses(List<MessageAddress> cc) {
         this.ccAddresses = list(cc);

         return this;
      }

      public MessageBuilder ccAddresses(MessageAddress... cc) {
         return ccAddresses(list(cc));
      }

      public MessageBuilder addCcAddresses(MessageAddress... cc) {
         this.ccAddresses.addAll(list(cc));

         return this;
      }

      @Setter
      public MessageBuilder bccAddresses(List<MessageAddress> bcc) {
         this.bccAddresses = list(bcc);

         return this;
      }

      public MessageBuilder bccAddresses(MessageAddress... bcc) {
         return bccAddresses(list(bcc));
      }

      public MessageBuilder addBccAddresses(MessageAddress... bcc) {
         this.bccAddresses.addAll(list(bcc));

         return this;
      }

      @Setter
      public MessageBuilder content(MessageContent content) {
         this.content = content;

         return this;
      }

      @Setter
      public MessageBuilder subject(String subject) {
         this.subject = subject;

         return this;
      }

      @Setter
      public MessageBuilder attachments(List<MessageDocument> attachments) {
         this.attachments = list(attachments);

         return this;
      }

      public MessageBuilder attachments(MessageDocument... attachments) {
         return attachments(list(attachments));
      }

      public MessageBuilder addAttachments(MessageDocument... attachments) {
         this.attachments.addAll(list(attachments));

         return this;
      }

      @Override
      public Message buildDomainObject() {
         return new Message(this);
      }
   }

}
