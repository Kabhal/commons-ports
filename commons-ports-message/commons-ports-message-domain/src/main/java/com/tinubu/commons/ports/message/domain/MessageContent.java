/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.message.domain;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.hasNoNullElements;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_XHTML;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.TEXT_HTML;
import static com.tinubu.commons.lang.util.CollectionUtils.immutable;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static java.util.stream.Collectors.joining;

import java.util.List;
import java.util.stream.Stream;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.valueformatter.HiddenValueFormatter;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.beans.Setter;
import com.tinubu.commons.lang.mimetype.MimeType;

/**
 * Message content.
 */
public class MessageContent extends AbstractValue {

   /** Recognized MIME types for HTML content. */
   protected static final MimeType[] HTML_MIME_TYPES = { TEXT_HTML, APPLICATION_XHTML };

   protected final String content;
   protected final MimeType contentType;
   protected final List<MessageDocument> inlines;

   protected MessageContent(MessageContentBuilder builder) {
      this.content = builder.content;
      this.contentType = builder.contentType;
      this.inlines = immutable(builder.inlines);
   }

   @Override
   @SuppressWarnings("unchecked")
   public Fields<? extends MessageContent> defineDomainFields() {
      return Fields
            .<MessageContent>builder()
            .superFields((Fields<MessageContent>) super.defineDomainFields())
            .field("content", v -> v.content, new HiddenValueFormatter(), isNotNull())
            .field("contentType", v -> v.contentType, isNotNull())
            .field("inlines", v -> v.inlines, this::displayDocuments, hasNoNullElements())
            .build();
   }

   /**
    * Returns {@code true} if content is HTML or XHTML.
    *
    * @return whether content is HTML or XHTML
    */
   public boolean isHtml() {
      return Stream.of(HTML_MIME_TYPES).anyMatch(contentType::equalsTypeAndSubtype);
   }

   /**
    * Message content.
    *
    * @return message content
    */
   @Getter
   public String content() {
      return content;
   }

   /**
    * Message content type.
    *
    * @return message content type
    */
   @Getter
   public MimeType contentType() {
      return contentType;
   }

   /**
    * Optional content related inlines. These documents are referenced by
    * {@link MessageDocument#messageDocumentId()} in the content.
    *
    * @return attachments or empty list
    */
   @Getter
   public List<MessageDocument> inlines() {
      return inlines;
   }

   protected Object displayDocuments(List<MessageDocument> documents) {
      return documents
            .stream().map(d -> d.messageDocumentId() + "->" + d.documentId())
            .collect(joining(",", "[", "]"));
   }

   public static MessageContentBuilder reconstituteBuilder() {
      return new MessageContentBuilder().reconstitute();
   }

   public static class MessageContentBuilder extends DomainBuilder<MessageContent> {
      private String content;
      private MimeType contentType;
      private List<MessageDocument> inlines = list();

      public static MessageContent of(String content, MimeType contentType, List<MessageDocument> inlines) {
         return new MessageContentBuilder()
               .content(content)
               .contentType(contentType)
               .inlines(inlines)
               .build();
      }

      public static MessageContent of(String content, MimeType contentType, MessageDocument... inlines) {
         return of(content, contentType, list(inlines));
      }

      public static MessageContent of(String content, MimeType contentType) {
         return of(content, contentType, list());
      }

      public static MessageContentBuilder from(MessageContent messageContent) {
         validate(messageContent, "messageContent", isNotNull()).orThrow();

         return new MessageContentBuilder()
               .<MessageContentBuilder>reconstitute()
               .content(messageContent.content)
               .contentType(messageContent.contentType)
               .inlines(messageContent.inlines);
      }

      @Setter
      public MessageContentBuilder content(String content) {
         this.content = content;
         return this;
      }

      @Setter
      public MessageContentBuilder contentType(MimeType contentType) {
         this.contentType = contentType;
         return this;
      }

      @Setter
      public MessageContentBuilder inlines(List<MessageDocument> inlines) {
         this.inlines = list(inlines);

         return this;
      }

      public MessageContentBuilder inlines(MessageDocument... inlines) {
         return inlines(list(inlines));
      }

      public MessageContentBuilder addInlines(MessageDocument... inlines) {
         this.inlines.addAll(list(inlines));

         return this;
      }

      @Override
      public MessageContent buildDomainObject() {
         return new MessageContent(this);
      }
   }

}
