/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.message.domain;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import java.util.regex.Pattern;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.message.domain.MessageDocument.MessageDocumentBuilder;

public class MessageDocumentTest {

   public static final Pattern UUID_PATTERN =
         Pattern.compile("[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}");

   @Test
   public void testMessageDocumentWhenDefaultIdentifier() {
      MessageDocument document = new MessageDocumentBuilder()
            .documentId(DocumentPath.of("test.txt"))
            .loadedContent("content", UTF_8)
            .build();

      assertThat(document.messageDocumentId()).matches(UUID_PATTERN);
   }

   @Test
   public void testMessageDocumentWhenExternalIdentifier() {
      MessageDocument document = new MessageDocumentBuilder()
            .messageDocumentId("External Identifier")
            .documentId(DocumentPath.of("test.txt"))
            .loadedContent("content", UTF_8)
            .build();

      assertThat(document.messageDocumentId()).isEqualTo("external+identifier");
   }

   @Test
   public void testMessageDocumentWhenBlankIdentifier() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> new MessageDocumentBuilder()
                  .messageDocumentId(" \t")
                  .documentId(DocumentPath.of("test.txt"))
                  .loadedContent("content", UTF_8)
                  .build()).withMessage("Invariant validation error > 'messageDocumentId' must not be blank");
   }

   @Test
   public void testOfDocumentWhenNominal() {
      MessageDocument document = MessageDocument.of(new DocumentBuilder()
                                                          .documentId(DocumentPath.of("test.txt"))
                                                          .loadedContent("content", UTF_8)
                                                          .build());

      assertThat(document.messageDocumentId()).matches(UUID_PATTERN);
      assertThat(document.documentId()).isEqualTo(DocumentPath.of("test.txt"));
      assertThat(document.content().stringContent()).isEqualTo("content");
   }

   @Test
   public void testOfDocumentWhenNullMessageDocumentId() {
      assertThatNullPointerException()
            .isThrownBy(() -> MessageDocument.of(null,
                                                 new DocumentBuilder()
                                                       .documentId(DocumentPath.of("test.txt"))
                                                       .loadedContent("content", UTF_8)
                                                       .build()))
            .withMessage("'messageDocumentId' must not be null");
   }

   @Test
   public void testOfDocumentWhenBlankMessageDocumentId() {
      assertThatIllegalArgumentException()
            .isThrownBy(() -> MessageDocument.of(" \t",
                                                 new DocumentBuilder()
                                                       .documentId(DocumentPath.of("test.txt"))
                                                       .loadedContent("content", UTF_8)
                                                       .build()))
            .withMessage("'messageDocumentId' must not be blank");
   }

   @Test
   public void testOfDocumentWhenNullDocument() {
      assertThatNullPointerException()
            .isThrownBy(() -> MessageDocument.of(null))
            .withMessage("'document' must not be null");
   }

   @Test
   public void testOfDocumentWhenExternalMessageDocumentId() {
      MessageDocument document = MessageDocument.of("External Identifier",
                                                    new DocumentBuilder()
                                                          .documentId(DocumentPath.of("test.txt"))
                                                          .loadedContent("content", UTF_8)
                                                          .build());

      assertThat(document.messageDocumentId()).isEqualTo("external+identifier");
      assertThat(document.documentId()).isEqualTo(DocumentPath.of("test.txt"));
      assertThat(document.content().stringContent()).isEqualTo("content");
   }

}