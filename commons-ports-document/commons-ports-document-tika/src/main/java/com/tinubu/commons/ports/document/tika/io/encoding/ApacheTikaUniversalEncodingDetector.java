/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.tika.io.encoding;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Optional;

import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.txt.UniversalEncodingDetector;

import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.lang.io.encoding.EncodingDetector;

public class ApacheTikaUniversalEncodingDetector implements EncodingDetector {

   protected static final float TIKA_CONFIDENCE = 0.8f;

   @Override
   public Optional<DetectedEncoding> hasEncoding(ByteBuffer buffer, List<Charset> encodings) {
      Check.notNull(buffer, "buffer");
      Check.noNullElements(encodings, "encodings");

      try (ByteArrayInputStream inputStream = inputStream(buffer)) {
         UniversalEncodingDetector detector = new UniversalEncodingDetector();
         detector.setMarkLimit(buffer.limit());

         Metadata metadata = new Metadata();
         Charset charset;

         try {
            charset = detector.detect(inputStream, metadata);
         } catch (Exception e) {
            return optional();
         }

         return nullable(charset)
               .filter(o -> encodings.isEmpty() || encodings.contains(o))
               .map(encoding -> new DetectedEncoding(encoding, TIKA_CONFIDENCE));
      } catch (IOException e) {
         throw new UncheckedIOException(e);
      }
   }

   @Override
   public Optional<DetectedEncoding> hasBom(ByteBuffer buffer, List<Charset> encodings) {
      return Optional.empty();
   }

   protected static ByteArrayInputStream inputStream(ByteBuffer buffer) {
      buffer = buffer.duplicate();

      byte[] bytes = new byte[buffer.remaining()];
      buffer.get(bytes);

      return new ByteArrayInputStream(bytes);
   }
}
