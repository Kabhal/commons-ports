/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.typed;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.parseMimeType;
import static java.nio.charset.StandardCharsets.US_ASCII;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatIllegalStateException;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.lang.datetime.ApplicationClock;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentContent;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent.InputStreamDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.LoadedDocumentContent;
import com.tinubu.commons.ports.document.domain.OutputStreamDocumentContent;
import com.tinubu.commons.ports.document.domain.OutputStreamDocumentContent.OutputStreamDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.processor.DocumentContentProcessor;
import com.tinubu.commons.ports.document.domain.typed.TestDocument.TestDocumentBuilder;
import com.tinubu.commons.ports.document.domain.typed.TypedDocumentTest.ContentShifterProcessor.ContentEncodingProcessor;

public class TypedDocumentTest {

   private static final ZonedDateTime FIXED_DATE =
         ZonedDateTime.of(LocalDateTime.of(2021, 11, 10, 14, 0, 0), ZoneId.of("UTC"));

   @BeforeEach
   public void initializeApplicationClock() {
      ApplicationClock.setFixedClock(FIXED_DATE);
   }

   @Test
   public void testDuplicateWhenNominal() {
      TestDocument document = new TestDocumentBuilder()
            .documentId(DocumentPath.of("test.txt"))
            .loadedContent("test content", UTF_8)
            .nature("test")
            .build();

      TestDocument copy = document.duplicate(DocumentPath.of("copy.txt"));

      assertThat(copy.documentId()).isEqualTo(DocumentPath.of("copy.txt"));
      assertThat(copy.nature()).isEqualTo("test");
   }

   @Test
   public void testOpenWhenNominal() {
      GenericDocument document = GenericDocument.open(DocumentPath.of("path", "document.txt"), UTF_8);

      assertThat(document.documentId()).isEqualTo(DocumentPath.of("path/document.txt"));
      assertThat(document.metadata().documentPath()).isEqualTo(Path.of("path/document.txt"));
      assertThat(document.metadata().documentName()).isEqualTo("document.txt");
      assertThat(document.metadata().contentType()).hasValue(parseMimeType("text/plain;charset=UTF-8"));
      assertThat(document.metadata().contentSize()).isEmpty();
      assertThat(document.metadata().creationDate()).hasValue(FIXED_DATE.toInstant());
      assertThat(document.metadata().lastUpdateDate()).hasValue(FIXED_DATE.toInstant());
      assertThat(document.content().contentEncoding()).hasValue(UTF_8);
      assertThat(document.content().contentSize()).isEmpty();

      document.writeLn("Line 1").writeLn("Line 2").finish();

      Document finishedDocument = document.readableContent();

      assertThat(finishedDocument.content().stringContent()).isEqualTo("Line 1\nLine 2\n");
   }

   @Test
   public void testOpenWhenNoEncoding() {
      Document document = GenericDocument.open(DocumentPath.of("path", "document.txt"));

      assertThat(document.documentId()).isEqualTo(DocumentPath.of("path/document.txt"));
      assertThat(document.metadata().documentPath()).isEqualTo(Path.of("path/document.txt"));
      assertThat(document.metadata().documentName()).isEqualTo("document.txt");
      assertThat(document.metadata().contentType()).hasValue(parseMimeType("text/plain"));
      assertThat(document.metadata().contentSize()).isEmpty();
      assertThat(document.metadata().creationDate()).hasValue(FIXED_DATE.toInstant());
      assertThat(document.metadata().lastUpdateDate()).hasValue(FIXED_DATE.toInstant());
      assertThat(document.content().contentEncoding()).isEmpty();
      assertThat(document.content().contentSize()).isEmpty();
   }

   @Test
   public void testOpenWhenSpecifiedOutputStream() throws IOException {
      try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {

         GenericDocument document = GenericDocument.open(DocumentPath.of("path", "document.txt"), out, UTF_8);

         assertThat(document.documentId()).isEqualTo(DocumentPath.of("path/document.txt"));
         assertThat(document.metadata().documentPath()).isEqualTo(Path.of("path/document.txt"));
         assertThat(document.metadata().documentName()).isEqualTo("document.txt");
         assertThat(document.metadata().contentType()).hasValue(parseMimeType("text/plain;charset=UTF-8"));
         assertThat(document.metadata().contentSize()).isEmpty();
         assertThat(document.metadata().creationDate()).hasValue(FIXED_DATE.toInstant());
         assertThat(document.metadata().lastUpdateDate()).hasValue(FIXED_DATE.toInstant());
         assertThat(document.content().contentEncoding()).hasValue(UTF_8);
         assertThat(document.content().contentSize()).isEmpty();

         document.writeLn("Line 1").writeLn("Line 2").finish();

         assertThat(out.toByteArray()).asString().isEqualTo("Line 1\nLine 2\n");
      }
   }

   @Test
   public void testWriteWhenNoEncoding() {
      GenericDocument document = GenericDocument.open(DocumentPath.of("path", "document.txt"));

      document.write("Line 1".getBytes(UTF_8)).finish();

      assertThatIllegalStateException()
            .isThrownBy(() -> document.write("Line 2"))
            .withMessage("No content encoding set, you should specify one to encode this content");

   }

   @Test
   public void testWriteWhenContentProcessorWithDifferentEncoding() {
      GenericDocument document = GenericDocument
            .open(DocumentPath.of("path", "document.txt"), UTF_8)
            .process(new ContentEncodingProcessor(US_ASCII));

      assertThat(document.metadata().contentType()).hasValue(parseMimeType("text/plain;charset=US-ASCII"));
      assertThat(((OutputStreamDocumentContent) document.content()).sourceEncoding()).hasValue(UTF_8);
      assertThat(document.content().contentEncoding()).hasValue(US_ASCII);

      document.write("é").finish();

      Document finishedDocument = document.readableContent();

      assertThat(finishedDocument.content().content()).isEqualTo(new byte[] { -61, -87 });
   }

   @Test
   public void testWriteWhenByteArray() {
      GenericDocument document = GenericDocument.open(DocumentPath.of("path", "document.txt"), UTF_8);

      document.write("Line 1".getBytes(UTF_8)).finish();

      assertThat(document.readableContent().content().stringContent()).isEqualTo("Line 1");
   }

   @Test
   public void testWriteWhenNullByteArray() {
      GenericDocument document = GenericDocument.open(DocumentPath.of("path", "document.txt"), UTF_8);

      assertThatNullPointerException().isThrownBy(() -> document.write((byte[]) null));
   }

   @Test
   public void testWriteWhenSubByteArray() {
      GenericDocument document = GenericDocument.open(DocumentPath.of("path", "document.txt"), UTF_8);

      document.write("Line 1".getBytes(UTF_8), 1, 3).finish();

      assertThat(document.readableContent().content().stringContent()).isEqualTo("ine");
   }

   @Test
   public void testWriteWhenInputStream() {
      GenericDocument document = GenericDocument.open(DocumentPath.of("path", "document.txt"), UTF_8);

      document.write(new ByteArrayInputStream("Line 1".getBytes(UTF_8))).finish();

      assertThat(document.readableContent().content().stringContent()).isEqualTo("Line 1");
   }

   @Test
   public void testWriteWhenInputStreamWithMismatchingEncoding() {
      GenericDocument document = GenericDocument.open(DocumentPath.of("path", "document.txt"), US_ASCII);

      document.write(new ByteArrayInputStream("é".getBytes(UTF_8))).finish();

      // -61 -> 0xC3, -87 -> 0xA9
      assertThat(document.readableContent().content().content()).isEqualTo(new byte[] { -61, -87 });
   }

   @Test
   public void testWriteWhenString() {
      GenericDocument document = GenericDocument.open(DocumentPath.of("path", "document.txt"), UTF_8);

      document.write("Line 1").finish();

      assertThat(document.readableContent().content().stringContent()).isEqualTo("Line 1");
   }

   @Test
   public void testWriteWhenNullString() {
      GenericDocument document = GenericDocument.open(DocumentPath.of("path", "document.txt"), UTF_8);

      assertThatNullPointerException().isThrownBy(() -> document.write((String) null));
   }

   @Test
   public void testWriteWhenReader() {
      GenericDocument document = GenericDocument.open(DocumentPath.of("path", "document.txt"), UTF_8);

      document.write(new StringReader("Line 1")).finish();

      assertThat(document.readableContent().content().stringContent()).isEqualTo("Line 1");
   }

   @Test
   public void testWriteWhenReaderWithMismatchingEncoding() {
      GenericDocument document = GenericDocument.open(DocumentPath.of("path", "document.txt"), US_ASCII);

      document.write(new StringReader("é")).finish();

      assertThat(document.readableContent().content().content()).isEqualTo(new byte[] { '?' });
   }

   @Test
   public void testWriteLnWhenString() {
      GenericDocument document = GenericDocument.open(DocumentPath.of("path", "document.txt"), UTF_8);

      document.writeLn("Line 1").finish();

      assertThat(document.readableContent().content().stringContent()).isEqualTo("Line 1\n");
   }

   @Test
   public void testWriteLnWhenNullString() {
      GenericDocument document = GenericDocument.open(DocumentPath.of("path", "document.txt"), UTF_8);

      assertThatNullPointerException().isThrownBy(() -> document.writeLn(null));
   }

   @Test
   public void testWriteLnWhenCustomLineSeparator() {
      GenericDocument document = GenericDocument.open(DocumentPath.of("path", "document.txt"), UTF_8);

      GenericDocument specialized =
            new GenericDocument(document.documentBuilder().reconstitute().copy(document)) {
               @Override
               public String lineSeparator() {
                  return "|";
               }
            };

      specialized.writeLn("Line 1").finish();

      assertThat(specialized.readableContent().content().stringContent()).isEqualTo("Line 1|");
   }

   @Test
   public void testWriteWhenDocument() {
      GenericDocument document = GenericDocument.open(DocumentPath.of("path", "document.txt"), UTF_8);

      document
            .write(new DocumentBuilder()
                         .documentId(DocumentPath.of("source.txt"))
                         .loadedContent("Line 1", UTF_8)
                         .build())
            .finish();

      assertThat(document.readableContent().content().stringContent()).isEqualTo("Line 1");
   }

   @Test
   public void testWriteWhenDocumentWithMismatchingEncoding() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> GenericDocument
                  .open(DocumentPath.of("path", "document.txt"))
                  .write(new DocumentBuilder()
                               .documentId(DocumentPath.of("source.txt"))
                               .loadedContent("é", UTF_8)
                               .build()))
            .withMessage(
                  "Invariant validation error > 'document.contentEncoding=Optional[UTF-8]' must be equal to 'contentEncoding=Optional.empty'");

      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> GenericDocument
                  .open(DocumentPath.of("path", "document.txt"), UTF_8)
                  .write(new DocumentBuilder()
                               .documentId(DocumentPath.of("source.txt"))
                               .loadedContent("é".getBytes(UTF_8))
                               .build()))
            .withMessage(
                  "Invariant validation error > 'document.contentEncoding=Optional.empty' must be equal to 'contentEncoding=Optional[UTF-8]'");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> GenericDocument
                  .open(DocumentPath.of("path", "document.txt"), US_ASCII)
                  .write(new DocumentBuilder()
                               .documentId(DocumentPath.of("source.txt"))
                               .loadedContent("é", UTF_8)
                               .build()))
            .withMessage(
                  "Invariant validation error > 'document.contentEncoding=Optional[UTF-8]' must be equal to 'contentEncoding=Optional[US-ASCII]'");
   }

   /**
    * Testing {@link TypedDocument#flush()} function.
    * Caution : this is not an advised use-case, you must call closeContent() to ensure correct flush of all
    * data.
    */
   @Test
   public void testFlush() throws IOException {
      try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {

         GenericDocument document = GenericDocument.open(DocumentPath.of("path", "document.txt"), out, UTF_8);

         document.writeLn("Line 1").writeLn("Line 2");
         document.flush();

         assertThat(out.toByteArray()).asString().isEqualTo("Line 1\nLine 2\n");
         document.finish();
      }
   }

   @Test
   public void testOpenWhenContentProcessor() {
      GenericDocument document = GenericDocument
            .open(DocumentPath.of("path", "document.txt"), UTF_8)
            .process(new ContentEncodingProcessor().andThen(new ContentShifterProcessor()));

      document.writeLn("Line 1").finish();

      Document finishedDocument = document.readableContent();

      assertThat(finishedDocument.content().stringContent()).isEqualTo("Mjof!2\n");
   }

   @Test
   public void testOpenWhenContentProcessorWithDifferentEncoding() {
      GenericDocument document = GenericDocument
            .open(DocumentPath.of("path", "document.txt"), UTF_8)
            .process(new ContentEncodingProcessor(US_ASCII).andThen(new ContentShifterProcessor()));

      assertThat(document.documentId()).isEqualTo(DocumentPath.of("path/document.txt"));
      assertThat(document.metadata().documentPath()).isEqualTo(Path.of("path/document.txt"));
      assertThat(document.metadata().documentName()).isEqualTo("document.txt");
      assertThat(document.metadata().contentType()).hasValue(parseMimeType("text/plain;charset=US-ASCII"));
      assertThat(document.metadata().contentSize()).isEmpty();
      assertThat(document.metadata().creationDate()).hasValue(FIXED_DATE.toInstant());
      assertThat(document.metadata().lastUpdateDate()).hasValue(FIXED_DATE.toInstant());
      assertThat(((OutputStreamDocumentContent) document.content()).sourceEncoding()).hasValue(UTF_8);
      assertThat(document.content().contentEncoding()).hasValue(US_ASCII);
      assertThat(document.content().contentSize()).isEmpty();

      document.writeLn("Line 1").finish();

      Document finishedDocument = document.readableContent();

      assertThat(finishedDocument.content().stringContent()).isEqualTo("Mjof!2\n");
   }

   /**
    * Testing content processor that change the content by shifting characters.
    */
   public static class ContentShifterProcessor implements DocumentContentProcessor {

      @Override
      public DocumentContent process(DocumentContent documentContent) {
         validate(documentContent, "documentContent", isNotNull()).orThrow();

         if (documentContent instanceof InputStreamDocumentContent
             || documentContent instanceof LoadedDocumentContent) {
            try {
               return InputStreamDocumentContentBuilder
                     .from(documentContent)
                     .content(new ContentShifterInputStream(documentContent.inputStreamContent()))
                     .build();
            } catch (Exception e) {
               throw new DocumentAccessException(e.getMessage(), e);
            }
         } else if (documentContent instanceof OutputStreamDocumentContent) {
            try {
               return OutputStreamDocumentContentBuilder
                     .from(documentContent)
                     .contentMapper(ContentShifterOutputStream::new)
                     .build();
            } catch (Exception e) {
               throw new DocumentAccessException(e.getMessage(), e);
            }
         } else {
            throw new IllegalStateException(String.format("Unsupported '%s' content type",
                                                          documentContent.getClass().getName()));
         }
      }

      /**
       * Testing content processor that change the content encoding.
       */
      public static class ContentEncodingProcessor implements DocumentContentProcessor {
         private static final Charset DEFAULT_PROCESSOR_ENCODING = UTF_8;

         private final Charset encoding;

         protected ContentEncodingProcessor(Charset encoding) {
            this.encoding = encoding;
         }

         protected ContentEncodingProcessor() {
            this(DEFAULT_PROCESSOR_ENCODING);
         }

         @Override
         public DocumentContent process(DocumentContent documentContent) {
            validate(documentContent, "documentContent", isNotNull()).orThrow();

            if (documentContent instanceof InputStreamDocumentContent
                || documentContent instanceof LoadedDocumentContent) {
               try {
                  return InputStreamDocumentContentBuilder
                        .from(documentContent)
                        .contentEncoding(encoding)
                        .build();
               } catch (Exception e) {
                  throw new DocumentAccessException(e.getMessage(), e);
               }
            } else if (documentContent instanceof OutputStreamDocumentContent) {
               try {
                  return OutputStreamDocumentContentBuilder
                        .from(documentContent)
                        .contentEncoding(encoding)
                        .build();
               } catch (Exception e) {
                  throw new DocumentAccessException(e.getMessage(), e);
               }
            } else {
               throw new IllegalStateException(String.format("Unsupported '%s' content type",
                                                             documentContent.getClass().getName()));
            }
         }

      }

      /**
       * Testing input stream implementation that change the stream content by shifting characters.
       */
      public static class ContentShifterInputStream extends InputStream {

         private final InputStream inputStream;

         public ContentShifterInputStream(InputStream inputStream) {
            this.inputStream = inputStream;
         }

         @Override
         public int read() throws IOException {
            int b = inputStream.read();
            return b != '\n' ? b + 1 : b;
         }
      }

      /**
       * Testing output stream implementation that change the stream content by shifting characters.
       */
      public static class ContentShifterOutputStream extends OutputStream {
         private final OutputStream outputStream;

         public ContentShifterOutputStream(OutputStream outputStream) {
            this.outputStream = outputStream;
         }

         @Override
         public void write(int b) throws IOException {
            outputStream.write(b != '\n' ? b + 1 : b);
         }
      }
   }

}
