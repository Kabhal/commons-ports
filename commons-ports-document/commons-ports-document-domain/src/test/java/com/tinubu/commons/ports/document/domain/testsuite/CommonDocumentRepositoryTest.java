/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.testsuite;

import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.mimeType;
import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.parseMimeType;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_PDF;
import static com.tinubu.commons.lang.util.CollectionUtils.entry;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.StreamUtils.inputStream;
import static com.tinubu.commons.lang.util.StreamUtils.reader;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.time.temporal.ChronoUnit.MILLIS;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.ArrayUtils.toPrimitive;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.clearInvocations;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.io.ByteArrayInputStream;
import java.io.CharArrayReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.assertj.core.api.Assertions;
import org.assertj.core.api.InstanceOfAssertFactories;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledIf;

import com.tinubu.commons.ddd2.criterion.Criterion.CriterionBuilder;
import com.tinubu.commons.ddd2.domain.event.DomainEvent;
import com.tinubu.commons.ddd2.domain.event.DomainEventListener;
import com.tinubu.commons.ddd2.domain.specification.CompositeSpecification;
import com.tinubu.commons.ddd2.domain.specification.Specification;
import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.lang.datetime.ApplicationClock;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentContent;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentEntry.DocumentEntryBuilder;
import com.tinubu.commons.ports.document.domain.DocumentEntryCriteria.DocumentEntryCriteriaBuilder;
import com.tinubu.commons.ports.document.domain.DocumentMetadata;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.LoadedDocumentContent.LoadedDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata.OpenDocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.event.DocumentAccessed;
import com.tinubu.commons.ports.document.domain.event.DocumentDeleted;
import com.tinubu.commons.ports.document.domain.event.DocumentSaved;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;

// FIXME add sameRepositoryAs tests
// FIXME add attributes tests
// FIXME add creation/lastUpdateDate tests
// FIXME test DocumentAccessException (simulate IO errors ?)
// FIXME test DocumentAccessException -> events not sent

/**
 * Common testsuite for all {@link DocumentRepository} implementations.
 */
public abstract class CommonDocumentRepositoryTest extends BaseDocumentRepositoryTest {

   static {
      Assertions.setMaxStackTraceElementsDisplayed(50);
   }

   @Test
   public void testFindDocumentByIdWhenNominal() {
      Path documentPath = path("path/pathfile1.pdf");
      DocumentPath testDocument = DocumentPath.of(documentPath);

      DomainEventListener<DomainEvent> domainEventListener = instrumentDocumentRepositoryListeners();

      try {
         createDocuments(documentPath);

         clearInvocations(domainEventListener);

         assertThat(documentRepository().findDocumentById(testDocument)).hasValueSatisfying(document -> {
            assertThat(document)
                  .usingComparator(contentAgnosticDocumentComparator())
                  .isEqualTo(new DocumentBuilder()
                                   .<DocumentBuilder>reconstitute()
                                   .documentId(testDocument)
                                   .streamContent(new ByteArrayInputStream("path/pathfile1.pdf".getBytes(UTF_8)),
                                                  document.metadata().contentEncoding().orElse(null),
                                                  isSupportingContentLength() ? 18L : null)
                                   .metadata(new DocumentMetadataBuilder()
                                                   .<DocumentMetadataBuilder>reconstitute()
                                                   .documentPath(documentPath)
                                                   .creationDate(stubCreationDate())
                                                   .lastUpdateDate(stubLastUpdateDate())
                                                   .contentSize(isSupportingContentLength() ? 18L : null)
                                                   .contentType(parseMimeType("application/pdf"), UTF_8)
                                                   .chain(synchronizeExpectedMetadata(document.metadata()))
                                                   .build())
                                   .build());

         });

         verify(domainEventListener).accept(any(DocumentAccessed.class));
         verifyNoMoreInteractions(domainEventListener);

      } finally {
         deleteDocuments(documentPath);
      }
   }

   @Test
   @EnabledIf(
         "com.tinubu.commons.ports.document.domain.testsuite.CommonDocumentRepositoryTest#isSupportingMetadataAttributes")
   public void testFindDocumentByIdWhenAttributes() {
      Path documentPath = path("path/pathfile1.pdf");
      DocumentPath testDocument = DocumentPath.of(documentPath);

      try {
         documentRepository().saveDocument(new DocumentBuilder()
                                                 .documentId(testDocument)
                                                 .attributes(map(entry("key1", "value1"),
                                                                 entry("key2", "value2")))
                                                 .streamContent("content", UTF_8)
                                                 .build(), true);

         assertThat(documentRepository().findDocumentEntryById(testDocument)).hasValueSatisfying(document -> {
            assertThat(document.metadata().attributes()).isEqualTo(map(entry("key1", "value1"),
                                                                       entry("key2", "value2")));
         });

      } finally {
         deleteDocuments(documentPath);
      }
   }

   @Test
   @EnabledIf(
         "com.tinubu.commons.ports.document.domain.testsuite.CommonDocumentRepositoryTest#isSupportingMetadataAttributes")
   public void testFindDocumentEntryByIdWhenAttributes() {
      Path documentPath = path("path/pathfile1.pdf");
      DocumentPath testDocument = DocumentPath.of(documentPath);

      try {
         documentRepository().saveDocument(new DocumentBuilder()
                                                 .documentId(testDocument)
                                                 .attributes(map(entry("key1", "value1"),
                                                                 entry("key2", "value2")))
                                                 .streamContent("content", UTF_8)
                                                 .build(), true);

         assertThat(documentRepository().findDocumentEntryById(testDocument)).hasValueSatisfying(document -> {
            assertThat(document.metadata().attributes()).isEqualTo(map(entry("key1", "value1"),
                                                                       entry("key2", "value2")));
         });

      } finally {
         deleteDocuments(documentPath);
      }
   }

   @Test
   @EnabledIf(
         "com.tinubu.commons.ports.document.domain.testsuite.CommonDocumentRepositoryTest#isSupportingMetadataAttributes")
   public void testFindDocumentsBySpecificationWhenAttributes() {
      Path documentPath = path("path/pathfile1.pdf");
      DocumentPath testDocument = DocumentPath.of(documentPath);

      try {
         documentRepository().saveDocument(new DocumentBuilder()
                                                 .documentId(testDocument)
                                                 .attributes(map(entry("key1", "value1"),
                                                                 entry("key2", "value2")))
                                                 .streamContent("content", UTF_8)
                                                 .build(), true);

         try (Stream<Document> documents = documentRepository().findDocumentsBySpecification(new DocumentEntryCriteriaBuilder()
                                                                                                   .attribute(
                                                                                                         "key1",
                                                                                                         CriterionBuilder.equal(
                                                                                                               "value1"))
                                                                                                   .build())) {
            assertThat(documents.map(DocumentRepository.autoCloseDocument(Document::metadata))).satisfiesExactly(
                  metadata -> {
                     assertThat(metadata.attributes()).isEqualTo(map(entry("key1", "value1"),
                                                                     entry("key2", "value2")));
                  });
         }

         acceptClosingStream(documentRepository().findDocumentsBySpecification(new DocumentEntryCriteriaBuilder()
                                                                                     .attribute("key1",
                                                                                                CriterionBuilder.equal(
                                                                                                      "othervalue"))
                                                                                     .build()),
                             s -> assertThat(s).isEmpty());

         acceptClosingStream(documentRepository().findDocumentsBySpecification(new DocumentEntryCriteriaBuilder()
                                                                                     .attribute("otherkey",
                                                                                                CriterionBuilder.equal(
                                                                                                      "othervalue"))
                                                                                     .build()),
                             s -> assertThat(s).isEmpty());

      } finally {
         deleteDocuments(documentPath);
      }
   }

   @Test
   @EnabledIf(
         "com.tinubu.commons.ports.document.domain.testsuite.CommonDocumentRepositoryTest#isSupportingMetadataAttributes")
   public void testFindDocumentEntriesBySpecificationWhenAttributes() {
      Path documentPath = path("path/pathfile1.pdf");
      DocumentPath testDocument = DocumentPath.of(documentPath);

      try {
         documentRepository().saveDocument(new DocumentBuilder()
                                                 .documentId(testDocument)
                                                 .attributes(map(entry("key1", "value1"),
                                                                 entry("key2", "value2")))
                                                 .streamContent("content", UTF_8)
                                                 .build(), true);

         assertThat(documentRepository().findDocumentEntriesBySpecification(new DocumentEntryCriteriaBuilder()
                                                                                  .attribute("key1",
                                                                                             CriterionBuilder.equal(
                                                                                                   "value1"))
                                                                                  .build()))
               .extracting(DocumentEntry::metadata)
               .satisfiesExactly(metadata -> {
                  assertThat(metadata.attributes()).isEqualTo(map(entry("key1", "value1"),
                                                                  entry("key2", "value2")));
               });

         assertThat(documentRepository().findDocumentEntriesBySpecification(new DocumentEntryCriteriaBuilder()
                                                                                  .attribute("key1",
                                                                                             CriterionBuilder.equal(
                                                                                                   "othervalue"))
                                                                                  .build())).isEmpty();

         assertThat(documentRepository().findDocumentEntriesBySpecification(new DocumentEntryCriteriaBuilder()
                                                                                  .attribute("otherkey",
                                                                                             CriterionBuilder.equal(
                                                                                                   "othervalue"))
                                                                                  .build())).isEmpty();

      } finally {
         deleteDocuments(documentPath);
      }
   }

   @Test
   public void testFindDocumentByIdWhenNotFound() {
      DomainEventListener<DomainEvent> domainEventListener = instrumentDocumentRepositoryListeners();

      assertThat(documentRepository().findDocumentById(DocumentPath.of("unknown.pdf"))).isEmpty();
      assertThat(documentRepository().findDocumentById(DocumentPath.of("path/unknown.pdf"))).isEmpty();
      assertThat(documentRepository().findDocumentById(DocumentPath.of("nonexistentpath/unknown.pdf"))).isEmpty();

      verifyNoMoreInteractions(domainEventListener);

   }

   @Test
   public void testFindDocumentsBySpecificationWhenNominal() {
      DomainEventListener<DomainEvent> domainEventListener = instrumentDocumentRepositoryListeners();

      String[] createDocuments = new String[] {
            "rootfile1.pdf",
            "rootfile2.txt",
            "path/pathfile1.pdf",
            "path/pathfile2.txt",
            "path/subpath/subpathfile1.pdf",
            "path/subpath/subpathfile2.txt" };

      try {
         createDocuments(createDocuments);

         clearInvocations(domainEventListener);

         try (Stream<Document> documents = documentRepository().findDocumentsBySpecification(new DocumentEntryCriteriaBuilder()
                                                                                                   .documentParentPath(
                                                                                                         CriterionBuilder.equal(
                                                                                                               path("path")))
                                                                                                   .build())) {

            assertThat(documents.map(DocumentRepository.autoCloseDocument((Document d) -> d
                  .documentId()
                  .stringValue()))).containsExactlyInAnyOrder("path/pathfile1.pdf", "path/pathfile2.txt");

            verify(domainEventListener, times(2)).accept(any(DocumentAccessed.class));
            verifyNoMoreInteractions(domainEventListener);
         }
      } finally {
         deleteDocuments(createDocuments);
      }
   }

   @Test
   public void testFindDocumentsBySpecificationWhenParallel() {
      DomainEventListener<DomainEvent> domainEventListener = instrumentDocumentRepositoryListeners();

      String[] createDocuments = new String[] {
            "rootfile1.pdf",
            "rootfile2.txt",
            "path/pathfile1.pdf",
            "path/pathfile2.txt",
            "path/subpath/subpathfile1.pdf",
            "path/subpath/subpathfile2.txt" };

      try {
         createDocuments(createDocuments);

         clearInvocations(domainEventListener);

         try (Stream<Document> documents = documentRepository()
               .findDocumentsBySpecification(new DocumentEntryCriteriaBuilder()
                                                   .documentParentPath(CriterionBuilder.equal(path("path")))
                                                   .build())
               .parallel()) {

            assertThat(documents.map(DocumentRepository.autoCloseDocument((Document d) -> d
                  .documentId()
                  .stringValue()))).containsExactlyInAnyOrder("path/pathfile1.pdf", "path/pathfile2.txt");

            verify(domainEventListener, times(2)).accept(any(DocumentAccessed.class));
            verifyNoMoreInteractions(domainEventListener);
         }
      } finally {
         deleteDocuments(createDocuments);
      }
   }

   @Test
   public void testSaveDocumentWhenNominal() {
      Path documentPath = path("path/pathfile1.pdf");
      DocumentPath testDocument = DocumentPath.of(documentPath);

      DomainEventListener<DomainEvent> domainEventListener = instrumentDocumentRepositoryListeners();

      deleteDocuments(documentPath);

      try {
         clearInvocations(domainEventListener);

         Optional<DocumentEntry> savedDocument = documentRepository().saveDocument(new DocumentBuilder()
                                                                                         .documentId(
                                                                                               testDocument)
                                                                                         .streamContent(
                                                                                               "content",
                                                                                               UTF_8)
                                                                                         .build(), true);
         assertThat(savedDocument).hasValueSatisfying(entry -> {
            assertThat(entry.documentId()).isEqualTo(testDocument);
         });

         verify(domainEventListener).accept(any(DocumentSaved.class));
         verifyNoMoreInteractions(domainEventListener);

         assertThat(documentRepository().findDocumentById(testDocument)).hasValueSatisfying(document -> {
            assertThat(document)
                  .usingComparator(contentAgnosticDocumentComparator())
                  .isEqualTo(new DocumentBuilder()
                                   .<DocumentBuilder>reconstitute()
                                   .documentId(testDocument)
                                   .streamContent("content".getBytes(UTF_8),
                                                  document.metadata().contentEncoding().orElse(null))
                                   .metadata(new DocumentMetadataBuilder()
                                                   .<DocumentMetadataBuilder>reconstitute()
                                                   .documentPath(documentPath)
                                                   .contentType(parseMimeType("application/pdf"), UTF_8)
                                                   .contentSize(7L)
                                                   .chain(synchronizeExpectedMetadata(document.metadata()))
                                                   .build())
                                   .build());
         });

      } finally {
         deleteDocuments(documentPath);
      }

   }

   @Test
   public void testSaveDocumentWhenOverwrite() throws InterruptedException {
      Path documentPath = path("path/pathfile1.pdf");
      DocumentPath testDocument = DocumentPath.of(documentPath);

      DomainEventListener<DomainEvent> domainEventListener = instrumentDocumentRepositoryListeners();

      deleteDocuments(documentPath);

      assertThat(documentRepository().findDocumentById(testDocument)).isEmpty();

      try {
         Document document =
               new DocumentBuilder().documentId(testDocument).loadedContent("content", UTF_8).build();

         clearInvocations(domainEventListener);

         Optional<DocumentEntry> initialSave = documentRepository().saveDocument(document, true);
         assertThat(initialSave).isPresent();

         verify(domainEventListener).accept(any(DocumentSaved.class));
         verifyNoMoreInteractions(domainEventListener);

         long waitTimeMs = 1200; // Wait at least 1s to ensure overridden file's lastUpdateDate > creationDate
         Thread.sleep(waitTimeMs);
         ApplicationClock.setFixedClock(FIXED_DATE.plus(waitTimeMs, MILLIS));

         clearInvocations(domainEventListener);

         Optional<DocumentEntry> overwriteSave = documentRepository().saveDocument(document, true);

         assertThat(overwriteSave)
               .as("document save with overwrite flag must return value even if file already exist")
               .isPresent();
         verify(domainEventListener).accept(any(DocumentSaved.class));
         verifyNoMoreInteractions(domainEventListener);

         assertThat(overwriteSave).hasValueSatisfying(entry -> {
            assertThat(entry.documentId()).isEqualTo(testDocument);
         });

         if (!isUpdatingCreationDateOnOverwrite()) {
            assertThat(overwriteSave
                             .get()
                             .metadata()
                             .creationDate()).satisfiesAnyOf(cd -> assertThat(cd).isEmpty(),
                                                             cd -> assertThat(cd)
                                                                   .as("overridden file creationDate must not be updated")
                                                                   .hasValue(initialSave
                                                                                   .get()
                                                                                   .metadata()
                                                                                   .creationDate()
                                                                                   .get()));
            assertThat(overwriteSave
                             .get()
                             .metadata()
                             .lastUpdateDate()).satisfiesAnyOf(cd -> assertThat(cd).isEmpty(),
                                                               cd -> assertThat(cd)
                                                                     .as("overridden file lastUpdateDate must be > creationDate")
                                                                     .get(InstanceOfAssertFactories.INSTANT)
                                                                     .isAfter(initialSave
                                                                                    .get()
                                                                                    .metadata()
                                                                                    .lastUpdateDate()
                                                                                    .get()));
         }

         clearInvocations(domainEventListener);

         assertThat(documentRepository().saveDocument(document, false))
               .as("document save without overwrite flag must return empty value if file already exist")
               .isEmpty();

         verifyNoMoreInteractions(domainEventListener);

      } finally {
         deleteDocuments(documentPath);
      }

   }

   @Test
   public void testSaveAndReturnDocumentWhenNominal() {
      Path documentPath = path("path/pathfile1.pdf");
      DocumentPath testDocument = DocumentPath.of(documentPath);

      DomainEventListener<DomainEvent> domainEventListener = instrumentDocumentRepositoryListeners();

      deleteDocuments(documentPath);

      try {
         clearInvocations(domainEventListener);

         Optional<Document> savedDocument = documentRepository().saveAndReturnDocument(new DocumentBuilder()
                                                                                             .documentId(
                                                                                                   testDocument)
                                                                                             .streamContent(
                                                                                                   "content",
                                                                                                   UTF_8)
                                                                                             .build(), true);
         try {
            assertThat(savedDocument).hasValueSatisfying(entry -> {
               assertThat(entry.documentId()).isEqualTo(testDocument);
            });

            verify(domainEventListener).accept(any(DocumentSaved.class));
            verifyNoMoreInteractions(domainEventListener);

         } finally {
            savedDocument.ifPresent(d -> d.content().close());
         }

         assertThat(documentRepository().findDocumentById(testDocument)).hasValueSatisfying(document -> {
            assertThat(document)
                  .usingComparator(contentAgnosticDocumentComparator())
                  .isEqualTo(new DocumentBuilder()
                                   .<DocumentBuilder>reconstitute()
                                   .documentId(testDocument)
                                   .streamContent("content".getBytes(UTF_8),
                                                  document.metadata().contentEncoding().orElse(null))
                                   .metadata(new DocumentMetadataBuilder()
                                                   .<DocumentMetadataBuilder>reconstitute()
                                                   .documentPath(documentPath)
                                                   .contentType(parseMimeType("application/pdf"), UTF_8)
                                                   .contentSize(7L)
                                                   .chain(synchronizeExpectedMetadata(document.metadata()))
                                                   .build())
                                   .build());
         });

      } finally {
         deleteDocuments(documentPath);
      }

   }

   @Test
   public void testSaveAndReturnDocumentWhenOverwrite() throws InterruptedException {
      Path documentPath = path("path/pathfile1.pdf");
      DocumentPath testDocument = DocumentPath.of(documentPath);

      DomainEventListener<DomainEvent> domainEventListener = instrumentDocumentRepositoryListeners();

      deleteDocuments(documentPath);

      assertThat(documentRepository().findDocumentById(testDocument)).isEmpty();

      try {
         Document document =
               new DocumentBuilder().documentId(testDocument).loadedContent("content", UTF_8).build();

         clearInvocations(domainEventListener);

         Optional<Document> initialSave = documentRepository().saveAndReturnDocument(document, true);

         try {
            assertThat(initialSave).isPresent();

            verify(domainEventListener).accept(any(DocumentSaved.class));
            verifyNoMoreInteractions(domainEventListener);
         } finally {
            initialSave.ifPresent(d -> d.content().close());
         }

         long waitTimeMs = 1200; // Wait at least 1s to ensure overridden file's lastUpdateDate > creationDate
         Thread.sleep(waitTimeMs);
         ApplicationClock.setFixedClock(FIXED_DATE.plus(waitTimeMs, MILLIS));

         clearInvocations(domainEventListener);

         Optional<Document> overwriteSave = documentRepository().saveAndReturnDocument(document, true);

         try {
            assertThat(overwriteSave)
                  .as("document save with overwrite flag must return value even if file already exist")
                  .isPresent();

            verify(domainEventListener).accept(any(DocumentSaved.class));
            verifyNoMoreInteractions(domainEventListener);

            assertThat(overwriteSave).hasValueSatisfying(entry -> {
               assertThat(entry.documentId()).isEqualTo(testDocument);
            });

            if (!isUpdatingCreationDateOnOverwrite()) {
               assertThat(overwriteSave
                                .get()
                                .metadata()
                                .creationDate()).satisfiesAnyOf(cd -> assertThat(cd).isEmpty(),
                                                                cd -> assertThat(cd)
                                                                      .as("overridden file creationDate must not be updated")
                                                                      .hasValue(initialSave
                                                                                      .get()
                                                                                      .metadata()
                                                                                      .creationDate()
                                                                                      .get()));
               assertThat(overwriteSave
                                .get()
                                .metadata()
                                .lastUpdateDate()).satisfiesAnyOf(cd -> assertThat(cd).isEmpty(),
                                                                  cd -> assertThat(cd)
                                                                        .as("overridden file lastUpdateDate must be > creationDate")
                                                                        .get(InstanceOfAssertFactories.INSTANT)
                                                                        .isAfter(initialSave
                                                                                       .get()
                                                                                       .metadata()
                                                                                       .lastUpdateDate()
                                                                                       .get()));
            }
         } finally {
            overwriteSave.ifPresent(d -> d.content().close());
         }

         clearInvocations(domainEventListener);

         assertThat(documentRepository().saveAndReturnDocument(document, false))
               .as("document save without overwrite flag must return empty value if file already exist")
               .isEmpty();

         verifyNoMoreInteractions(domainEventListener);

      } finally {
         deleteDocuments(documentPath);
      }

   }

   @Test
   public void testDeleteDocumentByIdWhenNominal() {
      Path documentPath = path("path/pathfile1.pdf");
      DocumentPath testDocument = DocumentPath.of(documentPath);

      DomainEventListener<DomainEvent> domainEventListener = instrumentDocumentRepositoryListeners();

      DocumentEntry document = createDocument(documentPath);

      try {
         clearInvocations(domainEventListener);

         assertThat(documentRepository().deleteDocumentById(testDocument)).hasValueSatisfying(documentEntry -> {
            assertThat(documentEntry).isEqualTo(new DocumentEntryBuilder()
                                                      .<DocumentEntryBuilder>reconstitute()
                                                      .documentId(testDocument)
                                                      .metadata(new DocumentMetadataBuilder()
                                                                      .<DocumentMetadataBuilder>reconstitute()
                                                                      .documentPath(documentPath)
                                                                      .contentSize(18L)
                                                                      .contentType(parseMimeType(
                                                                            "application/pdf"), UTF_8)
                                                                      .chain(synchronizeExpectedMetadata(
                                                                            document.metadata()))
                                                                      .build())
                                                      .build());
         });

         verify(domainEventListener).accept(any(DocumentDeleted.class));
         verifyNoMoreInteractions(domainEventListener);

      } finally {
         deleteDocuments(documentPath);
      }
   }

   @Test
   public void testDeleteDocumentByIdWhenNotExist() {
      Path documentPath = path("path/subpath/missing.pdf");
      DocumentPath testDocument = DocumentPath.of(documentPath);

      DomainEventListener<DomainEvent> domainEventListener = instrumentDocumentRepositoryListeners();

      assertThat(documentRepository().deleteDocumentById(testDocument)).isEmpty();

      verifyNoMoreInteractions(domainEventListener);
   }

   @Test
   public void testDeleteDocumentsBySpecificationWhenNominal() {
      DomainEventListener<DomainEvent> domainEventListener = instrumentDocumentRepositoryListeners();

      String[] createDocuments = new String[] {
            "rootfile1.pdf",
            "rootfile2.txt",
            "path/pathfile1.pdf",
            "path/pathfile2.txt",
            "path/subpath/subpathfile1.pdf",
            "path/subpath/subpathfile2.txt" };

      try {
         createDocuments(createDocuments);

         clearInvocations(domainEventListener);

         List<DocumentEntry> documentEntries =
               documentRepository().deleteDocumentsBySpecification(new DocumentEntryCriteriaBuilder()
                                                                         .documentParentPath(CriterionBuilder.equal(
                                                                               path("path")))
                                                                         .build());

         verify(domainEventListener, times(2)).accept(any(DocumentDeleted.class));
         verifyNoMoreInteractions(domainEventListener);

         assertThat(documentEntries)
               .extracting(entry -> entry.documentId().stringValue())
               .containsExactlyInAnyOrder("path/pathfile1.pdf", "path/pathfile2.txt");
      } finally {
         deleteDocuments(createDocuments);
      }
   }

   @Test
   public void testDeleteDocumentsBySpecificationWhenDocumentEntrySpecificationWithNoDirectory() {
      String[] createDocuments = new String[] {
            "path/pathfile1.pdf",
            "path/pathfile2.txt",
            "path/subpath/subpathfile1.pdf",
            "path/subpath/subpathfile2.txt" };

      try {
         createDocuments(createDocuments);

         List<DocumentEntry> documentEntries =
               documentRepository().deleteDocumentsBySpecification(path("path"),
                                                                   new DocumentEntryCriteriaBuilder().build());

         assertThat(documentEntries)
               .extracting(entry -> entry.documentId().stringValue())
               .containsExactlyInAnyOrder("path/pathfile1.pdf",
                                          "path/pathfile2.txt",
                                          "path/subpath/subpathfile1.pdf",
                                          "path/subpath/subpathfile2.txt");
      } finally {
         deleteDocuments(createDocuments);
      }
   }

   @Test
   public void testDeleteDocumentsBySpecificationWhenDocumentEntrySpecificationWithDirectoriesAndExtensions() {
      String[] createDocuments = new String[] {
            "rootfile1.pdf",
            "rootfile2.txt",
            "path/pathfile1.pdf",
            "path/pathfile2.txt",
            "path/subpath/subpathfile1.pdf",
            "path/subpath/subpathfile2.txt",
            "otherpath/subpath/subpathfile1.pdf",
            "otherpath/subpath/subpathfile2.txt" };

      try {
         createDocuments(createDocuments);

         List<DocumentEntry> documentEntries =
               documentRepository().deleteDocumentsBySpecification(new DocumentEntryCriteriaBuilder()
                                                                         .documentParentPath(CriterionBuilder.in(
                                                                               path("path"),
                                                                               path("otherpath/subpath")))
                                                                         .documentExtension(CriterionBuilder.equal(
                                                                               "pdf"))
                                                                         .build());

         assertThat(documentEntries)
               .extracting(entry -> entry.documentId().stringValue())
               .containsExactlyInAnyOrder("path/pathfile1.pdf", "otherpath/subpath/subpathfile1.pdf");
      } finally {
         deleteDocuments(createDocuments);
      }
   }

   @Test
   public void testDeleteDocumentsBySpecificationWhenBaseDirectory() {
      String[] createDocuments = new String[] {
            "path/pathfile1.pdf",
            "path/pathfile2.txt",
            "path/subpath/subpathfile1.pdf",
            "path/subpath/subpathfile2.txt" };

      try {
         createDocuments(createDocuments);

         List<DocumentEntry> documentEntries =
               documentRepository().deleteDocumentsBySpecification(path("path"),
                                                                   new DocumentEntryCriteriaBuilder().build());

         assertThat(documentEntries)
               .extracting(entry -> entry.documentId().stringValue())
               .containsExactlyInAnyOrder("path/pathfile1.pdf",
                                          "path/pathfile2.txt",
                                          "path/subpath/subpathfile1.pdf",
                                          "path/subpath/subpathfile2.txt");

      } finally {
         deleteDocuments(createDocuments);
      }
   }

   @Test
   public void testDeleteDocumentsBySpecificationWhenAbsoluteBaseDirectory() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> documentRepository().deleteDocumentsBySpecification(path("/path"),
                                                                                  new DocumentEntryCriteriaBuilder().build()))
            .withMessage("Invariant validation error > 'basePath=/path' must not be absolute path");
   }

   @Test
   public void testDeleteDocumentsBySpecificationWhenTraversalBaseDirectory() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> documentRepository().deleteDocumentsBySpecification(path("path/.."),
                                                                                  new DocumentEntryCriteriaBuilder().build()))
            .withMessage("Invariant validation error > 'basePath=path/..' must not have traversal paths");
   }

   @Test
   public void testFindDocumentEntryByIdWhenNominal() {
      Path documentPath = path("path/pathfile1.pdf");
      DocumentPath testDocument = DocumentPath.of(documentPath);

      DomainEventListener<DomainEvent> domainEventListener = instrumentDocumentRepositoryListeners();

      try {
         createDocuments(documentPath);

         clearInvocations(domainEventListener);

         Optional<DocumentEntry> document = documentRepository().findDocumentEntryById(testDocument);

         verify(domainEventListener).accept(any(DocumentAccessed.class));
         verifyNoMoreInteractions(domainEventListener);

         assertThat(document).hasValue(new DocumentEntryBuilder()
                                             .<DocumentEntryBuilder>reconstitute()
                                             .documentId(testDocument)
                                             .metadata(new DocumentMetadataBuilder()
                                                             .<DocumentMetadataBuilder>reconstitute()
                                                             .documentPath(documentPath)
                                                             .contentSize(isSupportingContentLength()
                                                                          ? 18L
                                                                          : null)
                                                             .contentType(parseMimeType("application/pdf"),
                                                                          UTF_8)
                                                             .chain(synchronizeExpectedMetadata(document
                                                                                                      .map(DocumentEntry::metadata)
                                                                                                      .orElse(
                                                                                                            null)))
                                                             .build())
                                             .build());
      } finally {
         deleteDocuments(documentPath);
      }
   }

   @Test
   public void testFindDocumentEntryByIdWhenNotFound() {
      DomainEventListener<DomainEvent> domainEventListener = instrumentDocumentRepositoryListeners();

      DocumentPath missingDocument = DocumentPath.of(path("missing.pdf"));
      DocumentPath missingDocumentInPath = DocumentPath.of(path("path/missing.pdf"));
      DocumentPath missingDocumentInMissingPath = DocumentPath.of(path("notexist/missing.pdf"));

      deleteDocuments(missingDocument, missingDocumentInPath, missingDocumentInMissingPath);

      clearInvocations(domainEventListener);

      assertThat(documentRepository().findDocumentEntryById(missingDocument)).isEmpty();
      assertThat(documentRepository().findDocumentEntryById(missingDocumentInPath)).isEmpty();
      assertThat(documentRepository().findDocumentEntryById(missingDocumentInMissingPath)).isEmpty();

      verifyNoMoreInteractions(domainEventListener);
   }

   @Test
   public void testFindDocumentEntriesBySpecificationWhenNominal() {
      DomainEventListener<DomainEvent> domainEventListener = instrumentDocumentRepositoryListeners();

      String[] createDocuments = new String[] {
            "rootfile1.pdf",
            "rootfile2.txt",
            "path/pathfile1.pdf",
            "path/pathfile2.txt",
            "path/subpath/subpathfile1.pdf",
            "path/subpath/subpathfile2.txt" };

      try {
         createDocuments(createDocuments);

         clearInvocations(domainEventListener);

         List<DocumentEntry> documentEntries =
               applyClosingStream(documentRepository().findDocumentEntriesBySpecification(new DocumentEntryCriteriaBuilder()
                                                                                                .documentParentPath(
                                                                                                      CriterionBuilder.equal(
                                                                                                            path("path")))
                                                                                                .build()),
                                  s -> s.collect(toList()));

         verify(domainEventListener, times(2)).accept(any(DocumentAccessed.class));
         verifyNoMoreInteractions(domainEventListener);

         assertThat(documentEntries)
               .extracting(entry -> entry.documentId().stringValue())
               .containsExactlyInAnyOrder("path/pathfile1.pdf", "path/pathfile2.txt");
      } finally {
         deleteDocuments(createDocuments);
      }
   }

   @Test
   public void testFindDocumentEntriesBySpecificationWhenDocumentEntrySpecificationWithNoDirectory() {
      String[] createDocuments = new String[] {
            "path/pathfile1.pdf",
            "path/pathfile2.txt",
            "path/subpath/subpathfile1.pdf",
            "path/subpath/subpathfile2.txt" };

      try {
         createDocuments(createDocuments);

         try (Stream<DocumentEntry> documentEntries = documentRepository().findDocumentEntriesBySpecification(
               path("path"),
               new DocumentEntryCriteriaBuilder().build())) {

            assertThat(documentEntries)
                  .extracting(entry -> entry.documentId().stringValue())
                  .containsExactlyInAnyOrder("path/pathfile1.pdf",
                                             "path/pathfile2.txt",
                                             "path/subpath/subpathfile1.pdf",
                                             "path/subpath/subpathfile2.txt");
         }

      } finally {
         deleteDocuments(createDocuments);
      }
   }

   @Test
   public void testFindDocumentEntriesBySpecificationWhenDocumentEntrySpecificationWithDirectoriesAndExtensions() {
      String[] createDocuments = new String[] {
            "rootfile1.pdf",
            "rootfile2.txt",
            "path/pathfile1.pdf",
            "path/pathfile2.txt",
            "path/subpath/subpathfile1.pdf",
            "path/subpath/subpathfile2.txt",
            "otherpath/subpath/subpathfile1.pdf",
            "otherpath/subpath/subpathfile2.txt" };

      try {
         createDocuments(createDocuments);

         try (Stream<DocumentEntry> documentEntries = documentRepository().findDocumentEntriesBySpecification(
               new DocumentEntryCriteriaBuilder()
                     .documentParentPath(CriterionBuilder.in(path("path"), path("otherpath/subpath")))
                     .documentExtension(CriterionBuilder.equal("pdf"))
                     .build())) {

            assertThat(documentEntries)
                  .extracting(entry -> entry.documentId().stringValue())
                  .containsExactlyInAnyOrder("path/pathfile1.pdf", "otherpath/subpath/subpathfile1.pdf");
         }
      } finally {
         deleteDocuments(createDocuments);
      }
   }

   @Test
   public void testFindDocumentEntriesBySpecificationWhenBaseDirectory() {
      String[] createDocuments = new String[] {
            "path/pathfile1.pdf",
            "path/pathfile2.txt",
            "path/subpath/subpathfile1.pdf",
            "path/subpath/subpathfile2.txt" };

      try {
         createDocuments(createDocuments);

         try (Stream<DocumentEntry> documentEntries = documentRepository().findDocumentEntriesBySpecification(
               path("path"),
               new DocumentEntryCriteriaBuilder().build())) {

            assertThat(documentEntries)
                  .extracting(entry -> entry.documentId().stringValue())
                  .containsExactlyInAnyOrder("path/pathfile1.pdf",
                                             "path/pathfile2.txt",
                                             "path/subpath/subpathfile1.pdf",
                                             "path/subpath/subpathfile2.txt");
         }

      } finally {
         deleteDocuments(createDocuments);
      }
   }

   @Test
   public void testFindDocumentEntriesBySpecificationWhenAbsoluteBaseDirectory() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> documentRepository()
                  .findDocumentEntriesBySpecification(path("/path"),
                                                      new DocumentEntryCriteriaBuilder().build())
                  .count())
            .withMessage("Invariant validation error > 'basePath=/path' must not be absolute path");
   }

   @Test
   public void testFindDocumentEntriesBySpecificationWhenTraversalBaseDirectory() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> documentRepository()
                  .findDocumentEntriesBySpecification(path("path/.."),
                                                      new DocumentEntryCriteriaBuilder().build())
                  .count())
            .withMessage("Invariant validation error > 'basePath=path/..' must not have traversal paths");
   }

   @Test
   public void testFindDocumentEntriesBySpecificationWhenArbitrarySpecification() {
      String[] createDocuments = new String[] {
            "rootfile1.pdf",
            "rootfile2.txt",
            "path/pathfile1.pdf",
            "path/pathfile2.txt",
            "path/subpath/subpathfile1.pdf",
            "path/subpath/subpathfile2.txt" };

      try {
         createDocuments(createDocuments);

         Specification<DocumentEntry> arbitrarySpecification =
               entry -> (entry.documentId().sameValueAs(DocumentPath.of("path/pathfile1.pdf")) || entry
                     .documentId()
                     .stringValue()
                     .contains("/subpath/") || entry.metadata().contentSize().orElse(0L) < 16) && entry
                              .documentId()
                              .stringValue()
                              .endsWith(".pdf") && !entry.documentId().stringValue().startsWith("otherpath/");

         try (Stream<DocumentEntry> documentEntries = documentRepository().findDocumentEntriesBySpecification(
               arbitrarySpecification)) {

            assertThat(documentEntries)
                  .extracting(entry -> entry.documentId().stringValue())
                  .containsExactlyInAnyOrder("rootfile1.pdf",
                                             "path/pathfile1.pdf",
                                             "path/subpath/subpathfile1.pdf");
         }

      } finally {
         deleteDocuments(createDocuments);
      }
   }

   @Test
   public void testFindDocumentEntriesBySpecificationWhenCompositeSpecification() {
      String[] createDocuments = new String[] {
            "rootfile1.pdf",
            "rootfile2.txt",
            "path/pathfile1.pdf",
            "path/pathfile2.txt",
            "path/subpath/subpathfile1.pdf",
            "path/subpath/subpathfile2.txt" };

      try {
         createDocuments(createDocuments);

         CompositeSpecification<DocumentEntry> documentIdSpecification =
               entry -> entry.documentId().sameValueAs(DocumentPath.of("path/pathfile1.pdf"));
         CompositeSpecification<DocumentEntry> subpathSpecification =
               entry -> entry.documentId().stringValue().contains("/subpath/");
         CompositeSpecification<DocumentEntry> smallContentSpecification =
               entry -> entry.metadata().contentSize().orElse(0L) < 16;
         CompositeSpecification<DocumentEntry> pdfSpecification =
               entry -> entry.documentId().stringValue().endsWith(".pdf");
         CompositeSpecification<DocumentEntry> otherpathSpecification =
               entry -> entry.documentId().stringValue().startsWith("otherpath/");

         CompositeSpecification<DocumentEntry> compositeSpecification = documentIdSpecification
               .or(subpathSpecification)
               .or(smallContentSpecification)
               .and(pdfSpecification)
               .and(otherpathSpecification.not());

         try (Stream<DocumentEntry> documentEntries = documentRepository().findDocumentEntriesBySpecification(
               compositeSpecification)) {

            assertThat(documentEntries)
                  .extracting(entry -> entry.documentId().stringValue())
                  .containsExactlyInAnyOrder("rootfile1.pdf",
                                             "path/pathfile1.pdf",
                                             "path/subpath/subpathfile1.pdf");
         }

      } finally {
         deleteDocuments(createDocuments);
      }
   }

   @Test
   @EnabledIf(
         "com.tinubu.commons.ports.document.domain.testsuite.CommonDocumentRepositoryTest#isSupportingDocumentUri")
   public void testToUriWhenNullParameter() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> documentRepository().toUri((DocumentPath) null))
            .withMessage("Invariant validation error > 'documentId' must not be null");
   }

   @Test
   @EnabledIf(
         "com.tinubu.commons.ports.document.domain.testsuite.CommonDocumentRepositoryTest#isSupportingDocumentUri")
   public void testSupportsDocumentUriWhenUnsupportedScheme() {
      assertThat(documentRepository().supportsUri(documentUri("notexists:/path/test.txt"))).isFalse();
      assertThat(documentRepository().supportsUri(documentUri("notexists:path/test.txt"))).isFalse();
   }

   @Test
   @EnabledIf(
         "com.tinubu.commons.ports.document.domain.testsuite.CommonDocumentRepositoryTest#isSupportingRepositoryUri")
   public void testSupportsRepositoryUriWhenUnsupportedScheme() {
      assertThat(documentRepository().supportsUri(repositoryUri("notexists:/path/test.txt"))).isFalse();
      assertThat(documentRepository().supportsUri(repositoryUri("notexists:path/test.txt"))).isFalse();
   }

   @Test
   @EnabledIf(
         "com.tinubu.commons.ports.document.domain.testsuite.CommonDocumentRepositoryTest#isSupportingRepositoryUri")
   public void testSupportsRepositoryUriWhenNullUri() {
      assertThatIllegalArgumentException()
            .isThrownBy(() -> documentRepository().supportsUri(null))
            .withMessage("Invariant validation error > 'uri' must not be null");
   }

   @Test
   @EnabledIf(
         "com.tinubu.commons.ports.document.domain.testsuite.CommonDocumentRepositoryTest#isSupportingDocumentUri")
   public void testFindDocumentByUriWhenNominal() {
      DomainEventListener<DomainEvent> domainEventListener = instrumentDocumentRepositoryListeners();

      DocumentPath documentPath = DocumentPath.of("path/pathfile2.txt");

      try {
         createDocuments(documentPath);

         DocumentUri documentUri = documentRepository().toUri(documentPath);

         clearInvocations(domainEventListener);

         assertThat(documentRepository().findDocumentEntryByUri(documentUri)).hasValueSatisfying(document -> {
            assertThat(document.documentId()).isEqualTo(DocumentPath.of("path/pathfile2.txt"));
            assertThat(document.metadata().documentPath()).isEqualTo(Path.of("path/pathfile2.txt"));
         });

         verify(domainEventListener).accept(any(DocumentAccessed.class));
         verifyNoMoreInteractions(domainEventListener);

      } finally {
         deleteDocuments(documentPath);
      }
   }

   @Test
   @EnabledIf(
         "com.tinubu.commons.ports.document.domain.testsuite.CommonDocumentRepositoryTest#isSupportingDocumentUri")
   public void testFindDocumentByUriWhenNullParameter() {
      DomainEventListener<DomainEvent> domainEventListener = instrumentDocumentRepositoryListeners();

      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> documentRepository().findDocumentByUri(null))
            .withMessage("Invariant validation error > 'documentUri' must not be null");

      verifyNoMoreInteractions(domainEventListener);
   }

   @Test
   @EnabledIf(
         "com.tinubu.commons.ports.document.domain.testsuite.CommonDocumentRepositoryTest#isSupportingDocumentUri")
   public void testFindDocumentByUriWhenUnsupportedUri() {
      DomainEventListener<DomainEvent> domainEventListener = instrumentDocumentRepositoryListeners();

      assertThatExceptionOfType(InvalidUriException.class)
            .isThrownBy(() -> documentRepository().findDocumentByUri(documentUri("notexists:/path/test.txt")))
            .withMessageContaining(
                  "'documentUri=DefaultDocumentUri[value=notexists:/path/test.txt]' must be supported");
      assertThatExceptionOfType(InvalidUriException.class)
            .isThrownBy(() -> documentRepository().findDocumentByUri(documentUri("notexists:path/test.txt")))
            .withMessageContaining(
                  "'documentUri=DefaultDocumentUri[value=notexists:path/test.txt]' must be supported");

      verifyNoMoreInteractions(domainEventListener);
   }

   @Test
   public void testOpenDocumentWhenNominal() throws Exception {
      Path documentPath = path("path/pathfile1.pdf");
      DocumentPath testDocument = DocumentPath.of(documentPath);

      DomainEventListener<DomainEvent> domainEventListener = instrumentDocumentRepositoryListeners();

      try {
         OpenDocumentMetadata metadata = new OpenDocumentMetadataBuilder()
               .documentPath(documentPath)
               .contentType(mimeType(APPLICATION_PDF, UTF_8))
               .build();

         var newDocument = documentRepository().openDocument(testDocument, true, false, metadata);
         try (var newDocumentContent = newDocument.orElseThrow().closeableContent()) {
            writeContent(newDocumentContent, "content");
         }

         verify(domainEventListener).accept(any(DocumentSaved.class));
         verifyNoMoreInteractions(domainEventListener);

         assertThat(newDocument).hasValueSatisfying(od -> {
            assertThat(od.documentId()).isEqualTo(testDocument);
            assertThat(od.metadata().documentPath()).isEqualTo(documentPath);
            assertThat(od.metadata().contentType()).hasValue(mimeType(APPLICATION_PDF, UTF_8));
            assertThat(od.content().contentEncoding()).hasValue(UTF_8);
         });

         Optional<Document> document = documentRepository().findDocumentById(testDocument);

         assertThat(document)
               .usingValueComparator(contentAgnosticDocumentComparator())
               .hasValue(new DocumentBuilder()
                               .<DocumentBuilder>reconstitute()
                               .documentId(testDocument)
                               .metadata(new DocumentMetadataBuilder()
                                               .<DocumentMetadataBuilder>reconstitute()
                                               .documentPath(documentPath)
                                               .contentSize(isSupportingContentLength() ? 7L : null)
                                               .contentType(parseMimeType("application/pdf"), UTF_8)
                                               .chain(synchronizeExpectedMetadata(document
                                                                                        .map(Document::metadata)
                                                                                        .orElse(null)))
                                               .build())
                               .content(new LoadedDocumentContentBuilder()
                                              .<LoadedDocumentContentBuilder>reconstitute()
                                              .<LoadedDocumentContentBuilder>chain(b -> {
                                                 if (document
                                                       .map(Document::content)
                                                       .flatMap(DocumentContent::contentEncoding)
                                                       .isPresent()) {
                                                    b.content("content", UTF_8);
                                                 } else {
                                                    b.content("content".getBytes(UTF_8));
                                                 }
                                                 return b;
                                              })
                                              .chain(synchronizeExpectedContent(document
                                                                                      .map(Document::content)
                                                                                      .orElse(null)))
                                              .build())
                               .build());
      } finally {
         deleteDocuments(documentPath);
      }
   }

   @Test
   public void testOpenDocumentWhenWriteNothing() throws IOException {
      Path documentPath = path("path/pathfile1.pdf");
      DocumentPath testDocument = DocumentPath.of(documentPath);

      DomainEventListener<DomainEvent> domainEventListener = instrumentDocumentRepositoryListeners();

      try {
         OpenDocumentMetadata metadata = new OpenDocumentMetadataBuilder()
               .documentPath(documentPath)
               .contentType(mimeType(APPLICATION_PDF, UTF_8))
               .build();

         Optional<Document> newDocument =
               documentRepository().openDocument(testDocument, true, false, metadata);

         verify(domainEventListener).accept(any(DocumentSaved.class));
         verifyNoMoreInteractions(domainEventListener);

         assertThat(newDocument).isPresent();

         try (OutputStream output = newDocument.get().content().outputStreamContent()) {
            /* close stream */
         }

         Optional<Document> document = documentRepository().findDocumentById(testDocument);

         assertThat(document)
               .isNotEmpty()
               .usingValueComparator(contentAgnosticDocumentComparator())
               .hasValue(new DocumentBuilder()
                               .<DocumentBuilder>reconstitute()
                               .documentId(testDocument)
                               .metadata(new DocumentMetadataBuilder()
                                               .<DocumentMetadataBuilder>reconstitute()
                                               .documentPath(documentPath)
                                               .contentSize(isSupportingContentLength() ? 0L : null)
                                               .contentType(parseMimeType("application/pdf"), UTF_8)
                                               .chain(synchronizeExpectedMetadata(document
                                                                                        .map(Document::metadata)
                                                                                        .orElse(null)))
                                               .build())
                               .content(new LoadedDocumentContentBuilder()
                                              .<LoadedDocumentContentBuilder>reconstitute()
                                              .<LoadedDocumentContentBuilder>chain(b -> {
                                                 if (document
                                                       .map(Document::metadata)
                                                       .flatMap(DocumentMetadata::contentEncoding)
                                                       .isPresent()) {
                                                    b.content("", UTF_8);
                                                 } else {
                                                    b.content("".getBytes(UTF_8));
                                                 }
                                                 return b;
                                              })
                                              .chain(synchronizeExpectedContent(document
                                                                                      .map(Document::content)
                                                                                      .orElse(null)))
                                              .build())
                               .build());
      } finally {
         deleteDocuments(documentPath);
      }
   }

   @Test
   public void testOpenDocumentWhenOverwriteAndAppend() throws Exception {
      Path documentPath = path("path/pathfile1.pdf");
      DocumentPath testDocument = DocumentPath.of(documentPath);

      DomainEventListener<DomainEvent> domainEventListener = instrumentDocumentRepositoryListeners();

      try {
         OpenDocumentMetadata metadata =
               new OpenDocumentMetadataBuilder().contentType(APPLICATION_PDF).build();

         try (var newDocument = documentRepository()
               .openDocument(testDocument, true, true, metadata)
               .orElseThrow()
               .closeableContent()) {
            writeContent(newDocument, "content");
         }

         verify(domainEventListener).accept(any(DocumentSaved.class));
         verifyNoMoreInteractions(domainEventListener);

         assertThat(documentRepository().findDocumentById(testDocument)).hasValueSatisfying(nd -> assertThatDocumentContent(
               nd,
               UTF_8,
               "content"));

         clearInvocations(domainEventListener);

         try (var existingDocument = documentRepository()
               .openDocument(testDocument, true, true, metadata)
               .orElseThrow()
               .closeableContent()) {
            writeContent(existingDocument, "append");
         }

         verify(domainEventListener).accept(any(DocumentSaved.class));
         verifyNoMoreInteractions(domainEventListener);

         assertThat(documentRepository().findDocumentById(testDocument)).hasValueSatisfying(nd -> assertThatDocumentContent(
               nd,
               UTF_8,
               "contentappend"));

      } finally {
         deleteDocuments(documentPath);
      }
   }

   @Test
   public void testOpenDocumentWhenOverwriteAndNotAppend() throws Exception {
      Path documentPath = path("path/pathfile1.pdf");
      DocumentPath testDocument = DocumentPath.of(documentPath);

      DomainEventListener<DomainEvent> domainEventListener = instrumentDocumentRepositoryListeners();

      try {
         OpenDocumentMetadata metadata =
               new OpenDocumentMetadataBuilder().contentType(APPLICATION_PDF).build();

         try (var newDocument = documentRepository()
               .openDocument(testDocument, true, false, metadata)
               .orElseThrow()
               .closeableContent()) {
            writeContent(newDocument, "content");
         }

         verify(domainEventListener).accept(any(DocumentSaved.class));
         verifyNoMoreInteractions(domainEventListener);

         assertThat(documentRepository().findDocumentById(testDocument)).hasValueSatisfying(nd -> assertThatDocumentContent(
               nd,
               UTF_8,
               "content"));

         clearInvocations(domainEventListener);

         try (var existingDocument = documentRepository()
               .openDocument(testDocument, true, false, metadata)
               .orElseThrow()
               .closeableContent()) {
            writeContent(existingDocument, "overwrite");
         }

         verify(domainEventListener).accept(any(DocumentSaved.class));
         verifyNoMoreInteractions(domainEventListener);

         assertThat(documentRepository().findDocumentById(testDocument)).hasValueSatisfying(nd -> assertThatDocumentContent(
               nd,
               UTF_8,
               "overwrite"));

      } finally {
         deleteDocuments(documentPath);
      }
   }

   @Test
   public void testOpenDocumentWhenNotOverwriteAndAppend() throws Exception {
      Path documentPath = path("path/pathfile1.pdf");
      DocumentPath testDocument = DocumentPath.of(documentPath);

      DomainEventListener<DomainEvent> domainEventListener = instrumentDocumentRepositoryListeners();

      try {
         OpenDocumentMetadata metadata =
               new OpenDocumentMetadataBuilder().contentType(APPLICATION_PDF).build();

         try (var newDocument = documentRepository()
               .openDocument(testDocument, false, true, metadata)
               .orElseThrow()
               .closeableContent()) {
            writeContent(newDocument, "content");
         }

         verify(domainEventListener).accept(any(DocumentSaved.class));
         verifyNoMoreInteractions(domainEventListener);

         assertThat(documentRepository().findDocumentById(testDocument)).hasValueSatisfying(nd -> assertThatDocumentContent(
               nd,
               UTF_8,
               "content"));

         clearInvocations(domainEventListener);

         try (var existingDocument = documentRepository()
               .openDocument(testDocument, false, true, metadata)
               .orElseThrow()
               .closeableContent()) {
            writeContent(existingDocument, "append");
         }

         verify(domainEventListener).accept(any(DocumentSaved.class));
         verifyNoMoreInteractions(domainEventListener);

         assertThat(documentRepository().findDocumentById(testDocument)).hasValueSatisfying(nd -> assertThatDocumentContent(
               nd,
               UTF_8,
               "contentappend"));

      } finally {
         deleteDocuments(documentPath);
      }
   }

   @Test
   public void testOpenDocumentWhenNotOverwriteAndNotAppend() throws Exception {
      Path documentPath = path("path/pathfile1.pdf");
      DocumentPath testDocument = DocumentPath.of(documentPath);

      DomainEventListener<DomainEvent> domainEventListener = instrumentDocumentRepositoryListeners();

      try {
         OpenDocumentMetadata metadata =
               new OpenDocumentMetadataBuilder().contentType(APPLICATION_PDF).build();

         try (var newDocument = documentRepository()
               .openDocument(testDocument, false, false, metadata)
               .orElseThrow()
               .closeableContent()) {
            writeContent(newDocument, "content");
         }

         verify(domainEventListener).accept(any(DocumentSaved.class));
         verifyNoMoreInteractions(domainEventListener);

         assertThat(documentRepository().findDocumentById(testDocument)).hasValueSatisfying(nd -> assertThatDocumentContent(
               nd,
               UTF_8,
               "content"));

         clearInvocations(domainEventListener);

         Optional<Document> existingDocument =
               documentRepository().openDocument(testDocument, false, false, metadata);

         verifyNoMoreInteractions(domainEventListener);

         assertThat(existingDocument).isEmpty();

      } finally {
         deleteDocuments(documentPath);
      }
   }

   @Test
   @EnabledIf(
         "com.tinubu.commons.ports.document.domain.testsuite.CommonDocumentRepositoryTest#isTransactionalRepository")
   public void testOpenDocumentWhenTransactionalRepositoryAndWriteFailure() {
      Path documentPath = path("path/pathfile1.pdf");
      DocumentPath testDocument = DocumentPath.of(documentPath);

      DomainEventListener<DomainEvent> domainEventListener = instrumentDocumentRepositoryListeners();

      try {
         OpenDocumentMetadata metadata = new OpenDocumentMetadataBuilder()
               .documentPath(documentPath)
               .contentType(mimeType(APPLICATION_PDF, UTF_8))
               .build();

         Optional<Document> newDocument =
               documentRepository().openDocument(testDocument, true, false, metadata);

         verify(domainEventListener).accept(any(DocumentSaved.class));
         verifyNoMoreInteractions(domainEventListener);

         assertThat(newDocument).isPresent();

         try (Writer writer = newDocument.get().content().writerContent()) {
            writer.write("content");
            instrumentIOException(documentPath, true);
            writer.write("next");
         } catch (IOException e) {
         } finally {
            instrumentIOException(documentPath, false);
         }

         Optional<Document> document = documentRepository().findDocumentById(testDocument);

         assertThat(document).isEmpty();
      } finally {
         deleteDocuments(documentPath);
      }
   }

   /**
    * Document size must be greater than {@link #chunkSize()} to have a change to trigger chunking mechanisms
    * in various repository implementations.
    */
   @Test
   public void testSaveAndRestoreBigCharacterDocument() {

      DocumentPath documentId = DocumentPath.of("document.txt");

      final long size = 3 * chunkSize() / 2;

      Document document = new DocumentBuilder()
            .documentId(documentId)
            .streamContent(reader(contentGenerator.randomCharacterStream().limit(size)), UTF_8)
            .build()
            .loadContent();

      assertThat(documentRepository().saveDocument(document, true)).isNotEmpty();

      try {
         assertThat(documentRepository().findDocumentById(documentId)).hasValueSatisfying(readDocument -> {
            assertThatDocumentContent(readDocument, UTF_8, document.content().readerContent());
         });
      } finally {
         deleteDocuments(documentId);
      }
   }

   /**
    * Document size must be greater than {@link #chunkSize()} to have a change to trigger chunking mechanisms
    * in various repository implementations.
    */
   @Test
   public void testSaveAndRestoreBigByteDocument() {

      DocumentPath documentId = DocumentPath.of("document.txt");

      final long size = 3 * chunkSize() / 2;

      Document document = new DocumentBuilder()
            .documentId(documentId)
            .streamContent(inputStream(contentGenerator.randomByteStream().limit(size)), UTF_8)
            .build()
            .loadContent();

      assertThat(documentRepository().saveDocument(document, true)).isNotEmpty();

      try {
         assertThat(documentRepository().findDocumentById(documentId)).hasValueSatisfying(readDocument -> {
            assertThatDocumentContent(readDocument, document.content().inputStreamContent());
         });
      } finally {
         deleteDocuments(documentId);
      }
   }

   /**
    * Document size must be greater than {@link #chunkSize()} to have a change to trigger chunking mechanisms
    * in various repository implementations.
    */
   @Test
   public void testOpenAndRestoreBigCharacterDocument() throws Exception {

      DocumentPath documentId = DocumentPath.of("document.txt");

      final long size = 3 * chunkSize() / 2;

      char[] content =
            toPrimitive(contentGenerator.randomCharacterStream().limit(size).toArray(Character[]::new));
      Reader reader = new CharArrayReader(content);

      try (var newDocument = documentRepository()
            .openDocument(documentId, true, false)
            .orElseThrow()
            .closeableContent()) {
         writeContent(newDocument, reader);
      }

      try {
         assertThat(documentRepository().findDocumentById(documentId)).hasValueSatisfying(readDocument -> {
            assertThatDocumentContent(readDocument, UTF_8, new CharArrayReader(content));
         });
      } finally {
         deleteDocuments(documentId);
      }
   }

   /**
    * Document size must be greater than {@link #chunkSize()} to have a change to trigger chunking mechanisms
    * in various repository implementations.
    */
   @Test
   public void testOpenAndRestoreBigByteDocument() throws Exception {

      DocumentPath documentId = DocumentPath.of("document.bin");

      final long size = 3 * chunkSize() / 2;

      byte[] content = toPrimitive(contentGenerator.randomByteStream().limit(size).toArray(Byte[]::new));
      InputStream inputStream = new ByteArrayInputStream(content);

      try (var newDocument = documentRepository()
            .openDocument(documentId, true, false)
            .orElseThrow()
            .closeableContent()) {
         writeContent(newDocument, inputStream);
      }

      try {
         assertThat(documentRepository().findDocumentById(documentId)).hasValueSatisfying(readDocument -> {
            assertThatDocumentContent(readDocument, new ByteArrayInputStream(content));
         });
      } finally {
         deleteDocuments(documentId);
      }
   }

   @Test
   @EnabledIf(
         "com.tinubu.commons.ports.document.domain.testsuite.CommonDocumentRepositoryTest#isSupportingSubPath")
   public void testSubPathWhenNominal() {
      Path documentPath = path("path/subpath/subpathfile1.pdf");

      try {
         createDocuments(documentPath);

         try (DocumentRepository repository = documentRepository().subPath(Path.of("path"), false)) {
            assertThat(repository.findDocumentEntryById(DocumentPath.of("subpath/subpathfile1.pdf"))).hasValueSatisfying(
                  document -> {
                     assertThat(document.documentId()).isEqualTo(DocumentPath.of("subpath/subpathfile1.pdf"));
                  });
         }
      } finally {
         deleteDocuments(documentPath);
      }
   }

   @Test
   @EnabledIf(
         "com.tinubu.commons.ports.document.domain.testsuite.CommonDocumentRepositoryTest#isSupportingSubPath")
   public void testSubPathWhenShareContext() {
      Path documentPath = path("path/subpath/subpathfile1.pdf");

      try {
         createDocuments(documentPath);

         try (DocumentRepository repository = documentRepository().subPath(Path.of("path"), true)) {
            assertThat(repository.findDocumentEntryById(DocumentPath.of("subpath/subpathfile1.pdf"))).hasValueSatisfying(
                  document -> {
                     assertThat(document.documentId()).isEqualTo(DocumentPath.of("subpath/subpathfile1.pdf"));
                  });
         }
      } finally {
         deleteDocuments(documentPath);
      }
   }

   @Test
   @EnabledIf(
         "com.tinubu.commons.ports.document.domain.testsuite.CommonDocumentRepositoryTest#isSupportingSubPath")
   public void testSubPathWhenMultipleSubPathComponents() {
      Path documentPath = path("path/subpath/subpathfile1.pdf");

      try {
         createDocuments(documentPath);

         try (DocumentRepository repository = documentRepository().subPath(Path.of("path/subpath"), false)) {
            assertThat(repository.findDocumentEntryById(DocumentPath.of("subpathfile1.pdf"))).hasValueSatisfying(
                  document -> {
                     assertThat(document.documentId()).isEqualTo(DocumentPath.of("subpathfile1.pdf"));
                  });
         }

      } finally {
         deleteDocuments(documentPath);
      }
   }

   @Test
   @EnabledIf(
         "com.tinubu.commons.ports.document.domain.testsuite.CommonDocumentRepositoryTest#isSupportingSubPath")
   public void testSubPathWhenAbsoluteOrHasTraversal() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> documentRepository().subPath(Path.of("/path"), false))
            .withMessage("Invariant validation error > 'subPath=/path' must not be absolute path");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> documentRepository().subPath(Path.of("path/.."), false))
            .withMessage("Invariant validation error > 'subPath=path/..' must not have traversal paths");
   }

   @Test
   @EnabledIf(
         "com.tinubu.commons.ports.document.domain.testsuite.CommonDocumentRepositoryTest#isSupportingSubPath")
   public void testSubPathWhenMismatching() {
      Path documentPath = path("path/subpath/subpathfile1.pdf");

      try {
         createDocuments(documentPath);

         try (DocumentRepository repository = documentRepository().subPath(Path.of("doesnotexist"), false)) {
            assertThat(repository.findDocumentEntryById(DocumentPath.of("subpath/subpathfile1.pdf"))).isEmpty();
         }

      } finally {
         deleteDocuments(documentPath);
      }
   }

   @Test
   @EnabledIf(
         "com.tinubu.commons.ports.document.domain.testsuite.CommonDocumentRepositoryTest#isSupportingSubPath")
   public void testFindDocumentEntriesBySpecificationWhenSubPath() {
      String[] createDocuments = new String[] {
            "rootfile1.pdf",
            "rootfile2.txt",
            "path/pathfile1.pdf",
            "path/pathfile2.txt",
            "path/subpath/subpathfile1.pdf",
            "path/subpath/subpathfile2.txt" };

      try {
         createDocuments(createDocuments);

         try (DocumentRepository repository = documentRepository().subPath(Path.of("path"), false)) {
            List<DocumentEntry> documents =
                  applyClosingStream(repository.findDocumentEntriesBySpecification(new DocumentEntryCriteriaBuilder()
                                                                                         .documentParentPath(
                                                                                               CriterionBuilder.equal(
                                                                                                     path("subpath")))
                                                                                         .build()),
                                     s -> s.collect(toList()));

            assertThat(documents)
                  .extracting(entry -> entry.documentId().stringValue())
                  .containsExactlyInAnyOrder("subpath/subpathfile1.pdf", "subpath/subpathfile2.txt");
         }
      } finally {
         deleteDocuments(createDocuments);
      }
   }

}