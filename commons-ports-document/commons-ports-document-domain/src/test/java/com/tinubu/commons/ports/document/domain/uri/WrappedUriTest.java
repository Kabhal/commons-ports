/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.uri;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.net.URI;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;

public class WrappedUriTest {

   @Nested
   public class WrapUri {

      @Test
      public void wrapUriWhenNominal() {
         assertThat(WrappedUri.wrapUri(URI.create("s3://bucket"), "s3")).satisfies(wrappedUri -> {
            assertThat(wrappedUri.wrappedUri().toString()).isEqualTo("document:s3:s3://bucket");
            assertThat(wrappedUri.repositoryType()).isEqualTo("s3");
            assertThat(wrappedUri.originalUri().toString()).isEqualTo("s3://bucket");
         });
      }

      @Test
      public void wrapUriWhenCaseSensitiveRepositoryType() {
         assertThat(WrappedUri.wrapUri(URI.create("s3://bucket"), "S3")).satisfies(wrappedUri -> {
            assertThat(wrappedUri.wrappedUri().toString()).isEqualTo("document:S3:s3://bucket");
            assertThat(wrappedUri.repositoryType()).isEqualTo("S3");
            assertThat(wrappedUri.originalUri().toString()).isEqualTo("s3://bucket");
         });
      }

      @Test
      public void wrapUriWhenEmptyUri() {
         assertThat(WrappedUri.wrapUri(URI.create(""), "empty")).satisfies(wrappedUri -> {
            assertThat(wrappedUri.wrappedUri().toString()).isEqualTo("document:empty:");
            assertThat(wrappedUri.repositoryType()).isEqualTo("empty");
            assertThat(wrappedUri.originalUri().toString()).isEqualTo("");
         });
      }

      @Test
      public void wrapUriWhenBadArgument() {
         assertThatThrownBy(() -> WrappedUri.wrapUri(null, "s3"))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'originalUri' must not be null");
         assertThatThrownBy(() -> WrappedUri.wrapUri(URI.create("s3://bucket"), null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'repositoryType' must not be null");
         assertThatThrownBy(() -> WrappedUri.wrapUri(URI.create("s3://bucket"), " "))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'repositoryType= ' characters must be in [a-z0-9_-]");
      }

      @Test
      public void wrapUriWhenInvalidUri() {
         assertThatThrownBy(() -> WrappedUri.wrapUri(URI.create("s3://bucket"), "s:"))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'repositoryType=s:' characters must be in [a-z0-9_-]");
      }

      @Test
      public void wrapUriWhenFragment() {
         assertThat(WrappedUri.wrapUri(URI.create("s3://bucket#fragment"), "s3")).satisfies(wrappedUri -> {
            assertThat(wrappedUri.wrappedUri().toString()).isEqualTo("document:s3:s3://bucket#fragment");
            assertThat(wrappedUri.repositoryType()).isEqualTo("s3");
            assertThat(wrappedUri.originalUri().toString()).isEqualTo("s3://bucket#fragment");
         });
      }

      @Test
      public void wrapUriWhenQueryWithEncodedQueryAndFragment() {
         assertThat(WrappedUri.wrapUri(URI.create(
                                             "s3://bucket?parameter1=value1&endpoint=http://endpoint%3Feparameter1=evalue1%26eparameter2=evalue2%23efragment&parameter2=value2#fragment"),
                                       "s3")).satisfies(wrappedUri -> {
            assertThat(wrappedUri.wrappedUri().toString()).isEqualTo(
                  "document:s3:s3://bucket?parameter1=value1&endpoint=http://endpoint%3Feparameter1=evalue1%26eparameter2=evalue2%23efragment&parameter2=value2#fragment");
            assertThat(wrappedUri.repositoryType()).isEqualTo("s3");
            assertThat(wrappedUri.originalUri().toString()).isEqualTo(
                  "s3://bucket?parameter1=value1&endpoint=http://endpoint%3Feparameter1=evalue1%26eparameter2=evalue2%23efragment&parameter2=value2#fragment");
         });
      }

      @Test
      public void wrapUriWhenQueryWithNotEncodedQueryAndFragment() {
         assertThat(WrappedUri.wrapUri(URI.create(
                                             "s3://bucket?parameter1=value1&endpoint=http://endpoint?eparameter1=evalue1&eparameter2=evalue2%23efragment&parameter2=value2#fragment"),
                                       "s3")).satisfies(wrappedUri -> {
            assertThat(wrappedUri.wrappedUri().toString()).isEqualTo(
                  "document:s3:s3://bucket?parameter1=value1&endpoint=http://endpoint?eparameter1=evalue1&eparameter2=evalue2%23efragment&parameter2=value2#fragment");
            assertThat(wrappedUri.repositoryType()).isEqualTo("s3");
            assertThat(wrappedUri.originalUri().toString()).isEqualTo(
                  "s3://bucket?parameter1=value1&endpoint=http://endpoint?eparameter1=evalue1&eparameter2=evalue2%23efragment&parameter2=value2#fragment");
         });
      }
   }

   @Nested
   public class ofWrappedUri {

      @Test
      public void unwrapUriWhenNominal() {
         assertThat(WrappedUri.unwrapUri(URI.create("document:s3:s3://bucket"))).hasValueSatisfying(wrappedUri -> {
            assertThat(wrappedUri.wrappedUri().toString()).isEqualTo("document:s3:s3://bucket");
            assertThat(wrappedUri.repositoryType()).isEqualTo("s3");
            assertThat(wrappedUri.originalUri().toString()).isEqualTo("s3://bucket");
         });
      }

      @Test
      public void unwrapUriWhenAlternativeScheme() {
         assertThat(WrappedUri.unwrapUri(URI.create("doc:s3:s3://bucket"))).hasValueSatisfying(wrappedUri -> {
            assertThat(wrappedUri.wrappedUri().toString()).isEqualTo("doc:s3:s3://bucket");
            assertThat(wrappedUri.repositoryType()).isEqualTo("s3");
            assertThat(wrappedUri.originalUri().toString()).isEqualTo("s3://bucket");
         });
      }

      @Test
      public void unwrapUriWhenCaseSensitiveScheme() {
         assertThat(WrappedUri.unwrapUri(URI.create("DOCUMENT:s3:s3://bucket"))).isEmpty();
      }

      @Test
      public void unwrapUriWhenCaseSensitiveRepositoryType() {
         assertThat(WrappedUri.unwrapUri(URI.create("document:S3:s3://bucket"))).hasValueSatisfying(wrappedUri -> {
            assertThat(wrappedUri.wrappedUri().toString()).isEqualTo("document:S3:s3://bucket");
            assertThat(wrappedUri.repositoryType()).isEqualTo("S3");
            assertThat(wrappedUri.originalUri().toString()).isEqualTo("s3://bucket");
         });
      }

      @Test
      public void unwrapUriWhenEmptyUri() {
         assertThat(WrappedUri.unwrapUri(URI.create("document:empty:"))).hasValueSatisfying(wrappedUri -> {
            assertThat(wrappedUri.wrappedUri().toString()).isEqualTo("document:empty:");
            assertThat(wrappedUri.repositoryType()).isEqualTo("empty");
            assertThat(wrappedUri.originalUri().toString()).isEqualTo("");
         });
      }

      @Test
      public void unwrapUriWhenBadArgument() {
         assertThatThrownBy(() -> WrappedUri.unwrapUri(null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'wrappedUri' must not be null");
      }

      @Test
      public void unwrapUriWhenRegularUri() {
         assertThat(WrappedUri.unwrapUri(URI.create("s3://bucket"))).isEmpty();
      }

      @Test
      public void unwrapUriWhenFragment() {
         assertThat(WrappedUri.unwrapUri(URI.create("document:s3:s3://bucket#fragment"))).hasValueSatisfying(
               wrappedUri -> {
                  assertThat(wrappedUri
                                   .wrappedUri()
                                   .toString()).isEqualTo("document:s3:s3://bucket#fragment");
                  assertThat(wrappedUri.repositoryType()).isEqualTo("s3");
                  assertThat(wrappedUri.originalUri().toString()).isEqualTo("s3://bucket#fragment");
               });
      }

      @Test
      public void unwrapUriWhenInvalidUri() {
         assertThatThrownBy(() -> WrappedUri.unwrapUri(URI.create("document:wrapped-uri")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'document:wrapped-uri' URI: Invalid wrapped URI format, must be 'document:<repository-type>:<original-uri>'");
         assertThatThrownBy(() -> WrappedUri.unwrapUri(URI.create("document:s3:user@bucket:path")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'document:s3:user@bucket:path' URI: Wrapped 'user@bucket:path' URI is not a valid URI");
      }

      @Test
      public void unwrapUriWhenQueryWithEncodedParameterAndFragment() {
         assertThat(WrappedUri.unwrapUri(URI.create(
               "document:s3:s3://bucket?parameter1=value1&endpoint=http://endpoint%3Feparameter1=evalue1%26eparameter2=evalue2%23efragment&parameter2=value2#fragment"))).hasValueSatisfying(
               wrappedUri -> {
                  assertThat(wrappedUri.wrappedUri().toString()).isEqualTo(
                        "document:s3:s3://bucket?parameter1=value1&endpoint=http://endpoint%3Feparameter1=evalue1%26eparameter2=evalue2%23efragment&parameter2=value2#fragment");
                  assertThat(wrappedUri.repositoryType()).isEqualTo("s3");
                  assertThat(wrappedUri.originalUri().toString()).isEqualTo(
                        "s3://bucket?parameter1=value1&endpoint=http://endpoint%3Feparameter1=evalue1%26eparameter2=evalue2%23efragment&parameter2=value2#fragment");
               });
      }

      @Test
      public void unwrapUriWhenQueryWithNotEncodedParameterAndFragment() {
         assertThat(WrappedUri.unwrapUri(URI.create(
               "document:s3:s3://bucket?parameter1=value1&endpoint=http://endpoint?eparameter1=evalue1&eparameter2=evalue2%23efragment&parameter2=value2#fragment"))).hasValueSatisfying(
               wrappedUri -> {
                  assertThat(wrappedUri.wrappedUri().toString()).isEqualTo(
                        "document:s3:s3://bucket?parameter1=value1&endpoint=http://endpoint?eparameter1=evalue1&eparameter2=evalue2%23efragment&parameter2=value2#fragment");
                  assertThat(wrappedUri.repositoryType()).isEqualTo("s3");
                  assertThat(wrappedUri.originalUri().toString()).isEqualTo(
                        "s3://bucket?parameter1=value1&endpoint=http://endpoint?eparameter1=evalue1&eparameter2=evalue2%23efragment&parameter2=value2#fragment");
               });
      }

   }

}