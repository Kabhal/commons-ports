/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.uri;

public class ParameterizedQueryTest {

   // FIXME

   /*
   @Test
   public void testParameterizedUriWhenNominal() {
      Parameter<String> p1 = Parameter.registered("p1", String.class);

      ParameterizedUri simple = ParameterizedUri.of(uri("scheme://host/path?p1=v1")).registerParameter(p1);

      assertThat(simple.path()).hasValue(Path.of("/path"));
      assertThat(simple.relativePath()).hasValue(Path.of("path"));
      assertThat(simple.noAuthorityPath()).hasValue(Path.of("host/path"));
      assertThat(simple.parameter(p1, alwaysFalse())).hasValue("v1");
   }

   @Test
   public void testParameterizedUriWhenNoArgQuery() {
      Parameter<String> p1 = Parameter.registered("p1", String.class);

      ParameterizedUri simple = ParameterizedUri.of(uri("scheme://host/path?p1")).registerParameter(p1);

      assertThat(simple.parameter(p1, alwaysFalse())).hasValue("true");
      assertThat(simple.parameter("p1", Boolean.class, alwaysFalse())).hasValue(true);
   }

   @Test
   public void testParameterizedUriWhenEmptyArgQuery() {
      Parameter<String> p1 = Parameter.registered("p1", String.class);

      ParameterizedUri simple = ParameterizedUri.of(uri("scheme://host/path?p1=")).registerParameter(p1);

      assertThat(simple.parameter(p1, alwaysFalse())).hasValue("");
   }

   @Test
   public void testPathWhenAuthority() {
      ParameterizedUri simple = ParameterizedUri.of(uri("scheme://host/path"));

      assertThat(simple.path()).hasValue(Path.of("/path"));
      assertThat(simple.relativePath()).hasValue(Path.of("path"));
      assertThat(simple.noAuthorityPath()).hasValue(Path.of("host/path"));

      ParameterizedUri subpath = ParameterizedUri.of(uri("scheme://host/path/subpath"));

      assertThat(subpath.path()).hasValue(Path.of("/path/subpath"));
      assertThat(subpath.relativePath()).hasValue(Path.of("path/subpath"));
      assertThat(subpath.noAuthorityPath()).hasValue(Path.of("host/path/subpath"));
   }

   @Test
   public void testPathWhenOnlyRootSlash() {
      ParameterizedUri simple = ParameterizedUri.of(uri("scheme://host/"));

      assertThat(simple.path()).hasValue(Path.of("/"));
      assertThat(simple.relativePath()).isEmpty();
      assertThat(simple.noAuthorityPath()).hasValue(Path.of("host"));
   }

   @Test
   public void testPathWhenNoAuthorityAndRelative() {
      ParameterizedUri simple = ParameterizedUri.of(uri("scheme://path"));

      assertThat(simple.path()).isEmpty();
      assertThat(simple.relativePath()).isEmpty();
      assertThat(simple.noAuthorityPath()).hasValue(Path.of("path"));

      ParameterizedUri subpath = ParameterizedUri.of(uri("scheme://path/subpath"));

      assertThat(subpath.path()).hasValue(Path.of("/subpath"));
      assertThat(subpath.relativePath()).hasValue(Path.of("subpath"));
      assertThat(subpath.noAuthorityPath()).hasValue(Path.of("path/subpath"));
   }

   @Test
   public void testPathWhenNoAuthorityAndAbsolute() {
      ParameterizedUri simple = ParameterizedUri.of(uri("scheme:///path"));

      assertThat(simple.path()).hasValue(Path.of("/path"));
      assertThat(simple.relativePath()).hasValue(Path.of("/path"));
      assertThat(simple.noAuthorityPath()).hasValue(Path.of("/path"));

      ParameterizedUri subpath = ParameterizedUri.of(uri("scheme:///path/subpath"));

      assertThat(subpath.path()).hasValue(Path.of("/path/subpath"));
      assertThat(subpath.relativePath()).hasValue(Path.of("/path/subpath"));
      assertThat(subpath.noAuthorityPath()).hasValue(Path.of("/path/subpath"));
   }

   @Test
   public void testPathWhenNoAuthorityAndOnlyRootSlash() {
      ParameterizedUri simple = ParameterizedUri.of(uri("scheme:///"));

      assertThat(simple.path()).hasValue(Path.of("/"));
      assertThat(simple.relativePath()).hasValue(Path.of("/"));
      assertThat(simple.noAuthorityPath()).hasValue(Path.of("/"));
   }

   @Test
   public void testPathWhenNoPath() {
      ParameterizedUri parameters = ParameterizedUri.of(uri("scheme://host"));

      assertThat(parameters.path()).isEmpty();
      assertThat(parameters.relativePath()).isEmpty();
      assertThat(parameters.noAuthorityPath()).hasValue(Path.of("host"));
   }

   @Test
   public void testPathWhenDoubleSlash() {
      ParameterizedUri simple = ParameterizedUri.of(uri("scheme://host//path"));

      assertThat(simple.path()).hasValue(Path.of("/path"));
      assertThat(simple.relativePath()).hasValue(Path.of("/path"));
      assertThat(simple.noAuthorityPath()).hasValue(Path.of("host/path"));

      ParameterizedUri subpath = ParameterizedUri.of(uri("scheme://host//path//subpath"));

      assertThat(subpath.path()).hasValue(Path.of("/path/subpath"));
      assertThat(subpath.relativePath()).hasValue(Path.of("/path/subpath"));
      assertThat(subpath.noAuthorityPath()).hasValue(Path.of("host/path/subpath"));
   }

   @Test
   public void testPathWhenOnlyRootDoubleSlash() {
      ParameterizedUri simple = ParameterizedUri.of(uri("scheme://host//"));

      assertThat(simple.path()).hasValue(Path.of("/"));
      assertThat(simple.relativePath()).hasValue(Path.of("/"));
      assertThat(simple.noAuthorityPath()).hasValue(Path.of("host"));
   }

   @Test
   public void testIncludeParametersWhenNominal() {
      Parameter<String> p1 = Parameter.registered("p1", String.class);
      ParameterizedUri reference = parameterizedUri("scheme://host?p1=v1&p2=v2", p1);

      assertThat(reference.includeParameters(parameterizedUri("scheme://host?p1=v1&p2=v2", p1),
                                             isAlwaysTrue())).isTrue();
   }

   @Test
   public void testIncludeParametersWhenEmptyValues() {
      Parameter<String> p1 = Parameter.registered("p1", String.class);
      ParameterizedUri reference = parameterizedUri("scheme://host", p1);

      assertThat(reference.includeParameters(parameterizedUri("scheme://host", p1), isAlwaysTrue())).isTrue();
   }

   @Test
   public void testIncludeParametersWhenNotEqualValue() {
      Parameter<String> p1 = Parameter.registered("p1", String.class);
      ParameterizedUri reference = parameterizedUri("scheme://host?p1=v1&p2=v2", p1);

      assertThat(reference.includeParameters(parameterizedUri("scheme://host?p1=o1&p2=v2", p1),
                                             isAlwaysTrue())).isFalse();
   }

   @Test
   public void testIncludeParametersWhenEqualMultiValue() {
      Parameter<String> p1 = Parameter.registered("p1", String.class);
      ParameterizedUri reference = parameterizedUri("scheme://host?p1=v1a&p1=v1b", p1);

      assertThat(reference.includeParameters(parameterizedUri("scheme://host?p1=v1a&p1=v1b", p1),
                                             isAlwaysTrue())).isTrue();
   }

   @Test
   public void testIncludeParametersWhenNotEqualMultiValue() {
      Parameter<String> p1 = Parameter.registered("p1", String.class);
      ParameterizedUri reference = parameterizedUri("scheme://host?p1=v1a&p1=v1b", p1);

      assertThat(reference.includeParameters(parameterizedUri("scheme://host?p1=v1a&p1=o1b", p1),
                                             isAlwaysTrue())).isFalse();
      assertThat(reference.includeParameters(parameterizedUri("scheme://host?p1=v1a", p1),
                                             isAlwaysTrue())).isFalse();
      assertThat(reference.includeParameters(parameterizedUri("scheme://host", p1), isAlwaysTrue())).isTrue();
   }

   @Test
   public void testIncludeParametersWhenMissingRegisteredParameter() {
      Parameter<String> p1 = Parameter.registered("p1", String.class);

      assertThat(parameterizedUri("scheme://host?p1=v1&p2=v2", p1).includeParameters(parameterizedUri(
            "scheme://host?p2=v2",
            p1), isAlwaysTrue())).isTrue();

      assertThat(parameterizedUri("scheme://host?p2=v2", p1).includeParameters(parameterizedUri(
            "scheme://host?p2=v2",
            p1), isAlwaysTrue())).isTrue();
   }

   @Test
   public void testIncludeParametersWhenMissingRegisteredParameterAndRegisteredParameterHasDefault() {
      Parameter<String> p1 = Parameter.registered("p1", String.class).defaultValue("default1");

      assertThat(parameterizedUri("scheme://host?p1=v1&p2=v2", p1).includeParameters(parameterizedUri(
            "scheme://host?p2=v2",
            p1), isAlwaysTrue())).isTrue();

      assertThat(parameterizedUri("scheme://host?p2=v2", p1).includeParameters(parameterizedUri(
            "scheme://host?p2=v2",
            p1), isAlwaysTrue())).isTrue();

      assertThat(parameterizedUri("scheme://host?p2=v2", p1).includeParameters(parameterizedUri(
            "scheme://host?p1=v1&p2=v2",
            p1), isAlwaysTrue())).isFalse();

      assertThat(parameterizedUri("scheme://host?p2=v2", p1).includeParameters(parameterizedUri(
            "scheme://host?p1=default1&p2=v2",
            p1), isAlwaysTrue())).isTrue();
   }

   @Test
   public void testIncludeParametersWhenAdditionalRegisteredParameter() {
      Parameter<String> p1 = Parameter.registered("p1", String.class);
      ParameterizedUri reference = parameterizedUri("scheme://host?p1=v1&p2=v2", p1);

      assertThat(reference.includeParameters(parameterizedUri("scheme://host?o1=v1&p2=v2",
                                                              Parameter.registered("o1", String.class)),
                                             isAlwaysTrue())).isFalse();
   }

   @Test
   public void testIncludeParametersWhenAdditionalUnregisteredParameter() {
      Parameter<String> p1 = Parameter.registered("p1", String.class);
      ParameterizedUri reference = parameterizedUri("scheme://host?p1=v1", p1);

      assertThat(reference.includeParameters(parameterizedUri("scheme://host?p1=v1&p2=v2", p1),
                                             isAlwaysTrue())).isFalse();
   }

   @Test
   public void testIncludeParametersWhenFilteredParameter() {
      Parameter<String> p1 = Parameter.registered("p1", String.class);
      ParameterizedUri reference = parameterizedUri("scheme://host?p1=v1", p1);

      assertThat(reference.includeParameters(parameterizedUri("scheme://host?p1=v1&p2=v2", p1),
                                             isRegisteredParameter())).isTrue();
   }

   @Test
   public void testIncludeParametersWhenMismatchingUri() {
      Parameter<String> p1 = Parameter.registered("p1", String.class);
      ParameterizedUri reference = parameterizedUri("scheme://host?p1=v1", p1);

      assertThat(reference.includeParameters(parameterizedUri("other://other?p1=v1", p1),
                                             isRegisteredParameter())).isTrue();
   }

   @Nested
   public class ExportUri {

      @Test
      public void exportUriWhenNominal() {
         Parameter<String> p1 = Parameter.registered("p1", String.class);

         ParameterizedUri uri = ParameterizedUri.of(uri("scheme://host/path?p1=v1")).registerParameter(p1);

         assertThat(uri.exportUri().toString()).isEqualTo("scheme://host/path?p1=v1");
      }

      @Test
      public void exportUriWhenQueryStringAndFragment() {
         ParameterizedUri uri = ParameterizedUri.of(uri("scheme://host/path?q1=v1&q2=v2#fragment"));

         assertThat(uri.exportUri().toString()).isEqualTo("scheme://host/path?q1=v1&q2=v2#fragment");
      }

      @Test
      public void exportUriWhenUrlEncodedQuery() {
         Parameter<String> p1 = Parameter.registered("p1", String.class);

         ParameterizedUri uri = ParameterizedUri
               .of(uri("scheme://host/path?p1=url://path/value?q1=v1&p2=v2#fragment"))
               .registerParameter(p1);

         assertThat(uri.parameter(p1, alwaysFalse())).hasValue("url://path/value?q1=v1");
         assertThat(uri.exportUri().toString()).isEqualTo(
               "scheme://host/path?p1=url://path/value?q1=v1&p2=v2#fragment");
      }

      @Test
      public void exportUriWhenPreUrlEncodedQuery() {
         Parameter<String> p1 = Parameter.registered("p1", String.class);

         ParameterizedUri uri = ParameterizedUri
               .of(uri("scheme://host/path?p1=url%3A%2F%2Fpath%2Fvalue%3Fq1%3Dv1%23fragment0&p2=v2#fragment"))
               .registerParameter(p1);

         assertThat(uri.parameter(p1, alwaysFalse())).hasValue("url://path/value?q1=v1#fragment0");
         assertThat(uri.exportUri().toString()).isEqualTo(
               "scheme://host/path?p1=url://path/value?q1=v1%23fragment0&p2=v2#fragment");
      }

      @Test
      public void exportUriWhenNoParameterRegistered() {
         ParameterizedUri uri = ParameterizedUri.of(uri("scheme://host/path?p1=v1"));

         assertThat(uri.exportUri().toString()).isEqualTo("scheme://host/path?p1=v1");
      }

      @Test
      public void exportUriWhenParameterRegisteredWithNoDefault() {
         Parameter<String> p1 = Parameter.registered("p1", String.class);

         ParameterizedUri uri =
               ParameterizedUri.of(uri("scheme://host/path?p1=v1&p2=v2")).registerParameter(p1);
         assertThat(uri.exportUri().toString()).isEqualTo("scheme://host/path?p1=v1&p2=v2");

         ParameterizedUri uriWithoutParameter =
               ParameterizedUri.of(uri("scheme://host/path?p2=v2")).registerParameter(p1);
         assertThat(uriWithoutParameter.exportUri().toString()).isEqualTo("scheme://host/path?p2=v2");
      }

      @Test
      public void exportUriWhenParameterRegisteredWithDefault() {
         Parameter<String> p1 = Parameter.registered("p1", String.class).defaultValue("default1");

         ParameterizedUri uri =
               ParameterizedUri.of(uri("scheme://host/path?p1=v1&p2=v2")).registerParameter(p1);
         assertThat(uri.exportUri().toString()).isEqualTo("scheme://host/path?p1=v1&p2=v2");

         ParameterizedUri uriWithoutParameter =
               ParameterizedUri.of(uri("scheme://host/path?p2=v2")).registerParameter(p1);
         assertThat(uriWithoutParameter.exportUri().toString()).isEqualTo(
               "scheme://host/path?p2=v2&p1=default1");
      }

      @Test
      public void exportUriWhenParameterRegisteredWithMultiValuedDefault() {
         Parameter<String> p1 =
               Parameter.registered("p1", String.class).defaultValue(list("defaultA", "defaultB"));

         ParameterizedUri uri = ParameterizedUri.of(uri("scheme://host/path")).registerParameter(p1);
         assertThat(uri.exportUri().toString()).isEqualTo("scheme://host/path?p1=defaultA&p1=defaultB");

         ParameterizedUri uriWithQuery =
               ParameterizedUri.of(uri("scheme://host/path?p1=v1")).registerParameter(p1);
         assertThat(uriWithQuery.exportUri().toString()).isEqualTo("scheme://host/path?p1=v1");
      }

      @Test
      public void isParameterDefaultValueFilter() {
         Parameter<String> p1 = Parameter.registered("p1", String.class).defaultValue("default1");
         Parameter<String> p2 = Parameter.registered("p2", String.class);
         ValueParameter<String> p3 = ValueParameter.ofValue(Parameter.registered("p3", String.class), "v3");
         ValueParameter<String> p4 =
               ValueParameter.ofValue(Parameter.registered("p4", String.class).defaultValue("default4"),
                                      "v4");
         Parameter<String> p5 = Parameter.registered("p5", String.class);
         Parameter<String> p6 = Parameter.registered("p6", String.class).defaultValue("default6");

         ParameterizedUri uriWithNoQuery =
               ParameterizedUri.of(uri("scheme://host/path")).registerParameters(p1, p2, p3, p4, p5, p6);

         assertThat(uriWithNoQuery.exportUri().toString()).isEqualTo(
               "scheme://host/path?p1=default1&p3=v3&p4=v4&p6=default6");
         assertThat(uriWithNoQuery.exportUri(isParameterDefaultValue()).toString()).isEqualTo(
               "scheme://host/path?p1=default1&p6=default6");
         assertThat(uriWithNoQuery.exportUri(isParameterDefaultValue().negate()).toString()).isEqualTo(
               "scheme://host/path?p3=v3&p4=v4");

         ParameterizedUri uriWithQuery = ParameterizedUri
               .of(uri("scheme://host/path?p1=v1&p2=v2&p3=v3&p4=v4"))
               .registerParameters(p1, p2, p3, p4, p5, p6);

         assertThat(uriWithQuery.exportUri().toString()).isEqualTo(
               "scheme://host/path?p1=v1&p2=v2&p3=v3&p4=v4&p6=default6");
         assertThat(uriWithQuery.exportUri(isParameterDefaultValue()).toString()).isEqualTo(
               "scheme://host/path?p6=default6");
         assertThat(uriWithQuery.exportUri(isParameterDefaultValue().negate()).toString()).isEqualTo(
               "scheme://host/path?p1=v1&p2=v2&p3=v3&p4=v4");

         ParameterizedUri uriWithDefaultQuery = ParameterizedUri
               .of(uri("scheme://host/path?p1=default1&p2=v2&p3=v3&p4=v4"))
               .registerParameters(p1, p2, p3, p4, p5, p6);

         assertThat(uriWithDefaultQuery.exportUri().toString()).isEqualTo(
               "scheme://host/path?p1=default1&p2=v2&p3=v3&p4=v4&p6=default6");
         assertThat(uriWithDefaultQuery.exportUri(isParameterDefaultValue()).toString()).isEqualTo(
               "scheme://host/path?p1=default1&p6=default6");
         assertThat(uriWithDefaultQuery.exportUri(isParameterDefaultValue().negate()).toString()).isEqualTo(
               "scheme://host/path?p2=v2&p3=v3&p4=v4");
      }

   }

   private static ParameterizedUri parameterizedUri(String uri, Parameter<String> p) {
      return ParameterizedUri.of(uri(uri)).registerParameter(p);
   }
*/
}