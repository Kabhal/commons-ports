/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.typed;

import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.mimeType;
import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.parseMimeType;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_PDF;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.TEXT_PLAIN;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.regex.Pattern;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.lang.datetime.ApplicationClock;
import com.tinubu.commons.ports.document.domain.DocumentPath;

public class NoopDocumentTest {

   public static final Pattern UUID_PATTERN =
         Pattern.compile("[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}");
   public static final Pattern UUID_PATTERN_WITH_EXT =
         Pattern.compile("[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}\\.[a-z]{3}");

   private static final ZonedDateTime FIXED_DATE =
         ZonedDateTime.of(LocalDateTime.of(2021, 11, 10, 14, 0, 0), ZoneId.of("UTC"));

   @BeforeEach
   public void initializeApplicationClock() {
      ApplicationClock.setFixedClock(FIXED_DATE);
   }

   @Test
   public void noopWhenNominal() {
      NoopDocument noop = NoopDocument.noop();

      assertThat(noop.documentId().stringValue()).matches(UUID_PATTERN);
      assertThat(noop.metadata().documentPath().toString()).matches(UUID_PATTERN);
      assertThat(noop.metadata().documentName()).matches(UUID_PATTERN);
      assertThat(noop.metadata().contentType()).isEmpty();
      assertThat(noop.metadata().contentEncoding()).isEmpty();
      assertThat(noop.metadata().contentSize()).hasValue(0L);
      assertThat(noop.metadata().creationDate()).hasValue(FIXED_DATE.toInstant());
      assertThat(noop.metadata().lastUpdateDate()).hasValue(FIXED_DATE.toInstant());
      assertThat(noop.content().contentEncoding()).isEmpty();
      assertThat(noop.content().contentSize()).hasValue(0L);
   }

   @Test
   public void noopWhenContentTypeWithCharset() {
      NoopDocument noop = NoopDocument.noop(mimeType(APPLICATION_PDF, UTF_8));

      assertThat(noop.documentId().stringValue()).matches(UUID_PATTERN_WITH_EXT);
      assertThat(noop.metadata().documentPath().toString()).matches(UUID_PATTERN_WITH_EXT);
      assertThat(noop.metadata().documentName()).matches(UUID_PATTERN_WITH_EXT);
      assertThat(noop.metadata().contentType()).hasValue(parseMimeType("application/pdf;charset=UTF-8"));
      assertThat(noop.metadata().contentEncoding()).hasValue(UTF_8);
      assertThat(noop.metadata().contentSize()).hasValue(0L);
      assertThat(noop.metadata().creationDate()).hasValue(FIXED_DATE.toInstant());
      assertThat(noop.metadata().lastUpdateDate()).hasValue(FIXED_DATE.toInstant());
      assertThat(noop.content().contentEncoding()).hasValue(UTF_8);
      assertThat(noop.content().contentSize()).hasValue(0L);
   }

   @Test
   public void noopWhenContentTypeWithoutCharset() {
      NoopDocument noop = NoopDocument.noop(APPLICATION_PDF);

      assertThat(noop.documentId().stringValue()).matches(UUID_PATTERN_WITH_EXT);
      assertThat(noop.metadata().documentPath().toString()).matches(UUID_PATTERN_WITH_EXT);
      assertThat(noop.metadata().documentName()).matches(UUID_PATTERN_WITH_EXT);
      assertThat(noop.metadata().contentType()).hasValue(parseMimeType("application/pdf"));
      assertThat(noop.metadata().contentEncoding()).isEmpty();
      assertThat(noop.metadata().contentSize()).hasValue(0L);
      assertThat(noop.metadata().creationDate()).hasValue(FIXED_DATE.toInstant());
      assertThat(noop.metadata().lastUpdateDate()).hasValue(FIXED_DATE.toInstant());
      assertThat(noop.content().contentEncoding()).isEmpty();
      assertThat(noop.content().contentSize()).hasValue(0L);
   }

   @Test
   public void noopWhenDocumentIdAndContentType() {
      NoopDocument noop = NoopDocument.noop(DocumentPath.of("test.txt"), mimeType(APPLICATION_PDF, UTF_8));

      assertThat(noop.documentId().stringValue()).isEqualTo("test.txt");
      assertThat(noop.metadata().documentPath().toString()).isEqualTo("test.txt");
      assertThat(noop.metadata().documentName()).isEqualTo("test.txt");
      assertThat(noop.metadata().contentType()).hasValue(parseMimeType("application/pdf;charset=UTF-8"));
      assertThat(noop.metadata().contentEncoding()).hasValue(UTF_8);
      assertThat(noop.metadata().contentSize()).hasValue(0L);
      assertThat(noop.metadata().creationDate()).hasValue(FIXED_DATE.toInstant());
      assertThat(noop.metadata().lastUpdateDate()).hasValue(FIXED_DATE.toInstant());
      assertThat(noop.content().contentEncoding()).hasValue(UTF_8);
      assertThat(noop.content().contentSize()).hasValue(0L);
   }

   @Test
   public void noopWhenDocumentId() {
      NoopDocument noop = NoopDocument.noop(DocumentPath.of("test.txt"));

      assertThat(noop.documentId().stringValue()).isEqualTo("test.txt");
      assertThat(noop.metadata().documentPath().toString()).isEqualTo("test.txt");
      assertThat(noop.metadata().documentName()).isEqualTo("test.txt");
      assertThat(noop.metadata().contentType()).hasValue(TEXT_PLAIN);
      assertThat(noop.metadata().contentEncoding()).isEmpty();
      assertThat(noop.metadata().contentSize()).hasValue(0L);
      assertThat(noop.metadata().creationDate()).hasValue(FIXED_DATE.toInstant());
      assertThat(noop.metadata().lastUpdateDate()).hasValue(FIXED_DATE.toInstant());
      assertThat(noop.content().contentEncoding()).isEmpty();
      assertThat(noop.content().contentSize()).hasValue(0L);
   }

}