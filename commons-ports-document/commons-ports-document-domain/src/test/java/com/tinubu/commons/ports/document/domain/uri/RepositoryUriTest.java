/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.uri;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.net.URI;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;

public class RepositoryUriTest {

   @Test
   public void ofUriWhenNominal() {
      assertThat(RepositoryUri.ofRepositoryUri(URI.create("s3://bucket"))).satisfies(repository -> {
         assertThat(repository.stringValue()).isEqualTo("s3://bucket");
      });
   }

   @Test
   public void ofUriWhenBadParameters() {
      assertThatThrownBy(() -> RepositoryUri.ofRepositoryUri((URI) null))
            .isInstanceOf(InvariantValidationException.class)
            .hasMessage("Invariant validation error > 'uri' must not be null");
      assertThatThrownBy(() -> RepositoryUri.ofRepositoryUri((String) null))
            .isInstanceOf(InvariantValidationException.class)
            .hasMessage("Invariant validation error > 'uri' must not be null");
   }

   @Test
   public void ofUriWhenNotNormalized() {
      assertThat(RepositoryUri.ofRepositoryUri(URI.create("scheme://host//path/"))).satisfies(repository -> {
         assertThat(repository.stringValue()).isEqualTo("scheme://host//path/");
      });
      assertThat(RepositoryUri.ofRepositoryUri("scheme://host//path/")).satisfies(repository -> {
         assertThat(repository.stringValue()).isEqualTo("scheme://host//path/");
      });
      assertThat(RepositoryUri.ofRepositoryUri(URI.create("scheme://host/path/../."))).satisfies(repository -> {
         assertThat(repository.stringValue()).isEqualTo("scheme://host/path/../.");
      });
      assertThat(RepositoryUri.ofRepositoryUri("scheme://host/path/../.")).satisfies(repository -> {
         assertThat(repository.stringValue()).isEqualTo("scheme://host/path/../.");
      });
      assertThat(RepositoryUri.ofRepositoryUri(URI.create("scheme://host/../path"))).satisfies(repository -> {
         assertThat(repository.stringValue()).isEqualTo("scheme://host/../path");
      });
      assertThat(RepositoryUri.ofRepositoryUri("scheme://host/../path")).satisfies(repository -> {
         assertThat(repository.stringValue()).isEqualTo("scheme://host/../path");
      });
   }

}
