/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.repository;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObject;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotAbsolute;
import static com.tinubu.commons.ddd2.invariant.rules.PredicateRules.satisfies;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.OptionalUtils.optionalPredicate;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.METADATA_CONTENT_ENCODING;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.METADATA_CONTENT_SIZE;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.time.StopWatch;

import com.tinubu.commons.ddd2.domain.event.RegistrableDomainEventService;
import com.tinubu.commons.ddd2.domain.event.SynchronousDomainEventService;
import com.tinubu.commons.ddd2.domain.repository.Repository;
import com.tinubu.commons.ddd2.domain.specification.Specification;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.lang.datetime.ApplicationClock;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.lang.validation.CheckReturnValue;
import com.tinubu.commons.ports.document.domain.AbstractDocumentRepository;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentContent;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentMetadata;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent.InputStreamDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.LoadedDocumentContent;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata;
import com.tinubu.commons.ports.document.domain.OutputStreamDocumentContent;
import com.tinubu.commons.ports.document.domain.ReferencedDocument;
import com.tinubu.commons.ports.document.domain.capability.RepositoryCapability;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.ExportUriOptions;
import com.tinubu.commons.ports.document.domain.uri.RepositoryUri;

/**
 * In-memory {@link DocumentRepository} adapter implementation for testing purpose.
 *
 * @implSpec Immutable class implementation
 */
public class TestDocumentRepository extends AbstractDocumentRepository<TestDocumentRepository> {
   private static final String URI_SCHEME = "test";

   private static final String DEFAULT_INSTANCE = "default";
   private static final Map<String, TestDocumentRepository> sharedInstances = new ConcurrentHashMap<>();

   private final Map<DocumentPath, Document> documentStorage;

   private final boolean caseInsensitive;

   private TestDocumentRepository(Map<DocumentPath, Document> documentStorage,
                                  boolean caseInsensitive,
                                  RegistrableDomainEventService eventService) {
      super(eventService);

      this.documentStorage = nullable(documentStorage, new ConcurrentHashMap<>());
      this.caseInsensitive = caseInsensitive;
   }

   public TestDocumentRepository(boolean caseInsensitive, RegistrableDomainEventService eventService) {
      this(null, caseInsensitive, eventService);
   }

   public TestDocumentRepository(boolean caseInsensitive) {
      this(caseInsensitive, new SynchronousDomainEventService());
   }

   public TestDocumentRepository() {
      this(false);
   }

   public static TestDocumentRepository sharedInstance(String name) {
      return sharedInstances.computeIfAbsent(name, __ -> new TestDocumentRepository());
   }

   /**
    * Returns an instance from document repository URI information, only if URI is
    * compatible with this repository.
    *
    * @param uri repository URI, without document
    *
    * @return pre-configured repository or {@link Optional#empty} if URI is not compatible with this repository.
    */
   public static TestDocumentRepository ofUri(RepositoryUri uri) {
      validate(uri, "uri", isNotNull()).orThrow();

      return optional(uri)
            .filter(u -> isCompatibleUri(u, false))
            .map(u -> sharedInstance(DEFAULT_INSTANCE))
            .orElseThrow(() -> new IncompatibleUriException(uri));
   }

   @Override
   public boolean sameRepositoryAs(Repository<Document, DocumentPath> documentRepository) {
      validate(documentRepository, "documentRepository", isNotNull()).orThrow();

      return Objects.equals(this, documentRepository);
   }

   @Override
   public TestDocumentRepository subPath(Path subPath, boolean shareContext) {
      validate(subPath, "subPath", isNotAbsolute().andValue(hasNoTraversal())).orThrow();

      Map<DocumentPath, Document> subPathDocumentStorage = documentStorage
            .values()
            .stream()
            .flatMap(document -> stream(document
                                              .documentId()
                                              .subPath(subPath)
                                              .map(documentId -> DocumentBuilder
                                                    .from(document)
                                                    .documentId(documentId)
                                                    .build())))
            .collect(toMap(Document::documentId, identity()));

      return new TestDocumentRepository(subPathDocumentStorage, caseInsensitive, eventService);
   }

   @Override
   @CheckReturnValue
   public Optional<Document> openDocument(DocumentPath documentId,
                                          boolean overwrite,
                                          boolean append,
                                          OpenDocumentMetadata metadata) {

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEvent(() -> {
         if (!overwrite && !append && documentStorage.containsKey(documentId)) {
            return optional();
         }

         Document documentToSave = new DocumentBuilder()
               .documentId(documentId)
               .content(MemoryOutputStreamDocumentContent.create(metadata.contentEncoding().orElse(null)))
               .chain(metadata.chainDocumentBuilder())
               .build();

         if (append && documentStorage.containsKey(documentId)) {
            try {
               IOUtils.copy(documentStorage.get(documentId).readableContent().content().inputStreamContent(),
                            documentToSave.content().outputStreamContent());
            } catch (IOException e) {
               throw new DocumentAccessException(e);
            }
         }

         Document savedDocument = documentStorage.merge(adaptCase(documentId),
                                                        documentToSave,
                                                        (pv, nv) -> overwrite || append
                                                                    ? documentToSave
                                                                    : pv);

         return optionalPredicate(savedDocument, sd -> sd == documentToSave);
      }, d -> documentSaved(d.documentEntry(), watch));
   }

   @Override
   public Optional<Document> findDocumentById(DocumentPath documentId) {
      validate(documentId, "documentId", isNotNull()).orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEvent(() -> {
         return nullable(documentStorage.get(adaptCase(documentId))).map(d -> {
            DocumentContent adaptedContent = adaptContent(d.content());
            DocumentMetadata adaptedMetadata = DocumentMetadataBuilder
                  .from(d.metadata())
                  .contentSize(adaptedContent.contentSize().orElse(null))
                  .build();

            return DocumentBuilder
                  .from(d)
                  .<DocumentBuilder>conditionalChain(__ -> caseInsensitive, b -> b.documentId(documentId))
                  .metadata(adaptedMetadata)
                  .content(adaptedContent)
                  .build();
         });
      }, d -> documentAccessed(d.documentEntry(), watch));
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryById(DocumentPath documentId) {
      validate(documentId, "documentId", isNotNull()).orThrow();

      return findDocumentById(documentId).map(Document::documentEntry);
   }

   @Override
   public Stream<Document> findDocumentsBySpecification(Path basePath,
                                                        Specification<DocumentEntry> specification) {
      validate(basePath, "basePath", isNotAbsolute().andValue(hasNoTraversal()))
            .and(validate(specification, "specification", isNotNull()))
            .orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentStreamEvent(() -> stream(documentStorage.values())
                                             .filter(d -> specification.satisfiedBy(d.documentEntry()))
                                             .map(d -> DocumentBuilder.from(d).content(adaptContent(d.content())).build()),
                                       d -> documentAccessed(d.documentEntry(), watch));
   }

   @Override
   public Stream<DocumentEntry> findDocumentEntriesBySpecification(Path basePath,
                                                                   Specification<DocumentEntry> specification) {
      validate(basePath, "basePath", isNotAbsolute().andValue(hasNoTraversal()))
            .and(validate(specification, "specification", isNotNull()))
            .orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEntryStreamEvent(() -> stream(documentStorage.values())
            .map(Document::documentEntry)
            .filter(specification::satisfiedBy), d -> documentAccessed(d, watch));
   }

   @Override
   @CheckReturnValue
   public Optional<DocumentEntry> saveDocument(Document document, boolean overwrite) {
      validate(document, "document", isNotNull()).orThrow();

      if (document.content() instanceof OutputStreamDocumentContent) {
         throw new IllegalArgumentException(
               "Unsupported operation for this document content implementation : " + document
                     .content()
                     .getClass()
                     .getName());
      }

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEntryEvent(() -> rawSaveDocument(document, overwrite).map(Document::documentEntry),
                                      d -> documentSaved(d, watch));
   }

   @Override
   @CheckReturnValue
   public Optional<Document> saveAndReturnDocument(Document document, boolean overwrite) {
      validate(document, "document", isNotNull()).orThrow();

      if (document.content() instanceof OutputStreamDocumentContent) {
         throw new IllegalArgumentException(
               "Unsupported operation for this document content implementation : " + document
                     .content()
                     .getClass()
                     .getName());
      }

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEvent(() -> rawSaveDocument(document, overwrite),
                                 d -> documentSaved(d.documentEntry(), watch));
   }

   private Optional<Document> rawSaveDocument(Document document, boolean overwrite) {
      Document documentToSave = simulateSave(document);
      Document savedDocument = documentStorage.merge(adaptCase(document.documentId()),
                                                     documentToSave,
                                                     (pv, nv) -> overwrite ? documentToSave : pv);

      return optionalPredicate(savedDocument, sd -> sd == documentToSave);
   }

   @Override
   @CheckReturnValue
   public Optional<DocumentEntry> deleteDocumentById(DocumentPath documentId) {
      validate(documentId, "documentId", isNotNull()).orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEntryEvent(() -> nullable(documentStorage.remove(adaptCase(documentId))).map(
            Document::documentEntry), d -> documentDeleted(d, watch));
   }

   @Override
   @CheckReturnValue
   public List<DocumentEntry> deleteDocumentsBySpecification(Path basePath,
                                                             Specification<DocumentEntry> specification) {
      validate(basePath, "basePath", isNotAbsolute().andValue(hasNoTraversal()))
            .and(validate(specification, "specification", isNotNull()))
            .orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEntriesEvent(() -> list(stream(documentStorage.values())
                                                         .map(Document::documentEntry)
                                                         .filter(specification::satisfiedBy)
                                                         .filter(documentEntry -> documentStorage.remove(
                                                               adaptCase(documentEntry.documentId()))
                                                                                  != null)),
                                        d -> documentDeleted(d, watch));
   }

   /**
    * Checks if specified URI is compatible with this repository, independently to current repository
    * configuration
    *
    * @param uri URI
    * @param documentUri whether specified URI is a document URI or any supported URI
    *
    * @return {@code true} if specified URI is compatible with this repository
    *
    * @see #supportsUri(RepositoryUri)
    */
   public static boolean isCompatibleUri(RepositoryUri uri, boolean documentUri) {
      validate(uri, "uri", isNotNull()).orThrow();

      URI u = uri.toURI();

      if (!(uri.isAbsolute()
            && u.getScheme().equals(URI_SCHEME)
            && u.getPath() != null
            && u.getAuthority() == null
            && u.getFragment() == null
            && u.getQuery() == null)) {
         return false;
      }

      return !documentUri || !Path.of(u.getPath()).equals(Path.of("/"));
   }

   @Override
   public ReferencedDocument<? extends DocumentRepository> referencedDocumentId(DocumentUri documentUri) {
      Check.validate(documentUri,
                     "documentUri",
                     satisfies(this::supportsUri, "'%s' must be supported", validatingObject()));

      return ReferencedDocument.lazyLoad(this, documentId(documentUri.toURI()));
   }

   @Override
   public boolean supportsUri(RepositoryUri uri) {
      Check.notNull(uri, "uri");

      return isCompatibleUri(uri, uri instanceof DocumentUri);
   }

   @Override
   public RepositoryUri toUri(ExportUriOptions options) {
      Check.notNull(options, "options");

      try {
         return RepositoryUri.ofRepositoryUri(new URI(URI_SCHEME, null, "/", null));
      } catch (URISyntaxException e) {
         throw new IllegalStateException(e);
      }
   }

   @Override
   public DocumentUri toUri(DocumentPath documentId, ExportUriOptions options) {
      Check.notNull(documentId, "documentId");

      try {
         return DocumentUri.ofDocumentUri(new URI(URI_SCHEME, null, "/" + documentId.stringValue(), null));
      } catch (URISyntaxException e) {
         throw new IllegalStateException(e);
      }
   }

   @Override
   public Optional<Document> findDocumentByUri(DocumentUri documentUri) {
      Check.validate(documentUri,
                     "documentUri",
                     satisfies(this::supportsUri, "'%s' must be supported", validatingObject()));

      return findDocumentById(documentId(documentUri.toURI()));
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryByUri(DocumentUri documentUri) {
      Check.validate(documentUri,
                     "documentUri",
                     satisfies(this::supportsUri, "'%s' must be supported", validatingObject()));

      return findDocumentEntryById(documentId(documentUri.toURI()));
   }

   /**
    * - Potentially converts an {@link OutputStreamDocumentContent} to an
    * {@link InputStreamDocumentContentBuilder}.
    * - Filter metadata to simulate capabilities
    */
   private DocumentContent adaptContent(DocumentContent content) {
      if (content instanceof MemoryOutputStreamDocumentContent) {
         return new InputStreamDocumentContentBuilder()
               .content(new ByteArrayInputStream(content.content()),
                        hasCapability(METADATA_CONTENT_ENCODING)
                        ? content.contentEncoding().orElse(null)
                        : null,
                        hasCapability(METADATA_CONTENT_SIZE) ? (long) content.content().length : null)
               .build();
      } else if (content instanceof LoadedDocumentContent || content instanceof InputStreamDocumentContent) {
         return new InputStreamDocumentContentBuilder()
               .content(new ByteArrayInputStream(content.content()),
                        hasCapability(METADATA_CONTENT_ENCODING)
                        ? content.contentEncoding().orElse(null)
                        : null,
                        hasCapability(METADATA_CONTENT_SIZE) ? content.contentSize().orElse(null) : null)
               .build();
      } else {
         return content;
      }
   }

   /**
    * Extracts document identifier from specified document URI.
    *
    * @param documentUri document URI
    *
    * @return document identifier
    */
   private DocumentPath documentId(URI documentUri) {
      return DocumentPath.of(Path.of("/").relativize(Path.of(documentUri.getPath())));
   }

   private DocumentPath adaptCase(DocumentPath documentId) {
      if (caseInsensitive) {
         return documentId.value(documentId.stringValue().toUpperCase().toLowerCase());
      } else {
         return documentId;
      }
   }

   /**
    * Simulates a physical save.
    *
    * @param document document to save
    *
    * @return document updated for save
    *
    * @implNote Content must be loaded into memory in the case it's a stream content as a real save : a
    *       stream must be consumed by a save operation, and safely closed after that without compromising the
    *       saved content
    */
   private Document simulateSave(Document document) {
      Instant now = ApplicationClock.nowAsInstant();

      DocumentMetadataBuilder metadataBuilder = DocumentMetadataBuilder
            .from(document.metadata())
            .chain((DocumentMetadataBuilder b) -> b.lastUpdateDate(now));

      if (!hasCapability(RepositoryCapability.METADATA_DOCUMENT_PATH)) {
         metadataBuilder.documentPath(document.documentId().value());
      }
      if (!hasCapability(RepositoryCapability.METADATA_CREATION_DATE)) {
         metadataBuilder.creationDate(null);
      }
      if (!hasCapability(RepositoryCapability.METADATA_LAST_UPDATE_DATE)) {
         metadataBuilder.lastUpdateDate(null);
      }
      if (!hasCapability(RepositoryCapability.METADATA_CONTENT_TYPE)) {
         metadataBuilder.contentType((MimeType) null);
      } else {
         document.metadata().simpleContentType().ifPresent(metadataBuilder::contentType);
      }
      if (!hasCapability(METADATA_CONTENT_ENCODING)) {
         metadataBuilder.contentType((Charset) null);
      } else {
         document.metadata().contentEncoding().ifPresent(metadataBuilder::contentType);
      }
      if (!hasCapability(METADATA_CONTENT_SIZE)) {
         metadataBuilder.contentSize(null);
      }
      if (!hasCapability(RepositoryCapability.METADATA_ATTRIBUTES)) {
         metadataBuilder.attributes(null);
      }

      document = document.loadContent();

      return DocumentBuilder.from(document).metadata(metadataBuilder.build()).build();
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", TestDocumentRepository.class.getSimpleName() + "[", "]")
            .add("caseInsensitive=" + caseInsensitive)
            .toString();
   }

   /**
    * Special {@link OutputStreamDocumentContent} that store data in a {@link ByteArrayOutputStream} and
    * provide access to internal byte array.
    */
   public static class MemoryOutputStreamDocumentContent extends OutputStreamDocumentContent {

      private final ByteArrayOutputStream outputStream;

      protected MemoryOutputStreamDocumentContent(ByteArrayOutputStream outputStream,
                                                  Charset contentEncoding) {
         super(new OutputStreamDocumentContentBuilder().content(outputStream, contentEncoding));
         this.outputStream = outputStream;
      }

      public static MemoryOutputStreamDocumentContent create(Charset contentEncoding) {
         return new MemoryOutputStreamDocumentContent(new ByteArrayOutputStream(), contentEncoding);
      }

      public ByteArrayOutputStream outputStream() {
         return outputStream;
      }

      @Override
      public byte[] content() {
         return outputStream.toByteArray();
      }
   }

}
