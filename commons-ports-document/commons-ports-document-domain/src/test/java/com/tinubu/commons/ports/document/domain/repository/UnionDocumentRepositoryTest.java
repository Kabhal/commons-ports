/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.repository;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.InOrder;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;

class UnionDocumentRepositoryTest {

   @Test
   public void testOfEmpty() {
      assertThat(UnionDocumentRepository.ofEmpty()).isNotNull();
   }

   @Test
   public void testOfLayersWhenNominal() {
      TestDocumentRepository layer1 = new TestDocumentRepository();
      TestDocumentRepository layer2 = new TestDocumentRepository();

      createDocument(layer1, "file1", "layer1.file1");
      createDocument(layer1, "file2", "layer1.file2");
      createDocument(layer2, "file1", "layer2.file1");
      createDocument(layer2, "file3", "layer2.file3");

      assertThat(UnionDocumentRepository.ofLayers(layer1, layer2)).isNotNull();
      assertThat(UnionDocumentRepository.ofLayers(list(layer1, layer2))).isNotNull();

      assertThat(UnionDocumentRepository.ofLayers(layer1, layer2).upperLayer()).hasValue(layer2);
      assertThat(UnionDocumentRepository.ofLayers(list(layer1, layer2)).upperLayer()).hasValue(layer2);
   }

   @Test
   public void testOfLayersWhenEmpty() {
      assertThat(UnionDocumentRepository.ofLayers()).isNotNull();
      assertThat(UnionDocumentRepository.ofLayers(list())).isNotNull();
   }

   @Test
   public void testOfLayersWhenNull() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> UnionDocumentRepository.ofLayers((DocumentRepository[]) null))
            .withMessage("Invariant validation error > 'layers' must not be null");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> UnionDocumentRepository.ofLayers((List<DocumentRepository>) null))
            .withMessage("Invariant validation error > 'layers' must not be null");
   }

   @Test
   public void testOfLayersWhenNullElement() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> UnionDocumentRepository.ofLayers((DocumentRepository) null))
            .withMessage("Invariant validation error > 'layers=[null]' > 'layers[0]' must not be null");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> UnionDocumentRepository.ofLayers(list((DocumentRepository) null)))
            .withMessage("Invariant validation error > 'layers=[null]' > 'layers[0]' must not be null");
   }

   @Test
   public void testFindDocumentByIdWhenNominal() {
      TestDocumentRepository layer1 = new TestDocumentRepository();
      TestDocumentRepository layer2 = new TestDocumentRepository();

      createDocument(layer1, "file1", "layer1.file1");
      createDocument(layer1, "file2", "layer1.file2");
      createDocument(layer2, "file1", "layer2.file1");
      createDocument(layer2, "file3", "layer2.file3");

      UnionDocumentRepository repository = UnionDocumentRepository.ofLayers(layer1, layer2);

      assertThat(repository.findDocumentById(documentId("file1"))).hasValueSatisfying(d -> assertThatDocumentEqualTo(
            d,
            "layer2",
            "file1"));
      assertThat(repository.findDocumentById(documentId("file2"))).hasValueSatisfying(d -> assertThatDocumentEqualTo(
            d,
            "layer1",
            "file2"));
      assertThat(repository.findDocumentById(documentId("file3"))).hasValueSatisfying(d -> assertThatDocumentEqualTo(
            d,
            "layer2",
            "file3"));
      assertThat(repository.findDocumentById(documentId("file4"))).isEmpty();
   }

   @Test
   public void testFindDocumentByIdWhenEmpty() {
      try (UnionDocumentRepository repository = UnionDocumentRepository.ofLayers()) {
         assertThat(repository.findDocumentById(documentId("file1"))).isEmpty();
      }
   }

   @Test
   public void testFindDocumentEntryByIdWhenNominal() {
      TestDocumentRepository layer1 = new TestDocumentRepository();
      TestDocumentRepository layer2 = new TestDocumentRepository();

      createDocument(layer1, "file1", "layer1.file1");
      createDocument(layer1, "file2", "layer1.file2");
      createDocument(layer2, "file1", "layer2.file1");
      createDocument(layer2, "file3", "layer2.file3");

      UnionDocumentRepository repository = UnionDocumentRepository.ofLayers(layer1, layer2);

      assertThat(repository.findDocumentEntryById(documentId("file1"))).hasValueSatisfying(d -> assertThatDocumentEntryEqualTo(
            d,
            "file1"));
      assertThat(repository.findDocumentEntryById(documentId("file2"))).hasValueSatisfying(d -> assertThatDocumentEntryEqualTo(
            d,
            "file2"));
      assertThat(repository.findDocumentEntryById(documentId("file3"))).hasValueSatisfying(d -> assertThatDocumentEntryEqualTo(
            d,
            "file3"));
      assertThat(repository.findDocumentEntryById(documentId("file4"))).isEmpty();
   }

   @Test
   public void testFindDocumentEntryByIdWhenEmpty() {
      try (UnionDocumentRepository repository = UnionDocumentRepository.ofLayers()) {
         assertThat(repository.findDocumentEntryById(documentId("file1"))).isEmpty();
      }
   }

   @Test
   public void testLayerCloseStrategyWhenNominal() {
      DocumentRepository layer1 = spy(new TestDocumentRepository());
      DocumentRepository layer2 = spy(new TestDocumentRepository());
      DocumentRepository layer3 = spy(UnionDocumentRepository.ofLayers(new TestDocumentRepository()));

      createDocument(layer1, "file1", "layer1.file1");
      createDocument(layer2, "file2", "layer2.file2");
      createDocument(layer3, "file3", "layer3.file3");

      try (UnionDocumentRepository unionRepository = UnionDocumentRepository.ofLayers(layer1, layer2, layer3)
            .layerCloseStrategy(__ -> false)) {
         assertThat(unionRepository.upperLayer()).hasValue(layer3);
      }

      InOrder ordered = inOrder(layer1, layer2, layer3);

      ordered.verify(layer3, times(1)).close();
      ordered.verify(layer2, never()).close();
      ordered.verify(layer1, never()).close();
   }

   @Test
   public void testLayerCloseStrategyWhenCustomStrategy() {
      TestDocumentRepository layer1 = spy(new TestDocumentRepository());
      TestDocumentRepository layer2 = spy(new TestDocumentRepository());
      DocumentRepository layer3 = spy(UnionDocumentRepository.ofLayers(new TestDocumentRepository()));

      createDocument(layer1, "file1", "layer1.file1");
      createDocument(layer2, "file2", "layer2.file2");
      createDocument(layer3, "file3", "layer3.file3");

      try (UnionDocumentRepository unionRepository = UnionDocumentRepository.ofLayers(layer1, layer2, layer3)
            .layerCloseStrategy(layer -> layer == layer1)) {
         assertThat(unionRepository.upperLayer()).hasValue(layer3);
      }

      InOrder ordered = inOrder(layer1, layer2, layer3);

      ordered.verify(layer3, times(1)).close();
      ordered.verify(layer2, never()).close();
      ordered.verify(layer1, times(1)).close();
   }

   private void assertThatDocumentEqualTo(Document document, String expectedLayer, String expectedDocument) {
      assertThat(document.documentId().stringValue()).isEqualTo(expectedDocument);
      assertThat(document.content().stringContent()).isEqualTo(expectedLayer + "." + expectedDocument);
   }

   private void assertThatDocumentEntryEqualTo(DocumentEntry documentEntry, String expectedDocument) {
      assertThat(documentEntry.documentId().stringValue()).isEqualTo(expectedDocument);
   }

   private static DocumentPath documentId(String documentPath) {
      return DocumentPath.of(documentPath);
   }

   private static void createDocument(DocumentRepository repository, String documentName, String content) {
      repository.saveDocument(new DocumentBuilder()
                                    .documentId(documentId(documentName))
                                    .loadedContent(content, UTF_8)
                                    .build(), true);
   }

   private static void createDocument(DocumentRepository repository, String documentName) {
      createDocument(repository, documentName, documentName);
   }
}