/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.repository.cache;

import static com.tinubu.commons.ports.document.domain.repository.cache.eviction.CacheEvictionStrategy.ContextOperation.ACCESS;
import static com.tinubu.commons.ports.document.domain.repository.cache.eviction.CacheEvictionStrategy.ContextOperation.DELETE;
import static com.tinubu.commons.ports.document.domain.repository.cache.eviction.CacheEvictionStrategy.ContextOperation.SAVE;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalStateException;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.clearInvocations;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

import java.nio.file.Path;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;

import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanRegistrationException;
import javax.management.MalformedObjectNameException;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatcher;

import com.tinubu.commons.ddd2.domain.event.DomainEvent;
import com.tinubu.commons.ddd2.domain.event.DomainEventListener;
import com.tinubu.commons.lang.datetime.ApplicationClock;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentEntryCriteria.DocumentEntryCriteriaBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.metrology.DefaultDocumentRepositoryMetrology;
import com.tinubu.commons.ports.document.domain.metrology.jmx.JmxDocumentRepositoryMetrology;
import com.tinubu.commons.ports.document.domain.repository.TestDocumentRepository;
import com.tinubu.commons.ports.document.domain.repository.cache.event.CacheDocumentEvent;
import com.tinubu.commons.ports.document.domain.repository.cache.event.CacheEvicted;
import com.tinubu.commons.ports.document.domain.repository.cache.eviction.AlwaysCacheEvictionStrategy;
import com.tinubu.commons.ports.document.domain.repository.cache.eviction.CacheEvictionStrategy;
import com.tinubu.commons.ports.document.domain.repository.cache.eviction.DefaultCacheEvictionStrategy;
import com.tinubu.commons.ports.document.domain.repository.cache.metrology.CacheDocumentRepositoryMetrology;
import com.tinubu.commons.ports.document.domain.testsuite.BaseDocumentRepositoryTest;
import com.tinubu.commons.ports.document.domain.testsuite.CommonDocumentRepositoryTest;

public class CacheDocumentRepositoryTest {

   /** Test value of expiration durations in tests to base testing logic on. */
   protected static final Duration EXPIRATION = Duration.ofHours(1);

   private final ZonedDateTime FIXED_DATE =
         ZonedDateTime.of(LocalDateTime.of(2021, 11, 10, 14, 0, 0), ZoneId.of("UTC"));

   @BeforeEach
   public void setApplicationClock() {
      ApplicationClock.setFixedClock(FIXED_DATE);
   }

   protected static ArgumentMatcher<Document> matchDocument(DocumentPath documentId) {
      return document -> document.documentId().sameValueAs(documentId);
   }

   protected static ArgumentMatcher<DocumentEntry> matchDocumentEntry(DocumentPath documentId) {
      return document -> document.documentId().sameValueAs(documentId);
   }

   protected static void assertThatDefaultMetrology(DefaultDocumentRepositoryMetrology metrology,
                                                    long documentsSaved,
                                                    long documentsAccessed,
                                                    long documentsDeleted) {
      assertThat(metrology)
            .extracting(DefaultDocumentRepositoryMetrology::documentsSaved,
                        DefaultDocumentRepositoryMetrology::documentsAccessed,
                        DefaultDocumentRepositoryMetrology::documentsDeleted)
            .satisfiesExactly(v -> assertThat(v)
                                    .as("documentsSaved must be equal to %s", documentsSaved)
                                    .isEqualTo(documentsSaved),
                              v -> assertThat(v)
                                    .as("documentsAccessed must be equal to %s", documentsAccessed)
                                    .isEqualTo(documentsAccessed),
                              v -> assertThat(v)
                                    .as("documentsDeleted must be equal to %s", documentsDeleted)
                                    .isEqualTo(documentsDeleted));
   }

   protected static void assertThatCacheMetrology(CacheDocumentRepositoryMetrology metrology,
                                                  long cacheLoaded,
                                                  long cacheSaved,
                                                  long cacheHit,
                                                  long cacheEvicted,
                                                  long cacheMissed) {
      assertThat(metrology)
            .extracting(CacheDocumentRepositoryMetrology::cacheLoaded,
                        CacheDocumentRepositoryMetrology::cacheSaved,
                        CacheDocumentRepositoryMetrology::cacheHit,
                        CacheDocumentRepositoryMetrology::cacheEvicted,
                        CacheDocumentRepositoryMetrology::cacheMissed)
            .satisfiesExactly(v -> assertThat(v)
                                    .as("cacheLoaded must be equal to %s", cacheLoaded)
                                    .isEqualTo(cacheLoaded),
                              v -> assertThat(v)
                                    .as("cacheSaved must be equal to %s", cacheSaved)
                                    .isEqualTo(cacheSaved),
                              v -> assertThat(v)
                                    .as("cacheHit must be equal to %s", cacheHit)
                                    .isEqualTo(cacheHit),
                              v -> assertThat(v)
                                    .as("cacheEvicted must be equal to %s", cacheEvicted)
                                    .isEqualTo(cacheEvicted),
                              v -> assertThat(v)
                                    .as("cacheMissed must be equal to %s", cacheMissed)
                                    .isEqualTo(cacheMissed));
   }

   public abstract class BaseCacheDocumentRepositoryTest extends CommonDocumentRepositoryTest {
      private TestDocumentRepository sourceRepository;
      private TestDocumentRepository cacheRepository;
      private CacheEvictionStrategy cacheEvictionStrategy;
      private CacheDocumentRepository documentRepository;
      private CacheDocumentRepositoryMetrology metrology;

      @BeforeEach
      public void initializeDocumentRepository()
            throws InstanceAlreadyExistsException, MBeanRegistrationException, MalformedObjectNameException {
         this.sourceRepository = spy(new TestDocumentRepository());
         this.cacheRepository = spy(new TestDocumentRepository());
         this.cacheEvictionStrategy = spy(newCacheEvictionStrategy());
         this.documentRepository = newDocumentRepository();
         this.metrology =
               spy(JmxDocumentRepositoryMetrology.register(new CacheDocumentRepositoryMetrology(), "test"));
         documentRepository.registerMetrology(metrology);
      }

      @AfterEach
      public void uninitializeDocumentRepository()
            throws InstanceNotFoundException, MBeanRegistrationException, MalformedObjectNameException {
         JmxDocumentRepositoryMetrology.unregister(CacheDocumentRepositoryMetrology.class, "test");
      }

      @Override
      protected boolean isSupportingRepositoryUri() {
         return false;
      }

      @Override
      protected boolean isSupportingDocumentUri() {
         return false;
      }

      protected DocumentRepository sourceRepository() {
         return sourceRepository;
      }

      protected DocumentRepository cacheRepository() {
         return cacheRepository;
      }

      protected CacheEvictionStrategy newCacheEvictionStrategy() {
         return new AlwaysCacheEvictionStrategy();
      }

      protected CacheEvictionStrategy cacheEvictionStrategy() {
         return cacheEvictionStrategy;
      }

      @Override
      protected CacheDocumentRepository documentRepository() {
         return documentRepository;
      }

      protected CacheDocumentRepository newDocumentRepository() {
         return CacheDocumentRepository.of(sourceRepository(), cacheRepository(), cacheEvictionStrategy());
      }

      protected DocumentRepository setDocumentRepository(CacheDocumentRepository cacheDocumentRepository) {
         this.documentRepository = cacheDocumentRepository;
         return documentRepository;
      }

      protected CacheDocumentRepositoryMetrology metrology() {
         return metrology;
      }

      @Override
      protected Document zipTransformer(DocumentPath zipPath, List<Document> documents) {
         throw new UnsupportedOperationException();
      }

   }

   @Nested
   public class WhenNominal extends BaseCacheDocumentRepositoryTest {

      @Test
      public void saveDocumentShouldUpdateCacheWhenEffectivelySaved() {
         DocumentPath testDocument = DocumentPath.of(path("path/pathfile1.pdf"));

         // Save document with overwrite

         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().saveDocument(new DocumentBuilder()
                                                            .documentId(testDocument)
                                                            .streamContent("content", UTF_8)
                                                            .build(), true)).isPresent();

         verify(sourceRepository()).saveAndReturnDocument(argThat(matchDocument(testDocument)), eq(true));
         verify(cacheRepository()).saveAndReturnDocument(argThat(matchDocument(testDocument)), eq(true));

         // Save document without overwrite

         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().saveDocument(new DocumentBuilder()
                                                            .documentId(testDocument)
                                                            .streamContent("new content", UTF_8)
                                                            .build(), false)).isEmpty();

         verify(sourceRepository()).saveAndReturnDocument(argThat(matchDocument(testDocument)), eq(false));
         verify(cacheRepository(), never()).saveAndReturnDocument(argThat(matchDocument(testDocument)),
                                                                  eq(false));
         assertThat(cacheRepository().findDocumentById(testDocument)).hasValueSatisfying(document -> {
            assertThat(document.content().stringContent(UTF_8)).isEqualTo("content");
         });

         assertThatDefaultMetrology(metrology(), 1, 0, 0);
         assertThatCacheMetrology(metrology(), 0, 1, 0, 0, 0);
      }

      @Test
      public void saveAndReturnDocumentShouldUpdateCacheWhenEffectivelySaved() {
         DocumentPath testDocument = DocumentPath.of(path("path/pathfile1.pdf"));

         // Save document with overwrite

         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().saveAndReturnDocument(new DocumentBuilder()
                                                                     .documentId(testDocument)
                                                                     .streamContent("content", UTF_8)
                                                                     .build(), true)).isPresent();

         verify(sourceRepository()).saveAndReturnDocument(argThat(matchDocument(testDocument)), eq(true));
         verify(cacheRepository()).saveAndReturnDocument(argThat(matchDocument(testDocument)), eq(true));

         // Save document without overwrite

         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().saveAndReturnDocument(new DocumentBuilder()
                                                                     .documentId(testDocument)
                                                                     .streamContent("new content", UTF_8)
                                                                     .build(), false)).isEmpty();

         verify(sourceRepository()).saveAndReturnDocument(argThat(matchDocument(testDocument)), eq(false));
         verify(cacheRepository(), never()).saveAndReturnDocument(argThat(matchDocument(testDocument)),
                                                                  eq(false));
         assertThat(cacheRepository().findDocumentById(testDocument)).hasValueSatisfying(document -> {
            assertThat(document.content().stringContent(UTF_8)).isEqualTo("content");
         });
      }

      @Test
      public void deleteDocumentWhenCached() {
         DocumentPath testDocument = DocumentPath.of(path("path/pathfile1.pdf"));

         createDocument(testDocument);

         clearInvocations(sourceRepository(), cacheRepository());
         assertThat(documentRepository().deleteDocumentById(testDocument)).isPresent();

         verify(sourceRepository()).deleteDocumentById(testDocument);
         verify(cacheRepository()).deleteDocumentById(testDocument);

         clearInvocations(sourceRepository(), cacheRepository());
         assertThat(documentRepository().deleteDocumentById(testDocument)).isEmpty();

         verify(sourceRepository()).deleteDocumentById(testDocument);
         verify(cacheRepository(), never()).deleteDocumentById(testDocument);
      }

      @Test
      public void deleteDocumentWhenNotCached() {
         DocumentPath testDocument = DocumentPath.of("path/pathfile1.pdf");

         createDocument(sourceRepository(), testDocument);

         clearInvocations(sourceRepository(), cacheRepository());
         assertThat(documentRepository().deleteDocumentById(testDocument)).isPresent();

         verify(sourceRepository()).deleteDocumentById(testDocument);
         verify(cacheRepository()).deleteDocumentById(testDocument);

         clearInvocations(sourceRepository(), cacheRepository());
         assertThat(documentRepository().deleteDocumentById(testDocument)).isEmpty();

         verify(sourceRepository()).deleteDocumentById(testDocument);
         verify(cacheRepository(), never()).deleteDocumentById(testDocument);
      }

      @Test
      public void findDocumentWhenBypassedDeletionFromSourceRepository() {
         DocumentPath testDocument = DocumentPath.of("path/pathfile1.pdf");

         // Save document

         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().saveDocument(new DocumentBuilder()
                                                            .documentId(testDocument)
                                                            .streamContent("content", UTF_8)
                                                            .build(), true)).isPresent();

         verify(sourceRepository()).saveAndReturnDocument(argThat(matchDocument(testDocument)), eq(true));
         verify(cacheRepository()).saveAndReturnDocument(argThat(matchDocument(testDocument)), eq(true));

         // Delete document from source repository (bypass)

         deleteDocument(sourceRepository(), testDocument);
         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().findDocumentById(testDocument)).isPresent();

         verify(cacheRepository()).findDocumentById(testDocument);
         verify(cacheRepository(), never()).deleteDocumentById(testDocument);
         verify(sourceRepository(), never()).findDocumentById(testDocument);
         verify(cacheRepository(), never()).saveAndReturnDocument(any(Document.class), eq(true));
         assertThat(cacheRepository().findDocumentEntryById(testDocument)).isPresent();
      }

      @Test
      public void findDocumentBySpecificationWhenPartialEviction() {
         String[] documents = new String[] {
               "rootfile1.pdf", "rootfile2.txt", "path/pathfile1.pdf", "path/pathfile2.txt" };

         // Find by specification

         createDocuments(sourceRepository(), documents);

         assertThat(documentRepository().findDocumentEntriesBySpecification(DocumentEntryCriteriaBuilder.ofDocumentExtensions(
               "pdf")))
               .extracting(DocumentEntry::documentId)
               .containsExactlyInAnyOrder(DocumentPath.of("rootfile1.pdf"),
                                          DocumentPath.of("path/pathfile1.pdf"));

         verify(cacheRepository()).findDocumentsBySpecification(any(), any());
         verify(sourceRepository()).findDocumentsBySpecification(any(), any());
         verify(sourceRepository(), never()).findDocumentById(DocumentPath.of("rootfile1.pdf"));
         verify(sourceRepository(), never()).findDocumentById(DocumentPath.of("rootfile2.txt"));
         verify(sourceRepository(), never()).findDocumentById(DocumentPath.of("path/pathfile1.pdf"));
         verify(sourceRepository(), never()).findDocumentById(DocumentPath.of("path/pathfile2.txt"));
         verify(cacheRepository(), times(2)).saveAndReturnDocument(any(Document.class), eq(true));
         assertThat(cacheRepository().findDocumentEntryById(DocumentPath.of("rootfile1.pdf"))).isPresent();
         assertThat(cacheRepository().findDocumentEntryById(DocumentPath.of("rootfile2.txt"))).isEmpty();
         assertThat(cacheRepository().findDocumentEntryById(DocumentPath.of("path/pathfile1.pdf"))).isPresent();
         assertThat(cacheRepository().findDocumentEntryById(DocumentPath.of("path/pathfile2.txt"))).isEmpty();

         // Find again with partial eviction

         clearInvocations(sourceRepository(), cacheRepository());
         when(cacheEvictionStrategy().evictable(eq(DocumentPath.of("path/pathfile1.pdf")), any())).thenReturn(
               true);

         assertThat(documentRepository().findDocumentEntriesBySpecification(DocumentEntryCriteriaBuilder.ofDocumentExtensions(
               "pdf")))
               .extracting(DocumentEntry::documentId)
               .containsExactlyInAnyOrder(DocumentPath.of("rootfile1.pdf"),
                                          DocumentPath.of("path/pathfile1.pdf"));

         verify(cacheRepository()).findDocumentsBySpecification(any(), any());
         verify(sourceRepository()).findDocumentsBySpecification(any(), any());
         verify(sourceRepository(), never()).findDocumentById(DocumentPath.of("rootfile1.pdf"));
         verify(sourceRepository(), never()).findDocumentById(DocumentPath.of("rootfile2.txt"));
         verify(sourceRepository(), never()).findDocumentById(DocumentPath.of("path/pathfile1.pdf"));
         verify(sourceRepository(), never()).findDocumentById(DocumentPath.of("path/pathfile2.txt"));
         verify(cacheRepository(), times(1)).saveAndReturnDocument(any(Document.class), eq(true));
         assertThat(cacheRepository().findDocumentEntryById(DocumentPath.of("rootfile1.pdf"))).isPresent();
         assertThat(cacheRepository().findDocumentEntryById(DocumentPath.of("rootfile2.txt"))).isEmpty();
         assertThat(cacheRepository().findDocumentEntryById(DocumentPath.of("path/pathfile1.pdf"))).isPresent();
         assertThat(cacheRepository().findDocumentEntryById(DocumentPath.of("path/pathfile2.txt"))).isEmpty();
      }

      @Test
      public void evictWhenNominal() {
         DocumentPath testDocument = DocumentPath.of("path/pathfile1.pdf");

         DomainEventListener<DomainEvent> domainEventListener =
               instrumentDocumentRepositoryListeners(CacheDocumentEvent.class);

         createDocuments(sourceRepository(), testDocument);

         // Evict

         clearInvocations(sourceRepository(), cacheRepository(), domainEventListener);

         assertThat(documentRepository().evict(testDocument)).isFalse();

         verify(cacheRepository()).deleteDocumentById(testDocument);
         verifyNoMoreInteractions(domainEventListener);

         // Find document

         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().findDocumentEntryById(testDocument)).isPresent();

         verify(cacheRepository()).findDocumentEntryById(testDocument);
         verify(sourceRepository()).findDocumentById(testDocument);
         verify(cacheRepository()).saveDocument(any(Document.class), eq(true));
         assertThat(cacheRepository().findDocumentEntryById(testDocument)).isPresent();

         // Evict

         clearInvocations(sourceRepository(), cacheRepository(), domainEventListener);

         assertThat(documentRepository().evict(testDocument)).isTrue();

         verify(cacheRepository()).deleteDocumentById(testDocument);
         verify(sourceRepository(), never()).findDocumentById(testDocument);
         verify(cacheRepository(), never()).saveDocument(any(Document.class), eq(true));
         assertThat(cacheRepository().findDocumentEntryById(testDocument)).isEmpty();
         verify(domainEventListener).accept(any(CacheEvicted.class));
         verifyNoMoreInteractions(domainEventListener);

         // Find document after eviction

         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().findDocumentEntryById(testDocument)).isPresent();

         verify(cacheRepository()).findDocumentEntryById(testDocument);
         verify(cacheRepository(), never()).deleteDocumentById(testDocument);
         verify(sourceRepository()).findDocumentById(testDocument);
         verify(cacheRepository()).saveDocument(any(Document.class), eq(true));
         assertThat(cacheRepository().findDocumentEntryById(testDocument)).isPresent();
      }

      @Test
      public void findDocumentBySpecificationWhenNeverRefreshSourceMetadata() {
         String[] documents = new String[] {
               "rootfile1.pdf", "rootfile2.txt", "path/pathfile1.pdf", "path/pathfile2.txt" };

         setDocumentRepository(CacheDocumentRepository.of(sourceRepository(),
                                                          cacheRepository(),
                                                          cacheEvictionStrategy(),
                                                          null));

         // Find by specification

         createDocuments(sourceRepository(), documents);

         assertThat(documentRepository().findDocumentEntriesBySpecification(DocumentEntryCriteriaBuilder.ofDocumentExtensions(
               "pdf"))).extracting(DocumentEntry::documentId).isEmpty();

         verify(cacheRepository()).findDocumentsBySpecification(any(), any());
         verify(sourceRepository(), never()).findDocumentsBySpecification(any(), any());
         verify(sourceRepository(), never()).findDocumentById(DocumentPath.of("rootfile1.pdf"));
         verify(sourceRepository(), never()).findDocumentById(DocumentPath.of("rootfile2.txt"));
         verify(sourceRepository(), never()).findDocumentById(DocumentPath.of("path/pathfile1.pdf"));
         verify(sourceRepository(), never()).findDocumentById(DocumentPath.of("path/pathfile2.txt"));
         verify(cacheRepository(), never()).saveAndReturnDocument(any(Document.class), eq(true));
         assertThat(cacheRepository().findDocumentEntryById(DocumentPath.of("rootfile1.pdf"))).isEmpty();
         assertThat(cacheRepository().findDocumentEntryById(DocumentPath.of("rootfile2.txt"))).isEmpty();
         assertThat(cacheRepository().findDocumentEntryById(DocumentPath.of("path/pathfile1.pdf"))).isEmpty();
         assertThat(cacheRepository().findDocumentEntryById(DocumentPath.of("path/pathfile2.txt"))).isEmpty();

         // Find again after save documents to cache repository

         createDocuments(documents);

         clearInvocations(sourceRepository(), cacheRepository());
         when(cacheEvictionStrategy().evictable(eq(DocumentPath.of("path/pathfile1.pdf")), any())).thenReturn(
               true);

         assertThat(documentRepository().findDocumentEntriesBySpecification(DocumentEntryCriteriaBuilder.ofDocumentExtensions(
               "pdf")))
               .extracting(DocumentEntry::documentId)
               .containsExactlyInAnyOrder(DocumentPath.of("rootfile1.pdf"),
                                          DocumentPath.of("path/pathfile1.pdf"));

         verify(cacheRepository()).findDocumentsBySpecification(any(), any());
         verify(sourceRepository(), never()).findDocumentsBySpecification(any(), any());
         verify(sourceRepository(), never()).findDocumentById(DocumentPath.of("rootfile1.pdf"));
         verify(sourceRepository(), never()).findDocumentById(DocumentPath.of("rootfile2.txt"));
         verify(sourceRepository()).findDocumentById(DocumentPath.of("path/pathfile1.pdf"));
         verify(sourceRepository(), never()).findDocumentById(DocumentPath.of("path/pathfile2.txt"));
         verify(cacheRepository(), times(1)).saveAndReturnDocument(any(Document.class), eq(true));
         assertThat(cacheRepository().findDocumentEntryById(DocumentPath.of("rootfile1.pdf"))).isPresent();
         assertThat(cacheRepository().findDocumentEntryById(DocumentPath.of("rootfile2.txt"))).isPresent();
         assertThat(cacheRepository().findDocumentEntryById(DocumentPath.of("path/pathfile1.pdf"))).isPresent();
         assertThat(cacheRepository().findDocumentEntryById(DocumentPath.of("path/pathfile2.txt"))).isPresent();

         // Find again after delete documents from cache repository

         deleteDocuments(DocumentPath.of("rootfile1.pdf"));

         clearInvocations(sourceRepository(), cacheRepository());
         when(cacheEvictionStrategy().evictable(eq(DocumentPath.of("path/pathfile1.pdf")), any())).thenReturn(
               true);

         assertThat(documentRepository().findDocumentEntriesBySpecification(DocumentEntryCriteriaBuilder.ofDocumentExtensions(
               "pdf")))
               .extracting(DocumentEntry::documentId)
               .containsExactlyInAnyOrder(DocumentPath.of("path/pathfile1.pdf"));

         verify(cacheRepository()).findDocumentsBySpecification(any(), any());
         verify(sourceRepository(), never()).findDocumentsBySpecification(any(), any());
         verify(sourceRepository(), never()).findDocumentById(DocumentPath.of("rootfile1.pdf"));
         verify(sourceRepository(), never()).findDocumentById(DocumentPath.of("rootfile2.txt"));
         verify(sourceRepository()).findDocumentById(DocumentPath.of("path/pathfile1.pdf"));
         verify(sourceRepository(), never()).findDocumentById(DocumentPath.of("path/pathfile2.txt"));
         verify(cacheRepository(), times(1)).saveAndReturnDocument(any(Document.class), eq(true));
         assertThat(cacheRepository().findDocumentEntryById(DocumentPath.of("rootfile1.pdf"))).isEmpty();
         assertThat(cacheRepository().findDocumentEntryById(DocumentPath.of("rootfile2.txt"))).isPresent();
         assertThat(cacheRepository().findDocumentEntryById(DocumentPath.of("path/pathfile1.pdf"))).isPresent();
         assertThat(cacheRepository().findDocumentEntryById(DocumentPath.of("path/pathfile2.txt"))).isPresent();
      }

      @Test
      public void findDocumentBySpecificationWhenRefreshMetadataMaximumDuration() {
         String[] documents = new String[] {
               "rootfile1.pdf", "rootfile2.txt", "path/pathfile1.pdf", "path/pathfile2.txt" };

         Duration refreshSourceMetadataMaximumDuration = Duration.ofHours(4);
         setDocumentRepository(CacheDocumentRepository.of(sourceRepository(),
                                                          cacheRepository(),
                                                          cacheEvictionStrategy(),
                                                          refreshSourceMetadataMaximumDuration));

         // Find by specification

         createDocuments(sourceRepository(), documents);

         assertThat(documentRepository().findDocumentEntriesBySpecification(DocumentEntryCriteriaBuilder.ofDocumentExtensions(
               "pdf")))
               .extracting(DocumentEntry::documentId)
               .containsExactlyInAnyOrder(DocumentPath.of("rootfile1.pdf"),
                                          DocumentPath.of("path/pathfile1.pdf"));

         verify(cacheRepository()).findDocumentsBySpecification(any(), any());
         verify(sourceRepository()).findDocumentsBySpecification(any(), any());
         verify(sourceRepository(), never()).findDocumentById(DocumentPath.of("rootfile1.pdf"));
         verify(sourceRepository(), never()).findDocumentById(DocumentPath.of("rootfile2.txt"));
         verify(sourceRepository(), never()).findDocumentById(DocumentPath.of("path/pathfile1.pdf"));
         verify(sourceRepository(), never()).findDocumentById(DocumentPath.of("path/pathfile2.txt"));
         verify(cacheRepository(), times(2)).saveAndReturnDocument(any(Document.class), eq(true));
         assertThat(cacheRepository().findDocumentEntryById(DocumentPath.of("rootfile1.pdf"))).isPresent();
         assertThat(cacheRepository().findDocumentEntryById(DocumentPath.of("rootfile2.txt"))).isEmpty();
         assertThat(cacheRepository().findDocumentEntryById(DocumentPath.of("path/pathfile1.pdf"))).isPresent();
         assertThat(cacheRepository().findDocumentEntryById(DocumentPath.of("path/pathfile2.txt"))).isEmpty();

         // Find again before refresh metadata expiration, but after document eviction expiration

         assertThat(EXPIRATION)
               .as("explicit the test context")
               .isLessThan(refreshSourceMetadataMaximumDuration.minusMinutes(5));
         ApplicationClock.setFixedClock(BaseDocumentRepositoryTest.FIXED_DATE
                                              .plus(refreshSourceMetadataMaximumDuration)
                                              .minusMinutes(5));
         clearInvocations(sourceRepository(), cacheRepository());
         when(cacheEvictionStrategy().evictable(eq(DocumentPath.of("path/pathfile1.pdf")), any())).thenReturn(
               true);

         assertThat(documentRepository().findDocumentEntriesBySpecification(DocumentEntryCriteriaBuilder.ofDocumentExtensions(
               "pdf")))
               .extracting(DocumentEntry::documentId)
               .containsExactlyInAnyOrder(DocumentPath.of("rootfile1.pdf"),
                                          DocumentPath.of("path/pathfile1.pdf"));

         verify(cacheRepository()).findDocumentsBySpecification(any(), any());
         verify(sourceRepository(), never()).findDocumentsBySpecification(any(), any());
         verify(sourceRepository(), never()).findDocumentById(DocumentPath.of("rootfile1.pdf"));
         verify(sourceRepository(), never()).findDocumentById(DocumentPath.of("rootfile2.txt"));
         verify(sourceRepository()).findDocumentById(DocumentPath.of("path/pathfile1.pdf"));
         verify(sourceRepository(), never()).findDocumentById(DocumentPath.of("path/pathfile2.txt"));
         verify(cacheRepository(), times(1)).saveAndReturnDocument(any(Document.class), eq(true));
         assertThat(cacheRepository().findDocumentEntryById(DocumentPath.of("rootfile1.pdf"))).isPresent();
         assertThat(cacheRepository().findDocumentEntryById(DocumentPath.of("rootfile2.txt"))).isEmpty();
         assertThat(cacheRepository().findDocumentEntryById(DocumentPath.of("path/pathfile1.pdf"))).isPresent();
         assertThat(cacheRepository().findDocumentEntryById(DocumentPath.of("path/pathfile2.txt"))).isEmpty();

         // Find again after refresh metadata expiration

         ApplicationClock.setFixedClock(BaseDocumentRepositoryTest.FIXED_DATE
                                              .plus(refreshSourceMetadataMaximumDuration)
                                              .plusMinutes(5));
         clearInvocations(sourceRepository(), cacheRepository());
         when(cacheEvictionStrategy().evictable(eq(DocumentPath.of("path/pathfile1.pdf")), any())).thenReturn(
               true);

         assertThat(documentRepository().findDocumentEntriesBySpecification(DocumentEntryCriteriaBuilder.ofDocumentExtensions(
               "pdf")))
               .extracting(DocumentEntry::documentId)
               .containsExactlyInAnyOrder(DocumentPath.of("rootfile1.pdf"),
                                          DocumentPath.of("path/pathfile1.pdf"));

         verify(cacheRepository()).findDocumentsBySpecification(any(), any());
         verify(sourceRepository()).findDocumentsBySpecification(any(), any());
         verify(sourceRepository(), never()).findDocumentById(DocumentPath.of("rootfile1.pdf"));
         verify(sourceRepository(), never()).findDocumentById(DocumentPath.of("rootfile2.txt"));
         verify(sourceRepository(), never()).findDocumentById(DocumentPath.of("path/pathfile1.pdf"));
         verify(sourceRepository(), never()).findDocumentById(DocumentPath.of("path/pathfile2.txt"));
         verify(cacheRepository(), times(1)).saveAndReturnDocument(any(Document.class), eq(true));
         assertThat(cacheRepository().findDocumentEntryById(DocumentPath.of("rootfile1.pdf"))).isPresent();
         assertThat(cacheRepository().findDocumentEntryById(DocumentPath.of("rootfile2.txt"))).isEmpty();
         assertThat(cacheRepository().findDocumentEntryById(DocumentPath.of("path/pathfile1.pdf"))).isPresent();
         assertThat(cacheRepository().findDocumentEntryById(DocumentPath.of("path/pathfile2.txt"))).isEmpty();
      }

   }

   @Nested
   public class WhenCacheFailures extends BaseDocumentRepositoryTest {
      private TestDocumentRepository sourceRepository;
      private TestDocumentRepository cacheRepository;
      private CacheEvictionStrategy cacheEvictionStrategy;
      private DocumentRepository documentRepository;

      @BeforeEach
      public void initializeDocumentRepository() {
         this.sourceRepository = spy(new TestDocumentRepository());
         this.cacheRepository = spy(new TestDocumentRepository());
         this.cacheEvictionStrategy = spy(new AlwaysCacheEvictionStrategy());
         this.documentRepository = newDocumentRepository();
      }

      protected DocumentRepository sourceRepository() {
         return sourceRepository;
      }

      protected DocumentRepository cacheRepository() {
         return cacheRepository;
      }

      private CacheEvictionStrategy cacheEvictionStrategy() {
         return cacheEvictionStrategy;
      }

      @Override
      protected DocumentRepository documentRepository() {
         return documentRepository;
      }

      protected DocumentRepository newDocumentRepository() {
         return CacheDocumentRepository.of(sourceRepository(), cacheRepository(), cacheEvictionStrategy());
      }

      @Override
      protected Document zipTransformer(DocumentPath zipPath, List<Document> documents) {
         throw new UnsupportedOperationException();
      }

      @Test
      public void saveDocumentShouldThrowWhenCacheFailure() {
         DocumentPath testDocument = DocumentPath.of("path/pathfile1.pdf");

         doThrow(new IllegalStateException("save cache error"))
               .when(cacheRepository())
               .saveAndReturnDocument(argThat(matchDocument(testDocument)), anyBoolean());

         clearInvocations(sourceRepository(), cacheRepository());
         assertThatIllegalStateException()
               .isThrownBy(() -> documentRepository().saveDocument(new DocumentBuilder()
                                                                         .documentId(testDocument)
                                                                         .streamContent("content", UTF_8)
                                                                         .build(), true))
               .withMessage("save cache error");

         verify(sourceRepository()).saveAndReturnDocument(argThat(matchDocument(testDocument)), eq(true));
         verify(cacheRepository()).saveAndReturnDocument(argThat(matchDocument(testDocument)), eq(true));
         verify(cacheEvictionStrategy(), never()).updateCacheContext(any(), any());

         assertThat(cacheRepository().findDocumentEntryById(testDocument)).isEmpty();
      }

      @Test
      public void deleteDocumentShouldThrowWhenCacheFailure() {
         DocumentPath testDocument = DocumentPath.of("path/pathfile1.pdf");

         createDocument(sourceRepository(), testDocument);
         createDocument(cacheRepository(), testDocument);

         doThrow(new IllegalStateException("delete cache error"))
               .when(cacheRepository())
               .deleteDocumentById(any());

         clearInvocations(sourceRepository(), cacheRepository(), cacheEvictionStrategy());
         assertThatIllegalStateException()
               .isThrownBy(() -> documentRepository().deleteDocumentById(testDocument))
               .withMessage("delete cache error");

         verify(sourceRepository()).deleteDocumentById(testDocument);
         verify(cacheRepository()).deleteDocumentById(testDocument);
         verify(cacheEvictionStrategy(), never()).updateCacheContext(any(), eq(DELETE));
         verify(cacheEvictionStrategy(), never()).updateCacheContext(any(), eq(ACCESS));

         reset(cacheRepository());
         assertThat(cacheRepository().findDocumentEntryById(testDocument)).isPresent();
      }

      @Test
      public void findDocumentShouldThrowWhenFindFromCacheFailure() {
         DocumentPath testDocument = DocumentPath.of("path/pathfile1.pdf");

         createDocument(sourceRepository(), testDocument);

         doThrow(new IllegalStateException("find cache error"))
               .when(cacheRepository())
               .findDocumentById(any());

         clearInvocations(sourceRepository(), cacheRepository(), cacheEvictionStrategy());
         assertThatIllegalStateException()
               .isThrownBy(() -> documentRepository().findDocumentById(testDocument))
               .withMessage("find cache error");

         verify(cacheRepository()).findDocumentById(testDocument);
         verify(sourceRepository(), never()).findDocumentById(testDocument);
         verify(cacheRepository(), never()).saveAndReturnDocument(argThat(matchDocument(testDocument)),
                                                                  eq(true));
         verify(cacheEvictionStrategy(), never()).updateCacheContext(any(), eq(ACCESS));
         verify(cacheEvictionStrategy(), never()).updateCacheContext(any(), eq(SAVE));

         reset(cacheRepository());
         assertThat(cacheRepository().findDocumentEntryById(testDocument)).isEmpty();
      }

      @Test
      public void findDocumentShouldThrowWhenSaveToCacheFailure() {
         DocumentPath testDocument = DocumentPath.of("path/pathfile1.pdf");

         createDocument(sourceRepository(), testDocument);

         when(cacheEvictionStrategy().evictable(eq(testDocument), any())).thenReturn(true);
         doThrow(new IllegalStateException("save cache error"))
               .when(cacheRepository())
               .saveAndReturnDocument(any(), eq(true));

         clearInvocations(sourceRepository(), cacheRepository(), cacheEvictionStrategy());
         assertThatIllegalStateException()
               .isThrownBy(() -> documentRepository().findDocumentById(testDocument))
               .withMessage("save cache error");

         verify(cacheRepository()).findDocumentById(testDocument);
         verify(sourceRepository()).findDocumentById(testDocument);
         verify(cacheRepository()).saveAndReturnDocument(argThat(matchDocument(testDocument)), eq(true));
         verify(cacheEvictionStrategy(), never()).updateCacheContext(any(), eq(ACCESS));
         verify(cacheEvictionStrategy(), never()).updateCacheContext(any(), eq(SAVE));

         reset(cacheRepository());
         assertThat(cacheRepository().findDocumentEntryById(testDocument)).isEmpty();
      }

      @Test
      public void findDocumentEntryShouldThrowWhenSaveToCacheFailure() {
         DocumentPath testDocument = DocumentPath.of("path/pathfile1.pdf");

         createDocument(sourceRepository(), testDocument);

         when(cacheEvictionStrategy().evictable(eq(testDocument), any())).thenReturn(true);
         doThrow(new IllegalStateException("save cache error"))
               .when(cacheRepository())
               .saveDocument(any(), eq(true));

         clearInvocations(sourceRepository(), cacheRepository(), cacheEvictionStrategy());

         assertThatIllegalStateException()
               .isThrownBy(() -> documentRepository().findDocumentEntryById(testDocument))
               .withMessage("save cache error");

         verify(cacheRepository()).findDocumentEntryById(testDocument);
         verify(sourceRepository()).findDocumentById(testDocument);
         verify(cacheRepository()).saveDocument(argThat(matchDocument(testDocument)), eq(true));
         verify(cacheEvictionStrategy(), never()).updateCacheContext(any(), eq(ACCESS));
         verify(cacheEvictionStrategy(), never()).updateCacheContext(any(), eq(SAVE));

         reset(cacheRepository());
         assertThat(cacheRepository().findDocumentEntryById(testDocument)).isEmpty();
      }

   }

   @Nested
   public class WhenDefaultCacheExpireAfterWrite extends BaseCacheDocumentRepositoryTest {

      @Override
      protected CacheEvictionStrategy newCacheEvictionStrategy() {
         return DefaultCacheEvictionStrategy.expireAfterWrite(EXPIRATION);
      }

      @Test
      public void testSaveDocumentWhenExpireAfterWrite() {
         DocumentPath testDocument = DocumentPath.of("path/pathfile1.pdf");

         // Save document

         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().saveDocument(new DocumentBuilder()
                                                            .documentId(testDocument)
                                                            .streamContent("content", UTF_8)
                                                            .build(), true)).isPresent();

         verify(sourceRepository()).saveAndReturnDocument(argThat(matchDocument(testDocument)), eq(true));
         verify(cacheRepository()).saveAndReturnDocument(argThat(matchDocument(testDocument)), eq(true));

         // Save document before cache expiration

         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().saveDocument(new DocumentBuilder()
                                                            .documentId(testDocument)
                                                            .streamContent("content", UTF_8)
                                                            .build(), true)).isPresent();

         verify(sourceRepository()).saveAndReturnDocument(argThat(matchDocument(testDocument)), eq(true));
         verify(cacheRepository()).saveAndReturnDocument(argThat(matchDocument(testDocument)), eq(true));
         assertThat(cacheRepository().findDocumentEntryById(testDocument)).isPresent();

         // Save document after cache expiration

         ApplicationClock.setFixedClock(BaseDocumentRepositoryTest.FIXED_DATE.plus(EXPIRATION)
                                              .plusMinutes(5));
         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().saveDocument(new DocumentBuilder()
                                                            .documentId(testDocument)
                                                            .streamContent("content", UTF_8)
                                                            .build(), true)).isPresent();

         verify(sourceRepository()).saveAndReturnDocument(argThat(matchDocument(testDocument)), eq(true));
         verify(cacheRepository()).saveAndReturnDocument(argThat(matchDocument(testDocument)), eq(true));
         assertThat(cacheRepository().findDocumentEntryById(testDocument)).isPresent();
      }

      @Test
      public void testFindDocumentWhenExpireAfterWrite() {
         DocumentPath testDocument = DocumentPath.of("path/pathfile1.pdf");

         // Find document

         createDocument(sourceRepository(), testDocument);
         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().findDocumentById(testDocument)).isPresent();

         verify(cacheRepository()).findDocumentById(testDocument);
         verify(sourceRepository()).findDocumentById(testDocument);
         verify(cacheRepository()).saveAndReturnDocument(any(Document.class), eq(true));
         assertThat(cacheRepository().findDocumentEntryById(testDocument)).isPresent();

         // Find document before cache expiration

         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().findDocumentById(testDocument)).isPresent();

         verify(cacheRepository()).findDocumentById(testDocument);
         verify(cacheRepository(), never()).deleteDocumentById(testDocument);
         verify(sourceRepository(), never()).findDocumentById(testDocument);
         verify(cacheRepository(), never()).saveAndReturnDocument(any(Document.class), eq(true));
         assertThat(cacheRepository().findDocumentEntryById(testDocument)).isPresent();

         // Find document after cache expiration

         ApplicationClock.setFixedClock(BaseDocumentRepositoryTest.FIXED_DATE.plus(EXPIRATION)
                                              .plusMinutes(5));
         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().findDocumentById(testDocument)).isPresent();

         verify(cacheRepository()).findDocumentById(testDocument);
         verify(cacheRepository()).deleteDocumentById(testDocument);
         verify(sourceRepository()).findDocumentById(testDocument);
         verify(cacheRepository()).saveAndReturnDocument(any(Document.class), eq(true));
         assertThat(cacheRepository().findDocumentEntryById(testDocument)).isPresent();
      }

      @Test
      public void testFindDocumentEntryWhenExpireAfterWrite() {
         DocumentPath testDocument = DocumentPath.of("path/pathfile1.pdf");

         // Find document

         createDocument(sourceRepository(), testDocument);
         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().findDocumentEntryById(testDocument)).isPresent();

         verify(cacheRepository()).findDocumentEntryById(testDocument);
         verify(sourceRepository()).findDocumentById(testDocument);
         verify(cacheRepository()).saveDocument(any(Document.class), eq(true));
         assertThat(cacheRepository().findDocumentEntryById(testDocument)).isPresent();

         // Find document before cache expiration

         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().findDocumentEntryById(testDocument)).isPresent();

         verify(cacheRepository()).findDocumentEntryById(testDocument);
         verify(cacheRepository(), never()).deleteDocumentById(testDocument);
         verify(sourceRepository(), never()).findDocumentById(testDocument);
         verify(cacheRepository(), never()).saveDocument(any(Document.class), eq(true));
         assertThat(cacheRepository().findDocumentEntryById(testDocument)).isPresent();

         // Find document after cache expiration

         ApplicationClock.setFixedClock(BaseDocumentRepositoryTest.FIXED_DATE.plus(EXPIRATION)
                                              .plusMinutes(5));
         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().findDocumentEntryById(testDocument)).isPresent();

         verify(cacheRepository()).findDocumentEntryById(testDocument);
         verify(cacheRepository()).deleteDocumentById(testDocument);
         verify(sourceRepository()).findDocumentById(testDocument);
         verify(cacheRepository()).saveDocument(any(Document.class), eq(true));
         assertThat(cacheRepository().findDocumentEntryById(testDocument)).isPresent();
      }

      @Test
      public void testDeleteDocumentWhenExpireAfterWrite() {
         DocumentPath testDocument = DocumentPath.of("path/pathfile1.pdf");

         // Delete document before cache expiration

         createDocument(testDocument);
         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().deleteDocumentById(testDocument)).isPresent();

         verify(sourceRepository()).deleteDocumentById(testDocument);
         verify(cacheRepository()).deleteDocumentById(testDocument);
         assertThat(cacheRepository().findDocumentEntryById(testDocument)).isEmpty();

         // Recreate

         createDocument(testDocument);

         // Delete document after cache expiration

         ApplicationClock.setFixedClock(BaseDocumentRepositoryTest.FIXED_DATE.plus(EXPIRATION)
                                              .plusMinutes(5));
         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().deleteDocumentById(testDocument)).isPresent();

         verify(sourceRepository()).deleteDocumentById(testDocument);
         verify(cacheRepository()).deleteDocumentById(testDocument);
         assertThat(cacheRepository().findDocumentEntryById(testDocument)).isEmpty();
      }
   }

   @Nested
   public class WhenSubPath extends BaseCacheDocumentRepositoryTest {

      @Override
      protected CacheEvictionStrategy newCacheEvictionStrategy() {
         return DefaultCacheEvictionStrategy.expireAfterWrite(EXPIRATION);
      }

      @Test
      public void testCacheExpirationWhenSubPath() {
         DocumentPath testDocument = DocumentPath.of(path("path/pathfile1.pdf"));
         DocumentPath testSubPathDocument = DocumentPath.of(path("pathfile1.pdf"));

         // Save document

         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().saveDocument(new DocumentBuilder()
                                                            .documentId(testDocument)
                                                            .streamContent("content", UTF_8)
                                                            .build(), true)).isPresent();

         verify(sourceRepository()).saveAndReturnDocument(argThat(matchDocument(testDocument)), eq(true));
         verify(cacheRepository()).saveAndReturnDocument(argThat(matchDocument(testDocument)), eq(true));

         // Find document

         assertThat(documentRepository().subPath(Path.of("path"), false)
                          .findDocumentEntryById(testSubPathDocument)).isPresent();

         assertThat(cacheRepository().subPath(Path.of("path"), false)
                          .findDocumentEntryById(testSubPathDocument)).isPresent();

         // Find document after cache expiration

         ApplicationClock.setFixedClock(BaseDocumentRepositoryTest.FIXED_DATE.plus(EXPIRATION)
                                              .plusMinutes(5));
         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().subPath(Path.of("path"), false)
                          .findDocumentEntryById(testSubPathDocument)).isPresent();

         assertThat(cacheRepository().subPath(Path.of("path"), false)
                          .findDocumentEntryById(testSubPathDocument)).isPresent();
      }

   }

   @Nested
   public class WhenDefaultCacheExpireAfterAccess extends BaseCacheDocumentRepositoryTest {

      @Override
      protected CacheEvictionStrategy newCacheEvictionStrategy() {
         return DefaultCacheEvictionStrategy.expireAfterAccess(EXPIRATION);
      }

      @Test
      public void testSaveDocumentWhenExpireAfterAccess() {
         DocumentPath testDocument = DocumentPath.of("path/pathfile1.pdf");

         // Save document

         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().saveDocument(new DocumentBuilder()
                                                            .documentId(testDocument)
                                                            .streamContent("content", UTF_8)
                                                            .build(), true)).isPresent();

         verify(sourceRepository()).saveAndReturnDocument(argThat(matchDocument(testDocument)), eq(true));
         verify(cacheRepository()).saveAndReturnDocument(argThat(matchDocument(testDocument)), eq(true));

         // Save document before cache expiration

         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().saveDocument(new DocumentBuilder()
                                                            .documentId(testDocument)
                                                            .streamContent("content", UTF_8)
                                                            .build(), true)).isPresent();

         verify(sourceRepository()).saveAndReturnDocument(argThat(matchDocument(testDocument)), eq(true));
         verify(cacheRepository()).saveAndReturnDocument(argThat(matchDocument(testDocument)), eq(true));
         assertThat(cacheRepository().findDocumentEntryById(testDocument)).isPresent();

         // Save document after cache expiration

         ApplicationClock.setFixedClock(BaseDocumentRepositoryTest.FIXED_DATE.plus(EXPIRATION)
                                              .plusMinutes(5));
         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().saveDocument(new DocumentBuilder()
                                                            .documentId(testDocument)
                                                            .streamContent("content", UTF_8)
                                                            .build(), true)).isPresent();

         verify(sourceRepository()).saveAndReturnDocument(argThat(matchDocument(testDocument)), eq(true));
         verify(cacheRepository()).saveAndReturnDocument(argThat(matchDocument(testDocument)), eq(true));
         assertThat(cacheRepository().findDocumentEntryById(testDocument)).isPresent();
      }

      @Test
      public void testSaveDocumentWhenRefreshedBeforeExpireAccess() {
         DocumentPath testDocument = DocumentPath.of("path/pathfile1.pdf");

         // Save document

         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().saveDocument(new DocumentBuilder()
                                                            .documentId(testDocument)
                                                            .streamContent("content", UTF_8)
                                                            .build(), true)).isPresent();

         verify(sourceRepository()).saveAndReturnDocument(argThat(matchDocument(testDocument)), eq(true));
         verify(cacheRepository()).saveAndReturnDocument(argThat(matchDocument(testDocument)), eq(true));

         // Refresh access before expiration

         ApplicationClock.setFixedClock(BaseDocumentRepositoryTest.FIXED_DATE.plus(EXPIRATION)
                                              .minusMinutes(5));
         documentRepository().findDocumentById(testDocument);

         // Save document after cache expiration

         ApplicationClock.setFixedClock(BaseDocumentRepositoryTest.FIXED_DATE.plus(EXPIRATION)
                                              .plusMinutes(5));
         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().saveDocument(new DocumentBuilder()
                                                            .documentId(testDocument)
                                                            .streamContent("content", UTF_8)
                                                            .build(), true)).isPresent();

         verify(sourceRepository()).saveAndReturnDocument(argThat(matchDocument(testDocument)), eq(true));
         verify(cacheRepository()).saveAndReturnDocument(argThat(matchDocument(testDocument)), eq(true));
         assertThat(cacheRepository().findDocumentEntryById(testDocument)).isPresent();
      }

      @Test
      public void testFindDocumentWhenExpireAfterAccess() {
         DocumentPath testDocument = DocumentPath.of("path/pathfile1.pdf");

         // Find document

         createDocument(sourceRepository(), testDocument);
         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().findDocumentById(testDocument)).isPresent();

         verify(cacheRepository()).findDocumentById(testDocument);
         verify(sourceRepository()).findDocumentById(testDocument);
         verify(cacheRepository()).saveAndReturnDocument(any(Document.class), eq(true));
         assertThat(cacheRepository().findDocumentEntryById(testDocument)).isPresent();

         // Find document before cache expiration

         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().findDocumentById(testDocument)).isPresent();

         verify(cacheRepository()).findDocumentById(testDocument);
         verify(cacheRepository(), never()).deleteDocumentById(testDocument);
         verify(sourceRepository(), never()).findDocumentById(testDocument);
         verify(cacheRepository(), never()).saveAndReturnDocument(any(Document.class), eq(true));
         assertThat(cacheRepository().findDocumentEntryById(testDocument)).isPresent();

         // Find document after cache expiration

         ApplicationClock.setFixedClock(BaseDocumentRepositoryTest.FIXED_DATE.plus(EXPIRATION)
                                              .plusMinutes(5));
         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().findDocumentById(testDocument)).isPresent();

         verify(cacheRepository()).findDocumentById(testDocument);
         verify(cacheRepository()).deleteDocumentById(testDocument);
         verify(sourceRepository()).findDocumentById(testDocument);
         verify(cacheRepository()).saveAndReturnDocument(any(Document.class), eq(true));
         assertThat(cacheRepository().findDocumentEntryById(testDocument)).isPresent();
      }

      @Test
      public void testFindDocumentWhenRefreshedBeforeExpireAccess() {
         DocumentPath testDocument = DocumentPath.of("path/pathfile1.pdf");

         // Find document

         createDocument(sourceRepository(), testDocument);
         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().findDocumentById(testDocument)).isPresent();

         verify(cacheRepository()).findDocumentById(testDocument);
         verify(sourceRepository()).findDocumentById(testDocument);
         verify(cacheRepository()).saveAndReturnDocument(any(Document.class), eq(true));
         assertThat(cacheRepository().findDocumentEntryById(testDocument)).isPresent();

         // Refresh access before expiration

         ApplicationClock.setFixedClock(BaseDocumentRepositoryTest.FIXED_DATE.plus(EXPIRATION)
                                              .minusMinutes(5));
         documentRepository().findDocumentById(testDocument);

         // Find document after cache expiration

         ApplicationClock.setFixedClock(BaseDocumentRepositoryTest.FIXED_DATE.plus(EXPIRATION)
                                              .plusMinutes(5));
         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().findDocumentById(testDocument)).isPresent();

         verify(cacheRepository()).findDocumentById(testDocument);
         verify(cacheRepository(), never()).deleteDocumentById(testDocument);
         verify(sourceRepository(), never()).findDocumentById(testDocument);
         verify(cacheRepository(), never()).saveAndReturnDocument(any(Document.class), eq(true));
         assertThat(cacheRepository().findDocumentEntryById(testDocument)).isPresent();

      }

      @Test
      public void testFindDocumentEntryWhenExpireAfterAccess() {
         DocumentPath testDocument = DocumentPath.of("path/pathfile1.pdf");

         // Find document

         createDocument(sourceRepository(), testDocument);
         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().findDocumentEntryById(testDocument)).isPresent();

         verify(cacheRepository()).findDocumentEntryById(testDocument);
         verify(sourceRepository()).findDocumentById(testDocument);
         verify(cacheRepository()).saveDocument(any(Document.class), eq(true));
         assertThat(cacheRepository().findDocumentEntryById(testDocument)).isPresent();

         // Find document before cache expiration

         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().findDocumentEntryById(testDocument)).isPresent();

         verify(cacheRepository()).findDocumentEntryById(testDocument);
         verify(cacheRepository(), never()).deleteDocumentById(testDocument);
         verify(sourceRepository(), never()).findDocumentById(testDocument);
         verify(cacheRepository(), never()).saveDocument(any(Document.class), eq(true));
         assertThat(cacheRepository().findDocumentEntryById(testDocument)).isPresent();

         // Find document after cache expiration

         ApplicationClock.setFixedClock(BaseDocumentRepositoryTest.FIXED_DATE
                                              .plus(EXPIRATION)
                                              .plusMinutes(5));
         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().findDocumentEntryById(testDocument)).isPresent();

         verify(cacheRepository()).findDocumentEntryById(testDocument);
         verify(cacheRepository()).deleteDocumentById(testDocument);
         verify(sourceRepository()).findDocumentById(testDocument);
         verify(cacheRepository()).saveDocument(any(Document.class), eq(true));
         assertThat(cacheRepository().findDocumentEntryById(testDocument)).isPresent();
      }

      @Test
      public void testFindDocumentEntryWhenRefreshedBeforeExpireAfter() {
         DocumentPath testDocument = DocumentPath.of("path/pathfile1.pdf");

         // Find document

         createDocument(sourceRepository(), testDocument);
         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().findDocumentEntryById(testDocument)).isPresent();

         verify(cacheRepository()).findDocumentEntryById(testDocument);
         verify(sourceRepository()).findDocumentById(testDocument);
         verify(cacheRepository()).saveDocument(any(Document.class), eq(true));
         assertThat(cacheRepository().findDocumentEntryById(testDocument)).isPresent();

         // Refresh access before expiration

         ApplicationClock.setFixedClock(BaseDocumentRepositoryTest.FIXED_DATE.plus(EXPIRATION)
                                              .minusMinutes(5));
         documentRepository().findDocumentById(testDocument);

         // Find document after cache expiration

         ApplicationClock.setFixedClock(BaseDocumentRepositoryTest.FIXED_DATE.plus(EXPIRATION)
                                              .plusMinutes(5));
         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().findDocumentEntryById(testDocument)).isPresent();

         verify(cacheRepository()).findDocumentEntryById(testDocument);
         verify(cacheRepository(), never()).deleteDocumentById(testDocument);
         verify(sourceRepository(), never()).findDocumentById(testDocument);
         verify(cacheRepository(), never()).saveDocument(any(Document.class), eq(true));
         assertThat(cacheRepository().findDocumentEntryById(testDocument)).isPresent();
      }

      @Test
      public void testDeleteDocumentWhenExpireAfterAccess() {
         DocumentPath testDocument = DocumentPath.of("path/pathfile1.pdf");

         // Delete document before cache expiration

         createDocument(testDocument);
         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().deleteDocumentById(testDocument)).isPresent();

         verify(sourceRepository()).deleteDocumentById(testDocument);
         verify(cacheRepository()).deleteDocumentById(testDocument);
         assertThat(cacheRepository().findDocumentEntryById(testDocument)).isEmpty();

         // Recreate

         createDocument(testDocument);

         // Delete document after cache expiration

         ApplicationClock.setFixedClock(BaseDocumentRepositoryTest.FIXED_DATE.plus(EXPIRATION)
                                              .plusMinutes(5));
         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().deleteDocumentById(testDocument)).isPresent();

         verify(sourceRepository()).deleteDocumentById(testDocument);
         verify(cacheRepository()).deleteDocumentById(testDocument);
         assertThat(cacheRepository().findDocumentEntryById(testDocument)).isEmpty();
      }

      @Test
      public void testDeleteDocumentWhenRefreshedBeforeExpireAccess() {
         DocumentPath testDocument = DocumentPath.of("path/pathfile1.pdf");

         // Delete document before cache expiration

         createDocument(testDocument);
         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().deleteDocumentById(testDocument)).isPresent();

         verify(sourceRepository()).deleteDocumentById(testDocument);
         verify(cacheRepository()).deleteDocumentById(testDocument);
         assertThat(cacheRepository().findDocumentEntryById(testDocument)).isEmpty();

         // Recreate

         createDocument(testDocument);

         // Refresh access before expiration

         ApplicationClock.setFixedClock(BaseDocumentRepositoryTest.FIXED_DATE.plus(EXPIRATION)
                                              .minusMinutes(5));
         documentRepository().findDocumentById(testDocument);

         // Delete document after cache expiration

         ApplicationClock.setFixedClock(BaseDocumentRepositoryTest.FIXED_DATE.plus(EXPIRATION)
                                              .plusMinutes(5));
         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().deleteDocumentById(testDocument)).isPresent();

         verify(sourceRepository()).deleteDocumentById(testDocument);
         verify(cacheRepository()).deleteDocumentById(testDocument);
         assertThat(cacheRepository().findDocumentEntryById(testDocument)).isEmpty();
      }
   }

   @Nested
   public class WhenDefaultCacheMaximumSize extends BaseCacheDocumentRepositoryTest {

      @Override
      protected CacheEvictionStrategy newCacheEvictionStrategy() {
         return DefaultCacheEvictionStrategy.neverExpire().maximumSize(2);
      }

      @Test
      public void testSaveDocumentWhenMaximumSize() {
         DocumentPath testDocument1 = DocumentPath.of(path("path/pathfile1.pdf"));
         DocumentPath testDocument2 = DocumentPath.of(path("path/pathfile2.pdf"));
         DocumentPath testDocument3 = DocumentPath.of(path("path/pathfile3.pdf"));

         // Save document 1 -> refresh document 1

         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().saveDocument(new DocumentBuilder()
                                                            .documentId(testDocument1)
                                                            .streamContent("content", UTF_8)
                                                            .build(), true)).isPresent();

         verify(sourceRepository()).saveAndReturnDocument(argThat(matchDocument(testDocument1)), eq(true));
         verify(cacheRepository()).saveAndReturnDocument(argThat(matchDocument(testDocument1)), eq(true));
         verify(cacheRepository(), never()).deleteDocumentById(any());

         // Save document 2 -> refresh document 2

         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().saveDocument(new DocumentBuilder()
                                                            .documentId(testDocument2)
                                                            .streamContent("content", UTF_8)
                                                            .build(), true)).isPresent();

         verify(sourceRepository()).saveAndReturnDocument(argThat(matchDocument(testDocument2)), eq(true));
         verify(cacheRepository()).saveAndReturnDocument(argThat(matchDocument(testDocument2)), eq(true));
         verify(cacheRepository(), never()).deleteDocumentById(any());

         // Save document 3 -> refresh document 3 -> evict document 1 (evictAll)

         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().saveDocument(new DocumentBuilder()
                                                            .documentId(testDocument3)
                                                            .streamContent("content", UTF_8)
                                                            .build(), true)).isPresent();

         verify(sourceRepository()).saveAndReturnDocument(argThat(matchDocument(testDocument3)), eq(true));
         verify(cacheRepository()).saveAndReturnDocument(argThat(matchDocument(testDocument3)), eq(true));
         verify(cacheRepository()).deleteDocumentById(testDocument1);
         verify(cacheRepository(), never()).deleteDocumentById(testDocument2);
         verify(cacheRepository(), never()).deleteDocumentById(testDocument3);

         // Save document 2 -> refresh document 2

         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().saveDocument(new DocumentBuilder()
                                                            .documentId(testDocument2)
                                                            .streamContent("content", UTF_8)
                                                            .build(), true)).isPresent();

         verify(sourceRepository()).saveAndReturnDocument(argThat(matchDocument(testDocument2)), eq(true));
         verify(cacheRepository()).saveAndReturnDocument(argThat(matchDocument(testDocument2)), eq(true));
         verify(cacheRepository(), never()).deleteDocumentById(any());

         // Save document 1 -> refresh document 1 -> evict document 3 (evictAll)

         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().saveDocument(new DocumentBuilder()
                                                            .documentId(testDocument1)
                                                            .streamContent("content", UTF_8)
                                                            .build(), true)).isPresent();

         verify(sourceRepository()).saveAndReturnDocument(argThat(matchDocument(testDocument1)), eq(true));
         verify(cacheRepository()).saveAndReturnDocument(argThat(matchDocument(testDocument1)), eq(true));
         verify(cacheRepository(), never()).deleteDocumentById(testDocument1);
         verify(cacheRepository(), never()).deleteDocumentById(testDocument2);
         verify(cacheRepository()).deleteDocumentById(testDocument3);
      }

      @Test
      public void testFindDocumentWhenMaximumSize() {
         DocumentPath testDocument1 = DocumentPath.of(path("path/pathfile1.pdf"));
         DocumentPath testDocument2 = DocumentPath.of(path("path/pathfile2.pdf"));
         DocumentPath testDocument3 = DocumentPath.of(path("path/pathfile3.pdf"));

         createDocuments(sourceRepository(), testDocument1, testDocument2, testDocument3);

         // Find document 1 -> refresh document 1

         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().findDocumentById(testDocument1)).isPresent();

         verify(sourceRepository()).findDocumentById(testDocument1);
         verify(cacheRepository()).saveAndReturnDocument(argThat(matchDocument(testDocument1)), eq(true));
         verify(cacheRepository(), never()).deleteDocumentById(any());

         // Find document 2 -> refresh document 2

         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().findDocumentById(testDocument2)).isPresent();

         verify(sourceRepository()).findDocumentById(testDocument2);
         verify(cacheRepository()).saveAndReturnDocument(argThat(matchDocument(testDocument2)), eq(true));
         verify(cacheRepository(), never()).deleteDocumentById(any());

         // Find document 3 -> refresh document 3 -> evict document 1 (evictAll)

         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().findDocumentById(testDocument3)).isPresent();

         verify(sourceRepository()).findDocumentById(testDocument3);
         verify(cacheRepository()).saveAndReturnDocument(argThat(matchDocument(testDocument3)), eq(true));
         verify(cacheRepository()).deleteDocumentById(testDocument1);
         verify(cacheRepository(), never()).deleteDocumentById(testDocument2);
         verify(cacheRepository(), never()).deleteDocumentById(testDocument3);

         // Find document 2 -> refresh document 2

         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().findDocumentById(testDocument2)).isPresent();

         verify(cacheRepository()).findDocumentById(testDocument2);
         verify(cacheRepository(), never()).saveAndReturnDocument(any(), eq(true));
         verify(cacheRepository(), never()).deleteDocumentById(any());

         // Find document 1 -> refresh document 1 -> evict document 3 (evictAll)

         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().findDocumentById(testDocument1)).isPresent();

         verify(sourceRepository()).findDocumentById(testDocument1);
         verify(cacheRepository()).saveAndReturnDocument(argThat(matchDocument(testDocument1)), eq(true));
         verify(cacheRepository(), never()).deleteDocumentById(testDocument1);
         verify(cacheRepository(), never()).deleteDocumentById(testDocument2);
         verify(cacheRepository()).deleteDocumentById(testDocument3);
      }

      @Test
      public void testFindDocumentEntryWhenMaximumSize() {
         DocumentPath testDocument1 = DocumentPath.of(path("path/pathfile1.pdf"));
         DocumentPath testDocument2 = DocumentPath.of(path("path/pathfile2.pdf"));
         DocumentPath testDocument3 = DocumentPath.of(path("path/pathfile3.pdf"));

         createDocuments(sourceRepository(), testDocument1, testDocument2, testDocument3);

         // Find document 1 -> refresh document 1

         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().findDocumentEntryById(testDocument1)).isPresent();

         verify(sourceRepository()).findDocumentById(testDocument1);
         verify(cacheRepository()).saveDocument(argThat(matchDocument(testDocument1)), eq(true));
         verify(cacheRepository(), never()).deleteDocumentById(any());

         // Find document 2 -> refresh document 2

         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().findDocumentEntryById(testDocument2)).isPresent();

         verify(sourceRepository()).findDocumentById(testDocument2);
         verify(cacheRepository()).saveDocument(argThat(matchDocument(testDocument2)), eq(true));
         verify(cacheRepository(), never()).deleteDocumentById(any());

         // Find document 3 -> refresh document 3 -> evict document 1 (evictAll)

         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().findDocumentEntryById(testDocument3)).isPresent();

         verify(sourceRepository()).findDocumentById(testDocument3);
         verify(cacheRepository()).saveDocument(argThat(matchDocument(testDocument3)), eq(true));
         verify(cacheRepository()).deleteDocumentById(testDocument1);
         verify(cacheRepository(), never()).deleteDocumentById(testDocument2);
         verify(cacheRepository(), never()).deleteDocumentById(testDocument3);

         // Find document 2 -> refresh document 2

         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().findDocumentEntryById(testDocument2)).isPresent();

         verify(cacheRepository()).findDocumentById(testDocument2);
         verify(cacheRepository(), never()).saveDocument(any(), eq(true));
         verify(cacheRepository(), never()).deleteDocumentById(any());

         // Find document 1 -> refresh document 1 -> evict document 3 (evictAll)

         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().findDocumentEntryById(testDocument1)).isPresent();

         verify(sourceRepository()).findDocumentById(testDocument1);
         verify(cacheRepository()).saveDocument(argThat(matchDocument(testDocument1)), eq(true));
         verify(cacheRepository(), never()).deleteDocumentById(testDocument1);
         verify(cacheRepository(), never()).deleteDocumentById(testDocument2);
         verify(cacheRepository()).deleteDocumentById(testDocument3);
      }

   }

   @Nested
   public class WhenComposedEvictionStrategy extends BaseCacheDocumentRepositoryTest {

      @Override
      protected CacheEvictionStrategy newCacheEvictionStrategy() {
         return DefaultCacheEvictionStrategy
               .expireAfterWrite(EXPIRATION)
               .or(DefaultCacheEvictionStrategy.neverExpire().maximumSize(2));
      }

      @Test
      public void testFindDocumentEntryWhenComposedStrategy() {
         DocumentPath testDocument1 = DocumentPath.of(path("path/pathfile1.pdf"));
         DocumentPath testDocument2 = DocumentPath.of(path("path/pathfile2.pdf"));
         DocumentPath testDocument3 = DocumentPath.of(path("path/pathfile3.pdf"));

         createDocuments(sourceRepository(), testDocument1, testDocument2, testDocument3);

         // Find document 1 [expireAfterWrite & maximumSize] -> refresh document 1

         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().findDocumentEntryById(testDocument1)).isPresent();

         verify(sourceRepository()).findDocumentById(testDocument1);
         verify(cacheRepository()).saveDocument(argThat(matchDocument(testDocument1)), eq(true));
         verify(cacheRepository(), never()).deleteDocumentById(any());

         // Find document 2 [expireAfterWrite] -> refresh document 2 -> evict document 1 (evictAll)

         ApplicationClock.setFixedClock(BaseDocumentRepositoryTest.FIXED_DATE.plus(EXPIRATION)
                                              .plusMinutes(5));
         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().findDocumentEntryById(testDocument2)).isPresent();

         verify(sourceRepository()).findDocumentById(testDocument2);
         verify(cacheRepository()).saveDocument(argThat(matchDocument(testDocument2)), eq(true));
         verify(cacheRepository()).deleteDocumentById(testDocument1);
         verify(cacheRepository(), never()).deleteDocumentById(testDocument2);
         verify(cacheRepository(), never()).deleteDocumentById(testDocument3);

         // Find document 3 [ø] -> refresh document 3

         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().findDocumentEntryById(testDocument3)).isPresent();

         verify(sourceRepository()).findDocumentById(testDocument3);
         verify(cacheRepository()).saveDocument(argThat(matchDocument(testDocument3)), eq(true));
         verify(cacheRepository()).deleteDocumentById(testDocument1);
         verify(cacheRepository(), never()).deleteDocumentById(testDocument2);
         verify(cacheRepository(), never()).deleteDocumentById(testDocument3);

         // Find document 2 [ø] -> refresh document 2

         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().findDocumentEntryById(testDocument2)).isPresent();

         verify(cacheRepository()).findDocumentById(testDocument2);
         verify(cacheRepository(), never()).saveDocument(any(), eq(true));
         verify(cacheRepository(), never()).deleteDocumentById(any());

         // Find document 1 [maximumSize] -> refresh document 1 -> evict document 3 (evictAll)

         clearInvocations(sourceRepository(), cacheRepository());

         assertThat(documentRepository().findDocumentEntryById(testDocument1)).isPresent();

         verify(sourceRepository()).findDocumentById(testDocument1);
         verify(cacheRepository()).saveDocument(argThat(matchDocument(testDocument1)), eq(true));
         verify(cacheRepository(), never()).deleteDocumentById(testDocument1);
         verify(cacheRepository(), never()).deleteDocumentById(testDocument2);
         verify(cacheRepository()).deleteDocumentById(testDocument3);
      }

   }
}