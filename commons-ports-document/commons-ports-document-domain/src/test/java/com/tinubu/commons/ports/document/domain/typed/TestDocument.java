/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.typed;

import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optionalInstanceOf;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;

import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentPath;

/**
 * Specialized test {@link Document}.
 */
public class TestDocument extends TypedDocument<TestDocument> {

   private static final Charset DEFAULT_CONTENT_ENCODING = UTF_8;
   private static final String LINE_SEPARATOR = "\n";

   private final String nature;

   protected TestDocument(TestDocumentBuilder builder) {
      super(builder);
      this.nature = nullable(builder.nature, "undefined");
   }

   @Override
   @SuppressWarnings("unchecked")
   public Fields<? extends TestDocument> defineDomainFields() {
      return Fields
            .<TestDocument>builder()
            .superFields((Fields<TestDocument>) super.defineDomainFields())
            .field("nature", v -> v.nature, isNotBlank())
            .build();
   }

   /**
    * Creates a new test document from existing document. Source document metadata are not changed.
    *
    * @param document existing document to base on
    *
    * @return new message document
    */
   public static TestDocument of(Document document) {
      notNull(document, "document");

      return new TestDocumentBuilder().reconstitute().copy(document).build();
   }

   public static TestDocument open(DocumentPath documentId,
                                   OutputStream contentOutputStream,
                                   Charset contentEncoding) {
      notNull(documentId, "documentId");

      return new TestDocumentBuilder()
            .open(documentId, contentOutputStream, nullable(contentEncoding, DEFAULT_CONTENT_ENCODING), null)
            .build();
   }

   public static TestDocument open(DocumentPath documentId, Charset contentEncoding) {
      return open(documentId, null, contentEncoding);
   }

   public static TestDocument open(DocumentPath documentId) {
      return open(documentId, null);
   }

   public String nature() {
      return nature;
   }

   @Override
   public TestDocument write(byte[] buffer) {
      return super.write(buffer);
   }

   @Override
   public TestDocument write(InputStream inputStream) {
      return super.write(inputStream);
   }

   @Override
   public TestDocument write(String string) {
      return super.write(string);
   }

   @Override
   public TestDocument writeLn(String string) {
      return super.writeLn(string);
   }

   @Override
   protected String lineSeparator() {
      return LINE_SEPARATOR;
   }

   @Override
   public TestDocumentBuilder documentBuilder() {
      return new TestDocumentBuilder();
   }

   public static TestDocumentBuilder reconstituteBuilder() {
      return new TestDocumentBuilder().reconstitute();
   }

   public static class TestDocumentBuilder extends TypedDocumentBuilder<TestDocument, TestDocumentBuilder> {

      private String nature;

      /**
       * Copy constructor for {@link TestDocument}
       *
       * @param document document to copy
       *
       * @return test document builder
       */
      public static TestDocumentBuilder from(Document document) {
         notNull(document, "document");

         return new TestDocumentBuilder().reconstitute().copy(document);
      }

      @Override
      protected TestDocumentBuilder copy(Document document) {
         return super
               .copy(document)
               .optionalChain(optionalInstanceOf(document, TestDocument.class), (b, d) -> b.nature(d.nature));
      }

      public TestDocumentBuilder nature(String nature) {
         this.nature = nature;
         return this;
      }

      @Override
      public TestDocument buildDomainObject() {
         return new TestDocument(this);
      }

   }
}
