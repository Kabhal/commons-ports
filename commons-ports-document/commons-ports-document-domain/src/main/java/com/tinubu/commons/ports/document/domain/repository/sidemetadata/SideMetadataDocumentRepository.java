/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.repository.sidemetadata;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.lang.util.CollectionUtils.collectionConcat;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.OptionalUtils.peek;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.METADATA_ATTRIBUTES;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.METADATA_CONTENT_ENCODING;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.METADATA_CONTENT_SIZE;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.METADATA_CONTENT_TYPE;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.METADATA_CREATION_DATE;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.METADATA_DOCUMENT_PATH;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.METADATA_LAST_UPDATE_DATE;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.allMetadataCapabilities;

import java.nio.file.Path;
import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.function.Predicate;
import java.util.stream.Stream;

import com.tinubu.commons.ddd2.domain.event.DomainEvent;
import com.tinubu.commons.ddd2.domain.event.DomainEventListener;
import com.tinubu.commons.ddd2.domain.repository.Repository;
import com.tinubu.commons.ddd2.domain.specification.Specification;
import com.tinubu.commons.lang.validation.CheckReturnValue;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentEntry.DocumentEntryBuilder;
import com.tinubu.commons.ports.document.domain.DocumentEntrySpecification;
import com.tinubu.commons.ports.document.domain.DocumentMetadata;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata;
import com.tinubu.commons.ports.document.domain.ReferencedDocument;
import com.tinubu.commons.ports.document.domain.capability.RepositoryCapability;
import com.tinubu.commons.ports.document.domain.event.DocumentRepositoryOperationEvent;
import com.tinubu.commons.ports.document.domain.metrology.DocumentRepositoryMetrology;
import com.tinubu.commons.ports.document.domain.processor.DocumentProcessor;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.ExportUriOptions;
import com.tinubu.commons.ports.document.domain.uri.RepositoryUri;

/**
 * Wrapper repository adding the side-metadata feature to delegated repository.
 * A side-metadata is managed to complement a document's metadata if delegated repository is lacking
 * support for all or some metadata. Underlying repository
 * {@link DocumentRepository#capabilities() capabilities} are used to select metadata to override, even if
 * metadata is present anyway.
 * <p>
 * Depending on {@link SideMetadataStorage} strategy, some side-metadata documents can be created on
 * delegated repository. These side-metadata documents are always filtered out from all operations, you can't
 * search for them, nor save or delete them. Also, no events are sent for them, and metrology ignores them.
 * If you need to take them into account, directly operate on delegated repository.
 * <p>
 * This repository enables all metadata-related capabilities. However, it's possible
 * that side-metadata are not yet existing for a previously existing document, in this case the configured
 * {@link SideMetadataFallback}, or {@link #defaultSideMetadataFallback()}, will be applied.
 */
// FIXME optimization : always store sd, even if all metadata capabilities are supported ? corner cases with existing documents ??? (think about it)
public class SideMetadataDocumentRepository implements DocumentRepository {

   private final DocumentRepository delegate;
   private final SideMetadataStorage sideMetadata;
   private final SideMetadataFallback sideMetadataFallback;

   private final SideMetadataProcessor sideMetadataProcessor = new SideMetadataProcessor();
   private final DocumentEntrySpecification sideMetadataSpecification;

   /**
    * Creates new side-metadata repository wrapping delegating repository.
    *
    * @param delegate delegated repository to wrap
    */
   protected SideMetadataDocumentRepository(DocumentRepository delegate,
                                            SideMetadataStorage sideMetadata,
                                            SideMetadataFallback sideMetadataFallback) {
      this.delegate = delegate;
      this.sideMetadata = sideMetadata;
      this.sideMetadataFallback = normalizeSideMetadataFallback(sideMetadataFallback);
      this.sideMetadataSpecification = sideMetadata.sideMetadataSpecification();
   }

   public static SideMetadataDocumentRepository of(DocumentRepository delegate,
                                                   SideMetadataStorage sideMetadata,
                                                   SideMetadataFallback sideMetadataFallback) {
      return new SideMetadataDocumentRepository(delegate, sideMetadata, sideMetadataFallback);
   }

   public static SideMetadataDocumentRepository of(DocumentRepository delegate,
                                                   SideMetadataStorage sideMetadata) {
      return new SideMetadataDocumentRepository(delegate, sideMetadata, defaultSideMetadataFallback());
   }

   public static SideMetadataDocumentRepository of(DocumentRepository delegate,
                                                   SideMetadataFallback sideMetadataFallback) {
      return new SideMetadataDocumentRepository(delegate,
                                                SameRepositorySideMetadataStorage.of(delegate),
                                                sideMetadataFallback);
   }

   public static SideMetadataDocumentRepository of(DocumentRepository delegate) {
      return of(delegate, defaultSideMetadataFallback());
   }

   private static SideMetadataFallback defaultSideMetadataFallback() {
      return documentEntry -> {
         return new DocumentMetadataBuilder()
               .<DocumentMetadataBuilder>reconstitute()
               .documentPath(documentEntry.documentId().value())
               .creationDate(Instant.EPOCH)
               .lastUpdateDate(Instant.EPOCH)
               .build();
      };
   }

   private SideMetadataFallback normalizeSideMetadataFallback(SideMetadataFallback sideMetadataFallback) {
      return documentEntry -> enhanceDocumentMetadata(documentEntry,
                                                      sideMetadataFallback.apply(documentEntry));
   }

   public SideMetadataStorage sideMetadata() {
      return sideMetadata;
   }

   public DocumentRepository delegate() {
      return delegate;
   }

   /**
    * Clean orphans side-metadata documents.
    * All side-metadata documents, associated to a document that no longer exists, are cleaned
    * depending on {@link SideMetadataStorage} implementation.
    */
   public void cleanOrphanSideMetadata() {
      sideMetadata.cleanOrphanSideMetadata();
   }

   /**
    * Caution : Clean all side-metadata documents.
    */
   public void cleanAllSideMetadata() {
      sideMetadata.cleanAllSideMetadata();
   }

   @Override
   public HashSet<RepositoryCapability> capabilities() {
      return collectionConcat(HashSet::new, delegate.capabilities(), allMetadataCapabilities());
   }

   @Override
   public boolean hasCapability(RepositoryCapability capability) {
      return delegate.hasCapability(capability);
   }

   @Override
   public SideMetadataDocumentRepository subPath(Path subPath, boolean shareContext) {
      DocumentRepository subPathDelegate = delegate.subPath(subPath, shareContext);

      return new SideMetadataDocumentRepository(subPathDelegate,
                                                SameRepositorySideMetadataStorage.of(subPathDelegate),
                                                sideMetadataFallback);
   }

   @Override
   @CheckReturnValue
   public Optional<Document> openDocument(DocumentPath documentId,
                                          boolean overwrite,
                                          boolean append,
                                          OpenDocumentMetadata metadata) {
      if (documentId != null && sideMetadata.isSideMetadata(documentId)) {
         return optional();
      }

      return peek(delegate.openDocument(documentId, overwrite, append, metadata), document -> {
         sideMetadata.saveMetadata(document.documentEntry());
      });
   }

   @Override
   @CheckReturnValue
   public Optional<Document> openDocument(DocumentPath documentId, boolean overwrite, boolean append) {
      if (documentId != null && sideMetadata.isSideMetadata(documentId)) {
         return optional();
      }

      return peek(delegate.openDocument(documentId, overwrite, append), document -> {
         sideMetadata.saveMetadata(document.documentEntry());
      });
   }

   @Override
   @CheckReturnValue
   public Optional<Document> openDocument(DocumentPath documentId, boolean overwrite) {
      if (documentId != null && sideMetadata.isSideMetadata(documentId)) {
         return optional();
      }

      return peek(delegate.openDocument(documentId, overwrite), document -> {
         sideMetadata.saveMetadata(document.documentEntry());
      });
   }

   @Override
   public Optional<Document> findDocumentById(DocumentPath documentId) {
      return delegate
            .findDocumentById(documentId)
            .filter(excludeSideMetadataDocument(null))
            .map(this::processDocument);
   }

   @Override
   public Stream<Document> findDocumentsBySpecification(Path basePath,
                                                        Specification<DocumentEntry> specification) {
      return delegate
            .findDocumentsBySpecification(basePath, excludeSideMetadata(specification))
            .map(this::processDocument);
   }

   @Override
   public Stream<Document> findDocumentsBySpecification(Specification<DocumentEntry> specification) {
      return delegate
            .findDocumentsBySpecification(excludeSideMetadata(specification))
            .map(this::processDocument);
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryById(DocumentPath documentId) {
      return delegate
            .findDocumentEntryById(documentId)
            .filter(excludeSideMetadata(null))
            .map(this::processDocumentEntry);
   }

   @Override
   public Stream<DocumentEntry> findDocumentEntriesBySpecification(Path basePath,
                                                                   Specification<DocumentEntry> specification) {
      return delegate.findDocumentEntriesBySpecification(basePath, excludeSideMetadata(specification))
            .map(this::processDocumentEntry);
   }

   @Override
   public Stream<DocumentEntry> findDocumentEntriesBySpecification(Specification<DocumentEntry> specification) {
      return delegate
            .findDocumentEntriesBySpecification(excludeSideMetadata(specification))
            .map(this::processDocumentEntry);
   }

   /**
    * @implNote Saved side metadata is pre-saved metadata to ensure no metadata is lost during save on
    *       a low-capability delegated repository.
    */
   @Override
   @CheckReturnValue
   public Optional<DocumentEntry> saveDocument(Document document, boolean overwrite) {
      if (document != null && sideMetadata.isSideMetadata(document.documentId())) {
         return optional();
      }

      return peek(delegate.saveDocument(document, overwrite),
                  documentEntry -> sideMetadata.saveMetadata(document.documentEntry()));
   }

   /**
    * @implNote Saved side metadata is pre-saved metadata to ensure no metadata is lost during save on
    *       a low-capability delegated repository.
    */
   @Override
   @CheckReturnValue
   public Optional<Document> saveAndReturnDocument(Document document, boolean overwrite) {
      if (document != null && sideMetadata.isSideMetadata(document.documentId())) {
         return optional();
      }

      return peek(delegate.saveAndReturnDocument(document, overwrite),
                  savedDocument -> sideMetadata.saveMetadata(document.documentEntry()));
   }

   @Override
   @CheckReturnValue
   public Optional<DocumentEntry> deleteDocumentById(DocumentPath documentId) {
      if (documentId != null && sideMetadata.isSideMetadata(documentId)) {
         return optional();
      }

      return peek(delegate.deleteDocumentById(documentId).map(this::processDocumentEntry),
                  documentEntry -> sideMetadata.deleteMetadata(documentEntry.documentId()));
   }

   @Override
   @CheckReturnValue
   public List<DocumentEntry> deleteDocumentsBySpecification(Path basePath,
                                                             Specification<DocumentEntry> specification) {
      List<DocumentEntry> deleteDocuments =
            delegate.deleteDocumentsBySpecification(basePath, excludeSideMetadata(specification));

      deleteDocuments.forEach(documentEntry -> {
         processDocumentEntry(documentEntry);
         sideMetadata.deleteMetadata(documentEntry.documentId());
      });

      return deleteDocuments;
   }

   @Override
   public <E extends DomainEvent> void registerEventListener(DomainEventListener<? super E> eventListener,
                                                             Class<? extends E>... eventClasses) {
      delegate.registerEventListener(eventListener.filteredListener(excludeSideMetadataEvent()),
                                     eventClasses);
   }

   @Override
   public void registerWriteEventsListener(DomainEventListener<? super DomainEvent> eventListener) {
      delegate.registerWriteEventsListener(eventListener);
   }

   @Override
   public void registerAccessEventsListener(DomainEventListener<? super DomainEvent> eventListener) {
      delegate.registerAccessEventsListener(eventListener);
   }

   @Override
   public <E extends DomainEvent> void registerEventLoggingListener(Class<? extends E>... eventClasses) {
      delegate.registerEventLoggingListener(eventClasses);
   }

   @Override
   public void registerWriteEventsLoggingListener() { delegate.registerWriteEventsLoggingListener(); }

   @Override
   public void registerAccessEventsLoggingListener() { delegate.registerAccessEventsLoggingListener(); }

   @Override
   public <T extends DocumentRepositoryMetrology> void registerMetrology(T metrology) {
      delegate.registerMetrology(metrology);
   }

   @Override
   public void unregisterEventListeners() { delegate.unregisterEventListeners(); }

   @Override
   public void close() {
      delegate.close();
   }

   @Override
   public Stream<Document> findBySpecification(Specification<Document> specification) {
      return delegate
            .findBySpecification(excludeSideMetadataDocument(specification))
            .map(this::processDocument);
   }

   @Override
   public Stream<Document> findAll() {
      return delegate.findAll().map(this::processDocument);
   }

   @Override
   public Optional<Document> findById(DocumentPath documentId) {
      return delegate.findById(documentId).map(this::processDocument);
   }

   @Override
   @CheckReturnValue
   public Optional<Document> save(Document document) {
      return delegate.save(document).map(d -> {
         sideMetadata.saveMetadata(d.documentEntry());
         return d;
      });
   }

   @Override
   @CheckReturnValue
   public Optional<Document> delete(DocumentPath documentId) {
      return delegate.delete(documentId).map(document -> {
         sideMetadata.deleteMetadata(document.documentId());
         return document;
      });
   }

   @Override
   public boolean sameRepositoryAs(Repository<Document, DocumentPath> documentRepository) {
      validate(documentRepository, "documentRepository", isNotNull()).orThrow();

      Repository<Document, DocumentPath> comparingRepository = documentRepository;

      if (documentRepository instanceof SideMetadataDocumentRepository) {
         comparingRepository = ((SideMetadataDocumentRepository) documentRepository).delegate;
      }

      return delegate.sameRepositoryAs(comparingRepository);
   }

   @Override
   public ReferencedDocument<? extends DocumentRepository> referencedDocumentId(DocumentUri documentUri) {
      return delegate.referencedDocumentId(documentUri);
   }

   @Override
   public RepositoryUri toUri(ExportUriOptions options) { return delegate.toUri(options); }

   @Override
   public DocumentUri toUri(DocumentPath documentId, ExportUriOptions options) {
      return delegate.toUri(documentId, options);
   }

   @Override
   public boolean supportsUri(RepositoryUri uri) { return delegate.supportsUri(uri); }

   @Override
   public Optional<Document> findDocumentByUri(DocumentUri documentUri) {
      return delegate.findDocumentByUri(documentUri).map(this::processDocument);
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryByUri(DocumentUri documentUri) {
      return delegate.findDocumentEntryByUri(documentUri).map(this::processDocumentEntry);
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", SideMetadataDocumentRepository.class.getSimpleName() + "[", "]")
            .add("delegate=" + delegate)
            .toString();
   }

   private Predicate<DomainEvent> excludeSideMetadataEvent() {
      return event -> {
         if (event instanceof DocumentRepositoryOperationEvent) {
            return !sideMetadata.isSideMetadata(((DocumentRepositoryOperationEvent) event).documentId());
         } else {
            return true;
         }
      };
   }

   /**
    * Generates a new {@link DocumentEntrySpecification} that excludes side metadata documents, composed with
    * original specification.
    *
    * @param specification original specification, or {@code null}
    *
    * @return composed document entry specification
    */
   private DocumentEntrySpecification excludeSideMetadata(Predicate<DocumentEntry> specification) {
      if (specification == null) {
         return sideMetadataSpecification.not();
      } else if (specification instanceof DocumentEntrySpecification) {
         return ((DocumentEntrySpecification) specification).and(sideMetadataSpecification.not());
      } else {
         return documentEntry -> (specification.and(sideMetadataSpecification.not())).test(documentEntry);
      }
   }

   /**
    * Generates a new {@link Specification} of {@link Document} that excludes side metadata documents,
    * composed with original specification.
    *
    * @param specification original specification, or {@code null}
    *
    * @return composed document specification
    */
   private Specification<Document> excludeSideMetadataDocument(Predicate<Document> specification) {
      if (specification == null) {
         return sideMetadataSpecification.not().documentSpecification();
      } else
         return document -> (specification.and(sideMetadataSpecification.not().documentSpecification())).test(
               document);
   }

   /**
    * Returns enhanced document metadata for specified document entry.
    * For each metadata field, side metadata always overrides source metadata if delegate repository has not
    * the corresponding capability.
    *
    * @param documentEntry document entry used to identify document and as source metadata
    *
    * @return enhanced document metadata
    */
   protected Optional<DocumentMetadata> enhanceDocumentMetadata(DocumentEntry documentEntry) {
      return sideMetadata.loadMetadata(documentEntry.documentId()).map(sm -> {
         return enhanceDocumentMetadata(documentEntry, sm);
      }).or(() -> nullable(sideMetadataFallback).map(f -> f.apply(documentEntry)));
   }

   protected DocumentMetadata enhanceDocumentMetadata(DocumentEntry documentEntry,
                                                      DocumentMetadata metadata) {
      DocumentMetadataBuilder builder = DocumentMetadataBuilder.from(documentEntry.metadata());

      if (!delegate.hasCapability(METADATA_DOCUMENT_PATH)) {
         builder.documentPath(metadata.documentPath());
      }
      if (!delegate.hasCapability(METADATA_CONTENT_TYPE)) {
         metadata.simpleContentType().ifPresent(builder::contentType);
      }
      if (!delegate.hasCapability(METADATA_CONTENT_ENCODING)) {
         metadata.contentEncoding().ifPresent(builder::contentType);
      }
      if (!delegate.hasCapability(METADATA_CONTENT_SIZE)) {
         metadata.contentSize().ifPresent(builder::contentSize);
      }
      if (!delegate.hasCapability(METADATA_CREATION_DATE)) {
         metadata.creationDate().ifPresent(builder::creationDate);
      }
      if (!delegate.hasCapability(METADATA_LAST_UPDATE_DATE)) {
         metadata.lastUpdateDate().ifPresent(builder::lastUpdateDate);
      }
      if (!delegate.hasCapability(METADATA_ATTRIBUTES)) {
         builder.attributes(metadata.attributes());
      }

      return builder.build();
   }

   /**
    * Process document by enhancing metadata with side metadata.
    *
    * @param document document to enhance
    *
    * @return enhanced document
    *
    * @see #processDocumentEntry(DocumentEntry)
    */
   protected Document processDocument(Document document) {
      return document.process(sideMetadataProcessor);
   }

   /**
    * Process document entry by enhancing metadata with side metadata.
    *
    * @param documentEntry document entry to enhance
    *
    * @return enhanced document entry
    *
    * @see #processDocument(Document)
    */
   protected DocumentEntry processDocumentEntry(DocumentEntry documentEntry) {
      validate(documentEntry, "documentEntry", isNotNull()).orThrow();

      return enhanceDocumentMetadata(documentEntry)
            .map(metadata -> DocumentEntryBuilder.from(documentEntry).metadata(metadata).build())
            .orElse(documentEntry);
   }

   protected class SideMetadataProcessor implements DocumentProcessor {

      @Override
      public Document process(Document document) {
         validate(document, "document", isNotNull()).orThrow();

         return enhanceDocumentMetadata(document.documentEntry())
               .map(metadata -> DocumentBuilder.from(document).metadata(metadata).build())
               .orElse(document);

      }

   }

}