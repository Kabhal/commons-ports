/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.uri;

import static com.tinubu.commons.ddd2.domain.type.support.DomainObjectSupport.checkInvariants;

import java.net.URI;

import com.tinubu.commons.ddd2.domain.type.Id;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.uri.Uri;

/**
 * URI identifying a document in a repository.
 * <p>
 * See {@link DefaultRepositoryUri} to only identify a repository.
 *
 * @implSpec It is assumed that document URI is not independent of its repository URI, so that a
 *       document URI must always been also a repository URI (in other words contains the repository URI).
 * @see DefaultRepositoryUri
 */
public class DefaultDocumentUri extends DefaultRepositoryUri implements DocumentUri {

   public DefaultDocumentUri(Uri uri) {
      super(uri);
   }

   /**
    * Creates a document URI from URI string.
    *
    * @param uri document URI
    *
    * @return new instance
    */
   static DefaultDocumentUri ofDocumentUri(Uri uri) {
      Check.notNull(uri, "uri");

      return checkInvariants(new DefaultDocumentUri(uri));
   }

   /**
    * Creates a document URI from URI string.
    *
    * @param uri document URI
    *
    * @return new instance
    */
   static DefaultDocumentUri ofDocumentUri(URI uri) {
      Check.notNull(uri, "uri");

      return ofDocumentUri(Uri.ofUri(uri));
   }

   /**
    * Creates a document URI from URI string.
    * URI syntax is checked as part of this operation.
    *
    * @param uri document URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI has invalid syntax
    */
   static DefaultDocumentUri ofDocumentUri(String uri) {
      Check.notNull(uri, "uri");

      return ofDocumentUri(Uri.ofUri(uri));
   }

   /**
    * {@inheritDoc}
    *
    * @implSpec Covariant override.
    */
   @Override
   public DefaultDocumentUri normalize() {
      return ofDocumentUri(super.normalize());
   }

   /**
    * {@inheritDoc}
    *
    * @implSpec Covariant override.
    */
   @Override
   public DefaultDocumentUri resolve(Uri uri) {
      return ofDocumentUri(super.resolve(uri));
   }

   /**
    * {@inheritDoc}
    *
    * @implSpec Covariant override.
    */
   @Override
   public DefaultDocumentUri resolve(URI uri) {
      return ofDocumentUri(super.resolve(uri));
   }

   /**
    * {@inheritDoc}
    *
    * @implSpec Covariant override.
    */
   @Override
   public DefaultDocumentUri relativize(Uri uri) {
      return ofDocumentUri(super.relativize(uri));
   }

   /**
    * {@inheritDoc}
    *
    * @implSpec Covariant override.
    */
   @Override
   public DefaultDocumentUri relativize(URI uri) {
      return ofDocumentUri(super.relativize(uri));
   }

   @Override
   public int compareTo(Id o) {
      if (!(o instanceof DefaultDocumentUri)) {
         return -1;
      } else if (o == this) {
         return 0;
      } else {
         return uri.compareTo(((DefaultDocumentUri) o).uri);
      }
   }

}