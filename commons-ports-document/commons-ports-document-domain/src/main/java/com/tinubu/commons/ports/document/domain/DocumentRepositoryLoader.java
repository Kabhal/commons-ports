/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import java.util.List;
import java.util.Optional;
import java.util.ServiceLoader;

import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.RepositoryUri;

/**
 * Dynamically loads {@link DocumentRepository} for {@link ServiceLoader}.
 * <p>
 * All generated returned repositories must be closed externally after use.
 */
public interface DocumentRepositoryLoader {

   /**
    * Loads a document repository from a parameterized repository URI.
    *
    * @param uri URI to use to load the repository, can be a {@link RepositoryUri} or a
    *       {@link DocumentUri}
    * @param loaders complete list of available loaders
    *
    * @return new document repository, or {@link Optional#empty} if URI is not compatible with repository
    *
    * @see DocumentRepositoryFactory#documentRepository(RepositoryUri)
    * @see DocumentRepositoryFactory#documentRepository(RepositoryUri, List)
    */
   Optional<? extends DocumentRepository> load(RepositoryUri uri, List<DocumentRepositoryLoader> loaders);

   /**
    * Loads a document repository from a parameterized document URI.
    *
    * @param documentUri document URI
    * @param loaders complete list of available loaders
    *
    * @return new document repository, or {@link Optional#empty} if URI is not compatible with repository
    *
    * @see DocumentFactory#document(DocumentUri)
    * @see DocumentFactory#document(DocumentUri, List)
    */
   Optional<? extends ReferencedDocument<? extends DocumentRepository>> loadReferencedDocument(DocumentUri documentUri,
                                                                                               List<DocumentRepositoryLoader> loaders);

}
