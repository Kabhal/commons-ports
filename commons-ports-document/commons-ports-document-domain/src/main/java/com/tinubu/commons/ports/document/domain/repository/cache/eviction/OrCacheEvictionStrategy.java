/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.repository.cache.eviction;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.hasNoNullElements;
import static com.tinubu.commons.lang.util.CollectionUtils.list;

import java.nio.file.Path;
import java.util.List;
import java.util.StringJoiner;
import java.util.function.Consumer;

import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentPath;

/**
 * Composed cache eviction strategy.
 * Documents will be evicted if any composed strategy considers the document as evictable.
 */
public class OrCacheEvictionStrategy extends AbstractCacheEvictionStrategy {

   protected final List<CacheEvictionStrategy> evictionStrategies;

   private OrCacheEvictionStrategy(List<CacheEvictionStrategy> evictionStrategies) {
      this.evictionStrategies =
            validate(evictionStrategies, "evictionStrategies", hasNoNullElements()).orThrow();
   }

   public static OrCacheEvictionStrategy of(List<CacheEvictionStrategy> evictionStrategies) {
      return new OrCacheEvictionStrategy(evictionStrategies);
   }

   public static OrCacheEvictionStrategy of(CacheEvictionStrategy... evictionStrategies) {
      return new OrCacheEvictionStrategy(list(evictionStrategies));
   }

   @Override
   public boolean evictables(Consumer<DocumentPath> callback) {
      return evictionStrategies
            .stream()
            .reduce(false, (e, s) -> e || s.evictables(callback), Boolean::logicalOr);
   }

   @Override
   public boolean evictable(DocumentPath documentId, Consumer<DocumentPath> callback) {
      return evictionStrategies.stream().anyMatch(s -> s.evictable(documentId, callback));
   }

   @Override
   public void updateCacheContext(DocumentEntry documentEntry, ContextOperation operation) {
      evictionStrategies.forEach(s -> s.updateCacheContext(documentEntry, operation));
   }

   @Override
   public OrCacheEvictionStrategy subPathCacheContext(Path subPath) {
      return new OrCacheEvictionStrategy(list(evictionStrategies
                                                    .stream()
                                                    .map(s -> s.subPathCacheContext(subPath))));
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", OrCacheEvictionStrategy.class.getSimpleName() + "[", "]")
            .add("evictionStrategies=" + evictionStrategies)
            .toString();
   }
}
