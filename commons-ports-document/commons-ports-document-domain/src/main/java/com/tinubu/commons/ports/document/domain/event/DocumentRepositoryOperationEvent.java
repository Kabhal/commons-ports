/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.event;

import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;

import java.net.URI;
import java.time.Duration;
import java.util.Optional;

import com.tinubu.commons.ddd2.domain.event.AbstractDomainEventValue;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.capability.RepositoryCapability;

/**
 * Base implementation for repository events which are the result of a repository operation/command.
 * These events are concerning a specific document, identified in the event, and have a duration.
 * If the repository has not the {@link RepositoryCapability#DOCUMENT_URI} capability, document URI is not
 * set.
 */
public abstract class DocumentRepositoryOperationEvent extends AbstractDomainEventValue
      implements DocumentRepositoryEvent {

   protected final DocumentRepository documentRepository;
   protected final DocumentPath documentId;
   protected final URI documentUri;
   protected final Duration duration;

   public DocumentRepositoryOperationEvent(DocumentRepository repository,
                                           DocumentPath documentId,
                                           URI documentUri,
                                           Duration duration) {
      super(repository);
      this.documentRepository = repository;
      this.documentId = documentId;
      this.documentUri = documentUri;
      this.duration = duration;
   }

   public DocumentRepositoryOperationEvent(DocumentRepository repository,
                                           DocumentPath documentId,
                                           URI documentUri) {
      this(repository, documentId, documentUri, Duration.ZERO);
   }

   @Override
   @SuppressWarnings("unchecked")
   protected Fields<? extends DocumentRepositoryOperationEvent> defineDomainFields() {
      return Fields
            .<DocumentRepositoryOperationEvent>builder()
            .superFields((Fields<DocumentRepositoryOperationEvent>) super.defineDomainFields())
            .field("documentId", v -> v.documentId, isNotNull())
            .field("documentUri", v -> v.documentUri)
            .field("duration", v -> v.duration, isNotNull())
            .build();
   }

   @Override
   public DocumentRepository documentRepository() {
      return documentRepository;
   }

   /**
    * Document identifier for which this event has been sent.
    *
    * @return document identifier
    */
   @Getter
   public DocumentPath documentId() {
      return documentId;
   }

   /**
    * Optional document URI for which this event has been sent.
    *
    * @return document URI
    */
   @Getter
   public Optional<URI> documentUri() {
      return nullable(documentUri);
   }

   /**
    * Duration of the operation for which this event has been sent.
    *
    * @return operation duration
    */
   @Getter
   public Duration duration() {
      return duration;
   }

   /**
    * Repository class name.
    *
    * @return repository class name
    */
   protected String repositoryName() {
      return documentRepository.getClass().getSimpleName();
   }

   /**
    * Generates a document identity for display purpose, with best-effort.
    *
    * @return document representation
    */
   protected String documentDisplayId() {
      return nullable(documentUri)
            .map(URI::toString)
            .orElseGet(() -> documentRepository.documentDisplayId(documentId));
   }
}
