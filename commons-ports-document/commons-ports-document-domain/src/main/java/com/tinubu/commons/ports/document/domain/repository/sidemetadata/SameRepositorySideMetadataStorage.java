/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.repository.sidemetadata;

import static com.tinubu.commons.ddd2.domain.type.support.DomainObjectSupport.checkInvariants;
import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObject;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.notSatisfiesValue;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.lang.validation.Validate.ignore;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.ports.document.domain.DocumentEntryCriteria.DocumentEntryCriteriaBuilder;
import static com.tinubu.commons.ports.document.domain.rules.DocumentRules.DocumentEntryRules.documentId;
import static java.util.Objects.requireNonNull;
import static org.apache.commons.lang3.StringUtils.removeEnd;
import static org.apache.commons.lang3.StringUtils.removeStart;

import java.util.Optional;

import com.tinubu.commons.ddd2.criterion.Criterion.CriterionBuilder;
import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.formatter.FastStringFormat;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentEntryCriteria;
import com.tinubu.commons.ports.document.domain.DocumentEntrySpecification;
import com.tinubu.commons.ports.document.domain.DocumentMetadata;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata.OpenDocumentMetadataBuilder;

/**
 * Side metadata storage strategy reusing original document repository to store metadata as dedicated
 * {@link SideMetadataDocument} documents.
 */
public class SameRepositorySideMetadataStorage extends AbstractValue implements SideMetadataStorage {

   private static final String SIDE_METADATA_EXTENSION = "metadata";

   private final DocumentRepository repository;

   private final DocumentSideMetadataLocationStrategy locationStrategy;

   protected SameRepositorySideMetadataStorage(DocumentRepository repository,
                                               DocumentSideMetadataLocationStrategy locationStrategy) {
      this.repository = repository;
      this.locationStrategy = locationStrategy;
   }

   public static SameRepositorySideMetadataStorage of(DocumentRepository repository) {
      return checkInvariants(new SameRepositorySideMetadataStorage(repository, defaultLocationStrategy()));
   }

   public static SameRepositorySideMetadataStorage of(DocumentRepository repository,
                                                      DocumentSideMetadataLocationStrategy locationStrategy) {
      return checkInvariants(new SameRepositorySideMetadataStorage(repository, locationStrategy));
   }

   @Override
   public Fields<? extends SameRepositorySideMetadataStorage> defineDomainFields() {
      return Fields.<SameRepositorySideMetadataStorage>builder()
            .field("repository", v -> v.repository, isNotNull())
            .field("locationStrategy", v -> v.locationStrategy, isNotNull())
            .build();
   }

   @Override
   public boolean isSideMetadata(DocumentPath documentId) {
      notNull(documentId, "documentId");

      return locationStrategy.isSideMetadata(documentId);
   }

   @Override
   public DocumentEntrySpecification sideMetadataSpecification() {
      return requireNonNull(locationStrategy.sideMetadataSpecification());
   }

   @Override
   public void cleanOrphanSideMetadata() {
      repository
            .findDocumentEntriesBySpecification(requireNonNull(locationStrategy.sideMetadataSpecification()))
            .forEach(sideMetadata -> {
               DocumentPath sideMetadataDocumentId = sideMetadata.documentId();
               DocumentPath originalDocument =
                     requireNonNull(locationStrategy.originalLocation(sideMetadataDocumentId));

               if (repository.findDocumentById(originalDocument).isEmpty()) {
                  ignore(repository.deleteDocumentById(sideMetadataDocumentId));
               }
            });
   }

   @Override
   public void cleanAllSideMetadata() {
      ignore(repository.deleteDocumentsBySpecification(requireNonNull(locationStrategy.sideMetadataSpecification())));
   }

   /**
    * Load specified document's side-metadata.
    *
    * @param documentId original document identifier to load side-metadata for
    *
    * @return side metadata or {@link Optional#empty} if no side-metadata found
    *
    * @implNote Site metadata content-type must be forced or we have a chicken-and-egg problem with
    *       the underlying repository.
    */
   @Override
   public Optional<DocumentMetadata> loadMetadata(DocumentPath documentId) {
      validate(documentId, "documentId", isNotSideMetadata()).orThrow();

      DocumentPath sideMetadataDocumentId = requireNonNull(locationStrategy.sideMetadataLocation(documentId));

      return repository.findDocumentById(sideMetadataDocumentId).map(document -> {
         SideMetadataDocument sideMetadata = SideMetadataDocument.of(document, true);
         return sideMetadata.read(documentId);
      });
   }

   @Override
   public void saveMetadata(DocumentEntry documentEntry) {
      validate(documentEntry,
               "documentEntry",
               isNotNull().andValue(documentId(isNotSideMetadata()))).orThrow();

      DocumentPath sideMetadataDocumentId =
            requireNonNull(locationStrategy.sideMetadataLocation(documentEntry.documentId()));

      Document document = repository
            .openDocument(sideMetadataDocumentId,
                          true,
                          false,
                          new OpenDocumentMetadataBuilder()
                                .contentType(SideMetadataDocument.CONTENT_TYPE)
                                .build())
            .orElseThrow(IllegalStateException::new);
      SideMetadataDocument sideMetadata = SideMetadataDocument.of(document, true);
      sideMetadata.write(documentEntry);
   }

   @Override
   public void deleteMetadata(DocumentPath documentId) {
      validate(documentId, "documentId", isNotSideMetadata()).orThrow();

      DocumentPath sideMetadataDocumentId = requireNonNull(locationStrategy.sideMetadataLocation(documentId));

      ignore(repository.deleteDocumentById(sideMetadataDocumentId));
   }

   private InvariantRule<DocumentPath> isNotSideMetadata() {
      return isNotNull().andValue(notSatisfiesValue(this::isSideMetadata,
                                                    FastStringFormat.of("'",
                                                                        validatingObject(),
                                                                        "' must not be a direct side-metadata document")).ruleContext(
            "SameRepositorySideMetadataStorage.isNotSideMetadata"));
   }

   public static DocumentSideMetadataLocationStrategy defaultLocationStrategy() {
      return new DocumentSideMetadataLocationStrategy() {

         @Override
         public DocumentPath sideMetadataLocation(DocumentPath documentId) {
            return documentId.name("." + documentId.name() + "." + SIDE_METADATA_EXTENSION);
         }

         @Override
         public DocumentPath originalLocation(DocumentPath sideMetadataDocumentId) {
            return sideMetadataDocumentId.name(removeEnd(removeStart(sideMetadataDocumentId.name(), "."),
                                                         "." + SIDE_METADATA_EXTENSION));
         }

         @Override
         public boolean isSideMetadata(DocumentPath documentId) {
            return documentId.hidden() && documentId.hasExtension(SIDE_METADATA_EXTENSION);
         }

         @Override
         public DocumentEntryCriteria sideMetadataSpecification() {
            return new DocumentEntryCriteriaBuilder()
                  .documentName(CriterionBuilder.startWith("."))
                  .documentExtension(CriterionBuilder.equal(SIDE_METADATA_EXTENSION))
                  .build();
         }
      };
   }

   /**
    * Strategy to associate original document name with side-metadata document name.
    * Both documents will be stored in the same repository.
    */
   public interface DocumentSideMetadataLocationStrategy {

      /**
       * Generates the side-metadata document path from original document path.
       *
       * @param documentId original document path
       *
       * @return side-metadata document path
       */
      DocumentPath sideMetadataLocation(DocumentPath documentId);

      /**
       * Generates the original document path from side-metadata document path.
       *
       * @param sideMetadataDocumentId side-metadata document path
       *
       * @return original document path
       */
      DocumentPath originalLocation(DocumentPath sideMetadataDocumentId);

      /**
       * Checks whether specified document path is a side-metadata document.
       *
       * @param documentId document path to check
       *
       * @return {@code true} if document path is a side-metadata document
       */
      boolean isSideMetadata(DocumentPath documentId);

      /**
       * Returns specification that matches side-metadata documents.
       *
       * @return specification that matches side-metadata documents
       */
      DocumentEntrySpecification sideMetadataSpecification();

   }

}
