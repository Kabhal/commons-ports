/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.repository.cache.event;

import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;

import java.time.Duration;

import com.tinubu.commons.ddd2.domain.event.AbstractDomainEventValue;
import com.tinubu.commons.ddd2.domain.event.DomainEvent;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.repository.cache.CacheDocumentRepository;

/**
 * Base event for all cache documents related events.
 */
public abstract class CacheDocumentEvent extends AbstractDomainEventValue implements DomainEvent {

   protected final DocumentPath documentId;
   protected final Duration duration;

   /**
    * Creates a cache document event.
    *
    * @param documentRepository {@link CacheDocumentRepository} source repository
    * @param documentId document relating to event
    * @param duration underlying cache operation duration
    */
   public CacheDocumentEvent(DocumentRepository documentRepository,
                             DocumentPath documentId,
                             Duration duration) {
      super(documentRepository);
      this.documentId = documentId;
      this.duration = duration;
   }

   public CacheDocumentEvent(DocumentRepository documentRepository, DocumentPath documentId) {
      this(documentRepository, documentId, Duration.ZERO);
   }

   @Override
   @SuppressWarnings("unchecked")
   protected Fields<? extends CacheDocumentEvent> defineDomainFields() {
      return Fields
            .<CacheDocumentEvent>builder()
            .superFields((Fields<CacheDocumentEvent>) super.defineDomainFields())
            .field("documentId", v -> v.documentId, isNotNull())
            .field("duration", v -> v.duration, isNotNull())
            .build();
   }

   /**
    * Document identifier for which this event has been sent.
    *
    * @return document identifier
    */
   @Getter
   public DocumentPath documentId() {
      return documentId;
   }

   /**
    * Duration of the operation for which this event has been sent.
    *
    * @return operation duration
    */
   @Getter
   public Duration duration() {
      return duration;
   }

}
