/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.repository.cache.eviction;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.ports.document.domain.repository.cache.eviction.CacheEvictionStrategy.ContextOperation.DELETE;
import static java.util.Collections.synchronizedMap;

import java.nio.file.Path;
import java.time.Duration;
import java.time.Instant;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringJoiner;
import java.util.function.Consumer;

import com.tinubu.commons.lang.datetime.ApplicationClock;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentPath;

/**
 * Default cache eviction strategy supporting :
 * <ul>
 *    <li>expireAfterWrite : maximum duration after cached document is created</li>
 *    <li>expireAfterAccess : maximum duration after cached document is accessed</li>
 *    <li>maximumSize : maximum cache size</li>
 * </ul>
 */
public class DefaultCacheEvictionStrategy extends AbstractCacheEvictionStrategy {

   private final Map<DocumentPath, CacheContext> context;

   private final Duration expireAfterWrite;
   private final Duration expireAfterAccess;
   private final Integer maximumSize;

   private DefaultCacheEvictionStrategy(Duration expireAfterWrite,
                                        Duration expireAfterAccess,
                                        Integer maximumSize,
                                        Map<DocumentPath, CacheContext> context) {
      this.expireAfterWrite = expireAfterWrite;
      this.expireAfterAccess = expireAfterAccess;
      this.maximumSize = maximumSize;
      this.context = nullable(context, synchronizedMap(new CacheContextMap(maximumSize)));
   }

   public static DefaultCacheEvictionStrategy expireAfterWrite(Duration expireAfterWrite) {
      notNull(expireAfterWrite, "expireAfterWrite");

      return new DefaultCacheEvictionStrategy(expireAfterWrite, null, null, null);
   }

   public static DefaultCacheEvictionStrategy expireAfterAccess(Duration expireAfterAccess) {
      notNull(expireAfterAccess, "expireAfterAccess");

      return new DefaultCacheEvictionStrategy(null, expireAfterAccess, null, null);
   }

   public static DefaultCacheEvictionStrategy neverExpire() {
      return new DefaultCacheEvictionStrategy(null, null, null, null);
   }

   public DefaultCacheEvictionStrategy maximumSize(int maximumSize) {
      return new DefaultCacheEvictionStrategy(expireAfterWrite, expireAfterAccess, maximumSize, context);
   }

   /**
    * {@inheritDoc}
    *
    * @implNote Context elements that are related to maximumSize and expireAfterAccess are stacked at
    *       the beginning of the LRU context collection, but not those related to expireAfterWrite, so we must
    *       walk through the whole collection.
    */
   @Override
   public boolean evictables(Consumer<DocumentPath> callback) {
      boolean evict = false;
      for (Iterator<Entry<DocumentPath, CacheContext>> iterator = context.entrySet().iterator();
           iterator.hasNext(); ) {
         Entry<DocumentPath, CacheContext> next = iterator.next();
         DocumentPath documentId = next.getKey();

         if ((maximumSize != null && context.size() > maximumSize) || evictable(next.getValue())) {
            evict = true;
            try {
               if (callback != null) {
                  callback.accept(documentId);
               }
               iterator.remove();
            } catch (Exception ignored) {
            }
         }
      }

      return evict;
   }

   /**
    * @implNote In case of inconsistency between the cache repository and the eviction strategy
    *       context, context in {@link ContextOperation#ACCESS} is created on the fly. An alternative would be
    *       to always evict in this case.
    */
   @Override
   public boolean evictable(DocumentPath documentId, Consumer<DocumentPath> callback) {
      notNull(documentId, "documentId");

      boolean evictable = evictable(this.context.get(documentId));

      if (evictable) {
         try {
            if (callback != null) {
               callback.accept(documentId);
            }
            this.context.remove(documentId);
         } catch (Exception ignored) {
         }
      }

      return evictable;
   }

   private boolean evictable(CacheContext context) {
      boolean evictable;
      if (context == null) {
         evictable = true;
      } else {
         Instant now = ApplicationClock.nowAsInstant();

         evictable = false;
         evictable |= nullable(expireAfterWrite).map(eaw -> context.lastWrite().plus(eaw).isBefore(now))
               .orElse(false);
         evictable |= nullable(expireAfterAccess).map(eaa -> context.lastAccess().plus(eaa).isBefore(now))
               .orElse(false);
      }

      return evictable;
   }

   @Override
   public void updateCacheContext(DocumentEntry documentEntry, ContextOperation operation) {
      context.compute(documentEntry.documentId(), (key, context) -> {
         if (context == null && operation != DELETE) {
            return new CacheContext();
         } else {
            switch (operation) {
               case SAVE:
                  context.refreshLastWrite();
                  break;
               case ACCESS:
                  context.refreshLastAccess();
                  break;
               case DELETE:
                  return null;
            }

            return context;
         }
      });
   }

   @Override
   public DefaultCacheEvictionStrategy subPathCacheContext(Path subPath) {
      return new DefaultCacheEvictionStrategy(expireAfterWrite,
                                              expireAfterAccess,
                                              maximumSize,
                                              synchronizedMap(subPathContextMap(context, subPath)));
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", DefaultCacheEvictionStrategy.class.getSimpleName() + "[", "]")
            .add("expireAfterWrite=" + expireAfterWrite)
            .add("expireAfterAccess=" + expireAfterAccess)
            .add("maximumSize=" + maximumSize)
            .toString();
   }

   public static class CacheContextMap extends LinkedHashMap<DocumentPath, CacheContext> {

      public CacheContextMap(Integer maximumSize) {
         super(16, 0.75f, true);
      }

   }

   public static class CacheContext {
      Instant lastWrite;
      Instant lastAccess;

      public CacheContext(Instant lastWrite, Instant lastAccess) {
         this.lastWrite = notNull(lastWrite, "lastWrite");
         this.lastAccess = notNull(lastAccess, "lastAccess");
      }

      public CacheContext() {
         Instant now = ApplicationClock.nowAsInstant();
         this.lastWrite = now;
         this.lastAccess = now;
      }

      public Instant lastAccess() {
         return lastAccess;
      }

      public CacheContext lastAccess(Instant lastAccess) {
         this.lastAccess = lastAccess;
         return this;
      }

      public CacheContext refreshLastAccess() {
         return lastAccess(ApplicationClock.nowAsInstant());
      }

      public Instant lastWrite() {
         return lastWrite;
      }

      public CacheContext lastWrite(Instant lastWrite) {
         this.lastWrite = lastWrite;
         lastAccess(lastWrite);
         return this;
      }

      public CacheContext refreshLastWrite() {
         return lastWrite(ApplicationClock.nowAsInstant());
      }
   }
}
