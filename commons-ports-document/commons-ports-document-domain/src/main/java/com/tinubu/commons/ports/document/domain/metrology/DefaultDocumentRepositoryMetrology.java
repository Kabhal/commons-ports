/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.metrology;

import java.util.Objects;
import java.util.StringJoiner;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Predicate;

import com.tinubu.commons.ddd2.domain.event.DomainEvent;
import com.tinubu.commons.lang.metrology.Counter;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.event.DocumentAccessed;
import com.tinubu.commons.ports.document.domain.event.DocumentDeleted;
import com.tinubu.commons.ports.document.domain.event.DocumentSaved;

/**
 * Default metrology bean for all {@link DocumentRepository}.
 */
public class DefaultDocumentRepositoryMetrology implements DocumentRepositoryMetrology {

   protected final AtomicLong documentsAccessed = new AtomicLong();
   protected final AtomicLong documentsDeleted = new AtomicLong();
   protected final AtomicLong documentsSaved = new AtomicLong();

   public DefaultDocumentRepositoryMetrology() {
   }

   public void incrementDocumentsAccessed(int count) {
      documentsAccessed.addAndGet(count);
   }

   @Counter(description = "Document accessed counter")
   public long documentsAccessed() {
      return documentsAccessed.get();
   }

   public void incrementDocumentsDeleted(int count) {
      documentsDeleted.addAndGet(count);
   }

   @Counter(description = "Document deleted counter")
   public long documentsDeleted() {
      return documentsDeleted.get();
   }

   public void incrementDocumentsSaved(int count) {
      documentsSaved.addAndGet(count);
   }

   @Counter(description = "Document saved counter")
   public long documentsSaved() {
      return documentsSaved.get();
   }

   @Override
   public void resetMetrics() {
      documentsSaved.set(0);
      documentsAccessed.set(0);
      documentsDeleted.set(0);
   }

   @Override
   public DefaultDocumentRepositoryMetrology filteredListener(Predicate<DomainEvent> filter) {
      return new DefaultDocumentRepositoryMetrology() {
         @Override
         public void accept(DomainEvent event) {
            if (filter.test(event)) {
               DefaultDocumentRepositoryMetrology.this.accept(event);
            }
         }
      };
   }

   @Override
   public void accept(DomainEvent event) {
      if (event instanceof DocumentAccessed) {
         incrementDocumentsAccessed(1);
      } else if (event instanceof DocumentDeleted) {
         incrementDocumentsDeleted(1);
      } else if (event instanceof DocumentSaved) {
         incrementDocumentsSaved(1);
      }
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      DefaultDocumentRepositoryMetrology that = (DefaultDocumentRepositoryMetrology) o;
      return Objects.equals(documentsAccessed.get(), that.documentsAccessed.get()) && Objects.equals(
            documentsDeleted.get(),
            that.documentsDeleted.get()) && Objects.equals(documentsSaved.get(), that.documentsSaved.get());
   }

   @Override
   public int hashCode() {
      return Objects.hash(documentsAccessed, documentsDeleted, documentsSaved);
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", DefaultDocumentRepositoryMetrology.class.getSimpleName() + "[", "]")
            .add("documentsAccessed=" + documentsAccessed)
            .add("documentsDeleted=" + documentsDeleted)
            .add("documentsSaved=" + documentsSaved)
            .toString();
   }
}
