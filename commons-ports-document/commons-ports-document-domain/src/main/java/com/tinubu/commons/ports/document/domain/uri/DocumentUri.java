/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.uri;

import java.net.URI;

import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.uri.Uri;

/**
 * URI identifying a document in a repository.
 * <p>
 * See {@link RepositoryUri} to only identify a repository.
 *
 * @implSpec It is assumed that document URI is not independent of its repository URI, so that a
 *       document URI must always been also a repository URI (in other words contains the repository URI).
 *       Moreover, {@link DocumentUri} interface has no {@code documentId()} accessor because its purpose is
 *       to hold a generic URI string, and it's not possible to identify the document part without the
 *       specific URI implementation.
 * @see RepositoryUri
 */
public interface DocumentUri extends RepositoryUri {

   /**
    * Creates a document URI from URI string.
    *
    * @param uri document URI
    *
    * @return new instance
    */
   static DefaultDocumentUri ofDocumentUri(Uri uri) {
      return DefaultDocumentUri.ofDocumentUri(uri);
   }

   /**
    * Creates a document URI from URI string.
    *
    * @param uri document URI
    *
    * @return new instance
    */
   static DocumentUri ofDocumentUri(URI uri) {
      return DefaultDocumentUri.ofDocumentUri(uri);
   }

   /**
    * Creates a document URI from URI string.
    * URI syntax is checked as part of this operation.
    *
    * @param uri document URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI has invalid syntax
    */
   static DocumentUri ofDocumentUri(String uri) {
      Check.notNull(uri, "uri");

      return ofDocumentUri(Uri.ofUri(uri));
   }

}