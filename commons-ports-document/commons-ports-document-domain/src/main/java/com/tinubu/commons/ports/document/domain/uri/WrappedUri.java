/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.uri;

import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObject;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.Validate.Check.notNull;
import static com.tinubu.commons.ddd2.invariant.Validate.Check.validate;
import static com.tinubu.commons.ddd2.invariant.rules.MatchingRules.matches;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotEmpty;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.formatter.FastStringFormat;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.lang.util.Pair;

public class WrappedUri {

   private static final List<String> WRAPPED_URI_SCHEMES = list("document", "doc");
   private static final Pattern WRAPPED_URI_PATTERN = Pattern.compile(":");
   private static final Pattern REPOSITORY_TYPE_PATTERN = Pattern.compile("[A-Za-z0-9-_]*");

   private final URI wrappedUri;
   private final String repositoryType;
   private final URI originalUri;

   private WrappedUri(URI wrappedUri, String repositoryType, URI originalUri) {
      this.wrappedUri = notNull(wrappedUri, "wrappedUri");
      this.repositoryType = validate(repositoryType, "repositoryType", isValidRepositoryType());
      this.originalUri = notNull(originalUri, "originalUri");
   }

   /**
    * Unwrap specified URI and creates a new {@link WrappedUri} if specified repository URI is "wrapped".
    * <p>
    * Wrapped URI has the format {@code document:<repository-type>:<original-uri>} with case-sensitive
    * repository type.
    * Original URI must be a valid {@link URI}. Original URI query and fragment will be kept and URL encoding
    * preserved.
    * <p>
    * A wrapped URI is optional, is not official and is for disambiguation between document repositories.
    *
    * @param wrappedUri document repository URI, wrapped or not
    *
    * @return new wrapped uri, or {@link Optional#empty()} is specified URI is not wrapped
    *
    * @throws InvalidUriException if URI has wrapped scheme but has not valid syntax, or if wrapped URI
    *       is not a valid URI
    * @see #wrapUri(URI, String)
    */
   public static Optional<WrappedUri> unwrapUri(URI wrappedUri) {
      notNull(wrappedUri, "wrappedUri");

      var unwrappedUri = rawUnwrapUri(wrappedUri);

      return unwrappedUri.map(unwrapped -> new WrappedUri(wrappedUri,
                                                          unwrapped.getLeft(),
                                                          unwrapped.getRight()));
   }

   /**
    * Wraps specified URI in a new wrapped URI.
    * <p>
    * Wrapped URI has the format {@code document:<repository-type>:<original-uri>} with case-sensitive
    * repository type.
    * <p>
    * Original URI query and fragment will be kept and URL encoding preserved.
    *
    * @param originalUri Original URI to wrap
    * @param repositoryType repository type to use to distinguish repositories, with characters in
    *       [a-z0-9_-]
    *
    * @return wrapped URI
    */
   public static WrappedUri wrapUri(URI originalUri, String repositoryType) {
      notNull(originalUri, "originalUri");
      validate(repositoryType, "repositoryType", isValidRepositoryType());

      return new WrappedUri(rawWrapUri(originalUri, repositoryType), repositoryType, originalUri);
   }

   /**
    * Returns wrapped URI in format {@code document:<repository-type>:<original-uri>}.
    *
    * @return wrapped URI
    */
   public URI wrappedUri() {
      return wrappedUri;
   }

   /**
    * Returns case-sensitive repository type.
    *
    * @return repository type
    */
   public String repositoryType() {
      return repositoryType;
   }

   /**
    * Returns original URI in wrapped URI.
    *
    * @return original URI
    */
   public URI originalUri() {
      return originalUri;
   }

   private static InvariantRule<String> isValidRepositoryType() {
      return isNotEmpty().andValue(matches(value(REPOSITORY_TYPE_PATTERN),
                                           FastStringFormat.of("'",
                                                               validatingObject(),
                                                               "' characters must be in [a-z0-9_-]")));
   }

   private static URI rawWrapUri(URI uri, String repositoryType) {
      var wrappedUri = wrappedUriScheme() + ":" + repositoryType + ":" + uri;

      try {
         return URI.create(wrappedUri);
      } catch (IllegalArgumentException e) {
         throw new InvalidUriException(wrappedUri, e).subMessage("Can't wrap URI");
      }
   }

   public static Optional<Pair<String, URI>> rawUnwrapUri(URI uri) {
      notNull(uri, "uri");

      if (uri.getScheme() != null && WRAPPED_URI_SCHEMES.contains(uri.getScheme()) && uri.isOpaque()) {
         var wrappedUriSplit = WRAPPED_URI_PATTERN.split(uri.toString(), 3);

         if (wrappedUriSplit.length == 3) {
            var repositoryType = wrappedUriSplit[1];
            var wrappedUri = wrappedUriSplit[2];
            try {
               return optional(Pair.of(repositoryType, URI.create(wrappedUri)));
            } catch (IllegalArgumentException e) {
               throw new InvalidUriException(uri, e).subMessage("Wrapped '%s' URI is not a valid URI",
                                                                wrappedUri);
            }
         } else {
            throw new InvalidUriException(uri).subMessage(
                  "Invalid wrapped URI format, must be '%s:<repository-type>:<original-uri>'",
                  wrappedUriScheme());
         }
      } else {
         return optional();
      }
   }

   private static String wrappedUriScheme() {
      return WRAPPED_URI_SCHEMES.get(0);
   }

}
