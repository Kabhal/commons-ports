/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.noNullElements;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.List;
import java.util.Optional;
import java.util.ServiceLoader;
import java.util.function.Predicate;

import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.RepositoryUri;

/**
 * Document repository factory.
 */
// FIXME tests
public final class DocumentRepositoryFactory {

   private DocumentRepositoryFactory() {
   }

   private static class SingletonHolder {
      private static final DocumentRepositoryFactory INSTANCE = new DocumentRepositoryFactory();
   }

   public static DocumentRepositoryFactory instance() {
      return SingletonHolder.INSTANCE;
   }

   /**
    * Global repository loader registry.
    */
   private static List<DocumentRepositoryLoader> repositoryLoadersRegistry = repositoryLoadersRegistry();

   /**
    * Global predefined repository registry.
    */
   private static List<DocumentRepository> predefinedRepositories = list();

   /**
    * Selects a document repository by URI from specified pre-defined repositories.
    * <p>
    * A predefined repository is a pre-configured repository that enforces specified URI parameters. The
    * usage is to select a repository among predefined repositories without the possibility to change its
    * configuration.
    * <p>
    * First repository {@link DocumentRepository#supportsUri(RepositoryUri) supporting} the specified URI will
    * be returned.
    * <p>
    * Specified URI does not require parameters to be set. If present, URI parameters can't
    * override pre-defined repository parameters, and must match them.
    * <p>
    * Returned repository should not be manually closed after use, instead, specified repositories
    * should be closed externally after use.
    *
    * @param uri URI to use to select the repository, can be a {@link RepositoryUri} or a
    *       {@link DocumentUri}
    * @param repositories pre-defined repositories
    *
    * @return matching predefined document repository or {@link Optional#empty()} if repository not found
    */
   public Optional<DocumentRepository> predefinedDocumentRepository(RepositoryUri uri,
                                                                    List<DocumentRepository> repositories) {
      notNull(uri, "uri");
      noNullElements(repositories, "repositories");

      return stream(repositories).filter(repository -> repository.supportsUri(uri)).findFirst();
   }

   /**
    * Selects a document repository by URI from specified pre-defined repositories.
    * <p>
    * A predefined repository is a pre-configured repository that enforces specified URI parameters. The
    * usage is to select a repository among predefined repositories without the possibility to change its
    * configuration.
    * <p>
    * First repository {@link DocumentRepository#supportsUri(RepositoryUri) supporting} the specified URI will
    * be returned.
    * <p>
    * Specified URI does not require parameters to be set. If present, URI parameters can't
    * override pre-defined repository parameters, and must match them.
    * <p>
    * Returned repository should not be manually closed after use, instead, specified repositories
    * should be closed externally after use.
    *
    * @param uri URI to use to select the repository, can be a {@link RepositoryUri} or a
    *       {@link DocumentUri}
    * @param repositories pre-defined repositories
    *
    * @return matching predefined document repository or {@link Optional#empty()} if repository not found
    */
   public Optional<DocumentRepository> predefinedDocumentRepository(RepositoryUri uri,
                                                                    DocumentRepository... repositories) {
      return predefinedDocumentRepository(uri, list(notNull(repositories, "repositories")));
   }

   /**
    * Selects a document repository by URI from specified pre-defined repositories.
    * <p>
    * A predefined repository is a pre-configured repository that enforces specified URI parameters. The
    * usage is to select a repository among predefined repositories without the possibility to change its
    * configuration.
    * <p>
    * First repository {@link DocumentRepository#supportsUri(RepositoryUri) supporting} the specified URI will
    * be returned.
    * <p>
    * Specified URI does not require parameters to be set. If present, URI parameters can't
    * override pre-defined repository parameters, and must match them.
    * <p>
    * You can manually register new predefined repositories or clear the registry.
    * <p>
    * Returned repository should not be manually closed after use, instead, registered predefined repositories
    * should be closed externally after use (using for example {@link #closePredefinedRepositoryRegistry()}).
    *
    * @param uri URI to use to select the repository, can be a {@link RepositoryUri} or a
    *       {@link DocumentUri}
    *
    * @return matching predefined document repository or {@link Optional#empty()} if repository not found
    *
    * @see #closePredefinedRepositoryRegistry()
    * @see #resetPredefinedRepositoryRegistry(boolean)
    * @see #removePredefinedRepositoryRegistry(Predicate, boolean)
    * @see #setPredefinedRepositoryRegistry(List, boolean)
    * @see #addPredefinedRepositoryRegistry(List)
    */
   public Optional<DocumentRepository> predefinedDocumentRepository(RepositoryUri uri) {
      return predefinedDocumentRepository(uri, predefinedRepositories);
   }

   /**
    * Creates document repository by URI from specified repository loaders.
    * <p>
    * First repository compatible with the specified URI will be created using specified parameters, or
    * defaults. Specified URI must define all needed repository parameters.
    * <p>
    * Returned repository must be manually closed after use.
    *
    * @param uri URI to use to load the repository, can be a {@link RepositoryUri} or a
    *       {@link DocumentUri}
    * @param loaders loaders to match from
    *
    * @return document repository new instance or {@link Optional#empty()} if repository not found
    */
   @SuppressWarnings("unchecked")
   public Optional<DocumentRepository> documentRepository(RepositoryUri uri,
                                                          List<DocumentRepositoryLoader> loaders) {
      notNull(uri, "uri");
      noNullElements(loaders, "loaders");

      return stream(loaders)
            .flatMap(loader -> stream((Optional<DocumentRepository>) loader.load(uri, loaders)))
            .findFirst();
   }

   /**
    * Creates document repository by URI from specified repository loaders.
    * <p>
    * First repository supporting the specified URI will be created.
    * <p>
    * Specified URI must define all needed repository parameters.
    * <p>
    * Returned repository must be manually closed after use.
    *
    * @param uri URI to use to load the repository, can be a {@link RepositoryUri} or a
    *       {@link DocumentUri}
    * @param loaders loaders to match from
    *
    * @return document repository new instance or {@link Optional#empty()} if repository not found
    */
   public Optional<DocumentRepository> documentRepository(RepositoryUri uri,
                                                          DocumentRepositoryLoader... loaders) {
      return documentRepository(uri, list(notNull(loaders, "loaders")));
   }

   /**
    * Creates document repository by URI from repository loaders registry.
    * <p>
    * First repository supporting the specified URI will be created.
    * <p>
    * Specified URI must define all needed repository parameters.
    * <p>
    * Returned repository must be manually closed after use.
    * <p>
    * Repository loaders are dynamically loaded using {@link ServiceLoader} registered
    * {@link DocumentRepositoryLoader}s. You can manually register new loaders or clear the registry.
    *
    * @param uri URI to use to load the repository, can be a {@link RepositoryUri} or a
    *       {@link DocumentUri}
    *
    * @return document repository new instance or {@link Optional#empty()} if repository not found
    *
    * @see #resetRepositoryLoaderRegistry()
    * @see #removeRegistryRepositoryLoaders(Predicate)
    * @see #setRepositoryLoaderRegistry(List)
    * @see #addRepositoryLoaderRegistry(List)
    */
   public Optional<DocumentRepository> documentRepository(RepositoryUri uri) {
      return documentRepository(uri, repositoryLoadersRegistry);
   }

   private synchronized static List<DocumentRepositoryLoader> repositoryLoadersRegistry() {
      ServiceLoader<DocumentRepositoryLoader> uriAdapters =
            ServiceLoader.load(DocumentRepositoryLoader.class);

      return list(stream(uriAdapters));
   }

   /**
    * Returns repository loaders currently registered.
    *
    * @return registered repository loaders
    */
   public synchronized static List<DocumentRepositoryLoader> repositoryLoaderRegistry() {
      return repositoryLoadersRegistry;
   }

   /**
    * Resets current loader registry.
    * This has no impact on predefined repository registry that is updated independently.
    */
   public synchronized static void resetRepositoryLoaderRegistry() {
      repositoryLoadersRegistry = list();
   }

   /**
    * Removes loaders matching specified predicate from current loader registry.
    * This has no impact on predefined repository registry that is updated independently.
    *
    * @param filter predicate matching loaders to remove
    */
   public synchronized static void removeRegistryRepositoryLoaders(Predicate<DocumentRepositoryLoader> filter) {
      notNull(filter, "filter");

      repositoryLoadersRegistry.removeIf(filter);
   }

   /**
    * Replaces current loader registry.
    * This has no impact on predefined repository registry that is updated independently.
    *
    * @param loaders loaders to set
    */
   public synchronized static void setRepositoryLoaderRegistry(List<DocumentRepositoryLoader> loaders) {
      noNullElements(loaders, "loaders");

      repositoryLoadersRegistry = list(loaders);
   }

   /**
    * Adds specified loaders to current loader registry.
    * This has no impact on predefined repository registry that is updated independently.
    *
    * @param loaders loaders to add
    */
   public synchronized static void addRepositoryLoaderRegistry(List<DocumentRepositoryLoader> loaders) {
      noNullElements(loaders, "loaders");

      repositoryLoadersRegistry.addAll(loaders);
   }

   /**
    * Resets current predefined repository registry.
    *
    * @param automaticClose whether to automatically or manually close existing registry repositories
    */
   public synchronized static void resetPredefinedRepositoryRegistry(boolean automaticClose) {
      if (automaticClose) {
         closePredefinedRepositoryRegistry();
      }
      predefinedRepositories = list();
   }

   /**
    * Closes all registered predefined repositories.
    */
   public synchronized static void closePredefinedRepositoryRegistry() {
      predefinedRepositories.forEach(DocumentRepository::close);
   }

   /**
    * Removes repositories matching specified predicate from current predefined repository registry.
    *
    * @param automaticClose whether to automatically or manually close evicted registry repositories
    * @param filter predicate matching repositories to remove
    */
   public synchronized static void removePredefinedRepositoryRegistry(Predicate<DocumentRepository> filter,
                                                                      boolean automaticClose) {
      notNull(filter, "filter");

      predefinedRepositories = list(predefinedRepositories.stream().filter(repository -> {
         if (filter.test(repository)) {
            if (automaticClose) {
               repository.close();
            }
            return false;
         } else {
            return true;
         }
      }));
   }

   /**
    * Replaces current predefined repository registry.
    *
    * @param repositories repositories to set
    * @param automaticClose whether to automatically or manually close existing registry repositories
    */
   public synchronized static void setPredefinedRepositoryRegistry(List<DocumentRepository> repositories,
                                                                   boolean automaticClose) {
      noNullElements(repositories, "repositories");

      resetPredefinedRepositoryRegistry(automaticClose);
      addPredefinedRepositoryRegistry(repositories);
   }

   /**
    * Adds specified repositories to current predefined repository registry.
    *
    * @param repositories repositories to set
    */
   public synchronized static void addPredefinedRepositoryRegistry(List<DocumentRepository> repositories) {
      noNullElements(repositories, "repositories");

      predefinedRepositories.addAll(repositories);
   }

}
