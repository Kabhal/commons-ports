/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.processor;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.function.Function;

import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentContent;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent;
import com.tinubu.commons.ports.document.domain.LoadedDocumentContent;
import com.tinubu.commons.ports.document.domain.OutputStreamDocumentContent;
import com.tinubu.commons.ports.document.domain.OutputStreamDocumentContent.OutputStreamDocumentContentBuilder;

/**
 * General document content processor.
 * <p>
 * Implementation rationales :
 * <ul>
 *    <li>{@code process} operations <em>can</em> throw exceptions that are not {@link DocumentAccessException}, they'll be converted later by {@link Document#process(DocumentProcessor)}</li>
 *    <li>Processor should take care of source document content type ({@link InputStreamDocumentContent}, {@link LoadedDocumentContent}, {@link OutputStreamDocumentContent}, ...), and notably the later or throw an exception if content type is unsupported</li>
 *    <li>If source document content type is {@link OutputStreamDocumentContent}, processor should be added as a {@link OutputStreamDocumentContentBuilder#contentMapper(Function)} instead of replacing the current content
 *    <li>If source document content type is {@link LoadedDocumentContent}, the new returned content <em>can</em> be a {@link InputStreamDocumentContent}</li>
 *    <li>Always use {@code *DocumentContent::from} operation to create the new returned content to not loose data (e.g.: content finalizers)</li>
 *    <li>When consuming source document content, you <em>must</em> ensure to {@link DocumentContent::close} it</li>
 * </ul>
 *
 * @see Document#process(DocumentProcessor)
 */
@FunctionalInterface
public interface DocumentContentProcessor extends DocumentProcessor {

   /**
    * Processes specified document content.
    *
    * @param documentContent document content to process
    *
    * @return new processed document content instance
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   DocumentContent process(DocumentContent documentContent);

   @Override
   default Document process(Document document) {
      return document.content(process(document.content()));
   }

   /**
    * Re-exposes this processor as a {@link DocumentContentProcessor} when implementation class implements
    * both {@link DocumentContentProcessor} and {@link DocumentProcessor} so that
    * {@link DocumentContentProcessor#process(Document)} is overridden.
    *
    * @return new {@link DocumentContentProcessor}
    */
   default DocumentContentProcessor contentProcessor() {
      return new DocumentContentProcessor() {
         @Override
         public DocumentContent process(DocumentContent documentContent) {
            return DocumentContentProcessor.this.process(documentContent);
         }
      };
   }

   default DocumentContentProcessor compose(DocumentContentProcessor before) {
      notNull(before, "before");
      return (DocumentContent documentContent) -> process(before.process(documentContent));
   }

   default DocumentContentProcessor andThen(DocumentContentProcessor after) {
      notNull(after, "after");
      return (DocumentContent documentContent) -> after.process(process(documentContent));
   }

}
