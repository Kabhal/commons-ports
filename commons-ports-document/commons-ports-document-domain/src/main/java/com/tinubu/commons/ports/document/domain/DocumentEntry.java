/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.lang.beans.Getter;

/**
 * References an identified document with its {@link DocumentMetadata}.
 */
public class DocumentEntry extends AbstractValue {
   private final DocumentPath documentId;
   private final DocumentMetadata metadata;

   private DocumentEntry(DocumentEntryBuilder builder) {
      this.documentId = builder.documentId;
      this.metadata = builder.metadata;
   }

   @Override
   @SuppressWarnings("unchecked")
   public Fields<? extends DocumentEntry> defineDomainFields() {
      return Fields
            .<DocumentEntry>builder()
            .superFields((Fields<DocumentEntry>) super.defineDomainFields())
            .field("documentId", v -> v.documentId, isNotNull())
            .field("metadata", v -> v.metadata, isNotNull())
            .build();
   }

   @Getter
   public DocumentPath documentId() {
      return documentId;
   }

   @Getter
   public DocumentMetadata metadata() {
      return metadata;
   }

   public static DocumentEntryBuilder reconstituteBuilder() {
      return new DocumentEntryBuilder().reconstitute();
   }

   public static class DocumentEntryBuilder extends DomainBuilder<DocumentEntry> {
      private DocumentPath documentId;
      private DocumentMetadata metadata;

      public static DocumentEntryBuilder from(DocumentEntry documentEntry) {
         notNull(documentEntry, "documentEntry");
         return new DocumentEntryBuilder()
               .<DocumentEntryBuilder>reconstitute()
               .documentId(documentEntry.documentId)
               .metadata(documentEntry.metadata);
      }

      public DocumentEntryBuilder documentId(DocumentPath documentId) {
         this.documentId = documentId;
         return this;
      }

      public DocumentEntryBuilder metadata(DocumentMetadata metadata) {
         this.metadata = metadata;
         return this;
      }

      @Override
      public DocumentEntry buildDomainObject() {
         return new DocumentEntry(this);
      }

   }

}
