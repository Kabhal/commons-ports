/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.uri;

import static com.tinubu.commons.ddd2.domain.type.support.DomainObjectSupport.hideField;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.ClassRules.isInstanceOf;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.instanceOf;
import static com.tinubu.commons.lang.util.Try.ofThrownBy;
import static com.tinubu.commons.lang.util.XCheckedSupplier.checkedSupplier;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;

import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.ParameterValue;
import com.tinubu.commons.ddd2.invariant.Validate;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.invariant.rules.domain.ids.UriRules;
import com.tinubu.commons.ddd2.uri.AbstractUri;
import com.tinubu.commons.ddd2.uri.ComponentUri;
import com.tinubu.commons.ddd2.uri.DefaultComponentUri;
import com.tinubu.commons.ddd2.uri.DefaultUri;
import com.tinubu.commons.ddd2.uri.DefaultUriRestrictions;
import com.tinubu.commons.ddd2.uri.DefaultUriRestrictions.PortRestriction;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ddd2.uri.UriRestrictions;
import com.tinubu.commons.lang.convert.ConversionFailedException;
import com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery.Parameter;

/**
 * @implSpec Class can't be generic because of Document -> Repository inheritance.
 */
// FIXME extends AbstractComponentUri ?
public abstract class AbstractRepositoryUri extends AbstractUri<AbstractRepositoryUri>
      implements RepositoryUri {

   protected static final String COMPATIBILITY_GROUP = "compatibility";

   protected final Uri uri;
   protected final boolean documentUri;

   protected AbstractRepositoryUri(Uri uri, boolean documentUri) {
      super(uri.toURI(), uri.restrictions(), false);
      this.uri = uri;
      this.documentUri = documentUri;
   }

   @Override
   @SuppressWarnings("unchecked")
   protected Fields<? extends AbstractRepositoryUri> defineDomainFields() {
      return Fields
            .<AbstractRepositoryUri>builder()
            .superFields((Fields<AbstractRepositoryUri>) super.defineDomainFields())
            .field("restrictions", v -> v.restrictions, hideField(), isNotNull())
            .build();
   }

   public Uri uri() {
      return uri;
   }

   public boolean documentUri() {
      return documentUri;
   }

   /**
    * @implSpec Re-creation is not supported in {@link RepositoryUri} implementations, because of design
    *       and lack of generic on {@link AbstractRepositoryUri}.
    */
   @Override
   protected AbstractRepositoryUri recreate(URI uri, UriRestrictions restrictions, boolean newObject) {
      throw new UnsupportedOperationException();
   }

   @Override
   public Optional<String> scheme() {
      return uri.scheme();
   }

   @Override
   public Optional<String> schemeSpecific(boolean encoded) {
      return uri.schemeSpecific(encoded);
   }

   @Override
   public Optional<String> authority(boolean encoded) {
      return uri.authority(encoded);
   }

   @Override
   public Optional<String> serverAuthority(boolean encoded) {
      return uri.serverAuthority(encoded);
   }

   @Override
   public Optional<String> registryAuthority(boolean encoded) {
      return uri.registryAuthority(encoded);
   }

   @Override
   public Optional<String> userInfo(boolean encoded) {
      return uri.userInfo(encoded);
   }

   @Override
   public Optional<String> host() {
      return uri.host();
   }

   @Override
   public Optional<Integer> port() {
      return uri.port();
   }

   @Override
   public Optional<String> path(boolean encoded) {
      return uri.path(encoded);
   }

   @Override
   public Optional<String> query(boolean encoded) {
      return uri.query(encoded);
   }

   @Override
   public Optional<String> fragment(boolean encoded) {
      return uri.fragment(encoded);
   }

   /**
    * Returns specified {@link ComponentUri}'s parameterized query. It is assumed that URI's query has
    * {@link ParameterizedQuery} type.
    */
   protected static <T extends ComponentUri> ParameterizedQuery query(T uri) {
      return uri.component().query().map(ParameterizedQuery.class::cast).orElseThrow();
   }

   /**
    * Returns current {@link #uri()}'s parameterized query. It is assumed that URI's query has
    * {@link ParameterizedQuery} type.
    */
   @SuppressWarnings("unchecked")
   public <T extends ComponentUri> ParameterizedQuery query() {
      return query((T) uri);
   }

   protected static <T extends ComponentUri, R> R mapQuery(T uri,
                                                           BiFunction<? super T, ? super ParameterizedQuery, ? extends R> mapper) {
      return mapper.apply(uri, uri.component().query().map(ParameterizedQuery.class::cast).orElseThrow());
   }

   @SuppressWarnings("unchecked")
   protected <T extends ComponentUri, R> R mapQuery(BiFunction<? super T, ? super ParameterizedQuery, ? extends R> mapper) {
      return mapQuery((T) uri, mapper);
   }

   /**
    * Alternative to {@link Validate#validate(Object, String, InvariantRule)} that throws
    * {@link IncompatibleUriException} if validation fails.
    */
   protected static <T extends Uri> T validateUriCompatibility(T uri,
                                                               String objectName,
                                                               InvariantRule<T> rule) {
      return Validate
            .validate(uri, objectName, rule)
            .invariantMatchingGroups(COMPATIBILITY_GROUP)
            .orThrow(uriInvariantResultHandler(uri));
   }

   /**
    * Invariant rule to check if a given {@link Uri} is instance of {@link DefaultUri} or
    * {@link DefaultComponentUri}, or any specified compatible URI.
    */
   @SafeVarargs
   protected static <T extends Uri> InvariantRule<T> isCompatibleUriType(Class<? extends Uri>... compatibleUris) {
      Check.notNull(compatibleUris, "compatiblesUris");

      InvariantRule<T> rules =
            isInstanceOf(ParameterValue.value(DefaultUri.class)).orValue(isInstanceOf(ParameterValue.value(
                  DefaultComponentUri.class)));
      for (Class<? extends Uri> compatibleUri : compatibleUris) {
         rules = rules.orValue(isInstanceOf(ParameterValue.value(compatibleUri)));
      }

      return rules;
   }

   /**
    * Invariant rule to check if a given {@link RepositoryUri} is instance of {@link DefaultRepositoryUri} or
    * any specified compatible URI.
    */
   @SafeVarargs
   protected static <T extends RepositoryUri> InvariantRule<T> isCompatibleRepositoryUriType(Class<? extends RepositoryUri>... compatibleUris) {
      Check.notNull(compatibleUris, "compatiblesUris");

      InvariantRule<T> rules = isInstanceOf(ParameterValue.value(DefaultRepositoryUri.class));
      for (Class<? extends RepositoryUri> compatibleUri : compatibleUris) {
         rules = rules.orValue(isInstanceOf(ParameterValue.value(compatibleUri)));
      }

      return rules;
   }

   /**
    * Invariant rule to check if a given {@link DocumentUri} is instance of {@link DefaultDocumentUri} or
    * any specified compatible URI.
    */
   @SafeVarargs
   protected static <T extends DocumentUri> InvariantRule<T> isCompatibleDocumentUriType(Class<? extends DocumentUri>... compatibleUris) {
      Check.notNull(compatibleUris, "compatiblesUris");

      InvariantRule<T> rules = isInstanceOf(ParameterValue.value(DefaultDocumentUri.class));
      for (Class<? extends DocumentUri> compatibleUri : compatibleUris) {
         rules = rules.orValue(isInstanceOf(ParameterValue.value(compatibleUri)));
      }

      return rules;
   }

   /**
    * Unwraps specified {@link Uri}.
    *
    * @param uri URI to unwrap
    * @param repositoryType supported repository type
    *
    * @return unwrapped URI
    *
    * @throws IncompatibleUriException if repository type is not supported
    * @throws InvalidUriException if URI is invalid
    */
   protected static Uri unwrapUri(Uri uri, String repositoryType) {
      return WrappedUri.unwrapUri(uri.toURI()).map(wrappedUri -> {
         if (wrappedUri.repositoryType().equalsIgnoreCase(repositoryType)) {
            return wrappedUri.originalUri();
         } else {
            throw new IncompatibleUriException(uri).subMessage("Unsupported '%s' repository type",
                                                               wrappedUri.repositoryType());
         }
      }).<Uri>map(Uri::ofUri).orElse(uri);
   }

   /**
    * Creates a parameterized query from specified {@link Uri}.
    * <p>
    * Created {@link Uri} is normalized and checked for remaining traversals.
    *
    * @param uri URI to apply to factory
    * @param restrictions URI restrictions to apply to factory
    * @param uriFactory URI factory
    * @param <T> {@link Uri} type
    *
    * @return new parameterized query {@link Uri}, with all registered parameters
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   @SuppressWarnings("unchecked")
   protected static <T extends ComponentUri> T parameterizedQueryUri(Uri uri,
                                                                     UriRestrictions restrictions,
                                                                     BiFunction<Uri, UriRestrictions, T> uriFactory,
                                                                     List<Parameter> parameters) {
      Check.notNull(uri, "uri");

      try {
         var parameterizedQueryUri = (T) uriFactory
               .apply(uri, restrictions)
               .component()
               .query(nq -> nullable(nq)
                     .map(ParameterizedQuery::of)
                     .orElseGet(() -> ParameterizedQuery.ofEmpty().noQueryIfEmpty(true))
                     .registerParameters(parameters));

         parameterizedQueryUri = (T) parameterizedQueryUri.normalize();

         return Validate
               .validate(parameterizedQueryUri, "uri", UriRules.hasNoTraversal())
               .orThrowMessage(message -> new InvalidUriException(uri).subMessage(message));
      } catch (ConversionFailedException e) {
         throw new InvalidUriException(uri, e).subMessage(e);
      }
   }

   /**
    * Creates a parameterized query from specified {@link Uri}, using default restrictions.
    * <p>
    * Created {@link Uri} is normalized and checked for remaining traversals.
    *
    * @param uri URI to apply to factory
    * @param uriFactory URI factory
    * @param <T> {@link Uri} type
    *
    * @return new parameterized query {@link Uri}, with all registered parameters
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   protected static <T extends ComponentUri> T parameterizedQueryUri(Uri uri,
                                                                     BiFunction<Uri, UriRestrictions, T> uriFactory,
                                                                     List<Parameter> parameters) {

      return parameterizedQueryUri(uri, defaultRestrictions(), uriFactory, parameters);
   }

   /** Default {@link DefaultUriRestrictions restrictions} for repository URIs. */
   protected static DefaultUriRestrictions defaultRestrictions() {
      return DefaultUriRestrictions
            .ofRestrictions(false, true)
            .portRestriction(PortRestriction.REMOVE_MAPPED_PORT);
   }

   /**
    * Converts specified URI to convert type if instance of {@link DefaultRepositoryUri}, and if URI is not
    * already converted.
    * <p>
    * Returns {@link Optional#empty()} if URI has not convertible type, or if converter fails with
    * {@link IncompatibleUriException}.
    *
    * @param uri URI to convert
    * @param convertType conversion target type class
    * @param converter converter, if needed
    * @param <T> conversion target type
    *
    * @return converted repository URI, or {@link Optional#empty()}
    */
   protected static <T extends RepositoryUri> Optional<T> supportsRepositoryUri(RepositoryUri uri,
                                                                                Class<T> convertType,
                                                                                Function<? super RepositoryUri, ? extends T> converter) {
      return instanceOf(uri, convertType).or(() -> instanceOf(uri, DefaultRepositoryUri.class).flatMap(
            defaultUri -> ofThrownBy(checkedSupplier(() -> converter.apply(defaultUri)),
                                     IncompatibleUriException.class).optional()));
   }

}
