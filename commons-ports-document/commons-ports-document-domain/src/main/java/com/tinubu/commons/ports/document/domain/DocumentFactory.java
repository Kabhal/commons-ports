/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.OptionalUtils.peek;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.noNullElements;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.ports.document.domain.DocumentRepositoryFactory.repositoryLoaderRegistry;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.RepositoryUri;

/**
 * Simple document factory, in complement to more complex {@link DocumentRepository} implementations.
 */
// FIXME tests
public class DocumentFactory {

   /**
    * Generates a document from local filesystem's {@link File}.
    * Metadata document name is set to the original file name without path.
    *
    * @param file file to generate this document from
    * @param documentId generated document identifier
    * @param documentEncoding document encoding
    *
    * @return document builder
    *
    * @throws NoSuchFileException if specified file does not exist
    * @throws IOException if an I/O error occurs while reading file
    */
   public Document document(File file, DocumentPath documentId, Charset documentEncoding) throws IOException {
      notNull(file, "file");
      notNull(documentId, "documentId");

      BasicFileAttributes basicFileAttributes =
            Files.readAttributes(file.toPath(), BasicFileAttributes.class);
      Instant creationDate = basicFileAttributes.creationTime().toInstant();
      Instant lastUpdateDate = basicFileAttributes.lastModifiedTime().toInstant();

      return new DocumentBuilder()
            .<DocumentBuilder>reconstitute()
            .documentId(documentId)
            .metadata(new DocumentMetadataBuilder()
                            .<DocumentMetadataBuilder>reconstitute()
                            .documentPath(file.toPath().getFileName())
                            .creationDate(creationDate)
                            .lastUpdateDate(lastUpdateDate)
                            .contentType(documentEncoding)
                            .contentSize(file.length())
                            .build())
            .streamContent(Files.newInputStream(file.toPath()), documentEncoding, file.length())
            .build();
   }

   /**
    * Generates a document from local filesystem's {@link File} with
    * {@link Charset#defaultCharset() default JVM encoding}.
    * Metadata document name is set to the original file name without path.
    *
    * @param file file to generate this document from
    * @param documentId generated document identifier
    *
    * @return document builder
    *
    * @throws NoSuchFileException if specified file does not exist
    * @throws IOException if an I/O error occurs while reading file
    */
   public Document document(File file, DocumentPath documentId) throws IOException {
      return document(file, documentId, Charset.defaultCharset());
   }

   /**
    * Finds a document by URI from a selected repository among specified predefined repositories.
    * <p>
    * A predefined repository is a pre-configured repository that enforces specified URI parameters. The
    * usage is to select a repository among predefined repositories without the possibility to change its
    * configuration.
    * <p>
    * First repository {@link DocumentRepository#supportsUri(RepositoryUri) supporting} the specified URI will
    * be returned.
    * <p>
    * Specified URI does not require parameters to be set. If present, URI parameters can't
    * override pre-defined repository parameters, and must match them.
    * <p>
    * Returned referenced repository should not be manually closed after use, instead, specified repositories
    * should be closed externally.
    * On the contrary, returned document must be manually closed after use.
    * Alternatively, you can use {@link ReferencedDocument#close()} that safely manages these two rules
    * depending on context.
    *
    * @param documentUri document URI
    * @param repositories repositories to match URI from
    *
    * @return found referenced document or {@link Optional#empty()} if document not found
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   public Optional<ReferencedDocument<? extends DocumentRepository>> predefinedDocument(DocumentUri documentUri,
                                                                                        List<DocumentRepository> repositories) {
      notNull(documentUri, "documentUri");
      noNullElements(repositories, "repositories");

      return DocumentRepositoryFactory.instance().predefinedDocumentRepository(documentUri, repositories)
            .flatMap(documentRepository -> documentRepository
                  .findDocumentByUri(documentUri)
                  .map(documentRepository::referencedDocument));
   }

   /**
    * Finds a document by URI from a selected repository among specified predefined repositories.
    * <p>
    * A predefined repository is a pre-configured repository that enforces specified URI parameters. The
    * usage is to select a repository among predefined repositories without the possibility to change its
    * configuration.
    * <p>
    * First repository {@link DocumentRepository#supportsUri(RepositoryUri) supporting} the specified URI will
    * be returned.
    * <p>
    * Specified URI does not require parameters to be set. If present, URI parameters can't
    * override pre-defined repository parameters, and must match them.
    * <p>
    * Returned referenced repository should not be manually closed after use, instead, specified repositories
    * should be closed externally.
    * On the contrary, returned document must be manually closed after use.
    * Alternatively, you can use {@link ReferencedDocument#close()} that safely manages these two rules
    * depending on context.
    *
    * @param documentUri document URI
    * @param repositories repositories to match URI from
    *
    * @return found referenced document or {@link Optional#empty()} if document not found
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   public Optional<ReferencedDocument<? extends DocumentRepository>> predefinedDocument(DocumentUri documentUri,
                                                                                        DocumentRepository... repositories) {
      return predefinedDocument(documentUri, list(notNull(repositories, "repositories")));
   }

   /**
    * Finds a document by URI from a selected repository among specified predefined repositories.
    * <p>
    * A predefined repository is a pre-configured repository that enforces specified URI parameters. The
    * usage is to select a repository among predefined repositories without the possibility to change its
    * configuration.
    * <p>
    * First repository {@link DocumentRepository#supportsUri(RepositoryUri) supporting} the specified URI will
    * be returned.
    * <p>
    * Specified URI does not require parameters to be set. If present, URI parameters can't
    * override pre-defined repository parameters, and must match them.
    * <p>
    * You can manually register new predefined repositories or clear the registry.
    * <p>
    * Returned referenced repository should not be manually closed after use, instead, specified repositories
    * should be closed externally.
    * On the contrary, returned document must be manually closed after use.
    * Alternatively, you can use {@link ReferencedDocument#close()} that safely manages these two rules
    * depending on context.
    *
    * @param documentUri document URI
    *
    * @return found referenced document or {@link Optional#empty()} if document not found
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    * @see DocumentRepositoryFactory#closePredefinedRepositoryRegistry()
    * @see DocumentRepositoryFactory#resetPredefinedRepositoryRegistry(boolean)
    * @see DocumentRepositoryFactory#removePredefinedRepositoryRegistry(Predicate, boolean)
    * @see DocumentRepositoryFactory#setPredefinedRepositoryRegistry(List, boolean)
    * @see DocumentRepositoryFactory#addPredefinedRepositoryRegistry(List)
    */
   public Optional<ReferencedDocument<? extends DocumentRepository>> predefinedDocument(DocumentUri documentUri) {
      notNull(documentUri, "documentUri");

      return DocumentRepositoryFactory.instance().predefinedDocumentRepository(documentUri)
            .flatMap(documentRepository -> documentRepository
                  .findDocumentByUri(documentUri)
                  .map(documentRepository::referencedDocument));
   }

   /**
    * Creates document repository and finds document in it by URI from specified repository loaders.
    * <p>
    * First repository compatible with the specified URI will be created using specified parameters, or
    * defaults. Specified URI must define all needed repository parameters.
    * <p>
    * Both returned repository and document must be manually closed after use. Alternatively, you can use
    * {@link ReferencedDocument#close()} that safely manages this rule depending on context.
    *
    * @param documentUri whether specified URI is a repository URI or a document URI
    * @param loaders loaders to match from
    *
    * @return found referenced document or {@link Optional#empty()} if document not found
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   @SuppressWarnings("unchecked")
   public Optional<ReferencedDocument<DocumentRepository>> document(DocumentUri documentUri,
                                                                    List<DocumentRepositoryLoader> loaders) {
      notNull(documentUri, "documentUri");
      noNullElements(loaders, "loaders");

      return peek(stream(loaders)
                        .flatMap(loader -> stream((Optional<ReferencedDocument<DocumentRepository>>) loader.loadReferencedDocument(
                              documentUri,
                              loaders))).findFirst(), rd -> rd.autoCloseRepository().tryLoadDocument());
   }

   /**
    * Creates document repository and finds document in it by URI from specified repository loaders.
    * <p>
    * First repository compatible with the specified URI will be created using specified parameters, or
    * defaults. Specified URI must define all needed repository parameters.
    * <p>
    * Both returned repository and document must be manually closed after use. Alternatively, you can use
    * {@link ReferencedDocument#close()} that safely manages this rule depending on context.
    *
    * @param documentUri whether specified URI is a repository URI or a document URI
    * @param loaders loaders to match from
    *
    * @return found referenced document or {@link Optional#empty()} if document not found
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   public Optional<ReferencedDocument<DocumentRepository>> document(DocumentUri documentUri,
                                                                    DocumentRepositoryLoader... loaders) {
      return document(documentUri, list(notNull(loaders, "loaders")));
   }

   /**
    * Creates document repository and finds document in it by URI from registered loaders.
    * <p>
    * First repository compatible with the specified URI will be created using specified parameters, or
    * defaults. Specified URI must define all needed repository parameters.
    * <p>
    * Both returned repository and document must be manually closed after use. Alternatively, you can use
    * {@link ReferencedDocument#close()} that safely manages this rule depending on context.
    *
    * @param documentUri whether specified URI is a repository URI or a document URI
    *
    * @return found referenced document or {@link Optional#empty()} if document not found
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    * @see DocumentRepositoryFactory#resetRepositoryLoaderRegistry()
    * @see DocumentRepositoryFactory#removeRegistryRepositoryLoaders(Predicate)
    * @see DocumentRepositoryFactory#setRepositoryLoaderRegistry(List)
    * @see DocumentRepositoryFactory#addRepositoryLoaderRegistry(List)
    */
   public Optional<ReferencedDocument<DocumentRepository>> document(DocumentUri documentUri) {
      notNull(documentUri, "documentUri");

      return document(documentUri, repositoryLoaderRegistry());
   }

}
