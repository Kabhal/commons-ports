/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.uri;

import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery.ExportUriFilter.isAlwaysTrue;
import static com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery.ExportUriFilter.isParameterDefaultValue;
import static com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery.ExportUriFilter.isRegisteredParameter;
import static com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery.ExportUriFilter.isSensitiveParameter;

import java.util.function.BiPredicate;

import com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery.Parameter;

public class UriUtils {

   /**
    * Generates a default parameter filter for URI export.
    *
    * @param options URI generation options
    * @param registeredParameterFilter extra parameter filter for registered parameters only
    *
    * @return URI generation parameter filter
    */
   @SuppressWarnings("rawtypes")
   public static BiPredicate<ParameterizedQuery, Parameter> exportUriFilter(ExportUriOptions options,
                                                                            BiPredicate<ParameterizedQuery, Parameter> registeredParameterFilter) {
      notNull(options, "options");
      notNull(registeredParameterFilter, "registeredParameterFilter");

      BiPredicate<ParameterizedQuery, Parameter> parameterFilter = isRegisteredParameter().negate();

      if (options.exportParameters()) {
         parameterFilter = parameterFilter.or((options.forceDefaultValues()
                                               ? isAlwaysTrue()
                                               : isParameterDefaultValue().negate())
                                                    .and(options.excludeSensitiveParameters()
                                                         ? isSensitiveParameter().negate()
                                                         : isAlwaysTrue())
                                                    .and(registeredParameterFilter));
      }
      return options.filterParameters().and(parameterFilter);
   }

}
