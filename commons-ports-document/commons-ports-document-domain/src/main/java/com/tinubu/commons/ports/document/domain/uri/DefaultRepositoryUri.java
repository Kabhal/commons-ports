/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.uri;

import static com.tinubu.commons.ddd2.domain.type.support.DomainObjectSupport.checkInvariants;
import static com.tinubu.commons.lang.util.OptionalUtils.instanceOf;

import java.net.URI;

import com.tinubu.commons.ddd2.domain.type.Id;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.uri.Uri;

/**
 * URI identifying a repository.
 * <p>
 * See {@link DefaultDocumentUri} to identify a specific document in a repository.
 *
 * @see DefaultDocumentUri
 */
public class DefaultRepositoryUri extends AbstractRepositoryUri {

   public DefaultRepositoryUri(Uri uri) {
      super(uri, false);
   }

   /**
    * Creates a repository URI from {@link Uri}.
    *
    * @param uri repository URI
    *
    * @return new instance
    */
   static DefaultRepositoryUri ofRepositoryUri(Uri uri) {
      Check.notNull(uri, "uri");

      return checkInvariants(new DefaultRepositoryUri(uri));
   }

   /**
    * Creates a repository URI from {@link URI}.
    *
    * @param uri repository URI
    *
    * @return new instance
    */
   static DefaultRepositoryUri ofRepositoryUri(URI uri) {
      Check.notNull(uri, "uri");

      return ofRepositoryUri(Uri.ofUri(uri));
   }

   /**
    * Creates a repository URI from URI string.
    * URI syntax is checked as part of this operation.
    *
    * @param uri repository URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI has invalid syntax
    */
   static DefaultRepositoryUri ofRepositoryUri(String uri) {
      Check.notNull(uri, "uri");

      return ofRepositoryUri(Uri.ofUri(uri));

   }

   @Override
   public DefaultRepositoryUri normalize() {
      return ofRepositoryUri(uri.normalize());
   }

   @Override
   public DefaultRepositoryUri resolve(Uri uri) {
      return ofRepositoryUri(uri.resolve(uri));
   }

   @Override
   public DefaultRepositoryUri resolve(URI uri) {
      return ofRepositoryUri(uri.resolve(uri));
   }

   @Override
   public DefaultRepositoryUri relativize(Uri uri) {
      return ofRepositoryUri(uri.relativize(uri));
   }

   @Override
   public DefaultRepositoryUri relativize(URI uri) {
      return ofRepositoryUri(uri.relativize(uri));
   }

   @Override
   public boolean supportsUri(RepositoryUri uri) {
      return instanceOf(uri, DefaultRepositoryUri.class).map(dUri -> {
         return dUri.uri().value().equals(uri().value());
      }).orElse(false);
   }

   @Override
   public URI exportURI(ExportUriOptions options) {
      return uri().toURI();
   }

   @Override
   public DefaultRepositoryUri exportUri(ExportUriOptions options) {
      return DefaultRepositoryUri.ofRepositoryUri(exportURI(options));
   }

   @Override
   public int compareTo(Id o) {
      if (!(o instanceof DefaultRepositoryUri)) {
         return -1;
      } else if (o == this) {
         return 0;
      } else {
         return uri.compareTo(((DefaultRepositoryUri) o).uri);
      }
   }

}

