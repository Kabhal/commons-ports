/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.repository.cache.eviction;

import java.nio.file.Path;
import java.util.function.Consumer;

import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;

/**
 * Defines a cache eviction strategy.
 * Note that the cached content is not part of the strategy, but is orchestrated externally depending on
 * strategy results.
 */
public interface CacheEvictionStrategy {

   /**
    * Available operations related to cached documents for context update.
    */
   enum ContextOperation {
      /**
       * A cached document has been accessed.
       */
      ACCESS,
      /**
       * A cached document has been saved.
       */
      SAVE,
      /**
       * A cached document has been deleted.
       */
      DELETE
   }

   /**
    * Checks for all evictable documents from cache depending on eviction strategy.
    * Eviction strategy should remove eviction context for evicted document only if callback is successful. In
    * case of callback failure, returned value should not change.
    *
    * @param callback optional callback to call for each evictable document
    *
    * @return {@code true} if at least one document is evictable
    */
   boolean evictables(Consumer<DocumentPath> callback);

   /**
    * Returns {@code true} if specified document is evictable from cache.
    * Eviction strategy should remove eviction context for evicted document only if callback is successful. In
    * case of callback failure, returned value should not change.
    *
    * @param documentId document to check
    * @param callback optional callback to call if document is evictable
    *
    * @return {@code true} if specified document is evictable from cache
    */
   boolean evictable(DocumentPath documentId, Consumer<DocumentPath> callback);

   /**
    * Updates the cache eviction context for specified document entry, and for the specified operation
    *
    * @param documentEntry document entry to update cache context for
    * @param operation operation related to context update
    */
   void updateCacheContext(DocumentEntry documentEntry, ContextOperation operation);

   /**
    * Updates cache eviction context, depending on implementation, when a
    * {@link DocumentRepository#subPath(Path, boolean)} operation is called.
    *
    * @param subPath sub-path to apply
    *
    * @return updated cache eviction strategy
    */
   CacheEvictionStrategy subPathCacheContext(Path subPath);

   /**
    * Compose this cache eviction strategy with other one.
    *
    * @param evictionStrategies other strategies to compose
    *
    * @return new composed cache eviction strategy
    */
   CacheEvictionStrategy or(CacheEvictionStrategy... evictionStrategies);

}

