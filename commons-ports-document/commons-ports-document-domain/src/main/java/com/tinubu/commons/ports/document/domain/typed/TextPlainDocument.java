/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.typed;

import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.MimeTypeRules.isTypeAndSubtypeEqualTo;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.TEXT_PLAIN;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.NullableUtils.nullableInstanceOf;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.nio.charset.Charset;

import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.lang.beans.Setter;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentPath;

/**
 * Specialized {@link Document} representing a {@code text/plain} document.
 * <p>
 * Default content-encoding is {@link #DEFAULT_CONTENT_ENCODING}.
 * Default line separator is {@link #DEFAULT_LINE_SEPARATOR}.
 */
public class TextPlainDocument extends TypedDocument<TextPlainDocument> {

   /**
    * Default line separator for {@link #writeLn(String)} operation.
    *
    * @implSpec Default should be deterministic here.
    */
   protected static final String DEFAULT_LINE_SEPARATOR = "\n";
   /**
    * Default content encoding.
    *
    * @implSpec Default should be deterministic here.
    */
   protected static final Charset DEFAULT_CONTENT_ENCODING = UTF_8;
   /**
    * Document enforced content-type.
    */
   protected static final MimeType SUPPORTED_CONTENT_TYPE = TEXT_PLAIN;

   /** Line separator. */
   protected final String lineSeparator;

   protected TextPlainDocument(TextPlainDocumentBuilder builder) {
      super(builder);
      this.lineSeparator = nullable(builder.lineSeparator, DEFAULT_LINE_SEPARATOR);
   }

   // Optional<Document> document = findDocumentById(...)
   // document.map(TextPlainDocument::of) ->
   // Optional<TextPlainDocument>

   /**
    * Creates a new text document from existing document. Source document metadata are not changed.
    *
    * @param document existing document to base on
    * @param assumeContentType whether to assume document content-type if content-type is unknown or has
    *       no encoding
    *
    * @return new text document
    */
   public static TextPlainDocument of(Document document, boolean assumeContentType) {
      notNull(document, "document");

      return new TextPlainDocumentBuilder()
            .reconstitute()
            .copy(document)
            .assumeContentType(assumeContentType, SUPPORTED_CONTENT_TYPE)
            .build();
   }

   public static TextPlainDocument of(Document document) {
      return of(document, false);
   }

   public static TextPlainDocument open(DocumentPath documentId,
                                        OutputStream contentOutputStream,
                                        Charset contentEncoding) {
      notNull(documentId, "documentId");

      return new TextPlainDocumentBuilder().open(documentId,
                  contentOutputStream,
                  nullable(contentEncoding, DEFAULT_CONTENT_ENCODING),
                  SUPPORTED_CONTENT_TYPE)
            .build();
   }

   public static TextPlainDocument open(DocumentPath documentId, Charset contentEncoding) {
      return open(documentId, null, contentEncoding);
   }

   public static TextPlainDocument open(DocumentPath documentId, OutputStream contentOutputStream) {
      return open(documentId, contentOutputStream, null);
   }

   public static TextPlainDocument open(DocumentPath documentId) {
      return open(documentId, null, null);
   }

   @Override
   public TextPlainDocument write(byte[] buffer, int offset, int length) {
      return super.write(buffer, offset, length);
   }

   @Override
   public TextPlainDocument write(byte[] buffer) {
      return super.write(buffer);
   }

   @Override
   public TextPlainDocument write(InputStream inputStream) {
      return super.write(inputStream);
   }

   @Override
   public TextPlainDocument write(String string) {
      return super.write(string);
   }

   @Override
   public TextPlainDocument write(Reader reader) {
      return super.write(reader);
   }

   @Override
   public TextPlainDocument write(Document document) {
      return super.write(document);
   }

   @Override
   public TextPlainDocument writeLn(String string) {
      return super.writeLn(string);
   }

   @Override
   public String lineSeparator() {
      return lineSeparator;
   }

   @Override
   public TextPlainDocument finish() {
      return super.finish();
   }

   @Override
   protected InvariantRule<MimeType> hasValidContentType() {
      return isTypeAndSubtypeEqualTo(value(SUPPORTED_CONTENT_TYPE));
   }

   @Override
   public TextPlainDocumentBuilder documentBuilder() {
      return new TextPlainDocumentBuilder();
   }

   public static TextPlainDocumentBuilder reconstituteBuilder() {
      return new TextPlainDocumentBuilder().reconstitute();
   }

   public static class TextPlainDocumentBuilder
         extends TypedDocumentBuilder<TextPlainDocument, TextPlainDocumentBuilder> {

      private String lineSeparator;

      /**
       * Copy constructor for {@link TextPlainDocument}
       *
       * @param document document to copy
       *
       * @return text plain document builder
       */
      public static TextPlainDocumentBuilder from(Document document) {
         notNull(document, "document");

         return new TextPlainDocumentBuilder().reconstitute().copy(document);
      }

      protected TextPlainDocumentBuilder assumeContentType(boolean assumeContentType, MimeType contentType) {
         return super.assumeContentType(assumeContentType, assumeContentType, contentType);
      }

      @Override
      protected TextPlainDocumentBuilder copy(Document document) {
         return super
               .copy(document)
               .optionalChain(nullableInstanceOf(document, TextPlainDocument.class),
                              (b, d) -> b.lineSeparator(d.lineSeparator));
      }

      @Setter
      public TextPlainDocumentBuilder lineSeparator(String lineSeparator) {
         this.lineSeparator = lineSeparator;
         return this;
      }

      public TextPlainDocumentBuilder systemDependentLineSeparator(Boolean systemDependentLineSeparator) {
         if (Boolean.TRUE.equals(systemDependentLineSeparator)) {
            return lineSeparator(System.lineSeparator());
         } else {
            return this;
         }
      }

      public TextPlainDocumentBuilder systemDependentLineSeparator() {
         return systemDependentLineSeparator(true);
      }

      @Override
      public TextPlainDocument buildDomainObject() {
         return new TextPlainDocument(this);
      }

   }
}
