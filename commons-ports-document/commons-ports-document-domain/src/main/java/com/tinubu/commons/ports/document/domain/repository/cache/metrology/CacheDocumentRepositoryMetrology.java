/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.repository.cache.metrology;

import java.util.Objects;
import java.util.StringJoiner;
import java.util.concurrent.atomic.AtomicLong;

import com.tinubu.commons.ddd2.domain.event.DomainEvent;
import com.tinubu.commons.lang.metrology.Counter;
import com.tinubu.commons.lang.metrology.Gauge;
import com.tinubu.commons.ports.document.domain.metrology.DefaultDocumentRepositoryMetrology;
import com.tinubu.commons.ports.document.domain.repository.cache.event.CacheEvicted;
import com.tinubu.commons.ports.document.domain.repository.cache.event.CacheHit;
import com.tinubu.commons.ports.document.domain.repository.cache.event.CacheLoaded;
import com.tinubu.commons.ports.document.domain.repository.cache.event.CacheMissed;
import com.tinubu.commons.ports.document.domain.repository.cache.event.CacheSaved;

public class CacheDocumentRepositoryMetrology extends DefaultDocumentRepositoryMetrology {

   private final AtomicLong cacheEvicted = new AtomicLong();
   private final AtomicLong cacheHit = new AtomicLong();
   private final AtomicLong cacheLoaded = new AtomicLong();
   private final AtomicLong cacheMissed = new AtomicLong();
   private final AtomicLong cacheSaved = new AtomicLong();
   private final AtomicLong cacheSize = new AtomicLong();

   @Override
   public void accept(DomainEvent event) {
      if (event instanceof CacheEvicted) {
         incrementCacheEvicted(1);
      } else if (event instanceof CacheHit) {
         incrementCacheHit(1);
      } else if (event instanceof CacheLoaded) {
         incrementCacheLoaded(1);
      } else if (event instanceof CacheMissed) {
         incrementCacheMissed(1);
      } else if (event instanceof CacheSaved) {
         incrementCacheSaved(1);
      } else {
         super.accept(event);
      }
   }

   @Counter(description = "Document evicted from cache counter")
   public long cacheEvicted() {
      return cacheEvicted.get();
   }

   public void incrementCacheEvicted(int count) {
      cacheEvicted.addAndGet(count);
   }

   @Counter(description = "Document hit from cache counter")
   public long cacheHit() {
      return cacheHit.get();
   }

   public void incrementCacheHit(int count) {
      cacheHit.addAndGet(count);
   }

   @Counter(description = "Document loaded from source counter")
   public long cacheLoaded() {
      return cacheLoaded.get();
   }

   public void incrementCacheLoaded(int count) {
      cacheLoaded.addAndGet(count);
   }

   @Counter(description = "Document miss from cache counter")
   public long cacheMissed() {
      return cacheMissed.get();
   }

   public void incrementCacheMissed(int count) {
      cacheMissed.addAndGet(count);
   }

   @Counter(description = "Document saved to cache counter")
   public long cacheSaved() {
      return cacheSaved.get();
   }

   public void incrementCacheSaved(int count) {
      cacheSaved.addAndGet(count);
   }

   @Gauge(description = "Cache size gauge")
   public long cacheSize() {
      return cacheSize.get();
   }

   public void setCacheSize(int cacheSize) {
      this.cacheSize.set(cacheSize);
   }

   @Override
   public void resetMetrics() {
      super.resetMetrics();

      cacheEvicted.set(0);
      cacheHit.set(0);
      cacheLoaded.set(0);
      cacheMissed.set(0);
      cacheSaved.set(0);
      cacheSize.set(0);
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      if (!super.equals(o)) return false;
      CacheDocumentRepositoryMetrology that = (CacheDocumentRepositoryMetrology) o;
      return Objects.equals(cacheEvicted.get(), that.cacheEvicted.get())
             && Objects.equals(cacheHit.get(),
                               that.cacheHit.get())
             && Objects.equals(cacheLoaded.get(), that.cacheLoaded.get())
             && Objects.equals(cacheMissed.get(), that.cacheMissed.get())
             && Objects.equals(cacheSaved.get(), that.cacheSaved.get());
   }

   @Override
   public int hashCode() {
      return Objects.hash(super.hashCode(), cacheEvicted, cacheHit, cacheLoaded, cacheMissed, cacheSaved);
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", CacheDocumentRepositoryMetrology.class.getSimpleName() + "[", "]")
            .add(super.toString())
            .add("cacheEvicted=" + cacheEvicted)
            .add("cacheHit=" + cacheHit)
            .add("cacheLoaded=" + cacheLoaded)
            .add("cacheMissed=" + cacheMissed)
            .add("cacheSaved=" + cacheSaved)
            .toString();
   }

}
