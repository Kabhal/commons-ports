/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.repository.cache.event;

import static com.tinubu.commons.ddd2.domain.type.support.DomainObjectSupport.checkInvariants;

import java.time.Duration;

import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;

/**
 * Document has not been found in cache, or is evictable.
 */
public final class CacheMissed extends CacheDocumentEvent {

   public CacheMissed(DocumentRepository repository, DocumentPath documentId, Duration duration) {
      super(repository, documentId, duration);

      checkInvariants(this);
   }

}
