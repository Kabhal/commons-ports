/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.uri;

import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.noParameters;

import java.net.URI;
import java.util.Optional;

import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.ReferencedDocument;
import com.tinubu.commons.ports.document.domain.capability.RepositoryCapability;
import com.tinubu.commons.ports.document.domain.capability.UnsupportedCapabilityException;

/**
 * {@link URI} support for document repositories.
 */
public interface DocumentRepositoryUriAdapter {

   /**
    * Returns referenced document in this repository with lazy-loading of specified document.
    * Returned referenced document has {@link ReferencedDocument#autoCloseRepository()} not set.
    *
    * @param documentUri document URI to reference
    *
    * @return new referenced document
    *
    * @throws InvalidUriException if URI is invalid, or is not supported by this repository. You can
    *       use {@link #supportsUri(RepositoryUri)} to check URI before calling this method
    */
   ReferencedDocument<? extends DocumentRepository> referencedDocumentId(DocumentUri documentUri);

   /**
    * Returns the current repository as a {@link RepositoryUri} with a scheme specific to this
    * repository.
    *
    * @param options URI generation options
    *
    * @return repository-specific absolute URI
    *
    * @throws UnsupportedCapabilityException if implementing repository does not support
    *       {@link RepositoryCapability#REPOSITORY_URI} capability
    */
   // FIXME add operation to return RepositoryUri without options, then use RepositoryUri::toUri(options) and remove options paramater here ?
   RepositoryUri toUri(ExportUriOptions options);

   /**
    * Returns the current repository as a {@link RepositoryUri} with a scheme specific to this
    * repository. No repository parameters are exported.
    *
    * @return repository-specific absolute URI
    *
    * @throws UnsupportedCapabilityException if implementing repository does not support
    *       {@link RepositoryCapability#REPOSITORY_URI} capability
    */
   default RepositoryUri toUri() {
      return toUri(noParameters());
   }

   /**
    * Returns the specified document as a {@link DocumentUri} with a scheme specific to this
    * repository.
    * <p>
    * The specified document is not required to exist on the repository and should not be
    * checked in this operation.
    *
    * @param documentId document identifier
    * @param options URI generation options
    *
    * @return document as a repository-specific absolute URI
    *
    * @throws UnsupportedCapabilityException if implementing repository does not support
    *       {@link RepositoryCapability#DOCUMENT_URI} capability
    */
   // FIXME add operation to return DocumentUri without options, then use DocumentUri::toUri(options) and remove options paramater here ?
   DocumentUri toUri(DocumentPath documentId, ExportUriOptions options);

   /**
    * Returns the specified document as a {@link DocumentUri} with a scheme specific to this
    * repository. No repository parameters are exported.
    * <p>
    * The specified document is not required to exist on the repository and should not be
    * checked in this operation.
    *
    * @param documentId document identifier
    *
    * @return document as a repository-specific absolute URI
    *
    * @throws UnsupportedCapabilityException if implementing repository does not support
    *       {@link RepositoryCapability#DOCUMENT_URI} capability
    */
   default DocumentUri toUri(DocumentPath documentId) {
      return toUri(documentId, noParameters());
   }

   /**
    * Returns {@code true} if the specified repository URI is supported by this repository.
    * <p>
    * A repository URI is supported by this repository if :
    * <ul>
    *    <li>it is compatible</li>
    *    <li>it identifies the same implementation's physical repository</li>
    *    <li>URI can also specify some or all parameters, in this case each parameter value must be equal to
    *     corresponding repository configuration value</li>
    * </ul>
    *
    * @param uri repository URI
    *
    * @return {@code true} if the specified URI is supported by this repository
    *
    * @throws UnsupportedCapabilityException if implementing repository does not support
    *       {@link RepositoryCapability#REPOSITORY_URI} capability
    * @throws InvalidUriException if URI is compatible with this repository but is invalid
    */
   boolean supportsUri(RepositoryUri uri);

   /**
    * Finds document by URI.
    *
    * @param documentUri document URI
    *
    * @return found document or {@link Optional#empty()} if document not found
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    * @throws UnsupportedCapabilityException if implementing repository does not support
    *       {@link RepositoryCapability#DOCUMENT_URI} capability
    * @throws InvalidUriException if URI is invalid, or is not supported by this repository. You can
    *       use {@link #supportsUri(RepositoryUri)} to check URI before calling this method
    * @see #supportsUri(RepositoryUri)
    */
   Optional<Document> findDocumentByUri(DocumentUri documentUri);

   /**
    * Finds document entry by URI.
    *
    * @param documentUri document URI
    *
    * @return found document entry or {@link Optional#empty()} if document not found
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    * @throws UnsupportedCapabilityException if implementing repository does not support
    *       {@link RepositoryCapability#READABLE} capability
    * @throws InvalidUriException if URI is invalid, or is not supported by this repository. You can
    *       use {@link #supportsUri(RepositoryUri)} to check URI before calling this method
    * @see #supportsUri(RepositoryUri)
    */
   Optional<DocumentEntry> findDocumentEntryByUri(DocumentUri documentUri);

}
