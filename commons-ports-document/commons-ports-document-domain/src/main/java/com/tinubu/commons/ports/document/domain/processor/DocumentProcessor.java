/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.processor;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;

/**
 * General document processor.
 * <p>
 * Implementation rationales :
 * <ul>
 *    <li>See {@link DocumentContentProcessor} rationales</li>
 *    <li>Returned document type is useless when used with {@link Document#process(DocumentProcessor)} as the original document type is reconstituted at that time</li>
 * </ul>
 *
 * @see Document#process(DocumentProcessor)
 */
@FunctionalInterface
public interface DocumentProcessor {

   /**
    * Processes specified document.
    *
    * @param document document to process
    *
    * @return new processed document instance
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   Document process(Document document);

   default DocumentProcessor compose(DocumentProcessor before) {
      notNull(before, "before");
      return (Document document) -> process(before.process(document));
   }

   default DocumentProcessor andThen(DocumentProcessor after) {
      notNull(after, "after");
      return (Document document) -> after.process(process(document));
   }

}
