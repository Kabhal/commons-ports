/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.uri;

import static com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery.ExportUriFilter.isAlwaysTrue;

import java.util.function.BiPredicate;
import java.util.function.Function;

import com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery.Parameter;

public class DefaultExportUriOptions implements ExportUriOptions {

   private static final DefaultExportUriOptions EXPORT_NO_PARAMETERS =
         new DefaultExportUriOptions(false, false, true, isAlwaysTrue());
   private static final DefaultExportUriOptions EXPORT_DEFAULT_PARAMETERS =
         new DefaultExportUriOptions(true, false, true, isAlwaysTrue());
   private static final DefaultExportUriOptions EXPORT_SENSITIVE_PARAMETERS =
         new DefaultExportUriOptions(true, false, false, isAlwaysTrue());

   protected final boolean exportParameters;
   protected final boolean excludeSensitiveParameters;
   protected final boolean forceDefaultValues;
   protected final BiPredicate<ParameterizedQuery, Parameter> filterParameters;

   protected DefaultExportUriOptions(boolean exportParameters,
                                     boolean forceDefaultValues,
                                     boolean excludeSensitiveParameters,
                                     BiPredicate<ParameterizedQuery, Parameter> filterParameters) {
      this.exportParameters = exportParameters;
      this.forceDefaultValues = forceDefaultValues;
      this.excludeSensitiveParameters = excludeSensitiveParameters;
      this.filterParameters = filterParameters;
   }

   protected DefaultExportUriOptions() {
      this(false, false, true, isAlwaysTrue());
   }

   /**
    * Sets export URI options to export all parameters.
    *
    * @param excludeSensitiveParameters whether to exclude sensitive parameters
    *
    * @return export URI options
    */
   public static DefaultExportUriOptions allParameters(boolean excludeSensitiveParameters) {
      return new DefaultExportUriOptions(true, true, excludeSensitiveParameters, isAlwaysTrue());
   }

   /**
    * Sets export URI options to export all parameters, excluding sensitive parameters.
    *
    * @return export URI options
    */
   public static DefaultExportUriOptions allParameters() {
      return allParameters(true);
   }

   /**
    * Sets export URI options to export no parameters.
    *
    * @return export URI options
    */
   public static DefaultExportUriOptions noParameters() {
      return EXPORT_NO_PARAMETERS;
   }

   /**
    * Sets export URI options to export the minimum set of parameters to recreate a repository with the same
    * configuration.
    * Sensitive parameters are not exported.
    * Parameters with default values are not exported.
    *
    * @return export URI options
    */
   public static DefaultExportUriOptions defaultParameters() {
      return EXPORT_DEFAULT_PARAMETERS;
   }

   /**
    * Sets export URI options to export the minimum set of parameters to recreate a repository with the same
    * configuration.
    * Sensitive parameters are not exported.
    *
    * @param forceDefaultValues whether to force export parameters with default values
    *
    * @return export URI options
    */
   public static DefaultExportUriOptions defaultParameters(boolean forceDefaultValues) {
      return new DefaultExportUriOptions(true, forceDefaultValues, true, isAlwaysTrue());
   }

   /**
    * Sets export URI options to export the minimum set of parameters to recreate a repository with the same
    * configuration.
    * Sensitive parameters are also exported.
    * Parameters with default values are not exported.
    *
    * @return export URI options
    */
   public static DefaultExportUriOptions sensitiveParameters() {
      return EXPORT_SENSITIVE_PARAMETERS;
   }

   /**
    * Sets export URI options to export the minimum set of parameters to recreate a repository with the same
    * configuration.
    * Sensitive parameters are also exported.
    *
    * @param forceDefaultValues whether to force export parameters with default values
    *
    * @return export URI options
    */
   public static DefaultExportUriOptions sensitiveParameters(boolean forceDefaultValues) {
      return new DefaultExportUriOptions(true, forceDefaultValues, false, isAlwaysTrue());
   }

   public DefaultExportUriOptions exportParameters(boolean exportParameters) {
      return new DefaultExportUriOptions(exportParameters,
                                         forceDefaultValues,
                                         excludeSensitiveParameters,
                                         filterParameters);
   }

   public DefaultExportUriOptions forceDefaultValues(boolean forceDefaultValues) {
      return new DefaultExportUriOptions(exportParameters,
                                         forceDefaultValues,
                                         excludeSensitiveParameters,
                                         filterParameters);
   }

   public DefaultExportUriOptions excludeSensitiveParameters(boolean excludeSensitiveParameters) {
      return new DefaultExportUriOptions(exportParameters,
                                         forceDefaultValues,
                                         excludeSensitiveParameters,
                                         filterParameters);
   }

   public DefaultExportUriOptions filterParameters(BiPredicate<ParameterizedQuery, Parameter> filterParameters) {
      return new DefaultExportUriOptions(exportParameters,
                                         forceDefaultValues,
                                         excludeSensitiveParameters,
                                         filterParameters);
   }

   @Override
   public boolean exportParameters() {
      return this.exportParameters;
   }

   @Override
   public boolean forceDefaultValues() {
      return forceDefaultValues;
   }

   @Override
   public boolean excludeSensitiveParameters() {
      return this.excludeSensitiveParameters;
   }

   @Override
   public BiPredicate<ParameterizedQuery, Parameter> filterParameters() {
      return this.filterParameters;
   }

   @Override
   public ExportUriOptions filterParameters(Function<? super BiPredicate<ParameterizedQuery, Parameter>, ? extends BiPredicate<ParameterizedQuery, Parameter>> mapper) {
      return new DefaultExportUriOptions(exportParameters,
                                         forceDefaultValues,
                                         excludeSensitiveParameters,
                                         mapper.apply(filterParameters));
   }
}
