/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.repository.cache.eviction;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.ArrayRules.list;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.hasNoNullElements;
import static com.tinubu.commons.lang.util.CollectionUtils.listConcat;
import static com.tinubu.commons.lang.util.OptionalUtils.optionalInstanceOf;
import static com.tinubu.commons.lang.util.StreamUtils.stream;

import java.util.stream.Stream;

/**
 * Composite cache eviction strategy.
 *
 * @implSpec When using composite strategy, it's possible that an evicted entry in a strategy is still
 *       present in another strategy context, hence, that the same document is evicted several times.
 *       This requires callback to be idempotent.
 */
public abstract class CompositeCacheEvictionStrategy implements CacheEvictionStrategy {

   @Override
   public OrCacheEvictionStrategy or(CacheEvictionStrategy... evictionStrategies) {
      validate(evictionStrategies, "evictionStrategies", list(hasNoNullElements())).orThrow();

      Stream<CacheEvictionStrategy> associativeStrategies =
            optionalInstanceOf(this, OrCacheEvictionStrategy.class)
                  .map(t -> stream(t.evictionStrategies))
                  .orElseGet(() -> stream(this));

      return OrCacheEvictionStrategy.of(listConcat(associativeStrategies, stream(evictionStrategies)));
   }

}

