/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.capability;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import com.tinubu.commons.ports.document.domain.DocumentRepository;

/**
 * Error because of missing repository capability.
 * <p>
 * If a repository function can throw such exception, for any reason, this capability must be removed from
 * {@link DocumentRepository#capabilities()}.
 */
public class UnsupportedCapabilityException extends RuntimeException {
   private static final String DEFAULT_MESSAGE = "Unsupported '%s' (%s) capability";

   private final RepositoryCapability capability;

   public UnsupportedCapabilityException(RepositoryCapability capability, String message) {
      super(String.format(DEFAULT_MESSAGE, capability.name(), capability.description()) + ": " + message);
      this.capability = notNull(capability, "capability");
   }

   public UnsupportedCapabilityException(RepositoryCapability capability) {
      super(String.format(DEFAULT_MESSAGE, capability.name(), capability.description()));
      this.capability = notNull(capability, "capability");
   }

   public RepositoryCapability capability() {
      return capability;
   }
}
