/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.repository.sidemetadata;

import java.util.Optional;

import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentEntrySpecification;
import com.tinubu.commons.ports.document.domain.DocumentMetadata;
import com.tinubu.commons.ports.document.domain.DocumentPath;

/**
 * Side metadata storage.
 */
public interface SideMetadataStorage {

   /**
    * Loads side metadata for the specified document, if any.
    *
    * @param documentId document to load side metadata for
    *
    * @return side metadata, or {@link Optional#empty}
    */
   Optional<DocumentMetadata> loadMetadata(DocumentPath documentId);

   /**
    * Saves side metadata from the specified document.
    *
    * @param documentEntry document to save side metadata from
    *
    * @implSpec side metadata should be overridden if it already exists
    */
   void saveMetadata(DocumentEntry documentEntry);

   /**
    * Deletes side metadata from the specified document.
    *
    * @param documentId document to delete side metadata for
    */
   void deleteMetadata(DocumentPath documentId);

   /**
    * Returns whether the specified document is a side metadata artifact document, or an original document.
    *
    * @param documentId document to check
    *
    * @return {@code true} if specified document is a side metadata artifact document
    */
   boolean isSideMetadata(DocumentPath documentId);

   /**
    * Defines the {@link DocumentEntrySpecification} matching a side-metadata document used by this storage.
    * If this storage strategy does not involve side-metadata extra documents, returns an empty specification.
    *
    * @return document entry specification matching side-metadata documents
    */
   DocumentEntrySpecification sideMetadataSpecification();

   /**
    * Clean orphans side-metadata documents.
    * All side-metadata documents, associated to a document that no longer exists, are cleaned.
    */
   void cleanOrphanSideMetadata();

   /**
    * Clean all side-metadata documents.
    */
   void cleanAllSideMetadata();
}
