/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.repository.sidemetadata;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.EqualsRules.isEqualTo;
import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.mimeType;
import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.parseMimeType;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.apache.commons.lang3.StringUtils.isBlank;

import java.io.OutputStream;
import java.nio.file.Path;
import java.time.Instant;
import java.util.Iterator;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.lang.util.Pair;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentMetadata;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.typed.TypedDocument;

/**
 * Side metadata {@link Document} to store the metadata of a referenced document.
 */
public class SideMetadataDocument extends TypedDocument<SideMetadataDocument> {

   static final MimeType CONTENT_TYPE = mimeType("application", "document-metadata", UTF_8);
   protected static final String LINE_SEPARATOR = "\n";
   protected static final Pattern METADATA_LINE_SEPARATOR = Pattern.compile("=");

   protected SideMetadataDocument(SideMetadataDocumentBuilder builder) {
      super(builder);
   }

   /**
    * Creates a new text document from existing document. Source document metadata are not changed.
    *
    * @param document existing document to base on
    * @param assumeContentType whether to assume document as a side-metadata document if content-type is
    *       unknown or has no encoding
    *
    * @return new message document
    */
   public static SideMetadataDocument of(Document document, boolean assumeContentType) {
      notNull(document, "document");

      return new SideMetadataDocumentBuilder()
            .reconstitute()
            .copy(document)
            .assumeContentType(assumeContentType, CONTENT_TYPE)
            .build();
   }

   public static SideMetadataDocument of(Document document) {
      return of(document, false);
   }

   public static SideMetadataDocument open(DocumentPath documentId, OutputStream contentOutputStream) {
      notNull(documentId, "documentId");

      return new SideMetadataDocumentBuilder()
            .open(documentId, contentOutputStream, null, CONTENT_TYPE)
            .build();
   }

   public static SideMetadataDocument open(DocumentPath documentId) {
      return open(documentId, null);
   }

   public SideMetadataDocument write(DocumentEntry documentEntry) {
      validate(this, isNotFinished()).orThrow();

      try {
         writeMetadata("document-id", documentEntry.documentId().stringValue());
         writeMetadata("document-path", documentEntry.metadata().documentPath());
         writeMetadata("creation-date", documentEntry.metadata().creationDate().map(Instant::toEpochMilli));
         writeMetadata("last-update-date",
                       documentEntry.metadata().lastUpdateDate().map(Instant::toEpochMilli));
         writeMetadata("content-type", documentEntry.metadata().contentType());
         writeMetadata("content-size", documentEntry.metadata().contentSize());
         documentEntry.metadata().attributes().forEach((key, value) -> {
            writeMetadata("attribute", key + "=" + value);
         });

         return this;
      } finally {
         finish();
      }
   }

   public DocumentMetadata read(DocumentPath documentId) {
      validate(this, isNotFinished()).orThrow();

      try {
         DocumentMetadataBuilder builder = new DocumentMetadataBuilder()
               .<DocumentMetadataBuilder>reconstitute()
               .documentPath(documentId.value());

         readMetadata().forEach(metadata -> {
            switch (metadata.getKey()) {
               case "document-id":
                  if (!documentId.value().equals(Path.of(metadata.getValue()))) {
                     throw new IllegalStateException(String.format(
                           "Corrupted '%s' side metadata : metadata '%s' document-id does not match current '%s' document id",
                           this.documentId.stringValue(),
                           metadata.getValue(),
                           documentId.stringValue()));
                  }
                  break;
               case "document-path":
                  builder.documentPath(Path.of(metadata.getValue()));
                  break;
               case "creation-date":
                  builder.creationDate(Instant.ofEpochMilli(Long.parseLong(metadata.getValue())));
                  break;
               case "last-update-date":
                  builder.lastUpdateDate(Instant.ofEpochMilli(Long.parseLong(metadata.getValue())));
                  break;
               case "content-type":
                  builder.contentType(parseMimeType(metadata.getValue()));
                  break;
               case "content-size":
                  builder.contentSize(Long.parseLong(metadata.getValue()));
                  break;
               case "attribute":
                  String[] parts = METADATA_LINE_SEPARATOR.split(metadata.getValue(), 2);
                  if (parts.length != 2 || isBlank(parts[0])) {
                     throw new IllegalStateException(String.format(
                           "Corrupted '%s' side metadata line : Bad '%s' attribute value format",
                           this.documentId.stringValue(),
                           metadata.getValue()));
                  } else {
                     builder.addAttribute(parts[0], parts[1]);
                  }
                  break;
               default:
                  throw new IllegalStateException(String.format(
                        "Corrupted '%s' side metadata line : Unknown '%s' key",
                        this.documentId.stringValue(),
                        metadata.getKey()));
            }
         });

         return builder.build();
      } finally {
         content.close();
      }
   }

   @Override
   public String lineSeparator() {
      return LINE_SEPARATOR;
   }

   @Override
   protected InvariantRule<MimeType> hasValidContentType() {
      return isEqualTo(value(CONTENT_TYPE));
   }

   protected SideMetadataDocument writeMetadata(String key, Object value) {
      return writeMetadata(key, nullable(value));
   }

   @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
   protected <T> SideMetadataDocument writeMetadata(String key, Optional<T> value) {
      value.ifPresent(v -> writeLn(key + "=" + String.valueOf(v)));

      return this;
   }

   /**
    * Returns metadata lines as a stream of (metadata-key,metadata-value) pairs.
    *
    * @throws IllegalStateException if a metadata line is corrupted
    */
   protected Stream<Pair<String, String>> readMetadata() {

      return stream(() -> new Iterator<>() {

         String line;

         @Override
         public boolean hasNext() {
            line = readLn();

            return line != null;
         }

         @Override
         public Pair<String, String> next() {
            if (line == null) {
               throw new IllegalStateException();
            }
            String[] parts = METADATA_LINE_SEPARATOR.split(line, 2);
            if (parts.length != 2 || isBlank(parts[0])) {
               throw new IllegalStateException(String.format("Corrupted '%s' side metadata line : '%s'",
                                                             documentId.stringValue(),
                                                             line));
            } else {
               return Pair.of(parts[0], parts[1]);
            }
         }
      });
   }

   @Override
   public SideMetadataDocumentBuilder documentBuilder() {
      return new SideMetadataDocumentBuilder();
   }

   public static SideMetadataDocumentBuilder reconstituteBuilder() {
      return new SideMetadataDocumentBuilder().reconstitute();
   }

   public static class SideMetadataDocumentBuilder
         extends TypedDocumentBuilder<SideMetadataDocument, SideMetadataDocumentBuilder> {

      /**
       * Copy constructor for {@link SideMetadataDocument}
       *
       * @param document document to copy
       *
       * @return side metadata document builder
       */
      public static SideMetadataDocumentBuilder from(Document document) {
         notNull(document, "document");

         return new SideMetadataDocumentBuilder().reconstitute().copy(document);
      }

      @Override
      protected SideMetadataDocumentBuilder copy(Document document) {
         return super.copy(document);
      }

      protected SideMetadataDocumentBuilder assumeContentType(boolean assumeContentType,
                                                              MimeType contentType) {
         return super.assumeContentType(assumeContentType, assumeContentType, contentType);
      }

      @Override
      public SideMetadataDocument buildDomainObject() {
         return new SideMetadataDocument(this);
      }

   }
}
