/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.processor.replacer;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.NumberRules.isStrictlyPositive;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.ports.document.domain.InputStreamDocumentContent.InputStreamDocumentContentBuilder;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.function.Function;

import com.tinubu.commons.lang.io.replacer.GeneralReplacerReader;
import com.tinubu.commons.lang.util.MechanicalSympathy;
import com.tinubu.commons.ports.document.domain.DocumentContent;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent;
import com.tinubu.commons.ports.document.domain.LoadedDocumentContent;
import com.tinubu.commons.ports.document.domain.processor.DocumentContentProcessor;

/**
 * Document content processor to replace simple placeholders {@code ${key}} with specified model values.
 * Document must not be in binary format. {@link #DEFAULT_CONTENT_ENCODING} is assumed for document without
 * explicit content-type.
 */
public class PlaceholderReplacer implements DocumentContentProcessor {

   protected static final Charset DEFAULT_CONTENT_ENCODING = StandardCharsets.UTF_8;
   protected static final int DEFAULT_BUFFER_SIZE = MechanicalSympathy.generalBufferSize();
   protected static final String DEFAULT_PLACEHOLDER_BEGIN = "${";
   protected static final String DEFAULT_PLACEHOLDER_END = "}";

   protected final Function<String, Object> replacementFunction;
   protected final String placeholderBegin;
   protected final String placeholderEnd;
   protected final int bufferSize;
   protected final Charset defaultContentEncoding;

   protected PlaceholderReplacer(Function<String, Object> replacementFunction,
                                 String placeholderBegin,
                                 String placeholderEnd,
                                 int bufferSize,
                                 Charset defaultContentEncoding) {
      this.replacementFunction = validate(replacementFunction, "replacementFunction", isNotNull()).orThrow();
      this.placeholderBegin = validate(placeholderBegin, "placeholderBegin", isNotBlank()).orThrow();
      this.placeholderEnd = validate(placeholderEnd, "placeholderEnd", isNotBlank()).orThrow();
      this.bufferSize = validate(bufferSize, "bufferSize", isStrictlyPositive()).orThrow();
      this.defaultContentEncoding =
            validate(defaultContentEncoding, "defaultContentEncoding", isNotNull()).orThrow();
   }

   public static PlaceholderReplacer of(Function<String, Object> replacementFunction) {
      return new PlaceholderReplacer(replacementFunction,
                                     DEFAULT_PLACEHOLDER_BEGIN,
                                     DEFAULT_PLACEHOLDER_END,
                                     DEFAULT_BUFFER_SIZE,
                                     DEFAULT_CONTENT_ENCODING);
   }

   public PlaceholderReplacer bufferSize(int bufferSize) {
      return new PlaceholderReplacer(replacementFunction,
                                     placeholderBegin,
                                     placeholderEnd,
                                     bufferSize,
                                     defaultContentEncoding);
   }

   public PlaceholderReplacer delimiters(String placeholderBegin, String placeholderEnd) {
      return new PlaceholderReplacer(replacementFunction,
                                     placeholderBegin,
                                     placeholderEnd,
                                     bufferSize,
                                     defaultContentEncoding);
   }

   public PlaceholderReplacer defaultContentEncoding(Charset defaultContentEncoding) {
      return new PlaceholderReplacer(replacementFunction,
                                     placeholderBegin,
                                     placeholderEnd,
                                     bufferSize,
                                     defaultContentEncoding);
   }

   @Override
   public DocumentContent process(DocumentContent documentContent) {
      Charset encoding = documentContent.contentEncoding().orElse(defaultContentEncoding);

      if (documentContent instanceof InputStreamDocumentContent
          || documentContent instanceof LoadedDocumentContent) {

         return InputStreamDocumentContentBuilder
               .from(documentContent)
               .content(new GeneralReplacerReader(documentContent.readerContent(encoding),
                                                  bs -> new com.tinubu.commons.lang.io.replacer.PlaceholderReplacer(
                                                        bs,
                                                        placeholderBegin,
                                                        placeholderEnd,
                                                        (__, content) -> replacementFunction.apply(content)),
                                                  bufferSize), encoding)
               .contentSize(null) // content-size will change, but is unknown
               .build();
      } else {
         throw new IllegalStateException(String.format("Unsupported '%s' content type",
                                                       documentContent.getClass().getName()));
      }
   }

}

