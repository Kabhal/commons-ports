/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.repository;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.hasNoNullElements;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotAbsolute;
import static com.tinubu.commons.lang.util.CheckedPredicate.nonNull;
import static com.tinubu.commons.lang.util.CollectionUtils.collectionConcat;
import static com.tinubu.commons.lang.util.CollectionUtils.collectionEquals;
import static com.tinubu.commons.lang.util.CollectionUtils.collectionIntersection;
import static com.tinubu.commons.lang.util.CollectionUtils.immutable;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.StreamUtils.reversedStream;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.util.XCheckedRunnable.noop;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.DOCUMENT_URI;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.REPOSITORY_URI;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.WRITABLE;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

import java.nio.file.Path;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.function.Predicate;
import java.util.stream.Stream;

import com.tinubu.commons.ddd2.domain.repository.Repository;
import com.tinubu.commons.ddd2.domain.specification.Specification;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.lang.mapper.SchwartzianTransformer;
import com.tinubu.commons.lang.util.CollectionUtils;
import com.tinubu.commons.lang.util.XCheckedRunnable;
import com.tinubu.commons.lang.validation.CheckReturnValue;
import com.tinubu.commons.ports.document.domain.AbstractDocumentRepository;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata;
import com.tinubu.commons.ports.document.domain.ReferencedDocument;
import com.tinubu.commons.ports.document.domain.capability.RepositoryCapability;
import com.tinubu.commons.ports.document.domain.capability.UnsupportedCapabilityException;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.ExportUriOptions;
import com.tinubu.commons.ports.document.domain.uri.RepositoryUri;

/**
 * Union {@link DocumentRepository} adapter implementation.
 * Zero or more repositories can be registered as "layers" in this meta-repository. Documents are searched
 * from upper layers to lower layers until one matches.
 * <p>
 * Some operations are restricted to the upper layer :
 * <ul>
 *    <li>open/save/delete document</li>
 *    <li>{@link #toUri(ExportUriOptions)}/{@link #toUri(DocumentPath, ExportUriOptions)}, {@link #supportsUri(RepositoryUri)}</li>
 * </ul>
 * <p>
 * Capabilities are defined as following :
 * <ul>
 *    <li>an empty union repository has no capabilities</li>
 *    <li>{@link RepositoryCapability#WRITABLE}, {@link RepositoryCapability#REPOSITORY_URI}, {@link RepositoryCapability#DOCUMENT_URI} capabilities are defined only if upper layer has defined some or all of them</li>
 *    <li>other capabilities are restrictively set to the intersection of layer capabilities</li>
 * </ul>
 *
 * @implSpec Immutable class implementation.
 */
public class UnionDocumentRepository extends AbstractDocumentRepository<UnionDocumentRepository> {

   /**
    * Default layer close strategy on repository close. All layer satisfying the predicate are closed.
    */
   private static final Predicate<DocumentRepository> DEFAULT_LAYER_CLOSE_STRATEGY = __ -> false;
   /**
    * Union repository layers, ordered from the upper to the lower layer. Lower layers have lesser priority.
    */
   protected final List<DocumentRepository> layers;
   protected final Predicate<DocumentRepository> layerCloseStrategy;

   protected UnionDocumentRepository(List<DocumentRepository> layers,
                                     Predicate<DocumentRepository> layerCloseStrategy) {
      validate(layers, "layers", hasNoNullElements()).orThrow();
      validate(layerCloseStrategy, "layerCloseStrategy", isNotNull()).orThrow();

      this.layers = immutable(layers);
      this.layerCloseStrategy = layerCloseStrategy;
   }

   /**
    * Creates an empty union repository.
    *
    * @return empty union repository
    */
   public static UnionDocumentRepository ofEmpty() {
      return ofLayers(list());
   }

   /**
    * Creates a union repository from layers.
    *
    * @param layers layers ordered from the lower to the upper layers. Lower layers have
    *       lesser priority.
    *
    * @return union repository with specified layers
    */
   public static UnionDocumentRepository ofLayers(DocumentRepository... layers) {
      return ofLayers(nullable(layers).map(CollectionUtils::list).orElse(null));
   }

   /**
    * Creates a union repository from layers, ordered from the lower to the upper layers. Lower layers have
    * lesser priority.
    * {@link NoopDocumentRepository} are filtered out as an optimization.
    *
    * @param layers layers ordered from the lower to the upper layers. Lower layers have
    *       lesser priority.
    *
    * @return union repository with specified layers
    */
   public static UnionDocumentRepository ofLayers(List<DocumentRepository> layers) {
      List<DocumentRepository> unionLayers = nullable(layers,
                                                      ls -> list(reversedStream(ls).filter(layer -> !(layer instanceof NoopDocumentRepository))));

      return new UnionDocumentRepository(unionLayers, DEFAULT_LAYER_CLOSE_STRATEGY);
   }

   /**
    * Changes the layer close strategy to apply at {@link #close()}. By default, no layer is closed,
    * consequently, this is the caller responsibility to later close the layer repositories. If a layer is a
    * {@link UnionDocumentRepository} it is always recursively closed, you can't change this behavior.
    *
    * @param layerCloseStrategy layer close strategy to apply
    *
    * @return new repository with specified strategy set
    */
   public UnionDocumentRepository layerCloseStrategy(Predicate<DocumentRepository> layerCloseStrategy) {
      Predicate<DocumentRepository> isUnionRepository = layer -> layer instanceof UnionDocumentRepository;

      return new UnionDocumentRepository(layers, isUnionRepository.or(layerCloseStrategy));
   }

   /**
    * Appends ("or") the layer close strategy to apply at {@link #close()}. By default, no layer is closed,
    * consequently, this is the caller responsibility to later close the layer repositories.If a layer
    * is a {@link UnionDocumentRepository} it is always recursively closed, you can't change this behavior.
    * Practically, the operation does an {@link Predicate#or(Predicate)} between current and specified
    * strategy.
    *
    * @param layerCloseStrategy layer close strategy to append ("or") to current strategy
    *
    * @return new repository with specified strategy set
    */
   public UnionDocumentRepository appendLayerCloseStrategy(Predicate<DocumentRepository> layerCloseStrategy) {
      return layerCloseStrategy(this.layerCloseStrategy.or(layerCloseStrategy));
   }

   /**
    * Returns current layer close strategy.
    *
    * @return current layer close strategy
    */
   public Predicate<DocumentRepository> layerCloseStrategy() {
      return layerCloseStrategy;
   }

   /**
    * Returns upper layer.
    *
    * @return upper layer, or {@link Optional#empty} if there's no layers
    */
   public Optional<DocumentRepository> upperLayer() {
      return layers.stream().findFirst();
   }

   @Override
   public HashSet<RepositoryCapability> capabilities() {
      return upperLayer().map(ul -> {
         Predicate<RepositoryCapability> upperLayerExclusiveCapability =
               c -> c == WRITABLE || c == REPOSITORY_URI || c == DOCUMENT_URI;
         HashSet<RepositoryCapability> layersIntersectionCapabilities =
               collectionIntersection(HashSet::new, stream(layers).map(DocumentRepository::capabilities));

         return collectionConcat(HashSet::new,
                                 stream(layersIntersectionCapabilities).filter(upperLayerExclusiveCapability.negate()),
                                 stream(ul.capabilities()).filter(upperLayerExclusiveCapability));
      }).orElse(new HashSet<>());
   }

   @Override
   public boolean sameRepositoryAs(Repository<Document, DocumentPath> documentRepository) {
      validate(documentRepository, "documentRepository", isNotNull()).orThrow();

      return documentRepository instanceof UnionDocumentRepository
             && collectionEquals(((UnionDocumentRepository) documentRepository).layers,
                                 layers,
                                 Repository::sameRepositoryAs);
   }

   /**
    * {@inheritDoc}
    * <p>
    * Sub path operation is applied to all layers, an {@link UnsupportedCapabilityException} is thrown if one
    * of them is not compatible.
    */
   @Override
   public UnionDocumentRepository subPath(Path subPath, boolean shareContext) {
      return new UnionDocumentRepository(list(stream(layers).map(layer -> layer.subPath(subPath,
                                                                                        shareContext))),
                                         layerCloseStrategy);
   }

   @Override
   @CheckReturnValue
   public Optional<Document> openDocument(DocumentPath documentId,
                                          boolean overwrite,
                                          boolean append,
                                          OpenDocumentMetadata metadata) {
      return upperLayer().flatMap(upperLayer -> upperLayer.openDocument(documentId,
                                                                        overwrite,
                                                                        append,
                                                                        metadata));
   }

   @Override
   public Optional<Document> findDocumentById(DocumentPath documentId) {
      notNull(documentId, "documentId");

      return stream(layers).flatMap(layer -> stream(layer.findDocumentById(documentId))).findFirst();
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryById(DocumentPath documentId) {
      notNull(documentId, "documentId");

      return stream(layers).flatMap(layer -> stream(layer.findDocumentEntryById(documentId))).findFirst();
   }

   /**
    * {@inheritDoc}
    * Search documents on all layers. If documents with the same identifier are found on multiple layers,
    * only the upper layer document is returned.
    *
    * @implNote We use a {@code map(..orElse(null)).filter(Objects::nonNull} scheme instead of a simpler
    *       Stream::flatMap as an optimization.
    */
   @Override
   public Stream<Document> findDocumentsBySpecification(Path basePath,
                                                        Specification<DocumentEntry> specification) {
      return findDocumentEntriesBySpecification(basePath, specification)
            .map(entry -> findDocumentById(entry.documentId()).orElse(null))
            .filter(nonNull());
   }

   /**
    * {@inheritDoc}
    * Search documents on all layers. If documents with the same identifier are found on multiple layers,
    * only the upper layer document is returned.
    */
   @Override
   public Stream<DocumentEntry> findDocumentEntriesBySpecification(Path basePath,
                                                                   Specification<DocumentEntry> specification) {
      return stream(layers)
            .flatMap(layer -> layer.findDocumentEntriesBySpecification(basePath, specification))
            .map(e -> SchwartzianTransformer.wrap(e, DocumentEntry::documentId))
            .distinct()
            .map(SchwartzianTransformer::unwrap);
   }

   /**
    * {@inheritDoc}
    * <p>
    * Saves document on the upper layer only, if there is at least one layer, otherwise returns
    * {@link Optional#empty}.
    */
   @Override
   @CheckReturnValue
   public Optional<DocumentEntry> saveDocument(Document document, boolean overwrite) {
      notNull(document, "document");

      return upperLayer().flatMap(upperLayer -> upperLayer.saveDocument(document, overwrite));
   }

   /**
    * {@inheritDoc}
    * <p>
    * Saves document on the upper layer only, if there is at least one layer, otherwise returns
    * {@link Optional#empty}.
    */
   @Override
   @CheckReturnValue
   public Optional<Document> saveAndReturnDocument(Document document, boolean overwrite) {
      notNull(document, "document");

      return upperLayer().flatMap(upperLayer -> upperLayer.saveAndReturnDocument(document, overwrite));
   }

   /**
    * {@inheritDoc}
    * <p>
    * Deletes document on the upper layer only, if there is at least one layer, otherwise returns
    * {@link Optional#empty}.
    */
   @Override
   @CheckReturnValue
   public Optional<DocumentEntry> deleteDocumentById(DocumentPath documentId) {
      notNull(documentId, "documentId");

      return upperLayer().flatMap(upperLayer -> upperLayer.deleteDocumentById(documentId));
   }

   /**
    * {@inheritDoc}
    * <p>
    * Deletes documents on the upper layer only, if there is at least one layer, otherwise returns empty list.
    */
   @Override
   @CheckReturnValue
   public List<DocumentEntry> deleteDocumentsBySpecification(Path basePath,
                                                             Specification<DocumentEntry> specification) {
      validate(basePath, "basePath", isNull().orValue(isNotAbsolute().andValue(hasNoTraversal())))
            .and(validate(specification, "specification", isNotNull()))
            .orThrow();

      return upperLayer()
            .map(upperLayer -> upperLayer.deleteDocumentsBySpecification(basePath, specification))
            .orElse(emptyList());
   }

   /**
    * {@inheritDoc}
    * <p>
    * Layers are closed in order, from upper to lower layer.
    * Layer is only closed if it satisfies the {@link #layerCloseStrategy}.
    * In an error occur while closing one layer, an exception will occur, but remaining layers are closed
    * anyway.
    */
   @Override
   public void close() {
      noop()
            .tryFinally(stream(layers)
                              .filter(layerCloseStrategy)
                              .map((DocumentRepository documentRepository) -> XCheckedRunnable.checkedRunnable(
                                    documentRepository::close))
                              .collect(toList()))
            .run();
   }

   /**
    * {@inheritDoc}
    * {@link UnionDocumentRepository} has no URI support by itself, and relies on layers support, hence, this
    * operation returns referenced document on first layer
    * {@link DocumentRepository#supportsUri(RepositoryUri) supporting} specified document URI.
    *
    * @param documentUri document URI to reference
    *
    * @return referenced document in first layer supporting specified document URI.
    */
   @Override
   public ReferencedDocument<? extends DocumentRepository> referencedDocumentId(DocumentUri documentUri) {
      Check.notNull(documentUri, "documentUri");

      return stream(layers)
            .filter(layer -> layer.supportsUri(documentUri))
            .map(layer -> layer.referencedDocumentId(documentUri))
            .findFirst()
            .orElseThrow(() -> new InvalidUriException(documentUri).subMessage("No layer supporting this URI"));
   }

   @Override
   public RepositoryUri toUri(ExportUriOptions options) {
      return upperLayer().map(layer -> layer.toUri(options))
            .orElseThrow(() -> new UnsupportedCapabilityException(REPOSITORY_URI, "No layer available"));
   }

   @Override
   public DocumentUri toUri(DocumentPath documentId, ExportUriOptions options) {
      Check.notNull(documentId, "documentId");
      Check.notNull(options, "options");

      return upperLayer().map(layer -> layer.toUri(documentId, options))
            .orElseThrow(() -> new UnsupportedCapabilityException(DOCUMENT_URI, "No layer available"));
   }

   @Override
   public boolean supportsUri(RepositoryUri uri) {
      Check.notNull(uri, "uri");

      return stream(layers).anyMatch(layer -> layer.supportsUri(uri));
   }

   @Override
   public Optional<Document> findDocumentByUri(DocumentUri documentUri) {
      Check.notNull(documentUri, "documentUri");

      return stream(layers).flatMap(layer -> {
         Stream<Document> layerDocument = stream();
         if (layer.supportsUri(documentUri)) {
            layerDocument = stream(layer.findDocumentByUri(documentUri));
         }
         return layerDocument;
      }).findFirst();
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryByUri(DocumentUri documentUri) {
      Check.notNull(documentUri, "documentUri");

      return stream(layers).flatMap(layer -> {
         Stream<DocumentEntry> layerDocument = stream();
         if (layer.supportsUri(documentUri)) {
            layerDocument = stream(layer.findDocumentEntryByUri(documentUri));
         }
         return layerDocument;
      }).findFirst();
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", UnionDocumentRepository.class.getSimpleName() + "[", "]")
            .add("layers=" + layers)
            .toString();
   }
}
