/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.lazyValue;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.string;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.allSatisfies;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isInRangeInclusive;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isLessThanOrEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.EqualsRules.isEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.MapRules.entrySet;
import static com.tinubu.commons.ddd2.invariant.rules.MapRules.hasNoBlankKey;
import static com.tinubu.commons.ddd2.invariant.rules.MapRules.hasNoNullValue;
import static com.tinubu.commons.ddd2.invariant.rules.MimeTypeRules.optionalCharset;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.fileName;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotAbsolute;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotEmpty;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.length;
import static com.tinubu.commons.ddd2.invariant.rules.TemporalRules.isNotBefore;
import static com.tinubu.commons.lang.datetime.ApplicationClock.nowAsInstant;
import static com.tinubu.commons.lang.util.CollectionUtils.immutable;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.nio.charset.Charset;
import java.nio.file.Path;
import java.time.Duration;
import java.time.Instant;
import java.util.Map;
import java.util.Optional;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.ParameterValue;
import com.tinubu.commons.ddd2.valueformatter.StringValueFormatter;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.beans.Setter;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.lang.mimetype.registry.MimeTypeRegistries;
import com.tinubu.commons.lang.mimetype.registry.MimeTypeRegistry;

/**
 * Document metadata attached to a {@link Document}.
 */
public class DocumentMetadata extends AbstractValue {

   /** Maximum document name length. This value is aligned on POSIX filesystems limits. */
   protected static final int DOCUMENT_NAME_MAX_LENGTH = 255;
   /** Hard-cap content size. */
   protected static final long CONTENT_MAX_SIZE = 2L * 1024 * 1024 * 1024 * 1024;
   /** Default mime-type registry to use for auto-resolver. */
   protected static final MimeTypeRegistry MIME_TYPE_REGISTRY = MimeTypeRegistries.ofDefaults();

   protected final Path documentPath;
   protected final MimeType contentType;
   protected final Charset contentEncoding;
   protected final Long contentSize;
   protected final Instant creationDate;
   protected final Instant lastUpdateDate;
   protected final Map<String, String> attributes;

   protected DocumentMetadata(DocumentMetadataBuilder builder) {
      this.documentPath = normalize(builder.documentPath);
      if (builder.isReconstitute()) {
         this.creationDate = builder.creationDate;
         this.lastUpdateDate = builder.lastUpdateDate;
      } else {
         Instant now = nowAsInstant();
         this.creationDate = now;
         this.lastUpdateDate = now;
      }
      this.attributes = immutable(map(builder.attributes));
      this.contentSize = builder.contentSize;
      this.contentType = resolveContentType(builder).orElse(null);
      this.contentEncoding = nullable(contentType).flatMap(MimeType::charset).orElse(builder.contentEncoding);
   }

   @Override
   @SuppressWarnings("unchecked")
   public Fields<? extends DocumentMetadata> defineDomainFields() {
      ParameterValue<Instant> creationDate = lazyValue(() -> this.creationDate, "creationDate");

      return Fields
            .<DocumentMetadata>builder()
            .superFields((Fields<DocumentMetadata>) super.defineDomainFields())
            .field("documentPath",
                   v -> v.documentPath,
                   new StringValueFormatter(DOCUMENT_NAME_MAX_LENGTH),
                   isNotEmpty()
                         .andValue(isNotAbsolute())
                         .andValue(hasNoTraversal())
                         .andValue(fileName(string(isNotBlank().andValue(length(isLessThanOrEqualTo(value(
                               DOCUMENT_NAME_MAX_LENGTH,
                               "DOCUMENT_NAME_MAX_LENGTH"))))))))
            .field("contentType", v -> v.contentType)
            .field("contentSize",
                   v -> v.contentSize,
                   isNull().orValue(isInRangeInclusive(value(0L),
                                                       value(CONTENT_MAX_SIZE, "CONTENT_MAX_SIZE"))))
            .field("creationDate", v -> v.creationDate)
            .field("lastUpdateDate",
                   v -> v.lastUpdateDate,
                   isNull().orValue(isNotBefore(creationDate).ifIsSatisfied(creationDate::nonNull)))
            .field("attributes",
                   v -> v.attributes,
                   entrySet(allSatisfies(hasNoBlankKey().andValue(hasNoNullValue()))))
            .build();
   }

   /**
    * Document logical path.
    * This path is an abstraction of a hierarchical representation, not actually linked to a filesystem
    * representation.
    * Document path must be relative.
    * <p>
    * It represents a logical name for this document, not a technical name or identifier.
    * The underlying storage system must use the {@link Document#documentId()} for storage, not this name
    * that is not guaranteed to be unique.
    *
    * @return document path
    *
    * @see #documentName()
    */
   @Getter
   public Path documentPath() {
      return documentPath;
   }

   /**
    * Returns document logical name, without relative path if any.
    *
    * @return name of document
    */
   public String documentName() {
      return documentPath != null ? documentPath.getFileName().toString() : null;
   }

   /**
    * Checks and resolves document content-type from builder parameters.
    * Resulting {@link MimeType} includes charset parameter for content encoding if possible.
    * If charset is set in both contentType and contentEncoding, the value must match.
    *
    * @param builder builder
    *
    * @return resolved content-type, never {@code null}
    */
   // FIXME should resolveContentType auto-detect only if !reconstitute ? => many breaks, probably :/
   protected Optional<MimeType> resolveContentType(DocumentMetadataBuilder builder) {
      Optional<MimeType> mimeType;

      if (builder.contentType == null) {
         MimeTypeRegistry mimeTypeRegistry = nullable(builder.mimeTypeRegistry, MIME_TYPE_REGISTRY);
         mimeType = nullable(builder.documentPath).flatMap(mimeTypeRegistry::mimeType);
      } else {
         mimeType = optional(builder.contentType);
      }

      mimeType = mimeType.map(mt -> {
         if (!mt.charset().isPresent() && builder.contentEncoding != null) {
            mt = mt.charset(builder.contentEncoding);
         }

         ParameterValue<Charset> contentEncoding = value(builder.contentEncoding, "contentEncoding");
         validate(mt,
                  "contentType",
                  optionalCharset(isEqualTo(contentEncoding).ifNonNull(contentEncoding))).orThrow();

         return mt;
      });

      return mimeType;
   }

   /**
    * Document content-type.
    * Never {@code null}, returns {@code application/octet-stream} if content-type is not set.
    *
    * @return document content-type
    */
   @Getter
   public Optional<MimeType> contentType() {
      return nullable(contentType);
   }

   /**
    * Document content-type without parameters (charset, ...), as a simple type + subtype mime-type.
    *
    * @return document mime-type without parameters (charset, ...)
    */
   public Optional<MimeType> simpleContentType() {
      return nullable(contentType).map(MimeType::strippedParameters);
   }

   /**
    * Document content encoding if set has attribute in content type.
    *
    * @return document content encoding or {@link Optional#empty}
    */
   public Optional<Charset> contentEncoding() {
      return nullable(contentEncoding);
   }

   /**
    * Content size in bytes, if available.
    *
    * @return content size or {@link Optional#empty} if content size is not available
    */
   @Getter
   public Optional<Long> contentSize() {
      return nullable(contentSize);
   }

   /**
    * Document creation date.
    *
    * @return document creation date
    */
   @Getter
   public Optional<Instant> creationDate() {
      return nullable(creationDate);
   }

   /**
    * Document last update date.
    *
    * @return document last update date
    */
   @Getter
   public Optional<Instant> lastUpdateDate() {
      return nullable(lastUpdateDate);
   }

   /**
    * Document extra attributes.
    *
    * @return document extra attributes
    */
   @Getter
   public Map<String, String> attributes() {
      return attributes;
   }

   /**
    * Document extra attribute value for specified key.
    *
    * @param key attribute key
    *
    * @return attribute value or {@link Optional#empty} if attribute not set
    */
   public Optional<String> attribute(String key) {
      return nullable(attributes.get(key));
   }

   /**
    * Creation date expiration predicate.
    *
    * @param validity validity duration since creation date before document is considered expired
    *
    * @return {@code true} if document is expired, or {@link Optional#empty} if no metadata available for this
    *       document
    */
   public Optional<Boolean> creationDateExpired(Duration validity) {
      return creationDate().map(cd -> !cd.plus(validity).isAfter(nowAsInstant()));
   }

   /**
    * Last update date expiration predicate.
    *
    * @param validity validity duration since last update date before document is considered expired
    *
    * @return {@code true} if document is expired, or {@link Optional#empty} if no metadata available for this
    *       document
    */
   public Optional<Boolean> lastUpdateDateExpired(Duration validity) {
      return creationDate().map(lud -> !lud.plus(validity).isAfter(nowAsInstant()));
   }

   /**
    * Normalizes document path.
    *
    * @param documentPath document path
    *
    * @return normalized document path
    */
   protected static Path normalize(Path documentPath) {
      return documentPath == null ? null : documentPath.normalize();
   }

   public static DocumentMetadataBuilder reconstituteBuilder() {
      return new DocumentMetadataBuilder().reconstitute();
   }

   public static class DocumentMetadataBuilder extends DomainBuilder<DocumentMetadata> {
      private Path documentPath;
      private MimeType contentType;
      private Charset contentEncoding;
      private Long contentSize;
      private Instant creationDate;
      private Instant lastUpdateDate;
      private Map<String, String> attributes = map();
      private MimeTypeRegistry mimeTypeRegistry;

      public static DocumentMetadataBuilder from(DocumentMetadata metadata) {
         notNull(metadata, "metadata");
         return new DocumentMetadataBuilder()
               .<DocumentMetadataBuilder>reconstitute()
               .documentPath(metadata.documentPath)
               .contentType(metadata.contentType)
               .contentSize(metadata.contentSize)
               .attributes(metadata.attributes)
               .creationDate(metadata.creationDate)
               .lastUpdateDate(metadata.lastUpdateDate);
      }

      public DocumentMetadataBuilder mimeTypeRegistry(MimeTypeRegistry mimeTypeRegistry) {
         this.mimeTypeRegistry = mimeTypeRegistry;
         return this;
      }

      @Setter
      public DocumentMetadataBuilder documentPath(Path documentPath) {
         this.documentPath = documentPath;
         return this;
      }

      public DocumentMetadataBuilder documentPath(String documentPath, String... moreDocumentPath) {
         return documentPath(Path.of(notNull(documentPath, "documentPath"), moreDocumentPath));
      }

      @Setter
      public DocumentMetadataBuilder contentType(MimeType contentType) {
         this.contentType = contentType;
         return this;
      }

      public DocumentMetadataBuilder contentType(MimeType contentType, Charset contentEncoding) {
         this.contentType = contentType;
         this.contentEncoding = contentEncoding;
         return this;
      }

      public DocumentMetadataBuilder contentType(Charset contentEncoding) {
         this.contentEncoding = contentEncoding;
         return this;
      }

      @Setter
      public DocumentMetadataBuilder contentSize(Long contentSize) {
         this.contentSize = contentSize;
         return this;
      }

      @Setter
      public DocumentMetadataBuilder creationDate(Instant creationDate) {
         ensureReconstitute();
         this.creationDate = creationDate;
         return this;
      }

      @Setter
      public DocumentMetadataBuilder lastUpdateDate(Instant lastUpdateDate) {
         ensureReconstitute();
         this.lastUpdateDate = lastUpdateDate;
         return this;
      }

      @Setter
      public DocumentMetadataBuilder attributes(Map<String, String> metadata) {
         this.attributes = map(metadata);
         return this;
      }

      public DocumentMetadataBuilder addAttribute(String key, String value) {
         this.attributes.put(key, value);
         return this;
      }

      public DocumentMetadataBuilder addAttributes(Map<String, String> metadata) {
         this.attributes.putAll(map(metadata));
         return this;
      }

      @Override
      public DocumentMetadata buildDomainObject() {
         return new DocumentMetadata(this);
      }
   }

}

