/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.uri;

import static com.tinubu.commons.lang.util.CheckedPredicate.alwaysFalse;
import static com.tinubu.commons.lang.util.CheckedPredicate.alwaysTrue;
import static com.tinubu.commons.lang.util.CollectionUtils.immutable;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.instanceOf;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.noNullElements;
import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URI;
import java.time.Duration;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.uri.ComponentUri.Query;
import com.tinubu.commons.ddd2.uri.parts.KeyValueQuery;
import com.tinubu.commons.lang.convert.ConversionFailedException;
import com.tinubu.commons.lang.convert.ConversionService;
import com.tinubu.commons.lang.convert.ConverterNotFoundException;
import com.tinubu.commons.lang.convert.services.StandardConversionService;
import com.tinubu.commons.lang.util.CollectionUtils;
import com.tinubu.commons.lang.util.Pair;
import com.tinubu.commons.lang.util.types.Base64String;

// FIXME 2 "registered" concepts in a single class : registeres parameters and Parameter::isRegistered
public class ParameterizedQuery extends KeyValueQuery {

   private final Map<String, ValueParameter> parameters;
   private final ConversionService conversionService;

   /**
    * For internal use. You must ensure validation of parameters prior to calling this constructor.
    * You must ensure parameters consistency between all parameters.
    */
   private ParameterizedQuery(String query,
                              String encodedQuery,
                              boolean noQueryIfEmpty,
                              Map<String, List<String>> queryMap,
                              Map<String, List<String>> encodedQueryMap,
                              ConversionService conversionService) {
      super(query, encodedQuery, noQueryIfEmpty, queryMap, encodedQueryMap);
      this.parameters = new LinkedHashMap<>();
      resetParameters(this);
      this.conversionService = nullable(conversionService, new StandardConversionService());
   }

   protected ParameterizedQuery(String query,
                                String encodedQuery,
                                boolean noQueryIfEmpty,
                                ConversionService conversionService) {
      super(query, encodedQuery, noQueryIfEmpty);
      this.parameters = new LinkedHashMap<>();
      resetParameters(this);
      this.conversionService = nullable(conversionService, new StandardConversionService());
   }

   protected ParameterizedQuery(Map<String, List<String>> query,
                                Map<String, List<String>> encodedQuery,
                                boolean noQueryIfEmpty,
                                ConversionService conversionService) {
      super(query, encodedQuery, noQueryIfEmpty);
      this.parameters = new LinkedHashMap<>();
      resetParameters(this);
      this.conversionService = nullable(conversionService, new StandardConversionService());
   }

   /**
    * Creates {@link ParameterizedQuery} from un-encoded query string.
    * Each key and value are encoded following encoding rules for query.
    * <p>
    * If specified query string is empty, an empty query will be generated ({@code ?} alone).
    *
    * @param query un-encoded query string
    *
    * @return new instance
    *
    * @see #ofEncoded(String)
    */
   public static ParameterizedQuery of(String query) {
      Check.notNull(query, "query");

      return new ParameterizedQuery(query, encodeQuery(query, false), DEFAULT_NO_QUERY_IF_EMPTY, null);
   }

   /**
    * Creates {@link ParameterizedQuery} from structured map of un-encoded parameters.
    * Specified map associates parameter name to one or more values. If values list is {@code null} or
    * empty, parameter is ignored. Each parameter value in list can be empty (empty parameter value) or
    * {@code null} (unset parameter value).
    * <p>
    * Each key and value are encoded following encoding rules for query. Moreover, {@code &} are
    * also encoded specifically when using this method.
    * <p>
    * If specified map is empty, an empty query string will be generated.
    *
    * @param query map of query parameters indexed by parameter name
    *
    * @return new instance
    *
    * @see #ofEncoded(Map)
    */
   public static ParameterizedQuery of(Map<String, List<String>> query) {
      Check.notNull(query, "query");

      var encodedQuery = map(LinkedHashMap::new,
                             stream(query.entrySet())
                                   .filter(e -> !(e.getValue() == null || e.getValue().isEmpty()))
                                   .map(e -> Pair.of(encodeQuery(e.getKey(), true),
                                                     list(stream(e.getValue()).map(v -> encodeQuery(v,
                                                                                                    true))))));

      return new ParameterizedQuery(query, encodedQuery, DEFAULT_NO_QUERY_IF_EMPTY, null);
   }

   /**
    * Creates an empty {@link ParameterizedQuery} ({@code ?} alone).
    *
    * @return new instance
    */
   public static ParameterizedQuery ofEmpty() {
      return new ParameterizedQuery("", "", DEFAULT_NO_QUERY_IF_EMPTY, map(), map(), null);
   }

   /**
    * Creates {@link ParameterizedQuery} from encoded query string.
    * <p>
    * If specified query string is empty, an empty query will be generated ({@code ?} alone).
    *
    * @param encodedQuery encoded query string
    *
    * @return new instance
    *
    * @see #of(String)
    */
   public static ParameterizedQuery ofEncoded(String encodedQuery) {
      Check.notNull(encodedQuery, "encodedQuery");

      return new ParameterizedQuery(decodeQuery(encodedQuery), encodedQuery, DEFAULT_NO_QUERY_IF_EMPTY, null);
   }

   /**
    * Creates {@link ParameterizedQuery} from structured map of encoded parameters.
    * Specified map associates parameter name to one or more values. If values list is {@code null} or
    * empty, parameter is ignored. Each parameter value in list can be empty (empty parameter value) or
    * {@code null} (unset parameter value).
    * <p>
    * If specified map is empty, an empty query string will be generated.
    *
    * @param encodedQuery map of encoded query parameters indexed by parameter name
    *
    * @return new instance, or {@link Optional#empty()} if map is empty and {@code noQueryIfEmpty} is set
    *
    * @see #of(Map)
    */
   public static ParameterizedQuery ofEncoded(Map<String, List<String>> encodedQuery) {
      Check.notNull(encodedQuery, "encodedQuery");

      var query = map(LinkedHashMap::new,
                      stream(encodedQuery.entrySet())
                            .filter(e -> !(e.getValue() == null || e.getValue().isEmpty()))
                            .map(e -> Pair.of(decodeQuery(e.getKey()),
                                              list(stream(e.getValue()).map(KeyValueQuery::decodeQuery)))));
      return new ParameterizedQuery(query, encodedQuery, DEFAULT_NO_QUERY_IF_EMPTY, null);
   }

   public static Optional<? extends ParameterizedQuery> of(URI uri) {
      Check.notNull(uri, "uri");

      return nullable(uri.getQuery()).map(__ -> new ParameterizedQuery(uri.getQuery(),
                                                                       uri.getRawQuery(),
                                                                       DEFAULT_NO_QUERY_IF_EMPTY,
                                                                       null));
   }

   /**
    * Creates {@link ParameterizedQuery} from existing {@link Query}.
    *
    * @param query existing query
    *
    * @return new instance
    */
   public static ParameterizedQuery of(Query query) {
      Check.notNull(query, "query");

      if (query instanceof ParameterizedQuery) {
         var kvq = (ParameterizedQuery) query;
         return new ParameterizedQuery(kvq.value(),
                                       kvq.encodedValue(),
                                       kvq.noQueryIfEmpty,
                                       kvq.query,
                                       kvq.encodedQuery,
                                       kvq.conversionService);
      } else {
         return new ParameterizedQuery(query.value(), query.encodedValue(), query.noQueryIfEmpty(), null);
      }
   }

   @Override
   public Optional<? extends ParameterizedQuery> recreate(URI uri) {
      Check.notNull(uri, "uri");

      return super.recreate(uri).map(ParameterizedQuery.class::cast);
   }

   @Override
   protected ParameterizedQuery recreate(String query, String encodedQuery, boolean noQueryIfEmpty) {
      return new ParameterizedQuery(query,
                                    encodedQuery,
                                    noQueryIfEmpty,
                                    conversionService).registerParameters(registeredParameters());
   }

   @Override
   protected ParameterizedQuery recreate(String query,
                                         String encodedQuery,
                                         boolean noQueryIfEmpty,
                                         Map<String, List<String>> queryMap,
                                         Map<String, List<String>> encodedQueryMap) {
      return new ParameterizedQuery(query,
                                    encodedQuery,
                                    noQueryIfEmpty,
                                    queryMap,
                                    encodedQueryMap,
                                    conversionService).registerParameters(registeredParameters());
   }

   @Override
   public ParameterizedQuery noQueryIfEmpty(boolean noQueryIfEmpty) {
      return (ParameterizedQuery) super.noQueryIfEmpty(noQueryIfEmpty);
   }

   @Override
   public ParameterizedQuery setParameter(boolean encoded, String key, List<String> values) {
      return ((ParameterizedQuery) super.setParameter(encoded, key, values)).refreshParameter(key);
   }

   @Override
   public ParameterizedQuery setParameters(boolean encoded, Map<String, List<String>> query) {
      return ((ParameterizedQuery) super.setParameters(encoded, query)).refreshParameters(query.keySet());
   }

   @Override
   public ParameterizedQuery addParameter(boolean encoded, String key, List<String> values) {
      return ((ParameterizedQuery) super.addParameter(encoded, key, values)).refreshParameter(key);
   }

   @Override
   public ParameterizedQuery addParameters(boolean encoded, Map<String, List<String>> query) {
      return ((ParameterizedQuery) super.addParameters(encoded, query)).refreshParameters(query.keySet());
   }

   public ParameterizedQuery refreshParameter(String key) {
      Check.notNull(key, "key");

      return refreshParameters(list(key));
   }

   @SuppressWarnings("unchecked")
   public ParameterizedQuery refreshParameters(Collection<String> keys) {
      Check.notNull(keys, "keys");

      keys.forEach(key -> {
         var currentParameter = this.parameters.get(key);
         if (currentParameter != null) {
            this.parameters.put(key,
                                parseQueryParameter(key,
                                                    query(key),
                                                    currentParameter.registered,
                                                    currentParameter.sensitive));
            registerParameter(Parameter.of(currentParameter));
         }
      });

      return this;
   }

   public ParameterizedQuery resetParameters(KeyValueQuery query) {
      this.parameters.clear();
      parseQueryParameters(query).forEach(value -> this.parameters.put(value.key(), value));
      return this;
   }

   public <T> ParameterizedQuery mapParameters(Function<? super ValueParameter, ? extends ValueParameter> mapper) {
      var mappedParameters = list(this.parameters
                                        .entrySet()
                                        .stream()
                                        .map(e -> Pair.of(e.getKey(), mapper.apply(e.getValue()))));

      this.parameters.clear();
      mappedParameters.forEach(e -> this.parameters.put(e.getKey(), e.getValue()));

      return this;
   }

   public <T> ParameterizedQuery markAllParametersAsRegistered() {
      return mapParameters(p -> ValueParameter.ofValues(new Parameter(p.key(),
                                                                      true,
                                                                      p.valueType(),
                                                                      p.sensitive(),
                                                                      p.defaultValue), p.values()));
   }

   /**
    * Registers the specified parameters. If a parameter is a {@link ValueParameter}, value is directly
    * replaced, otherwise parameter values are retrieved from URI.
    *
    * @param parameters parameters
    *
    * @return this instance
    */
   public ParameterizedQuery registerParameters(List<? extends Parameter> parameters) {
      noNullElements(parameters, "parameters");

      parameters.forEach(parameter -> {
         if (parameter instanceof ValueParameter) {
            this.parameters.put(parameter.key(), (ValueParameter) parameter);
         } else {
            this.parameters.put(parameter.key(),
                                ValueParameter.ofValues(parameter,
                                                        parameters(parameter.key(),
                                                                   parameter.valueType(),
                                                                   alwaysFalse())));

         }
      });
      return this;
   }

   @SafeVarargs
   public final <T> ParameterizedQuery registerParameters(Parameter<T>... parameters) {
      noNullElements(parameters, "parameters");

      return registerParameters(list(parameters));
   }

   public final <T> ParameterizedQuery registerParameter(Parameter<T> parameter) {
      notNull(parameter, "parameter");

      return registerParameters(list(parameter));
   }

   public List<Parameter> registeredParameters() {
      return list(parameters.values());
   }

   public Parameter registeredParameter(String name) {
      notBlank(name, "name");

      return parameters.get(name);
   }

   public ConversionService conversionService() {
      return conversionService;
   }

   public static class ExportUriFilter {

      public static BiPredicate<ParameterizedQuery, Parameter> isRegisteredParameter(Predicate<Parameter> registeredParameterFilter) {
         notNull(registeredParameterFilter, "registeredParameterFilter");

         return (parameters, parameter) -> nullable(parameters.parameters.get(parameter.key()))
               .filter(Parameter::registered)
               .filter(registeredParameterFilter)
               .isPresent();
      }

      public static BiPredicate<ParameterizedQuery, Parameter> isRegisteredParameter() {
         return isRegisteredParameter(__ -> true);
      }

      public static BiPredicate<ParameterizedQuery, Parameter> includeParametersByName(List<String> parameters) {
         notNull(parameters, "parameters");

         return (__, parameter) -> parameters.contains(parameter.key());
      }

      public static BiPredicate<ParameterizedQuery, Parameter> includeParametersByName(String... parameters) {
         return includeParametersByName(list(parameters));
      }

      public static BiPredicate<ParameterizedQuery, Parameter> includeParameters(List<? extends Parameter> parameters) {
         notNull(parameters, "parameters");

         return includeParametersByName(list(parameters.stream().map(Parameter::key)));
      }

      public static BiPredicate<ParameterizedQuery, Parameter> includeParameters(Parameter... parameters) {
         return includeParameters(list(parameters));
      }

      public static BiPredicate<ParameterizedQuery, Parameter> excludeParametersByName(List<String> parameters) {
         notNull(parameters, "parameters");

         return (__, parameter) -> !parameters.contains(parameter.key());
      }

      public static BiPredicate<ParameterizedQuery, Parameter> excludeParametersByName(String... parameters) {
         return excludeParametersByName(list(parameters));
      }

      public static BiPredicate<ParameterizedQuery, Parameter> excludeParameters(List<? extends Parameter> parameters) {
         notNull(parameters, "parameters");

         return excludeParametersByName(list(parameters.stream().map(Parameter::key)));
      }

      public static BiPredicate<ParameterizedQuery, Parameter> excludeParameters(Parameter... parameters) {
         return excludeParameters(list(parameters));
      }

      public static BiPredicate<ParameterizedQuery, Parameter> isSensitiveParameter() {
         return (__, parameter) -> parameter.sensitive();
      }

      public static BiPredicate<ParameterizedQuery, Parameter> isAlwaysTrue() {
         return (__1, __2) -> true;
      }

      public static BiPredicate<ParameterizedQuery, Parameter> isAlwaysFalse() {
         return (__1, __2) -> false;
      }

      public static <T> BiPredicate<ParameterizedQuery, Parameter> isParameterType(Class<T> parameterType) {
         notNull(parameterType, "parameterType");

         return (__, parameter) -> parameterType.isAssignableFrom(parameter.valueType());
      }

      @SuppressWarnings("unchecked")
      public static <T> BiPredicate<ParameterizedQuery, Parameter> isParameterDefaultValue() {
         return (__, parameter) -> instanceOf(parameter, ValueParameter.class)
               .map(vp -> ((Optional<List<T>>) vp.defaultValues())
                     .map(vpd -> vp.valuesOrDefault().equals(vpd))
                     .orElse(false))
               .orElse(false);
      }
   }

   /**
    * Exports URI with all parameters and user infos.
    * <p>
    * URI query parameters are individually and systematically URL-encoded to support URI/URLs in query
    * string, or other advanced use cases.
    *
    * @return exported URI
    */
   public ParameterizedQuery exportQuery() {
      return exportQuery(ExportUriFilter.isAlwaysTrue());
   }

   /**
    * Exports parameterized query.
    *
    * @param queryFilter query parameters filter
    *
    * @return exported query
    */
   public ParameterizedQuery exportQuery(BiPredicate<ParameterizedQuery, ? super Parameter> queryFilter) {
      notNull(queryFilter, "queryFilter");

      Map<String, Parameter> filteredParameters = map(LinkedHashMap::new,
                                                      parameters
                                                            .entrySet()
                                                            .stream()
                                                            .filter(q -> queryFilter.test(this,
                                                                                          q.getValue())));

      return ParameterizedQuery
            .of(query(list(filteredParameters.values()), alwaysTrue(), conversionService))
            .noQueryIfEmpty(noQueryIfEmpty);
   }

   public ParameterizedQuery exportQuery(Predicate<? super Parameter> filter) {
      notNull(filter, "filter");

      return exportQuery((__, parameter) -> filter.test(parameter));
   }

   /**
    * Returns {@code true} if actual parameterized URI contains specified URI parameters matching filter.
    * Specified URI parameters can be a subgroup of actual URI parameters, this operation only checks
    * included
    * URI parameters that exists.
    * A parameter is included in actual URI if its value equals actual URI parameter value (or its default
    * value).
    *
    * @param query query to check
    * @param filter filter specified URI parameters to check for inclusion
    *
    * @return {@code true} if specified URI is contained in actual parameterized URI
    */
   public boolean includeParameters(ParameterizedQuery query,
                                    BiPredicate<ParameterizedQuery, ? super Parameter> filter) {
      notNull(query, "uri");
      notNull(filter, "filter");

      return query.parameters.values().stream().filter(p -> filter.test(query, p)).allMatch(parameter -> {
         List values = parameter.values();

         if (values.isEmpty()) {
            return true;
         }

         ValueParameter valueParameter = parameters.get(parameter.key());

         if (valueParameter == null) {
            return false;
         }

         return values.equals(valueParameter.valuesOrDefault());
      });
   }

   public Optional<String> stringParameter(String key, Predicate<Parameter> useDefault) {
      return parameter(key, String.class, useDefault);
   }

   public List<String> stringParameters(String key, Predicate<Parameter> useDefault) {
      return parameters(key, String.class, useDefault);
   }

   public Optional<Long> longParameter(String key, Predicate<Parameter> useDefault) {
      return parameter(key, Long.class, useDefault);
   }

   public List<Long> longParameters(String key, Predicate<Parameter> useDefault) {
      return parameters(key, Long.class, useDefault);
   }

   public Optional<Integer> integerParameter(String key, Predicate<Parameter> useDefault) {
      return parameter(key, Integer.class, useDefault);
   }

   public List<Integer> integerParameters(String key, Predicate<Parameter> useDefault) {
      return parameters(key, Integer.class, useDefault);
   }

   public Optional<Byte> byteParameter(String key, Predicate<Parameter> useDefault) {
      return parameter(key, Byte.class, useDefault);
   }

   public List<Byte> byteParameters(String key, Predicate<Parameter> useDefault) {
      return parameters(key, Byte.class, useDefault);
   }

   public Optional<Short> shortParameter(String key, Predicate<Parameter> useDefault) {
      return parameter(key, Short.class, useDefault);
   }

   public List<Short> shortParameters(String key, Predicate<Parameter> useDefault) {
      return parameters(key, Short.class, useDefault);
   }

   public Optional<Float> floatParameter(String key, Predicate<Parameter> useDefault) {
      return parameter(key, Float.class, useDefault);
   }

   public List<Float> floatParameters(String key, Predicate<Parameter> useDefault) {
      return parameters(key, Float.class, useDefault);
   }

   public Optional<Double> doubleParameter(String key, Predicate<Parameter> useDefault) {
      return parameter(key, Double.class, useDefault);
   }

   public List<Double> doubleParameters(String key, Predicate<Parameter> useDefault) {
      return parameters(key, Double.class, useDefault);
   }

   public Optional<BigDecimal> bigDecimalParameter(String key, Predicate<Parameter> useDefault) {
      return parameter(key, BigDecimal.class, useDefault);
   }

   public List<BigDecimal> bigDecimalParameters(String key, Predicate<Parameter> useDefault) {
      return parameters(key, BigDecimal.class, useDefault);
   }

   public Optional<BigInteger> bigIntegerParameter(String key, Predicate<Parameter> useDefault) {
      return parameter(key, BigInteger.class, useDefault);
   }

   public List<BigInteger> bigIntegerParameters(String key, Predicate<Parameter> useDefault) {
      return parameters(key, BigInteger.class, useDefault);
   }

   public Optional<Boolean> booleanParameter(String key, Predicate<Parameter> useDefault) {
      return parameter(key, Boolean.class, useDefault);
   }

   public List<Boolean> booleanParameters(String key, Predicate<Parameter> useDefault) {
      return parameters(key, Boolean.class, useDefault);
   }

   public Optional<Class> classParameter(String key, Predicate<Parameter> useDefault) {
      return parameter(key, Class.class, useDefault);
   }

   public Optional<Duration> durationParameter(String key, Predicate<Parameter> useDefault) {
      return parameter(key, Duration.class, useDefault);
   }

   public Optional<java.nio.file.Path> pathParameter(String key, Predicate<Parameter> useDefault) {
      return parameter(key, java.nio.file.Path.class, useDefault);
   }

   public List<java.nio.file.Path> pathParameters(String key, Predicate<Parameter> useDefault) {
      return parameters(key, java.nio.file.Path.class, useDefault);
   }

   public Optional<Base64String> base64Parameter(String key, Predicate<Parameter> useDefault) {
      return parameter(key, Base64String.class, useDefault);
   }

   public List<Base64String> base64Parameters(String key, Predicate<Parameter> useDefault) {
      return parameters(key, Base64String.class, useDefault);
   }

   public <T extends Enum<T>> Optional<T> enumParameter(String key,
                                                        Class<T> enumClass,
                                                        Predicate<Parameter> useDefault) {
      return parameter(key, enumClass, useDefault);
   }

   public <T extends Enum<T>> List<T> enumParameters(String key,
                                                     Class<T> enumClass,
                                                     Predicate<Parameter> useDefault) {
      return parameters(key, enumClass, useDefault);
   }

   /**
    * Checks if URI has specified parameter explicitly declared on URI. Having parameter registered is not
    * sufficient.
    *
    * @param key parameter name
    *
    * @return {@code true} if URI has specified parameter explicitly declared on URI
    */
   public boolean hasParameter(String key) {
      return nullable(parameters.get(key)).map(value -> value.value().isPresent()).orElse(false);
   }

   /**
    * Checks if URI has specified parameter explicitly declared on URI. Having parameter registered is not
    * sufficient.
    *
    * @param parameter parameter
    *
    * @return {@code true} if URI has specified parameter explicitly declared on URI
    */
   public <T> boolean hasParameter(Parameter<T> parameter) {
      return nullable(parameters.get(parameter.key)).map(value -> value.value().isPresent()).orElse(false);
   }

   /**
    * Returns specified parameter value from query string if any. If key is specified multiple times, returns
    * only the first occurrence.
    *
    * @param key parameter name
    * @param targetType conversion target type class
    * @param <T> conversion target type
    *
    * @return converted parameter value or {@link Optional#empty()} if parameter is not found
    */
   public <T> Optional<T> parameter(String key, Class<T> targetType, Predicate<Parameter> useDefault) {
      return streamParameters(key, targetType, useDefault).findFirst();
   }

   public <T> Optional<T> parameter(Parameter<T> parameter, Predicate<Parameter> useDefault) {
      return streamParameters(parameter.key(), parameter.valueType(), useDefault).findFirst();
   }

   public <T> Optional<T> parameter(String key, Class<T> targetType, boolean useDefault) {
      return parameter(key, targetType, useDefault ? alwaysTrue() : alwaysFalse());
   }

   public <T> Optional<T> parameter(Parameter<T> parameter, boolean useDefault) {
      return parameter(parameter, useDefault ? alwaysTrue() : alwaysFalse());
   }

   /**
    * Returns specified parameter values from query string. If key is specified multiple times, returns
    * all occurrences.
    *
    * @param key parameter name
    * @param targetType conversion target type class
    * @param <T> conversion target type
    *
    * @return converted parameter values or an empty list if parameter is not found
    */
   public <T> List<T> parameters(String key, Class<T> targetType, Predicate<Parameter> useDefault) {
      return list(streamParameters(key, targetType, useDefault));
   }

   public <T> List<T> parameters(Parameter<T> parameter, Predicate<Parameter> useDefault) {
      return list(streamParameters(parameter.key(), parameter.valueType(), useDefault));
   }

   public <T> List<T> parameters(String key, Class<T> targetType, boolean useDefault) {
      return parameters(key, targetType, useDefault ? alwaysTrue() : alwaysFalse());
   }

   public <T> List<T> parameters(Parameter<T> parameter, boolean useDefault) {
      return parameters(parameter, useDefault ? alwaysTrue() : alwaysFalse());
   }

   /**
    * Returns specified parameter values converted to target type. If key is specified multiple times, returns
    * all occurrences.
    *
    * @param key parameter name
    * @param targetType conversion target type class
    * @param useDefault whether to use parameter default if parameter has no value
    * @param <T> conversion target type
    *
    * @return converted parameter values or an empty list if parameter is not found and does not have default
    *       defined
    *
    * @throws ConverterNotFoundException if no converter from source type to target type is registered
    * @throws ConversionFailedException if conversion failed because of invalid source format
    */
   @SuppressWarnings("unchecked")
   public <T> Stream<T> streamParameters(String key, Class<T> targetType, Predicate<Parameter> useDefault) {
      ValueParameter p = parameters.get(key);

      if (p == null) {
         return stream();
      }

      if (p.valueType.equals(targetType)) {
         return p.values(useDefault).stream();
      } else if (p.valueType.equals(String.class) && targetType.isEnum()) {
         return ((List<String>) p.values(useDefault)).stream().map(v -> enumValue(targetType, v));
      } else {
         return p.values(useDefault).stream().map(v -> conversionService.convert(v, p.valueType, targetType));
      }
   }

   public <T> Stream<T> streamParameters(Parameter<T> parameter, Predicate<Parameter> useDefault) {
      return streamParameters(parameter.key(), parameter.valueType(), useDefault);
   }

   public <T> Stream<T> streamParameters(String key, Class<T> targetType, boolean useDefault) {
      return streamParameters(key, targetType, useDefault ? alwaysTrue() : alwaysFalse());
   }

   public <T> Stream<T> streamParameters(Parameter<T> parameter, boolean useDefault) {
      return streamParameters(parameter, useDefault ? alwaysTrue() : alwaysFalse());
   }

   /**
    * Converts value to target type as part of the parameter conversion feature.
    * Value is always upper-cased before mapping to enum.
    *
    * @param targetType conversion target type class
    * @param value value to convert
    * @param <T> conversion target type
    *
    * @return converted value
    *
    * @throws ConversionFailedException if enum can't be mapped
    */
   @SuppressWarnings("unchecked")
   private static <T> T enumValue(Class<T> targetType, String value) {
      try {
         return (T) Enum.valueOf((Class<? extends Enum>) targetType.asSubclass(Enum.class),
                                 value.toUpperCase());
      } catch (IllegalArgumentException e) {
         throw new ConversionFailedException(String.class, targetType, value, e);
      }
   }

   /**
    * Returns decoded query map from specified parameters.
    *
    * @param queries parameter to set to query
    * @param useDefault whether to use {@link Parameter#defaultValues()} if {@link ValueParameter} has
    *       no value, or is a raw {@link Parameter}
    * @param conversionService conversion service to convert parameters to {@link String}
    *
    * @return query string or {@link Optional#empty} if queries parameter list is empty
    */
   @SuppressWarnings("unchecked")
   private static Map<String, List<String>> query(List<Parameter> queries,
                                                  Predicate<Parameter> useDefault,
                                                  ConversionService conversionService) {
      return map(LinkedHashMap::new,
                 queries
                       .stream()
                       .map(parameter -> Pair.of(parameter.key(),
                                                 instanceOf(parameter, ValueParameter.class)
                                                       .map(vp -> (List<String>) list(vp.stringValues(
                                                             useDefault,
                                                             conversionService)))
                                                       .orElseGet(() -> {
                                                          if (useDefault.test(parameter)) {
                                                             throw new UnsupportedOperationException(); //FIXME Parameter has no conversionService support for default values : see ValueParameter::stringValue(s)"
                                                          } else {
                                                             return CollectionUtils.list();
                                                          }
                                                       }))));
   }

   private static Stream<ValueParameter<String>> parseQueryParameters(KeyValueQuery query) {
      if (query == null) {
         return stream();
      }

      return query
            .query()
            .entrySet()
            .stream()
            .map(e -> parseQueryParameter(e.getKey(), e.getValue(), false, false));
   }

   // FIXME refactor no-arg parameter management (currently, "true" is set for no-arg parameters)
   private static ValueParameter<String> parseQueryParameter(String key,
                                                             List<String> value,
                                                             boolean registered,
                                                             boolean sensitive) {
      return ValueParameter.ofValues(Parameter.of(key, registered, String.class, sensitive),
                                     list(stream(value).map(v -> nullable(v, "true"))));
   }

   /**
    * Parameter definition.
    *
    * @param <T> parameter type
    */
   public static class Parameter<T> {
      protected final String key;
      protected final boolean registered;
      protected final Class<T> valueType;
      protected final boolean sensitive;
      protected final List<T> defaultValue;

      protected Parameter(String key,
                          boolean registered,
                          Class<T> valueType,
                          boolean sensitive,
                          List<T> defaultValue) {
         this.key = notBlank(key, "key");
         this.registered = registered;
         this.valueType = notNull(valueType, "valueType");
         this.sensitive = sensitive;
         this.defaultValue = defaultValue;
      }

      protected Parameter(String key, boolean registered, Class<T> valueType, boolean sensitive) {
         this(key, registered, valueType, sensitive, null);
      }

      public static <T> Parameter<T> of(String key,
                                        boolean registered,
                                        Class<T> valueType,
                                        boolean sensitive) {
         return new Parameter<>(key, registered, valueType, sensitive);
      }

      public static <T> Parameter<T> of(Parameter<T> parameter) {
         return of(parameter.key, parameter.registered, parameter.valueType, parameter.sensitive);
      }

      /**
       * Creates a new registered parameter. A register parameter is a commons-ports related parameter. On the
       * contrary, unregistered parameters are backend related queries.
       *
       * @param key parameter name
       * @param valueType parameter type class
       * @param sensitive whether this parameter is a sensitive one
       * @param <T> parameter type
       *
       * @return new instance
       */
      public static <T> Parameter<T> registered(String key, Class<T> valueType, boolean sensitive) {
         return new Parameter<>(key, true, valueType, sensitive);
      }

      /**
       * Creates a new registered un-sensitive parameter. A register parameter is a commons-ports related
       * parameter. On the
       * contrary, unregistered parameters are backend related queries.
       *
       * @param key parameter name
       * @param valueType parameter type class
       * @param <T> parameter type
       *
       * @return new instance
       */
      public static <T> Parameter<T> registered(String key, Class<T> valueType) {
         return new Parameter<>(key, true, valueType, false);
      }

      /**
       * Creates a new unregistered parameter. Unregistered parameters are backend related. On the
       * contrary, a register parameter is a commons-ports related parameter.
       *
       * @param key parameter name
       * @param valueType parameter type class
       * @param sensitive whether this parameter is a sensitive one
       * @param <T> parameter type
       *
       * @return new instance
       */
      public static <T> Parameter<T> unregistered(String key, Class<T> valueType, boolean sensitive) {
         return new Parameter<>(key, false, valueType, sensitive);
      }

      /**
       * Creates a new unregistered un-sensitive parameter. Unregistered parameters are backend related. On
       * the contrary, a register parameter is a commons-ports related parameter.
       *
       * @param key parameter name
       * @param valueType parameter type class
       * @param <T> parameter type
       *
       * @return new instance
       */
      public static <T> Parameter<T> unregistered(String key, Class<T> valueType) {
         return new Parameter<>(key, false, valueType, false);
      }

      public Parameter<T> sensitive(boolean sensitive) {
         return new Parameter<>(key, registered, valueType, sensitive, defaultValue);
      }

      /**
       * Sets a default value for this parameter.
       *
       * @param defaultValue default value
       *
       * @return this instance
       */
      public Parameter<T> defaultValue(List<T> defaultValue) {
         notNull(defaultValue, "defaultValue");

         return new Parameter<>(key, registered, valueType, sensitive, defaultValue);
      }

      /**
       * Sets a default value for this parameter.
       *
       * @param defaultValue default value
       *
       * @return this instance
       */
      public Parameter<T> defaultValue(T defaultValue) {
         notNull(defaultValue, "defaultValue");

         return defaultValue(list(defaultValue));
      }

      public String key() {
         return key;
      }

      public boolean registered() {
         return registered;
      }

      public Class<T> valueType() {
         return valueType;
      }

      public boolean sensitive() {
         return sensitive;
      }

      public Optional<List<T>> defaultValues() {
         return nullable(defaultValue);
      }

      public Optional<T> defaultValue() {
         if (defaultValue != null && !defaultValue.isEmpty()) {
            return optional(defaultValue.get(0));
         } else {
            return optional();
         }
      }

      @Override
      public boolean equals(Object o) {
         if (this == o) return true;
         if (!(o instanceof Parameter)) return false;
         Parameter<?> parameter = (Parameter<?>) o;
         return Objects.equals(key, parameter.key);
      }

      @Override
      public int hashCode() {
         return Objects.hash(key);
      }

      @Override
      public String toString() {
         return new StringJoiner(", ", Parameter.class.getSimpleName() + "[", "]")
               .add("key='" + key + "'")
               .add("registered=" + registered)
               .add("valueType=" + valueType)
               .add("sensitive=" + sensitive)
               .add("defaultValue=" + defaultValue)
               .toString();
      }

   }

   /**
    * A parameter with an associated multi-value.
    *
    * @param <T> value type
    */
   public static class ValueParameter<T> extends Parameter<T> {
      /**
       * Parameter values.
       */
      private final List<T> values;

      private ValueParameter(Parameter<T> parameter, List<T> values) {
         super(parameter.key,
               parameter.registered,
               parameter.valueType,
               parameter.sensitive,
               parameter.defaultValue);
         this.values = immutable(noNullElements(values, "values"));
      }

      public static <T> ValueParameter<T> ofValues(Parameter<T> parameter, List<T> values) {
         return new ValueParameter<>(parameter, values);
      }

      public static <T> ValueParameter<T> ofValue(Parameter<T> parameter, T value) {
         return new ValueParameter<>(parameter, value == null ? list() : list(value));
      }

      public ValueParameter<T> values(List<T> values) {
         return new ValueParameter<>(new Parameter(key, registered, valueType, sensitive, defaultValue),
                                     values);
      }

      public ValueParameter<T> value(T value) {
         return new ValueParameter<>(new Parameter(key, registered, valueType, sensitive, defaultValue),
                                     value == null ? list() : list(value));
      }

      public ValueParameter<T> addValue(T value) {
         var values = this.values;
         if (value != null) {
            values.add(value);
         }
         return new ValueParameter<>(new Parameter(key, registered, valueType, sensitive, defaultValue),
                                     values);
      }

      public List<T> values() {
         return values;
      }

      public List<T> values(Predicate<Parameter> useDefault) {
         if (useDefault.test(this)) {
            return valuesOrDefault();
         } else {
            return values();
         }
      }

      public List<T> valuesOrDefault() {
         return optional(values)
               .filter(v -> !v.isEmpty())
               .or(this::defaultValues)
               .orElseGet(Collections::emptyList);
      }

      public Optional<T> value() {
         return values.isEmpty() ? optional() : optional(values.get(0));
      }

      public Optional<T> value(Predicate<Parameter> useDefault) {
         if (useDefault.test(this)) {
            return valueOrDefault();
         } else {
            return value();
         }
      }

      public Optional<T> valueOrDefault() {
         return values.isEmpty() ? defaultValue() : optional(values.get(0));
      }

      /**
       * Returns values converted to a list of {@link String}.
       *
       * @param conversionService conversion service to use for conversion
       *
       * @return converted values or an empty stream if no values available
       *
       * @throws ConverterNotFoundException if no converter from source type to string type is
       *       registered
       * @throws ConversionFailedException if conversion failed because of invalid source format
       */
      public Stream<String> stringValues(ConversionService conversionService) {
         return stringValues(values, conversionService);
      }

      /**
       * Returns values, or defaults, converted to a list of {@link String}.
       *
       * @param conversionService conversion service to use for conversion
       *
       * @return converted values or an empty stream if no values and no defaults available
       *
       * @throws ConverterNotFoundException if no converter from source type to string type is
       *       registered
       * @throws ConversionFailedException if conversion failed because of invalid source format
       */
      public Stream<String> stringValuesOrDefault(ConversionService conversionService) {
         return stringValues(valuesOrDefault(), conversionService);
      }

      public Stream<String> stringValues(Predicate<Parameter> useDefault,
                                         ConversionService conversionService) {
         if (useDefault.test(this)) {
            return stringValuesOrDefault(conversionService);
         } else {
            return stringValues(conversionService);
         }
      }

      /**
       * Returns values converted to a list of {@link String}.
       *
       * @param values values to convert
       * @param conversionService conversion service to use for conversion
       *
       * @return converted values
       *
       * @throws ConverterNotFoundException if no converter from source type to string type is
       *       registered
       * @throws ConversionFailedException if conversion failed because of invalid source format
       */
      private Stream<String> stringValues(List<T> values, ConversionService conversionService) {
         if (values == null || values.isEmpty()) {
            return stream();
         } else {
            if (valueType.isEnum()) {
               return values.stream().map(Object::toString);
            } else {
               return values.stream().map(v -> conversionService.convert(v, valueType, String.class));
            }
         }
      }

      @Override
      public boolean equals(Object o) {
         if (this == o) return true;
         if (!(o instanceof ValueParameter)) return false;
         if (!super.equals(o)) return false;
         ValueParameter<?> that = (ValueParameter<?>) o;
         return Objects.equals(values, that.values);
      }

      @Override
      public int hashCode() {
         return Objects.hash(super.hashCode(), values);
      }

      @Override
      public String toString() {
         return new StringJoiner(", ", ValueParameter.class.getSimpleName() + "[", "]")
               .add("key='" + key + "'")
               .add("registered=" + registered)
               .add("valueType=" + valueType)
               .add("sensitive=" + sensitive)
               .add("defaultValue=" + defaultValue)
               .add("values=" + values)
               .toString();
      }

   }

}
