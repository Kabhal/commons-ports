/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import java.io.IOException;
import java.io.UncheckedIOException;

import com.tinubu.commons.ddd2.domain.repository.RepositoryException;

/**
 * Error while accessing document metadata or content.
 */
// FIXME Switch as RepositoryIOException and introduce a new exception for general errors ?
public class DocumentAccessException extends RepositoryException {
   public DocumentAccessException(String message) {
      super(message);
   }

   public DocumentAccessException(String message, Throwable cause) {
      super(message, cause);
   }

   public DocumentAccessException(String message, IOException cause) {
      super(message, cause);
   }

   public DocumentAccessException(String message, UncheckedIOException cause) {
      super(message, cause);
   }

   public DocumentAccessException(IOException cause) {
      super(cause.getMessage(), cause);
   }

   public DocumentAccessException(UncheckedIOException cause) {
      super(cause.getMessage(), cause);
   }
}
