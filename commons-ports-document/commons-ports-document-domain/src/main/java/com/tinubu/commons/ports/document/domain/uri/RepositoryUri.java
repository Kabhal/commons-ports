/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.uri;

import java.net.URI;

import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.uri.Uri;

/**
 * URI identifying a repository.
 * <p>
 * See {@link DocumentUri} to identify a specific document in a repository.
 *
 * @see DocumentUri
 */
public interface RepositoryUri extends Uri {

   /**
    * Creates a repository URI from {@link Uri}.
    *
    * @param uri repository URI
    *
    * @return new instance
    */
   static DefaultRepositoryUri ofRepositoryUri(Uri uri) {
      return DefaultRepositoryUri.ofRepositoryUri(uri);
   }

   /**
    * Creates a repository URI from {@link URI}.
    *
    * @param uri repository URI
    *
    * @return new instance
    */
   static DefaultRepositoryUri ofRepositoryUri(URI uri) {
      return DefaultRepositoryUri.ofRepositoryUri(uri);
   }

   /**
    * Creates a repository URI from URI string.
    * URI syntax is checked as part of this operation.
    *
    * @param uri repository URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI has invalid syntax
    */
   static DefaultRepositoryUri ofRepositoryUri(String uri) {
      return DefaultRepositoryUri.ofRepositoryUri(uri);
   }

   /**
    * Returns {@code true} this URI parameters supports specified URI.
    * Specified URI can contain query parameter, in this case all registered parameters of the specified URI
    * must have the same value that current parameterized URI (explicitly defined or default parameter value).
    *
    * @param uri URI to support
    *
    * @return {@code true} if specified URI is supported by this URI
    */
   boolean supportsUri(RepositoryUri uri);

   /**
    * Exports current URI respecting specified options.
    *
    * @param options export options
    *
    * @return exported URI
    */
   URI exportURI(ExportUriOptions options);

   /**
    * Exports current {@link RepositoryUri} respecting specified options.
    *
    * @param options export options
    *
    * @return exported {@link RepositoryUri}
    */
   RepositoryUri exportUri(ExportUriOptions options);
}

