/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.rules;

import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObject;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.satisfiesValue;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.as;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.property;
import static com.tinubu.commons.ddd2.invariant.rules.OptionalRules.optionalValue;
import static com.tinubu.commons.ddd2.invariant.rules.OptionalRules.requiresValue;

import java.nio.charset.Charset;
import java.nio.file.Path;
import java.util.Map;

import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.MessageValue;
import com.tinubu.commons.ddd2.invariant.formatter.DefaultMessageFormat;
import com.tinubu.commons.ddd2.invariant.formatter.FastStringFormat;
import com.tinubu.commons.ddd2.invariant.formatter.MessageFormatter;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentMetadata;
import com.tinubu.commons.ports.document.domain.DocumentPath;

/**
 * Invariant rules for {@link Document}.
 */
public final class DocumentRules {

   private DocumentRules() { }

   public static class DocumentMainRules {

      public static <T extends Document> InvariantRule<T> documentId(final InvariantRule<? super DocumentPath> rule) {
         return isNotNull().andValue(property(Document::documentId, "documentId", rule));
      }

      public static <T extends Document> InvariantRule<T> metadata(final InvariantRule<? super DocumentMetadata> rule) {
         return isNotNull().andValue(as(Document::metadata, rule));
      }

   }

   public static class DocumentPathRules {

      public static <T extends DocumentPath> InvariantRule<T> path(final InvariantRule<? super Path> rule) {
         return isNotNull().andValue(property(DocumentPath::value, "path", rule));
      }

      public static <T extends DocumentPath> InvariantRule<T> name(final InvariantRule<? super String> rule) {
         return isNotNull().andValue(property(DocumentPath::name, "name", rule));
      }

      public static <T extends DocumentPath> InvariantRule<T> extension(final InvariantRule<? super String> rule) {
         return isNotNull().andValue(property(DocumentPath::extension, "extension", rule));
      }

   }

   public static class DocumentEntryRules {

      public static <T extends DocumentEntry> InvariantRule<T> documentId(final InvariantRule<? super DocumentPath> rule) {
         return isNotNull().andValue(property(DocumentEntry::documentId, "documentId", rule));
      }

      public static <T extends DocumentEntry> InvariantRule<T> metadata(final InvariantRule<? super DocumentMetadata> rule) {
         return isNotNull().andValue(as(DocumentEntry::metadata, rule));
      }

   }

   public static class DocumentMetadataRules {

      public static <T extends DocumentMetadata> InvariantRule<T> documentName(final InvariantRule<? super String> rule) {
         return isNotNull().andValue(property(DocumentMetadata::documentName, "documentName", rule));
      }

      public static <T extends DocumentMetadata> InvariantRule<T> documentPath(final InvariantRule<? super Path> rule) {
         return isNotNull().andValue(property(DocumentMetadata::documentPath, "documentPath", rule));
      }

      public static <T extends DocumentMetadata> InvariantRule<T> optionalContentType(final InvariantRule<? super MimeType> rule) {
         return isNotNull().andValue(property(DocumentMetadata::contentType,
                                              "contentType",
                                              optionalValue(rule)));
      }

      public static <T extends DocumentMetadata> InvariantRule<T> contentType(final InvariantRule<? super MimeType> rule) {
         return isNotNull().andValue(property(DocumentMetadata::contentType,
                                              "contentType",
                                              requiresValue(rule)));
      }

      public static <T extends DocumentMetadata> InvariantRule<T> hasContentType(MessageFormatter<T> messageFormatter) {
         return isNotNull().andValue(satisfiesValue(v -> v.contentType().isPresent(),
                                                    messageFormatter).ruleContext(
               "DocumentMetadataRules.hasContentType"));
      }

      public static <T extends DocumentMetadata> InvariantRule<T> hasContentType(final String message,
                                                                                 final MessageValue<T>... values) {
         return hasContentType(DefaultMessageFormat.of(message, values));
      }

      public static <T extends DocumentMetadata> InvariantRule<T> hasContentType() {
         return hasContentType(FastStringFormat.of("'", validatingObject(), "' must have content-type"));
      }

      public static <T extends DocumentMetadata> InvariantRule<T> optionalContentEncoding(final InvariantRule<Charset> rule) {
         return isNotNull().andValue(property(DocumentMetadata::contentEncoding,
                                              "contentEncoding",
                                              optionalValue(rule)));
      }

      public static <T extends DocumentMetadata> InvariantRule<T> contentEncoding(final InvariantRule<? super Charset> rule) {
         return isNotNull().andValue(property(DocumentMetadata::contentEncoding,
                                              "contentEncoding",
                                              requiresValue(rule)));
      }

      public static <T extends DocumentMetadata> InvariantRule<T> optionalContentSize(final InvariantRule<Long> rule) {
         return isNotNull().andValue(property(DocumentMetadata::contentSize,
                                              "contentSize",
                                              optionalValue(rule)));
      }

      public static <T extends DocumentMetadata> InvariantRule<T> contentSize(final InvariantRule<? super Long> rule) {
         return isNotNull().andValue(property(DocumentMetadata::contentSize,
                                              "contentSize",
                                              requiresValue(rule)));
      }

      public static <T extends DocumentMetadata> InvariantRule<T> attributes(final InvariantRule<Map<String, String>> rule) {
         return isNotNull().andValue(property(DocumentMetadata::attributes, "attributes", rule));
      }

   }
}
