/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd2.invariant.rules.NumberRules.isPositive;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.XCheckedSupplier.checkedSupplier;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.ReaderInputStream;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.valueformatter.HiddenValueFormatter;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.beans.Setter;
import com.tinubu.commons.lang.io.encoding.AutoDetectEncodingInputStream;
import com.tinubu.commons.lang.io.encoding.EncodingDetector;
import com.tinubu.commons.lang.io.encoding.EncodingDetector.DetectedEncoding;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent.InputStreamDocumentContentBuilder.AutoDetectEncoding;

/**
 * Document content stream implementation for read access.
 * This content is represented with an {@link InputStream}.
 */
public class InputStreamDocumentContent extends AbstractValue implements DocumentContent {

   /** Document content as {@link InputStream}. Content can be read only one time. */
   protected final AutoDetectEncodingInputStream content;
   /** Optional document content encoding. */
   protected final Charset contentEncoding;
   /** Optional content size indication when known. */
   protected final Long contentSize;

   protected InputStreamDocumentContent(InputStreamDocumentContentBuilder builder) {
      var autoDetectEncoding = builder.autoDetectEncoding;
      this.content = nullable(builder.content)
            .map(content -> new AutoFinalizingInputStream(content, nullable(builder.finalize, __ -> { })))
            .map(content -> {
               return autoDetectEncoding
                     .detector()
                     .map(detector -> new AutoDetectEncodingInputStream(content,
                                                                        detector,
                                                                        autoDetectEncoding.detectionLength()))
                     .orElseGet(() -> new AutoDetectEncodingInputStream(content,
                                                                        autoDetectEncoding.detectionLength()));
            })
            .orElse(null);
      this.contentEncoding = nullable(builder.contentEncoding).orElseGet(() -> autoDetectEncoding(content,
                                                                                                  autoDetectEncoding));
      this.contentSize = builder.contentSize;
   }

   @Override
   @SuppressWarnings("unchecked")
   public Fields<? extends InputStreamDocumentContent> defineDomainFields() {
      return Fields
            .<InputStreamDocumentContent>builder()
            .superFields((Fields<InputStreamDocumentContent>) super.defineDomainFields())
            .field("content", v -> v.content, new HiddenValueFormatter(), isNotNull())
            .field("contentEncoding", v -> v.contentEncoding)
            .field("contentSize", v -> v.contentSize, isNull().orValue(isPositive()))
            .build();
   }

   @Override
   public Optional<DetectedEncoding> hasEncoding(List<Charset> encodings) {
      try {
         return content.hasEncoding(encodings);
      } catch (IOException e) {
         throw new DocumentAccessException(e);
      }
   }

   /**
    * {@inheritDoc}
    *
    * @implSpec content input stream is closed when calling this operation, even in case of
    *       exception.
    */
   @Override
   public String stringContent(Charset defaultContentEncoding) {
      Check.notNull(defaultContentEncoding, "defaultContentEncoding");

      try (InputStream cis = this.content) {
         return IOUtils.toString(cis, contentEncoding().orElse(defaultContentEncoding));
      } catch (IOException e) {
         throw new DocumentAccessException(e);
      }
   }

   /**
    * {@inheritDoc}
    *
    * @implSpec content input stream is closed when calling this operation, even in case of
    *       exception.
    */
   @Override
   public String stringContent() {
      return stringContent(contentEncoding().orElseThrow(() -> new IllegalStateException(
            "No content encoding set, you should specify one to encode this content")));
   }

   @Override
   public InputStream inputStreamContent() {
      return content;
   }

   @Override
   public OutputStream outputStreamContent() {
      throw new IllegalStateException("Unsupported operation for this content implementation");
   }

   @Override
   public InputStreamReader readerContent(Charset defaultContentEncoding) {
      Check.notNull(defaultContentEncoding, "defaultContentEncoding");

      return new InputStreamReader(content, contentEncoding().orElse(defaultContentEncoding));
   }

   @Override
   public Writer writerContent(Charset defaultContentEncoding) {
      throw new IllegalStateException("Unsupported operation for this content implementation");
   }

   @Override
   public Writer writerContent() {
      throw new IllegalStateException("Unsupported operation for this content implementation");
   }

   @Override
   public InputStreamReader readerContent() {
      return readerContent(contentEncoding().orElseThrow(() -> new IllegalStateException(
            "No content encoding set, you should specify one to encode this content")));
   }

   @Override
   public Optional<Long> contentSize() {
      return nullable(contentSize);
   }

   @Override
   @Getter
   public byte[] content() {
      try (InputStream cis = this.content) {
         return IOUtils.toByteArray(cis);
      } catch (IOException e) {
         throw new DocumentAccessException(e);
      }
   }

   @Override
   @Getter
   public Optional<Charset> contentEncoding() {
      return nullable(contentEncoding);
   }

   @Override
   public void close() {
      try {
         this.content.close();
      } catch (IOException e) {
         throw new DocumentAccessException(e);
      }
   }

   @SuppressWarnings("unchecked")
   private static Charset autoDetectEncoding(InputStream content, AutoDetectEncoding autoDetectEncoding) {
      if (content == null
          || !autoDetectEncoding.enabled()
          || !(content instanceof AutoDetectEncodingInputStream)) {
         return null;
      }

      try {
         return checkedSupplier(() -> {
            return ((AutoDetectEncodingInputStream) content)
                  .hasEncoding(autoDetectEncoding.encodings(), autoDetectEncoding.removeBom())
                  .filter(detectedEncoding -> detectedEncoding.confidence()
                                              >= autoDetectEncoding.confidenceThreshold())
                  .map(DetectedEncoding::encoding)
                  .orElse(null);
         }).tryCatch(content::close).getChecked();
      } catch (IOException e) {
         throw new DocumentAccessException(e);
      }
   }

   public static InputStreamDocumentContentBuilder reconstituteBuilder() {
      return new InputStreamDocumentContentBuilder().reconstitute();
   }

   public static class InputStreamDocumentContentBuilder extends DomainBuilder<InputStreamDocumentContent> {
      private InputStream content;
      private Charset contentEncoding;
      private Long contentSize;
      private AutoDetectEncoding autoDetectEncoding = AutoDetectEncoding.noDetection();
      private Consumer<InputStream> finalize;

      public static InputStreamDocumentContentBuilder from(DocumentContent documentContent) {
         return new InputStreamDocumentContentBuilder()
               .<InputStreamDocumentContentBuilder>reconstitute()
               .content(documentContent.inputStreamContent())
               .contentEncoding(documentContent.contentEncoding().orElse(null))
               .contentSize(documentContent.contentSize().orElse(null));
      }

      public InputStreamDocumentContentBuilder content(InputStream content,
                                                       Charset contentEncoding,
                                                       Long contentSize) {
         this.content = content;
         this.contentEncoding = contentEncoding;
         this.contentSize = contentSize;
         return this;
      }

      public InputStreamDocumentContentBuilder content(InputStream content, Charset contentEncoding) {
         return content(content, contentEncoding, null);
      }

      public InputStreamDocumentContentBuilder content(InputStream content, Long contentSize) {
         return content(content, null, contentSize);
      }

      public InputStreamDocumentContentBuilder content(byte[] content, Charset contentEncoding) {
         return content(new ByteArrayInputStream(Check.notNull(content, "content")),
                        contentEncoding,
                        (long) content.length);
      }

      public InputStreamDocumentContentBuilder content(byte[] content) {
         return content(content, null);
      }

      public InputStreamDocumentContentBuilder content(String content, Charset contentEncoding) {
         return content(Check
                              .notNull(content, "content")
                              .getBytes(Check.notNull(contentEncoding, "contentEncoding")), contentEncoding);
      }

      public InputStreamDocumentContentBuilder content(Reader content, Charset contentEncoding) {
         return content(new ReaderInputStream(Check.notNull(content, "content"),
                                              Check.notNull(contentEncoding, "contentEncoding")),
                        contentEncoding);
      }

      @Setter
      public InputStreamDocumentContentBuilder content(InputStream content) {
         if (isReconstitute()) {
            this.content = content;
         } else {
            content(content, null, null);
         }
         return this;
      }

      @Setter
      public InputStreamDocumentContentBuilder contentEncoding(Charset contentEncoding) {
         ensureReconstitute();
         this.contentEncoding = contentEncoding;
         return this;
      }

      @Setter
      public InputStreamDocumentContentBuilder contentSize(Long contentSize) {
         ensureReconstitute();
         this.contentSize = contentSize;
         return this;
      }

      public InputStreamDocumentContentBuilder autoDetectEncoding(AutoDetectEncoding autoDetectEncoding) {
         ensureNotReconstitute();
         this.autoDetectEncoding = nullable(autoDetectEncoding, AutoDetectEncoding.noDetection());
         return this;
      }

      /**
       * Maps {@link #autoDetectEncoding}, input builder is never {@code null}.
       */
      public InputStreamDocumentContentBuilder autoDetectEncoding(Function<? super AutoDetectEncoding, ? extends AutoDetectEncoding> autoDetectEncodingMapper) {
         return autoDetectEncoding(autoDetectEncodingMapper.apply(autoDetectEncoding));
      }

      public InputStreamDocumentContentBuilder finalize(Consumer<InputStream> finalize) {
         this.finalize = finalize;
         return this;
      }

      @Override
      public InputStreamDocumentContent buildDomainObject() {
         return new InputStreamDocumentContent(this);
      }

      public static class AutoDetectEncoding {
         /** Default detection length. */
         protected static final int DEFAULT_DETECTION_LENGTH = 1024;
         /** Whether to automatically remove BOM by default if auto-detect encodings are set. */
         protected static final boolean DEFAULT_REMOVE_BOM = false;
         /** Default confidence threshold. */
         protected static final float DEFAULT_CONFIDENCE_THRESHOLD = 0.3f;

         private final boolean enabled;
         private final EncodingDetector detector;
         private final int detectionLength;
         private final List<Charset> encodings;
         private final float confidenceThreshold;
         private final boolean removeBom;

         protected AutoDetectEncoding(boolean enabled,
                                      EncodingDetector detector,
                                      int detectionLength,
                                      List<Charset> encodings,
                                      float confidenceThreshold,
                                      boolean removeBom) {
            this.enabled = enabled;
            this.detector = detector;
            this.detectionLength = detectionLength;
            this.encodings = Check.noNullElements(encodings, "encodings");
            this.confidenceThreshold = confidenceThreshold;
            this.removeBom = removeBom;
         }

         public static AutoDetectEncoding noDetection() {
            return new AutoDetectEncoding(false,
                                          null,
                                          DEFAULT_DETECTION_LENGTH,
                                          list(),
                                          DEFAULT_CONFIDENCE_THRESHOLD,
                                          DEFAULT_REMOVE_BOM);
         }

         public static AutoDetectEncoding detectEncodings(List<Charset> encodings) {
            return new AutoDetectEncoding(true,
                                          null,
                                          DEFAULT_DETECTION_LENGTH,
                                          encodings,
                                          DEFAULT_CONFIDENCE_THRESHOLD,
                                          DEFAULT_REMOVE_BOM);
         }

         public static AutoDetectEncoding detectEncodings() {
            return detectEncodings(list());
         }

         public static AutoDetectEncoding detectEncodings(Charset... encodings) {
            return detectEncodings(list(encodings));
         }

         /**
          * Returns {@code true} if content encoding auto-detection is enabled.
          */
         @Getter
         public boolean enabled() {
            return enabled;
         }

         @Setter
         public AutoDetectEncoding enabled(boolean enabled) {
            return new AutoDetectEncoding(enabled,
                                          detector,
                                          detectionLength,
                                          encodings,
                                          confidenceThreshold,
                                          removeBom);
         }

         /**
          * Returns detector to use or {@link Optional#empty()} if default detector is used.
          */
         @Getter
         public Optional<EncodingDetector> detector() {
            return nullable(detector);
         }

         @Setter
         public AutoDetectEncoding detector(EncodingDetector detector) {
            return new AutoDetectEncoding(enabled,
                                          detector,
                                          detectionLength,
                                          encodings,
                                          confidenceThreshold,
                                          removeBom);
         }

         @Getter
         public int detectionLength() {
            return detectionLength;
         }

         @Setter
         public AutoDetectEncoding detectionLength(int detectionLength) {
            return new AutoDetectEncoding(enabled,
                                          detector,
                                          detectionLength,
                                          encodings,
                                          confidenceThreshold,
                                          removeBom);
         }

         @Getter
         public List<Charset> encodings() {
            return encodings;
         }

         @Setter
         public AutoDetectEncoding encodings(List<Charset> encodings) {
            return new AutoDetectEncoding(enabled,
                                          detector,
                                          detectionLength,
                                          encodings,
                                          confidenceThreshold,
                                          removeBom);
         }

         public AutoDetectEncoding encodings(Charset... encodings) {
            return encodings(list(encodings));
         }

         /**
          * Minimum detection confidence value to set detected encoding.
          */
         @Getter
         public float confidenceThreshold() {
            return confidenceThreshold;
         }

         @Setter
         public AutoDetectEncoding confidenceThreshold(float confidenceThreshold) {
            return new AutoDetectEncoding(enabled,
                                          detector,
                                          detectionLength,
                                          encodings,
                                          confidenceThreshold,
                                          removeBom);
         }

         @Getter
         public boolean removeBom() {
            return removeBom;
         }

         @Setter
         public AutoDetectEncoding removeBom(boolean removeBom) {
            return new AutoDetectEncoding(enabled,
                                          detector,
                                          detectionLength,
                                          encodings,
                                          confidenceThreshold,
                                          removeBom);
         }
      }
   }

   /**
    * {@link InputStream} that automatically calls an arbitrary finalize operation at close.
    */
   private static class AutoFinalizingInputStream extends InputStream {

      private final InputStream inputStream;
      private final Consumer<InputStream> finalize;
      private boolean closed = false;

      public AutoFinalizingInputStream(InputStream inputStream, Consumer<InputStream> finalize) {
         this.inputStream = Check.notNull(inputStream, "inputStream");
         this.finalize = Check.notNull(finalize, "finalize");
      }

      @Override
      public int read() throws IOException {
         return inputStream.read();
      }

      @Override
      public int read(byte[] b) throws IOException {
         return inputStream.read(b);
      }

      @Override
      public int read(byte[] b, int off, int len) throws IOException {
         return inputStream.read(b, off, len);
      }

      @Override
      public long skip(long n) throws IOException {
         return inputStream.skip(n);
      }

      @Override
      public int available() throws IOException {
         return inputStream.available();
      }

      @Override
      public synchronized void mark(int readlimit) {
         inputStream.mark(readlimit);
      }

      @Override
      public synchronized void reset() throws IOException {
         inputStream.reset();
      }

      @Override
      public boolean markSupported() {
         return inputStream.markSupported();
      }

      @Override
      public void close() throws IOException {
         if (!closed) {
            closed = true;

            try {
               inputStream.close();
            } finally {
               finalize.accept(inputStream);
            }
         }
      }
   }

}

