/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.repository;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObject;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotAbsolute;
import static com.tinubu.commons.ddd2.invariant.rules.PredicateRules.satisfies;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.DOCUMENT_URI;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.REPOSITORY_URI;
import static java.util.Collections.emptyList;

import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.stream.Stream;

import com.tinubu.commons.ddd2.domain.repository.Repository;
import com.tinubu.commons.ddd2.domain.specification.Specification;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.lang.validation.CheckReturnValue;
import com.tinubu.commons.ports.document.domain.AbstractDocumentRepository;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata;
import com.tinubu.commons.ports.document.domain.ReferencedDocument;
import com.tinubu.commons.ports.document.domain.capability.UnsupportedCapabilityException;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.ExportUriOptions;
import com.tinubu.commons.ports.document.domain.uri.RepositoryUri;

/**
 * No-op {@link DocumentRepository}. No operation fails, but returns only empty optionals and streams.
 *
 * @implSpec Immutable class implementation
 */
// FIXME supports URI noop:
public class NoopDocumentRepository extends AbstractDocumentRepository<NoopDocumentRepository> {

   @Override
   public boolean sameRepositoryAs(Repository<Document, DocumentPath> documentRepository) {
      validate(documentRepository, "documentRepository", isNotNull()).orThrow();

      return false;
   }

   @Override
   public NoopDocumentRepository subPath(Path subPath, boolean shareContext) {
      validate(subPath, "subPath", isNotAbsolute().andValue(hasNoTraversal())).orThrow();

      return this;
   }

   @Override
   @CheckReturnValue
   public Optional<Document> openDocument(DocumentPath documentId,
                                          boolean overwrite,
                                          boolean append,
                                          OpenDocumentMetadata metadata) {
      validate(documentId, "documentId", isNotNull())
            .and(validate(metadata, "metadata", isNotNull()))
            .orThrow();

      return optional();
   }

   @Override
   public Optional<Document> findDocumentById(DocumentPath documentId) {
      validate(documentId, "documentId", isNotNull()).orThrow();

      return optional();
   }

   @Override
   public Stream<Document> findDocumentsBySpecification(Path basePath,
                                                        Specification<DocumentEntry> specification) {
      validate(basePath, "basePath", isNull().orValue(isNotAbsolute().andValue(hasNoTraversal())))
            .and(validate(specification, "documentId", isNotNull()))
            .orThrow();

      return stream();
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryById(DocumentPath documentId) {
      validate(documentId, "documentId", isNotNull()).orThrow();

      return optional();
   }

   @Override
   public Stream<DocumentEntry> findDocumentEntriesBySpecification(Path basePath,
                                                                   Specification<DocumentEntry> specification) {
      validate(basePath, "basePath", isNull().orValue(isNotAbsolute().andValue(hasNoTraversal())))
            .and(validate(specification, "specification", isNotNull()))
            .orThrow();

      return stream();
   }

   @Override
   @CheckReturnValue
   public Optional<DocumentEntry> saveDocument(Document document, boolean overwrite) {
      validate(document, "document", isNotNull()).orThrow();

      return optional();
   }

   @Override
   @CheckReturnValue
   public Optional<Document> saveAndReturnDocument(Document document, boolean overwrite) {
      validate(document, "document", isNotNull()).orThrow();

      return optional();
   }

   @Override
   @CheckReturnValue
   public Optional<DocumentEntry> deleteDocumentById(DocumentPath documentId) {
      validate(documentId, "documentId", isNotNull()).orThrow();

      return optional();
   }

   @Override
   @CheckReturnValue
   public List<DocumentEntry> deleteDocumentsBySpecification(Path basePath,
                                                             Specification<DocumentEntry> specification) {
      validate(basePath, "basePath", isNull().orValue(isNotAbsolute().andValue(hasNoTraversal())))
            .and(validate(specification, "specification", isNotNull()))
            .orThrow();

      return emptyList();
   }

   @Override
   public ReferencedDocument<? extends DocumentRepository> referencedDocumentId(DocumentUri documentUri) {
      Check.validate(documentUri,
                     "documentUri",
                     satisfies(this::supportsUri, "'%s' must be supported", validatingObject()));

      throw new UnsupportedCapabilityException(DOCUMENT_URI);
   }

   @Override
   public RepositoryUri toUri(ExportUriOptions options) {
      throw new UnsupportedCapabilityException(REPOSITORY_URI);
   }

   @Override
   public DocumentUri toUri(DocumentPath documentId, ExportUriOptions options) {
      throw new UnsupportedCapabilityException(DOCUMENT_URI);
   }

   @Override
   public boolean supportsUri(RepositoryUri uri) {
      Check.notNull(uri, "uri");

      return false;
   }

   @Override
   public Optional<Document> findDocumentByUri(DocumentUri documentUri) {
      Check.validate(documentUri,
                     "documentUri",
                     satisfies(this::supportsUri, "'%s' must be supported", validatingObject()));

      return optional();
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryByUri(DocumentUri documentUri) {
      Check.validate(documentUri,
                     "documentUri",
                     satisfies(this::supportsUri, "'%s' must be supported", validatingObject()));

      return optional();
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", NoopDocumentRepository.class.getSimpleName() + "[", "]").toString();
   }
}
