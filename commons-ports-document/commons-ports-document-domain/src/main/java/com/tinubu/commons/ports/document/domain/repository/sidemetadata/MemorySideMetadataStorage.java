/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.repository.sidemetadata;

import static com.tinubu.commons.ddd2.domain.type.support.DomainObjectSupport.checkInvariants;
import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObject;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.notSatisfiesValue;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.ignore;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.ports.document.domain.rules.DocumentRules.DocumentEntryRules.documentId;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.formatter.FastStringFormat;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentEntrySpecification;
import com.tinubu.commons.ports.document.domain.DocumentMetadata;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;

/**
 * Side metadata storage using memory.
 */
public class MemorySideMetadataStorage extends AbstractValue implements SideMetadataStorage {

   /**
    * Side metadata memory storage, indexed by original document path.
    */
   private final Map<DocumentPath, DocumentMetadata> storage = new ConcurrentHashMap<>();

   private final DocumentRepository repository;

   protected MemorySideMetadataStorage(DocumentRepository repository) {
      this.repository = repository;
   }

   public static MemorySideMetadataStorage of(DocumentRepository repository) {
      return checkInvariants(new MemorySideMetadataStorage(repository));
   }

   @Override
   public Fields<? extends MemorySideMetadataStorage> defineDomainFields() {
      return Fields
            .<MemorySideMetadataStorage>builder()
            .field("repository", v -> v.repository, isNotNull())
            .technicalField("storage", v -> v.storage)
            .build();
   }

   @Override
   public boolean isSideMetadata(DocumentPath documentId) {
      notNull(documentId, "documentId");

      return false;
   }

   @Override
   public DocumentEntrySpecification sideMetadataSpecification() {
      return DocumentEntrySpecification.noneDocuments();
   }

   @Override
   public void cleanOrphanSideMetadata() {
      storage.entrySet().removeIf(storageEntry -> {
         DocumentPath originalDocument = storageEntry.getKey();

         return repository.findDocumentById(originalDocument).isEmpty();
      });
   }

   @Override
   public void cleanAllSideMetadata() {
      storage.clear();
   }

   /**
    * Load specified document's side-metadata.
    *
    * @param documentId original document identifier to load side-metadata for
    *
    * @return side metadata or {@link Optional#empty} if no side-metadata found
    *
    * @implNote Site metadata content-type must be forced or we have a chicken-and-egg problem with
    *       the underlying repository.
    */
   @Override
   public Optional<DocumentMetadata> loadMetadata(DocumentPath documentId) {
      validate(documentId, "documentId", isNotSideMetadata()).orThrow();

      return nullable(storage.get(documentId));
   }

   @Override
   public void saveMetadata(DocumentEntry documentEntry) {
      validate(documentEntry, "documentEntry", documentId(isNotSideMetadata())).orThrow();

      storage.put(documentEntry.documentId(), documentEntry.metadata());
   }

   @Override
   public void deleteMetadata(DocumentPath documentId) {
      validate(documentId, "documentId", isNotSideMetadata()).orThrow();

      ignore(storage.remove(documentId));
   }

   private InvariantRule<DocumentPath> isNotSideMetadata() {
      return isNotNull().andValue(notSatisfiesValue(this::isSideMetadata,
                                                    FastStringFormat.of("'",
                                                                        validatingObject(),
                                                                        "' must not be a direct side-metadata document")).ruleContext(
            "MemorySideMetadataStorage.isNotSideMetadata"));
   }

}
