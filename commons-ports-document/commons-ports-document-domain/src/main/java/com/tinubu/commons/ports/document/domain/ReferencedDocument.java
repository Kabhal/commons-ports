/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static com.tinubu.commons.ddd2.domain.type.support.DomainObjectSupport.checkInvariants;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.as;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd2.invariant.rules.EqualsRules.isEqualTo;
import static com.tinubu.commons.lang.util.CheckedRunnable.checkedRunnable;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.OptionalUtils.peek;

import java.util.Optional;
import java.util.function.Predicate;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.Invariant;
import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ports.document.domain.rules.DocumentRules.DocumentMainRules;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.ExportUriOptions;

/**
 * Defines a reference to a document in a document repository.
 * <p>
 * Holds a ({@link DocumentRepository documentRepository}, {@link DocumentPath documentId},
 * [{@link Document document}]).
 * <p>
 * Document is optional and can be eagerly loaded or lazily loaded. In any case, document is always loaded
 * at most once and cached, so that document closing management is easier.
 * <p>
 * {@link Document} and {@link DocumentRepository} closing is managed externally. To support closing, you can
 * use {@link #close()} that ensures that document is closed, then document repository is closed only if
 * {@code autoCloseRepository} set for the class.
 */
public class ReferencedDocument<T extends DocumentRepository> extends AbstractValue implements AutoCloseable {

   protected final T documentRepository;
   protected final DocumentPath documentId;
   protected Document document;
   protected final boolean autoCloseRepository;

   protected ReferencedDocument(T documentRepository,
                                DocumentPath documentId,
                                Document document,
                                boolean autoCloseRepository) {
      this.documentRepository = documentRepository;
      this.documentId = documentId;
      this.document = document;
      this.autoCloseRepository = autoCloseRepository;
   }

   protected ReferencedDocument(T documentRepository, DocumentPath documentId, Document document) {
      this(documentRepository, documentId, document, false);
   }

   @Override
   @SuppressWarnings("unchecked")
   public Fields<? extends ReferencedDocument<T>> defineDomainFields() {
      return Fields
            .<ReferencedDocument<T>>builder()
            .superFields((Fields<ReferencedDocument<T>>) super.defineDomainFields())
            .field("documentRepository", v -> v.documentRepository, isNotNull())
            .field("documentId", v -> v.documentId, isNotNull())
            .field("document",
                   v -> v.document,
                   isNull().orValue(DocumentMainRules.documentId(isEqualTo(value(documentId, "documentId")))))
            .invariants(Invariant
                              .of(() -> this, as(rd -> rd.document, __ -> "document", isNotNull()))
                              .name("requiredDocument")
                              .groups("requiredDocument"))
            .build();
   }

   /**
    * Creates a referenced document from a repository and an existing document.
    * <p>
    * Document is not checked for existence in specified repository.
    *
    * @param documentRepository document repository
    * @param document existing document
    *
    * @return new referenced document
    */
   public static <T extends DocumentRepository> ReferencedDocument<T> of(T documentRepository,
                                                                         Document document) {
      return checkInvariants(new ReferencedDocument<>(documentRepository,
                                                      nullable(document)
                                                            .map(Document::documentId)
                                                            .orElse(null),
                                                      document));
   }

   /**
    * Creates a referenced document from a repository and a document identifier.
    * <p>
    * Document is not loaded immediately from repository, and could be lazily loaded later. Hence, document is
    * not checked for existence in specified repository.
    *
    * @param documentRepository document repository
    * @param documentId document identifier
    *
    * @return new referenced document
    */
   public static <T extends DocumentRepository> ReferencedDocument<T> lazyLoad(T documentRepository,
                                                                               DocumentPath documentId) {
      return checkInvariants(new ReferencedDocument<>(documentRepository, documentId, null),
                             "!requiredDocument");
   }

   /**
    * Creates a referenced document from a repository and a document identifier.
    * <p>
    * Document is tried to be loaded immediately from repository, but could be lazily loaded later if document
    * is not found.
    *
    * @param documentRepository document repository
    * @param documentId document identifier
    *
    * @return new referenced document
    */
   public static <T extends DocumentRepository> ReferencedDocument<T> tryLoad(T documentRepository,
                                                                              DocumentPath documentId) {
      return checkInvariants(new ReferencedDocument<>(documentRepository,
                                                      documentId,
                                                      nullable(documentId)
                                                            .flatMap(documentRepository::findDocumentById)
                                                            .orElse(null)), "!requiredDocument");
   }

   /**
    * Creates a referenced document from a repository and a document identifier.
    * <p>
    * Document is loaded immediately from repository and operation fails immediately if document is not found.
    *
    * @param documentRepository document repository
    * @param documentId document identifier
    *
    * @return new referenced document
    *
    * @throws InvariantValidationException if document is not present in repository
    */
   public static <T extends DocumentRepository> ReferencedDocument<T> requireLoadOrThrow(T documentRepository,
                                                                                         DocumentPath documentId) {
      return checkInvariants(new ReferencedDocument<T>(documentRepository,
                                                       documentId,
                                                       nullable(documentId)
                                                             .flatMap(documentRepository::findDocumentById)
                                                             .orElse(null)));
   }

   /**
    * Creates a referenced document from a repository and a document identifier.
    * <p>
    * Document is loaded immediately from repository. Returns {@link Optional#empty} if document is not found.
    *
    * @param documentRepository document repository
    * @param documentId document identifier
    *
    * @return new referenced document, or {@link Optional#empty} of document is not found
    */
   public static <T extends DocumentRepository> Optional<ReferencedDocument<T>> requireLoad(T documentRepository,
                                                                                            DocumentPath documentId) {
      return nullable(documentId)
            .flatMap(documentRepository::findDocumentById)
            .map(document -> checkInvariants(new ReferencedDocument<>(documentRepository,
                                                                      documentId,
                                                                      document)));
   }

   public String documentDisplayId() {
      return documentRepository.documentDisplayId(documentId);
   }

   public DocumentUri toUri(ExportUriOptions options) {
      return documentRepository.toUri(documentId, options);
   }

   /**
    * Sets document repository auto-closing when calling {@link #close()}.
    *
    * @param autoCloseRepository whether to auto-close the repository
    *
    * @return new instance
    */
   public ReferencedDocument<T> autoCloseRepository(boolean autoCloseRepository) {
      return new ReferencedDocument<>(documentRepository, documentId, document, autoCloseRepository);
   }

   /**
    * Enables document repository auto-closing when calling {@link #close()}.
    *
    * @return new instance
    */
   public ReferencedDocument<T> autoCloseRepository() {
      return autoCloseRepository(true);
   }

   /**
    * Returns document repository.
    *
    * @return document repository
    */
   public T documentRepository() {
      return documentRepository;
   }

   /**
    * Returns document id.
    *
    * @return document id
    */
   public DocumentPath documentId() {
      return documentId;
   }

   /**
    * Returns document entry.
    *
    * @param loadPredicate whether to load or reload the document. If document is not present, predicate
    *       is called with {@code null} value. If predicate is not satisfied, returns current document state.
    *
    * @return document entry or {@link Optional#empty} if document is not loaded
    */
   public Optional<DocumentEntry> documentEntry(Predicate<Document> loadPredicate) {
      return document(loadPredicate).map(Document::documentEntry);
   }

   /**
    * Returns document entry.
    *
    * @param lazyLoad whether to try to load the document if document is not already loaded. Returns
    *       {@link Optional#empty} if document is not found in repository.
    * @param forceReload whether to reload document if document is already loaded. Returns
    *       {@link Optional#empty} if document is not found in repository.
    *
    * @return document entry or {@link Optional#empty} if document is not loaded
    */
   public Optional<DocumentEntry> documentEntry(boolean lazyLoad, boolean forceReload) {
      return documentEntry(loadPredicate(lazyLoad, forceReload));
   }

   /**
    * Returns document entry.
    *
    * @param lazyLoad whether to try to load the document if document is not already loaded. Returns
    *       {@link Optional#empty} if document is not found in repository.
    *
    * @return document entry or {@link Optional#empty} if document is not loaded
    */
   public Optional<DocumentEntry> documentEntry(boolean lazyLoad) {
      return documentEntry(lazyLoad, false);
   }

   /**
    * Returns document entry if document is loaded.
    *
    * @return document entry or {@link Optional#empty} if document is not loaded
    */
   public Optional<DocumentEntry> documentEntry() {
      return documentEntry(false, false);
   }

   /**
    * Returns document entry.
    *
    * @param loadPredicate whether to load or reload the document. If document is not present, predicate
    *       is called with {@code null} value. If predicate is not satisfied, returns current document state.
    *
    * @return document entry
    *
    * @throws IllegalStateException if document is not loaded
    */
   public DocumentEntry requiredDocumentEntry(Predicate<Document> loadPredicate) {
      return requiredDocument(loadPredicate).documentEntry();
   }

   /**
    * Returns document entry.
    *
    * @param lazyLoad whether to load the document if document is not already loaded. Throws exception
    *       if document is not found in repository.
    * @param forceReload whether to reload document if document is already loaded. Returns
    *       {@link Optional#empty} if document is not found in repository.
    *
    * @return document entry
    *
    * @throws IllegalStateException if document is not loaded
    */
   public DocumentEntry requiredDocumentEntry(boolean lazyLoad, boolean forceReload) {
      return requiredDocumentEntry(loadPredicate(lazyLoad, forceReload));
   }

   /**
    * Returns document entry.
    *
    * @param lazyLoad whether to load the document if document is not already loaded. Throws exception
    *       if document is not found in repository.
    *
    * @return document entry
    *
    * @throws IllegalStateException if document is not loaded
    */
   public DocumentEntry requiredDocumentEntry(boolean lazyLoad) {
      return requiredDocumentEntry(lazyLoad, false);
   }

   /**
    * Returns document entry.
    *
    * @return document entry
    *
    * @throws IllegalStateException if document is not loaded
    */
   public DocumentEntry requiredDocumentEntry() {
      return requiredDocumentEntry(false, false);
   }

   /**
    * Tries to load document. Does nothing if document is already loaded.
    * No error is reported if document is not found in repository.
    *
    * @param loadPredicate whether to load or reload the document. If document is not present, predicate
    *       is called with {@code null} value. If predicate is not satisfied, returns current document state.
    */
   public ReferencedDocument<T> tryLoadDocument(Predicate<Document> loadPredicate) {
      document(loadPredicate);
      return this;
   }

   /**
    * Tries to load document. Does nothing if document is already loaded.
    * No error is reported if document is not found in repository.
    *
    * @param forceReload whether to reload document if document is already loaded. Returns
    *       {@link Optional#empty} if document is not found in repository.
    */
   public ReferencedDocument<T> tryLoadDocument(boolean forceReload) {
      return tryLoadDocument(loadPredicate(true, forceReload));
   }

   /**
    * Tries to load document. Does nothing if document is already loaded.
    * No error is reported if document is not found in repository.
    */
   public ReferencedDocument<T> tryLoadDocument() {
      return tryLoadDocument(false);
   }

   /**
    * Load document. Does nothing if document is already loaded.
    *
    * @param loadPredicate whether to load or reload the document. If document is not present, predicate
    *       is called with {@code null} value. If predicate is not satisfied, returns current document state.
    *
    * @throws IllegalStateException if document is not found in repository
    */
   public ReferencedDocument<T> requireLoadDocument(Predicate<Document> loadPredicate) {
      requiredDocument(loadPredicate);
      return this;
   }

   /**
    * Load document. Does nothing if document is already loaded.
    *
    * @param forceReload whether to reload document if document is already loaded. Returns
    *       {@link Optional#empty} if document is not found in repository.
    *
    * @throws IllegalStateException if document is not found in repository
    */
   public ReferencedDocument<T> requireLoadDocument(boolean forceReload) {
      return requireLoadDocument(loadPredicate(true, forceReload));
   }

   /**
    * Load document. Does nothing if document is already loaded.
    *
    * @throws IllegalStateException if document is not found in repository
    */
   public ReferencedDocument<T> requireLoadDocument() {
      return requireLoadDocument(false);
   }

   /**
    * Returns document.
    *
    * @param loadPredicate whether to load or reload the document. If document is not present, predicate
    *       is called with {@code null} value. If predicate is not satisfied, returns current document state.
    *
    * @return document or {@link Optional#empty} if document is not loaded
    */
   public Optional<Document> document(Predicate<Document> loadPredicate) {
      return nullable(document).flatMap(d -> {
         if (loadPredicate.test(d)) {
            document.content().close();
            return peek(documentRepository.findDocumentById(documentId), loadDocument -> {
               document = loadDocument;
            });
         } else {
            return optional(d);
         }
      }).or(() -> {
         if (loadPredicate.test(null)) {
            return peek(documentRepository.findDocumentById(documentId),
                        loadDocument -> document = loadDocument);
         } else {
            return optional();
         }
      });
   }

   /**
    * Returns document.
    *
    * @param lazyLoad whether to try to load the document if document is not already loaded. Returns
    *       {@link Optional#empty} if document is not found in repository.
    * @param forceReload whether to reload document if document is already loaded. Returns
    *       {@link Optional#empty} if document is not found in repository.
    *
    * @return document or {@link Optional#empty} if document is not loaded
    */
   public Optional<Document> document(boolean lazyLoad, boolean forceReload) {
      return document(loadPredicate(lazyLoad, forceReload));
   }

   /**
    * Returns document if loaded.
    *
    * @return document or {@link Optional#empty} if document is not loaded
    */
   public Optional<Document> document() {
      return nullable(document);
   }

   /**
    * Returns document.
    *
    * @param loadPredicate whether to load or reload the document. If document is not present, predicate
    *       is called with {@code null} value. If predicate is not satisfied, returns current document state.
    *
    * @return document
    *
    * @throws IllegalStateException if document is not loaded
    */
   public Document requiredDocument(Predicate<Document> loadPredicate) {
      return document(loadPredicate).orElseThrow(() -> new IllegalStateException(String.format(
            "'%s' document is required",
            documentId.stringValue())));
   }

   /**
    * Returns document.
    *
    * @param lazyLoad whether to load the document if document is not already loaded. Throws exception
    *       if document is not found in repository.
    * @param forceReload whether to reload document if document is already loaded. Returns
    *       {@link Optional#empty} if document is not found in repository.
    *
    * @return document
    *
    * @throws IllegalStateException if document is not loaded
    */
   public Document requiredDocument(boolean lazyLoad, boolean forceReload) {
      return requiredDocument(loadPredicate(lazyLoad, forceReload));
   }

   /**
    * Returns document.
    *
    * @param lazyLoad whether to load the document if document is not already loaded. Throws exception
    *       if document is not found in repository.
    *
    * @return document
    *
    * @throws IllegalStateException if document is not loaded
    */
   public Document requiredDocument(boolean lazyLoad) {
      return requiredDocument(lazyLoad, false);
   }

   /**
    * Returns document.
    *
    * @return document
    *
    * @throws IllegalStateException if document is not loaded
    */
   public Document requiredDocument() {
      return nullable(document).orElseThrow(() -> new IllegalStateException(String.format(
            "'%s' document is required",
            documentId.stringValue())));
   }

   /**
    * Closes document if it is loaded, then close repository if {@link #autoCloseRepository} is set.
    *
    * @throws Exception if an error occurs while closing
    */
   @Override
   @SuppressWarnings("unchecked")
   public void close() throws Exception {
      checkedRunnable(() -> document().ifPresent(document -> document.content().close())).tryFinally(() -> {
         if (autoCloseRepository) {
            documentRepository().close();
         }
      });
   }

   private static Predicate<Document> loadPredicate(boolean lazyLoad, boolean forceReload) {
      return d -> (d == null && lazyLoad) || (d != null && forceReload);
   }

}
