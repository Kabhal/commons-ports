/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.repository.cache;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotAbsolute;
import static com.tinubu.commons.lang.util.CheckedRunnable.checkedRunnable;
import static com.tinubu.commons.lang.util.CollectionUtils.collectionIntersection;
import static com.tinubu.commons.lang.util.CollectionUtils.collectionSubtraction;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.OptionalUtils.peek;
import static com.tinubu.commons.lang.util.OptionalUtils.peekEmpty;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.util.StreamUtils.streamConcat;
import static com.tinubu.commons.lang.validation.Validate.ignore;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.DOCUMENT_URI;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.READABLE;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.REPOSITORY_URI;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.WRITABLE;
import static com.tinubu.commons.ports.document.domain.repository.cache.eviction.CacheEvictionStrategy.ContextOperation.ACCESS;
import static com.tinubu.commons.ports.document.domain.repository.cache.eviction.CacheEvictionStrategy.ContextOperation.DELETE;
import static com.tinubu.commons.ports.document.domain.repository.cache.eviction.CacheEvictionStrategy.ContextOperation.SAVE;
import static com.tinubu.commons.ports.document.domain.rules.DocumentRepositoryRules.hasCapabilities;

import java.nio.file.Path;
import java.time.Duration;
import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.stream.Stream;

import org.apache.commons.lang3.time.StopWatch;

import com.tinubu.commons.ddd2.domain.event.RegistrableDomainEventService;
import com.tinubu.commons.ddd2.domain.event.SynchronousDomainEventService;
import com.tinubu.commons.ddd2.domain.repository.Repository;
import com.tinubu.commons.ddd2.domain.specification.Specification;
import com.tinubu.commons.lang.datetime.ApplicationClock;
import com.tinubu.commons.lang.mapper.SchwartzianTransformer;
import com.tinubu.commons.lang.util.Pair;
import com.tinubu.commons.lang.validation.CheckReturnValue;
import com.tinubu.commons.ports.document.domain.AbstractDocumentRepository;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata;
import com.tinubu.commons.ports.document.domain.OutputStreamDocumentContent;
import com.tinubu.commons.ports.document.domain.ReferencedDocument;
import com.tinubu.commons.ports.document.domain.capability.RepositoryCapability;
import com.tinubu.commons.ports.document.domain.capability.UnsupportedCapabilityException;
import com.tinubu.commons.ports.document.domain.repository.cache.event.CacheEvicted;
import com.tinubu.commons.ports.document.domain.repository.cache.event.CacheHit;
import com.tinubu.commons.ports.document.domain.repository.cache.event.CacheLoaded;
import com.tinubu.commons.ports.document.domain.repository.cache.event.CacheMissed;
import com.tinubu.commons.ports.document.domain.repository.cache.event.CacheSaved;
import com.tinubu.commons.ports.document.domain.repository.cache.eviction.CacheEvictionStrategy;
import com.tinubu.commons.ports.document.domain.repository.cache.eviction.CacheEvictionStrategy.ContextOperation;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.ExportUriOptions;
import com.tinubu.commons.ports.document.domain.uri.RepositoryUri;

/**
 * Caching {@link DocumentRepository}.
 * <p>
 * Cache repository capabilities are restrictively set to the intersection of source and cache repository
 * capabilities. Note that each operation depends dynamically on one or another repository capabilities
 * depending on caching state and other behaviors (e.g.: returning a document either from cache or from
 * source repository).
 * <p>
 * Operations are not transactional, so that if an error occurs while updating cache, the open/save/delete
 * operations on source repository will nonetheless be committed.
 * <p>
 * Moreover, changes directly made on source repository without using this cache layer operations are not
 * reflected onto this cache :
 * <ul>
 *    <li>deleted documents will stay in cache until expiration</li>
 * </ul>
 * <p>
 * This repository has the following specific behaviors :
 * <ul>
 *    <li>{@link #sameRepositoryAs(Repository)} compares underlying source repository instead of itself</li>
 *    <li>Encapsulated delegate repository is closed on {@link #close()}</li>
 * </ul>
 *
 * @implSpec Immutable class implementation.
 */
// FIXME delegates URI operation instead of UnsupportedOPException ?
public class CacheDocumentRepository extends AbstractDocumentRepository<CacheDocumentRepository> {

   private final DocumentRepository sourceRepository;
   private final DocumentRepository cacheRepository;
   private final CacheEvictionStrategy cacheEvictionStrategy;

   private final Duration refreshSourceMetadataMaximumDuration;
   private final Map<Specification<?>, Instant> refreshSourceMetadataContext;
   private boolean closed = false;

   /**
    * @implSpec {@link RepositoryCapability#QUERYABLE} and {@link RepositoryCapability#SUBPATH}
    *       capabilities are not enforced on backing cache repository, so that failure only occurs lazily if
    *       {@link #subPath(Path, boolean)} or find by specification operations are effectively used.
    */
   private CacheDocumentRepository(DocumentRepository sourceRepository,
                                   DocumentRepository cacheRepository,
                                   CacheEvictionStrategy cacheEvictionStrategy,
                                   Duration refreshSourceMetadataMaximumDuration,
                                   RegistrableDomainEventService eventService) {
      super(eventService);
      this.sourceRepository = validate(sourceRepository, "sourceRepository", isNotNull()).orThrow();
      this.cacheRepository = validate(cacheRepository,
                                      "cacheRepository",
                                      isNotNull().andValue(hasCapabilities(value(list(READABLE,
                                                                                      WRITABLE))))).orThrow();
      this.cacheEvictionStrategy =
            validate(cacheEvictionStrategy, "cacheEvictionStrategy", isNotNull()).orThrow();

      this.refreshSourceMetadataMaximumDuration = refreshSourceMetadataMaximumDuration;
      this.refreshSourceMetadataContext = new ConcurrentHashMap<>();
   }

   /**
    * Creates a caching document repository.
    * Metadata is always read from source repository when searching document/entries by specification.
    *
    * @param sourceRepository source repository for documents
    * @param cacheRepository cache repository for caching documents
    * @param cacheEvictionStrategy cache eviction strategy
    *
    * @return new cache document repository
    */
   public static CacheDocumentRepository of(DocumentRepository sourceRepository,
                                            DocumentRepository cacheRepository,
                                            CacheEvictionStrategy cacheEvictionStrategy) {
      return new CacheDocumentRepository(sourceRepository,
                                         cacheRepository,
                                         cacheEvictionStrategy,
                                         Duration.ZERO,
                                         new SynchronousDomainEventService());
   }

   /**
    * Creates a caching document repository.
    *
    * @param sourceRepository source repository for documents
    * @param cacheRepository cache repository for caching documents
    * @param cacheEvictionStrategy cache eviction strategy
    * @param refreshSourceMetadataMaximumDuration optional maximum duration until metadata is read from
    *       source repository when searching document/entries by specification. Duration from the last search
    *       is recorded for each {@link Object#equals unique} {@link Specification}. Set to
    *       {@link Duration#ZERO} to always refresh, or {@code null} to never refresh. Never refresh mode
    *       should be used with caution and is only suitable if all interesting documents have always been
    *       saved/deleted using this cache document repository instance during its lifecycle.
    *
    * @return new cache document repository
    */
   public static CacheDocumentRepository of(DocumentRepository sourceRepository,
                                            DocumentRepository cacheRepository,
                                            CacheEvictionStrategy cacheEvictionStrategy,
                                            Duration refreshSourceMetadataMaximumDuration) {
      return new CacheDocumentRepository(sourceRepository,
                                         cacheRepository,
                                         cacheEvictionStrategy,
                                         refreshSourceMetadataMaximumDuration,
                                         new SynchronousDomainEventService());
   }

   /**
    * Manually evicts specified document.
    *
    * @param documentId document to evict
    *
    * @return {@code true} if document has been effectively evicted
    */
   public boolean evict(DocumentPath documentId) {
      StopWatch watch = StopWatch.create();

      return watch(watch, () -> cacheRepository.deleteDocumentById(documentId)).map(peek(documentEntry -> {
         updateCacheContext(documentEntry, DELETE);
         publishEvent(new CacheEvicted(this, documentId, duration(watch)));
      })).isPresent();
   }

   @Override
   public HashSet<RepositoryCapability> capabilities() {
      return collectionSubtraction(HashSet::new,
                                   collectionIntersection(HashSet::new,
                                                          sourceRepository.capabilities(),
                                                          cacheRepository.capabilities()),
                                   list(DOCUMENT_URI, REPOSITORY_URI));
   }

   @Override
   public boolean sameRepositoryAs(Repository<Document, DocumentPath> documentRepository) {
      validate(documentRepository, "documentRepository", isNotNull()).orThrow();

      if (documentRepository instanceof CacheDocumentRepository) {
         return ((CacheDocumentRepository) documentRepository).sourceRepository.sameRepositoryAs(
               sourceRepository)
                && ((CacheDocumentRepository) documentRepository).cacheRepository.sameRepositoryAs(
               cacheRepository);
      } else {
         return documentRepository.sameRepositoryAs(sourceRepository);
      }
   }

   /**
    * {@inheritDoc}
    * <p>
    * Sub path operation is applied to source and cache repositories, an
    * {@link UnsupportedCapabilityException} is thrown if one of them is not compatible.
    */
   @Override
   public CacheDocumentRepository subPath(Path subPath, boolean shareContext) {
      validate(subPath, "subPath", isNotAbsolute().andValue(hasNoTraversal())).orThrow();

      return new CacheDocumentRepository(sourceRepository.subPath(subPath, shareContext),
                                         cacheRepository.subPath(subPath, shareContext),
                                         cacheEvictionStrategy.subPathCacheContext(subPath),
                                         refreshSourceMetadataMaximumDuration,
                                         eventService);
   }

   /**
    * {@inheritDoc}
    *
    * @implNote open document is special (content created later), so that document content is not
    *       physically cached. However, cache context is created in eviction strategy, and previous document
    *       is removed from cache if {@code overwrite} or {@code append} is set.
    */
   @Override
   @CheckReturnValue
   public Optional<Document> openDocument(DocumentPath documentId,
                                          boolean overwrite,
                                          boolean append,
                                          OpenDocumentMetadata metadata) {
      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEvent(() -> {
         Optional<Document> document =
               peek(sourceRepository.openDocument(documentId, overwrite, append, metadata), d -> {
                  updateCacheContext(d, SAVE);
                  if (overwrite || append) {
                     cacheRepository.delete(documentId);
                  }
               });

         checkEvictables();

         return document;
      }, d -> documentSaved(d.documentEntry(), watch));
   }

   @Override
   public Optional<Document> findDocumentById(DocumentPath documentId) {
      validate(documentId, "documentId", isNotNull()).orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEvent(() -> {
         Optional<Document> documents = cachedDocument(documentId).or(() -> cacheDocument(documentId));

         checkEvictables();

         return documents;
      }, d -> documentAccessed(d.documentEntry(), watch));
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryById(DocumentPath documentId) {
      validate(documentId, "documentId", isNotNull()).orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEntryEvent(() -> {
         Optional<DocumentEntry> documentEntries =
               cachedDocumentEntry(documentId).or(() -> cacheDocumentEntry(documentId));

         checkEvictables();

         return documentEntries;
      }, d -> documentAccessed(d, watch));
   }

   /**
    * {@inheritDoc}
    *
    * @implNote Even if metadata is refreshed from source repository, cached documents are not evicted
    *       from cache if document does not exist anymore on source, for performance, and to be consistent
    *       with other API operations. Note that if documents are deleted without bypassing the cache
    *       repository API, documents will be removed from cache.
    */
   @Override
   public Stream<Document> findDocumentsBySpecification(Path basePath,
                                                        Specification<DocumentEntry> specification) {
      validate(basePath, "basePath", isNull().orValue(isNotAbsolute().andValue(hasNoTraversal())))
            .and(validate(specification, "documentId", isNotNull()))
            .orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentStreamEvent(() -> {
         Stream<Document> documents = rawFindDocumentPathBySpecification(basePath, specification);

         checkEvictables();

         return documents;
      }, d -> documentAccessed(d.documentEntry(), watch));
   }

   /**
    * {@inheritDoc}
    *
    * @implNote Even if metadata is refreshed from source repository, cached documents are not evicted
    *       from cache if document does not exist anymore on source, for performance, and to be consistent
    *       with other API operations. Note that if documents are deleted without bypassing the cache
    *       repository API, documents will be removed from cache.
    *       Implementation is based on {@link DocumentRepository#findDocumentsBySpecification(Specification)}
    *       as an optimization considering that content should always been an
    *       {@link InputStreamDocumentContent}, so that content is never really downloaded until used.
    */
   @Override
   public Stream<DocumentEntry> findDocumentEntriesBySpecification(Path basePath,
                                                                   Specification<DocumentEntry> specification) {
      validate(basePath, "basePath", isNull().orValue(isNotAbsolute().andValue(hasNoTraversal())))
            .and(validate(specification, "specification", isNotNull()))
            .orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEntryStreamEvent(() -> {
         Stream<DocumentEntry> documentEntries =
               rawFindDocumentPathBySpecification(basePath, specification).map(Document::documentEntry);

         checkEvictables();

         return documentEntries;
      }, d -> documentAccessed(d, watch));
   }

   /**
    * @implNote When refreshing data from source, the design is to retrieve documents from both cache
    *       and source, in this precise order ({@code distinct} keep the first occurrence when duplicate is
    *       found), then to {@code distinct} documents by id so that remaining documents are either :
    *       <ul>
    *          <li>a cached, not evicted, document, from cache repository</li>
    *          <li>a not cached, or cached but evicted document, from source repository</li>
    *       </ul>
    *       Origin repository is associated to each document to distinguish between cases after {@code distinct} operation.
    *       Events duration is set to 0, because we can't distinguish the duration for each individual document from
    *       the single find by specification request.
    */
   private Stream<Document> rawFindDocumentPathBySpecification(Path basePath,
                                                               Specification<DocumentEntry> specification) {
      if (refreshDocumentEntries(specification)) {
         return streamConcat(stream(cacheRepository), stream(sourceRepository))
               .flatMap(repository -> repository
                     .findDocumentsBySpecification(basePath, specification)
                     .filter(d -> repository != cacheRepository || !checkEvictable(d))
                     .map(d -> Pair.of(repository, d)))
               .map(refDocument -> SchwartzianTransformer.wrap(refDocument, p -> p.getValue().documentId()))
               .distinct()
               .map(SchwartzianTransformer::unwrap)
               .flatMap(refDocument -> {
                  if (refDocument.getKey() == sourceRepository) {
                     publishEvent(new CacheMissed(this, refDocument.getValue().documentId(), Duration.ZERO));
                     publishEvent(new CacheLoaded(this, refDocument.getValue().documentId(), Duration.ZERO));
                     return stream(cacheDocument(refDocument.getValue()));
                  } else {
                     publishEvent(new CacheHit(this, refDocument.getValue().documentId(), Duration.ZERO));
                     return stream(refDocument.getValue());
                  }
               });
      } else {
         StopWatch watch = StopWatch.create();

         return watch(watch,
                      () -> cacheRepository.findDocumentsBySpecification(basePath, specification)).flatMap(
               document -> {
                  if (checkEvictable(document)) {
                     return stream(cacheDocument(document.documentId()).map(peek(__ -> {
                        // Special consideration : cache miss only if evicted document still exist in source,
                        // otherwise it's just that document should not be returned by operation.
                        publishEvent(new CacheMissed(this, document.documentId(), Duration.ZERO));
                     })));
                  } else {
                     publishEvent(new CacheHit(this, document.documentId(), Duration.ZERO));
                     return stream(document);
                  }
               });
      }
   }

   /**
    * Returns {@code true} if we should refresh document entries from source repository for the given
    * specification. This refresh strategy is not related to cache eviction strategy, but is dedicated to
    * "search by specification" operations which do not know document identifiers before call to source.
    * Never refresh if {@link #refreshSourceMetadataMaximumDuration} is not set.
    * Always refresh if {@link #refreshSourceMetadataMaximumDuration} is set to {@link Duration#ZERO}.
    *
    * @param specification request specification
    *
    * @return {@code true} if we should refresh metadata from source repository
    *
    * @implSpec {@link Specification} is compared using
    *       {@link Object#equals(Object)}/{@link Object#hashCode()}. Caution, if
    *       correct implementation is missing, specifications are compared by address.
    */
   private boolean refreshDocumentEntries(Specification<DocumentEntry> specification) {
      if (refreshSourceMetadataMaximumDuration == Duration.ZERO) {
         return true;
      } else if (refreshSourceMetadataMaximumDuration == null) {
         return false;
      } else {
         Instant now = ApplicationClock.nowAsInstant();
         AtomicBoolean refresh = new AtomicBoolean(false);

         refreshSourceMetadataContext.compute(specification, (spec, lastSearch) -> {
            if (lastSearch == null || lastSearch.plus(refreshSourceMetadataMaximumDuration).isBefore(now)) {
               refresh.set(true);
               return now;
            } else {
               return lastSearch;
            }
         });

         return refresh.get();
      }
   }

   /**
    * {@inheritDoc}
    */
   @Override
   @CheckReturnValue
   public Optional<DocumentEntry> saveDocument(Document document, boolean overwrite) {
      notNull(document, "document");

      if (document.content() instanceof OutputStreamDocumentContent) {
         throw new IllegalArgumentException(
               "Unsupported operation for this document content implementation : " + document
                     .content()
                     .getClass()
                     .getName());
      }

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEntryEvent(() -> {
         Optional<DocumentEntry> documentEntry = sourceRepository
               .saveAndReturnDocument(document, overwrite)
               .map(this::cacheDocument)
               .map(Document::documentEntry);

         checkEvictables();

         return documentEntry;
      }, d -> documentSaved(d, watch));
   }

   /**
    * {@inheritDoc}
    */
   @Override
   @CheckReturnValue
   public Optional<Document> saveAndReturnDocument(Document document, boolean overwrite) {
      validate(document, "document", isNotNull()).orThrow();

      if (document.content() instanceof OutputStreamDocumentContent) {
         throw new IllegalArgumentException(
               "Unsupported operation for this document content implementation : " + document
                     .content()
                     .getClass()
                     .getName());
      }

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEvent(() -> {
         Optional<Document> saveDocument =
               sourceRepository.saveAndReturnDocument(document, overwrite).map(this::cacheDocument);

         checkEvictables();

         return saveDocument;
      }, d -> documentSaved(d.documentEntry(), watch));
   }

   /**
    * {@inheritDoc}
    */
   @Override
   @CheckReturnValue
   public Optional<DocumentEntry> deleteDocumentById(DocumentPath documentId) {
      validate(documentId, "documentId", isNotNull()).orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEntryEvent(() -> {
         Optional<DocumentEntry> deletedDocumentEntry =
               sourceRepository.deleteDocumentById(documentId).map(this::deleteCachedDocument);

         checkEvictables();

         return deletedDocumentEntry;
      }, d -> documentDeleted(d, watch));
   }

   @Override
   @CheckReturnValue
   public List<DocumentEntry> deleteDocumentsBySpecification(Path basePath,
                                                             Specification<DocumentEntry> specification) {
      validate(basePath, "basePath", isNull().orValue(isNotAbsolute().andValue(hasNoTraversal())))
            .and(validate(specification, "specification", isNotNull()))
            .orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEntriesEvent(() -> {
         List<DocumentEntry> deletedDocuments =
               sourceRepository.deleteDocumentsBySpecification(basePath, specification);

         deletedDocuments.forEach(this::deleteCachedDocument);

         checkEvictables();

         return deletedDocuments;
      }, d -> documentDeleted(d, watch));
   }

   @Override
   @SuppressWarnings("unchecked")
   public void close() {
      if (!closed) {
         closed = true;
         checkedRunnable(cacheRepository::close).tryFinally(sourceRepository::close).run();
      }
   }

   @Override
   public ReferencedDocument<? extends DocumentRepository> referencedDocumentId(DocumentUri documentUri) {
      throw new UnsupportedCapabilityException(DOCUMENT_URI);
   }

   @Override
   public RepositoryUri toUri(ExportUriOptions options) {
      throw new UnsupportedCapabilityException(REPOSITORY_URI);
   }

   @Override
   public DocumentUri toUri(DocumentPath documentId, ExportUriOptions options) {
      throw new UnsupportedCapabilityException(DOCUMENT_URI);
   }

   @Override
   public boolean supportsUri(RepositoryUri uri) {
      if (uri instanceof DocumentUri) {
         throw new UnsupportedCapabilityException(DOCUMENT_URI);
      } else {
         throw new UnsupportedCapabilityException(REPOSITORY_URI);
      }
   }

   @Override
   public Optional<Document> findDocumentByUri(DocumentUri documentUri) {
      throw new UnsupportedCapabilityException(DOCUMENT_URI);
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryByUri(DocumentUri documentUri) {
      throw new UnsupportedCapabilityException(DOCUMENT_URI);
   }

   private Optional<DocumentEntry> cachedDocumentEntry(DocumentPath documentId) {
      StopWatch watch = StopWatch.create();

      return watch(watch, () -> cacheRepository.findDocumentEntryById(documentId))
            .filter(documentEntry -> !checkEvictable(documentEntry))
            .map(peek(documentEntry -> {
               updateCacheContext(documentEntry, ACCESS);
               publishEvent(new CacheHit(this, documentId, duration(watch)));
            }))
            .or(peekEmpty(() -> publishEvent(new CacheMissed(this, documentId, duration(watch)))));
   }

   private Optional<Document> cachedDocument(DocumentPath documentId) {
      StopWatch watch = StopWatch.create();

      return watch(watch, () -> cacheRepository.findDocumentById(documentId))
            .filter(document -> !checkEvictable(document))
            .map(peek(document -> {
               updateCacheContext(document, ACCESS);
               publishEvent(new CacheHit(this, documentId, duration(watch)));
            }))
            .or(peekEmpty(() -> publishEvent(new CacheMissed(this, documentId, duration(watch)))));
   }

   private Optional<DocumentEntry> cacheDocumentEntry(DocumentPath documentId) {
      StopWatch watch = StopWatch.create();

      return watch(watch, () -> sourceRepository.findDocumentById(documentId))
            .map(peek(__ -> publishEvent(new CacheLoaded(this, documentId, duration(watch)))))
            .map(this::cacheDocumentEntry);
   }

   private Optional<Document> cacheDocument(DocumentPath documentId) {
      StopWatch watch = StopWatch.create();

      return watch(watch, () -> sourceRepository.findDocumentById(documentId))
            .map(peek(__ -> publishEvent(new CacheLoaded(this, documentId, duration(watch)))))
            .map(this::cacheDocument);
   }

   /**
    * @return cached document entry
    *
    * @implNote Implementation should be tolerant against cache repository failures.
    */
   private DocumentEntry cacheDocumentEntry(Document document) {
      StopWatch watch = StopWatch.create();

      return watch(watch, () -> cacheRepository.saveDocument(document, true)).map(peek(e -> {
         updateCacheContext(e, SAVE);
         publishEvent(new CacheSaved(this, document.documentId(), duration(watch)));
      })).orElseThrow(() -> new DocumentAccessException("Document not saved to cache"));
   }

   /**
    * @return cached document
    *
    * @implNote Implementation should be tolerant against cache repository failures.
    */
   private Document cacheDocument(Document document) {
      StopWatch watch = StopWatch.create();

      return watch(watch, () -> cacheRepository.saveAndReturnDocument(document, true)).map(peek(d -> {
         updateCacheContext(d, SAVE);
         publishEvent(new CacheSaved(this, document.documentId(), duration(watch)));
      })).orElseThrow(() -> new DocumentAccessException("Document not saved to cache"));
   }

   /**
    * @implNote This call do not throw an error if document has not been effectively deleted from cache
    *       because the result is the same for the cache (it means the document is already deleted). For the
    *       same reason, cache context is always updated.
    */
   private DocumentEntry deleteCachedDocument(DocumentEntry documentEntry) {
      ignore(cacheRepository.deleteDocumentById(documentEntry.documentId()));
      updateCacheContext(documentEntry, DELETE);

      return documentEntry;
   }

   /**
    * Evicts specified document from cache if it is evictable by the time of the call. Both eviction strategy
    * and cached document are updated by the operation.
    *
    * @param document document to evict
    *
    * @return {@code true} if document has been evicted
    */
   private boolean checkEvictable(Document document) {
      return checkEvictable(document.documentEntry());
   }

   /**
    * Evicts specified document from cache if it is evictable by the time of the call. Both eviction strategy
    * and cached document are updated by the operation.
    *
    * @param documentEntry document to evict
    *
    * @return {@code true} if document has been evicted
    */
   private boolean checkEvictable(DocumentEntry documentEntry) {
      return cacheEvictionStrategy.evictable(documentEntry.documentId(), evictableCallback());
   }

   /**
    * Evicts any document from cache if it is evictable by the time of the call. Both eviction strategy and
    * cached documents are updated by the operation.
    */
   private void checkEvictables() {
      cacheEvictionStrategy.evictables(evictableCallback());
   }

   private void updateCacheContext(Document document, ContextOperation operation) {
      updateCacheContext(document.documentEntry(), operation);
   }

   private void updateCacheContext(DocumentEntry documentEntry, ContextOperation operation) {
      cacheEvictionStrategy.updateCacheContext(documentEntry, operation);
   }

   /**
    * Callback to use when evicting a document from cache strategy.
    *
    * @implNote Callback must be idempotent. {@link CacheEvicted} is published only if deletion is
    *       effective, because of composite strategies that can call the callback multiple times for the same
    *       document, so that event count is correct.
    */
   private Consumer<DocumentPath> evictableCallback() {
      return documentId -> {
         StopWatch watch = StopWatch.create();

         watch(watch, () -> cacheRepository.deleteDocumentById(documentId)).ifPresent(__ -> {
            publishEvent(new CacheEvicted(this, documentId, duration(watch)));
         });
      };
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", CacheDocumentRepository.class.getSimpleName() + "[", "]")
            .add("sourceRepository=" + sourceRepository)
            .add("cacheRepository=" + cacheRepository)
            .add("cacheEvictionStrategy=" + cacheEvictionStrategy)
            .add("refreshSourceMetadataMaximumDuration=" + refreshSourceMetadataMaximumDuration)
            .toString();
   }
}
