/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static com.tinubu.commons.ddd2.domain.type.support.DomainObjectSupport.checkInvariants;
import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObject;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.satisfiesValue;
import static com.tinubu.commons.ddd2.invariant.Validate.Check.notBlank;
import static com.tinubu.commons.ddd2.invariant.Validate.Check.notNull;
import static com.tinubu.commons.ddd2.invariant.Validate.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.property;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.hasNoNullElements;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotAbsolute;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.PathUtils.addRoot;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.util.Optional;

import org.apache.commons.io.FilenameUtils;

import com.tinubu.commons.ddd2.domain.ids.PathId;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.Invariant;
import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ddd2.invariant.Invariants;
import com.tinubu.commons.ddd2.invariant.ParameterValue;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.invariant.formatter.FastStringFormat;
import com.tinubu.commons.ddd2.invariant.rules.ArrayRules;
import com.tinubu.commons.lang.util.PathUtils;

/**
 * Uniquely identify a document in the repository.
 * Document identification path is always relative.
 * <ul>
 *    <li>value cannot have more than {@value #MAX_VALUE_LENGTH} bytes total</li>
 *    <li>each value path element, including last element, cannot have more than {@value #MAX_PATH_ELEMENT_LENGTH} bytes</li>
 * </ul>
 */
// FIXME pull up some helpers to PathId ?
public class DocumentPath extends PathId {

   protected static final String URN_ID_TYPE = "path";

   protected DocumentPath(Path value, boolean newObject) {
      super(value, true, newObject);
   }

   protected DocumentPath(Path value) {
      super(value, true);
   }

   /**
    * Creates a new {@link DocumentPath} from a {@link Path}. If specified path is absolute, root is removed.
    *
    * @param value path, relative or absolute
    *
    * @return new document path
    */
   public static DocumentPath of(Path value) {
      return checkInvariants(new DocumentPath(nullable(value).map(PathUtils::removeRoot).orElse(null)));
   }

   /**
    * Creates a new {@link DocumentPath} from path names. If specified path is absolute, root is removed.
    *
    * @param first first path component, relative or absolute
    * @param more next path components
    *
    * @return new document path
    */
   public static DocumentPath of(String first, String... more) {
      validate(first, "first", isNotNull())
            .and(validate(more, "more", ArrayRules.list(hasNoNullElements())))
            .orThrow();

      return of(Path.of(first, more));
   }

   /**
    * Creates a new {@link DocumentPath} from a URN. If specified path is absolute, root is removed.
    *
    * @param urn URN
    *
    * @return new document path
    */
   public static DocumentPath of(URI urn) {
      return of(nullable(urn).map(u -> Path.of(documentUrnValue(u, URN_ID_TYPE))).orElse(null));
   }

   /**
    * Creates a new {@link DocumentPath}, with new object flag set to {@code true}, from a {@link Path}. If
    * specified path is absolute, root is removed.
    *
    * @param value path, relative or absolute
    *
    * @return new document path
    */
   public static DocumentPath ofNewObject(Path value) {
      return checkInvariants(new DocumentPath(nullable(value).map(PathUtils::removeRoot).orElse(null), true));
   }

   /**
    * Creates a new {@link DocumentPath}, with new object flag set to {@code true}, from path names. If
    * specified path is absolute, root is removed.
    *
    * @param first first path component, relative or absolute
    * @param more next path components
    *
    * @return new document path
    */
   public static DocumentPath ofNewObject(String first, String... more) {
      validate(first, "first", isNotNull())
            .and(validate(more, "more", ArrayRules.list(hasNoNullElements())))
            .orThrow();

      return ofNewObject(Path.of(first, more));
   }

   /**
    * Creates a new {@link DocumentPath}, with new object flag set to {@code true}, from a URN. If specified
    * path is absolute, root is removed.
    *
    * @param urn URN
    *
    * @return new document path
    */
   public static DocumentPath ofNewObject(URI urn) {
      return ofNewObject(nullable(urn).map(u -> Path.of(documentUrnValue(u, URN_ID_TYPE))).orElse(null));
   }

   @Override
   @SuppressWarnings("unchecked")
   protected Fields<? extends DocumentPath> defineDomainFields() {
      return Fields
            .<DocumentPath>builder()
            .superFields((Fields<DocumentPath>) super.defineDomainFields(), "relativePath")
            .build();
   }

   protected DocumentPath recreate(Path value, boolean newObject) {
      return checkInvariants(new DocumentPath(value, newObject));
   }

   /**
    * Updates document path value.
    *
    * @param value value to update
    *
    * @return new document path with updated path value
    */
   @Override
   public DocumentPath value(Path value) {
      return recreate(value, newObject);
   }

   /**
    * Returns {@link #value()} as an absolute path.
    *
    * @return absolute path value
    */
   public Path absoluteValue() {
      return addRoot(value());
   }

   /**
    * Updates document path value.
    *
    * @param first first path element value to update
    * @param more subsequent path element values to update
    *
    * @return new document path with updated path value
    */
   @Override
   public DocumentPath value(String first, String... more) {
      validate(first, "first", isNotNull())
            .and(validate(more, "more", ArrayRules.list(hasNoNullElements())))
            .orThrow();

      return value(Path.of(first, more));
   }

   /**
    * Updates document path name component (last component). The other components are preserved.
    *
    * @param name path name component to update
    *
    * @return new document path with updated path name component
    *
    * @apiNote Name is case-sensitive
    */
   public DocumentPath name(String name) {
      Path value = nullable(value().getParent()).map(parent -> parent.resolve(name)).orElse(Path.of(name));

      return recreate(value, newObject);
   }

   /**
    * Returns document path name component (last component).
    *
    * @return document path name component
    *
    * @apiNote Name is case-sensitive
    */
   public String name() {
      return value.getFileName().toString();
   }

   /**
    * Returns document path name extension (case-sensitive).
    *
    * @return document path name extension
    *
    * @apiNote Extension is case-sensitive
    */
   public String extension() {
      return FilenameUtils.getExtension(stringValue());
   }

   /**
    * Checks if document path name extension matches specified extension (case-sensitive).
    * Specify {@code ""} extension to match a name without extension, or with an empty extension.
    *
    * @param extension case-sensitive extension to check
    *
    * @return {@code true} if document name has specified extension
    *
    * @see #hasExtensionIgnoreCase(String)
    */
   public boolean hasExtension(String extension) {
      notNull(extension, "extension");

      return extension().equals(extension);
   }

   /**
    * Checks if document path name extension matches specified extension (case-insensitive).
    * Specify {@code ""} extension to match a name without extension, or with an empty extension.
    *
    * @param extension case-insensitive extension to check
    *
    * @return {@code true} if document name has specified extension
    *
    * @see #hasExtension(String)
    */
   public boolean hasExtensionIgnoreCase(String extension) {
      notNull(extension, "extension");

      return extension().equalsIgnoreCase(extension);
   }

   /**
    * Returns whether this document path name is <em>hidden</em>, i.e.: name starts with a {@code .}.
    *
    * @return whether this document path name is hidden
    */
   public boolean hidden() {
      return name().startsWith(".");
   }

   /**
    * Returns document parent path.
    *
    * @return document parent path or {@link Optional#empty()} if document has no parent
    */
   public Optional<Path> parent() {
      return nullable(value().getParent());
   }

   /**
    * Updates document path value removing sub-path prefix, only if it matches.
    * Current path is returned if sub-path is empty ({@code ""}).
    *
    * @param subPath sub-path prefix to remove
    *
    * @return new document path with updated path value, or {@link Optional#empty} if sub-pah prefix does not
    *       match
    */
   public Optional<DocumentPath> subPath(Path subPath) {
      Path normalizedSubPath = Check.validate(normalize(subPath),
                                              "subPath",
                                              isNotAbsolute()
                                                    .andValue(hasNoTraversal())
                                                    .andValue(isNotEqualTo(ParameterValue.value(value))));

      if (normalizedSubPath.toString().isEmpty()) {
         return optional(this);
      } else if (value.startsWith(normalizedSubPath)) {
         return optional(value(normalizedSubPath.relativize(value)));
      } else {
         return optional();
      }
   }

   @Override
   public Invariants domainInvariants() {
      return Invariants.of(Invariant.of(() -> this, property(d -> d.value, "value", isNotNull())));
   }

   /**
    * Creates a URN from this document path.
    * URN format is {@code urn:document:path:<relative-path>}.
    *
    * @return URN of this document path
    */
   @Override
   public URI urnValue() {
      return documentUrn(URN_ID_TYPE, stringValue());
   }

   protected URI documentUrn(String idType, String idValue) {
      notBlank(idType, "idType");
      notBlank(idValue, "idValue");

      try {
         return new URI("urn", "document:" + idType + ":" + idValue, null);
      } catch (URISyntaxException e) {
         throw new IllegalStateException(e);
      }
   }

   /**
    * Parses URN with format {@code urn:document:<id-type>:<id-value>}.
    *
    * @param urn URN
    * @param idType id type to check
    *
    * @return id URN value part
    *
    * @throws InvariantValidationException if URN syntax is invalid
    */
   protected static String documentUrnValue(URI urn, String idType) {
      validate(urn, "urn", isValidDocumentUrnSyntax(idType))
            .and(validate(idType, "idType", isNotBlank()))
            .orThrow();

      return urn.getSchemeSpecificPart().split(":", 3)[2];
   }

   protected static InvariantRule<URI> isValidDocumentUrnSyntax(String idType) {
      return satisfiesValue((URI urn) -> {
                               if (urn.isAbsolute() && urn.getScheme().equals("urn") && urn.isOpaque()) {
                                  String[] urnParts = urn.getSchemeSpecificPart().split(":", 3);

                                  if (urnParts.length == 3 && urnParts[0].equals("document") && urnParts[2].length() > 0) {
                                     return idType == null || urnParts[1].equals(idType);
                                  }
                               }
                               return false;
                            },
                            FastStringFormat.of("'",
                                                validatingObject(),
                                                "' must be valid URN : 'urn:document:",
                                                idType != null ? idType : "<id-type>",
                                                ":<id-value>'")).ruleContext(
            "DocumentPath.isValidDocumentUrnSyntax");
   }

}
