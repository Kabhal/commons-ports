/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.git;

/**
 * Merge result status.
 */
public enum MergeStatus {
   FAST_FORWARD(true),
   FAST_FORWARD_SQUASHED(true),
   ALREADY_UP_TO_DATE(true),
   FAILED,
   MERGED(true),
   MERGED_SQUASHED(true),
   MERGED_SQUASHED_NOT_COMMITTED(true),
   CONFLICTING,
   ABORTED,
   MERGED_NOT_COMMITTED(true),
   NOT_SUPPORTED,
   /**
    * Status representing a checkout conflict, the pre-scan for the trees already failed for certain
    * files (i.e. local modifications prevent checkout of files).
    */
   CHECKOUT_CONFLICT;

   private final boolean successful;

   MergeStatus(boolean successful) {
      this.successful = successful;
   }

   MergeStatus() {
      this(false);
   }

   public boolean successful() {
      return successful;
   }
}
