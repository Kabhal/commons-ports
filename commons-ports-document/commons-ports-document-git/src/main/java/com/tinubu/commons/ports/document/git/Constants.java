/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.git;

import static com.tinubu.commons.lang.util.PathUtils.emptyPath;

import java.nio.file.Path;
import java.time.Duration;

import com.tinubu.commons.ports.document.git.GitDocumentConfig.PullStrategy.PullMode;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.PushStrategy.PushMode;

public final class Constants {

   private Constants() { }

   public static final Path DEFAULT_BASE_PATH = emptyPath();
   public static final String DEFAULT_BRANCH = "main";
   public static final String DEFAULT_REMOTE = "origin";
   public static final boolean DEFAULT_INIT_IF_MISSING = true;
   public static final boolean DEFAULT_CLONE_IF_MISSING = true;
   public static final boolean DEFAULT_FORCE_LOCAL_URI = false;
   public static final PullMode DEFAULT_PULL_MODE = PullMode.NEVER;
   public static final FastForward DEFAULT_PULL_FAST_FORWARD = FastForward.FAST_FORWARD;
   public static final boolean DEFAULT_PULL_REBASE = false;
   public static final PushMode DEFAULT_COMMIT_MODE = PushMode.NEVER;
   public static final PushMode DEFAULT_PUSH_MODE = PushMode.NEVER;
   public static final Duration DEFAULT_CONNECT_TIMEOUT = Duration.ofSeconds(10);
   public static final boolean DEFAULT_DISABLE_SSL_VERIFY = false;
   public static final boolean DEFAULT_ALLOW_UNKNOWN_SSH_HOSTS = false;

}
