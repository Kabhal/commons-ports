/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.tinubu.commons.ports.document.git;

import static com.tinubu.commons.ddd2.invariant.Validate.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.as;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.ClassRules.isInstanceOf;
import static com.tinubu.commons.ddd2.invariant.rules.OptionalRules.optionalValue;
import static com.tinubu.commons.ddd2.invariant.rules.domain.ids.UriRules.isNotEmptyHierarchicalServer;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.NullableUtils.nullableInstanceOf;
import static com.tinubu.commons.lang.util.OptionalUtils.instanceOf;

import java.net.URI;
import java.util.Optional;
import java.util.function.Function;

import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.Invariant;
import com.tinubu.commons.ddd2.invariant.Invariants;
import com.tinubu.commons.ddd2.invariant.ParameterValue;
import com.tinubu.commons.ddd2.invariant.rules.ClassRules;
import com.tinubu.commons.ddd2.invariant.rules.domain.ids.UriRules;
import com.tinubu.commons.ddd2.uri.AbstractComponentUri;
import com.tinubu.commons.ddd2.uri.ComponentUri;
import com.tinubu.commons.ddd2.uri.DefaultUriRestrictions;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ddd2.uri.UriRestrictions;
import com.tinubu.commons.ddd2.uri.parts.SimpleAuthority;
import com.tinubu.commons.ddd2.uri.parts.SimpleFragment;
import com.tinubu.commons.ddd2.uri.parts.SimplePath;
import com.tinubu.commons.ddd2.uri.parts.SimplePort;
import com.tinubu.commons.ddd2.uri.parts.SimpleQuery;
import com.tinubu.commons.ddd2.uri.parts.SimpleScheme;
import com.tinubu.commons.ddd2.uri.parts.SimpleSchemeSpecific;
import com.tinubu.commons.ddd2.uri.parts.UsernamePasswordUserInfo;

/**
 * Git {@link Uri}.
 * <p>
 * Supported URI formats :
 * <ul>
 *    <li>HTTPS : {@code git://[<user>[:<password>]@]<host>[:<port>]</path>[?<query>][#<fragment>]}</li>
 * </ul>
 * {@code path} can be empty.
 * <p>
 * However, supported format depends on {@link DefaultUriRestrictions} restrictions. By default,
 * <ul>
 *    <li>{@link DefaultUriRestrictions#portRestriction() portRestriction} : no port restriction</li>
 *    <li>{@link DefaultUriRestrictions#portMapper() portMapper} : default port = {@value DEFAULT_GIT_PORT}</li>
 *    <li>{@link DefaultUriRestrictions#query() query} : query is supported</li>
 *    <li>{@link DefaultUriRestrictions#fragment() fragment} : fragment is supported</li>
 * </ul>
 */
public class GitUri extends AbstractComponentUri<GitUri> {

   protected static final String GIT_SCHEME = "git";
   protected static final int DEFAULT_GIT_PORT = 9418;

   protected GitUri(URI uri,
                    UriRestrictions restrictions,
                    Scheme scheme,
                    SchemeSpecific schemeSpecific,
                    Authority authority,
                    Path path,
                    Query query,
                    Fragment fragment,
                    boolean newObject) {
      super(uri,
            gitRestrictions(restrictions),
            scheme,
            schemeSpecific,
            gitUserInfo(authority),
            path,
            query,
            fragment,
            newObject);
   }

   protected GitUri(UriRestrictions restrictions,
                    Scheme scheme,
                    SchemeSpecific schemeSpecific,
                    Authority authority,
                    Path path,
                    Query query,
                    Fragment fragment) {
      super(gitRestrictions(restrictions),
            scheme,
            schemeSpecific,
            gitUserInfo(authority),
            path,
            query,
            fragment);
   }

   protected GitUri(URI uri, UriRestrictions restrictions) {
      this(uri,
           restrictions,
           SimpleScheme.of(uri).orElse(null),
           SimpleSchemeSpecific.of(uri).orElse(null),
           SimpleAuthority.of(uri).orElse(null),
           SimplePath.of(uri).orElse(null),
           SimpleQuery.of(uri).orElse(null),
           SimpleFragment.of(uri).orElse(null),
           false);
   }

   protected GitUri(Uri uri, UriRestrictions restrictions) {
      super(gitUserInfo(nullableInstanceOf(uri, ComponentUri.class, ComponentUri::ofUri)),
            gitRestrictions(restrictions));
   }

   @Override
   @SuppressWarnings("unchecked")
   protected Fields<? extends GitUri> defineDomainFields() {
      return Fields
            .<GitUri>builder()
            .superFields((Fields<? super GitUri>) super.defineDomainFields())
            .field("authority",
                   v -> v.authority,
                   isNotNull().andValue(ClassRules.instanceOf(ServerAuthority.class,
                                                              as(ServerAuthority::userInfo,
                                                                 optionalValue(isInstanceOf(ParameterValue.value(
                                                                       UsernamePasswordUserInfo.class)))))))
            .build();
   }

   @Override
   public Invariants domainInvariants() {
      var uriCompatibility = Invariant
            .of(() -> this,
                UriRules.isAbsolute(ParameterValue.value(GIT_SCHEME)),
                UriRules.isHierarchicalServer().andValue(isNotEmptyHierarchicalServer()))
            .groups(COMPATIBILITY_GROUP);

      return super.domainInvariants().withInvariants(uriCompatibility);
   }

   public static GitUri ofUri(URI uri, UriRestrictions restrictions) {
      return buildUri(() -> new GitUri(uri, restrictions));
   }

   public static GitUri ofUri(URI uri) {
      return ofUri(uri, DefaultUriRestrictions.ofDefault());
   }

   public static GitUri ofUri(Uri uri, UriRestrictions restrictions) {
      return buildUri(() -> new GitUri(uri, restrictions));
   }

   public static GitUri ofUri(Uri uri) {
      return ofUri(uri, DefaultUriRestrictions.ofDefault());
   }

   public static GitUri ofUri(String uri, UriRestrictions restrictions) {
      return ofUri(Uri.uri(uri), restrictions);
   }

   public static GitUri ofUri(String uri) {
      return ofUri(uri, DefaultUriRestrictions.ofDefault());
   }

   public static GitUri ofUrn(URI urn, UriRestrictions restrictions) {
      return ofUri(parseUrn(urn), restrictions);
   }

   public static GitUri ofUrn(URI urn) {
      return ofUrn(urn, DefaultUriRestrictions.ofDefault());
   }

   public static GitUri hierarchical(ServerAuthority authority, Path path, Query query, Fragment fragment) {
      validate(authority, "authority", isNotNull()).and(validate(path, "path", isNotNull())).orThrow();

      return buildUri(() -> new GitUri(DefaultUriRestrictions.automatic(query, fragment),
                                       SimpleScheme.of(GIT_SCHEME),
                                       null,
                                       authority,
                                       path,
                                       query,
                                       fragment));
   }

   public static GitUri hierarchical(ServerAuthority authority, Path path, Query query) {
      return hierarchical(authority, path, query, null);
   }

   public static GitUri hierarchical(ServerAuthority authority, Path path) {
      return hierarchical(authority, path, null, null);
   }

   public static GitUri hierarchical(ServerAuthority authority, Path path, Fragment fragment) {
      return hierarchical(authority, path, null, fragment);
   }

   @Override
   protected GitUri recreate(URI uri,
                             UriRestrictions restrictions,
                             Scheme scheme,
                             SchemeSpecific schemeSpecific,
                             Authority authority,
                             Path path,
                             Query query,
                             Fragment fragment,
                             boolean newObject) {
      return buildUri(() -> new GitUri(uri,
                                       restrictions,
                                       scheme,
                                       schemeSpecific,
                                       authority,
                                       path,
                                       query,
                                       fragment,
                                       newObject));
   }

   @Override
   public GitUri relativize(Uri uri) {
      throw new UnsupportedOperationException();
   }

   @Override
   public GitUri relativize(URI uri) {
      throw new UnsupportedOperationException();
   }

   public Optional<String> username() {
      return component()
            .serverAuthority()
            .flatMap(ServerAuthority::userInfo)
            .map(UsernamePasswordUserInfo.class::cast)
            .map(ui -> ui.username(false));
   }

   public Optional<String> password() {
      return component()
            .serverAuthority()
            .flatMap(ServerAuthority::userInfo)
            .map(UsernamePasswordUserInfo.class::cast)
            .flatMap(ui -> ui.password(false));
   }

   public boolean defaultPort() {
      return portOrDefault() == DEFAULT_GIT_PORT;
   }

   public int portOrDefault() {
      return port().orElse(DEFAULT_GIT_PORT);
   }

   @Override
   public Component component() {
      return new Component();
   }

   protected static UriRestrictions gitRestrictions(UriRestrictions restrictions) {
      return nullableInstanceOf(restrictions,
                                DefaultUriRestrictions::ofDefault,
                                DefaultUriRestrictions.class,
                                __ -> DefaultUriRestrictions.ofDefault()).defaultPortMapper(
            defaultGitPortMapper());

   }

   protected static ComponentUri gitUserInfo(ComponentUri uri) {
      return nullable(uri).map(u -> u.component().conditionalUserInfo(GitUri::gitUserInfo)).orElse(null);
   }

   /**
    * Transforms user-info to {@link UsernamePasswordUserInfo} if needed.
    */
   protected static Authority gitUserInfo(Authority authority) {
      return nullable(authority)
            .map(a -> instanceOf(a, ServerAuthority.class)
                  .map(sa -> (Authority) sa.userInfo(GitUri::gitUserInfo))
                  .orElse(a))
            .orElse(null);
   }

   /**
    * Transforms user-info to {@link UsernamePasswordUserInfo} if needed.
    */
   protected static UserInfo gitUserInfo(UserInfo ui) {
      return nullable(ui, UsernamePasswordUserInfo::of);
   }

   /**
    * Returns default port for given scheme.
    *
    * @return default port for given scheme, or {@code null} if scheme is unsupported
    */
   protected static Function<String, Integer> defaultGitPortMapper() {
      return __ -> DEFAULT_GIT_PORT;
   }

   public class Component extends AbstractComponent {

      public Port portOrDefault() {
         return super.port().orElseGet(() -> SimplePort.of(GitUri.this.portOrDefault()));
      }

   }

}
