/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.git;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.property;
import static com.tinubu.commons.ddd2.invariant.rules.ClassRules.isInstanceOf;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.hasNoBlankElements;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.hasNoNullElements;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.size;
import static com.tinubu.commons.ddd2.invariant.rules.NumberRules.isStrictlyPositive;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotAbsolute;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.lang.util.CheckedFunction.checkedFunction;
import static com.tinubu.commons.lang.util.CheckedRunnable.noop;
import static com.tinubu.commons.lang.util.CollectionUtils.collectionConcat;
import static com.tinubu.commons.lang.util.CollectionUtils.collectionIntersection;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.instanceOf;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.OptionalUtils.peek;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.util.StreamUtils.streamConcat;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.DOCUMENT_URI;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.ITERABLE;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.OPEN;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.OPEN_APPEND;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.QUERYABLE;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.READABLE;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.REPOSITORY_URI;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.WRITABLE;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.allMetadataCapabilities;
import static com.tinubu.commons.ports.document.git.gitops.GitUtils.branchExists;
import static com.tinubu.commons.ports.document.git.gitops.GitUtils.checkPushResults;
import static com.tinubu.commons.ports.document.git.gitops.GitUtils.fileSystemRepository;
import static com.tinubu.commons.ports.document.git.gitops.GitUtils.skipPromptCredentialsProvider;
import static java.util.function.Predicate.not;
import static java.util.function.UnaryOperator.identity;
import static java.util.stream.Collectors.joining;
import static org.eclipse.jgit.api.MergeCommand.FastForwardMode.FF;
import static org.eclipse.jgit.api.MergeCommand.FastForwardMode.FF_ONLY;
import static org.eclipse.jgit.api.MergeCommand.FastForwardMode.NO_FF;
import static org.eclipse.jgit.api.RemoteSetUrlCommand.UriType.FETCH;
import static org.eclipse.jgit.lib.CommitConfig.CleanupMode.DEFAULT;
import static org.eclipse.jgit.merge.ContentMergeStrategy.CONFLICT;
import static org.eclipse.jgit.merge.ContentMergeStrategy.OURS;
import static org.eclipse.jgit.merge.ContentMergeStrategy.THEIRS;
import static org.eclipse.jgit.merge.MergeStrategy.RECURSIVE;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.StringJoiner;
import java.util.function.Consumer;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

import org.apache.commons.lang3.time.StopWatch;
import org.eclipse.jgit.api.AddCommand;
import org.eclipse.jgit.api.CheckoutCommand;
import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.CommitCommand;
import org.eclipse.jgit.api.FetchCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.MergeCommand;
import org.eclipse.jgit.api.MergeCommand.FastForwardMode;
import org.eclipse.jgit.api.MergeResult;
import org.eclipse.jgit.api.PullCommand;
import org.eclipse.jgit.api.PushCommand;
import org.eclipse.jgit.api.RemoteSetUrlCommand.UriType;
import org.eclipse.jgit.api.RmCommand;
import org.eclipse.jgit.api.Status;
import org.eclipse.jgit.api.TransportCommand;
import org.eclipse.jgit.api.errors.CheckoutConflictException;
import org.eclipse.jgit.api.errors.EmptyCommitException;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidConfigurationException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.JGitInternalException;
import org.eclipse.jgit.api.errors.NoHeadException;
import org.eclipse.jgit.api.errors.RefNotAdvertisedException;
import org.eclipse.jgit.api.errors.RefNotFoundException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.api.errors.WrongRepositoryStateException;
import org.eclipse.jgit.errors.LockFailedException;
import org.eclipse.jgit.errors.MissingObjectException;
import org.eclipse.jgit.lib.ConfigConstants;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectLoader;
import org.eclipse.jgit.lib.ProgressMonitor;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.transport.ChainingCredentialsProvider;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.NetRCCredentialsProvider;
import org.eclipse.jgit.transport.RefSpec;
import org.eclipse.jgit.transport.TagOpt;
import org.eclipse.jgit.transport.URIish;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinubu.commons.ddd2.domain.event.DomainEvent;
import com.tinubu.commons.ddd2.domain.event.DomainEventListener;
import com.tinubu.commons.ddd2.domain.event.RegistrableDomainEventService;
import com.tinubu.commons.ddd2.domain.event.SynchronousDomainEventService;
import com.tinubu.commons.ddd2.domain.specification.Specification;
import com.tinubu.commons.ddd2.invariant.Validate;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.invariant.rules.ArrayRules;
import com.tinubu.commons.ddd2.invariant.rules.CollectionRules;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.lang.util.CheckedFunction;
import com.tinubu.commons.lang.util.Pair;
import com.tinubu.commons.lang.util.PathUtils;
import com.tinubu.commons.lang.validation.CheckReturnValue;
import com.tinubu.commons.ports.document.domain.AbstractDocumentRepository;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentEntry.DocumentEntryBuilder;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata;
import com.tinubu.commons.ports.document.domain.OutputStreamDocumentContent.OutputStreamDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.ReferencedDocument;
import com.tinubu.commons.ports.document.domain.capability.RepositoryCapability;
import com.tinubu.commons.ports.document.domain.uri.DocumentRepositoryUriAdapter;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.ExportUriOptions;
import com.tinubu.commons.ports.document.domain.uri.RepositoryUri;
import com.tinubu.commons.ports.document.fs.FsDocumentRepository;
import com.tinubu.commons.ports.document.fs.FsRepositoryUri;
import com.tinubu.commons.ports.document.fs.storagestrategy.DirectFsStorageStrategy;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.Authentication;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.GitDocumentConfigBuilder;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.PullStrategy.PullMode;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.PushStrategy.PushMode;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.Transport;
import com.tinubu.commons.ports.document.git.exception.CheckoutConflictGitException;
import com.tinubu.commons.ports.document.git.exception.EmptyCommitGitException;
import com.tinubu.commons.ports.document.git.exception.GitException;
import com.tinubu.commons.ports.document.git.exception.InvalidConfigurationGitException;
import com.tinubu.commons.ports.document.git.exception.InvalidRemoteGitException;
import com.tinubu.commons.ports.document.git.exception.IoGitException;
import com.tinubu.commons.ports.document.git.exception.LockFailedGitException;
import com.tinubu.commons.ports.document.git.exception.MergeFailedGitException;
import com.tinubu.commons.ports.document.git.exception.NoHeadGitException;
import com.tinubu.commons.ports.document.git.exception.NotCleanGitException;
import com.tinubu.commons.ports.document.git.exception.PushFailedGitException;
import com.tinubu.commons.ports.document.git.exception.RefNotAdvertisedGitException;
import com.tinubu.commons.ports.document.git.exception.RefNotFoundGitException;
import com.tinubu.commons.ports.document.git.exception.TransportGitException;
import com.tinubu.commons.ports.document.git.exception.UnknownRevisionGitException;
import com.tinubu.commons.ports.document.git.exception.WrongRepositoryStateGitException;
import com.tinubu.commons.ports.document.git.gitops.GitUtils;
import com.tinubu.commons.ports.document.transformer.DocumentTransformerUriAdapter;

/**
 * GIT {@link DocumentRepository} adapter implementation.
 * <p>
 * GIT remote repository is backed by a local document repository that must be a {@link FsDocumentRepository}
 * using {@link DirectFsStorageStrategy}. {@link FsDocumentRepository#storagePath()} always point to GIT
 * repository root for GIT commands, while optional {@link FsDocumentRepository#basePath()} is used to
 * resolve documents in repository, and also when using document-related GIT commands, e.g.:
 * {@link #addDocumentToIndex(DocumentPath...)}. It means that using {@link #subPath(Path, boolean)} does no
 * change GIT repository root, but only the base path for document resolution.
 * <p>
 * If local GIT repository need to be initialized and {@link GitDocumentConfig#initIfMissing()} is set, then
 * the configured {@link GitDocumentConfig#initialBranch()} is created. Moreover, if a work
 * {@link GitDocumentConfig#branch()} is configured, then an initial "empty" commit is created, and
 * configured work branch is created and checkout-ed.
 * <p>
 * If no work {@link GitDocumentConfig#branch()} is configured, the work branch will be the current local
 * repository branch. And remote repository will be cloned from their own default branch. On the contrary,
 * the configured work branch will be checkout-ed at initialization, after the optional
 * {@link GitDocumentConfig#pullStrategy()} is applied, and will fail-fast if branch does not exist.
 *
 * @implSpec Immutable class implementation
 * @apiNote This repository is not thread-safe by design, because of GIT operations side effects
 *       (indexed documents, etc...)
 */
// FIXME ofLocalUri + ofRemoteUri as symmetric options ? + toUri(ExportOption{localUri/remoteUri})
// FIXME override this.remoteUri/branch/remote even when not null by reading from .git/config, once GIT is initialized !!! caution with init/dynamic values :/ ?
public class GitDocumentRepository extends AbstractDocumentRepository<GitDocumentRepository> {

   private static final String HEAD_REVISION = "HEAD";

   private static final Logger log = LoggerFactory.getLogger(GitDocumentRepository.class);

   /** Disable GIT repository initialization for testing purpose. */
   static boolean disableRepositoryInitialization = false;

   private final GitDocumentConfig gitDocumentConfig;
   private final Git git;
   private final FsDocumentRepository localRepository;
   private final boolean closeLocalRepositoryOnClose;
   private final CredentialsProvider credentialsProvider;

   private final DocumentRepositoryUriAdapter uriAdapter;

   private URI remoteUri;
   private boolean closed = false;

   /**
    * Creates a new Git document repository wrapping specified local file-system repository.
    *
    * @param gitDocumentConfig repository configuration
    * @param eventService event service
    */
   public GitDocumentRepository(GitDocumentConfig gitDocumentConfig,
                                RegistrableDomainEventService eventService) {
      super(eventService);

      this.gitDocumentConfig = Check.notNull(gitDocumentConfig, "gitDocumentConfig");
      this.remoteUri = gitDocumentConfig.remoteUri().orElse(null);
      this.credentialsProvider = credentialsProvider(gitDocumentConfig.remoteAuthentication().orElse(null),
                                                     gitDocumentConfig.transport());

      this.closeLocalRepositoryOnClose = gitDocumentConfig.localRepository().isEmpty();
      try {
         this.localRepository = gitDocumentConfig
               .localRepository()
               .or(() -> gitDocumentConfig
                     .localUri()
                     .map(localUri -> FsDocumentRepository.ofUri(RepositoryUri.ofRepositoryUri(localUri))))
               .orElseGet(FsDocumentRepository::ofTemporaryDirectory)
               .subPath(gitDocumentConfig.basePath(), true);
      } catch (Exception e) {
         throw new IllegalStateException(String.format(
               "Can't create FsDocumentRepository from unsupported 'localUri=%s' URI",
               gitDocumentConfig.localUri()), e);
      }

      Check.satisfies(localRepository,
                      "localRepository",
                      property(FsDocumentRepository::storageStrategy,
                               "storageStrategy",
                               isInstanceOf(value(DirectFsStorageStrategy.class))));

      if (!disableRepositoryInitialization) {
         try {

            var initializeRepository = initializeRepository();
            var initializeState = initializeRepository.getLeft();
            this.git = initializeRepository.getRight();

            if (remoteUri == null) {
               remoteUri = remoteGetUrl(git, gitDocumentConfig.remote()).orElse(null);
            }
            if (initializeState == GitInitializationState.INITIALIZED) {
               gitDocumentConfig
                     .branch()
                     .filter(branch -> !gitDocumentConfig.initialBranch().equals(branch))
                     .ifPresent(branch -> {
                        commitIndexedDocuments("Initial commit", false, true);
                        checkout(branch, true);
                     });
            } else if (initializeState == GitInitializationState.EXISTING) {
               gitDocumentConfig.branch().ifPresent(branch -> {
                  checkout(branch, false);
               });
            }
            if (initializeState == GitInitializationState.EXISTING) {
               applyPullStrategy(git, true);
            }

         } catch (Throwable t) {
            closeLocalRepository();
            throw t;
         }
      } else {
         this.git = null;
      }

      this.uriAdapter = new GitUriAdapter();
   }

   public GitDocumentRepository(GitDocumentConfig gitDocumentConfig) {
      this(gitDocumentConfig, new SynchronousDomainEventService());
   }

   /**
    * Creates a new GIT repository from local URI. Local URI can contain {@link FsRepositoryUri} parameters in
    * query.
    *
    * @param localUri GIT local repository URI
    * @param builderConfigurer extra builder configurer to finalize configuration or override URI
    *       parameters
    *
    * @return new GIT repository
    */
   public static GitDocumentRepository ofLocalUri(URI localUri,
                                                  UnaryOperator<GitDocumentConfigBuilder> builderConfigurer) {
      Check.notNull(localUri, "localUri");
      Check.notNull(builderConfigurer, "builderConfigurer");

      return new GitDocumentRepository(new GitDocumentConfigBuilder()
                                             .localUri(localUri)
                                             .chain(builderConfigurer)
                                             .build());
   }

   /**
    * Creates a new GIT repository from existing {@link FsDocumentRepository}.
    * <p>
    * Closing specified {@link FsDocumentRepository} is responsibility of calling code.
    *
    * @param localRepository GIT local repository
    * @param builderConfigurer extra builder configurer to finalize configuration or override URI
    *       parameters
    *
    * @return new GIT repository
    */
   public static GitDocumentRepository ofLocalRepository(FsDocumentRepository localRepository,
                                                         UnaryOperator<GitDocumentConfigBuilder> builderConfigurer) {
      Check.notNull(localRepository, "localRepository");
      Check.notNull(builderConfigurer, "builderConfigurer");

      CheckedFunction<FsDocumentRepository, GitDocumentRepository> repositoryFactory =
            lr -> new GitDocumentRepository(new GitDocumentConfigBuilder()
                                                  .localRepository(lr)
                                                  .chain(builderConfigurer)
                                                  .build());
      return checkedFunction(repositoryFactory).autoCloseOnThrow().apply(localRepository);
   }

   /**
    * Creates a new document repository from URI with parameters.
    *
    * @param repositoryUri repository URI
    * @param builderConfigurer extra builder configurer to finalize configuration or override URI
    *       parameters
    *
    * @return new document repository
    *
    * @throws IncompatibleUriException if specified URI is not compatible with this repository
    * @throws InvalidUriException if specified URI is compatible with this repository but invalid
    */
   public static GitDocumentRepository ofUri(RepositoryUri repositoryUri,
                                             UnaryOperator<GitDocumentConfigBuilder> builderConfigurer) {
      Check.notNull(repositoryUri, "repositoryUri");
      Check.notNull(builderConfigurer, "builderConfigurer");

      return new GitDocumentRepository(GitRepositoryUri
                                             .ofRepositoryUri(repositoryUri)
                                             .toConfig()
                                             .chain(builderConfigurer)
                                             .build());
   }

   /**
    * Creates a new document repository from URI with parameters.
    *
    * @param repositoryUri repository URI
    *
    * @return new document repository
    *
    * @throws IncompatibleUriException if specified URI is not compatible with this repository
    * @throws InvalidUriException if specified URI is compatible with this repository but invalid
    */
   public static GitDocumentRepository ofUri(RepositoryUri repositoryUri) {
      return ofUri(repositoryUri, identity());
   }

   /**
    * Creates a lazily referenced document, in a new document repository, from URI with parameters. URI must
    * be compatible with this repository and identify a document.
    *
    * @param documentUri document URI
    * @param builderConfigurer extra builder configurer to finalize configuration or override URI
    *       parameters
    *
    * @return referenced document in new document repository
    *
    * @throws InvalidUriException if specified URI is compatible with this repository but invalid
    * @throws IncompatibleUriException if specified URI is not compatible with this repository
    */
   public static ReferencedDocument<GitDocumentRepository> referencedDocument(DocumentUri documentUri,
                                                                              UnaryOperator<GitDocumentConfigBuilder> builderConfigurer) {
      Check.notNull(documentUri, "documentUri");
      Check.notNull(builderConfigurer, "builderConfigurer");

      var gitUri = GitDocumentUri.ofDocumentUri(documentUri);
      return ReferencedDocument.lazyLoad(new GitDocumentRepository(gitUri
                                                                         .toConfig()
                                                                         .chain(builderConfigurer)
                                                                         .build()), gitUri.documentId());
   }

   /**
    * Creates a lazily referenced document, in a new document repository, from URI with parameters. URI must
    * be compatible with this repository and identify a document.
    *
    * @param documentUri document URI
    *
    * @return referenced document in new document repository
    *
    * @throws InvalidUriException if specified URI is compatible with this repository but invalid
    * @throws IncompatibleUriException if specified URI is not compatible with this repository
    */
   public static ReferencedDocument<GitDocumentRepository> referencedDocument(DocumentUri documentUri) {
      return referencedDocument(documentUri, identity());
   }

   /**
    * Returns local repository.
    *
    * @return local repository
    */
   public FsDocumentRepository localRepository() {
      return localRepository;
   }

   @Override
   public HashSet<RepositoryCapability> capabilities() {
      HashSet<RepositoryCapability> localRepositoryMetadataCapabilities =
            collectionIntersection(HashSet::new, localRepository.capabilities(), allMetadataCapabilities());

      return collectionConcat(HashSet::new,
                              list(WRITABLE,
                                   READABLE,
                                   ITERABLE,
                                   QUERYABLE,
                                   OPEN,
                                   OPEN_APPEND,
                                   REPOSITORY_URI,
                                   DOCUMENT_URI),
                              localRepositoryMetadataCapabilities);
   }

   @Override
   public boolean sameRepositoryAs(com.tinubu.commons.ddd2.domain.repository.Repository<Document, DocumentPath> documentRepository) {
      validate(documentRepository, "documentRepository", isNotNull()).orThrow();

      return documentRepository instanceof GitDocumentRepository
             && ((GitDocumentRepository) documentRepository).localRepository.sameRepositoryAs(localRepository);
   }

   /**
    * Returns a new GIT document repository with a sub-path of local repository. It means that GIT repository
    * root does not change, and all GIT commands will continue to work from root, but all references to a
    * document inside the repository will be relative to new sub-path. However, it affects some GIT commands
    * referencing some documents as {@link #addDocumentToIndex(DocumentPath...)},
    * {@link #addAllDocumentsToIndex()}, {@link #removeDocumentFromIndex(boolean, DocumentPath...)}. In these
    * cases, referenced documents are relative to sub-path.
    *
    * @implNote a new {@link GitDocumentRepository} is created as part of sub path implementation.
    *       {@code shareContext} is logically forced to true, so that no {@link DocumentRepository#close()}
    *       have to be called on it, moreover it wouldn't be practical as no direct reference to new local
    *       repository is returned.
    */
   @Override
   public GitDocumentRepository subPath(Path subPath, boolean shareContext) {
      validate(subPath, "subPath", isNotAbsolute().andValue(hasNoTraversal())).orThrow();

      return new GitDocumentRepository(GitDocumentConfigBuilder
                                             .from(gitDocumentConfig)
                                             .basePath(gitDocumentConfig.basePath().resolve(subPath))
                                             .localUri(null)
                                             .localRepository(localRepository.subPath(subPath, true))
                                             .build(), eventService);
   }

   @Override
   @CheckReturnValue
   public Optional<Document> openDocument(DocumentPath documentId,
                                          boolean overwrite,
                                          boolean append,
                                          OpenDocumentMetadata metadata) {
      applyPullStrategy(false);

      return localRepository
            .openDocument(documentId, overwrite, append, metadata)
            .map(document -> finalizeOpenDocument(document, __ -> {
               if (gitDocumentConfig.pushStrategy().commitMode() == PushMode.ALWAYS) {
                  addDocumentToIndex(document.documentId());
               }
               applyPushStrategy(String.format("Save '%s' document (overwrite=%s, append=%s)",
                                               document.documentId().stringValue(),
                                               overwrite,
                                               append), false);
            }));
   }

   @Override
   public Optional<Document> findDocumentById(DocumentPath documentId) {
      applyPullStrategy(false);

      return localRepository.findDocumentById(documentId);
   }

   /**
    * Finds GIT document by id from any GIT branch/commit/... using specified GIT revision string.
    *
    * @param documentId document id
    * @param revision GIT revision string
    *
    * @return found document or {@link Optional#empty()} if revision string not found, or document not found
    *
    * @throws UnknownRevisionGitException if specified revision can't be resolved
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   public Optional<Document> findDocumentById(String revision, DocumentPath documentId) {
      applyPullStrategy(false);

      try {
         return loadDocument(git, revision, documentId);
      } catch (IoGitException e) {
         throw new DocumentAccessException(e.getMessage(), e);
      }
   }

   /**
    * Finds GIT document entry by id from any GIT branch/commit/... using specified GIT revision string. This
    * enables to only retrieve metadata and not document content.
    * If document exists, a document metadata is always returned with available data.
    *
    * @param documentId document id
    * @param revision GIT revision string
    *
    * @return found document or {@link Optional#empty()} if revision string not found, or document not found
    *
    * @throws UnknownRevisionGitException if specified revision can't be resolved
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   public Optional<DocumentEntry> findDocumentEntryById(String revision, DocumentPath documentId) {
      applyPullStrategy(false);

      try {
         return loadDocumentEntry(git, revision, documentId);
      } catch (IoGitException e) {
         throw new DocumentAccessException(e.getMessage(), e);
      }
   }

   @Override
   public Stream<Document> findDocumentsBySpecification(Path basePath,
                                                        Specification<DocumentEntry> specification) {
      applyPullStrategy(false);

      return localRepository.findDocumentsBySpecification(basePath, specification);
   }

   @Override
   public Stream<Document> findDocumentsBySpecification(Specification<DocumentEntry> specification) {
      applyPullStrategy(false);

      return localRepository.findDocumentsBySpecification(specification);
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryById(DocumentPath documentId) {
      applyPullStrategy(false);

      return localRepository.findDocumentEntryById(documentId);
   }

   @Override
   public Stream<DocumentEntry> findDocumentEntriesBySpecification(Path basePath,
                                                                   Specification<DocumentEntry> specification) {
      applyPullStrategy(false);

      return localRepository.findDocumentEntriesBySpecification(basePath, specification);
   }

   @Override
   public Stream<DocumentEntry> findDocumentEntriesBySpecification(Specification<DocumentEntry> specification) {
      applyPullStrategy(false);

      return localRepository.findDocumentEntriesBySpecification(specification);
   }

   @Override
   @CheckReturnValue
   public Optional<DocumentEntry> saveDocument(Document document, boolean overwrite) {
      applyPullStrategy(false);

      return peek(localRepository.saveDocument(document, overwrite), documentEntry -> {
         if (gitDocumentConfig.pushStrategy().commitMode() == PushMode.ALWAYS) {
            addDocumentToIndex(documentEntry.documentId());
         }
         applyPushStrategy(String.format("Save '%s' document (overwrite=%s)",
                                         document.documentId().stringValue(),
                                         overwrite), false);
      });
   }

   @Override
   @CheckReturnValue
   public Optional<Document> saveAndReturnDocument(Document document, boolean overwrite) {
      applyPullStrategy(false);

      return peek(localRepository.saveAndReturnDocument(document, overwrite), savedDocument -> {
         if (gitDocumentConfig.pushStrategy().commitMode() == PushMode.ALWAYS) {
            addDocumentToIndex(savedDocument.documentId());
         }
         applyPushStrategy(String.format("Save '%s' document (overwrite=%s)",
                                         savedDocument.documentId().stringValue(),
                                         overwrite), false);
      });
   }

   @Override
   @CheckReturnValue
   public Optional<DocumentEntry> deleteDocumentById(DocumentPath documentId) {
      applyPullStrategy(false);

      return peek(localRepository.deleteDocumentById(documentId), documentEntry -> {
         if (gitDocumentConfig.pushStrategy().commitMode() == PushMode.ALWAYS) {
            removeDocumentFromIndex(true, documentEntry.documentId());
         }
         applyPushStrategy(String.format("Delete '%s' document", documentEntry.documentId().stringValue()),
                           false);
      });
   }

   @Override
   @CheckReturnValue
   public List<DocumentEntry> deleteDocumentsBySpecification(Path basePath,
                                                             Specification<DocumentEntry> specification) {
      applyPullStrategy(false);

      List<DocumentEntry> deletedDocuments =
            localRepository.deleteDocumentsBySpecification(basePath, specification);

      if (gitDocumentConfig.pushStrategy().commitMode() == PushMode.ALWAYS) {
         deletedDocuments.forEach(documentEntry -> removeDocumentFromIndex(true, documentEntry.documentId()));
      }

      applyPushStrategy(String.format("Delete specified documents from '%s' base path", basePath), false);

      return deletedDocuments;
   }

   @Override
   @SuppressWarnings("unchecked")
   public void close() {
      if (!closed) {
         closed = true;

         noop().tryFinally(() -> {
            if (git != null) {
               try {
                  if (gitDocumentConfig.pushStrategy().commitMode() == PushMode.ON_CLOSE) {
                     addAllDocumentsToIndex();
                  }
                  applyPushStrategy(onCloseCommitMessage(), true);
               } finally {
                  git.getRepository().close();
                  git.close();
               }
            }
         }, () -> {
            if (credentialsProvider instanceof UsernamePasswordCredentialsProvider) {
               ((UsernamePasswordCredentialsProvider) credentialsProvider).clear();
            }
         }, this::closeLocalRepository).run();
      }
   }

   private void closeLocalRepository() {
      if (closeLocalRepositoryOnClose) {
         localRepository.close();
      }
   }

   @Override
   public <E extends DomainEvent> void registerEventListener(DomainEventListener<? super E> eventListener,
                                                             Class<? extends E>... eventClasses) {
      localRepository.registerEventListener(eventListener, eventClasses);
   }

   @Override
   public void unregisterEventListeners() {
      localRepository.unregisterEventListeners();
   }

   @Override
   public ReferencedDocument<? extends DocumentRepository> referencedDocumentId(DocumentUri documentUri) {
      return uriAdapter.referencedDocumentId(documentUri);
   }

   @Override
   public boolean supportsUri(RepositoryUri uri) {
      return uriAdapter.supportsUri(uri);
   }

   @Override
   public RepositoryUri toUri(ExportUriOptions options) {
      return uriAdapter.toUri(options);
   }

   @Override
   public DocumentUri toUri(DocumentPath documentId, ExportUriOptions options) {
      return uriAdapter.toUri(documentId, options);
   }

   @Override
   public Optional<Document> findDocumentByUri(DocumentUri documentUri) {
      return uriAdapter.findDocumentByUri(documentUri);
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryByUri(DocumentUri documentUri) {
      return uriAdapter.findDocumentEntryByUri(documentUri);
   }

   /**
    * Real GIT URI adapter to be used by {@link DocumentTransformerUriAdapter}.
    */
   public class GitUriAdapter implements DocumentRepositoryUriAdapter {

      @Override
      public ReferencedDocument<? extends DocumentRepository> referencedDocumentId(DocumentUri documentUri) {
         Check.notNull(documentUri, "documentUri");
         var gitDocumentUri = validateGitDocumentUri(documentUri);

         return ReferencedDocument.lazyLoad(GitDocumentRepository.this, gitDocumentUri.documentId());
      }

      @Override
      public RepositoryUri toUri(ExportUriOptions options) {
         Check.notNull(options, "options");

         if (remoteUri == null || gitDocumentConfig.forceLocalUri() || instanceOf(options,
                                                                                  GitUriOptions.class)
               .map(GitUriOptions::forceLocalUri)
               .orElse(false)) {

            return RepositoryUri.ofRepositoryUri(localRepository.toUri(options));
         } else {
            return GitRepositoryUri
                  .ofConfig(gitDocumentConfig, localRepository, null, remoteUri, null)
                  .exportUri(options);
         }
      }

      @Override
      public DocumentUri toUri(DocumentPath documentId, ExportUriOptions options) {
         Check.notNull(documentId, "documentId");
         Check.notNull(options, "options");

         if (remoteUri == null || gitDocumentConfig.forceLocalUri() || instanceOf(options,
                                                                                  GitUriOptions.class)
               .map(GitUriOptions::forceLocalUri)
               .orElse(false)) {
            return DocumentUri.ofDocumentUri(localRepository.toUri(documentId, options));
         } else {
            return GitDocumentUri
                  .ofConfig(gitDocumentConfig, localRepository, null, remoteUri, null, documentId)
                  .exportUri(options);
         }
      }

      @Override
      public boolean supportsUri(RepositoryUri uri) {
         Check.notNull(uri, "uri");

         return supportsGitUri(uri);
      }

      @Override
      public Optional<Document> findDocumentByUri(DocumentUri documentUri) {
         Check.notNull(documentUri, "documentUri");
         var gitDocumentUri = validateGitDocumentUri(documentUri);

         return findDocumentById(gitDocumentUri.documentId());
      }

      @Override
      public Optional<DocumentEntry> findDocumentEntryByUri(DocumentUri documentUri) {
         Check.notNull(documentUri, "documentUri");
         var gitDocumentUri = validateGitDocumentUri(documentUri);

         return findDocumentEntryById(gitDocumentUri.documentId());
      }

      private boolean supportsGitUri(RepositoryUri repositoryUri) {
         try {
            return (GitRepositoryUri
                          .ofConfig(gitDocumentConfig, null, null, null, null)
                          .supportsUri(instanceOf(repositoryUri,
                                                  GitRepositoryUri.class).orElseGet(() -> GitRepositoryUri.ofRepositoryUri(
                                repositoryUri,
                                gitDocumentConfig.basePath()))));
         } catch (IncompatibleUriException __) {
            return false;
         }
      }

      private GitDocumentUri validateGitDocumentUri(DocumentUri documentUri) {
         try {
            var gitUri =
                  instanceOf(documentUri, GitDocumentUri.class).orElseGet(() -> GitDocumentUri.ofDocumentUri(
                        documentUri,
                        gitDocumentConfig.basePath()));

            if (!supportsGitUri(gitUri)) {
               throw new InvalidUriException(documentUri).message("'documentUri=%s' must be supported",
                                                                  documentUri.toString());
            }

            return gitUri;
         } catch (IncompatibleUriException e) {
            throw new InvalidUriException(documentUri).message("'documentUri=%s' must be supported: %s",
                                                               documentUri.toString(),
                                                               e.getMessage());
         }
      }
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", GitDocumentRepository.class.getSimpleName() + "[", "]")
            .add("gitDocumentConfig" + gitDocumentConfig)
            .add("localRepository=" + localRepository)
            .add("remoteUri=" + remoteUri)
            .toString();
   }

   /**
    * Returns access to internal porcelain GIT client directed at this repository location. .
    *
    * @return GIT porcelain client
    *
    * @apiNote You can close the {@link Git} client as it does nothing, but you must not close the
    *       underlying {@link Git#getRepository()}.
    */
   public Git git() {
      return git;
   }

   /**
    * Checks if working tree is clean
    *
    * @return {@code true} if working tree is clean
    *
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    */
   public boolean isClean() {
      try {
         return GitUtils.isClean(git);
      } catch (Exception e) {
         throw wrapGitException(e, "Can't check if repository is clean");
      }
   }

   /**
    * Returns current branch name.
    *
    * @return current branch name
    *
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    */
   public String branch() {
      try {
         return git.getRepository().getBranch();
      } catch (Exception e) {
         throw wrapGitException(e, "Can't detect current branch");
      }
   }

   /**
    * Resolve GIT revision to GIT object id.
    *
    * @param revision GIT revision
    *
    * @return GIT object id or {@link Optional#empty} if revision can't be found
    *
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    */
   public Optional<String> resolveObjectId(String revision) {
      validate(revision, "revision", isNotBlank()).orThrow();

      return resolve(git, revision).map(ObjectId::getName);
   }

   /**
    * Returns whether current branch has {@code HEAD}.
    *
    * @return {@code true} if current branch has {@code HEAD}
    *
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    */
   public boolean hasHead() {
      return resolve(git, HEAD_REVISION).isPresent();
   }

   /**
    * Returns current branch's {@code HEAD} as GIT object id.
    *
    * @return current branch's {@code HEAD} as GIT object id, or {@link Optional#empty()} if current branch
    *       has no {@code HEAD}
    *
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    */
   public Optional<String> head() {
      return resolve(git, HEAD_REVISION).map(ObjectId::name);
   }

   /**
    * Update repository's current branch using configured remote. Repository must be clean if configured pull
    * strategy use rebase.
    *
    * @param fastForward fast-forward mode
    * @param rebase whether to rebase
    *
    * @throws NotCleanGitException if repository is not clean
    * @throws NoHeadGitException if required {@code HEAD} ref not found
    * @throws RefNotAdvertisedGitException if required ref not advertised
    * @throws RefNotFoundGitException if ref can't be resolved
    * @throws WrongRepositoryStateGitException if repository is not in a correct state for the operation
    * @throws InvalidRemoteGitException if remote is invalid
    * @throws TransportGitException if an error occurs in transport protocol
    * @throws InvalidConfigurationGitException if configuration is malformed
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    */
   public void pull(FastForward fastForward, boolean rebase) {
      pull(gitDocumentConfig.remote(), fastForward, rebase);
   }

   /**
    * Update repository's current branch using specified remote. Repository must be clean if configured pull
    * strategy use rebase.
    *
    * @param remote remote name
    * @param fastForward fast-forward mode
    * @param rebase whether to rebase
    *
    * @throws NotCleanGitException if repository is not clean
    * @throws NoHeadGitException if required {@code HEAD} ref not found
    * @throws RefNotAdvertisedGitException if required ref not advertised
    * @throws RefNotFoundGitException if ref can't be resolved
    * @throws WrongRepositoryStateGitException if repository is not in a correct state for the operation
    * @throws InvalidRemoteGitException if remote is invalid
    * @throws TransportGitException if an error occurs in transport protocol
    * @throws InvalidConfigurationGitException if configuration is malformed
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    */
   public void pull(String remote, FastForward fastForward, boolean rebase) {
      validate(remote, "remote", isNotBlank())
            .and(validate(fastForward, "fastForward", isNotNull()))
            .orThrow();

      StopWatch watch = StopWatch.createStarted();

      pull(git, null, remote, fastForward, rebase);

      log.info("Pull from '{}' remote on current branch (fastforward={}, rebase={}) in {} ms",
               remote,
               fastForward,
               rebase,
               duration(watch).toMillis());
   }

   /**
    * Push repository's current branch using configured remote.
    * Tags are not pushed.
    *
    * @throws InvalidRemoteGitException if remote is invalid
    * @throws TransportGitException if an error occurs in transport protocol
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    * @throws PushFailedGitException if push has errors
    * @see #push(boolean, boolean, boolean, String...)
    */
   public void push() {
      push(gitDocumentConfig.remote());
   }

   /**
    * Push repository's current branch using specified remote.
    * Tags are not pushed.
    *
    * @param remote remote name
    *
    * @throws InvalidRemoteGitException if remote is invalid
    * @throws TransportGitException if an error occurs in transport protocol
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    * @throws PushFailedGitException if push has errors
    * @see #push(boolean, boolean, boolean, String...)
    */
   public void push(String remote) {
      StopWatch watch = StopWatch.createStarted();

      push(git, null, remote, false, false, false, false);

      log.info("Push current branch to '{}' remote in {} ms", remote, duration(watch).toMillis());
   }

   /**
    * Pushes specified objects to configured remote.
    *
    * @param force whether to force push
    * @param pushTags whether to also push tags
    * @param pushAllBranches whether to push all branches, not only the current branch
    * @param refSpecs extra RefSpecs to push
    *
    * @throws InvalidRemoteGitException if remote is invalid
    * @throws TransportGitException if an error occurs in transport protocol
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    * @throws PushFailedGitException if push has errors
    */
   public void push(boolean force, boolean pushTags, boolean pushAllBranches, String... refSpecs) {
      push(gitDocumentConfig.remote(), force, pushTags, pushAllBranches, refSpecs);
   }

   /**
    * Pushes current branch using specified remote.
    *
    * @param remote remote name
    * @param force whether to force push
    * @param pushTags whether to also push tags
    * @param pushAllBranches whether to push all branches, not only the current branch
    * @param refSpecs extra RefSpecs to push
    *
    * @throws InvalidRemoteGitException if remote is invalid
    * @throws TransportGitException if an error occurs in transport protocol
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    * @throws PushFailedGitException if push has errors
    */
   public void push(String remote,
                    boolean force,
                    boolean pushTags,
                    boolean pushAllBranches,
                    String... refSpecs) {
      validate(remote, "remote", isNotBlank())
            .and(validate(refSpecs, "refSpecs", ArrayRules.list(hasNoBlankElements())))
            .orThrow();

      StopWatch watch = StopWatch.createStarted();

      push(git,
           null,
           remote,
           false,
           force,
           pushTags,
           pushAllBranches,
           stream(refSpecs).map(RefSpec::new).toArray(RefSpec[]::new));

      log.info("Push [{}] to '{}' remote (force={}) in {} ms",
               streamConcat(stream(pushTags ? "<tags>" : null,
                                   pushAllBranches ? "<all branches>" : "<current branch>"), stream(refSpecs))
                     .filter(Objects::nonNull)
                     .collect(joining(",")),
               remote,
               force,
               duration(watch).toMillis());
   }

   /**
    * Fetch specified objects from configured remote.
    *
    * @param force whether to force update references
    * @param shallowDepth shallow repository depth, can be {@code null} or &gt; 0
    * @param shallowSince deepens or shortens the history of a shallow repository to include all
    *       reachable commits after a specified time
    * @param unshallow unshallow repository. Can't be set if other shallow options are also set
    * @param checkFetchedObjects whether to check fetched objects validity
    * @param removeDeletedRefs whether to remove references deleted from source
    * @param fetchTags fetch tags behavior
    * @param refSpecs RefSpecs to fetch. E.g. : {@code refs/heads/master:refs/heads/master},
    *       {@code refs/heads/remote-branch:local-branch}, {@code refs/heads/remote-branch}
    *
    * @throws InvalidRemoteGitException if remote is invalid
    * @throws TransportGitException if an error occurs in transport protocol
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    */
   public void fetch(boolean force,
                     Integer shallowDepth,
                     Instant shallowSince,
                     boolean unshallow,
                     boolean checkFetchedObjects,
                     Boolean removeDeletedRefs,
                     FetchTag fetchTags,
                     String... refSpecs) {
      fetch(gitDocumentConfig.remote(),
            force,
            shallowDepth,
            shallowSince,
            unshallow,
            checkFetchedObjects,
            removeDeletedRefs,
            fetchTags,
            refSpecs);
   }

   /**
    * Fetch specified objects from specified remote.
    *
    * @param remote remote name
    * @param force whether to force update references
    * @param shallowDepth shallow repository depth, can be {@code null} or &gt; 0
    * @param shallowSince deepens or shortens the history of a shallow repository to include all
    *       reachable commits after a specified time
    * @param unshallow unshallow repository. Can't be set if other shallow options are also set
    * @param checkFetchedObjects whether to check fetched objects validity
    * @param removeDeletedRefs whether to remove references deleted from source
    * @param fetchTags fetch tags behavior
    * @param refSpecs RefSpecs to fetch. E.g. : {@code refs/heads/master:refs/heads/master},
    *       {@code refs/heads/remote-branch:local-branch}, {@code refs/heads/remote-branch}
    *
    * @throws InvalidRemoteGitException if remote is invalid
    * @throws TransportGitException if an error occurs in transport protocol
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    */
   public void fetch(String remote,
                     boolean force,
                     Integer shallowDepth,
                     Instant shallowSince,
                     boolean unshallow,
                     boolean checkFetchedObjects,
                     Boolean removeDeletedRefs,
                     FetchTag fetchTags,
                     String... refSpecs) {
      validate(remote, "remote", isNotBlank())
            .and(validate(refSpecs,
                          "refSpecs",
                          ArrayRules.list(CollectionRules
                                                .<List<String>, String>hasNoBlankElements()
                                                .andValue(size(isStrictlyPositive())))))
            .and(validate(fetchTags, "fetchTags", isNotNull()))
            .orThrow();

      StopWatch watch = StopWatch.createStarted();

      fetch(git,
            null,
            remote,
            force,
            shallowDepth,
            shallowSince,
            unshallow,
            checkFetchedObjects,
            removeDeletedRefs,
            fetchTags,
            stream(refSpecs).map(RefSpec::new).toArray(RefSpec[]::new));

      log.info("Fetch [{}] from '{}' remote in {} ms",
               stream(refSpecs).collect(joining(",")),
               remote,
               duration(watch).toMillis());
   }

   /**
    * Commits indexed documents.
    *
    * @param message commit message, can be empty
    * @param amend whether to amend previous commit
    * @param allowEmptyCommit whether to allow empty commits
    *
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    * @throws EmptyCommitGitException if empty commit is not allowed
    */
   public void commitIndexedDocuments(String message, boolean amend, boolean allowEmptyCommit) {
      validate(message, "message", isNotNull()).orThrow();

      StopWatch watch = StopWatch.createStarted();

      commit(git, message, amend, false, allowEmptyCommit);

      log.info("Commit indexed documents (amend={}) in {} ms", amend, duration(watch).toMillis());
   }

   /**
    * Commits all documents, including all added, changed or deleted tracked files, but not new
    * un-indexed files.
    *
    * @param message commit message, can be empty
    * @param amend whether to amend previous commit
    *
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    * @throws EmptyCommitGitException if empty commit is not allowed
    */
   public void commitAllDocuments(String message, boolean amend, boolean allowEmptyCommit) {
      validate(message, "message", isNotNull()).orThrow();

      StopWatch watch = StopWatch.createStarted();

      commit(git, message, amend, true, allowEmptyCommit);

      log.info("Commit all documents (amend={}) in {} ms", amend, duration(watch).toMillis());
   }

   /**
    * Adds specified documents to index.
    *
    * @param documents list of documents identifiers to remove, relatively to repository
    *
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    */
   public void addDocumentToIndex(DocumentPath... documents) {
      validate(documents, "documents", ArrayRules.list(hasNoNullElements())).orThrow();

      addDocumentToIndex(git, false, documents);
   }

   /**
    * Adds all documents to index.
    *
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    */
   public void addAllDocumentsToIndex() {
      addDocumentToIndex(git, true);
   }

   /**
    * Removes a document from index.
    *
    * @param cached whether to remove file from index only, but not physically
    * @param documents list of documents identifiers to remove, relatively to repository
    *
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    */
   public void removeDocumentFromIndex(boolean cached, DocumentPath... documents) {
      validate(documents, "documents", ArrayRules.list(hasNoNullElements())).orThrow();

      removeDocumentFromIndex(git, cached, documents);
   }

   /**
    * Set configured remote's URI in GIT configuration.
    *
    * @param remoteUri URI to set
    *
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    */
   public void remoteSetUrl(URI remoteUri) {
      validate(remoteUri, "remoteUri", isNotNull()).orThrow();

      StopWatch watch = StopWatch.createStarted();

      remoteSetUrl(git, null, remoteUri, FETCH);

      log.info("Set default remote URL to '{}' in {} ms", remoteUri, duration(watch).toMillis());
   }

   /**
    * Set specified remote's URI in GIT configuration.
    *
    * @param remote remote name
    * @param remoteUri URI to set
    *
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    */
   public void remoteSetUrl(String remote, URI remoteUri) {
      validate(remote, "remote", isNotBlank()).and(validate(remoteUri, "remoteUri", isNotNull())).orThrow();

      StopWatch watch = StopWatch.createStarted();

      remoteSetUrl(git, remote, remoteUri, FETCH);

      log.info("Set '{}' remote URL to '{}' in {} ms", remote, remoteUri, duration(watch).toMillis());
   }

   /**
    * Get specified remote's URI from GIT configuration.
    *
    * @param remote remote name
    *
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    */
   public void remoteGetUrl(String remote) {
      validate(remote, "remote", isNotBlank()).orThrow();

      StopWatch watch = StopWatch.createStarted();

      remoteGetUrl(git, remote);

      log.info("Get remote URL from '{}' in {} ms", remote, duration(watch).toMillis());
   }

   /**
    * Checkout specified branch.
    *
    * @param branch branch name to checkout
    * @param createIfMissing create branch if missing
    * @param createStartRevision use this GIT revision to create branch from, if needed
    *
    * @return checkouted branch revision, or {@link Optional#empty()} if not checkout occurred (already on
    *       correct branch, ...)
    *
    * @throws RefNotFoundGitException if specified ref can't be resolved
    * @throws CheckoutConflictGitException if a conflict occurs
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    */
   public Optional<String> checkout(String branch, boolean createIfMissing, String createStartRevision) {
      validate(branch, "branch", isNotBlank())
            .and(validate(createStartRevision, "createStartRevision", isNotBlank()))
            .orThrow();

      StopWatch watch = StopWatch.createStarted();

      return checkout(git, null, branch, createIfMissing, createStartRevision).map(revision -> {
         if (log.isInfoEnabled()) {
            String parameters = "createIfMissing=" + createIfMissing;
            if (createIfMissing) {
               parameters += ", createStartRevision=" + createStartRevision;
            }

            log.info("Checkout '{}' branch ({}) in {} ms", branch, parameters, duration(watch).toMillis());
         }

         return revision;
      });
   }

   /**
    * Checkout specified branch.
    *
    * @param branch branch name to checkout
    * @param createIfMissing create branch from {@code HEAD} if missing
    *
    * @throws RefNotFoundGitException if specified ref can't be resolved
    * @throws CheckoutConflictGitException if a conflict occurs
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    */
   public void checkout(String branch, boolean createIfMissing) {
      checkout(branch, createIfMissing, HEAD_REVISION);
   }

   /**
    * Checkout specified branch, or creates an orphan branch.
    *
    * @param branch branch name to checkout
    * @param createIfMissing create branch if missing
    *
    * @throws CheckoutConflictGitException if a conflict occurs
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    */
   public void checkoutOrphan(String branch, boolean createIfMissing) {
      Check.notBlank(branch, "branch");

      StopWatch watch = StopWatch.createStarted();

      checkoutOrphan(git, null, branch, createIfMissing);

      if (log.isInfoEnabled()) {
         String parameters = "createIfMissing=" + createIfMissing;

         log.info("Checkout '{}' orphan branch ({}) in {} ms",
                  branch,
                  parameters,
                  duration(watch).toMillis());
      }
   }

   /**
    * Merge specified revision to current branch.
    *
    * @param revision revision name to merge to current branch (e.g.: {@code main},
    *       {@code refs/heads/main}, ...)
    * @param fastForward fast-forward mode to use
    * @param contentMergeStrategy content merge strategy to use
    * @param message merge message
    *
    * @return merge status if merge is successful
    *
    * @throws NoHeadGitException if required {@code HEAD} ref not found
    * @throws CheckoutConflictGitException if a conflict occurs
    * @throws WrongRepositoryStateGitException if repository is not in a correct state for the operation
    * @throws MergeFailedGitException if merge failed
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    */
   public MergeStatus merge(String revision,
                            FastForward fastForward,
                            ContentMergeStrategy contentMergeStrategy,
                            String message) {
      validate(revision, "revision", isNotBlank())
            .and(Validate.notNull(fastForward, "fastForward"))
            .and(Validate.notNull(contentMergeStrategy, "contentMergeStrategy"))
            .and(Validate.notNull(message, "message"))
            .orThrow();

      StopWatch watch = StopWatch.createStarted();

      MergeStatus mergeStatus = merge(git, null, revision, fastForward, contentMergeStrategy, message);

      log.info("Merge '{}' revision (fastForward={}, contentMergeStrategy={}) in {} ms",
               revision,
               fastForward,
               contentMergeStrategy,
               duration(watch).toMillis());

      return mergeStatus;
   }

   /**
    * Initializes document repository by either :
    * <ul>
    * <li>if {@code cloneIfMissing} : cloning GIT repository from remote URI to local repository</li>
    * <li>if {@code initIfMissing} : initializing local GIT repository and checkout to work branch</li>
    * <li>configure local repository</li>
    * </ul>
    *
    * @return initialization state and GIT repository
    *
    * @throws GitException if an error occurs while operating GIT repository
    * @implNote Caller has the responsibility to close the returned {@link Git#getRepository()}
    *       (beware that {@link Git#close()} close nothing in most cases !).
    */
   private Pair<GitInitializationState, Git> initializeRepository() {
      try {
         Repository repository = fileSystemRepository(localRepository.storagePath()).orElse(null);
         GitInitializationState initializationState;
         Git git;

         try {
            if (repository != null) {
               initializationState = GitInitializationState.EXISTING;
               git = new Git(repository);
            } else {
               if (gitDocumentConfig.cloneIfMissing() && gitDocumentConfig.remoteUri().isPresent()) {
                  initializationState = GitInitializationState.CLONED;
                  git = new Git(cloneRepository(null,
                                                gitDocumentConfig.remote(),
                                                gitDocumentConfig.remoteUri().get(),
                                                gitDocumentConfig.branch().orElse(null),
                                                gitDocumentConfig
                                                      .pullStrategy()
                                                      .shallowDepth()
                                                      .orElse(null)));
               } else {
                  initializationState = GitInitializationState.INITIALIZED;
                  git = new Git(fileSystemRepository(localRepository.storagePath(),
                                                     gitDocumentConfig.initIfMissing(),
                                                     gitDocumentConfig.initialBranch()));

                  gitDocumentConfig.remoteUri().ifPresent(remoteUri -> {
                     remoteSetUrl(git, gitDocumentConfig.remote(), remoteUri, FETCH);
                  });
               }
            }
         } catch (Exception e) {
            if (repository != null) {
               repository.close();
            }
            throw e;
         }
         return Pair.of(initializationState, git);

      } catch (Exception e) {
         throw wrapGitException(e, "Can't initialize repository");
      }
   }

   private enum GitInitializationState {
      CLONED, INITIALIZED, EXISTING
   }

   /** Generates a general commit message for commit on close mode. */
   private String onCloseCommitMessage() {
      Set<String> addedDocuments;
      Set<String> changedDocuments;
      Set<String> removedDocuments;

      try {
         Status status = git.status().call();
         addedDocuments = status.getAdded();
         changedDocuments = status.getChanged();
         removedDocuments = status.getRemoved();
      } catch (Exception e) {
         throw wrapGitException(e, "Can't get repository status");
      }

      return String.format("Add %d, change %d, remove %d documents",
                           addedDocuments.size(),
                           changedDocuments.size(),
                           removedDocuments.size());
   }

   /**
    * Apply GIT pull strategy in repository document operations.
    *
    * @param git GIT API
    * @param onCreate whether context is repository initialization
    *
    * @throws GitException if an error occurs while operating GIT repository
    */
   private void applyPullStrategy(Git git, boolean onCreate) {
      var pullStrategy = gitDocumentConfig.pullStrategy();

      if ((onCreate && pullStrategy.pullMode() == PullMode.ON_CREATE) || (!onCreate
                                                                          && pullStrategy.pullMode()
                                                                             == PullMode.ALWAYS)) {
         pull(git, null, gitDocumentConfig.remote(), pullStrategy.fastForward(), pullStrategy.rebase());
      }
   }

   /**
    * Apply GIT pull strategy in repository document operations.
    *
    * @param onCreate whether context is repository initialization
    *
    * @throws GitException if an error occurs while operating GIT repository
    */
   private void applyPullStrategy(boolean onCreate) {
      applyPullStrategy(git, onCreate);
   }

   /**
    * Apply GIT push strategy in repository document operations.
    *
    * @param message commit message
    * @param onClose whether context is repository closing
    *
    * @throws GitException if an error occurs while operating GIT repository
    */
   private void applyPushStrategy(String message, boolean onClose) {
      var pushStrategy = gitDocumentConfig.pushStrategy();

      if ((onClose && pushStrategy.commitMode() == PushMode.ON_CLOSE) || (!onClose
                                                                          && pushStrategy.commitMode()
                                                                             == PushMode.ALWAYS)) {
         try {
            commitIndexedDocuments(message, false, false);
         } catch (EmptyCommitGitException e) {
            /* do nothing */
         }
      }
      if ((onClose && pushStrategy.pushMode() == PushMode.ON_CLOSE) || (!onClose
                                                                        && pushStrategy.pushMode()
                                                                           == PushMode.ALWAYS)) {
         push(git, null, gitDocumentConfig.remote(), false, false, false, false);
      }
   }

   /**
    * Set configured remote's repository URI.
    *
    * @param git GIT API
    * @param remote remote name, or {@code null} to use configured remote
    * @param remoteUri remote URI to set
    * @param uriType URI type to set
    *
    * @throws GitException if an error occurs while operating GIT repository
    */
   private void remoteSetUrl(Git git, String remote, URI remoteUri, UriType uriType) {
      try {
         git
               .remoteSetUrl()
               .setRemoteName(nullable(remote).orElse(gitDocumentConfig.remote()))
               .setRemoteUri(new URIish(remoteUri.toString()))
               .setUriType(uriType)
               .call();
      } catch (Exception e) {
         throw wrapGitException(e, "Can't set '%s' remote URL to '%s'", remote, remoteUri);
      }
   }

   /**
    * Get configured remote's repository URI.
    *
    * @param git GIT API
    * @param remote remote name, or {@code null} to use configured remote
    *
    * @return remote URI, or {@link Optional#empty()} if no URL configured for specified remote, or if URI is
    *       not valid
    *
    * @throws GitException if an error occurs while operating GIT repository
    */
   private Optional<URI> remoteGetUrl(Git git, String remote) {
      String remoteUrl = git
            .getRepository()
            .getConfig()
            .getString(ConfigConstants.CONFIG_REMOTE_SECTION, remote, ConfigConstants.CONFIG_KEY_URL);
      try {
         if (remoteUrl != null) {
            return optional(new URI(remoteUrl));
         } else {
            return optional();
         }
      } catch (URISyntaxException e) {
         log.warn("GIT '{}' URL configuration for '{}' remote is not a supported URI", remoteUrl, remote);
         return optional();
      }
   }

   /**
    * Commit files.
    *
    * @param git GIT API
    * @param message commit message, can be empty
    * @param amend whether to amend commit
    * @param all commit all changes on tracked files
    * @param allowEmptyCommit whether to allow empty commits
    *
    * @throws GitException if an error occurs while operating GIT repository
    * @throws EmptyCommitGitException if empty commit is not allowed
    */
   private void commit(Git git, String message, boolean amend, boolean all, boolean allowEmptyCommit) {
      notNull(message, "message");

      try {
         CommitCommand commitCommand = git
               .commit()
               .setCleanupMode(DEFAULT)
               .setAllowEmpty(allowEmptyCommit)
               .setAmend(amend)
               .setMessage(message)
               .setAll(all);

         commitCommand.call();
      } catch (Exception e) {
         throw wrapGitException(e,
                                "Can't commit documents (amend=%s, all=%s, allowEmptyCommit=%s",
                                amend,
                                all,
                                allowEmptyCommit);
      }
   }

   /**
    * Clones the repository from specified repository URI.
    *
    * @param monitor optional progress monitor
    * @param remote remote name
    * @param remoteUri remote URI to clone
    * @param remoteBranch optional branch name to checkout after clone, or {@code null} to checkout
    *       repository's default branch (repository's HEAD)
    * @param shallowDepth optional shallow clone depth, can be set to {@code null} to disable shallow
    *       clone
    *
    * @return GIT repository
    *
    * @throws InvalidRemoteGitException if remote is invalid
    * @throws TransportGitException if an error occurs in transport protocol
    * @throws GitException if an error occurs while operating GIT repository
    * @implNote Caller has the responsibility to close the returned repository.
    */
   @SuppressWarnings("resource")
   private Repository cloneRepository(ProgressMonitor monitor,
                                      String remote,
                                      URI remoteUri,
                                      String remoteBranch,
                                      Integer shallowDepth) {
      try {
         CloneCommand cloneCommand = configureTransport(Git
                                                              .cloneRepository()
                                                              .setRemote(remote)
                                                              .setURI(remoteUri.toString())
                                                              .setBranch(remoteBranch)
                                                              .setDirectory(localRepository
                                                                                  .storagePath()
                                                                                  .toFile())
                                                              .setProgressMonitor(monitor));

         nullable(shallowDepth).ifPresent(cloneCommand::setDepth);

         Git call = cloneCommand.call();

         return call.getRepository();
      } catch (Exception e) {
         throw wrapGitException(e, "Can't clone '%s' repository", remoteUri);
      }
   }

   /**
    * Update repository's current branch using configured remote. Repository must be clean if configured pull
    * strategy use rebase.
    *
    * @param git GIT API
    * @param monitor optional progress monitor
    * @param remote remote name, or {@code null} to use configured remote
    * @param fastForward fast-forward mode
    * @param rebase whether to rebase
    *
    * @throws NotCleanGitException if repository is not clean
    * @throws NoHeadGitException if required {@code HEAD} ref not found
    * @throws RefNotAdvertisedGitException if required ref not advertised
    * @throws RefNotFoundGitException if ref can't be resolved
    * @throws WrongRepositoryStateGitException if repository is not in a correct state for the operation
    * @throws InvalidRemoteGitException if remote is invalid
    * @throws TransportGitException if an error occurs in transport protocol
    * @throws InvalidConfigurationGitException if configuration is malformed
    * @throws GitException if an error occurs while operating GIT repository
    */
   private void pull(Git git,
                     ProgressMonitor monitor,
                     String remote,
                     FastForward fastForward,
                     boolean rebase) {
      try {
         if (gitDocumentConfig.pullStrategy().rebase()) {
            checkClean();
         }

         PullCommand pullCommand = configureTransport(git
                                                            .pull()
                                                            .setFastForward(fastForward(fastForward))
                                                            .setRebase(rebase)
                                                            .setRemote(nullable(remote).orElse(
                                                                  gitDocumentConfig.remote()))
                                                            .setProgressMonitor(monitor));

         pullCommand.call();
      } catch (Exception e) {
         throw wrapGitException(e, "Can't pull repository");
      }
   }

   /**
    * Pushes specified objects to remote.
    *
    * @param git GIT API
    * @param monitor optional progress monitor
    * @param remote remote name
    * @param atomic whether to use atomic push
    * @param force whether to force push
    * @param pushTags whether to also push tags
    * @param pushAllBranches whether to push all branches, not only the current branch
    * @param refSpecs extra RefSpecs to push
    *
    * @throws InvalidRemoteGitException if remote is invalid
    * @throws TransportGitException if an error occurs in transport protocol
    * @throws PushFailedGitException if push failed
    * @throws GitException if an error occurs while operating GIT repository
    */
   private void push(Git git,
                     ProgressMonitor monitor,
                     String remote,
                     boolean atomic,
                     boolean force,
                     boolean pushTags,
                     boolean pushAllBranches,
                     RefSpec... refSpecs) {
      try {
         PushCommand pushCommand = configureTransport(git
                                                            .push()
                                                            .setAtomic(atomic)
                                                            .setRemote(remote)
                                                            .setForce(force)
                                                            .setProgressMonitor(monitor));

         List<RefSpec> pushRefSpecs = list(refSpecs);
         if (pushTags) {
            pushRefSpecs.add(org.eclipse.jgit.transport.Transport.REFSPEC_TAGS);
         }
         if (pushAllBranches) {
            pushRefSpecs.add(org.eclipse.jgit.transport.Transport.REFSPEC_PUSH_ALL);
         }

         pushCommand.setRefSpecs(pushRefSpecs);

         checkPushResults(pushCommand.call());
      } catch (PushFailedGitException e) {
         throw e;
      } catch (Exception e) {
         String objects = streamConcat(stream(pushTags ? "<tags>" : null,
                                              pushAllBranches ? "<all branches>" : "<current branch>"),
                                       stream(refSpecs).map(RefSpec::toString))
               .filter(Objects::nonNull)
               .collect(joining(","));

         throw wrapGitException(e, String.format("Can't push [%s] to '%s' remote", objects, remote));
      }
   }

   /**
    * Fetch specified RefSpecs.
    *
    * @param git GIT API
    * @param monitor optional progress monitor
    * @param remote remote name
    * @param force whether to force update references
    * @param shallowDepth shallow repository depth, can be {@code null} to ignore, otherwise must be
    *       &gt; 0
    * @param shallowSince deepens or shortens the history of a shallow repository to include all
    *       reachable commits after a specified time, can be {@code null} to ignore
    * @param unshallow unshallow repository
    * @param checkFetchedObjects whether to check fetched objects validity
    * @param removeDeletedRefs whether to remove references deleted from source, set to {@code null} to
    *       use GIT configuration
    * @param fetchTags fetch tags behavior
    * @param refSpecs RefSpecs to fetch. E.g. : {@code refs/heads/master:refs/heads/master},
    *       {@code refs/heads/remote-branch:local-branch}, {@code refs/heads/remote-branch}
    *
    * @throws InvalidRemoteGitException if remote is invalid
    * @throws TransportGitException if an error occurs in transport protocol
    * @throws GitException if an error occurs while operating GIT repository
    */
   private void fetch(Git git,
                      ProgressMonitor monitor,
                      String remote,
                      boolean force,
                      Integer shallowDepth,
                      Instant shallowSince,
                      boolean unshallow,
                      boolean checkFetchedObjects,
                      Boolean removeDeletedRefs,
                      FetchTag fetchTags,
                      RefSpec... refSpecs) {
      try {
         FetchCommand fetchCommand = configureTransport(git
                                                              .fetch()
                                                              .setUnshallow(unshallow)
                                                              .setCheckFetchedObjects(checkFetchedObjects)
                                                              .setForceUpdate(force)
                                                              .setProgressMonitor(monitor)
                                                              .setRemote(remote)
                                                              .setRefSpecs(refSpecs)
                                                              .setTagOpt(tagOpt(fetchTags)));

         nullable(shallowDepth).ifPresent(fetchCommand::setDepth);
         nullable(shallowSince).ifPresent(fetchCommand::setShallowSince);
         nullable(removeDeletedRefs).ifPresent(fetchCommand::setRemoveDeletedRefs);

         fetchCommand.call();
      } catch (Exception e) {
         String objects = stream(refSpecs).map(RefSpec::toString).collect(joining(","));

         throw wrapGitException(e, String.format("Can't fetch [%s] from '%s' remote", objects, remote));
      }
   }

   /**
    * Checkout specified branch.
    *
    * @param git GIT API
    * @param monitor optional progress monitor
    * @param branch branch name to checkout
    * @param createIfMissing create branch if missing
    * @param createStartRevision use this GIT revision to create branch from, if needed, or {@code null}
    *       to use {@code HEAD} default start point
    *
    * @return checkouted branch revision, or {@link Optional#empty()} if not checkout occurred (already on
    *       correct branch, ...)
    *
    * @throws RefNotFoundGitException if specified ref can't be resolved
    * @throws CheckoutConflictGitException if a conflict occurs
    * @throws GitException if an error occurs while operating GIT repository
    */
   private Optional<String> checkout(Git git,
                                     ProgressMonitor monitor,
                                     String branch,
                                     boolean createIfMissing,
                                     String createStartRevision) {
      try {

         if (!branch.equals(branch())) {
            var createBranch = createIfMissing && !branchExists(git.getRepository(), branch);
            CheckoutCommand checkoutCommand = git
                  .checkout()
                  .setName(branch)
                  .setCreateBranch(createBranch)
                  .setStartPoint(nullable(createStartRevision).orElse(HEAD_REVISION))
                  //FIXME .setUpstreamMode(SetupUpstreamMode.SET_UPSTREAM|TRACK)
                  .setProgressMonitor(monitor);

            return optional(checkoutCommand.call().getName());
         } else {
            return optional();
         }
      } catch (Exception e) {
         throw wrapGitException(e, "Can't checkout '%s' branch", branch);
      }
   }

   /**
    * Checkout specified branch, or creates an orphan branch.
    *
    * @param git GIT API
    * @param monitor optional progress monitor
    * @param branch branch name to checkout
    * @param createIfMissing create branch if missing
    *
    * @return checkouted branch revision, or {@link Optional#empty()} if not checkout occurred (already on
    *       correct branch, ...)
    *
    * @throws CheckoutConflictGitException if a conflict occurs
    * @throws GitException if an error occurs while operating GIT repository
    */
   private Optional<String> checkoutOrphan(Git git,
                                           ProgressMonitor monitor,
                                           String branch,
                                           boolean createIfMissing) {
      try {
         if (!branch.equals(branch())) {
            var createBranch = createIfMissing && !branchExists(git.getRepository(), branch);
            CheckoutCommand checkoutCommand = git.checkout().setName(branch).setOrphan(createBranch)
                                                 //FIXME .setUpstreamMode(SetupUpstreamMode.SET_UPSTREAM|TRACK)
                                                 .setProgressMonitor(monitor);

            return optional(checkoutCommand.call().getName());
         } else {
            return optional();
         }
      } catch (Exception e) {
         throw wrapGitException(e, "Can't checkout '%s' orphan branch", branch);
      }
   }

   /**
    * Merge specified ref to current branch
    *
    * @param git GIT API
    * @param monitor optional progress monitor
    * @param reference reference to merge to current branch
    * @param fastForward fast-forward mode to use
    * @param contentMergeStrategy content merge strategy to use
    * @param message merge message, can be empty
    *
    * @return merge status if merge is successful
    *
    * @throws NoHeadGitException if required {@code HEAD} ref not found
    * @throws CheckoutConflictGitException if a conflict occurs
    * @throws WrongRepositoryStateGitException if repository is not in a correct state for the operation
    * @throws MergeFailedGitException if merge failed
    * @throws GitException if an error occurs while operating GIT repository
    */
   private MergeStatus merge(Git git,
                             ProgressMonitor monitor,
                             Ref reference,
                             FastForward fastForward,
                             ContentMergeStrategy contentMergeStrategy,
                             String message) {
      try {
         MergeCommand mergeCommand = git
               .merge()
               .setFastForward(fastForward(fastForward))
               .setStrategy(RECURSIVE)
               .setContentMergeStrategy(contentMergeStrategy(contentMergeStrategy))
               .setMessage(message)
               .include(reference)
               .setProgressMonitor(monitor);

         return checkMergeResults(reference, mergeCommand.call());
      } catch (MergeFailedGitException e) {
         throw e;
      } catch (Exception e) {
         throw wrapGitException(e, "Can't merge '%s' reference", reference);
      }
   }

   /**
    * Merge specified branch to current branch.
    *
    * @param git GIT API
    * @param monitor optional progress monitor
    * @param revision revision to merge to current branch (e.g.: {@code main},
    *       {@code refs/heads/main}, ...)
    * @param fastForward fast-forward mode to use
    * @param contentMergeStrategy content merge strategy to use
    * @param message merge message, can be empty
    *
    * @return merge status if merge is successful
    *
    * @throws NoHeadGitException if required {@code HEAD} ref not found
    * @throws CheckoutConflictGitException if a conflict occurs
    * @throws WrongRepositoryStateGitException if repository is not in a correct state for the operation
    * @throws MergeFailedGitException if merge failed
    * @throws GitException if an error occurs while operating GIT repository
    */
   private MergeStatus merge(Git git,
                             ProgressMonitor monitor,
                             String revision,
                             FastForward fastForward,
                             ContentMergeStrategy contentMergeStrategy,
                             String message) {
      try {
         return merge(git,
                      monitor,
                      git.getRepository().findRef(revision),
                      fastForward,
                      contentMergeStrategy,
                      message);
      } catch (IOException e) {
         throw wrapGitException(e, "Can't merge '%s' revision", revision);
      }
   }

   /**
    * Check merge results and throw an exception with information if merge is not successful.
    *
    * @param reference merge from reference
    * @param mergeResults merge results
    *
    * @return merge status if merge is successful
    *
    * @throws MergeFailedGitException if merge is not successful
    */
   private MergeStatus checkMergeResults(Ref reference, MergeResult mergeResults) {
      MergeStatus mergeStatus = mergeStatus(mergeResults.getMergeStatus());

      if (!mergeStatus.successful()) {
         Map<String, int[][]> conflicts = mergeResults.getConflicts();

         StringBuilder documentsInConflicts = new StringBuilder();
         if (conflicts != null) {
            documentsInConflicts.append(System.lineSeparator());
            for (String path : conflicts.keySet()) {
               documentsInConflicts.append("Conflict: ").append(path).append(System.lineSeparator());
            }
         }

         throw new MergeFailedGitException(mergeStatus,
                                           String.format(
                                                 "Merge '%s (%s)' reference failed with '%s' status\n%s%s",
                                                 reference.getName(),
                                                 reference.getObjectId(),
                                                 mergeStatus.name(),
                                                 mergeResults,
                                                 documentsInConflicts));
      } else {
         return mergeStatus;
      }
   }

   /**
    * Adds specified documents to index.
    *
    * @param git GIT API
    * @param documents list of documents identifiers to add, relatively to repository
    */
   private void addDocumentToIndex(Git git, boolean all, DocumentPath... documents) {
      try {
         AddCommand addCommand = git.add();

         if (all) {
            addCommand.addFilepattern(optional(localRepository.basePath())
                                            .filter(not(PathUtils::isEmpty))
                                            .orElse(Path.of("."))
                                            .toString());
         }

         stream(documents)
               .map(document -> localRepository
                     .storagePath()
                     .relativize(localRepository.storageStrategy().storageFile(document)))
               .map(Path::toString)
               .forEach(addCommand::addFilepattern);

         addCommand.call();
      } catch (Exception e) {
         throw wrapGitException(e,
                                "Can't add %s documents to index",
                                streamConcat(stream(optional(".").filter(__ -> all)),
                                             stream(documents).map(DocumentPath::stringValue)).collect(joining(
                                      ",",
                                      "[",
                                      "]")));
      }
   }

   /**
    * Removes specified documents from index.
    *
    * @param git GIT API
    * @param cached whether to remove file from index only, but not physically
    * @param documents list of documents identifiers to remove, relatively to repository
    */
   private void removeDocumentFromIndex(Git git, boolean cached, DocumentPath... documents) {
      try {
         RmCommand rmCommand = git.rm();

         rmCommand.setCached(cached);

         stream(documents)
               .map(document -> localRepository
                     .storagePath()
                     .relativize(localRepository.storageStrategy().storageFile(document)))
               .map(Path::toString)
               .forEach(rmCommand::addFilepattern);

         rmCommand.call();
      } catch (Exception e) {
         throw wrapGitException(e,
                                "Can't remove %s documents from index (cached=%s)",
                                stream(documents)
                                      .map(DocumentPath::stringValue)
                                      .collect(joining(",", "[", "]")),
                                cached);
      }
   }

   /**
    * Resolve the specified revision to GIT object id.
    *
    * @param revision GIT revision string
    * @param git GIT API
    *
    * @return object id
    *
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    * @see Repository#resolve(String)
    */
   private Optional<ObjectId> resolve(Git git, String revision) {
      try {
         return nullable(git.getRepository().resolve(revision));
      } catch (Exception e) {
         throw wrapGitException(e, "Can't resolve '%s' revision", revision);
      }
   }

   /**
    * Loads the specified document from any GIT branch/commit/... using specified GIT revision string.
    *
    * @param git GIT API
    * @param revision GIT revision string
    * @param documentId document path to load
    *
    * @return document or {@link Optional#empty} if document not found
    *
    * @throws UnknownRevisionGitException if specified revision can't be resolved
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    * @see Repository#resolve(String)
    */
   private Optional<Document> loadDocument(Git git, String revision, DocumentPath documentId) {
      try {
         ObjectId revisionObjectId =
               resolve(git, revision).orElseThrow(() -> new UnknownRevisionGitException(String.format(
                     "Can't resolve '%s' revision",
                     revision)));

         try (RevWalk walk = new RevWalk(git.getRepository())) {
            RevCommit commit = walk.parseCommit(revisionObjectId);

            try (TreeWalk treeWalk = TreeWalk.forPath(git.getRepository(),
                                                      documentId.stringValue(),
                                                      commit.getTree())) {
               if (treeWalk == null) {
                  return optional();
               }

               ObjectId documentObjectId = treeWalk.getObjectId(0);

               ObjectLoader loader = git.getRepository().open(documentObjectId);
               return optional(new DocumentBuilder()
                                     .<DocumentBuilder>reconstitute()
                                     .documentId(documentId)
                                     .metadata(new DocumentMetadataBuilder()
                                                     .<DocumentMetadataBuilder>reconstitute()
                                                     .documentPath(documentId.value())
                                                     .contentSize(loader.getSize())
                                                     .build())
                                     .streamContent(loader.openStream(), loader.getSize())
                                     .build());
            } catch (MissingObjectException e) {
               return optional();
            }
         }
      } catch (Exception e) {
         throw wrapGitException(e,
                                "Can't load '%s' document from '%s' revision",
                                documentId.stringValue(),
                                revision);
      }
   }

   /**
    * Loads the specified document entry from any GIT branch/commit/... using specified GIT revision string.
    *
    * @param git GIT API
    * @param revision GIT revision string
    * @param documentId document path to load
    *
    * @return document entry or {@link Optional#empty} if document not found
    *
    * @throws UnknownRevisionGitException if specified revision can't be resolved
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    * @see Repository#resolve(String)
    */
   private Optional<DocumentEntry> loadDocumentEntry(Git git, String revision, DocumentPath documentId) {
      try {
         ObjectId revisionObjectId =
               resolve(git, revision).orElseThrow(() -> new UnknownRevisionGitException(String.format(
                     "Can't resolve '%s' revision",
                     revision)));

         try (RevWalk walk = new RevWalk(git.getRepository())) {
            RevCommit commit = walk.parseCommit(revisionObjectId);

            try (TreeWalk treeWalk = TreeWalk.forPath(git.getRepository(),
                                                      documentId.stringValue(),
                                                      commit.getTree())) {
               if (treeWalk == null) {
                  return optional();
               }

               ObjectId documentObjectId = treeWalk.getObjectId(0);

               ObjectLoader loader = git.getRepository().open(documentObjectId);
               return optional(new DocumentEntryBuilder()
                                     .<DocumentEntryBuilder>reconstitute()
                                     .documentId(documentId)
                                     .metadata(new DocumentMetadataBuilder()
                                                     .<DocumentMetadataBuilder>reconstitute()
                                                     .documentPath(documentId.value())
                                                     .contentSize(loader.getSize())
                                                     .build())
                                     .build());
            } catch (MissingObjectException e) {
               return optional();
            }
         }
      } catch (Exception t) {
         throw wrapGitException(t,
                                "Can't load '%s' document from '%s' revision",
                                documentId.stringValue(),
                                revision);
      }
   }

   /**
    * Git operations error management and exception wrapping.
    *
    * @param exception original exception
    * @param errorMessage operation error message
    * @param args operation error message arguments
    *
    * @return wrapped {@link GitException}
    */
   private static GitException wrapGitException(Exception exception, String errorMessage, Object... args) {
      String message = String.format(errorMessage, args) + " > " + exception.getMessage();

      if (exception instanceof NoHeadException) {
         return new NoHeadGitException(message, exception.getCause());
      } else if (exception instanceof LockFailedException) {
         return new LockFailedGitException(message, exception.getCause());
      } else if (exception instanceof RefNotAdvertisedException) {
         return new RefNotAdvertisedGitException(message, exception.getCause());
      } else if (exception instanceof RefNotFoundException) {
         return new RefNotFoundGitException(message, exception.getCause());
      } else if (exception instanceof WrongRepositoryStateException) {
         return new WrongRepositoryStateGitException(message, exception.getCause());
      } else if (exception instanceof InvalidRemoteException) {
         return new InvalidRemoteGitException(message, exception.getCause());
      } else if (exception instanceof TransportException) {
         return new TransportGitException(message, exception.getCause());
      } else if (exception instanceof InvalidConfigurationException) {
         return new InvalidConfigurationGitException(message, exception.getCause());
      } else if (exception instanceof CheckoutConflictException) {
         return new CheckoutConflictGitException(message, exception.getCause());
      } else if (exception instanceof EmptyCommitException) {
         return new EmptyCommitGitException(errorMessage, exception.getCause());
      } else if (exception instanceof JGitInternalException) {
         if (exception.getCause() != null) {
            if (exception.getCause() instanceof Error) {
               throw (Error) exception.getCause();
            } else {
               return wrapGitException((Exception) exception.getCause(), message);
            }
         } else {
            return new GitException(message, exception);
         }
      } else if (exception instanceof GitAPIException) {
         return new GitException(message, exception);
      } else if (exception instanceof IOException || exception instanceof UncheckedIOException) {
         return new IoGitException(errorMessage, exception);
      } else {
         return new GitException(message, exception);
      }
   }

   /**
    * Checks if GIT repository is "clean", otherwise throw an exception
    *
    * @throws NotCleanGitException if repository is not clean
    */
   private void checkClean() {
      if (!isClean()) {
         throw new NotCleanGitException("Working tree is not clean");
      }
   }

   /**
    * Generic configuration for transport commands.
    *
    * @param transportCommand transport command to configure
    * @param <C> command type
    * @param <T> command return type
    */
   private <C extends TransportCommand<C, T>, T> C configureTransport(C transportCommand) {
      var transport = gitDocumentConfig.transport();

      return transportCommand
            .setCredentialsProvider(credentialsProvider)
            .setTimeout((int) transport.connectTimeout().getSeconds());
   }

   private static TagOpt tagOpt(FetchTag fetchTags) {
      TagOpt tagOpt;

      switch (fetchTags) {
         case AUTO:
            tagOpt = TagOpt.AUTO_FOLLOW;
            break;
         case NO_TAGS:
            tagOpt = TagOpt.NO_TAGS;
            break;
         case TAGS:
            tagOpt = TagOpt.FETCH_TAGS;
            break;
         default:
            throw new IllegalStateException(String.format("Unknown '%s' fetch tag option", fetchTags.name()));
      }

      return tagOpt;
   }

   private static FastForwardMode fastForward(FastForward fastForward) {
      FastForwardMode ff;

      switch (fastForward) {
         case FAST_FORWARD:
            ff = FF;
            break;
         case NO_FAST_FORWARD:
            ff = NO_FF;
            break;
         case FAST_FORWARD_ONLY:
            ff = FF_ONLY;
            break;
         default:
            throw new IllegalStateException(String.format("Unknown '%s' fast-forward mode",
                                                          fastForward.name()));
      }

      return ff;
   }

   private static org.eclipse.jgit.merge.ContentMergeStrategy contentMergeStrategy(ContentMergeStrategy contentMergeStrategy) {
      org.eclipse.jgit.merge.ContentMergeStrategy cms;

      switch (contentMergeStrategy) {
         case CONFLICT:
            cms = CONFLICT;
            break;
         case OURS:
            cms = OURS;
            break;
         case THEIRS:
            cms = THEIRS;
            break;
         default:
            throw new IllegalStateException(String.format("Unknown '%s' content merge strategy",
                                                          contentMergeStrategy.name()));
      }

      return cms;
   }

   private MergeStatus mergeStatus(MergeResult.MergeStatus status) {
      MergeStatus ret;

      switch (status) {
         case FAST_FORWARD:
            ret = MergeStatus.FAST_FORWARD;
            break;
         case FAST_FORWARD_SQUASHED:
            ret = MergeStatus.FAST_FORWARD_SQUASHED;
            break;
         case ALREADY_UP_TO_DATE:
            ret = MergeStatus.ALREADY_UP_TO_DATE;
            break;
         case FAILED:
            ret = MergeStatus.FAILED;
            break;
         case MERGED:
            ret = MergeStatus.MERGED;
            break;
         case MERGED_SQUASHED:
            ret = MergeStatus.MERGED_SQUASHED;
            break;
         case MERGED_SQUASHED_NOT_COMMITTED:
            ret = MergeStatus.MERGED_SQUASHED_NOT_COMMITTED;
            break;
         case CONFLICTING:
            ret = MergeStatus.CONFLICTING;
            break;
         case ABORTED:
            ret = MergeStatus.ABORTED;
            break;
         case MERGED_NOT_COMMITTED:
            ret = MergeStatus.MERGED_NOT_COMMITTED;
            break;
         case NOT_SUPPORTED:
            ret = MergeStatus.NOT_SUPPORTED;
            break;
         case CHECKOUT_CONFLICT:
            ret = MergeStatus.CHECKOUT_CONFLICT;
            break;
         default:
            throw new IllegalStateException(String.format("Unknown '%s' status", status.name()));
      }

      return ret;
   }

   /**
    * Applies finalization operation to a document in open-mode.
    *
    * @param document document to instrument
    * @param finalize finalize operation
    *
    * @return new document copy with finalization operation set
    */
   private Document finalizeOpenDocument(Document document, Consumer<OutputStream> finalize) {
      return DocumentBuilder
            .from(document)
            .content(new OutputStreamDocumentContentBuilder()
                           .content(document.content().outputStreamContent(),
                                    document.content().contentEncoding().orElse(null))
                           .finalize(finalize)
                           .build())
            .build();
   }

   /**
    * Generates {@link CredentialsProvider} chain for configured authentication and security options.
    *
    * @param authentication authentication configuration
    * @param transport transport configuration
    *
    * @return credentials provider chain
    *
    * @implNote UsernamePasswordCredentialsProvider does not support null password, replace it with a
    *       fake "".
    */
   private static CredentialsProvider credentialsProvider(Authentication authentication,
                                                          Transport transport) {
      List<CredentialsProvider> credentialsProvider = list();

      if (authentication != null) {
         credentialsProvider.add(new UsernamePasswordCredentialsProvider(authentication.username(),
                                                                         authentication
                                                                               .password()
                                                                               .orElse("")));
         // authentication.clearSensitiveData();
      }

      credentialsProvider.add(new NetRCCredentialsProvider());

      if (transport.disableSslVerify()) {
         credentialsProvider.add(skipPromptCredentialsProvider("SSL verification"));
      }

      if (transport.allowUnknownSshHosts()) {
         credentialsProvider.add(skipPromptCredentialsProvider(null /* FIXME find a matching text */));
      }

      return new ChainingCredentialsProvider(credentialsProvider.toArray(new CredentialsProvider[0]));
   }

}
