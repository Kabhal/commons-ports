/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.git;

import static com.tinubu.commons.ddd2.invariant.Validate.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotAbsolute;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.ports.document.git.GitRepositoryUri.Parameters.BASE_PATH;
import static com.tinubu.commons.ports.document.git.GitRepositoryUri.Parameters.REPOSITORY_PATH;

import java.net.URI;
import java.nio.file.Path;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.domain.type.Id;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.uri.ComponentUri;
import com.tinubu.commons.ddd2.uri.ComponentUri.Query;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ddd2.uri.parts.KeyValueQuery;
import com.tinubu.commons.ddd2.uri.parts.SimplePath;
import com.tinubu.commons.ddd2.uri.uris.FileUri;
import com.tinubu.commons.ddd2.uri.uris.HttpUri;
import com.tinubu.commons.ddd2.uri.uris.SshUri;
import com.tinubu.commons.lang.util.PathUtils;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.ExportUriOptions;
import com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery;

/**
 * GIT URI document URI. This URI must be used to identify a document, use
 * {@link GitRepositoryUri} to identify a repository.
 * <p>
 * You can use this class when a {@link GitRepositoryUri} is required, but your current URI is a document
 * URI. In this case it's important that the base path is set, or it will be set to {@code ""}.
 * <p>
 * Base path is read from {@code basePath} URI query string parameter, or from specified
 * {@code defaultBasePath} parameter. Document id is read from {@code documentId} parameter.
 * <p>
 * Supported URI format :
 * <ul>
 *    <li>Remote URI : {@code git://[user[:password]]@<host>[:<port>]</repository-path[.git]>[/<base-path>]/<document-id>[?<parameters>][#<branch>]}</li>
 *    <li>Remote URI : {@code http://[user[:password]]@<host>[:<port>]</repository-path[.git]>[/<base-path>]/<document-id>[?<parameters>][#<branch>]}</li>
 *    <li>Remote URI : {@code https://[user[:password]]@<host>[:<port>]</repository-path[.git]>[/<base-path>]/<document-id>[?<parameters>][#<branch>]}</li>
 *    <li>Remote URI : {@code ssh://[user[:password]]@<host>[:<port>]</repository-path[.git]>[/<base-path>]/<document-id>[?<parameters>][#<branch>]}</li>
 *    <li>Remote URI : {@code file:</repository-path[.git]>[/<base-path>]/<document-id>[?<parameters>][#<branch>]}: local filesystem path</li>
 *    <li>Remote URI : {@code </repository-path[.git]>[/<base-path>]/<document-id>[?<parameters>][#<branch>]}: local filesystem path</li>
 *    <li>Local URI : {@code file:</repository-path[.git]>[/<base-path>]/<document-id>?forceLocalUri[&<git-parameters>][&<local-fs-parameters>][#<branch>]}: local filesystem path</li>
 *    <li>Local URI : {@code </repository-path[.git]>[/<base-path>]/<document-id>?forceLocalUri[&<git-parameters>][&<local-fs-parameters>][#<branch>]}: local filesystem path</li>
 *    <li>Commons-ports wrapped URI : {@code doc[ument]:git:<git-uri>}</li>
 * </ul>
 * <p>
 * Supported URI parameters :
 * <ul>
 * <li>{@code documentId} (Path) : relative document path in repository</li>
 * <li>{@link GitRepositoryUri} parameters</li>
 * </ul>
 *
 * @see GitRepositoryUri
 * @see GitDocumentConfig
 */
public class GitDocumentUri extends GitRepositoryUri implements DocumentUri {

   protected GitDocumentUri(Uri uri) {
      super(uri, true);
   }

   @Override
   @SuppressWarnings("unchecked")
   protected Fields<? extends GitDocumentUri> defineDomainFields() {
      return Fields
            .<GitDocumentUri>builder()
            .superFields((Fields<GitDocumentUri>) super.defineDomainFields())
            .field("documentId", v -> v.documentId, isNotNull())
            .build();
   }

   /**
    * Creates a document URI from a repository URI and a document id.
    *
    * @param repositoryUri GIT repository URI
    * @param documentId document id
    * @param query optional query for extra parameters (overrides repository URI query parameters if
    *       any)
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static GitDocumentUri of(GitRepositoryUri repositoryUri, DocumentPath documentId, Query query) {
      Check.notNull(repositoryUri, "repositoryUri");
      Check.notNull(documentId, "documentId");

      var gitUri = setupGitUri(repositoryUri
                                     .gitUri()
                                     .component()
                                     .conditionalPath(__ -> SimplePath.of(repositoryUri
                                                                                .repositoryPath()
                                                                                .resolve(repositoryUri.basePath())
                                                                                .resolve(documentId.value()))),
                               repositoryUri.path(false).map(Path::of).orElse(null),
                               query);

      return buildUri(() -> new GitDocumentUri(gitUri));
   }

   /**
    * @implNote We preserve {@code repositoryPath} parameter if specified by user, otherwise value is
    *       forced to remote URI path, only if repository path can't be auto-detected.
    */
   private static ComponentUri setupGitUri(ComponentUri gitUri, Path repositoryPath, Query query) {
      return mapQuery(gitUri, (gu, gq) -> {
         Map<String, List<String>> parameters = map(LinkedHashMap::new);

         nullable(repositoryPath)
               .filter(rp -> !PathUtils.contains(rp,
                                                 Path.of(".git"),
                                                 (p1, p2) -> p1.toString().endsWith(p2.toString())))
               .ifPresent(rp -> parameters.put(REPOSITORY_PATH.key(), list(rp.toString())));
         parameters.putAll(KeyValueQuery.of(gq).query());
         if (query != null) {
            parameters.putAll(KeyValueQuery.of(query).query());
         }

         return gu.component().query(guq -> ((ParameterizedQuery) guq).setParameters(false, parameters));
      });
   }

   /**
    * Creates a document URI from {@link Uri}.
    *
    * @param uri document URI
    * @param defaultBasePath default base path to use if {@code basePath} query
    *       parameter not present
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static GitDocumentUri ofDocumentUri(Uri uri, Path defaultBasePath) {
      Check.notNull(uri, "uri");
      var normalizedDefaultBasePath = Check.validate(nullable(defaultBasePath, Path::normalize),
                                                     "defaultBasePath",
                                                     isNotAbsolute().andValue(hasNoTraversal()));
      validateUriCompatibility(uri,
                               "uri",
                               isCompatibleUriType(GitUri.class, HttpUri.class, SshUri.class, FileUri.class));

      return buildUri(() -> new GitDocumentUri(mapQuery(gitUri(uri), (u, q) -> {
         q.registerParameter(BASE_PATH.defaultValue(normalizedDefaultBasePath));
         return u;
      })));
   }

   /**
    * Creates a document URI from {@link Uri}.
    * <p>
    * Default base path is set to {@code ""}.
    *
    * @param uri document URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static GitDocumentUri ofDocumentUri(Uri uri) {
      Check.notNull(uri, "uri");

      return ofDocumentUri(uri, defaultBasePath(uri, true));
   }

   /**
    * Creates a document URI from {@link URI}.
    *
    * @param uri document URI
    * @param defaultBasePath default base path to use if {@code basePath} query
    *       parameter not present
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static GitDocumentUri ofDocumentUri(URI uri, Path defaultBasePath) {
      Check.notNull(uri, "uri");
      Check.notNull(defaultBasePath, "defaultBasePath");

      return ofDocumentUri(Uri.ofUri(uri), defaultBasePath);
   }

   /**
    * Creates a document URI from {@link URI}.
    * <p>
    * Default base path is set to {@code ""}.
    *
    * @param uri document URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static GitDocumentUri ofDocumentUri(URI uri) {
      Check.notNull(uri, "uri");

      return ofDocumentUri(Uri.ofUri(uri));
   }

   /**
    * Creates a document URI from URI string.
    * URI syntax is checked as part of this operation.
    *
    * @param uri document URI
    * @param defaultBasePath default base path to use if {@code basePath} query
    *       parameter not present
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static GitDocumentUri ofDocumentUri(String uri, Path defaultBasePath) {
      Check.notNull(uri, "uri");
      Check.notNull(defaultBasePath, "defaultBasePath");

      return ofDocumentUri(Uri.ofUri(uri), defaultBasePath);
   }

   /**
    * Creates a document URI from URI string.
    * URI syntax is checked as part of this operation.
    * <p>
    * Default base path is set to {@code ""}.
    *
    * @param uri document URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static GitDocumentUri ofDocumentUri(String uri) {
      Check.notNull(uri, "uri");

      return ofDocumentUri(Uri.ofUri(uri));
   }

   /**
    * Creates a document URI from configuration.
    *
    * @param config repository configuration
    * @param dynamicLocalRepository optional local repository overriding configuration, or {@code null}.
    *       Value is required if both {@link GitDocumentConfig#localUri()} and
    *       {@link GitDocumentConfig#localRepository()} are unset so that a temporary local repository is
    *       auto-generated.
    * @param dynamicRemote optional remote overriding configuration, or {@code null}
    * @param dynamicRemoteUri optional remote URI overriding configuration, or {@code null}
    * @param dynamicBranch optional branch overriding configuration, or {@code null}
    * @param documentId document id
    *
    * @return new instance
    */
   public static GitDocumentUri ofConfig(GitDocumentConfig config,
                                         DocumentRepository dynamicLocalRepository,
                                         String dynamicRemote,
                                         URI dynamicRemoteUri,
                                         String dynamicBranch,
                                         DocumentPath documentId) {
      validate(config, "config", isNotNull())
            .and(validate(dynamicBranch, "dynamicRemote", isNull().orValue(isNotBlank())))
            .and(validate(dynamicBranch, "dynamicBranch", isNull().orValue(isNotBlank())))
            .orThrow();

      return buildUri(() -> new GitDocumentUri(gitUri(config,
                                                      dynamicLocalRepository,
                                                      dynamicRemote,
                                                      dynamicRemoteUri,
                                                      dynamicBranch,
                                                      documentId)));
   }

   /**
    * Creates a document URI from {@link DocumentUri}.
    *
    * @param uri document URI
    * @param defaultBasePath default base path to use if {@code basePath} query
    *       parameter not present
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    * @implSpec this method is package-private because user code should always create
    *       {@link DocumentUri} directly from a URI.
    */
   static GitDocumentUri ofDocumentUri(DocumentUri uri, Path defaultBasePath) {
      Check.notNull(uri, "uri");
      var normalizedDefaultBasePath = Check.validate(nullable(defaultBasePath, Path::normalize),
                                                     "defaultBasePath",
                                                     isNotAbsolute().andValue(hasNoTraversal()));

      validateUriCompatibility(uri, "uri", isCompatibleDocumentUriType(GitDocumentUri.class));

      if (uri instanceof GitRepositoryUri) {
         return buildUri(() -> new GitDocumentUri(mapQuery(gitUri(uri), (u, q) -> {
            q.registerParameter(REPOSITORY_PATH.defaultValue(((GitRepositoryUri) uri).repositoryPath()));
            q.registerParameter(BASE_PATH.defaultValue(((GitRepositoryUri) uri).basePath()));
            return u;
         })));
      } else {
         return buildUri(() -> new GitDocumentUri(mapQuery(gitUri(uri), (u, q) -> {
            q.registerParameter(BASE_PATH.defaultValue(normalizedDefaultBasePath));
            return u;
         })));
      }
   }

   /**
    * Creates a document URI from {@link DocumentUri}.
    * <p>
    * Default base path is set to {@code ""}.
    *
    * @param uri document URI
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    * @implSpec this method is package-private because user code should always create
    *       {@link DocumentUri} directly from a URI.
    */
   static GitDocumentUri ofDocumentUri(DocumentUri uri) {
      Check.notNull(uri, "uri");

      return ofDocumentUri(uri, defaultBasePath(uri, true));
   }

   public DocumentPath documentId() {
      return documentId;
   }

   @Override
   public DocumentUri exportUri(ExportUriOptions options) {
      return DocumentUri.ofDocumentUri(exportURI(options));
   }

   @Override
   public GitDocumentUri normalize() {
      return buildUri(() -> new GitDocumentUri(gitUri().normalize()));
   }

   @Override
   public GitDocumentUri resolve(Uri uri) {
      return buildUri(() -> new GitDocumentUri(gitUri().resolve(uri)));
   }

   @Override
   public GitDocumentUri resolve(URI uri) {
      return buildUri(() -> new GitDocumentUri(gitUri().resolve(uri)));
   }

   @Override
   public GitDocumentUri relativize(Uri uri) {
      throw new UnsupportedOperationException();
   }

   @Override
   public GitDocumentUri relativize(URI uri) {
      throw new UnsupportedOperationException();
   }

   @Override
   public int compareTo(Id o) {
      if (!(o instanceof GitDocumentUri)) {
         return -1;
      } else {
         return super.compareTo(o);
      }
   }

}
