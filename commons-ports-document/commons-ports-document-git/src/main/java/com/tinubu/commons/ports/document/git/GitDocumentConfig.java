/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.git;

import static com.tinubu.commons.ddd2.domain.type.SensitiveValue.Validate.Check;
import static com.tinubu.commons.ddd2.domain.type.support.DomainObjectSupport.checkInvariants;
import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.MessageValue.parameter;
import static com.tinubu.commons.ddd2.invariant.MessageValue.propertyNameMapper;
import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObject;
import static com.tinubu.commons.ddd2.invariant.MessageValue.validationResult;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.lazyValue;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.satisfies;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.satisfiesValue;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isMutuallyExclusive;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.property;
import static com.tinubu.commons.ddd2.invariant.rules.ClassRules.isInstanceOf;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isGreaterThan;
import static com.tinubu.commons.ddd2.invariant.rules.EqualsRules.isEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotAbsolute;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.ddd2.invariant.rules.UriRules.fragment;
import static com.tinubu.commons.ddd2.invariant.rules.UriRules.hasNoFragment;
import static com.tinubu.commons.ddd2.invariant.rules.UriRules.hasNoPassword;
import static com.tinubu.commons.ddd2.invariant.rules.UriRules.hasNoUserInfo;
import static com.tinubu.commons.ddd2.invariant.rules.UriRules.password;
import static com.tinubu.commons.ddd2.invariant.rules.UriRules.username;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.ports.document.git.Constants.DEFAULT_BASE_PATH;
import static com.tinubu.commons.ports.document.git.Constants.DEFAULT_BRANCH;
import static com.tinubu.commons.ports.document.git.Constants.DEFAULT_CLONE_IF_MISSING;
import static com.tinubu.commons.ports.document.git.Constants.DEFAULT_INIT_IF_MISSING;
import static com.tinubu.commons.ports.document.git.Constants.DEFAULT_REMOTE;
import static com.tinubu.commons.ports.document.git.FastForward.FAST_FORWARD;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.time.Duration;
import java.util.Optional;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.domain.type.SensitiveValue;
import com.tinubu.commons.ddd2.domain.type.support.SensitiveValueSupport;
import com.tinubu.commons.ddd2.invariant.Invariant;
import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.MessageValue;
import com.tinubu.commons.ddd2.invariant.ParameterValue;
import com.tinubu.commons.ddd2.invariant.formatter.FastStringFormat;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.valueformatter.HiddenValueFormatter;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.beans.Setter;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.fs.FsDocumentRepository;
import com.tinubu.commons.ports.document.fs.FsRepositoryUri;
import com.tinubu.commons.ports.document.fs.storagestrategy.DirectFsStorageStrategy;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.PullStrategy.PullStrategyBuilder;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.PushStrategy.PushStrategyBuilder;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.Transport.TransportBuilder;
import com.tinubu.commons.ports.document.git.gitops.GitUtils;

/**
 * GIT document repository configuration.
 */
public class GitDocumentConfig extends AbstractValue {

   private final FsDocumentRepository localRepository;
   private final URI localUri;
   private final URI remoteUri;
   private final boolean forceLocalUri;
   private final Authentication remoteAuthentication;
   private final Path basePath;
   private final String remote;
   private final String initialBranch;
   private final String branch;
   private final boolean cloneIfMissing;
   private final boolean initIfMissing;
   private final PullStrategy pullStrategy;
   private final PushStrategy pushStrategy;
   private final Transport transport;

   public GitDocumentConfig(GitDocumentConfigBuilder builder) {
      this.localRepository = builder.localRepository;
      this.localUri = builder.localUri;
      this.remoteUri = stripBranchFromRepositoryUri(builder.remoteUri);
      this.forceLocalUri = builder.forceLocalUri;
      this.remoteAuthentication = builder.remoteAuthentication;
      this.basePath = nullable(builder.basePath, DEFAULT_BASE_PATH).normalize();
      this.remote = nullable(builder.remote, DEFAULT_REMOTE);
      this.initialBranch = nullable(builder.initialBranch).orElse(DEFAULT_BRANCH);
      this.branch = nullable(builder.remoteUri)
            .flatMap(GitDocumentConfig::branchFromRepositoryUri)
            .orElseGet(() -> builder.branch);
      this.cloneIfMissing = nullable(builder.cloneIfMissing, DEFAULT_CLONE_IF_MISSING);
      this.initIfMissing = nullable(builder.initIfMissing, DEFAULT_INIT_IF_MISSING);
      this.pullStrategy = nullable(builder.pullStrategy, new PullStrategyBuilder().build());
      this.pushStrategy = nullable(builder.pushStrategy, new PushStrategyBuilder().build());
      this.transport = nullable(builder.transport, new TransportBuilder().build());
   }

   @Override
   public Fields<? extends GitDocumentConfig> defineDomainFields() {
      return Fields
            .<GitDocumentConfig>builder()
            .field("localRepository",
                   v -> v.localRepository,
                   isNull().orValue(isMutuallyExclusive(value(localUri, "localUri")).andValue(property(
                         FsDocumentRepository::storageStrategy,
                         "storageStrategy",
                         isInstanceOf(value(DirectFsStorageStrategy.class))))))
            .field("localUri",
                   v -> v.localUri,
                   isNull().orValue(isMutuallyExclusive(value(localRepository, "localRepository")).andValue(
                         isFsUri())))
            .field("remoteUri",
                   v -> v.remoteUri,
                   isNull().orValue(isGitCompatibleUri().andValue(hasMatchingAuthentication(value(
                         remoteAuthentication,
                         "authentication")).ifNonNull(remoteAuthentication))))
            .field("forceLocalUri", v -> v.forceLocalUri)
            .field("remoteAuthentication", v -> v.remoteAuthentication)
            .field("basePath", v -> v.basePath, isNotAbsolute().andValue(hasNoTraversal()))
            .field("remote", v -> v.remote, isNotBlank())
            .field("initialBranch", v -> v.initialBranch, isNotBlank())
            .field("branch", v -> v.branch, isNull().orValue(isNotBlank()))
            .field("createRepositoryIfMissing", v -> v.cloneIfMissing)
            .field("initIfMissing", v -> v.initIfMissing)
            .field("pullStrategy", v -> v.pullStrategy, isNotNull())
            .field("pushStrategy", v -> v.pushStrategy, isNotNull())
            .field("transport", v -> v.transport, isNotNull())
            .build();
   }

   /**
    * Local-filesystem GIT repository.
    * <p>
    * Specified repository must use {@link DirectFsStorageStrategy} storage strategy.
    * {@link FsDocumentRepository#storagePath()} always point to GIT repository root for GIT commands, while
    * optional {@link FsDocumentRepository#basePath()} is used to resolve documents in repository, and also
    * for some document-related GIT commands.
    *
    * <p>
    * You can either define local repository as an existing {@link #localRepository()}, or as a
    * {@link #localUri()}. If both are unset, a temporary directory will be created, and automatically
    * deleted on {@link GitDocumentRepository#close()}.
    * <p>
    * You have the responsibility to close the provided {@link DocumentRepository}.
    */
   public Optional<FsDocumentRepository> localRepository() {
      return nullable(localRepository);
   }

   /**
    * Local-filesystem GIT repository URI.
    * <p>
    * You can either define local repository as an existing {@link #localRepository()}, or as a
    * {@link #localUri()}. If both are unset, a temporary directory will be created, and automatically
    * deleted on {@link GitDocumentRepository#close()}.
    * <p>
    * A {@link DocumentRepository} will be automatically created from parameterized URI, and closed on
    * {@link GitDocumentRepository#close()}.
    */
   public Optional<URI> localUri() {
      return nullable(localUri);
   }

   /**
    * Optional GIT remote URI.
    * URI can contain authentication in userinfo part, that take priority on {@link #remoteAuthentication()},
    * however, authentication should be set in {@link #remoteAuthentication()} to better control sensitive
    * information leakage in logs etc...
    */
   public Optional<URI> remoteUri() {
      return nullable(remoteUri);
   }

   /**
    * Whether to force use of local URI when exporting URI's and both {@link #localUri()} and
    * {@link #remoteUri()} are set.
    */
   public boolean forceLocalUri() {
      return forceLocalUri;
   }

   /**
    * GIT remote authentication information if any.
    * {@link #remoteUri()} can contain authentication in userinfo part, that take priority on
    * these information, however, authentication should be set here to better control sensitive
    * information leakage in logs etc...
    *
    * @return remote authentication information or {@link Optional#empty()} if no authentication required
    */
   public Optional<Authentication> remoteAuthentication() {
      return nullable(remoteAuthentication);
   }

   /**
    * Optional base path relative to repository for all documents. This path must be relative. Set to
    * {@code ""} by default.
    * If omitted, or set to {@code ""} documents will be accessed directly from repository root path.
    * When {@link #cloneIfMissing()} or {@link #initIfMissing()} is set, base path won't be created.
    */
   public Path basePath() {
      return basePath;
   }

   /**
    * Remote name used for initial GIT configuration and GIT operations. Default to
    * {@link Constants#DEFAULT_REMOTE}
    */
   public String remote() {
      return remote;
   }

   /**
    * Initial branch name used when initializing a GIT repository, either to initialize a GIT repository with
    * an initial branch, or clone remote repository. Default to {@link Constants#DEFAULT_BRANCH}.
    * This value is totally independent from {@link #branch()} and/or {@link #remoteUri()}'s fragment part.
    */
   public String initialBranch() {
      return initialBranch;
   }

   /**
    * Optional branch name to work on. Default to current repository's branch, or cloned repository's HEAD
    * (default branch).
    * If a branch is set in {@link #remoteUri()}'s fragment part, it will take precedence over this value.
    */
   public Optional<String> branch() {
      return nullable(branch);
   }

   /**
    * Whether to automatically clone an uninitialized GIT repository. Default to {@code true} if
    * {@link #remoteUri()} is set.
    */
   public boolean cloneIfMissing() {
      return cloneIfMissing;
   }

   /**
    * Whether to automatically initialize an uninitialized GIT repository. Default to {@code true}.
    */
   public boolean initIfMissing() {
      return initIfMissing;
   }

   /**
    * GIT pull strategy configuration.
    *
    * @return GIT pull strategy configuration
    */
   public PullStrategy pullStrategy() {
      return pullStrategy;
   }

   /**
    * GIT push strategy configuration.
    *
    * @return GIT push strategy configuration
    */
   public PushStrategy pushStrategy() {
      return pushStrategy;
   }

   /**
    * GIT server transport configuration.
    *
    * @return GIT server transport configuration
    */
   public Transport transport() {
      return transport;
   }

   private static Optional<String> branchFromRepositoryUri(URI remoteUri) {
      validate(remoteUri, "remoteUri", isNotNull()).orThrow();

      return nullable(remoteUri.getFragment());
   }

   private static URI stripBranchFromRepositoryUri(URI remoteUri) {
      if (remoteUri == null) {
         return null;
      }
      try {
         return new URI(remoteUri.getScheme(), remoteUri.getSchemeSpecificPart(), null);
      } catch (URISyntaxException e) {
         throw new IllegalStateException(e);
      }
   }

   private InvariantRule<URI> isGitCompatibleUri() {
      return satisfiesValue(GitUtils::isCompatibleUri, "'%s' GIT URI must be compatible", validatingObject());
   }

   private InvariantRule<URI> hasMatchingAuthentication(ParameterValue<Authentication> authentication) {
      InvariantRule<URI> matchUsername =
            hasNoUserInfo().orValue(username(isEqualTo(lazyValue(() -> authentication
                                                             .requireNonNullValue()
                                                             .username()),
                                                       "'%s' must match configured '%s'",
                                                       validatingObject(),
                                                       parameter(authentication,
                                                                 Authentication::username,
                                                                 propertyNameMapper("username")))));
      InvariantRule<URI> matchPassword =
            hasNoPassword().orValue(password(isEqualTo(lazyValue(() -> authentication
                                                             .requireNonNullValue()
                                                             .password()
                                                             .orElse(null)),
                                                       "'%s' must match configured '%s'",
                                                       validatingObject(),
                                                       parameter(authentication,
                                                                 Authentication::password,
                                                                 propertyNameMapper("password")))));
      return matchUsername.andValue(matchPassword).ifNonNull(authentication);
   }

   @Deprecated
   private InvariantRule<URI> hasMatchingBranch(ParameterValue<String> branch) {
      return hasNoFragment().orValue(fragment(isEqualTo(lazyValue(branch::requireNonNullValue),
                                                        "'%s' must match configured '%s'",
                                                        validatingObject(),
                                                        parameter(branch))));
   }

   private InvariantRule<URI> isFsUri() {
      return satisfies(uri -> {
                          try {
                             FsRepositoryUri.ofRepositoryUri(uri.value());
                          } catch (InvalidUriException e) {
                             uri.addValidationResults(e);
                             return false;
                          }
                          return true;
                       },
                       FastStringFormat.of("'",
                                           validatingObject(),
                                           "' URI must be filesystem document repository URI : ",
                                           validationResult(0, Exception::getMessage)));
   }

   /**
    * GIT authentication information.
    * <p>
    * Known platforms authentication :
    * <ul>
    *    <li>GitHub : username={@code token}, password={@code <token>}</li>
    *    <li>Gitlab : username={@code token}, password={@code <token>}</li>
    *    <li>Gitlab : username={@code PRIVATE-TOKEN}, password={@code <token>}</li>
    *    <li>Gitlab : username={@code oauth2}, password={@code <token>}</li>
    * </ul>
    * <p>
    * Note that authentication information is also supported from repository URI user infos.
    */
   public static class Authentication extends AbstractValue implements SensitiveValue {
      private SensitiveValueSupport sensitiveValueSupport = new SensitiveValueSupport();
      /** GIT user name. */
      private String username;
      /** GIT user password or {@code null}. */
      private char[] password;

      private Authentication(String username, char[] password) {
         this.username = username;
         this.password = password;
      }

      public static Authentication githubToken(char[] token) {
         return of("token", token);
      }

      public static Authentication githubToken(String token) {
         return of("token", token);
      }

      public static Authentication gitlabToken(char[] token) {
         return of("token", token);
      }

      public static Authentication gitlabToken(String token) {
         return of("token", token);
      }

      public static Authentication of(String username, char[] password) {
         return checkInvariants(new Authentication(username, password));
      }

      public static Authentication of(String username) {
         return of(username, (char[]) null);
      }

      public static Authentication of(String username, String password) {
         return of(username, nullable(password).map(String::toCharArray).orElse(null));
      }

      @Override
      public Fields<? extends Authentication> defineDomainFields() {
         return Fields
               .<Authentication>builder()
               .field("username", v -> v.username, new HiddenValueFormatter(), isNotBlank())
               .field("password", v -> v.password, new HiddenValueFormatter())
               .build();
      }

      @Getter
      public String username() {
         Check.notCleared(this);
         return username;
      }

      @Getter
      public Optional<String> password() {
         Check.notCleared(this);
         return nullable(password).map(String::new);
      }

      @Override
      public void clearSensitiveValues() {
         username = null;
         password = sensitiveValueSupport.clearSensitiveValue(password);
      }

      @Override
      public boolean clearedSensitiveValues() {
         return sensitiveValueSupport.clearedSomeSensitiveValues();
      }
   }

   /**
    * GIT pull strategy configuration.
    */
   public static class PullStrategy extends AbstractValue {
      private static final boolean DEFAULT_REBASE = false;
      private static final FastForward DEFAULT_FAST_FORWARD = FAST_FORWARD;
      private static final PullMode DEFAULT_UPDATE_MODE = PullMode.NEVER;

      private final boolean rebase;
      private final FastForward fastForward;
      private final PullMode pullMode;
      private final Integer shallowDepth;

      public enum PullMode {
         /** Pull the repository once at document repository initialization. */
         ON_CREATE,
         /** Pull the repository before each document repository operation call. */
         ALWAYS,
         /** Never pull the repository. */
         NEVER;
      }

      private PullStrategy(PullStrategyBuilder builder) {
         this.rebase = nullable(builder.rebase, DEFAULT_REBASE);
         this.fastForward = nullable(builder.fastForward, DEFAULT_FAST_FORWARD);
         this.pullMode = nullable(builder.pullMode, DEFAULT_UPDATE_MODE);
         this.shallowDepth = builder.shallowDepth;
      }

      @Override
      public Fields<? extends PullStrategy> defineDomainFields() {
         return Fields
               .<PullStrategy>builder()
               .field("rebase", v -> v.rebase)
               .field("fastForward", v -> v.fastForward, isNotNull())
               .field("pullMode", v -> v.pullMode, isNotNull())
               .field("shallowDepth", v -> v.shallowDepth, isNull().orValue(isGreaterThan(value(0))))
               .build();
      }

      /**
       * Rebase when merging. Default to {@value #DEFAULT_REBASE}.
       */
      @Getter
      public boolean rebase() {
         return rebase;
      }

      /**
       * Fast-forward when merging. Default to {@link #DEFAULT_FAST_FORWARD}
       */
      @Getter
      public FastForward fastForward() {
         return fastForward;
      }

      /**
       * Pull strategy when using document repository. By default, repository is never updated.
       */
      @Getter
      public PullMode pullMode() {
         return pullMode;
      }

      /**
       * Shallow fetch depth. Can be {@link Optional#empty()} if fetch is not shallow, otherwise, strictly
       * positive number of commits to fetch.
       *
       * @return shallow fetch depth
       */
      public Optional<Integer> shallowDepth() {
         return nullable(shallowDepth);
      }

      public static class PullStrategyBuilder extends DomainBuilder<PullStrategy> {
         private Boolean rebase;
         private FastForward fastForward;
         private PullMode pullMode;
         private Integer shallowDepth;

         public static PullStrategy never() {
            return new PullStrategyBuilder().pullMode(PullMode.NEVER).build();
         }

         public static PullStrategy always() {
            return new PullStrategyBuilder().pullMode(PullMode.ALWAYS).build();
         }

         public static PullStrategy onCreate() {
            return new PullStrategyBuilder().pullMode(PullMode.ON_CREATE).build();
         }

         public Boolean rebase() {
            return rebase;
         }

         @Setter
         public PullStrategyBuilder rebase(Boolean rebase) {
            this.rebase = rebase;
            return this;
         }

         public FastForward fastForward() {
            return fastForward;
         }

         @Setter
         public PullStrategyBuilder fastForward(FastForward fastForward) {
            this.fastForward = fastForward;
            return this;
         }

         public PullMode pullMode() {
            return pullMode;
         }

         @Setter
         public PullStrategyBuilder pullMode(PullMode pullMode) {
            this.pullMode = pullMode;
            return this;
         }

         public Integer shallowDepth() {
            return shallowDepth;
         }

         @Setter
         public PullStrategyBuilder shallowDepth(Integer shallowDepth) {
            this.shallowDepth = shallowDepth;
            return this;
         }

         @Override
         protected PullStrategy buildDomainObject() {
            return new PullStrategy(this);
         }
      }
   }

   /**
    * GIT push strategy configuration.
    */
   public static class PushStrategy extends AbstractValue {
      private static final PushMode DEFAULT_COMMIT_MODE = PushMode.NEVER;
      private static final PushMode DEFAULT_PUSH_MODE = PushMode.NEVER;

      private final PushMode commitMode;
      private final PushMode pushMode;

      public enum PushMode {
         /** Never commit/push. */
         NEVER,
         /** Commit/push the repository after each document repository operation call. */
         ALWAYS,
         /** Commit/push the repository once on repository close. */
         ON_CLOSE
      }

      private PushStrategy(PushStrategyBuilder builder) {
         this.commitMode = nullable(builder.commitMode, DEFAULT_COMMIT_MODE);
         this.pushMode = nullable(builder.pushMode, DEFAULT_PUSH_MODE);
      }

      @Override
      public Fields<? extends PushStrategy> defineDomainFields() {
         return Fields
               .<PushStrategy>builder()
               .field("commitMode", v -> v.commitMode, isNotNull())
               .field("pushMode", v -> v.pushMode, isNotNull())
               .invariants(Invariant.of(() -> this, areCompatibleModes()))
               .build();
      }

      private InvariantRule<PushStrategy> areCompatibleModes() {
         return satisfiesValue(strategy -> {
                                  switch (pushMode) {
                                     case NEVER:
                                        return true;
                                     case ALWAYS:
                                        return commitMode == PushMode.ALWAYS;
                                     case ON_CLOSE:
                                        return commitMode == PushMode.ALWAYS || commitMode == PushMode.ON_CLOSE;
                                     default:
                                        throw new IllegalStateException();
                                  }
                               },
                               "'%s' commit mode must be compatible with '%s' push mode",
                               MessageValue.value(commitMode),
                               MessageValue.value(pushMode));
      }

      /**
       * Commit strategy when using document repository. By default, repository is never committed.
       */
      @Getter
      public PushMode commitMode() {
         return commitMode;
      }

      /**
       * Push strategy when using document repository. By default, repository is never pushed.
       */
      @Getter
      public PushMode pushMode() {
         return pushMode;
      }

      public static class PushStrategyBuilder extends DomainBuilder<PushStrategy> {
         private PushMode commitMode;
         private PushMode pushMode;

         public static PushStrategy never() {
            return new PushStrategyBuilder().pushMode(PushMode.NEVER).build();
         }

         public static PushStrategy always() {
            return new PushStrategyBuilder().pushMode(PushMode.ALWAYS).build();
         }

         public static PushStrategy onClose() {
            return new PushStrategyBuilder().pushMode(PushMode.ON_CLOSE).build();
         }

         @Getter
         public PushMode commitMode() {
            return commitMode;
         }

         @Setter
         public PushStrategyBuilder commitMode(PushMode commitMode) {
            this.commitMode = commitMode;
            return this;
         }

         @Getter
         public PushMode pushMode() {
            return pushMode;
         }

         @Setter
         public PushStrategyBuilder pushMode(PushMode pushMode) {
            this.pushMode = pushMode;
            if (commitMode == null) {
               commitMode = pushMode;
            }
            return this;
         }

         @Override
         protected PushStrategy buildDomainObject() {
            return new PushStrategy(this);
         }
      }
   }

   /**
    * GIT server transport configuration.
    */
   public static class Transport extends AbstractValue {
      private static final Duration DEFAULT_CONNECT_TIMEOUT = Duration.ofSeconds(10);
      private static final boolean DEFAULT_DISABLE_SSL_VERIFY = false;
      private static final boolean DEFAULT_ALLOW_UNKNOWN_SSH_HOSTS = false;

      private final Duration connectTimeout;
      private final boolean disableSslVerify;
      private final boolean allowUnknownSshHosts;

      private Transport(TransportBuilder builder) {
         this.connectTimeout = nullable(builder.connectTimeout, DEFAULT_CONNECT_TIMEOUT);
         this.disableSslVerify = nullable(builder.disableSslVerify, DEFAULT_DISABLE_SSL_VERIFY);
         this.allowUnknownSshHosts = nullable(builder.allowUnknownSshHosts, DEFAULT_ALLOW_UNKNOWN_SSH_HOSTS);
      }

      @Override
      public Fields<? extends Transport> defineDomainFields() {
         return Fields
               .<Transport>builder()
               .field("connectTimeout", v -> v.connectTimeout, isNotNull())
               .field("disableSslVerify", v -> v.disableSslVerify)
               .field("allowUnknownSshHosts", v -> v.allowUnknownSshHosts)
               .build();
      }

      /**
       * GIT server connection timeout.
       *
       * @return GIT server connection timeout
       */
      @Getter
      public Duration connectTimeout() {
         return connectTimeout;
      }

      /**
       * Disables SSL certificate verification (auto-signed certificates, ...).
       *
       * @return {@code true} if SSL certificate verification is disabled
       */
      @Getter
      public boolean disableSslVerify() {
         return disableSslVerify;
      }

      /**
       * Allows unknown SSH hosts.
       *
       * @return {@code true} if unknown SSH hosts are allowed
       */
      @Getter
      public boolean allowUnknownSshHosts() {
         return allowUnknownSshHosts;
      }

      public static class TransportBuilder extends DomainBuilder<Transport> {
         private Duration connectTimeout;
         private Boolean disableSslVerify;
         private Boolean allowUnknownSshHosts;

         @Getter
         public Duration connectTimeout() {
            return connectTimeout;
         }

         @Setter
         public TransportBuilder connectTimeout(Duration connectTimeout) {
            this.connectTimeout = connectTimeout;
            return this;
         }

         @Getter
         public Boolean disableSslVerify() {
            return disableSslVerify;
         }

         @Setter
         public TransportBuilder disableSslVerify(Boolean disableSslVerify) {
            this.disableSslVerify = disableSslVerify;
            return this;
         }

         @Getter
         public Boolean allowUnknownSshHosts() {
            return allowUnknownSshHosts;
         }

         @Setter
         public TransportBuilder allowUnknownSshHosts(Boolean allowUnknownSshHosts) {
            this.allowUnknownSshHosts = allowUnknownSshHosts;
            return this;
         }

         @Override
         protected Transport buildDomainObject() {
            return new Transport(this);
         }
      }

   }

   public static class GitDocumentConfigBuilder extends DomainBuilder<GitDocumentConfig> {

      private FsDocumentRepository localRepository;
      private URI localUri;
      private URI remoteUri;
      private boolean forceLocalUri;
      private Authentication remoteAuthentication;
      private Path basePath;
      private String remote;
      private String initialBranch;
      private String branch;
      private Boolean cloneIfMissing;
      private Boolean initIfMissing;
      private PullStrategy pullStrategy;
      private PushStrategy pushStrategy;
      private Transport transport;

      public static GitDocumentConfigBuilder from(GitDocumentConfig config) {
         return new GitDocumentConfigBuilder()
               .localRepository(config.localRepository)
               .localUri(config.localUri)
               .remoteUri(config.remoteUri)
               .forceLocalUri(config.forceLocalUri)
               .remoteAuthentication(config.remoteAuthentication)
               .basePath(config.basePath)
               .remote(config.remote)
               .branch(config.initialBranch)
               .branch(config.branch)
               .cloneIfMissing(config.cloneIfMissing)
               .initIfMissing(config.initIfMissing)
               .pullStrategy(config.pullStrategy)
               .pushStrategy(config.pushStrategy)
               .transport(config.transport);
      }

      @Getter
      public Optional<FsDocumentRepository> localRepository() {
         return nullable(localRepository);
      }

      @Setter
      public GitDocumentConfigBuilder localRepository(FsDocumentRepository localRepository) {
         this.localRepository = localRepository;
         return this;
      }

      @Getter
      public Optional<URI> localUri() {
         return nullable(localUri);
      }

      @Setter
      public GitDocumentConfigBuilder localUri(URI localUri) {
         this.localUri = localUri;
         return this;
      }

      @Getter
      public Optional<URI> remoteUri() {
         return nullable(remoteUri);
      }

      @Setter
      public GitDocumentConfigBuilder remoteUri(URI remoteUri) {
         this.remoteUri = remoteUri;
         return this;
      }

      @Getter
      public boolean forceLocalUri() {
         return forceLocalUri;
      }

      @Setter
      public GitDocumentConfigBuilder forceLocalUri(boolean forceLocalUri) {
         this.forceLocalUri = forceLocalUri;
         return this;
      }

      @Getter
      public Optional<Authentication> remoteAuthentication() {
         return nullable(remoteAuthentication);
      }

      @Setter
      public GitDocumentConfigBuilder remoteAuthentication(Authentication remoteAuthentication) {
         this.remoteAuthentication = remoteAuthentication;
         return this;
      }

      @Setter
      public GitDocumentConfigBuilder basePath(Path basePath) {
         this.basePath = basePath;
         return this;
      }

      @Getter
      public Path basePath() {
         return basePath;
      }

      @Getter
      public Optional<String> remote() {
         return nullable(remote);
      }

      @Setter
      public GitDocumentConfigBuilder remote(String remote) {
         this.remote = remote;
         return this;
      }

      @Getter
      public Optional<String> initialBranch() {
         return nullable(initialBranch);
      }

      @Setter
      public GitDocumentConfigBuilder initialBranch(String initialBranch) {
         this.initialBranch = initialBranch;
         return this;
      }

      @Getter
      public Optional<String> branch() {
         return nullable(branch);
      }

      @Setter
      public GitDocumentConfigBuilder branch(String branch) {
         this.branch = branch;
         return this;
      }

      @Getter
      public Optional<Boolean> cloneIfMissing() {
         return nullable(cloneIfMissing);
      }

      @Setter
      public GitDocumentConfigBuilder cloneIfMissing(Boolean cloneIfMissing) {
         this.cloneIfMissing = cloneIfMissing;
         return this;
      }

      @Getter
      public Optional<Boolean> initIfMissing() {
         return nullable(initIfMissing);
      }

      @Setter
      public GitDocumentConfigBuilder initIfMissing(Boolean initIfMissing) {
         this.initIfMissing = initIfMissing;
         return this;
      }

      @Getter
      public Optional<PullStrategy> pullStrategy() {
         return nullable(pullStrategy);
      }

      @Setter
      public GitDocumentConfigBuilder pullStrategy(PullStrategy pullStrategy) {
         this.pullStrategy = pullStrategy;
         return this;
      }

      @Getter
      public Optional<PushStrategy> pushStrategy() {
         return nullable(pushStrategy);
      }

      @Setter
      public GitDocumentConfigBuilder pushStrategy(PushStrategy pushStrategy) {
         this.pushStrategy = pushStrategy;
         return this;
      }

      @Getter
      public Optional<Transport> transport() {
         return nullable(transport);
      }

      @Setter
      public GitDocumentConfigBuilder transport(Transport transport) {
         this.transport = transport;
         return this;
      }

      @Override
      protected GitDocumentConfig buildDomainObject() {
         return new GitDocumentConfig(this);
      }
   }
}
