/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.git;

import java.util.function.BiPredicate;

import com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions;
import com.tinubu.commons.ports.document.domain.uri.ExportUriOptions;
import com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery;
import com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery.Parameter;

/**
 * GIT repository specific options to export URI operations.
 */
public class GitUriOptions extends DefaultExportUriOptions {

   private final boolean forceLocalUri;

   protected GitUriOptions(ExportUriOptions uriOptions, boolean forceLocalUri) {
      super(uriOptions.exportParameters(),
            uriOptions.forceDefaultValues(),
            uriOptions.excludeSensitiveParameters(),
            uriOptions.filterParameters());
      this.forceLocalUri = forceLocalUri;
   }

   /**
    * Sets export URI options to export all parameters.
    *
    * @param forceLocalUri whether to force export local URI instead of remote URI if both available
    * @param excludeSensitiveParameters whether to exclude sensitive parameters
    *
    * @return export URI options
    */
   public static GitUriOptions allParameters(boolean forceLocalUri, boolean excludeSensitiveParameters) {
      return new GitUriOptions(DefaultExportUriOptions.allParameters(excludeSensitiveParameters),
                               forceLocalUri);
   }

   /**
    * Sets export URI options to export all parameters, excluding sensitive parameters.
    *
    * @param forceLocalUri whether to force export local URI instead of remote URI if both available
    *
    * @return export URI options
    */
   public static GitUriOptions allParameters(boolean forceLocalUri) {
      return new GitUriOptions(DefaultExportUriOptions.allParameters(), forceLocalUri);
   }

   /**
    * Sets export URI options to export no parameters.
    *
    * @param forceLocalUri whether to force export local URI instead of remote URI if both available
    *
    * @return export URI options
    */
   public static GitUriOptions noParameters(boolean forceLocalUri) {
      return new GitUriOptions(DefaultExportUriOptions.noParameters(), forceLocalUri);
   }

   /**
    * Sets export URI options to export the minimum set of parameters to recreate a repository with the same
    * configuration.
    * Sensitive parameters are not exported.
    * Parameters with default values are not exported.
    *
    * @param forceLocalUri whether to force export local URI instead of remote URI if both available
    *
    * @return export URI options
    */
   public static GitUriOptions defaultParameters(boolean forceLocalUri) {
      return new GitUriOptions(DefaultExportUriOptions.defaultParameters(), forceLocalUri);
   }

   /**
    * Sets export URI options to export the minimum set of parameters to recreate a repository with the same
    * configuration.
    * Sensitive parameters are not exported.
    *
    * @param forceLocalUri whether to force export local URI instead of remote URI if both available
    * @param forceDefaultValues whether to force export parameters with default values
    *
    * @return export URI options
    */
   public static GitUriOptions defaultParameters(boolean forceLocalUri, boolean forceDefaultValues) {
      return new GitUriOptions(DefaultExportUriOptions.defaultParameters(forceDefaultValues), forceLocalUri);
   }

   /**
    * Sets export URI options to export the minimum set of parameters to recreate a repository with the same
    * configuration.
    * Sensitive parameters are also exported.
    * Parameters with default values are not exported.
    *
    * @param forceLocalUri whether to force export local URI instead of remote URI if both available
    *
    * @return export URI options
    */
   public static GitUriOptions sensitiveParameters(boolean forceLocalUri) {
      return new GitUriOptions(DefaultExportUriOptions.sensitiveParameters(), forceLocalUri);
   }

   /**
    * Sets export URI options to export the minimum set of parameters to recreate a repository with the same
    * configuration.
    * Sensitive parameters are also exported.
    *
    * @param forceLocalUri whether to force export local URI instead of remote URI if both available
    * @param forceDefaultValues whether to force export parameters with default values
    *
    * @return export URI options
    */
   public static GitUriOptions sensitiveParameters(boolean forceLocalUri, boolean forceDefaultValues) {
      return new GitUriOptions(DefaultExportUriOptions.sensitiveParameters(forceDefaultValues),
                               forceLocalUri);
   }

   @Override
   public GitUriOptions exportParameters(boolean exportParameters) {
      return new GitUriOptions(super.exportParameters(exportParameters), forceLocalUri);
   }

   @Override
   public GitUriOptions forceDefaultValues(boolean forceDefaultValues) {
      return new GitUriOptions(super.forceDefaultValues(forceDefaultValues), forceLocalUri);
   }

   @Override
   public GitUriOptions excludeSensitiveParameters(boolean excludeSensitiveParameters) {
      return new GitUriOptions(super.excludeSensitiveParameters(excludeSensitiveParameters), forceLocalUri);
   }

   @Override
   public GitUriOptions filterParameters(BiPredicate<ParameterizedQuery, Parameter> filterParameters) {
      return new GitUriOptions(super.filterParameters(filterParameters), forceLocalUri);
   }

   public GitUriOptions forceLocalUri(boolean forceLocalUri) {
      return new GitUriOptions(this, forceLocalUri);
   }

   public boolean forceLocalUri() {
      return forceLocalUri;
   }
}
