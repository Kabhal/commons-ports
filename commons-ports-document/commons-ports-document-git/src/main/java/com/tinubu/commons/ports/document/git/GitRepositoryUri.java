/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.git;

import static com.tinubu.commons.ddd2.invariant.Validate.satisfies;
import static com.tinubu.commons.ddd2.invariant.Validate.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotAbsolute;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.ddd2.uri.Uri.compatibleUri;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.PathUtils.emptyPath;
import static com.tinubu.commons.lang.util.PathUtils.isEmpty;
import static com.tinubu.commons.lang.util.PathUtils.isRoot;
import static com.tinubu.commons.lang.util.PathUtils.removeStart;
import static com.tinubu.commons.lang.util.PathUtils.rootPath;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.defaultParameters;
import static com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery.ExportUriFilter.excludeParameters;
import static com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery.ExportUriFilter.isRegisteredParameter;
import static com.tinubu.commons.ports.document.domain.uri.UriUtils.exportUriFilter;
import static com.tinubu.commons.ports.document.git.Constants.DEFAULT_ALLOW_UNKNOWN_SSH_HOSTS;
import static com.tinubu.commons.ports.document.git.Constants.DEFAULT_BRANCH;
import static com.tinubu.commons.ports.document.git.Constants.DEFAULT_CLONE_IF_MISSING;
import static com.tinubu.commons.ports.document.git.Constants.DEFAULT_COMMIT_MODE;
import static com.tinubu.commons.ports.document.git.Constants.DEFAULT_CONNECT_TIMEOUT;
import static com.tinubu.commons.ports.document.git.Constants.DEFAULT_DISABLE_SSL_VERIFY;
import static com.tinubu.commons.ports.document.git.Constants.DEFAULT_FORCE_LOCAL_URI;
import static com.tinubu.commons.ports.document.git.Constants.DEFAULT_INIT_IF_MISSING;
import static com.tinubu.commons.ports.document.git.Constants.DEFAULT_PULL_FAST_FORWARD;
import static com.tinubu.commons.ports.document.git.Constants.DEFAULT_PULL_MODE;
import static com.tinubu.commons.ports.document.git.Constants.DEFAULT_PULL_REBASE;
import static com.tinubu.commons.ports.document.git.Constants.DEFAULT_PUSH_MODE;
import static com.tinubu.commons.ports.document.git.Constants.DEFAULT_REMOTE;
import static com.tinubu.commons.ports.document.git.GitRepositoryUri.Parameters.ALLOW_UNKNOWN_SSH_HOSTS;
import static com.tinubu.commons.ports.document.git.GitRepositoryUri.Parameters.BASE_PATH;
import static com.tinubu.commons.ports.document.git.GitRepositoryUri.Parameters.BRANCH;
import static com.tinubu.commons.ports.document.git.GitRepositoryUri.Parameters.CLONE_IF_MISSING;
import static com.tinubu.commons.ports.document.git.GitRepositoryUri.Parameters.COMMIT_MODE;
import static com.tinubu.commons.ports.document.git.GitRepositoryUri.Parameters.CONNECT_TIMEOUT;
import static com.tinubu.commons.ports.document.git.GitRepositoryUri.Parameters.DISABLE_SSL_VERIFY;
import static com.tinubu.commons.ports.document.git.GitRepositoryUri.Parameters.FORCE_LOCAL_URI;
import static com.tinubu.commons.ports.document.git.GitRepositoryUri.Parameters.INITIAL_BRANCH;
import static com.tinubu.commons.ports.document.git.GitRepositoryUri.Parameters.INIT_IF_MISSING;
import static com.tinubu.commons.ports.document.git.GitRepositoryUri.Parameters.LOCAL_URI;
import static com.tinubu.commons.ports.document.git.GitRepositoryUri.Parameters.PULL_FAST_FORWARD;
import static com.tinubu.commons.ports.document.git.GitRepositoryUri.Parameters.PULL_MODE;
import static com.tinubu.commons.ports.document.git.GitRepositoryUri.Parameters.PULL_REBASE;
import static com.tinubu.commons.ports.document.git.GitRepositoryUri.Parameters.PULL_SHALLOW_DEPTH;
import static com.tinubu.commons.ports.document.git.GitRepositoryUri.Parameters.PUSH_MODE;
import static com.tinubu.commons.ports.document.git.GitRepositoryUri.Parameters.REMOTE;
import static com.tinubu.commons.ports.document.git.GitRepositoryUri.Parameters.REMOTE_URI;
import static com.tinubu.commons.ports.document.git.GitRepositoryUri.Parameters.REPOSITORY_PATH;
import static com.tinubu.commons.ports.document.git.GitRepositoryUri.Parameters.allParameters;
import static com.tinubu.commons.ports.document.git.GitRepositoryUri.Parameters.valueParameters;
import static java.util.function.Function.identity;

import java.net.URI;
import java.nio.file.Path;
import java.time.Duration;
import java.util.List;
import java.util.Optional;
import java.util.function.BiPredicate;

import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.domain.type.Id;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.invariant.rules.PathRules;
import com.tinubu.commons.ddd2.invariant.rules.UriRules;
import com.tinubu.commons.ddd2.uri.ComponentUri;
import com.tinubu.commons.ddd2.uri.DefaultUriRestrictions;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ddd2.uri.UriRestrictions;
import com.tinubu.commons.ddd2.uri.parts.SimpleFragment;
import com.tinubu.commons.ddd2.uri.parts.SimplePath;
import com.tinubu.commons.ddd2.uri.parts.SimplePort;
import com.tinubu.commons.ddd2.uri.parts.UsernamePasswordUserInfo;
import com.tinubu.commons.ddd2.uri.uris.FileUri;
import com.tinubu.commons.ddd2.uri.uris.FileUri.FileUriRestrictions;
import com.tinubu.commons.ddd2.uri.uris.HttpUri;
import com.tinubu.commons.ddd2.uri.uris.SshUri;
import com.tinubu.commons.ddd2.valueformatter.HiddenValueFormatter;
import com.tinubu.commons.lang.convert.ConversionFailedException;
import com.tinubu.commons.lang.util.NullableUtils;
import com.tinubu.commons.lang.util.Pair;
import com.tinubu.commons.lang.util.PathUtils;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.uri.AbstractRepositoryUri;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.ExportUriOptions;
import com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery;
import com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery.Parameter;
import com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery.ValueParameter;
import com.tinubu.commons.ports.document.domain.uri.RepositoryUri;
import com.tinubu.commons.ports.document.fs.FsRepositoryUri;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.Authentication;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.GitDocumentConfigBuilder;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.PullStrategy.PullMode;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.PullStrategy.PullStrategyBuilder;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.PushStrategy.PushMode;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.PushStrategy.PushStrategyBuilder;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.Transport.TransportBuilder;

/**
 * GIT URI repository URI. This URI must be used to identify a GIT repository.
 * <p>
 * GIT URI can identify, either a :
 * <ul>
 *    <li><em>Remote repository/URI</em> : A remote GIT/HTTP/SSH/FS repository associated to a local FS repository, identified by a <em>Local URI</em></li>
 *    <li><em>Local repository/URI</em> : A local FS repository, optionally associated with a Remote repository</li>
 * </ul>
 * By default, GIT URIs identify remote URIs, otherwise you have to defined a {@code remoteUri} (or {@code forceLocalUri} parameter).<br/>
 * Note that a remote URI can also be a FS URI, in this case the "remote" FS repository will be cloned to "local" FS repository the same as any other remote repository.
 *
 * <p>
 * Supported URI formats :
 * <ul>
 *    <li>Remote URI : {@code git://[user[:password]]@<host>[:<port>]</repository-path[.git]>[/<base-path>][?<parameters>][#<branch>]}</li>
 *    <li>Remote URI : {@code http://[user[:password]]@<host>[:<port>]</repository-path[.git]>[/<base-path>][?<parameters>][#<branch>]}</li>
 *    <li>Remote URI : {@code https://[user[:password]]@<host>[:<port>]</repository-path[.git]>[/<base-path>][?<parameters>][#<branch>]}</li>
 *    <li>Remote URI : {@code ssh://[user[:password]]@<host>[:<port>]</repository-path[.git][/<base-path>]>[?<parameters>][#<branch>]}</li>
 *    <li>Remote URI : {@code file:</repository-path[.git]>[/<base-path>][?<parameters>][#<branch>]}: local filesystem path</li>
 *    <li>Remote URI : {@code </repository-path[.git]>[/<base-path>][?<parameters>][#<branch>]}: local filesystem path</li>
 *    <li>Local URI : {@code file:</repository-path[.git]>[/<base-path>]?forceLocalUri[&<git-parameters>][&<local-fs-parameters>][#<branch>]}: local filesystem path</li>
 *    <li>Local URI : {@code </repository-path[.git]>[/<base-path>]?forceLocalUri[&<git-parameters>][&<local-fs-parameters>][#<branch>]}: local filesystem path</li>
 *    <li>Commons-ports wrapped URI : {@code doc[ument]:git:<git-uri>}</li>
 * </ul>
 * Notes :
 * <ul>
 *    <li>If {@code remoteUri} parameter is present, the URI will be automatically considered as a <em>local URI</em>, no need to define {@code forceLocalUri}</li>
 *    <li>With <em>local URI</em> you can define both {@link GitRepositoryUri} parameters and {@link FsRepositoryUri} parameters in query</li>
 * </ul>
 * <p>
 * Supported URI parameters :
 * <ul>
 * <li>{@code repositoryPath} (Path) [&lt;automatic&gt;] : optional absolute remote repository path (mandatory if no {@code .git} in document URI), can be empty</li>
 * <li>{@code basePath} (Path) [&lt;automatic&gt;] : optional relative base path to repository, for all documents, can be empty</li>
 * <li>{@code localUri (URI)} [&lt;temporary directory&gt;]: optional local-filesystem GIT repository URI to use with remote URI. {@code localUri} can contain
 *     a <em>URL-encoded</em> query with {@link FsRepositoryUri} parameters. If {@code localUri} is not defined, a temporary directory is created automatically</li>
 * <li>{@code remoteUri (URI)} : optional remote GIT repository URI to use with local URI</li>
 * <li>{@code forceLocalUri (Boolean)} [{@value Constants#DEFAULT_FORCE_LOCAL_URI}]: optional flag to force to consider current URI as a <em>local URI</em>.
 *      Can also be used to force {@link #exportUri(ExportUriOptions) export} local URI instead of remote URI when both are available</li>
 * <li>{@code remote} (String) [{@value Constants#DEFAULT_REMOTE}]: optional remote name</li>
 * <li>{@code initialBranch} (String) [{@value Constants#DEFAULT_BRANCH}]: initial branch name to create at initialization</li>
 * <li>{@code branch} (String) [&lt;current branch, or remote default branch&gt;] : optional branch name to work in</li>
 * <li>{@code cloneIfMissing} (Boolean) [{@value Constants#DEFAULT_CLONE_IF_MISSING}] : whether to clone remote URI if local repository is uninitialized</li>
 * <li>{@code initIfMissing} (Boolean) [{@value Constants#DEFAULT_INIT_IF_MISSING}] : whether to init local repository if uninitialized</li>
 * <li>{@code pullMode} (String) [{@code NEVER}] : pull strategy pull mode (NEVER | ON_CREATE | ALWAYS)</li>
 * <li>{@code pullFastForward} (String) [{@code FAST_FORWARD}] : pull strategy use ff (FAST_FORWARD | NO_FAST_FORWARD | FAST_FORWARD_ONLY)</li>
 * <li>{@code pullRebase} (Boolean) [{@value Constants#DEFAULT_PULL_REBASE}] : pull strategy use rebase</li>
 * <li>{@code pullShallowDepth} (Integer) : pull strategy shallow fetch depth (starting from 1). No shallow fetch if unset</li>
 * <li>{@code commitMode} (String) [{@code NEVER}] : push strategy commit mode (NEVER | ON_CLOSE | ALWAYS)</li>
 * <li>{@code pushMode} (String) [{@code NEVER}] : push strategy push mode (NEVER | ON_CLOSE | ALWAYS)</li>
 * <li>{@code connectTimeout} (Duration) [{@code PT10S}] : transport - connect timeout (ISO 8601 duration format)</li>
 * <li>{@code disableSslVerify} (Boolean) [{@value Constants#DEFAULT_DISABLE_SSL_VERIFY}] : transport - disable SSL verify</li>
 * <li>{@code allowUnknownSshHosts} (Boolean) [{@value Constants#DEFAULT_ALLOW_UNKNOWN_SSH_HOSTS}] : transport - allow unknown SSH hosts</li>
 * </ul>
 *
 * @see GitDocumentConfig
 * @see GitDocumentUri
 */
// FIXME add exportUriOptions::*wrap* to force wrap exported URIs in all repo URIs
public class GitRepositoryUri extends AbstractRepositoryUri implements RepositoryUri {

   private static final String WRAPPED_URI_REPOSITORY_TYPE = "git";
   /**
    * Feature flag : whether to use {@link #parseUriPath(Uri)} to override
    * {@link GitRepositoryUri.Parameters#REPOSITORY_PATH} default value in
    * {@link #exportUri(ExportUriOptions)}. If set to {@code true}, {@code repositoryPath} parameter won't be
    * exported if it matches default behavior, depending on export options.
    */
   private static final boolean EXPORT_URI_DEFAULT_REPOSITORY_PATH_AS_DEFAULT_VALUE = true;
   /**
    * Feature flag : whether to use {@link #defaultBasePath(Uri, boolean)} to override
    * {@link GitRepositoryUri.Parameters#BASE_PATH} default value in {@link #exportUri(ExportUriOptions)}. If
    * set to {@code true}, {@code basePath} parameter won't be exported if it matches default behavior,
    * depending on export options.
    */
   private static final boolean EXPORT_URI_DEFAULT_BASE_PATH_AS_DEFAULT_VALUE = true;

   protected final URI localUri;
   protected final URI remoteUri;
   protected final Authentication remoteAuthentication;
   protected final Path repositoryPath;
   protected final Path basePath;
   protected final String branch;
   protected final DocumentPath documentId;

   protected GitRepositoryUri(Uri uri, boolean documentUri) {
      super(uri, documentUri);

      try {
         var parsedUri = parseUri(documentUri);
         this.localUri = parsedUri.localUri();
         this.remoteUri = parsedUri.remoteUri();
         this.remoteAuthentication = parsedUri.remoteAuthentication().orElse(null);
         this.basePath = parsedUri.basePath();
         this.repositoryPath = parsedUri.repositoryPath();
         this.documentId = parsedUri.documentId().orElse(null);
         this.branch = parsedUri.branch().orElse(null);
      } catch (ConversionFailedException e) {
         throw new InvalidUriException(uri, e).subMessage(e);
      }
   }

   @Override
   @SuppressWarnings("unchecked")
   protected Fields<? extends GitRepositoryUri> defineDomainFields() {
      return Fields
            .<GitRepositoryUri>builder()
            .superFields((Fields<GitRepositoryUri>) super.defineDomainFields())
            .field("localUri", v -> v.localUri)
            .field("remoteUri",
                   v -> v.remoteUri,
                   isNull().orValue(UriRules.hasNoQuery().andValue(UriRules.hasNoFragment())))
            .field("remoteAuthentication", v -> v.remoteAuthentication, new HiddenValueFormatter())
            .field("repositoryPath",
                   v -> v.repositoryPath,
                   PathRules.isEmpty().orValue(PathRules.isAbsolute().andValue(hasNoTraversal())))
            .field("basePath", v -> v.basePath, PathRules.isNotAbsolute().andValue(hasNoTraversal()))
            .field("branch", v -> v.branch, isNull().orValue(isNotBlank()))
            .build();
   }

   /**
    * Creates a repository URI from {@link Uri}. Specified URI must represent a "repository" URI.
    *
    * @param uri repository URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static GitRepositoryUri ofRepositoryUri(Uri uri) {
      Check.notNull(uri, "uri");

      validateUriCompatibility(uri,
                               "uri",
                               isCompatibleUriType(GitUri.class, HttpUri.class, SshUri.class, FileUri.class));

      return buildUri(() -> new GitRepositoryUri(gitUri(uri), false));
   }

   /**
    * Creates a repository URI from {@link URI}. Specified URI must represent a "repository" URI.
    *
    * @param uri repository URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static GitRepositoryUri ofRepositoryUri(URI uri) {
      Check.notNull(uri, "uri");

      return ofRepositoryUri(Uri.ofUri(uri));
   }

   /**
    * Creates a repository URI from URI string. Specified URI must represent a "repository" URI.
    *
    * @param uri repository URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static GitRepositoryUri ofRepositoryUri(String uri) {
      Check.notNull(uri, "uri");

      return ofRepositoryUri(Uri.ofUri(uri));
   }

   /**
    * Creates a repository URI from configuration.
    *
    * @param config repository configuration
    * @param dynamicLocalRepository optional local repository overriding configuration, or {@code null}.
    *       Value is required if both {@link GitDocumentConfig#localUri()} and
    *       {@link GitDocumentConfig#localRepository()} are unset so that a temporary local repository is
    *       auto-generated.
    * @param dynamicRemote optional remote overriding configuration, or {@code null}
    * @param dynamicRemoteUri optional remote URI overriding configuration, or {@code null}
    * @param dynamicBranch optional branch overriding configuration, or {@code null}
    *
    * @return new instance
    */
   public static GitRepositoryUri ofConfig(GitDocumentConfig config,
                                           DocumentRepository dynamicLocalRepository,
                                           String dynamicRemote,
                                           URI dynamicRemoteUri,
                                           String dynamicBranch) {
      validate(config, "config", isNotNull())
            .and(validate(dynamicRemote, "dynamicRemote", isNull().orValue(isNotBlank())))
            .and(validate(dynamicBranch, "dynamicBranch", isNull().orValue(isNotBlank())))
            .orThrow();

      return buildUri(() -> new GitRepositoryUri(gitUri(config,
                                                        dynamicLocalRepository,
                                                        dynamicRemote,
                                                        dynamicRemoteUri,
                                                        dynamicBranch,
                                                        null), false));
   }

   /**
    * Creates a repository URI from {@link RepositoryUri}. Specified repository URI can can be a
    * {@link RepositoryUri} to represent a "repository" URI, or a {@link DocumentUri} to represent a
    * "document" URI.
    *
    * @param uri repository URI
    * @param defaultBasePath default base path to use if {@code basePath} query
    *       parameter not present
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    * @implSpec this method is package-private because user code should always create
    *       {@link RepositoryUri} directly from a URI.
    */
   static GitRepositoryUri ofRepositoryUri(RepositoryUri uri, Path defaultBasePath) {
      Check.notNull(uri, "uri");
      var normalizedDefaultBasePath = Check.validate(nullable(defaultBasePath, Path::normalize),
                                                     "defaultBasePath",
                                                     isNotAbsolute().andValue(hasNoTraversal()));

      validateUriCompatibility(uri, "uri", isCompatibleRepositoryUriType(GitRepositoryUri.class));

      if (uri instanceof GitRepositoryUri) {
         return buildUri(() -> new GitRepositoryUri(mapQuery(gitUri(uri), (u, q) -> {
            q.registerParameter(REPOSITORY_PATH.defaultValue(((GitRepositoryUri) uri).repositoryPath()));
            q.registerParameter(BASE_PATH.defaultValue(((GitRepositoryUri) uri).basePath()));
            return u;
         }), uri instanceof DocumentUri));
      } else {
         return buildUri(() -> new GitRepositoryUri(mapQuery(gitUri(uri), (u, q) -> {
            q.registerParameter(BASE_PATH.defaultValue(normalizedDefaultBasePath));
            return u;
         }), uri instanceof DocumentUri));
      }
   }

   /**
    * Creates a repository URI from {@link RepositoryUri}. Specified repository URI can can be a
    * {@link RepositoryUri} to represent a "repository" URI, or a {@link DocumentUri} to represent a
    * "document" URI.
    * <p>
    * Default base path is set to {@code ""}.
    *
    * @param uri repository URI
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    * @implSpec this method is package-private because user code should always create
    *       {@link RepositoryUri} directly from a URI.
    * @implNote Can't call {@link #ofRepositoryUri(RepositoryUri, Path)} because defaultBasePath can't
    *       be computed on wrapped URIs.
    */
   static GitRepositoryUri ofRepositoryUri(RepositoryUri uri) {
      Check.notNull(uri, "uri");

      validateUriCompatibility(uri, "uri", isCompatibleRepositoryUriType(GitRepositoryUri.class));

      if (uri instanceof GitRepositoryUri) {
         return buildUri(() -> new GitRepositoryUri(mapQuery(gitUri(uri), (u, q) -> {
            q.registerParameter(REPOSITORY_PATH.defaultValue(((GitRepositoryUri) uri).repositoryPath()));
            q.registerParameter(BASE_PATH.defaultValue(((GitRepositoryUri) uri).basePath()));
            return u;
         }), uri instanceof DocumentUri));
      } else {
         return buildUri(() -> new GitRepositoryUri(mapQuery(gitUri(uri), (u, q) -> {
            // FIXME is it really needed ? what about other URIs in other repos ?
            var defaultRepositoryAndBasePath = defaultRepositoryAndBasePath(u, false);
            defaultRepositoryAndBasePath
                  .getLeft()
                  .ifPresent(defaultRepositoryPath -> q.registerParameter(REPOSITORY_PATH.defaultValue(
                        defaultRepositoryPath)));
            q.registerParameter(BASE_PATH.defaultValue(defaultRepositoryAndBasePath.getRight()));
            return u;
         }), uri instanceof DocumentUri));
      }
   }

   public ComponentUri gitUri() {
      return (ComponentUri) uri;
   }

   public Optional<URI> localUri() {
      return nullable(localUri);
   }

   public Optional<URI> remoteUri() {
      return nullable(remoteUri);
   }

   public Optional<Authentication> remoteAuthentication() {
      return nullable(remoteAuthentication);
   }

   public Path basePath() {
      return basePath;
   }

   public Path repositoryPath() {
      return repositoryPath;
   }

   public Optional<String> branch() {
      return nullable(branch);
   }

   @Override
   public GitRepositoryUri normalize() {
      return buildUri(() -> new GitRepositoryUri(uri().normalize(), documentUri));
   }

   @Override
   public GitRepositoryUri resolve(Uri uri) {
      return buildUri(() -> new GitRepositoryUri(uri().resolve(uri), documentUri));
   }

   @Override
   public GitRepositoryUri resolve(URI uri) {
      return buildUri(() -> new GitRepositoryUri(uri().resolve(uri), documentUri));
   }

   @Override
   public GitRepositoryUri relativize(Uri uri) {
      throw new UnsupportedOperationException();
   }

   @Override
   public GitRepositoryUri relativize(URI uri) {
      throw new UnsupportedOperationException();
   }

   @Override
   public int compareTo(Id o) {
      if (!(o instanceof GitRepositoryUri)) {
         return -1;
      } else if (o == this) {
         return 0;
      } else {
         return uri().compareTo(((GitRepositoryUri) o).uri());
      }
   }

   /**
    * Returns a configuration from this repository URI.
    */
   public GitDocumentConfigBuilder toConfig() {
      var pullStrategy = new PullStrategyBuilder()
            .<PullStrategyBuilder>reconstitute()
            .<PullStrategyBuilder, PullMode>optionalChain(query().parameter(PULL_MODE, true),
                                                          PullStrategyBuilder::pullMode)
            .<PullStrategyBuilder, FastForward>optionalChain(query().parameter(PULL_FAST_FORWARD, true),
                                                             PullStrategyBuilder::fastForward)
            .<PullStrategyBuilder, Boolean>optionalChain(query().parameter(PULL_REBASE, true),
                                                         PullStrategyBuilder::rebase)
            .<PullStrategyBuilder, Integer>optionalChain(query().parameter(PULL_SHALLOW_DEPTH, true),
                                                         PullStrategyBuilder::shallowDepth)
            .build();

      var pushStrategy = new PushStrategyBuilder()
            .<PushStrategyBuilder>reconstitute()
            .<PushStrategyBuilder, PushMode>optionalChain(query().parameter(COMMIT_MODE, true),
                                                          PushStrategyBuilder::commitMode)
            .<PushStrategyBuilder, PushMode>optionalChain(query().parameter(PUSH_MODE, true),
                                                          PushStrategyBuilder::pushMode)
            .build();

      var transport = new TransportBuilder()
            .<TransportBuilder, Duration>optionalChain(query().parameter(CONNECT_TIMEOUT, true),
                                                       TransportBuilder::connectTimeout)
            .<TransportBuilder, Boolean>optionalChain(query().parameter(DISABLE_SSL_VERIFY, true),
                                                      TransportBuilder::disableSslVerify)
            .<TransportBuilder, Boolean>optionalChain(query().parameter(ALLOW_UNKNOWN_SSH_HOSTS, true),
                                                      TransportBuilder::allowUnknownSshHosts)
            .build();

      return new GitDocumentConfigBuilder()
            .<GitDocumentConfigBuilder>reconstitute()
            .basePath(basePath)
            .localUri(localUri)
            .remoteUri(remoteUri)
            .<GitDocumentConfigBuilder, String>optionalChain(query().parameter(REMOTE, true),
                                                             GitDocumentConfigBuilder::remote)
            .<GitDocumentConfigBuilder, String>optionalChain(query().parameter(INITIAL_BRANCH, true),
                                                             GitDocumentConfigBuilder::initialBranch)
            .branch(branch)
            .<GitDocumentConfigBuilder, Boolean>optionalChain(query().parameter(CLONE_IF_MISSING, true),
                                                              GitDocumentConfigBuilder::cloneIfMissing)
            .<GitDocumentConfigBuilder, Boolean>optionalChain(query().parameter(INIT_IF_MISSING, true),
                                                              GitDocumentConfigBuilder::initIfMissing)
            .<GitDocumentConfigBuilder, Authentication>optionalChain(nullable(remoteAuthentication),
                                                                     GitDocumentConfigBuilder::remoteAuthentication)
            .pullStrategy(pullStrategy)
            .pushStrategy(pushStrategy)
            .transport(transport);
   }

   /**
    * {@inheritDoc}
    * <p>
    * Specified URI can have a branch specified in fragment, if so, it must match.
    */
   @Override
   public boolean supportsUri(RepositoryUri uri) {
      Check.notNull(uri, "uri");

      return supportsRepositoryUri(uri, GitRepositoryUri.class, GitRepositoryUri::ofRepositoryUri)
            .map(gitUri -> {

               if (!gitUri.remoteUri().equals(remoteUri())) {
                  return false;
               }
               if (!gitUri.basePath().equals(basePath())) {
                  return false;
               }
               if (!gitUri.branch().equals(branch())) {
                  return false;
               }

               return mapQuery(gitUri(),
                               (u, q) -> q.includeParameters(query(gitUri.gitUri()),
                                                             isRegisteredParameter().and(excludeParameters(
                                                                   REMOTE_URI,
                                                                   FORCE_LOCAL_URI,
                                                                   REPOSITORY_PATH,
                                                                   BRANCH))));
            })
            .orElse(false);
   }

   @Override
   public URI exportURI(ExportUriOptions options) {
      Check.notNull(options, "options");

      var filter = exportUriFilter(options, excludeParameters(BRANCH));
      var exportQuery = exportQuery(filter).noQueryIfEmpty(true);
      var exportUri = gitUri().component().query(__ -> exportQuery).component().fragment(f -> {
         f = nullable(f, nullable(branch).map(SimpleFragment::of).orElse(null));

         if (options.forceDefaultValues() || f != null) {
            return f;
         } else {
            return null;
         }
      });

      if (options.excludeSensitiveParameters()) {
         exportUri = exportUri.component().conditionalUserInfo(__ -> null);
      }

      return exportUri.toURI();
   }

   private ParameterizedQuery exportQuery(BiPredicate<ParameterizedQuery, Parameter> filter) {
      var exportQuery = query();

      if (EXPORT_URI_DEFAULT_REPOSITORY_PATH_AS_DEFAULT_VALUE
          || EXPORT_URI_DEFAULT_BASE_PATH_AS_DEFAULT_VALUE) {
         var defaultRepositoryAndBasePath = defaultRepositoryAndBasePath(gitUri(), documentUri);

         if (EXPORT_URI_DEFAULT_REPOSITORY_PATH_AS_DEFAULT_VALUE) {
            if (defaultRepositoryAndBasePath.getLeft().isPresent()) {
               var repositoryPathParameter =
                     REPOSITORY_PATH.defaultValue(defaultRepositoryAndBasePath.getLeft().get());
               exportQuery = exportQuery.registerParameter(ValueParameter.ofValue(repositoryPathParameter,
                                                                                  this.repositoryPath));
            }
         }

         if (EXPORT_URI_DEFAULT_BASE_PATH_AS_DEFAULT_VALUE) {
            var basePathParameter = BASE_PATH.defaultValue(defaultRepositoryAndBasePath.getRight());
            exportQuery =
                  exportQuery.registerParameter(ValueParameter.ofValue(basePathParameter, this.basePath));
         }

      }

      return exportQuery.exportQuery(filter);
   }

   @Override
   public RepositoryUri exportUri(ExportUriOptions options) {
      return RepositoryUri.ofRepositoryUri(exportURI(options));
   }

   /**
    * Creates a parameterized {@link Uri} from specified {@link Uri}.
    * <p>
    * If specified URI is wrapped, it is unwrapped before creating parameterized URI.
    * Specified URI is normalized and checked for remaining traversals, after unwrapping.
    *
    * @param uri URI
    *
    * @return new parameterized URI, with all registered parameters
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   protected static ComponentUri gitUri(Uri uri) {
      Check.notNull(uri, "uri");

      var unwrappedUri = unwrapUri(uri, WRAPPED_URI_REPOSITORY_TYPE);

      return parameterizedQueryUri(unwrappedUri,
                                   GitRepositoryUri.defaultRestrictions(),
                                   GitRepositoryUri::gitUri,
                                   allParameters());
   }

   protected static ComponentUri gitUri(Uri uri, UriRestrictions restrictions) {
      return Uri
            .<ComponentUri>compatibleUri(uri, restrictions, GitUri::ofUri)
            .or(() -> compatibleUri(uri, restrictions, HttpUri::ofUri))
            .or(() -> compatibleUri(uri, restrictions, SshUri::ofUri))
            .or(() -> compatibleUri(uri,
                                    FileUriRestrictions.of(restrictions).relativeFormat(false),
                                    FileUri::ofUri))
            .orElseThrow(__ -> new IncompatibleUriException(uri).subMessage(
                  "'uri=%s' must be compatible with GitUri, HttpUri, SshUri or FileUri",
                  uri.value()));
   }

   /**
    * Creates a parameterized URI from specified configuration.
    *
    * @param config repository configuration
    * @param dynamicLocalRepository optional local repository overriding configuration, or {@code null}.
    *       Value is required if both {@link GitDocumentConfig#localUri()} and
    *       {@link GitDocumentConfig#localRepository()} are unset so that a temporary local repository is
    *       auto-generated.
    * @param dynamicRemote optional remote overriding configuration, or {@code null}
    * @param dynamicRemoteUri optional remote URI overriding configuration, or {@code null}
    * @param dynamicBranch optional branch overriding configuration, or {@code null}
    * @param documentId optional document id, or {@code null}
    *
    * @return new parameterized URI, with all registered value parameters from configuration
    *
    * @implSpec dynamicBranch is always exported in fragment and excluded from query parameters
    */
   protected static Uri gitUri(GitDocumentConfig config,
                               DocumentRepository dynamicLocalRepository,
                               String dynamicRemote,
                               URI dynamicRemoteUri,
                               String dynamicBranch,
                               DocumentPath documentId) {
      validate(config, "config", isNotNull())
            .and(validate(dynamicBranch, "dynamicRemote", isNull().orValue(isNotBlank())))
            .and(validate(dynamicBranch, "dynamicBranch", isNull().orValue(isNotBlank())))
            .orThrow();

      var uriMode = uriMode(config, dynamicRemoteUri);
      ComponentUri uri;

      if (uriMode == UriMode.REMOTE) {
         var parameterizedQuery = ParameterizedQuery
               .ofEmpty()
               .noQueryIfEmpty(true)
               .registerParameters(valueParameters(config,
                                                   uriMode,
                                                   dynamicLocalRepository,
                                                   dynamicRemote,
                                                   dynamicRemoteUri,
                                                   dynamicBranch));
         var query = parameterizedQuery.exportQuery(excludeParameters(BRANCH, REMOTE_URI));

         var resolveRemoteUri = nullable(dynamicRemoteUri).or(config::remoteUri).orElseThrow();

         uri = ComponentUri
               .ofUri(resolveRemoteUri)
               .component()
               .conditionalUserInfo(__ -> uriUserInfo(resolveRemoteUri, config).orElse(null))
               .component()
               .conditionalPort(__ -> SimplePort.of(resolveRemoteUri).orElse(null))
               .component()
               .conditionalQuery(__ -> query);
      } else {
         var resolveRemoteUri = nullable(dynamicRemoteUri).or(() -> {
            return config
                  .remoteUri()
                  .map(configRemoteUri -> ComponentUri
                        .ofUri(configRemoteUri)
                        .component()
                        .conditionalUserInfo(__ -> uriUserInfo(configRemoteUri, config).orElse(null)))
                  .map(Uri::toURI);
         }).orElse(null);
         var resolvedLocalUri = repositoryUri(dynamicLocalRepository)
               .or(config::localUri)
               .or(() -> config.localRepository().flatMap(GitRepositoryUri::repositoryUri))
               .orElseThrow(() -> new IllegalStateException("'dynamicLocalRepository' must be defined"));

         var parameterizedQuery = ((Optional<ParameterizedQuery>) ParameterizedQuery.of(resolvedLocalUri))
               .orElseGet(ParameterizedQuery::ofEmpty)
               .noQueryIfEmpty(true)
               .registerParameters(valueParameters(config,
                                                   uriMode,
                                                   dynamicLocalRepository,
                                                   dynamicRemote,
                                                   resolveRemoteUri,
                                                   dynamicBranch));
         if (resolveRemoteUri == null) {
            parameterizedQuery.registerParameters(ValueParameter.ofValue(FORCE_LOCAL_URI, true));
         }

         var query = parameterizedQuery.exportQuery(excludeParameters(BRANCH, LOCAL_URI));

         uri = ComponentUri.ofUri(resolvedLocalUri).component().conditionalQuery(__ -> query);
      }

      var resolvedBranch = nullable(dynamicBranch).or(config::branch);

      return gitUri(uri)
            .component()
            .conditionalPath(p -> p.resolve(config
                                                  .basePath()
                                                  .resolve(nullable(documentId)
                                                                 .map(DocumentPath::value)
                                                                 .orElse(emptyPath()))))
            .component()
            .conditionalFragment(__ -> resolvedBranch.map(SimpleFragment::of).orElse(null));
   }

   /**
    * Exports a {@link URI} from a {@link DocumentRepository}.
    *
    * @param repository optional repository, or {@code null}
    */
   private static Optional<URI> repositoryUri(DocumentRepository repository) {
      return nullable(repository).map(r -> r.toUri(defaultParameters()).toURI());
   }

   /**
    * Sets URI mode from specified configuration.
    *
    * @param config GIT configuration
    * @param remoteUri optional remoteUri overriding configuration, or {@code null}
    *
    * @return URI mode
    */
   protected static UriMode uriMode(GitDocumentConfig config, URI remoteUri) {
      if ((config.remoteUri().isEmpty() && remoteUri == null) || config.forceLocalUri()) {
         return UriMode.LOCAL;
      } else {
         return UriMode.REMOTE;
      }
   }

   protected static DefaultUriRestrictions defaultRestrictions() {
      return AbstractRepositoryUri.defaultRestrictions().fragment(false);
   }

   /**
    * Default base path to complement generic {@link DocumentUri} or {@link RepositoryUri}.
    * <p>
    * Default base path is set to {@code ""} for document URIs, or if no {@code .git} path element is found in
    * URI path. Otherwise, base path is the remaining path once {@code .git} sub path is removed.
    */
   protected static Path defaultBasePath(Uri uri, boolean documentUri) {
      if (documentUri) {
         return emptyPath();
      } else {
         return parseUriPath(uri).map(Pair::getRight).orElseGet(PathUtils::emptyPath);
      }
   }

   /**
    * Default repository path to complement generic {@link DocumentUri} or {@link RepositoryUri}.
    * <p>
    * Default repository path is set to auto-detected sub-path ending with {@code .git}. Otherwise, repository
    * path is set to the whole path for repository URIs, or {@link Optional#empty()} for document URIs.
    *
    * @return default repository path, or {@link Optional#empty()} auto-detection fails.
    */
   protected static Optional<Path> defaultRepositoryPath(Uri uri, boolean documentUri) {
      return parseUriPath(uri).map(Pair::getLeft).or(() -> {
         if (!documentUri) {
            return optional(Path.of(uri.path(false).orElseThrow()));
         } else {
            return optional();
         }
      });
   }

   /**
    * Optimized computation of both (defaultBasePath, defaultRepositoryPath).
    *
    * @return (defaultBasePath, defaultRepositoryPath)
    */
   protected static Pair<Optional<Path>, Path> defaultRepositoryAndBasePath(Uri uri, boolean documentUri) {
      var parsePath = parseUriPath(uri);

      return Pair.of(parsePath.map(Pair::getLeft).or(() -> {
         if (!documentUri) {
            return optional(Path.of(uri.path(false).orElseThrow()));
         } else {
            return optional();
         }
      }), documentUri ? emptyPath() : parsePath.map(Pair::getRight).orElseGet(PathUtils::emptyPath));
   }

   /**
    * Detects {@code .git} path element to extract repository path from specified URI.
    * If specified URI is {@code /}, repository path is set to {@code /}.
    *
    * @param uri URI to analyze
    *
    * @return (repository path, remaining path), or {@link Optional#empty()} if no repository path can be
    *       detected automatically
    */
   private static Optional<Pair<Path, Path>> parseUriPath(Uri uri) {
      var uriPath = uri.path(false).map(Path::of).orElseThrow();

      if (isRoot(uriPath)) {
         return optional(Pair.of(rootPath(), emptyPath()));
      } else if (isEmpty(uriPath)) {
         return optional(Pair.of(emptyPath(), emptyPath()));
      } else {
         var identifiedRepositoryPath =
               PathUtils.indexOf(uriPath, Path.of(".git"), (p1, p2) -> p1.toString().endsWith(p2.toString()));

         return identifiedRepositoryPath.map(rpi -> {
            var repositoryPath = rootPath().resolve(uriPath.subpath(0, rpi + 1));
            var remainingPath = removeStart(uriPath, repositoryPath).orElseThrow();

            return Pair.of(repositoryPath, remainingPath);
         });
      }
   }

   protected static class ParsedUri {
      private URI localUri;
      private URI remoteUri;
      private Authentication remoteAuthentication;
      private Path repositoryPath;
      private Path basePath;
      private DocumentPath documentId;
      private String branch;

      public ParsedUri(URI localUri,
                       URI remoteUri,
                       Authentication remoteAuthentication,
                       Path repositoryPath,
                       Path basePath,
                       DocumentPath documentId,
                       String branch) {
         this.localUri = localUri;
         this.remoteUri = remoteUri;
         this.remoteAuthentication = remoteAuthentication;
         this.repositoryPath = repositoryPath;
         this.basePath = basePath;
         this.documentId = documentId;
         this.branch = branch;
      }

      /**
       * Local URI including repository path, without base path and document id, without parameters,
       * and without fragment.
       */
      public URI localUri() {
         return localUri;
      }

      /**
       * Remote URI including repository path, without base path and document id, without parameters,
       * and without fragment.
       */
      public URI remoteUri() {
         return remoteUri;
      }

      /** Optional remote authentication extracted from remoteUri. */
      public Optional<Authentication> remoteAuthentication() {
         return nullable(remoteAuthentication);
      }

      /** Absolute repository path. */
      public Path repositoryPath() {
         return repositoryPath;
      }

      /** Relative base path, or {@code ""}. */
      public Path basePath() {
         return basePath;
      }

      /** Relative document id. */
      public Optional<DocumentPath> documentId() {
         return nullable(documentId);
      }

      /** Branch. */
      public Optional<String> branch() {
         return nullable(branch);
      }
   }

   /**
    * Checks URI syntax and parses current parameterized URI.
    *
    * @return parsed uri
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    * @implSpec The minimum set of parameters are checked here for consistency, only the parameters
    *       used in class internal state : basePath, branch. Other parameters
    *       could be checked later, e.g. in {@link #toConfig()}.
    */
   // FIXME write cleaner code using defaultRepoPathAndBasePath instead of recoding this logic here :
   protected ParsedUri parseUri(boolean documentUri) {
      Path repositoryPath;
      Path remainingPath;
      Path basePath;
      Path documentId = null;

      var repositoryPathParameter = query()
            .parameter(REPOSITORY_PATH, true)
            .map(Path::normalize)
            .map(rp -> satisfies(rp,
                                 PathRules
                                       .isEmpty()
                                       .orValue(PathRules
                                                      .isAbsolute()
                                                      .andValue(hasNoTraversal()))).orThrowMessage(message -> new InvalidUriException(
                  gitUri()).subMessage("'%s=%s' parameter must be absolute and must not contain traversal",
                                       REPOSITORY_PATH.key(),
                                       rp.toString())));
      var basePathParameter = query()
            .parameter(BASE_PATH, true)
            .map(Path::normalize)
            .map(bp -> satisfies(bp,
                                 isNotAbsolute().andValue(hasNoTraversal())).orThrowMessage(message -> new InvalidUriException(
                  gitUri()).subMessage("'%s=%s' parameter must not be absolute and must not contain traversal",
                                       BASE_PATH.key(),
                                       bp.toString())))
            .orElseThrow();
      var uriPath = Path.of(gitUri().path(false).orElseThrow());

      if (repositoryPathParameter.isPresent()) {
         repositoryPath = repositoryPathParameter.get();
         remainingPath = removeStart(uriPath,
                                     repositoryPath).orElseThrow(() -> new InvalidUriException(gitUri()).subMessage(
               "'%s=%s' parameter must start '%s' path",
               REPOSITORY_PATH.key(),
               repositoryPath.toString(),
               uriPath.toString()));
      } else {
         var defaultRepositoryPath = parseUriPath(gitUri());

         if (defaultRepositoryPath.isPresent()) {
            repositoryPath = defaultRepositoryPath.get().getLeft();
            remainingPath = defaultRepositoryPath.get().getRight();

         } else {
            if (documentUri) {
               throw new InvalidUriException(gitUri()).subMessage("Missing '%s' parameter",
                                                                  REPOSITORY_PATH.key());
            } else {
               repositoryPath = uriPath;
               remainingPath = emptyPath();
            }
         }
      }

      if (!documentUri) {
         if (!isEmpty(remainingPath)
             && query().hasParameter(BASE_PATH)
             && !PathUtils.equals(basePathParameter, remainingPath)) {
            throw new InvalidUriException(gitUri()).subMessage("'%s=%s' parameter must match '%s' path",
                                                               BASE_PATH.key(),
                                                               basePathParameter.toString(),
                                                               remainingPath.toString());
         }
         basePath = isEmpty(remainingPath) ? basePathParameter : remainingPath;
      } else {
         if (isEmpty(remainingPath)) {
            throw new InvalidUriException(gitUri()).subMessage("Missing document id");
         }

         var remainingDocumentId = removeStart(remainingPath, basePathParameter);
         if (remainingDocumentId.isEmpty() || isEmpty(remainingDocumentId.get())) {
            throw new InvalidUriException(gitUri()).subMessage(
                  "'%s=%s' parameter must start but not end with '%s' path",
                  BASE_PATH.key(),
                  basePathParameter.toString(),
                  remainingPath.toString());
         }

         basePath = basePathParameter;
         documentId = remainingDocumentId.get();
      }

      Optional<String> branchParameter = query().parameter(BRANCH, true);
      Optional<String> branchFragment = uri().fragment(false);
      if (branchFragment.isPresent()
          && branchParameter.isPresent()
          && query().hasParameter(BRANCH)
          && !branchFragment.equals(branchParameter)) {
         throw new InvalidUriException(uri()).subMessage(
               "Inconsistent branch value between URI '%s' fragment and '%s' branch parameter",
               branchFragment.get(),
               branchParameter.get());
      }

      URI localUri;
      URI remoteUri;
      Authentication remoteAuthentication;

      if (query().hasParameter(REMOTE_URI) || query()
            .parameter(FORCE_LOCAL_URI, true)
            .map(NullableUtils::nullableBoolean)
            .orElseThrow()) {

         compatibleUri(gitUri(),
                       FileUriRestrictions.of(defaultRestrictions()).relativeFormat(false),
                       FileUri::ofUri).orElseThrow(identity());

         if (query().hasParameter(LOCAL_URI)) {
            throw new InvalidUriException(gitUri()).subMessage(
                  "'localUri' parameter is not allowed in local URI");
         }

         localUri = gitUri()
               .component()
               .conditionalPath(__ -> SimplePath.of(repositoryPath))
               .component()
               .conditionalQuery(q -> ParameterizedQuery
                     .of(q)
                     .noQueryIfEmpty(true)
                     .registerParameters(allParameters())
                     .exportQuery(isRegisteredParameter().negate()))
               .component()
               .conditionalFragment(__ -> null)
               .toURI();
         var remoteUriParameter = query().parameter(REMOTE_URI, false);
         remoteUri = remoteUriParameter
               .map(uri -> ComponentUri
                     .ofUri(uri)
                     .component()
                     .conditionalUserInfo(__ -> null)
                     .component()
                     .conditionalQuery(__ -> null)
                     .component()
                     .conditionalFragment(__ -> null)
                     .toURI())
               .orElse(null);
         remoteAuthentication = remoteUriParameter
               .flatMap(UsernamePasswordUserInfo::of)
               .map(up -> Authentication.of(up.username(false), up.password(false).orElse(null)))
               .orElse(null);

      } else {
         if (query().hasParameter(REMOTE_URI)) {
            throw new InvalidUriException(gitUri()).subMessage(
                  "'remoteUri' parameter is not allowed in remote URI");
         }

         remoteUri = gitUri()
               .component()
               .conditionalPath(__ -> SimplePath.of(repositoryPath))
               .component()
               .conditionalUserInfo(__ -> null)
               .component()
               .conditionalQuery(__ -> null)
               .component()
               .conditionalFragment(__ -> null)
               .toURI();
         remoteAuthentication = gitUri()
               .component()
               .userInfo()
               .filter(ui -> ui instanceof UsernamePasswordUserInfo)
               .map(UsernamePasswordUserInfo.class::cast)
               .map(up -> Authentication.of(up.username(false), up.password(false).orElse(null)))
               .orElse(null);
         localUri =
               query().parameter(LOCAL_URI, false).map(uri -> ComponentUri.ofUri(uri).toURI()).orElse(null);
      }

      return new ParsedUri(localUri,
                           remoteUri,
                           remoteAuthentication,
                           repositoryPath,
                           basePath,
                           nullable(documentId).map(DocumentPath::of).orElse(null),
                           branchFragment.or(() -> branchParameter).orElse(null));
   }

   private static Optional<UsernamePasswordUserInfo> uriUserInfo(URI remoteUri, GitDocumentConfig config) {

      return nullable(remoteUri.getRawUserInfo()).map(UsernamePasswordUserInfo::ofEncoded).or(() -> {
         return config
               .remoteAuthentication()
               .map(remoteAuthentication -> UsernamePasswordUserInfo.of(remoteAuthentication.username(),
                                                                        remoteAuthentication
                                                                              .password()
                                                                              .orElse(null)));
      });
   }

   protected enum UriMode {
      /** Whether URI represents a local URI. */
      LOCAL,
      /** Whether URI represents a remote URI. */
      REMOTE
   }

   /**
    * Supported parameter registry.
    */
   static class Parameters {
      static final Parameter<Path> REPOSITORY_PATH = Parameter.registered("repositoryPath", Path.class);
      static final Parameter<Path> BASE_PATH =
            Parameter.registered("basePath", Path.class).defaultValue(emptyPath());
      static final Parameter<URI> REMOTE_URI = Parameter.registered("remoteUri", URI.class);
      static final Parameter<URI> LOCAL_URI = Parameter.registered("localUri", URI.class);
      static final Parameter<String> REMOTE =
            Parameter.registered("remote", String.class).defaultValue(DEFAULT_REMOTE);
      static final Parameter<String> INITIAL_BRANCH =
            Parameter.registered("initialBranch", String.class).defaultValue(DEFAULT_BRANCH);
      static final Parameter<String> BRANCH = Parameter.registered("branch", String.class);
      static final Parameter<Boolean> CLONE_IF_MISSING =
            Parameter.registered("cloneIfMissing", Boolean.class).defaultValue(DEFAULT_CLONE_IF_MISSING);
      static final Parameter<Boolean> INIT_IF_MISSING =
            Parameter.registered("initIfMissing", Boolean.class).defaultValue(DEFAULT_INIT_IF_MISSING);
      static final Parameter<Boolean> FORCE_LOCAL_URI =
            Parameter.registered("forceLocalUri", Boolean.class).defaultValue(DEFAULT_FORCE_LOCAL_URI);
      static final Parameter<PullMode> PULL_MODE =
            Parameter.registered("pullMode", PullMode.class).defaultValue(DEFAULT_PULL_MODE);
      static final Parameter<FastForward> PULL_FAST_FORWARD = Parameter
            .registered("pullFastForward", FastForward.class)
            .defaultValue(DEFAULT_PULL_FAST_FORWARD);
      static final Parameter<Boolean> PULL_REBASE =
            Parameter.registered("pullRebase", Boolean.class).defaultValue(DEFAULT_PULL_REBASE);
      static final Parameter<Integer> PULL_SHALLOW_DEPTH =
            Parameter.registered("pullShallowDepth", Integer.class);
      static final Parameter<PushMode> COMMIT_MODE =
            Parameter.registered("commitMode", PushMode.class).defaultValue(DEFAULT_COMMIT_MODE);
      static final Parameter<PushMode> PUSH_MODE =
            Parameter.registered("pushMode", PushMode.class).defaultValue(DEFAULT_PUSH_MODE);
      static final Parameter<Duration> CONNECT_TIMEOUT =
            Parameter.registered("connectTimeout", Duration.class).defaultValue(DEFAULT_CONNECT_TIMEOUT);
      static final Parameter<Boolean> DISABLE_SSL_VERIFY =
            Parameter.registered("disableSslVerify", Boolean.class).defaultValue(DEFAULT_DISABLE_SSL_VERIFY);
      static final Parameter<Boolean> ALLOW_UNKNOWN_SSH_HOSTS = Parameter
            .registered("allowUnknownSshHosts", Boolean.class)
            .defaultValue(DEFAULT_ALLOW_UNKNOWN_SSH_HOSTS);

      @SuppressWarnings("rawtypes")
      static List<Parameter> allParameters() {
         return list(REPOSITORY_PATH,
                     BASE_PATH,
                     REMOTE_URI,
                     LOCAL_URI,
                     REMOTE,
                     INITIAL_BRANCH,
                     BRANCH,
                     CLONE_IF_MISSING,
                     INIT_IF_MISSING,
                     FORCE_LOCAL_URI,
                     PULL_MODE,
                     PULL_FAST_FORWARD,
                     PULL_REBASE,
                     PULL_SHALLOW_DEPTH,
                     COMMIT_MODE,
                     PUSH_MODE,
                     CONNECT_TIMEOUT,
                     DISABLE_SSL_VERIFY,
                     ALLOW_UNKNOWN_SSH_HOSTS);
      }

      /**
       * @param config repository configuration
       * @param dynamicLocalRepository optional local repository overriding configuration, or
       *       {@code null}. Value is required if both {@link GitDocumentConfig#localUri()} and
       *       {@link GitDocumentConfig#localRepository()} are unset so that a temporary local repository is
       *       auto-generated.
       * @param dynamicRemote optional remote overriding configuration, or {@code null}
       * @param dynamicRemoteUri optional remote URI overriding configuration, or {@code null}
       * @param dynamicBranch optional branch overriding configuration, or {@code null}
       *
       * @implNote {@link #REPOSITORY_PATH} is forced to local/remote URI path to support repository
       *       URIs without {@code .git}. For this, {@code dynamicLocalRepository} is used, but only in
       *       {@link UriMode#LOCAL} mode.
       */
      @SuppressWarnings("rawtypes")
      static List<ValueParameter> valueParameters(GitDocumentConfig config,
                                                  UriMode uriMode,
                                                  DocumentRepository dynamicLocalRepository,
                                                  String dynamicRemote,
                                                  URI dynamicRemoteUri,
                                                  String dynamicBranch) {
         var resolvedLocalUri = config
               .localUri()
               .or(() -> config.localRepository().flatMap(GitRepositoryUri::repositoryUri))
               .orElse(null);
         var resolvedRemoteUri = nullable(dynamicRemoteUri).or(config::remoteUri).orElse(null);
         var resolvedBranch = nullable(dynamicBranch).or(config::branch).orElse(null);
         var resolvedRemote = nullable(dynamicRemote).orElseGet(config::remote);
         var resolvedRepositoryPath = nullable(uriMode == UriMode.LOCAL ? nullable(dynamicLocalRepository)
               .flatMap(GitRepositoryUri::repositoryUri)
               .orElse(resolvedLocalUri) : resolvedRemoteUri).map(URI::getPath).map(Path::of).orElse(null);

         return list(ValueParameter.ofValue(REPOSITORY_PATH, resolvedRepositoryPath),
                     ValueParameter.ofValue(BASE_PATH, config.basePath()),
                     ValueParameter.ofValue(REMOTE_URI, resolvedRemoteUri),
                     ValueParameter.ofValue(LOCAL_URI, resolvedLocalUri),
                     ValueParameter.ofValue(REMOTE, resolvedRemote),
                     ValueParameter.ofValue(INITIAL_BRANCH, config.initialBranch()),
                     ValueParameter.ofValue(BRANCH, resolvedBranch),
                     ValueParameter.ofValue(CLONE_IF_MISSING, config.cloneIfMissing()),
                     ValueParameter.ofValue(INIT_IF_MISSING, config.initIfMissing()),
                     ValueParameter.ofValue(FORCE_LOCAL_URI, config.forceLocalUri()),
                     ValueParameter.ofValue(PULL_MODE, config.pullStrategy().pullMode()),
                     ValueParameter.ofValue(PULL_FAST_FORWARD, config.pullStrategy().fastForward()),
                     ValueParameter.ofValue(PULL_REBASE, config.pullStrategy().rebase()),
                     ValueParameter.ofValue(PULL_SHALLOW_DEPTH,
                                            config.pullStrategy().shallowDepth().orElse(null)),
                     ValueParameter.ofValue(COMMIT_MODE, config.pushStrategy().commitMode()),
                     ValueParameter.ofValue(PUSH_MODE, config.pushStrategy().pushMode()),
                     ValueParameter.ofValue(CONNECT_TIMEOUT, config.transport().connectTimeout()),
                     ValueParameter.ofValue(DISABLE_SSL_VERIFY, config.transport().disableSslVerify()),
                     ValueParameter.ofValue(ALLOW_UNKNOWN_SSH_HOSTS,
                                            config.transport().allowUnknownSshHosts()));
      }

   }

}
