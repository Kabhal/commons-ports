/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.git.gitops;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static java.util.stream.Collectors.joining;
import static org.eclipse.jgit.transport.RemoteRefUpdate.Status.OK;
import static org.eclipse.jgit.transport.RemoteRefUpdate.Status.UP_TO_DATE;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRefNameException;
import org.eclipse.jgit.errors.UnsupportedCredentialItem;
import org.eclipse.jgit.internal.storage.dfs.DfsRepositoryDescription;
import org.eclipse.jgit.internal.storage.dfs.InMemoryRepository;
import org.eclipse.jgit.lib.BranchTrackingStatus;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.transport.CredentialItem;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.PushResult;
import org.eclipse.jgit.transport.RefSpec;
import org.eclipse.jgit.transport.URIish;

import com.tinubu.commons.ports.document.git.exception.GitException;
import com.tinubu.commons.ports.document.git.exception.PushFailedGitException;

public final class GitUtils {

   private GitUtils() {
   }

   /**
    * Checks if specified URI is a valid GIT repository URI.
    * <p>
    * Compatible URIs :
    * <ul>
    *    <li>{@code git://[user[:password]]@host/repository/path[.git][?<parameters>][#<branch>]}</li>
    *    <li>{@code http://[user[:password]]@host/repository/path[.git][?<parameters>][#<branch>]}</li>
    *    <li>{@code https://[user[:password]]@host/repository/path[.git][?<parameters>][#<branch>]}</li>
    *    <li>{@code ssh://[user[:password]]@host/repository/path[.git][?<parameters>][#<branch>]}</li>
    *    <li>{@code file:/repository/path[.git][?<parameters>][#<branch>]} (path must be absolute)</li>
    *    <li>{@code file:///repository/path[.git][?<parameters>][#<branch>]} (path must be absolute)</li>
    *    <li>{@code /repository/path[.git][?<parameters>][#<branch>]} (path must be absolute)</li>
    * </ul>
    *
    * @param repositoryUri GIT repository URI
    *
    * @return {@code true} if URI is GIT compatible
    */
   public static boolean isCompatibleUri(URI repositoryUri) {
      validate(repositoryUri, "repositoryUri", isNotNull()).orThrow();

      try {
         new URIish(repositoryUri.toString());
      } catch (URISyntaxException e) {
         return false;
      }

      Predicate<URI> matchRemoteAbsoluteUri =
            uri -> list("git", "http", "https", "ssh").contains(uri.getScheme()) && uri.getHost() != null;

      Predicate<URI> matchLocalFsAbsoluteUri =
            uri -> uri.getScheme().equals("file") && uri.getHost() == null && uri.getPath() != null;

      Predicate<URI> matchAbsoluteUri =
            uri -> uri.isAbsolute() && (matchRemoteAbsoluteUri.or(matchLocalFsAbsoluteUri).test(uri));

      Predicate<URI> matchNotAbsoluteUri =
            uri -> !uri.isAbsolute() && uri.getPath() != null && uri.getPath().startsWith("/");

      return matchAbsoluteUri.or(matchNotAbsoluteUri).test(repositoryUri);
   }

   /**
    * Returns port from URI, if port is not explicit, returns default port depending on URI scheme.
    *
    * @param uri repository URI
    *
    * @return URI port, never {@code null}
    *
    * @throws IllegalArgumentException if scheme is unknown
    */
   public static int defaultUriPort(URI uri) {
      int port = uri.getPort();

      if (port == -1) {
         if (!uri.isAbsolute() || uri.getScheme().equals("file")) {
            return -1;
         }

         switch (nullable(uri.getScheme(), "ssh")) {
            case "http":
               port = 80;
               break;
            case "https":
               port = 443;
               break;
            case "ssh":
               port = 22;
               break;
            case "git":
               port = 9418;
               break;
            default:
               throw new IllegalArgumentException(String.format("Unknown '%s' URI scheme", uri));

         }
      }

      return port;
   }

   /**
    * Whether URI has default port considering its scheme. If URI is not absolute or has {@code file} scheme,
    * default port is set to {@code -1}.
    *
    * @param uri repository URI
    *
    * @return {@code true} if URI is default port for scheme.
    *
    * @throws IllegalArgumentException if scheme is unknown
    */
   public static boolean isUriDefaultPort(URI uri) {
      int defaultPort = -1;

      if (uri.isAbsolute() && !uri.getScheme().equals("file")) {
         switch (nullable(uri.getScheme(), "ssh")) {
            case "http":
               defaultPort = 80;
               break;
            case "https":
               defaultPort = 443;
               break;
            case "ssh":
               defaultPort = 22;
               break;
            case "git":
               defaultPort = 9418;
               break;
            default:
               throw new IllegalArgumentException(String.format("Unknown '%s' URI scheme", uri));
         }
      }

      return uri.getPort() == defaultPort;
   }

   /**
    * Checks push results for errors.
    *
    * @param pushResults push results
    *
    * @throws PushFailedGitException if push has errors
    */
   // FIXME introduce per remote update errors in PushFailedGitException with getters
   public static void checkPushResults(Iterable<PushResult> pushResults) {
      List<String> errorMessages = list();
      stream(pushResults).forEach(pushResult -> {
         pushResult.getRemoteUpdates().forEach(pushUpdate -> {
            if (!list(OK, UP_TO_DATE).contains(pushUpdate.getStatus())) {
               errorMessages.add(String.format("Error while pushing to '%s' > %s",
                                               pushUpdate.getRemoteName(),
                                               nullable(pushUpdate.getMessage(),
                                                        "Push failed : " + pushUpdate.getStatus())));
            }
         });
      });
      if (errorMessages.size() > 0) {
         throw new PushFailedGitException(stream(errorMessages).collect(joining(" | ")));

      }
   }

   /**
    * Generates ref specification from parameters.
    *
    * @param src source ref
    * @param dst destination (local) ref
    * @param force force even if not fast-forward
    *
    * @return ref specification
    */
   public static RefSpec refSpec(String src, String dst, boolean force) {
      validate(src, "src", isNotBlank()).and(validate(dst, "dst", isNotBlank())).orThrow();

      return new RefSpec((force ? "+" : "") + "refs/" + src + ":refs/" + dst);
   }

   /**
    * Checks that specified branch exists on repository.
    *
    * @param repository GIT repository
    * @param branch branch name
    */
   public static boolean branchExists(Repository repository, String branch) throws IOException {
      validate(repository, "repository", isNotNull()).and(validate(branch, "branch", isNotBlank())).orThrow();

      return repository.exactRef("refs/heads/" + branch) != null;
   }

   /**
    * Checks if repository has no tracked files that are not committed.
    *
    * @param git GIT API
    *
    * @return {@code true} if repository is clean
    */
   public static boolean isClean(Git git) throws GitAPIException {
      validate(git, "git", isNotNull()).orThrow();

      if (git.getRepository().isBare()) {
         return true;
      } else {
         return git.status().call().isClean();
      }
   }

   /**
    * Checks if repository has commits not pushed to remote.
    *
    * @param repository GIT repository
    * @param branch branch name
    *
    * @return number of commits not pushed to remote
    */
   public static int branchAheadCount(Repository repository, String branch) throws IOException {
      validate(repository, "repository", isNotNull()).and(validate(branch, "branch", isNotBlank())).orThrow();

      if (repository.isBare()) {
         return 0;
      } else {
         BranchTrackingStatus trackingStatus = BranchTrackingStatus.of(repository, branch);
         return trackingStatus != null ? trackingStatus.getAheadCount() : 0;
      }
   }

   /**
    * Returns local file-system GIT repository, if path exists and is initialized.
    * Caller has the responsibility to close the returned repository.
    *
    * @param repositoryPath repository working tree
    *
    * @return file-system repository or {@link Optional#empty} if repository does not exist or is not
    *       initialized
    *
    * @throws GitException if an error occurs while operating GIT repository
    */
   public static Optional<Repository> fileSystemRepository(Path repositoryPath) throws IOException {
      validate(repositoryPath, "repositoryPath", isNotNull()).orThrow();

      File repositoryFile = repositoryPath.toFile();

      if (repositoryFile.exists() && !repositoryFile.isDirectory()) {
         throw new GitException(String.format("Existing '%s' repository path is not a directory",
                                              repositoryPath));
      }

      FileRepositoryBuilder repositoryBuilder =
            new FileRepositoryBuilder().addCeilingDirectory(repositoryFile);

      repositoryBuilder.findGitDir(repositoryFile);

      if (repositoryBuilder.getGitDir() != null) {
         return optional(repositoryBuilder.build());
      } else {
         return optional();
      }
   }

   /**
    * Returns always initialized local file-system GIT repository.
    * Caller has the responsibility to close the returned repository.
    *
    * @param repositoryPath repository working tree
    * @param createRepositoryIfMissing whether to create repository directory if missing
    * @param initialBranch initial branch name
    *
    * @return file-system repository
    */
   public static Repository fileSystemRepository(Path repositoryPath,
                                                 boolean createRepositoryIfMissing,
                                                 String initialBranch) throws IOException {
      validate(repositoryPath, "repositoryPath", isNotNull())
            .and(validate(initialBranch, "initialBranch", isNotBlank()))
            .orThrow();

      return fileSystemRepository(repositoryPath).orElseGet(() -> {
         if (!createRepositoryIfMissing) {
            throw new GitException(String.format("Can't initialize missing '%s' repository", repositoryPath));
         } else {
            try {
               return createFileSystemRepository(repositoryPath, initialBranch);
            } catch (IOException e) {
               throw new UncheckedIOException(e);
            }
         }
      });
   }

   /**
    * Creates a repository.
    * Caller has the responsibility to close the returned repository.
    *
    * @param repositoryPath repository working tree
    * @param initialBranch initial branch name
    *
    * @return initialized repository
    *
    * @throws GitException if an error occurs while operating GIT repository
    */
   public static Repository createFileSystemRepository(Path repositoryPath, String initialBranch)
         throws IOException {
      validate(repositoryPath, "repositoryPath", isNotNull())
            .and(validate(initialBranch, "initialBranch", isNotBlank()))
            .orThrow();

      Repository repository = null;
      try {
         repository = new FileRepositoryBuilder()
               .setWorkTree(repositoryPath.toFile())
               .setInitialBranch(initialBranch)
               .build();
      } catch (InvalidRefNameException e) {
         throw new GitException(e);
      }
      try {
         repository.create();
      } catch (IOException e) {
         repository.close();
         throw e;
      }
      return repository;
   }

   /**
    * Returns always initialized in-memory GIT repository.
    * Caller has the responsibility to close the returned repository.
    *
    * @return in-memory repository
    */
   public static Repository memoryRepository() {
      DfsRepositoryDescription repoDesc = new DfsRepositoryDescription();
      return new InMemoryRepository(repoDesc);
   }

   public static CredentialsProvider skipPromptCredentialsProvider(String containsText) {
      return new CredentialsProvider() {

         @Override
         public boolean supports(CredentialItem... items) {
            for (CredentialItem item : items) {
               if ((item instanceof CredentialItem.YesNoType)) {
                  if (containsText == null || containsText.isEmpty() || item
                        .getPromptText()
                        .contains(containsText)) {
                     return true;
                  }
               }
            }
            return false;
         }

         @Override
         public boolean get(URIish uri, CredentialItem... items) throws UnsupportedCredentialItem {
            for (CredentialItem item : items) {
               if (item instanceof CredentialItem.YesNoType) {
                  ((CredentialItem.YesNoType) item).setValue(true);
                  return true;
               }
            }
            return false;
         }

         @Override
         public boolean isInteractive() {
            return false;
         }
      };
   }

}
