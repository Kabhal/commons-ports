/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.git;

import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.defaultParameters;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.sensitiveParameters;
import static java.util.function.UnaryOperator.identity;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.net.URI;
import java.nio.file.Path;
import java.util.List;
import java.util.function.UnaryOperator;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentContent;
import com.tinubu.commons.ports.document.domain.DocumentMetadata;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.LoadedDocumentContent.LoadedDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.testsuite.CommonDocumentRepositoryTest;
import com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions;
import com.tinubu.commons.ports.document.domain.uri.RepositoryUri;
import com.tinubu.commons.ports.document.fs.FsDocumentConfig.FsDocumentConfigBuilder;
import com.tinubu.commons.ports.document.fs.FsDocumentRepository;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.Authentication;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.GitDocumentConfigBuilder;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ZipArchiver;

public class GitDocumentRepositoryTest {

   private static final URI REMOTE_REPOSITORY_URI = URI.create("https://github.com/github/testrepo.git");
   private static final String REMOTE_BRANCH = "email";

   @BeforeEach
   void enableGitInitialization() {
      GitDocumentRepository.disableRepositoryInitialization = false;
   }

   @Nested
   public class CommonTestSuite extends CommonDocumentRepositoryTest {
      private FsDocumentRepository localRepository;
      private GitDocumentRepository documentRepository;

      /**
       * {@inheritDoc}
       * At least the Linux filesystem updates the creation date on overwrite.
       */
      @Override
      public boolean isUpdatingCreationDateOnOverwrite() {
         return true;
      }

      // FIXME re-enable subpath tests ?
      @Override
      protected boolean isSupportingSubPath() {
         return false;
      }

      @Override
      protected boolean isSupportingMetadataAttributes() {
         return false;
      }

      @BeforeEach
      public void configureDocumentRepository() {
         this.localRepository =
               new FsDocumentRepository(new FsDocumentConfigBuilder().deleteRepositoryOnClose(true).build());
         this.documentRepository = new GitDocumentRepository(new GitDocumentConfigBuilder()
                                                                   .localRepository(localRepository)
                                                                   .remoteUri(REMOTE_REPOSITORY_URI)
                                                                   .branch(REMOTE_BRANCH)
                                                                   .build());
      }

      @AfterEach
      public void closeDocumentRepository() {
         try {
            this.documentRepository.close();
         } finally {
            this.localRepository.close();
         }
      }

      @Override
      protected DocumentRepository documentRepository() {
         return documentRepository;
      }

      @Override
      protected Document zipTransformer(DocumentPath zipPath, List<Document> documents) {
         return new ZipArchiver(zipPath).compress(documents);
      }

      @Override
      protected UnaryOperator<DocumentMetadataBuilder> synchronizeExpectedMetadata(DocumentMetadata actual) {
         return builder -> builder
               .chain(super.synchronizeExpectedMetadata(actual))
               .attributes(actual.attributes())
               .contentType(actual.contentEncoding().orElse(null));
      }

      @Override
      protected UnaryOperator<LoadedDocumentContentBuilder> synchronizeExpectedContent(DocumentContent actual) {
         return builder -> builder
               .chain(super.synchronizeExpectedContent(actual))
               .contentEncoding(actual.contentEncoding().orElse(null));
      }

      @Nested
      public class DocumentRepositoryUriAdapter {

         @Test
         public void testToRepositoryUriWhenNominal() {
            assertThat(documentRepository().toUri().stringValue()).isEqualTo(
                  "https://github.com/github/testrepo.git#email");
         }

         @Test
         public void testToUriWhenCompatibleUri() {
            assertThatToUriEquals("https://localhost/path.git", "https://localhost/path.git");
            assertThatToUriEquals("https://localhost/path", "https://localhost/path");
            assertThatToUriEquals("https://localhost/path#branch", "https://localhost/path#branch");
            assertThatToUriEquals("https://user@localhost/path.git", "https://localhost/path.git");
            assertThatToUriEquals("https://user@localhost/path.git", true, "https://user@localhost/path.git");
            assertThatToUriEquals("https://user:password@localhost/path.git", "https://localhost/path.git");
            assertThatToUriEquals("https://user:password@localhost/path.git",
                                  true,
                                  "https://user:password@localhost/path.git");
            assertThatToUriEquals("https://localhost/path.git#branch", "https://localhost/path.git#branch");
            assertThatToUriEquals("document:git:https://localhost/path.git", "https://localhost/path.git");
            assertThatToUriEquals("document:git:https://localhost/path.git#branch",
                                  "https://localhost/path.git#branch");

            assertThatToUriEquals("http://localhost/path.git", "http://localhost/path.git");
            assertThatToUriEquals("http://localhost/path", "http://localhost/path");
            assertThatToUriEquals("http://localhost/path#branch", "http://localhost/path#branch");
            assertThatToUriEquals("http://user@localhost/path.git", "http://localhost/path.git");
            assertThatToUriEquals("http://user@localhost/path.git", true, "http://user@localhost/path.git");
            assertThatToUriEquals("http://user:password@localhost/path.git", "http://localhost/path.git");
            assertThatToUriEquals("http://user:password@localhost/path.git",
                                  true,
                                  "http://user:password@localhost/path.git");
            assertThatToUriEquals("http://localhost/path.git#branch", "http://localhost/path.git#branch");
            assertThatToUriEquals("document:git:http://localhost/path.git", "http://localhost/path.git");
            assertThatToUriEquals("document:git:http://localhost/path.git#branch",
                                  "http://localhost/path.git#branch");

            assertThatToUriEquals("git://localhost/path.git", "git://localhost/path.git");
            assertThatToUriEquals("git://localhost/path", "git://localhost/path");
            assertThatToUriEquals("git://localhost/path#branch", "git://localhost/path#branch");
            assertThatToUriEquals("git://user@localhost/path.git", "git://localhost/path.git");
            assertThatToUriEquals("git://user@localhost/path.git", true, "git://user@localhost/path.git");
            assertThatToUriEquals("git://user:password@localhost/path.git", "git://localhost/path.git");
            assertThatToUriEquals("git://user:password@localhost/path.git",
                                  true,
                                  "git://user:password@localhost/path.git");
            assertThatToUriEquals("git://localhost/path.git#branch", "git://localhost/path.git#branch");
            assertThatToUriEquals("document:git:git://localhost/path.git", "git://localhost/path.git");
            assertThatToUriEquals("document:git:git://localhost/path.git#branch",
                                  "git://localhost/path.git#branch");

            assertThatToUriEquals("ssh://localhost/path.git", "ssh://localhost/path.git");
            assertThatToUriEquals("ssh://localhost/path", "ssh://localhost/path");
            assertThatToUriEquals("ssh://localhost/path#branch", "ssh://localhost/path#branch");
            assertThatToUriEquals("ssh://user@localhost/path.git", "ssh://localhost/path.git");
            assertThatToUriEquals("ssh://user@localhost/path.git", true, "ssh://user@localhost/path.git");
            assertThatToUriEquals("ssh://user:password@localhost/path.git", "ssh://localhost/path.git");
            assertThatToUriEquals("ssh://user:password@localhost/path.git",
                                  true,
                                  "ssh://user:password@localhost/path.git");
            assertThatToUriEquals("ssh://localhost/path.git#branch", "ssh://localhost/path.git#branch");
            assertThatToUriEquals("document:git:ssh://localhost/path.git", "ssh://localhost/path.git");
            assertThatToUriEquals("document:git:ssh://localhost/path.git#branch",
                                  "ssh://localhost/path.git#branch");

            assertThatToUriEquals("file:/path.git", "file:/path.git");
            assertThatToUriEquals("file:/path", "file:/path");
            assertThatToUriEquals("file:///path.git", "file:///path.git");
            assertThatToUriEquals("file:/path.git#branch", "file:/path.git#branch");
            assertThatToUriEquals("file:///path.git#branch", "file:///path.git#branch");
            assertThatToUriEquals("/path.git", "/path.git");
            assertThatToUriEquals("/path", "/path");
            assertThatToUriEquals("/path.git#branch", "/path.git#branch");
            assertThatToUriEquals("document:git:file:/path.git", "file:/path.git");
            assertThatToUriEquals("document:git:file:/path", "file:/path");
            assertThatToUriEquals("document:git:file:/path.git#branch", "file:/path.git#branch");
            assertThatToUriEquals("document:git:/path.git", "/path.git");
            assertThatToUriEquals("document:git:/path", "/path");
            assertThatToUriEquals("document:git:/path.git#branch", "/path.git#branch");
         }

         @Test
         public void testToUriWhenEmptyUserPasswordOrNoPasswordUri() {
            assertThatToUriEquals("https://user@localhost/path.git",
                                  true,
                                  "https://user@localhost/path.git"); // no password
            assertThatToUriEquals("https://user:@localhost/path.git",
                                  true,
                                  "https://user:@localhost/path.git"); // empty password
            assertThatThrownBy(() -> assertThatToUriEquals("https://:password@localhost/path.git",
                                                           true,
                                                           "https://:password@localhost/path.git"))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage(
                        "Invariant validation error > Context [Authentication[username=<hidden-value>,password=<hidden-value>]] > {username} 'username' must not be blank"); // empty user
            assertThatThrownBy(() -> assertThatToUriEquals("https://@localhost/path.git",
                                                           true,
                                                           "https://@localhost/path.git"))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage(
                        "Invariant validation error > Context [Authentication[username=<hidden-value>,password=<null>]] > {username} 'username' must not be blank"); // empty user and no password
            assertThatThrownBy(() -> assertThatToUriEquals("https://:@localhost/path.git",
                                                           true,
                                                           "https://:@localhost/path.git"))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage(
                        "Invariant validation error > Context [Authentication[username=<hidden-value>,password=<hidden-value>]] > {username} 'username' must not be blank"); // empty user & password
         }

         @Test
         public void testToUriWhenLocalUri() {
            assertThatToUriEquals(
                  "https://localhost/path.git?localUri=file:/tmp/git?failFastIfMissingStoragePath=false",
                  "https://localhost/path.git?localUri=file:/tmp/git?failFastIfMissingStoragePath=false");
         }

         @Test
         public void testToUriWhenLocalRepository() {
            assertThatToUriEquals("https://localhost/path.git",
                                  b -> b.localRepository(new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                                                        .storagePath(Path.of(
                                                                                              "/tmp/git"))
                                                                                        .failFastIfMissingStoragePath(
                                                                                              false)
                                                                                        .build())),
                                  defaultParameters(),
                                  "https://localhost/path.git?localUri=file:/tmp/git?failFastIfMissingStoragePath=false");
         }

         @Test
         public void testToUriWhenConflictingRepository() {
            assertThatThrownBy(() -> assertThatToUriEquals(
                  "https://localhost/path.git?localUri=file:/tmp/git%3FfailFastIfMissingStoragePath=false",
                  b -> b.localRepository(new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                                        .storagePath(Path.of("/tmp/git"))
                                                                        .failFastIfMissingStoragePath(false)
                                                                        .build())),
                  defaultParameters(),
                  "https://localhost/path.git?localUri=file%3A%2Ftmp%2Fgit%3FfailFastIfMissingStoragePath%3Dfalse"))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage(
                        "Invariant validation error > Context [GitDocumentConfig[localRepository=FsDocumentRepository[fsStorageStrategy=DirectFsStorageStrategy[storagePath=/tmp/git]],localUri=file:/tmp/git?failFastIfMissingStoragePath=false,remoteUri=https://localhost/path.git,forceLocalUri=false,remoteAuthentication=<null>,basePath=,remote=origin,initialBranch=main,branch=<null>,createRepositoryIfMissing=true,initIfMissing=true,pullStrategy=PullStrategy[rebase=false,fastForward=FAST_FORWARD,pullMode=NEVER,shallowDepth=<null>],pushStrategy=PushStrategy[commitMode=NEVER,pushMode=NEVER],transport=Transport[connectTimeout=PT10S,disableSslVerify=false,allowUnknownSshHosts=false]]] > {localRepository} 'localRepository=FsDocumentRepository[fsStorageStrategy=DirectFsStorageStrategy[storagePath=/tmp/git]]' and 'localUri=file:/tmp/git?failFastIfMissingStoragePath=false' must be mutually exclusive | {localUri} 'localUri=file:/tmp/git?failFastIfMissingStoragePath=false' and 'localRepository=FsDocumentRepository[fsStorageStrategy=DirectFsStorageStrategy[storagePath=/tmp/git]]' must be mutually exclusive");
         }

         @Test
         public void testToUriWhenTemporaryLocal() {
            assertThatToUriEquals("https://localhost/path.git", "https://localhost/path.git");
         }

         @Test
         public void testToUriWhenNonDefaultBranch() {
            assertThatToUriEquals("https://localhost/path.git#release", "https://localhost/path.git#release");
         }

         @Test
         public void testToUriWhenNonDefaultPorts() {
            assertThatToUriEquals("http://localhost:81/path.git", "http://localhost:81/path.git");
            assertThatToUriEquals("https://localhost:444/path.git", "https://localhost:444/path.git");
            assertThatToUriEquals("ssh://localhost:23/path.git", "ssh://localhost:23/path.git");
            assertThatToUriEquals("git://localhost:9419/path.git", "git://localhost:9419/path.git");
         }

         @Test
         public void testToUriWhenDefaultPorts() {
            assertThatToUriEquals("http://localhost:80/path.git", "http://localhost/path.git");
            assertThatToUriEquals("https://localhost:443/path.git", "https://localhost/path.git");
            assertThatToUriEquals("ssh://localhost:22/path.git", "ssh://localhost/path.git");
            assertThatToUriEquals("git://localhost:9418/path.git", "git://localhost/path.git");
         }

         @Test
         public void testToUriWhenAuthenticationInUri() {
            assertThatToUriEquals("https://user:password@localhost/path.git#release",
                                  sensitiveParameters(),
                                  "https://user:password@localhost/path.git#release");
            assertThatToUriEquals("https://user:password@localhost/path.git#release",
                                  defaultParameters(),
                                  "https://localhost/path.git#release");
         }

         @Test
         public void testToUriWhenAuthenticationInConfig() {
            assertThatToUriEquals("https://localhost/path.git#release",
                                  b -> b.remoteAuthentication(Authentication.of("user", "password")),
                                  sensitiveParameters(),
                                  "https://user:password@localhost/path.git#release");
            assertThatToUriEquals("https://localhost/path.git#release",
                                  b -> b.remoteAuthentication(Authentication.of("user", "password")),
                                  defaultParameters(),
                                  "https://localhost/path.git#release");
         }

         private void assertThatToUriEquals(String repositoryUri,
                                            UnaryOperator<GitDocumentConfigBuilder> builderConfigurer,
                                            DefaultExportUriOptions options,
                                            String expected) {
            GitDocumentRepository.disableRepositoryInitialization = true;
            assertThat(GitDocumentRepository
                             .ofUri(RepositoryUri.ofRepositoryUri(repositoryUri),
                                    b -> b.chain(builderConfigurer))
                             .toUri(options)
                             .stringValue()).isEqualTo(expected);
         }

         private void assertThatToUriEquals(String repositoryUri,
                                            DefaultExportUriOptions options,
                                            String expected) {
            assertThatToUriEquals(repositoryUri, identity(), options, expected);
         }

         private void assertThatToUriEquals(String repositoryUri, boolean sensitive, String expected) {
            assertThatToUriEquals(repositoryUri,
                                  sensitive ? sensitiveParameters() : defaultParameters(),
                                  expected);
         }

         private void assertThatToUriEquals(String repositoryUri, String expected) {
            assertThatToUriEquals(repositoryUri, false, expected);
         }

         @Test
         public void testSupportsRepositoryUriWhenNominal() {
            assertThat(documentRepository().supportsUri(repositoryUri(
                  "https://github.com/github/testrepo.git#email"))).isTrue();
            assertThat(documentRepository().supportsUri(repositoryUri(
                  "https://github.com:443/github/testrepo.git#email"))).isTrue();
         }

         @Test
         public void testSupportsRepositoryUriWhenNoExplicitBranch() {
            assertThat(documentRepository().supportsUri(repositoryUri("https://github.com/github/testrepo.git"))).isFalse();
         }

         @Test
         public void testSupportsRepositoryUriWhenMatchingBranch() {
            assertThat(documentRepository().supportsUri(repositoryUri(
                  "https://github.com/github/testrepo.git#email"))).isTrue();
         }

         @Test
         public void testSupportsRepositoryUriWhenNotMatchingBranch() {
            assertThat(documentRepository().supportsUri(repositoryUri(
                  "https://github.com/github/testrepo.git#bad"))).isFalse();
         }

         @Test
         public void testSupportsRepositoryUriWhenHasNotMatchingUri() {
            assertThat(documentRepository().supportsUri(repositoryUri(
                  "http://github.com/github/testrepo.git#email"))).isFalse();
            assertThat(documentRepository().supportsUri(repositoryUri(
                  "git://github.com/github/testrepo.git#email"))).isFalse();
            assertThat(documentRepository().supportsUri(repositoryUri(
                  "ssh://github.com/github/testrepo.git#email"))).isFalse();

            assertThat(documentRepository().supportsUri(repositoryUri(
                  "https://github.fr/github/testrepo.git#email"))).isFalse();
            assertThat(documentRepository().supportsUri(repositoryUri(
                  "https://github.com/bad/testrepo.git#email"))).isFalse();
            assertThat(documentRepository().supportsUri(repositoryUri(
                  "https://github.com/github/bad.git#email"))).isFalse();
            assertThat(documentRepository().supportsUri(repositoryUri(
                  "https://github.com:444/github/testrepo.git#email"))).isFalse();
         }

         @Test
         public void testSupportsRepositoryUriWhenNotMatchingAuthentication() {
            assertThat(documentRepository().supportsUri(repositoryUri(
                  "https://otheruser@github.com/github/testrepo.git#email"))).isTrue();
            assertThat(documentRepository().supportsUri(repositoryUri(
                  "https://otheruser:otherpassword@github.com/github/testrepo.git#email"))).isTrue();
         }

         @Test
         public void testSupportsRepositoryUriWhenHasNotCompatibleUri() {
            assertThat(documentRepository().supportsUri(repositoryUri("scheme://repository")))
                  .as("Scheme is not supported")
                  .isFalse();
         }

      }

      @Test
      public void testGitDocumentRepositoryWhenConflictingAuthentication() {
         assertThatThrownBy(() -> new GitDocumentRepository(new GitDocumentConfigBuilder()
                                                                  .remoteUri(URI.create(
                                                                        "https://user:password@localhost/path.git"))
                                                                  .remoteAuthentication(Authentication.of(
                                                                        "otheruser",
                                                                        "otherpassword"))
                                                                  .build()))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessageContaining(
                     "{remoteUri} 'remoteUri.username=user' must match configured 'authentication.username=otheruser'");
      }

   }

   @Nested
   public class WhenAlternativeUriSchemes {

      @Test
      @Disabled("GitHub authentication issues")
      public void testGitRepositoryWhenGitScheme() {
         try (FsDocumentRepository localRepository = new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                                                    .deleteRepositoryOnClose(
                                                                                          true)
                                                                                    .build());
              DocumentRepository repository = new GitDocumentRepository(new GitDocumentConfigBuilder()
                                                                              .localRepository(localRepository)
                                                                              .remoteUri(URI.create(
                                                                                    "git://github.com/github/testrepo.git"))
                                                                              .branch(REMOTE_BRANCH)
                                                                              .cloneIfMissing(true)
                                                                              .build())) {
            assertThat(repository).isNotNull();
         }
      }

      @Test
      @Disabled("GitHub authentication issues")
      public void testGitRepositoryWhenSshScheme() {
         try (FsDocumentRepository localRepository = new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                                                    .deleteRepositoryOnClose(
                                                                                          true)
                                                                                    .build());
              DocumentRepository repository = new GitDocumentRepository(new GitDocumentConfigBuilder()
                                                                              .localRepository(localRepository)
                                                                              .remoteUri(URI.create(
                                                                                    "ssh://git@github.com/github/testrepo.git"))
                                                                              .branch(REMOTE_BRANCH)
                                                                              .cloneIfMissing(true)
                                                                              .build())) {
            assertThat(repository).isNotNull();
         }

      }

      @Test
      public void testGitRepositoryWhenSshWithoutScheme() {
         assertThatThrownBy(() -> new GitDocumentRepository(new GitDocumentConfigBuilder()
                                                                  .remoteUri(URI.create(
                                                                        "git@github.com:github/testrepo.git"))
                                                                  .branch(REMOTE_BRANCH)
                                                                  .cloneIfMissing(true)
                                                                  .build()))
               .as("Not supported URI because java.lang.URI can't parse this one in first place")
               .isInstanceOf(IllegalArgumentException.class)
               .hasMessageContaining("Illegal character in scheme name");
      }

      @Test
      public void testGitRepositoryWhenLocalPath() {
         try (FsDocumentRepository firstLocalRepository = new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                                                         .deleteRepositoryOnClose(
                                                                                               true)
                                                                                         .build());
              GitDocumentRepository firstRepository = new GitDocumentRepository(new GitDocumentConfigBuilder()
                                                                                      .localRepository(
                                                                                            firstLocalRepository)
                                                                                      .remoteUri(
                                                                                            REMOTE_REPOSITORY_URI)
                                                                                      .branch(REMOTE_BRANCH)
                                                                                      .cloneIfMissing(true)
                                                                                      .build());
              FsDocumentRepository localRepository = new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                                                    .deleteRepositoryOnClose(
                                                                                          true)
                                                                                    .build());
              DocumentRepository repository = new GitDocumentRepository(new GitDocumentConfigBuilder()
                                                                              .localRepository(localRepository)
                                                                              .remoteUri(URI.create(
                                                                                    firstLocalRepository
                                                                                          .storagePath()
                                                                                          .toString()))
                                                                              .branch(REMOTE_BRANCH)
                                                                              .cloneIfMissing(true)
                                                                              .build())) {
            assertThat(repository).isNotNull();
         }
      }

   }

   @Nested
   public class WhenInitializedGitRepository {

      @Test
      public void testGitRepositoryWhenInitializedRepository() {
         try (FsDocumentRepository localRepository = new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                                                    .deleteRepositoryOnClose(
                                                                                          true)
                                                                                    .build())) {
            try (DocumentRepository initializeRepository = new GitDocumentRepository(new GitDocumentConfigBuilder()
                                                                                           .localRepository(
                                                                                                 localRepository)
                                                                                           .remoteUri(
                                                                                                 REMOTE_REPOSITORY_URI)
                                                                                           .branch("master")
                                                                                           .cloneIfMissing(
                                                                                                 false)
                                                                                           .initIfMissing(true)
                                                                                           .build())) {
               assertThat(initializeRepository).isNotNull();

            }

            try (DocumentRepository repository = new GitDocumentRepository(new GitDocumentConfigBuilder()
                                                                                 .localRepository(
                                                                                       localRepository)
                                                                                 .remoteUri(
                                                                                       REMOTE_REPOSITORY_URI)
                                                                                 .branch("master")
                                                                                 .cloneIfMissing(false)
                                                                                 .initIfMissing(true)
                                                                                 .build())) {
               assertThat(repository).isNotNull();
            }

            try (DocumentRepository repository = new GitDocumentRepository(new GitDocumentConfigBuilder()
                                                                                 .localRepository(
                                                                                       localRepository)
                                                                                 .remoteUri(
                                                                                       REMOTE_REPOSITORY_URI)
                                                                                 .branch("master")
                                                                                 .cloneIfMissing(true)
                                                                                 .build())) {
               assertThat(repository).isNotNull();
            }
         }

      }
   }

}