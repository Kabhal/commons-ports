/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.git;

import static com.tinubu.commons.ddd2.uri.Uri.uri;
import static com.tinubu.commons.lang.util.CheckedPredicate.alwaysTrue;
import static com.tinubu.commons.lang.util.PathUtils.emptyPath;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.defaultParameters;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.noParameters;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.sensitiveParameters;
import static com.tinubu.commons.ports.document.fs.FsDocumentConfig.FsDocumentConfigBuilder;
import static com.tinubu.commons.ports.document.git.Constants.DEFAULT_BRANCH;
import static com.tinubu.commons.ports.document.git.Constants.DEFAULT_CLONE_IF_MISSING;
import static com.tinubu.commons.ports.document.git.Constants.DEFAULT_INIT_IF_MISSING;
import static com.tinubu.commons.ports.document.git.Constants.DEFAULT_REMOTE;
import static com.tinubu.commons.ports.document.git.FastForward.NO_FAST_FORWARD;
import static com.tinubu.commons.ports.document.git.GitDocumentConfig.PullStrategy.PullMode;
import static com.tinubu.commons.ports.document.git.GitDocumentConfig.PullStrategy.PullStrategyBuilder;
import static com.tinubu.commons.ports.document.git.GitRepositoryUri.Parameters.BASE_PATH;
import static com.tinubu.commons.ports.document.git.GitRepositoryUri.Parameters.BRANCH;
import static com.tinubu.commons.ports.document.git.GitRepositoryUri.Parameters.CLONE_IF_MISSING;
import static com.tinubu.commons.ports.document.git.GitRepositoryUri.Parameters.FORCE_LOCAL_URI;
import static com.tinubu.commons.ports.document.git.GitRepositoryUri.Parameters.INITIAL_BRANCH;
import static com.tinubu.commons.ports.document.git.GitRepositoryUri.Parameters.INIT_IF_MISSING;
import static com.tinubu.commons.ports.document.git.GitRepositoryUri.Parameters.LOCAL_URI;
import static com.tinubu.commons.ports.document.git.GitRepositoryUri.Parameters.REMOTE;
import static com.tinubu.commons.ports.document.git.GitRepositoryUri.Parameters.REPOSITORY_PATH;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.net.URI;
import java.nio.file.Path;
import java.time.Duration;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ports.document.domain.uri.RepositoryUri;
import com.tinubu.commons.ports.document.fs.FsDocumentRepository;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.Authentication;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.GitDocumentConfigBuilder;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.PushStrategy.PushMode;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.PushStrategy.PushStrategyBuilder;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.Transport.TransportBuilder;

public class GitRepositoryUriTest {

   @Nested
   public class OfUriWhenURI {

      @Test
      public void ofUriWhenNominal() {
         assertThat(GitRepositoryUri.ofRepositoryUri(uri("https://host/path.git"))).satisfies(uri -> {
            assertThat(uri.localUri()).isEmpty();
            assertThat(uri.remoteUri()).hasValue(uri("https://host/path.git"));
            assertThat(uri.repositoryPath()).isEqualTo(Path.of("/path.git"));
            assertThat(uri.basePath()).isEqualTo(Path.of(""));
            assertThat(uri.branch()).isEmpty();
         });
         assertThat(GitRepositoryUri.ofRepositoryUri("https://host/path.git")).satisfies(uri -> {
            assertThat(uri.localUri()).isEmpty();
            assertThat(uri.remoteUri()).hasValue(uri("https://host/path.git"));
            assertThat(uri.repositoryPath()).isEqualTo(Path.of("/path.git"));
            assertThat(uri.basePath()).isEqualTo(Path.of(""));
            assertThat(uri.branch()).isEmpty();
         });
      }

      @Test
      public void ofUriWhenEmptyRepositoryPath() {
         assertThat(GitRepositoryUri.ofRepositoryUri("https://host")).satisfies(uri -> {
            assertThat(uri.localUri()).isEmpty();
            assertThat(uri.remoteUri()).hasValue(uri("https://host"));
            assertThat(uri.repositoryPath()).isEqualTo(Path.of(""));
            assertThat(uri.basePath()).isEqualTo(Path.of(""));
            assertThat(uri.branch()).isEmpty();
         });
         assertThat(GitRepositoryUri.ofRepositoryUri("https://host?repositoryPath=")).satisfies(uri -> {
            assertThat(uri.localUri()).isEmpty();
            assertThat(uri.remoteUri()).hasValue(uri("https://host"));
            assertThat(uri.repositoryPath()).isEqualTo(Path.of(""));
            assertThat(uri.basePath()).isEqualTo(Path.of(""));
            assertThat(uri.branch()).isEmpty();
         });
      }

      @Test
      public void ofUriWhenRootRepositoryPath() {
         assertThat(GitRepositoryUri.ofRepositoryUri("https://host/")).satisfies(uri -> {
            assertThat(uri.localUri()).isEmpty();
            assertThat(uri.remoteUri()).hasValue(uri("https://host/"));
            assertThat(uri.repositoryPath()).isEqualTo(Path.of("/"));
            assertThat(uri.basePath()).isEqualTo(Path.of(""));
            assertThat(uri.branch()).isEmpty();
         });
         assertThat(GitRepositoryUri.ofRepositoryUri("https://host/?repositoryPath=/")).satisfies(uri -> {
            assertThat(uri.localUri()).isEmpty();
            assertThat(uri.remoteUri()).hasValue(uri("https://host/"));
            assertThat(uri.repositoryPath()).isEqualTo(Path.of("/"));
            assertThat(uri.basePath()).isEqualTo(Path.of(""));
            assertThat(uri.branch()).isEmpty();
         });
      }

      @Test
      public void ofUriWhenAuthority() {
         assertThat(GitRepositoryUri.ofRepositoryUri(uri("https://user:password@host/path.git"))).satisfies(
               uri -> { });
      }

      @Test
      public void ofUriWhenBadParameters() {
         assertThatThrownBy(() -> GitRepositoryUri.ofRepositoryUri((URI) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
         assertThatThrownBy(() -> GitRepositoryUri.ofRepositoryUri((String) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
      }

      @Test
      public void ofUriWhenIllegalArgument() {
         assertThatExceptionOfType(InvalidUriException.class)
               .isThrownBy(() -> GitRepositoryUri.ofRepositoryUri(
                     "https://host?localUri=user@git.com:repo.git"))
               .withMessage(
                     "Invalid '%s' URI: Conversion from 'java.lang.String' to 'java.net.URI' failed for 'user@git.com:repo.git' value",
                     "https://host?localUri=user@git.com:repo.git");
      }

      @Test
      public void ofUriWhenCaseSensitiveParameterKey() {
         assertThat(GitRepositoryUri.ofRepositoryUri("https://host/path.git?rEpOsItOryPaTh=/")).satisfies(uri -> {
         });
      }

      @Test
      public void ofUriWhenNormalized() {
         assertThat(GitRepositoryUri.ofRepositoryUri(uri("https://host/path/../."))).satisfies(uri -> {
            assertThat(uri.localUri()).isEmpty();
            assertThat(uri.remoteUri()).hasValue(uri("https://host/"));
            assertThat(uri.basePath()).isEqualTo(Path.of(""));
         });
         assertThat(GitRepositoryUri.ofRepositoryUri("https://host/path/../.")).satisfies(uri -> {
            assertThat(uri.localUri()).isEmpty();
            assertThat(uri.remoteUri()).hasValue(uri("https://host/"));
            assertThat(uri.basePath()).isEqualTo(Path.of(""));

         });
         assertThat(GitRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri("https://host/path/../."))).satisfies(
               uri -> {
                  assertThat(uri.localUri()).isEmpty();
                  assertThat(uri.remoteUri()).hasValue(uri("https://host/"));
                  assertThat(uri.basePath()).isEqualTo(Path.of(""));
               });
         assertThatThrownBy(() -> GitRepositoryUri.ofRepositoryUri(uri("https://host/../path")))
               .isExactlyInstanceOf(InvalidUriException.class)
               .hasMessageContainingAll("Invalid 'https://host/../path' URI",
                                        "must not have traversal paths");
      }

      @Test
      public void ofUriWhenInvalidUriSyntax() {
         assertThatThrownBy(() -> GitRepositoryUri.ofRepositoryUri("git@git.com:user/repo.git"))
               .isExactlyInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'git@git.com:user/repo.git' URI: Illegal character in scheme name at index 3: git@git.com:user/repo.git");
         assertThatThrownBy(() -> GitRepositoryUri.ofRepositoryUri("git@git.com:user/repo.git"))
               .isExactlyInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'git@git.com:user/repo.git' URI: Illegal character in scheme name at index 3: git@git.com:user/repo.git");
      }

      @Test
      public void ofUriWhenIncompatibleUri() {
         assertThatThrownBy(() -> GitRepositoryUri.ofRepositoryUri("unknown:/path"))
               .isExactlyInstanceOf(IncompatibleUriException.class)
               .hasMessage(
                     "Incompatible 'unknown:/path' URI: 'uri=unknown:/path' must be compatible with GitUri, HttpUri, SshUri or FileUri");
      }

      @Test
      @Disabled("No way to produce InvalidUriException with an invalid URI")
      public void ofUriWhenInvalidUri() {
      }

      @Test
      public void ofUriWhenBranch() {
         assertThat(GitRepositoryUri.ofRepositoryUri(uri("https://host/path.git"))).satisfies(uri -> {
            assertThat(uri.branch()).isEmpty();
         });
         assertThat(GitRepositoryUri.ofRepositoryUri(uri("https://host/path.git#branch"))).satisfies(uri -> {
            assertThat(uri.branch()).hasValue("branch");
         });
         assertThat(GitRepositoryUri.ofRepositoryUri(uri("https://host/path.git?branch=branch"))).satisfies(
               uri -> {
                  assertThat(uri.branch()).hasValue("branch");
               });
      }

      @Test
      public void ofUriWhenInvalidBranchParameter() {
         assertThatThrownBy(() -> GitRepositoryUri.ofRepositoryUri(uri(
               "https://host/path.git?branch=branch#main")))
               .isExactlyInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'https://host/path.git?branch=branch#main' URI: Inconsistent branch value between URI 'main' fragment and 'branch' branch parameter");
      }

      @Test
      public void ofUriWhenDefaultParameters() {
         assertThat(GitRepositoryUri.ofRepositoryUri("https://host/path.git")).satisfies(uri -> {
            assertThat(uri.branch()).isEmpty();
            assertThat(uri.query().parameter(REMOTE, alwaysTrue())).hasValue(DEFAULT_REMOTE);
            assertThat(uri.query().parameter(INITIAL_BRANCH, alwaysTrue())).hasValue(DEFAULT_BRANCH);
            assertThat(uri.query().parameter(BRANCH, alwaysTrue())).isEmpty();
            assertThat(uri.query().parameter(LOCAL_URI, alwaysTrue())).isEmpty();
            assertThat(uri.query().parameter(CLONE_IF_MISSING, alwaysTrue())).hasValue(
                  DEFAULT_CLONE_IF_MISSING);
            assertThat(uri
                             .query()
                             .parameter(INIT_IF_MISSING, alwaysTrue())).hasValue(DEFAULT_INIT_IF_MISSING);
         });
      }

      @Test
      public void ofUriWhenAllParameters() {
         assertThat(GitRepositoryUri.ofRepositoryUri(
               "https://host/path.git?branch=branch&remote=remote&initialBranch=initial-branch&localUri=file://local&cloneIfMissing=false&initIfMissing=false")).satisfies(
               uri -> {
                  assertThat(uri.branch()).hasValue("branch");
                  assertThat(uri.query().parameter(REMOTE, alwaysTrue())).hasValue("remote");
                  assertThat(uri.query().parameter(INITIAL_BRANCH, alwaysTrue())).hasValue("initial-branch");
                  assertThat(uri.query().parameter(BRANCH, alwaysTrue())).hasValue("branch");
                  assertThat(uri.query().parameter(LOCAL_URI, alwaysTrue())).hasValue(uri("file://local"));
                  assertThat(uri.query().parameter(CLONE_IF_MISSING, alwaysTrue())).hasValue(false);
                  assertThat(uri.query().parameter(INIT_IF_MISSING, alwaysTrue())).hasValue(false);

               });

      }

      @Test
      public void ofUriWhenLocalUriParameter() {
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("https://host/path.git?localUri=file:/local")
                          .toConfig()
                          .build()).satisfies(config -> {
            assertThat(config.localUri()).hasValue(uri("file:/local"));
         });
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("https://host/path.git?localUri=/local")
                          .toConfig()
                          .build()).satisfies(config -> {
            assertThat(config.localUri()).hasValue(uri("/local"));
         });
         assertThatThrownBy(() -> GitRepositoryUri
               .ofRepositoryUri("https://host/path.git?localUri=ssh://localhost/path")
               .toConfig()
               .build())
               .isExactlyInstanceOf(InvariantValidationException.class)
               .hasMessageContainingAll("Invariant validation error >",
                                        "{localUri} 'localUri=ssh://localhost/path' URI must be filesystem document repository URI",
                                        "Incompatible 'ssh://localhost/path' URI",
                                        "must be absolute, and scheme must be equal to 'file'");

      }

      @Nested
      class RepositoryPathParameter {

         @Test
         public void ofUriWhenNominal() {
            assertThat(GitRepositoryUri.ofRepositoryUri("https://host/path.git?repositoryPath=/path.git")).satisfies(
                  uri -> {
                     assertThat(uri.localUri()).isEmpty();
                     assertThat(uri.remoteUri()).hasValue(uri("https://host/path.git"));
                     assertThat(uri.repositoryPath()).isEqualTo(Path.of("/path.git"));
                     assertThat(uri.basePath()).isEqualTo(emptyPath());
                  });
         }

         @Test
         public void ofUriWhenAutodetected() {
            assertThat(GitRepositoryUri.ofRepositoryUri("https://host/path/subpath.git")).satisfies(uri -> {
               assertThat(uri.localUri()).isEmpty();
               assertThat(uri.remoteUri()).hasValue(uri("https://host/path/subpath.git"));
               assertThat(uri.repositoryPath()).isEqualTo(Path.of("/path/subpath.git"));
               assertThat(uri.basePath()).isEqualTo(emptyPath());
            });
            assertThat(GitRepositoryUri.ofRepositoryUri("https://host/path/subpath.git/base")).satisfies(uri -> {
               assertThat(uri.localUri()).isEmpty();
               assertThat(uri.remoteUri()).hasValue(uri("https://host/path/subpath.git"));
               assertThat(uri.repositoryPath()).isEqualTo(Path.of("/path/subpath.git"));
               assertThat(uri.basePath()).isEqualTo(Path.of("base"));
            });
            assertThat(GitRepositoryUri.ofRepositoryUri(
                  "https://host/path/subpath.git/base?repositoryPath=/path/subpath.git&basePath=base")).satisfies(
                  uri -> {
                     assertThat(uri.localUri()).isEmpty();
                     assertThat(uri.remoteUri()).hasValue(uri("https://host/path/subpath.git"));
                     assertThat(uri.repositoryPath()).isEqualTo(Path.of("/path/subpath.git"));
                     assertThat(uri.basePath()).isEqualTo(Path.of("base"));
                  });
         }

         @Test
         public void ofUriWhenAutodetectedButOverriddenWithParameter() {
            assertThat(GitRepositoryUri.ofRepositoryUri(
                  "https://host/path/subpath.git/base/subpath?repositoryPath=/path/subpath.git/base")).satisfies(
                  uri -> {
                     assertThat(uri.localUri()).isEmpty();
                     assertThat(uri.remoteUri())
                           .as("basePath must not be present on remoteUri as GIT servers does no support this concept")
                           .hasValue(uri("https://host/path/subpath.git/base"));
                     assertThat(uri.repositoryPath()).isEqualTo(Path.of("/path/subpath.git/base"));
                     assertThat(uri.basePath()).isEqualTo(Path.of("subpath"));
                  });
            assertThat(GitRepositoryUri.ofRepositoryUri(
                  "https://host/path/subpath.git/base/subpath?repositoryPath=/path")).satisfies(uri -> {
               assertThat(uri.localUri()).isEmpty();
               assertThat(uri.remoteUri()).hasValue(uri("https://host/path"));
               assertThat(uri.repositoryPath()).isEqualTo(Path.of("/path"));
               assertThat(uri.basePath()).isEqualTo(Path.of("subpath.git/base/subpath"));
            });
         }

         @Test
         public void ofUriWhenRepositoryPathNotAutodetected() {
            assertThat(GitRepositoryUri.ofRepositoryUri("https://host/path/subpath")).satisfies(uri -> {
               assertThat(uri.localUri()).isEmpty();
               assertThat(uri.remoteUri()).hasValue(uri("https://host/path/subpath"));
               assertThat(uri.repositoryPath()).isEqualTo(Path.of("/path/subpath"));
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
            });
            assertThat(GitRepositoryUri.ofRepositoryUri(
                  "https://host/path/subpath?repositoryPath=/path/subpath&basePath=")).satisfies(uri -> {
               assertThat(uri.localUri()).isEmpty();
               assertThat(uri.remoteUri()).hasValue(uri("https://host/path/subpath"));
               assertThat(uri.repositoryPath()).isEqualTo(Path.of("/path/subpath"));
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
            });
         }

         @Test
         public void ofUriWhenNormalize() {
            assertThat(GitRepositoryUri.ofRepositoryUri("https://host/path.git?repositoryPath=/../path.git/./")).satisfies(
                  uri -> {
                     assertThat(uri.localUri()).isEmpty();
                     assertThat(uri.remoteUri()).hasValue(uri("https://host/path.git"));
                  });
         }

         @Test
         public void ofUriWhenNotAbsolute() {
            assertThatThrownBy(() -> GitRepositoryUri.ofRepositoryUri(
                  "https://host/path.git?repositoryPath=path.git"))
                  .isExactlyInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'https://host/path.git?repositoryPath=path.git' URI: 'repositoryPath=path.git' parameter must be absolute and must not contain traversal");
         }

         @Test
         public void ofUriWhenHasTraversals() {
            assertThatThrownBy(() -> GitRepositoryUri.ofRepositoryUri(
                  "https://host/path?repositoryPath=../base/."))
                  .isExactlyInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'https://host/path?repositoryPath=../base/.' URI: 'repositoryPath=../base' parameter must be absolute and must not contain traversal");
         }

         @Test
         public void ofUriWhenNotMatching() {
            assertThatThrownBy(() -> GitRepositoryUri.ofRepositoryUri(
                  "https://host/path.git?repositoryPath=/other"))
                  .isExactlyInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'https://host/path.git?repositoryPath=/other' URI: 'repositoryPath=/other' parameter must start '/path.git' path");
            assertThatThrownBy(() -> GitRepositoryUri.ofRepositoryUri(
                  "https://host/path.git?repositoryPath=/path"))
                  .isExactlyInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'https://host/path.git?repositoryPath=/path' URI: 'repositoryPath=/path' parameter must start '/path.git' path");
         }

      }

      @Nested
      class BasePathParameter {

         @Test
         public void ofUriWhenNominal() {
            assertThat(GitRepositoryUri.ofRepositoryUri("https://host/path.git/base?basePath=base")).satisfies(
                  uri -> {
                     assertThat(uri.localUri()).isEmpty();
                     assertThat(uri.remoteUri()).hasValue(uri("https://host/path.git"));
                     assertThat(uri.basePath()).isEqualTo(Path.of("base"));
                  });
         }

         @Test
         public void ofUriWhenAutodetectedRepositoryPath() {
            assertThat(GitRepositoryUri.ofRepositoryUri("https://host/path?basePath=base")).satisfies(uri -> {
               assertThat(uri.localUri()).isEmpty();
               assertThat(uri.remoteUri()).hasValue(uri("https://host/path"));
               assertThat(uri.basePath()).isEqualTo(Path.of("base"));
            });
         }

         @Test
         public void ofUriWhenNoUriBasePath() {
            assertThat(GitRepositoryUri.ofRepositoryUri("https://host/path.git?basePath=base")).satisfies(uri -> {
               assertThat(uri.localUri()).isEmpty();
               assertThat(uri.remoteUri()).hasValue(uri("https://host/path.git"));
               assertThat(uri.basePath()).isEqualTo(Path.of("base"));
            });
         }

         @Test
         public void ofUriWhenEmptyUriBasePath() {
            assertThat(GitRepositoryUri.ofRepositoryUri("https://host/path.git?basePath=")).satisfies(uri -> {
               assertThat(uri.localUri()).isEmpty();
               assertThat(uri.remoteUri()).hasValue(uri("https://host/path.git"));
               assertThat(uri.basePath()).isEqualTo(emptyPath());
            });
         }

         @Test
         public void ofUriWhenNormalize() {
            assertThat(GitRepositoryUri.ofRepositoryUri("https://host/path.git?basePath=sub/../base/./")).satisfies(
                  uri -> {
                     assertThat(uri.localUri()).isEmpty();
                     assertThat(uri.remoteUri()).hasValue(uri("https://host/path.git"));
                     assertThat(uri.basePath()).isEqualTo(Path.of("base"));
                  });
         }

         @Test
         public void ofUriWhenAbsolute() {
            assertThatThrownBy(() -> GitRepositoryUri.ofRepositoryUri("https://host/path.git?basePath=/base"))
                  .isExactlyInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'https://host/path.git?basePath=/base' URI: 'basePath=/base' parameter must not be absolute and must not contain traversal");
         }

         @Test
         public void ofUriWhenHasTraversals() {
            assertThatThrownBy(() -> GitRepositoryUri.ofRepositoryUri(
                  "https://host/path.git?basePath=../base/."))
                  .isExactlyInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'https://host/path.git?basePath=../base/.' URI: 'basePath=../base' parameter must not be absolute and must not contain traversal");
         }

         @Test
         public void ofUriWhenNotMatching() {
            assertThatThrownBy(() -> GitRepositoryUri.ofRepositoryUri(
                  "https://host/path.git/base?basePath=other"))
                  .isExactlyInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'https://host/path.git/base?basePath=other' URI: 'basePath=other' parameter must match 'base' path");
            assertThatThrownBy(() -> GitRepositoryUri.ofRepositoryUri(
                  "https://host/path.git/base?basePath=base/other"))
                  .isExactlyInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'https://host/path.git/base?basePath=base/other' URI: 'basePath=base/other' parameter must match 'base' path");
         }

      }

      @Nested
      class GitFormat {

         @Test
         public void ofUriWhenHttps() {
            assertThat(GitRepositoryUri
                             .ofRepositoryUri("https://user:password@host/path.git#branch")
                             .exportUri(sensitiveParameters())
                             .stringValue()).isEqualTo("https://user:password@host/path.git#branch");
            assertThat(GitRepositoryUri
                             .ofRepositoryUri("https://host/path.git#branch")
                             .exportUri(sensitiveParameters())
                             .stringValue()).isEqualTo("https://host/path.git#branch");
         }

         @Test
         public void ofUriWhenHttp() {
            assertThat(GitRepositoryUri
                             .ofRepositoryUri("http://user:password@host/path.git#branch")
                             .exportUri(sensitiveParameters())
                             .stringValue()).isEqualTo("http://user:password@host/path.git#branch");
            assertThat(GitRepositoryUri
                             .ofRepositoryUri("http://host/path.git#branch")
                             .exportUri(sensitiveParameters())
                             .stringValue()).isEqualTo("http://host/path.git#branch");
         }

         @Test
         public void ofUriWhenGit() {
            assertThat(GitRepositoryUri
                             .ofRepositoryUri("git://user:password@host/path.git#branch")
                             .exportUri(sensitiveParameters())
                             .stringValue()).isEqualTo("git://user:password@host/path.git#branch");
            assertThat(GitRepositoryUri
                             .ofRepositoryUri("git://host/path.git#branch")
                             .exportUri(sensitiveParameters())
                             .stringValue()).isEqualTo("git://host/path.git#branch");
         }

         @Test
         public void ofUriWhenSsh() {
            assertThat(GitRepositoryUri
                             .ofRepositoryUri("ssh://user:password@host/path.git#branch")
                             .exportUri(sensitiveParameters())
                             .stringValue()).isEqualTo("ssh://user:password@host/path.git#branch");
            assertThat(GitRepositoryUri
                             .ofRepositoryUri("ssh://host/path.git#branch")
                             .exportUri(sensitiveParameters())
                             .stringValue()).isEqualTo("ssh://host/path.git#branch");
         }

         @Test
         public void ofUriWhenFile() {
            assertThat(GitRepositoryUri
                             .ofRepositoryUri("file:/path.git#branch")
                             .exportUri(sensitiveParameters())
                             .stringValue()).isEqualTo("file:/path.git#branch");
            assertThat(GitRepositoryUri
                             .ofRepositoryUri("file:///path.git#branch")
                             .exportUri(sensitiveParameters())
                             .stringValue()).isEqualTo("file:///path.git#branch");
            assertThatThrownBy(() -> GitRepositoryUri
                  .ofRepositoryUri("file://host/path.git#branch")
                  .exportUri(sensitiveParameters())
                  .stringValue())
                  .isExactlyInstanceOf(IncompatibleUriException.class)
                  .hasMessage(
                        "Incompatible 'file://host/path.git#branch' URI: 'uri=file://host/path.git#branch' must be compatible with GitUri, HttpUri, SshUri or FileUri");
         }

         @Test
         public void ofUriWhenNoFileScheme() {
            assertThat(GitRepositoryUri
                             .ofRepositoryUri("/path.git#branch")
                             .exportUri(sensitiveParameters())
                             .stringValue()).isEqualTo("/path.git#branch");
            assertThatThrownBy(() -> GitRepositoryUri
                  .ofRepositoryUri("path.git#branch")
                  .exportUri(sensitiveParameters()))
                  .isExactlyInstanceOf(IncompatibleUriException.class)
                  .hasMessage(
                        "Incompatible 'path.git#branch' URI: 'uri=path.git#branch' must be compatible with GitUri, HttpUri, SshUri or FileUri");
         }
      }

      @Nested
      class WrappedUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(GitRepositoryUri.ofRepositoryUri(uri("document:git:https://host/path.git"))).satisfies(
                  uri -> {
                     assertThat(uri.value()).isEqualTo("https://host/path.git");
                  });
         }

         @Test
         public void ofUriWhenBadRepositoryType() {
            assertThatThrownBy(() -> GitRepositoryUri.ofRepositoryUri(uri("document:unknown:https://host/")))
                  .isExactlyInstanceOf(IncompatibleUriException.class)
                  .hasMessage(
                        "Incompatible 'document:unknown:https://host/' URI: Unsupported 'unknown' repository type");
         }

         @Test
         public void ofUriWhenNormalized() {
            assertThat(GitRepositoryUri.ofRepositoryUri(uri("document:git:https://host/path/.././"))).satisfies(
                  uri -> {
                     assertThat(uri.toURI()).isEqualTo(uri("https://host/"));
                  });
            assertThat(GitRepositoryUri.ofRepositoryUri("document:git:https://host/path/../.")).satisfies(uri -> {
               assertThat(uri.toURI()).isEqualTo(uri("https://host/"));
            });
            assertThat(GitRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
                  "document:git:https://host/path/../."))).satisfies(uri -> {
               assertThat(uri.toURI()).isEqualTo(uri("https://host/"));
            });
            assertThatThrownBy(() -> GitRepositoryUri.ofRepositoryUri(uri("document:git:https://host/../path")))
                  .isExactlyInstanceOf(InvalidUriException.class)
                  .hasMessageContainingAll("Invalid 'https://host/../path' URI",
                                           "must not have traversal paths");
         }

         @Test
         public void ofUriWhenUnsupportedWrappedUri() {
            assertThatThrownBy(() -> GitRepositoryUri.ofRepositoryUri(uri("document:git:unknown://host/path")))
                  .isExactlyInstanceOf(IncompatibleUriException.class)
                  .hasMessage(
                        "Incompatible 'unknown://host/path' URI: 'uri=unknown://host/path' must be compatible with GitUri, HttpUri, SshUri or FileUri");
         }

      }

      @Nested
      class WhenLocalUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(GitRepositoryUri.ofRepositoryUri(uri("file:/localpath.git?forceLocalUri#branch"))).satisfies(
                  uri -> {
                     assertThat(uri.localUri()).hasValue(uri("file:/localpath.git"));
                     assertThat(uri.remoteUri()).isEmpty();
                     assertThat(uri.repositoryPath()).isEqualTo(Path.of("/localpath.git"));
                     assertThat(uri.basePath()).isEqualTo(Path.of(""));
                     assertThat(uri.branch()).hasValue("branch");
                  });
         }

         @Test
         public void ofUriWhenNoFileScheme() {
            assertThat(GitRepositoryUri.ofRepositoryUri(uri("/localpath.git?forceLocalUri#branch"))).satisfies(
                  uri -> {
                     assertThat(uri.localUri()).hasValue(uri("/localpath.git"));
                     assertThat(uri.remoteUri()).isEmpty();
                     assertThat(uri.repositoryPath()).isEqualTo(Path.of("/localpath.git"));
                     assertThat(uri.basePath()).isEqualTo(Path.of(""));
                     assertThat(uri.branch()).hasValue("branch");
                  });
         }

         @Test
         public void ofUriWhenNoFileSchemeRelativePath() {
            assertThatThrownBy(() -> GitRepositoryUri.ofRepositoryUri(uri("localpath.git?forceLocalUri#branch")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Incompatible 'localpath.git?forceLocalUri#branch' URI: 'uri=localpath.git?forceLocalUri#branch' must be compatible with GitUri, HttpUri, SshUri or FileUri");
         }

         @Test
         public void ofUriWhenRemoteUri() {
            assertThat(GitRepositoryUri.ofRepositoryUri(uri(
                  "file:/localpath.git?forceLocalUri&remoteUri=https://host/path.git#branch"))).satisfies(uri -> {
               assertThat(uri.localUri()).hasValue(uri("file:/localpath.git"));
               assertThat(uri.remoteUri()).hasValue(uri("https://host/path.git"));
               assertThat(uri.repositoryPath()).isEqualTo(Path.of("/localpath.git"));
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
               assertThat(uri.branch()).hasValue("branch");
            });
         }

         @Test
         public void ofUriWhenOnlyRemoteUri() {
            assertThat(GitRepositoryUri.ofRepositoryUri(uri(
                  "file:/localpath.git?remoteUri=https://host/path.git#branch"))).satisfies(uri -> {
               assertThat(uri.localUri()).hasValue(uri("file:/localpath.git"));
               assertThat(uri.remoteUri()).hasValue(uri("https://host/path.git"));
               assertThat(uri.repositoryPath()).isEqualTo(Path.of("/localpath.git"));
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
               assertThat(uri.branch()).hasValue("branch");
            });
         }

         @Test
         public void ofUriWhenRepositoryAndBasePath() {
            assertThat(GitRepositoryUri.ofRepositoryUri(uri(
                  "file:/localpath/basePath?forceLocalUri&repositoryPath=/localpath&basePath=basePath&remoteUri=https://host/path.git#branch"))).satisfies(
                  uri -> {
                     assertThat(uri.localUri()).hasValue(uri("file:/localpath"));
                     assertThat(uri.remoteUri()).hasValue(uri("https://host/path.git"));
                     assertThat(uri.repositoryPath()).isEqualTo(Path.of("/localpath"));
                     assertThat(uri.basePath()).isEqualTo(Path.of("basePath"));
                     assertThat(uri.branch()).hasValue("branch");
                  });
         }

         @Test
         public void ofUriWhenLocalFsParameters() {
            assertThat(GitRepositoryUri.ofRepositoryUri(uri(
                  "file:/localpath.git?forceLocalUri&remoteUri=https://host/path.git&createStoragePathIfMissing&custom=value#branch"))).satisfies(
                  uri -> {
                     assertThat(uri.localUri()).hasValue(uri(
                           "file:/localpath.git?createStoragePathIfMissing=true&custom=value"));
                     assertThat(uri.remoteUri()).hasValue(uri("https://host/path.git"));
                     assertThat(uri.repositoryPath()).isEqualTo(Path.of("/localpath.git"));
                     assertThat(uri.basePath()).isEqualTo(Path.of(""));
                     assertThat(uri.branch()).hasValue("branch");
                  });
         }

         @Test
         public void ofUriWhenRemoteUriParameters() {
            assertThat(GitRepositoryUri.ofRepositoryUri(uri(
                  "file:/localpath.git?forceLocalUri&remoteUri=https://host/path.git%3FinitIfMissing=false%23remotebranch#branch")))
                  .as("remoteUri parameters/branch must be ignored")
                  .satisfies(uri -> {
                     assertThat(uri.localUri()).hasValue(uri("file:/localpath.git"));
                     assertThat(uri.remoteUri()).hasValue(uri("https://host/path.git"));
                     assertThat(uri.repositoryPath()).isEqualTo(Path.of("/localpath.git"));
                     assertThat(uri.basePath()).isEqualTo(Path.of(""));
                     assertThat(uri.branch()).hasValue("branch");
                     assertThat(uri.query().parameter(INIT_IF_MISSING, true)).hasValue(true);
                  });
         }

         @Test
         public void ofUriWhenLocalUriParameter() {
            assertThatThrownBy(() -> GitRepositoryUri.ofRepositoryUri(uri(
                  "file:/localpath.git?forceLocalUri&localUri=file:/otherpath.git#branch")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'file:/localpath.git?forceLocalUri&localUri=file:/otherpath.git#branch' URI: 'localUri' parameter is not allowed in local URI");
         }

         @Test
         public void ofUriWhenBadLocalUriScheme() {
            assertThatThrownBy(() -> GitRepositoryUri.ofRepositoryUri(uri(
                  "http://host/localpath.git?forceLocalUri#branch")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Incompatible 'http://host/localpath.git?forceLocalUri#branch' URI: 'FileUri[value=http://host/localpath.git?forceLocalUri#branch,restrictions=FileUriRestrictions[relativeFormat=false, relativePath=true, emptyPath=true, query=false, fragment=false],scheme=SimpleScheme[scheme='http'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='//host/localpath.git?forceLocalUri', encodedSchemeSpecific='//host/localpath.git?forceLocalUri'],authority=SimpleServerAuthority[userInfo='null', host='SimpleHost[host='host']', port='null'],path=SimplePath[path=/localpath.git, encodedPath=/localpath.git],query=KeyValueQuery[query={forceLocalUri=[null]}, encodedQuery={forceLocalUri=[null]}],fragment=SimpleFragment[fragment=branch, encodedFragment=branch]]' must be absolute, and scheme must be equal to 'file'");
         }

      }
   }

   @Nested
   public class OfUriWhenRepositoryUri {

      @Nested
      public class WhenRepositoryUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(GitRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri("https://host/path.git"))).satisfies(
                  uri -> {
                  });
         }

         @Test
         public void ofUriWhenRoot() {
            assertThat(GitRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri("https://host/"))).satisfies(
                  uri -> {
                  });
            assertThat(GitRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri("https://host"))).satisfies(
                  uri -> {
                  });
         }

      }

      @Nested
      public class WhenGitRepositoryUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(GitRepositoryUri.ofRepositoryUri(GitRepositoryUri.ofRepositoryUri(
                  "https://host/path.git"))).satisfies(uri -> {
            });
         }

         @Test
         public void ofUriWhenRoot() {
            assertThat(GitRepositoryUri.ofRepositoryUri(GitRepositoryUri.ofRepositoryUri("https://host/"))).satisfies(
                  uri -> {
                  });
            assertThat(GitRepositoryUri.ofRepositoryUri(GitRepositoryUri.ofRepositoryUri("https://host"))).satisfies(
                  uri -> {
                  });
         }

      }

   }

   @Nested
   public class OfConfig {

      @Test
      public void ofConfigWhenNominal() {
         assertThat(GitRepositoryUri.ofConfig(new GitDocumentConfigBuilder()
                                                    .remoteUri(uri("https://host/path.git"))
                                                    .build(), null, null, null, null)).satisfies(uri -> {
            assertThat(uri.localUri()).isEmpty();
            assertThat(uri.remoteUri()).hasValue(uri("https://host/path.git"));
            assertThat(uri.query().parameter(REPOSITORY_PATH, alwaysTrue())).hasValue(Path.of("/path.git"));
            assertThat(uri.query().parameter(BASE_PATH, alwaysTrue())).hasValue(Path.of(""));
            assertThat(uri.query().parameter(REMOTE, alwaysTrue())).hasValue(DEFAULT_REMOTE);
            assertThat(uri.query().parameter(INITIAL_BRANCH, alwaysTrue())).hasValue(DEFAULT_BRANCH);
            assertThat(uri.query().parameter(BRANCH, alwaysTrue())).isEmpty();
            assertThat(uri
                             .query()
                             .parameter(INIT_IF_MISSING, alwaysTrue())).hasValue(DEFAULT_INIT_IF_MISSING);
            assertThat(uri.query().parameter(CLONE_IF_MISSING, alwaysTrue())).hasValue(
                  DEFAULT_CLONE_IF_MISSING);
            assertThat(uri.query().parameter(LOCAL_URI, alwaysTrue())).isEmpty();
         });
      }

      @Test
      public void ofConfigWhenRepositoryPathNotAutodetected() {
         assertThat(GitRepositoryUri.ofConfig(new GitDocumentConfigBuilder()
                                                    .remoteUri(uri("https://host/path"))
                                                    .build(), null, null, null, null)).satisfies(uri -> {
            assertThat(uri.localUri()).isEmpty();
            assertThat(uri.remoteUri()).hasValue(uri("https://host/path"));
            assertThat(uri.query().parameter(REPOSITORY_PATH, alwaysTrue())).hasValue(Path.of("/path"));
            assertThat(uri.query().parameter(BASE_PATH, alwaysTrue())).hasValue(Path.of(""));
            assertThat(uri.query().parameter(REMOTE, alwaysTrue())).hasValue(DEFAULT_REMOTE);
            assertThat(uri.query().parameter(INITIAL_BRANCH, alwaysTrue())).hasValue(DEFAULT_BRANCH);
            assertThat(uri.query().parameter(BRANCH, alwaysTrue())).isEmpty();
            assertThat(uri
                             .query()
                             .parameter(INIT_IF_MISSING, alwaysTrue())).hasValue(DEFAULT_INIT_IF_MISSING);
            assertThat(uri.query().parameter(CLONE_IF_MISSING, alwaysTrue())).hasValue(
                  DEFAULT_CLONE_IF_MISSING);
            assertThat(uri.query().parameter(LOCAL_URI, alwaysTrue())).isEmpty();
         });
      }

      @Test
      public void ofConfigWhenNoRemoteUri() {
         assertThatThrownBy(() -> GitRepositoryUri.ofConfig(new GitDocumentConfigBuilder().build(),
                                                            null,
                                                            null,
                                                            null,
                                                            null))
               .isExactlyInstanceOf(IllegalStateException.class)
               .hasMessage("'dynamicLocalRepository' must be defined");
         assertThat(GitRepositoryUri.ofConfig(new GitDocumentConfigBuilder().build(),
                                              FsDocumentRepository.ofUri(RepositoryUri.ofRepositoryUri(
                                                    "file:/tmp/path")),
                                              null,
                                              null,
                                              null)).satisfies(uri -> {
            assertThat(uri.localUri()).hasValue(uri("file:/tmp/path"));
            assertThat(uri.remoteUri()).isEmpty();
         });
      }

      @Test
      public void ofConfigWhenLocalRepository() {
         assertThat(GitRepositoryUri.ofConfig(new GitDocumentConfigBuilder()
                                                    .remoteUri(uri("https://host/path.git"))
                                                    .localRepository(new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                                                                    .storagePath(
                                                                                                          Path.of(
                                                                                                                "/test/git"))
                                                                                                    .failFastIfMissingStoragePath(
                                                                                                          false)
                                                                                                    .build()))
                                                    .build(), null, null, null, null)).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo(
                  "https://host/path.git?repositoryPath=/path.git&basePath=&localUri=file:/test/git?failFastIfMissingStoragePath=false&remote=origin&initialBranch=main&cloneIfMissing=true&initIfMissing=true&forceLocalUri=false&pullMode=NEVER&pullFastForward=FAST_FORWARD&pullRebase=false&commitMode=NEVER&pushMode=NEVER&connectTimeout=PT10S&disableSslVerify=false&allowUnknownSshHosts=false");

            assertThat(uri.localUri()).hasValue(uri("file:/test/git?failFastIfMissingStoragePath=false"));
         });
      }

      @Test
      public void ofConfigWhenAllParameters() {
         assertThat(GitRepositoryUri.ofConfig(new GitDocumentConfigBuilder()
                                                    .remoteUri(uri("https://host/path.git"))
                                                    .forceLocalUri(false)
                                                    .remote("remote")
                                                    .initialBranch("initial-branch")
                                                    .branch("branch")
                                                    .initIfMissing(false)
                                                    .cloneIfMissing(false)
                                                    .localUri(uri(
                                                          "file:/test/git?failFastIfMissingStoragePath=false"))
                                                    .pullStrategy(new PullStrategyBuilder()
                                                                        .pullMode(PullMode.ALWAYS)
                                                                        .fastForward(NO_FAST_FORWARD)
                                                                        .rebase(true)
                                                                        .shallowDepth(1)
                                                                        .build())
                                                    .pushStrategy(new PushStrategyBuilder()
                                                                        .commitMode(PushMode.ALWAYS)
                                                                        .pushMode(PushMode.ALWAYS)
                                                                        .build())
                                                    .transport(new TransportBuilder()
                                                                     .connectTimeout(Duration.ofMinutes(1))
                                                                     .disableSslVerify(true)
                                                                     .allowUnknownSshHosts(true)
                                                                     .build())
                                                    .build(), null, null, null, null)).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo(
                  "https://host/path.git?repositoryPath=/path.git&basePath=&localUri=file:/test/git?failFastIfMissingStoragePath=false&remote=remote&initialBranch=initial-branch&cloneIfMissing=false&initIfMissing=false&forceLocalUri=false&pullMode=ALWAYS&pullFastForward=NO_FAST_FORWARD&pullRebase=true&pullShallowDepth=1&commitMode=ALWAYS&pushMode=ALWAYS&connectTimeout=PT1M&disableSslVerify=true&allowUnknownSshHosts=true#branch");
         });
      }

      @Test
      public void ofConfigWhenBadConfiguration() {
         assertThatThrownBy(() -> GitRepositoryUri.ofConfig(null, null, null, null, null))
               .isExactlyInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'config' must not be null");
      }

      @Test
      @Disabled("Branch is first read from remote URI then stripped")
      public void ofConfigWhenInvalidBranch() {
         assertThatThrownBy(() -> GitRepositoryUri.ofConfig(new GitDocumentConfigBuilder()
                                                                  .remoteUri(uri("https://host/path#main"))
                                                                  .branch("branch")
                                                                  .build(), null, null, null, null))
               .isExactlyInstanceOf(InvalidUriException.class)
               .hasMessage("");
      }

      @Test
      public void ofConfigWhenLocalUri() {
         assertThat(GitRepositoryUri.ofConfig(new GitDocumentConfigBuilder()
                                                    .localUri(uri("file:/test/git"))
                                                    .build(), null, null, null, null)).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo(
                  "file:/test/git?repositoryPath=/test/git&basePath=&remote=origin&initialBranch=main&cloneIfMissing=true&initIfMissing=true&forceLocalUri=true&pullMode=NEVER&pullFastForward=FAST_FORWARD&pullRebase=false&commitMode=NEVER&pushMode=NEVER&connectTimeout=PT10S&disableSslVerify=false&allowUnknownSshHosts=false");

            assertThat(uri.localUri()).hasValue(uri("file:/test/git"));
            assertThat(uri.remoteUri()).isEmpty();
            assertThat(uri.query().parameter(FORCE_LOCAL_URI, false)).hasValue(true);
         });
      }

      @Test
      public void ofConfigWhenLocalUriWithParameters() {
         assertThat(GitRepositoryUri.ofConfig(new GitDocumentConfigBuilder()
                                                    .localUri(uri(
                                                          "file:/test/git?failFastIfMissingStoragePath=false"))
                                                    .build(), null, null, null, null)).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo(
                  "file:/test/git?failFastIfMissingStoragePath=false&repositoryPath=/test/git&basePath=&remote=origin&initialBranch=main&cloneIfMissing=true&initIfMissing=true&forceLocalUri=true&pullMode=NEVER&pullFastForward=FAST_FORWARD&pullRebase=false&commitMode=NEVER&pushMode=NEVER&connectTimeout=PT10S&disableSslVerify=false&allowUnknownSshHosts=false");

            assertThat(uri.localUri()).hasValue(uri("file:/test/git?failFastIfMissingStoragePath=false"));
            assertThat(uri.remoteUri()).isEmpty();
            assertThat(uri.query().parameter(FORCE_LOCAL_URI, false)).hasValue(true);
         });
      }

      @Test
      public void ofConfigWhenLocalUriButRemoteUri() {
         assertThat(GitRepositoryUri.ofConfig(new GitDocumentConfigBuilder()
                                                    .localUri(uri("file:/test/git"))
                                                    .remoteUri(uri("https://host/path.git"))
                                                    .forceLocalUri(true)
                                                    .build(), null, null, null, null)).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo(
                  "file:/test/git?repositoryPath=/test/git&basePath=&remoteUri=https://host/path.git&remote=origin&initialBranch=main&cloneIfMissing=true&initIfMissing=true&forceLocalUri=true&pullMode=NEVER&pullFastForward=FAST_FORWARD&pullRebase=false&commitMode=NEVER&pushMode=NEVER&connectTimeout=PT10S&disableSslVerify=false&allowUnknownSshHosts=false");

            assertThat(uri.localUri()).hasValue(uri("file:/test/git"));
            assertThat(uri.remoteUri()).hasValue(uri("https://host/path.git"));
            assertThat(uri.query().parameter(FORCE_LOCAL_URI, false)).hasValue(true);
         });
      }

      @Test
      public void ofConfigWhenLocalUriButRemoteUriWithParameters() {
         assertThat(GitRepositoryUri.ofConfig(new GitDocumentConfigBuilder()
                                                    .localUri(uri("file:/test/git"))
                                                    .remoteUri(uri("https://host/path.git?custom"))
                                                    .forceLocalUri(true)
                                                    .build(), null, null, null, null)).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo(
                  "file:/test/git?repositoryPath=/test/git&basePath=&remoteUri=https://host/path.git?custom&remote=origin&initialBranch=main&cloneIfMissing=true&initIfMissing=true&forceLocalUri=true&pullMode=NEVER&pullFastForward=FAST_FORWARD&pullRebase=false&commitMode=NEVER&pushMode=NEVER&connectTimeout=PT10S&disableSslVerify=false&allowUnknownSshHosts=false");

            assertThat(uri.localUri()).hasValue(uri("file:/test/git"));
            assertThat(uri.remoteUri())
                  .as("remoteUri parameters are discarded")
                  .hasValue(uri("https://host/path.git"));
            assertThat(uri.query().parameter(FORCE_LOCAL_URI, false)).hasValue(true);
         });
      }

      @Test
      public void ofConfigWhenLocalUriWithDynamicParameters() {
         assertThat(GitRepositoryUri.ofConfig(new GitDocumentConfigBuilder()
                                                    .localUri(uri("file:/test/git"))
                                                    .build(),
                                              null,
                                              "dynremote",
                                              uri("https://host/path.git"),
                                              "branch")).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo(
                  "https://host/path.git?repositoryPath=/path.git&basePath=&localUri=file:/test/git&remote=dynremote&initialBranch=main&cloneIfMissing=true&initIfMissing=true&forceLocalUri=false&pullMode=NEVER&pullFastForward=FAST_FORWARD&pullRebase=false&commitMode=NEVER&pushMode=NEVER&connectTimeout=PT10S&disableSslVerify=false&allowUnknownSshHosts=false#branch");

            assertThat(uri.localUri()).hasValue(uri("file:/test/git"));
            assertThat(uri.remoteUri()).hasValue(uri("https://host/path.git"));
            assertThat(uri.query().parameter(FORCE_LOCAL_URI, false)).hasValue(false);
         });
         assertThat(GitRepositoryUri.ofConfig(new GitDocumentConfigBuilder()
                                                    .localUri(uri("file:/test/git"))
                                                    .forceLocalUri(true)
                                                    .build(),
                                              null,
                                              "dynremote",
                                              uri("https://host/path.git"),
                                              "branch")).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo(
                  "file:/test/git?repositoryPath=/test/git&basePath=&remoteUri=https://host/path.git&remote=dynremote&initialBranch=main&cloneIfMissing=true&initIfMissing=true&forceLocalUri=true&pullMode=NEVER&pullFastForward=FAST_FORWARD&pullRebase=false&commitMode=NEVER&pushMode=NEVER&connectTimeout=PT10S&disableSslVerify=false&allowUnknownSshHosts=false#branch");

            assertThat(uri.localUri()).hasValue(uri("file:/test/git"));
            assertThat(uri.remoteUri()).hasValue(uri("https://host/path.git"));
            assertThat(uri.query().parameter(FORCE_LOCAL_URI, false)).hasValue(true);
         });
      }

   }

   @Nested
   public class ToConfig {

      @Test
      public void toConfigWhenNominal() {
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("https://user:password@host/path.git")
                          .toConfig()).satisfies(config -> {
            assertThat(config.localUri()).isEmpty();
            assertThat(config.remoteUri()).hasValue(uri("https://host/path.git"));
            assertThat(config.remote()).hasValue(DEFAULT_REMOTE);
            assertThat(config.initialBranch()).hasValue(DEFAULT_BRANCH);
            assertThat(config.branch()).isEmpty();
            assertThat(config.cloneIfMissing()).hasValue(DEFAULT_CLONE_IF_MISSING);
            assertThat(config.initIfMissing()).hasValue(DEFAULT_INIT_IF_MISSING);
            assertThat(config.remoteAuthentication()).hasValue(Authentication.of("user", "password"));
         });
      }

      @Test
      public void toConfigWhenNoPassword() {
         assertThat(GitRepositoryUri.ofRepositoryUri("https://user@host/path.git").toConfig()).satisfies(
               config -> {
                  assertThat(config.remoteAuthentication()).hasValue(Authentication.of("user"));
               });
      }

      @Test
      public void toConfigWhenAllParameters() {
         assertThat(GitRepositoryUri
                          .ofRepositoryUri(
                                "https://user:password@host/path.git?branch=branch&localUri=file:/test/git%3FfailFastIfMissingStoragePath=false&cloneIfMissing=false&initIfMissing=false&remote=remote")
                          .toConfig()).satisfies(config -> {
            assertThat(config.branch()).hasValue("branch");
            assertThat(config.localUri()).hasValue(uri("file:/test/git?failFastIfMissingStoragePath=false"));
            assertThat(config.remoteUri()).hasValue(uri("https://host/path.git"));
            assertThat(config.remoteAuthentication()).hasValue(Authentication.of("user", "password"));
            assertThat(config.cloneIfMissing()).hasValue(false);
            assertThat(config.initIfMissing()).hasValue(false);
            assertThat(config.remote()).hasValue("remote");
         });
      }

      @Test
      public void toConfigWhenLocalUri() {
         assertThat(GitRepositoryUri
                          .ofRepositoryUri(
                                "file:/test/git?failFastIfMissingStoragePath=false&remoteUri=https://user:password@host/path.git&branch=branch&cloneIfMissing=false&initIfMissing=false&remote=remote")
                          .toConfig()).satisfies(config -> {
            assertThat(config.branch()).hasValue("branch");
            assertThat(config.localUri()).hasValue(uri("file:/test/git?failFastIfMissingStoragePath=false"));
            assertThat(config.remoteUri()).hasValue(uri("https://host/path.git"));
            assertThat(config.remoteAuthentication()).hasValue(Authentication.of("user", "password"));
            assertThat(config.cloneIfMissing()).hasValue(false);
            assertThat(config.initIfMissing()).hasValue(false);
            assertThat(config.remote()).hasValue("remote");
         });
      }

   }

   @Nested
   public class ExportUri {

      @Test
      public void exportUriWhenNominal() {
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("https://host/")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("https://host/");
      }

      @Test
      public void exportUriWhenRepositoryPath() {
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("https://host/path.git/base")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("https://host/path.git/base");
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("https://host/path.git/base?repositoryPath=/path.git")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("https://host/path.git/base");
      }

      @Test
      public void exportUriWhenNotAutodetectableRepositoryPath() {
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("https://host/path/base")
                          .exportUri(defaultParameters())
                          .stringValue())
               .as("auto-detected repositoryPath=/path/base and basePath='' so that these parameters does not need to be exported")
               .isEqualTo("https://host/path/base");
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("https://host/path/base?repositoryPath=/path")
                          .exportUri(defaultParameters())
                          .stringValue())
               .as("auto-detected repositoryPath=/path/base and basePath='base' so that these parameters must be exported")
               .isEqualTo("https://host/path/base?repositoryPath=/path&basePath=base");
      }

      @Test
      public void exportUriWhenBasePath() {
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("https://host/path.git/base?basePath=base")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("https://host/path.git/base");
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("https://host/path.git?basePath=base")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("https://host/path.git?basePath=base");
      }

      @Test
      public void exportUriWhenBranch() {
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("https://host/path.git#main")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("https://host/path.git#main");
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("https://host/path.git#main")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("https://host/path.git#main");
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("https://host/path.git#main")
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo(
               "https://host/path.git?repositoryPath=/path.git&basePath=&remote=origin&initialBranch=main&cloneIfMissing=true&initIfMissing=true&forceLocalUri=false&pullMode=NEVER&pullFastForward=FAST_FORWARD&pullRebase=false&commitMode=NEVER&pushMode=NEVER&connectTimeout=PT10S&disableSslVerify=false&allowUnknownSshHosts=false#main");
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("https://host/path.git?branch=main")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("https://host/path.git#main");
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("https://host/path.git?branch=main")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("https://host/path.git#main");
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("https://host/path.git?branch=main")
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo(
               "https://host/path.git?repositoryPath=/path.git&basePath=&remote=origin&initialBranch=main&cloneIfMissing=true&initIfMissing=true&forceLocalUri=false&pullMode=NEVER&pullFastForward=FAST_FORWARD&pullRebase=false&commitMode=NEVER&pushMode=NEVER&connectTimeout=PT10S&disableSslVerify=false&allowUnknownSshHosts=false#main");
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("https://host/path.git#branch")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("https://host/path.git#branch");
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("https://host/path.git#branch")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("https://host/path.git#branch");
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("https://host/path.git#branch")
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo(
               "https://host/path.git?repositoryPath=/path.git&basePath=&remote=origin&initialBranch=main&cloneIfMissing=true&initIfMissing=true&forceLocalUri=false&pullMode=NEVER&pullFastForward=FAST_FORWARD&pullRebase=false&commitMode=NEVER&pushMode=NEVER&connectTimeout=PT10S&disableSslVerify=false&allowUnknownSshHosts=false#branch");
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("https://host/path.git?branch=branch")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("https://host/path.git#branch");
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("https://host/path.git?branch=branch")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("https://host/path.git#branch");
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("https://host/path.git?branch=branch")
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo(
               "https://host/path.git?repositoryPath=/path.git&basePath=&remote=origin&initialBranch=main&cloneIfMissing=true&initIfMissing=true&forceLocalUri=false&pullMode=NEVER&pullFastForward=FAST_FORWARD&pullRebase=false&commitMode=NEVER&pushMode=NEVER&connectTimeout=PT10S&disableSslVerify=false&allowUnknownSshHosts=false#branch");
      }

      @Test
      public void exportUriWhenDefaultParameters() {
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("https://user:password@host/path.git")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("https://host/path.git");
      }

      @Test
      public void exportUriWhenDefaultParametersWithForceDefaultValues() {
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("https://user:password@host/path.git")
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo(
               "https://host/path.git?repositoryPath=/path.git&basePath=&remote=origin&initialBranch=main&cloneIfMissing=true&initIfMissing=true&forceLocalUri=false&pullMode=NEVER&pullFastForward=FAST_FORWARD&pullRebase=false&commitMode=NEVER&pushMode=NEVER&connectTimeout=PT10S&disableSslVerify=false&allowUnknownSshHosts=false");
      }

      @Test
      public void exportUriWhenSensitiveParameters() {
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("https://user:password@host/path.git")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("https://user:password@host/path.git");
      }

      @Test
      public void exportUriWhenCustomQuery() {
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("https://host/path.git?custom=value&cloneIfMissing=true")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("https://host/path.git?custom=value");
      }

      @Test
      public void exportUriWhenNoPassword() {
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("https://user@host/path.git")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("https://host/path.git");
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("https://user@host/path.git")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("https://user@host/path.git");
      }

      @Test
      public void exportUriWhenOfficialPorts() {
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("https://host:443/path.git")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("https://host/path.git");
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("https://host:444/path.git")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("https://host:444/path.git");
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("http://host:80/path.git")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("http://host/path.git");
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("http://host:81/path.git")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("http://host:81/path.git");
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("git://host:9418/path.git")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("git://host/path.git");
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("git://host:9419/path.git")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("git://host:9419/path.git");
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("ssh://host:22/path.git")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("ssh://host/path.git");
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("ssh://host:23/path.git")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("ssh://host:23/path.git");
      }

      @Test
      public void exportUriWhenVariousGitFormat() {
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("https://user:password@host/path.git")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("https://user:password@host/path.git");
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("https://host/path.git")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("https://host/path.git");
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("http://user:password@host/path.git")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("http://user:password@host/path.git");
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("http://host/path.git")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("http://host/path.git");
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("git://user:password@host/path.git")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("git://user:password@host/path.git");
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("git://host/path.git")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("git://host/path.git");
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("ssh://user:password@host/path.git")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("ssh://user:password@host/path.git");
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("ssh://host/path.git")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("ssh://host/path.git");
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("file:/path.git")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("file:/path.git");
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("file:///path.git")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("file:///path.git");
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("/path.git")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("/path.git");
      }

      @Test
      public void exportUriWhenConfigRemoteAuthentication() {
         assertThat(GitRepositoryUri
                          .ofConfig(new GitDocumentConfigBuilder()
                                          .remoteUri(uri("https://host/path.git"))
                                          .remoteAuthentication(Authentication.of("user", "password"))
                                          .build(), null, null, null, null)
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("https://user:password@host/path.git");
         assertThat(GitRepositoryUri
                          .ofConfig(new GitDocumentConfigBuilder()
                                          .remoteUri(uri("https://host/path.git"))
                                          .remoteAuthentication(Authentication.of("us:@er", "pass:@word"))
                                          .build(), null, null, null, null)
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("https://us%3A%40er:pass%3A%40word@host/path.git");
      }

      @Test
      public void exportUriWhenLocalUriParameter() {
         assertThat(GitRepositoryUri
                          .ofRepositoryUri(
                                "https://host/path.git?localUri=file:/test/git?failFastIfMissingStoragePath=false")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo(
               "https://host/path.git?localUri=file:/test/git?failFastIfMissingStoragePath=false");
      }

      @Test
      public void exportUriWhenLocalUri() {
         assertThat(GitRepositoryUri
                          .ofRepositoryUri(
                                "file:/test/git?failFastIfMissingStoragePath=false&remoteUri=https://host/path.git")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo(
               "file:/test/git?failFastIfMissingStoragePath=false&remoteUri=https://host/path.git");
      }

   }

   @Nested
   public class NormalizeResolveRelativize {

      @Test
      public void normalizeWhenNominal() {
         assertThat(GitRepositoryUri.ofRepositoryUri("https://host/path/../.").normalize()).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo("https://host/");
         });
      }

      @Test
      public void resolveWhenNominal() {
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("https://host/path.git")
                          .resolve(uri("otherpath"))).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo("https://host/otherpath");
         });
         assertThat(GitRepositoryUri
                          .ofRepositoryUri("https://host/path.git/")
                          .resolve(uri("otherpath"))).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo("https://host/path.git/otherpath");
         });
      }

      @Test
      public void relativizeWhenNominal() {
         assertThatThrownBy(() -> GitRepositoryUri
               .ofRepositoryUri("https://host/path.git")
               .relativize(uri("https://host/path.git/otherpath"))).isExactlyInstanceOf(
               UnsupportedOperationException.class);
      }

   }

}