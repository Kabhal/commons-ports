/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.git;

import static com.tinubu.commons.ddd2.uri.Uri.uri;
import static com.tinubu.commons.lang.util.CheckedPredicate.alwaysTrue;
import static com.tinubu.commons.lang.util.PathUtils.emptyPath;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.defaultParameters;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.noParameters;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.sensitiveParameters;
import static com.tinubu.commons.ports.document.git.Constants.DEFAULT_BRANCH;
import static com.tinubu.commons.ports.document.git.Constants.DEFAULT_CLONE_IF_MISSING;
import static com.tinubu.commons.ports.document.git.Constants.DEFAULT_INIT_IF_MISSING;
import static com.tinubu.commons.ports.document.git.Constants.DEFAULT_REMOTE;
import static com.tinubu.commons.ports.document.git.FastForward.NO_FAST_FORWARD;
import static com.tinubu.commons.ports.document.git.GitRepositoryUri.Parameters.BASE_PATH;
import static com.tinubu.commons.ports.document.git.GitRepositoryUri.Parameters.BRANCH;
import static com.tinubu.commons.ports.document.git.GitRepositoryUri.Parameters.CLONE_IF_MISSING;
import static com.tinubu.commons.ports.document.git.GitRepositoryUri.Parameters.FORCE_LOCAL_URI;
import static com.tinubu.commons.ports.document.git.GitRepositoryUri.Parameters.INITIAL_BRANCH;
import static com.tinubu.commons.ports.document.git.GitRepositoryUri.Parameters.INIT_IF_MISSING;
import static com.tinubu.commons.ports.document.git.GitRepositoryUri.Parameters.LOCAL_URI;
import static com.tinubu.commons.ports.document.git.GitRepositoryUri.Parameters.REMOTE;
import static com.tinubu.commons.ports.document.git.GitRepositoryUri.Parameters.REPOSITORY_PATH;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.net.URI;
import java.nio.file.Path;
import java.time.Duration;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ddd2.uri.parts.SimpleQuery;
import com.tinubu.commons.ddd2.uri.uris.HttpUri;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.RepositoryUri;
import com.tinubu.commons.ports.document.fs.FsDocumentConfig.FsDocumentConfigBuilder;
import com.tinubu.commons.ports.document.fs.FsDocumentRepository;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.Authentication;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.GitDocumentConfigBuilder;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.PullStrategy.PullMode;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.PullStrategy.PullStrategyBuilder;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.PushStrategy.PushMode;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.PushStrategy.PushStrategyBuilder;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.Transport.TransportBuilder;

public class GitDocumentUriTest {

   @Nested
   public class Of {

      @Test
      public void ofWhenNominal() {
         assertThat(GitDocumentUri.of(GitRepositoryUri.ofRepositoryUri(HttpUri.ofUri("https://host/path.git")),
                                      DocumentPath.of("path/document"),
                                      null)).satisfies(uri -> {
            assertThat(uri.localUri()).isEmpty();
            assertThat(uri.remoteUri()).hasValue(Uri.uri("https://host/path.git"));
            assertThat(uri.repositoryPath()).isEqualTo(Path.of("/path.git"));
            assertThat(uri.basePath()).isEqualTo(Path.of(""));
            assertThat(uri.branch()).isEmpty();
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/document"));
         });
      }

      @Test
      public void ofWhenGitDocumentUri() {
         assertThat(GitDocumentUri.of(GitDocumentUri.ofDocumentUri(HttpUri.ofUri(
               "https://host/path.git/path0/document0")), DocumentPath.of("path/document"), null)).satisfies(
               uri -> {
                  assertThat(uri.localUri()).isEmpty();
                  assertThat(uri.remoteUri()).hasValue(Uri.uri("https://host/path.git"));
                  assertThat(uri.repositoryPath()).isEqualTo(Path.of("/path.git"));
                  assertThat(uri.basePath()).isEqualTo(Path.of(""));
                  assertThat(uri.branch()).isEmpty();
                  assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/document"));
               });
      }

      @Test
      public void ofWhenBranchAndBasePathInRemoteUri() {
         assertThat(GitDocumentUri.of(GitRepositoryUri.ofRepositoryUri(HttpUri.ofUri(
                                            "https://host/path.git/base?basePath=base#branch")),
                                      DocumentPath.of("path/document"),
                                      null)).satisfies(uri -> {
            assertThat(uri.localUri()).isEmpty();
            assertThat(uri.remoteUri()).hasValue(Uri.uri("https://host/path.git"));
            assertThat(uri.basePath()).isEqualTo(Path.of("base"));
            assertThat(uri.branch()).hasValue("branch");
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/document"));
         });
         assertThat(GitDocumentUri.of(GitRepositoryUri.ofRepositoryUri(HttpUri.ofUri(
                                            "https://host/path.git?basePath=base#branch")),
                                      DocumentPath.of("path/document"),
                                      null)).satisfies(uri -> {
            assertThat(uri.localUri()).isEmpty();
            assertThat(uri.remoteUri()).hasValue(Uri.uri("https://host/path.git"));
            assertThat(uri.basePath()).isEqualTo(Path.of("base"));
            assertThat(uri.branch()).hasValue("branch");
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/document"));
         });
      }

      @Test
      public void ofWhenNotAutodetectedRepositoryPath() {
         assertThat(GitDocumentUri.of(GitRepositoryUri.ofRepositoryUri(HttpUri.ofUri(
               "https://host/path#branch")), DocumentPath.of("path/document"), null)).satisfies(uri -> {
            assertThat(uri.localUri()).isEmpty();
            assertThat(uri.remoteUri()).hasValue(Uri.uri("https://host/path"));
            assertThat(uri.repositoryPath()).isEqualTo(Path.of("/path"));
            assertThat(uri.basePath()).isEqualTo(Path.of(""));
            assertThat(uri.branch()).hasValue("branch");
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/document"));
         });
         assertThat(GitDocumentUri.of(GitRepositoryUri.ofRepositoryUri(HttpUri.ofUri(
               "https://host/path?basePath=base#branch")), DocumentPath.of("path/document"), null)).satisfies(
               uri -> {
                  assertThat(uri.localUri()).isEmpty();
                  assertThat(uri.remoteUri()).hasValue(Uri.uri("https://host/path"));
                  assertThat(uri.basePath()).isEqualTo(Path.of("base"));
                  assertThat(uri.branch()).hasValue("branch");
                  assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/document"));
               });
      }

      @Test
      public void ofWhenQuery() {
         assertThat(GitDocumentUri.of(GitRepositoryUri.ofRepositoryUri(HttpUri.ofUri("https://host/path/base")),
                                      DocumentPath.of("path/document"),
                                      SimpleQuery.of("repositoryPath=/path&basePath=base&branch=branch"))).satisfies(
               uri -> {
                  assertThat(uri.localUri()).isEmpty();
                  assertThat(uri.remoteUri()).hasValue(Uri.uri("https://host/path"));
                  assertThat(uri.repositoryPath()).isEqualTo(Path.of("/path"));
                  assertThat(uri.basePath()).isEqualTo(Path.of("base"));
                  assertThat(uri.branch()).hasValue("branch");
                  assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/document"));
               });
         assertThat(GitDocumentUri.of(GitRepositoryUri.ofRepositoryUri(HttpUri.ofUri(
                                            "https://host/path/base?repositoryPath=/path&basePath=base")),
                                      DocumentPath.of("path/document"),
                                      SimpleQuery.of("branch=branch"))).satisfies(uri -> {
            assertThat(uri.localUri()).isEmpty();
            assertThat(uri.remoteUri()).hasValue(Uri.uri("https://host/path"));
            assertThat(uri.basePath()).isEqualTo(Path.of("base"));
            assertThat(uri.branch()).hasValue("branch");
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/document"));
         });
      }

   }

   @Nested
   public class OfUriWhenURI {

      @Test
      public void ofUriWhenNominal() {
         assertThat(GitDocumentUri.ofDocumentUri(uri("https://host/path.git/path/document"))).satisfies(uri -> {
            assertThat(uri.localUri()).isEmpty();
            assertThat(uri.remoteUri()).hasValue(Uri.uri("https://host/path.git"));
            assertThat(uri.repositoryPath()).isEqualTo(Path.of("/path.git"));
            assertThat(uri.basePath()).isEqualTo(Path.of(""));
            assertThat(uri.branch()).isEmpty();
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/document"));
         });
         assertThat(GitDocumentUri.ofDocumentUri("https://host/path.git/path/document")).satisfies(uri -> {
            assertThat(uri.localUri()).isEmpty();
            assertThat(uri.remoteUri()).hasValue(Uri.uri("https://host/path.git"));
            assertThat(uri.repositoryPath()).isEqualTo(Path.of("/path.git"));
            assertThat(uri.basePath()).isEqualTo(Path.of(""));
            assertThat(uri.branch()).isEmpty();
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/document"));
         });
      }

      @Test
      public void ofUriWhenEmptyRepositoryPath() {
         assertThatThrownBy(() -> GitDocumentUri.ofDocumentUri("https://host/path/document"))
               .isExactlyInstanceOf(InvalidUriException.class)
               .hasMessage("Invalid 'https://host/path/document' URI: Missing 'repositoryPath' parameter");
         assertThat(GitDocumentUri.ofDocumentUri("https://host/path/document?repositoryPath=")).satisfies(uri -> {
            assertThat(uri.localUri()).isEmpty();
            assertThat(uri.remoteUri()).hasValue(Uri.uri("https://host"));
            assertThat(uri.repositoryPath()).isEqualTo(Path.of(""));
            assertThat(uri.basePath()).isEqualTo(Path.of(""));
            assertThat(uri.branch()).isEmpty();
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/document"));
         });
      }

      @Test
      public void ofUriWhenRootRepositoryPath() {
         assertThat(GitDocumentUri.ofDocumentUri("https://host/path/document?repositoryPath=/")).satisfies(uri -> {
            assertThat(uri.localUri()).isEmpty();
            assertThat(uri.remoteUri()).hasValue(Uri.uri("https://host/"));
            assertThat(uri.repositoryPath()).isEqualTo(Path.of("/"));
            assertThat(uri.basePath()).isEqualTo(Path.of(""));
            assertThat(uri.branch()).isEmpty();
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/document"));
         });
      }

      @Test
      public void ofUriWhenAuthority() {
         assertThat(GitDocumentUri.ofDocumentUri(uri("https://user:password@host/path.git/path/document"))).satisfies(
               uri -> { });
      }

      @Test
      public void ofUriWhenBadParameters() {
         assertThatThrownBy(() -> GitDocumentUri.ofDocumentUri((URI) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
         assertThatThrownBy(() -> GitDocumentUri.ofDocumentUri((String) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
      }

      @Test
      public void ofUriWhenIllegalArgument() {
         assertThatExceptionOfType(InvalidUriException.class)
               .isThrownBy(() -> GitDocumentUri.ofDocumentUri(
                     "https://host/path/document?localUri=user@git.com:repo.git"))
               .withMessage(
                     "Invalid '%s' URI: Conversion from 'java.lang.String' to 'java.net.URI' failed for 'user@git.com:repo.git' value",
                     "https://host/path/document?localUri=user@git.com:repo.git");
      }

      @Test
      public void ofUriWhenCaseSensitiveParameterKey() {
         assertThat(GitDocumentUri.ofDocumentUri("https://host/path.git/path/document?rEpOsItOryPaTh=/")).satisfies(
               uri -> {
                  assertThat(uri.localUri()).isEmpty();
                  assertThat(uri.localUri()).isEmpty();
                  assertThat(uri.remoteUri()).hasValue(Uri.uri("https://host/path.git"));
               });
      }

      @Test
      public void ofUriWhenNormalized() {
         assertThat(GitDocumentUri.ofDocumentUri(uri("https://host/path.git/path/document/../."))).satisfies(
               uri -> {
                  assertThat(uri.localUri()).isEmpty();
                  assertThat(uri.remoteUri()).hasValue(Uri.uri("https://host/path.git"));
                  assertThat(uri.basePath()).isEqualTo(Path.of(""));
                  assertThat(uri.documentId()).isEqualTo(DocumentPath.of("/path"));
               });
         assertThat(GitDocumentUri.ofDocumentUri("https://host/path.git/path/document/../.")).satisfies(uri -> {
            assertThat(uri.localUri()).isEmpty();
            assertThat(uri.remoteUri()).hasValue(Uri.uri("https://host/path.git"));
            assertThat(uri.basePath()).isEqualTo(Path.of(""));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("/path"));
         });
         assertThat(GitDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(
               "https://host/path.git/path/document/../."))).satisfies(uri -> {
            assertThat(uri.localUri()).isEmpty();
            assertThat(uri.remoteUri()).hasValue(Uri.uri("https://host/path.git"));
            assertThat(uri.basePath()).isEqualTo(Path.of(""));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("/path"));
         });
      }

      @Test
      public void ofUriWhenInvalidUriSyntax() {
         assertThatThrownBy(() -> GitDocumentUri.ofDocumentUri(Uri.uri("git@git.com:user/repo.git")))
               .isExactlyInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'git@git.com:user/repo.git' URI: Illegal character in scheme name at index 3: git@git.com:user/repo.git");
         assertThatThrownBy(() -> GitDocumentUri.ofDocumentUri("git@git.com:user/repo.git"))
               .isExactlyInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'git@git.com:user/repo.git' URI: Illegal character in scheme name at index 3: git@git.com:user/repo.git");
         assertThatThrownBy(() -> GitDocumentUri.ofDocumentUri("https://host/path.git"))
               .isExactlyInstanceOf(InvalidUriException.class)
               .hasMessageContainingAll("Invalid 'https://host/path.git' URI: Missing document id");
      }

      @Test
      public void ofUriWhenIncompatibleUri() {
         assertThatThrownBy(() -> GitDocumentUri.ofDocumentUri("unknown:/path"))
               .isExactlyInstanceOf(IncompatibleUriException.class)
               .hasMessage(
                     "Incompatible 'unknown:/path' URI: 'uri=unknown:/path' must be compatible with GitUri, HttpUri, SshUri or FileUri");
      }

      @Test
      @Disabled("No way to produce InvalidUriException with an invalid URI")
      public void ofUriWhenInvalidUri() {
      }

      @Test
      public void ofUriWhenBranch() {
         assertThat(GitDocumentUri.ofDocumentUri(uri("https://host/path.git/path/document"))).satisfies(uri -> {
            assertThat(uri.branch()).isEmpty();
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/document"));
         });
         assertThat(GitDocumentUri.ofDocumentUri(uri("https://host/path.git/path/document#branch"))).satisfies(
               uri -> {
                  assertThat(uri.branch()).hasValue("branch");
                  assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/document"));
               });
         assertThat(GitDocumentUri.ofDocumentUri(uri("https://host/path.git/path/document?branch=branch"))).satisfies(
               uri -> {
                  assertThat(uri.branch()).hasValue("branch");
                  assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/document"));
               });
      }

      @Test
      public void ofUriWhenInvalidBranchParameter() {
         assertThatThrownBy(() -> GitDocumentUri.ofDocumentUri(uri(
               "https://host/path.git/path/document?branch=branch#main")))
               .isExactlyInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'https://host/path.git/path/document?branch=branch#main' URI: Inconsistent branch value between URI 'main' fragment and 'branch' branch parameter");
      }

      @Test
      public void ofUriWhenDefaultParameters() {
         assertThat(GitDocumentUri.ofDocumentUri("https://host/path.git/path/document")).satisfies(uri -> {
            assertThat(uri.branch()).isEmpty();
            assertThat(uri.query().parameter(INITIAL_BRANCH, alwaysTrue())).hasValue(DEFAULT_BRANCH);
            assertThat(uri.query().parameter(BRANCH, alwaysTrue())).isEmpty();
            assertThat(uri.query().parameter(REMOTE, alwaysTrue())).hasValue(DEFAULT_REMOTE);
            assertThat(uri.query().parameter(LOCAL_URI, alwaysTrue())).isEmpty();
            assertThat(uri.query().parameter(CLONE_IF_MISSING, alwaysTrue())).hasValue(
                  DEFAULT_CLONE_IF_MISSING);
            assertThat(uri
                             .query()
                             .parameter(INIT_IF_MISSING, alwaysTrue())).hasValue(DEFAULT_INIT_IF_MISSING);
         });
      }

      @Test
      public void ofUriWhenAllParameters() {
         assertThat(GitDocumentUri.ofDocumentUri(
               "https://host/path.git/path/document?branch=branch&remote=remote&initialBranch=initial-branch&localUri=file://local&cloneIfMissing=false&initIfMissing=false")).satisfies(
               uri -> {
                  assertThat(uri.branch()).hasValue("branch");
                  assertThat(uri.query().parameter(INITIAL_BRANCH, alwaysTrue())).hasValue("initial-branch");
                  assertThat(uri.query().parameter(BRANCH, alwaysTrue())).hasValue("branch");
                  assertThat(uri.query().parameter(REMOTE, alwaysTrue())).hasValue("remote");
                  assertThat(uri.query().parameter(LOCAL_URI, alwaysTrue())).hasValue(uri("file://local"));
                  assertThat(uri.query().parameter(CLONE_IF_MISSING, alwaysTrue())).hasValue(false);
                  assertThat(uri.query().parameter(INIT_IF_MISSING, alwaysTrue())).hasValue(false);

               });

      }

      @Test
      public void ofUriWhenLocalUriParameter() {
         assertThat(GitDocumentUri
                          .ofDocumentUri("https://host/path.git/path/document?localUri=file:/local")
                          .toConfig()
                          .build()).satisfies(config -> {
            assertThat(config.localUri()).hasValue(uri("file:/local"));
         });
         assertThat(GitDocumentUri
                          .ofDocumentUri("https://host/path.git/path/document?localUri=/local")
                          .toConfig()
                          .build()).satisfies(config -> {
            assertThat(config.localUri()).hasValue(uri("/local"));
         });
         assertThatThrownBy(() -> GitDocumentUri
               .ofDocumentUri("https://host/path.git/path/document?localUri=ssh://localhost/path")
               .toConfig()
               .build())
               .isExactlyInstanceOf(InvariantValidationException.class)
               .hasMessageContainingAll("Invariant validation error >",
                                        "{localUri} 'localUri=ssh://localhost/path' URI must be filesystem document repository URI",
                                        "Incompatible 'ssh://localhost/path' URI",
                                        "must be absolute, and scheme must be equal to 'file'");

      }

      @Nested
      class RepositoryPathParameter {

         @Test
         public void ofUriWhenNominal() {
            assertThat(GitDocumentUri.ofDocumentUri(
                  "https://host/path.git/path/document?repositoryPath=/path.git")).satisfies(uri -> {
               assertThat(uri.localUri()).isEmpty();
               assertThat(uri.remoteUri()).hasValue(Uri.uri("https://host/path.git"));
               assertThat(uri.repositoryPath()).isEqualTo(Path.of("/path.git"));
               assertThat(uri.basePath()).isEqualTo(emptyPath());
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/document"));
            });
         }

         @Test
         public void ofUriWhenAutodetected() {
            assertThat(GitDocumentUri.ofDocumentUri("https://host/path/subpath.git/path/document")).satisfies(
                  uri -> {
                     assertThat(uri.localUri()).isEmpty();
                     assertThat(uri.remoteUri()).hasValue(Uri.uri("https://host/path/subpath.git"));
                     assertThat(uri.repositoryPath()).isEqualTo(Path.of("/path/subpath.git"));
                     assertThat(uri.basePath()).isEqualTo(emptyPath());
                     assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/document"));
                  });
         }

         @Test
         public void ofUriWhenAutodetectedButOverriddenWithParameter() {
            assertThat(GitDocumentUri.ofDocumentUri(
                  "https://host/path/subpath.git/base/subpath/document?repositoryPath=/path/subpath.git/base")).satisfies(
                  uri -> {
                     assertThat(uri.remoteUri())
                           .as("basePath must not be present on remoteUri as GIT servers does no support this concept")
                           .hasValue(Uri.uri("https://host/path/subpath.git/base"));
                     assertThat(uri.repositoryPath()).isEqualTo(Path.of("/path/subpath.git/base"));
                     assertThat(uri.basePath()).isEqualTo(Path.of(""));
                     assertThat(uri.documentId()).isEqualTo(DocumentPath.of("/subpath/document"));
                  });
            assertThat(GitDocumentUri.ofDocumentUri(
                  "https://host/path/subpath.git/base/subpath/document?repositoryPath=/path")).satisfies(uri -> {
               assertThat(uri.localUri()).isEmpty();
               assertThat(uri.remoteUri()).hasValue(Uri.uri("https://host/path"));
               assertThat(uri.repositoryPath()).isEqualTo(Path.of("/path"));
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("/subpath.git/base/subpath/document"));
            });
         }

         @Test
         public void ofUriWhenNotAutodetected() {
            assertThatThrownBy(() -> GitDocumentUri.ofDocumentUri("https://host/path/subpath"))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage("Invalid 'https://host/path/subpath' URI: Missing 'repositoryPath' parameter");
         }

         @Test
         public void ofUriWhenNormalize() {
            assertThat(GitDocumentUri.ofDocumentUri(
                  "https://host/path.git/path/document?repositoryPath=/../path.git/./")).satisfies(uri -> {
               assertThat(uri.localUri()).isEmpty();
               assertThat(uri.remoteUri()).hasValue(Uri.uri("https://host/path.git"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/document"));
            });
         }

         @Test
         public void ofUriWhenNotAbsolute() {
            assertThatThrownBy(() -> GitDocumentUri.ofDocumentUri(
                  "https://host/path.git/path/document?repositoryPath=path.git"))
                  .isExactlyInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'https://host/path.git/path/document?repositoryPath=path.git' URI: 'repositoryPath=path.git' parameter must be absolute and must not contain traversal");
         }

         @Test
         public void ofUriWhenHasTraversals() {
            assertThatThrownBy(() -> GitDocumentUri.ofDocumentUri(
                  "https://host/path.git/path/document?repositoryPath=../base/."))
                  .isExactlyInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'https://host/path.git/path/document?repositoryPath=../base/.' URI: 'repositoryPath=../base' parameter must be absolute and must not contain traversal");
         }

         @Test
         public void ofUriWhenNotMatching() {
            assertThatThrownBy(() -> GitDocumentUri.ofDocumentUri(
                  "https://host/path.git/path/document?repositoryPath=/other"))
                  .isExactlyInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'https://host/path.git/path/document?repositoryPath=/other' URI: 'repositoryPath=/other' parameter must start '/path.git/path/document' path");
            assertThatThrownBy(() -> GitDocumentUri.ofDocumentUri(
                  "https://host/path.git/path/document?repositoryPath=/path"))
                  .isExactlyInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'https://host/path.git/path/document?repositoryPath=/path' URI: 'repositoryPath=/path' parameter must start '/path.git/path/document' path");
         }

      }

      @Nested
      class BasePathParameter {

         @Test
         public void ofUriWhenNominal() {
            assertThat(GitDocumentUri.ofDocumentUri("https://host/path.git/base/path/document?basePath=base")).satisfies(
                  uri -> {
                     assertThat(uri.localUri()).isEmpty();
                     assertThat(uri.remoteUri()).hasValue(Uri.uri("https://host/path.git"));
                     assertThat(uri.basePath()).isEqualTo(Path.of("base"));
                     assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/document"));
                  });
         }

         @Test
         public void ofUriWhenAutodetectedRepositoryPath() {
            assertThat(GitDocumentUri.ofDocumentUri(
                  "https://host/path/base/path/document?repositoryPath=/path&basePath=base")).satisfies(uri -> {
               assertThat(uri.localUri()).isEmpty();
               assertThat(uri.remoteUri()).hasValue(Uri.uri("https://host/path"));
               assertThat(uri.basePath()).isEqualTo(Path.of("base"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/document"));
            });
         }

         @Test
         public void ofUriWhenEmptyUriBasePath() {
            assertThat(GitDocumentUri.ofDocumentUri("https://host/path.git/path/document?basePath=")).satisfies(
                  uri -> {
                     assertThat(uri.localUri()).isEmpty();
                     assertThat(uri.remoteUri()).hasValue(Uri.uri("https://host/path.git"));
                     assertThat(uri.basePath()).isEqualTo(emptyPath());
                     assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/document"));
                  });
         }

         @Test
         public void ofUriWhenNormalize() {
            assertThat(GitDocumentUri.ofDocumentUri(
                  "https://host/path.git/base/path/document?basePath=sub/../base/./")).satisfies(uri -> {
               assertThat(uri.localUri()).isEmpty();
               assertThat(uri.remoteUri()).hasValue(Uri.uri("https://host/path.git"));
               assertThat(uri.basePath()).isEqualTo(Path.of("base"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/document"));
            });
         }

         @Test
         public void ofUriWhenAbsolute() {
            assertThatThrownBy(() -> GitDocumentUri.ofDocumentUri(
                  "https://host/path.git/path/document?basePath=/base"))
                  .isExactlyInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'https://host/path.git/path/document?basePath=/base' URI: 'basePath=/base' parameter must not be absolute and must not contain traversal");
         }

         @Test
         public void ofUriWhenHasTraversals() {
            assertThatThrownBy(() -> GitDocumentUri.ofDocumentUri(
                  "https://host/path.git/path/document?basePath=../base/."))
                  .isExactlyInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'https://host/path.git/path/document?basePath=../base/.' URI: 'basePath=../base' parameter must not be absolute and must not contain traversal");
         }

         @Test
         public void ofUriWhenNotMatching() {
            assertThatThrownBy(() -> GitDocumentUri.ofDocumentUri(
                  "https://host/path.git/base/path/document?basePath=other"))
                  .isExactlyInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'https://host/path.git/base/path/document?basePath=other' URI: 'basePath=other' parameter must start but not end with 'base/path/document' path");
            assertThatThrownBy(() -> GitDocumentUri.ofDocumentUri(
                  "https://host/path.git/base/path/document?basePath=base/other"))
                  .isExactlyInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'https://host/path.git/base/path/document?basePath=base/other' URI: 'basePath=base/other' parameter must start but not end with 'base/path/document' path");
         }

      }

      @Nested
      class GitFormat {

         @Test
         public void ofUriWhenHttps() {
            assertThat(GitDocumentUri
                             .ofDocumentUri("https://user:password@host/path.git/path/document#branch")
                             .exportUri(sensitiveParameters())
                             .stringValue()).isEqualTo(
                  "https://user:password@host/path.git/path/document#branch");
            assertThat(GitDocumentUri
                             .ofDocumentUri("https://host/path.git/path/document#branch")
                             .exportUri(sensitiveParameters())
                             .stringValue()).isEqualTo("https://host/path.git/path/document#branch");
         }

         @Test
         public void ofUriWhenHttp() {
            assertThat(GitDocumentUri
                             .ofDocumentUri("http://user:password@host/path.git/path/document#branch")
                             .exportUri(sensitiveParameters())
                             .stringValue()).isEqualTo(
                  "http://user:password@host/path.git/path/document#branch");
            assertThat(GitDocumentUri
                             .ofDocumentUri("http://host/path.git/path/document#branch")
                             .exportUri(sensitiveParameters())
                             .stringValue()).isEqualTo("http://host/path.git/path/document#branch");
         }

         @Test
         public void ofUriWhenGit() {
            assertThat(GitDocumentUri
                             .ofDocumentUri("git://user:password@host/path.git/path/document#branch")
                             .exportUri(sensitiveParameters())
                             .stringValue()).isEqualTo(
                  "git://user:password@host/path.git/path/document#branch");
            assertThat(GitDocumentUri
                             .ofDocumentUri("git://host/path.git/path/document#branch")
                             .exportUri(sensitiveParameters())
                             .stringValue()).isEqualTo("git://host/path.git/path/document#branch");
         }

         @Test
         public void ofUriWhenSsh() {
            assertThat(GitDocumentUri
                             .ofDocumentUri("ssh://user:password@host/path.git/path/document#branch")
                             .exportUri(sensitiveParameters())
                             .stringValue()).isEqualTo(
                  "ssh://user:password@host/path.git/path/document#branch");
            assertThat(GitDocumentUri
                             .ofDocumentUri("ssh://host/path.git/path/document#branch")
                             .exportUri(sensitiveParameters())
                             .stringValue()).isEqualTo("ssh://host/path.git/path/document#branch");
         }

         @Test
         public void ofUriWhenFile() {
            assertThat(GitDocumentUri
                             .ofDocumentUri("file:/path.git/path/document#branch")
                             .exportUri(sensitiveParameters())
                             .stringValue()).isEqualTo("file:/path.git/path/document#branch");
            assertThat(GitDocumentUri
                             .ofDocumentUri("file:///path.git/path/document#branch")
                             .exportUri(sensitiveParameters())
                             .stringValue()).isEqualTo("file:///path.git/path/document#branch");
            assertThatThrownBy(() -> GitDocumentUri
                  .ofDocumentUri("file://host/path.git/path/document#branch")
                  .exportUri(sensitiveParameters())
                  .stringValue())
                  .isExactlyInstanceOf(IncompatibleUriException.class)
                  .hasMessage(
                        "Incompatible 'file://host/path.git/path/document#branch' URI: 'uri=file://host/path.git/path/document#branch' must be compatible with GitUri, HttpUri, SshUri or FileUri");
         }

         @Test
         public void ofUriWhenNoSchemeFile() {
            assertThat(GitDocumentUri
                             .ofDocumentUri("/path.git/path/document#branch")
                             .exportUri(sensitiveParameters())
                             .stringValue()).isEqualTo("/path.git/path/document#branch");
            assertThatThrownBy(() -> GitDocumentUri
                  .ofDocumentUri("path.git/path/document#branch")
                  .exportUri(sensitiveParameters()))
                  .isExactlyInstanceOf(IncompatibleUriException.class)
                  .hasMessage(
                        "Incompatible 'path.git/path/document#branch' URI: 'uri=path.git/path/document#branch' must be compatible with GitUri, HttpUri, SshUri or FileUri");
         }
      }

      @Nested
      class WrappedUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(GitDocumentUri.ofDocumentUri(uri("document:git:https://host/path.git/path/document"))).satisfies(
                  uri -> {
                     assertThat(uri.value()).isEqualTo("https://host/path.git/path/document");
                  });
         }

         @Test
         public void ofUriWhenBadRepositoryType() {
            assertThatThrownBy(() -> GitDocumentUri.ofDocumentUri(uri(
                  "document:unknown:https://host/path/document")))
                  .isExactlyInstanceOf(IncompatibleUriException.class)
                  .hasMessage(
                        "Incompatible 'document:unknown:https://host/path/document' URI: Unsupported 'unknown' repository type");
         }

         @Test
         public void ofUriWhenNormalized() {
            assertThat(GitDocumentUri.ofDocumentUri(uri(
                  "document:git:https://host/path.git/document/path/.././"))).satisfies(uri -> {
               assertThat(uri.toURI()).isEqualTo(uri("https://host/path.git/document/"));
            });
            assertThat(GitDocumentUri.ofDocumentUri("document:git:https://host/path.git/document/path/../.")).satisfies(
                  uri -> {
                     assertThat(uri.toURI()).isEqualTo(uri("https://host/path.git/document/"));
                  });
            assertThat(GitDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(
                  "document:git:https://host/path.git/document/path/../."))).satisfies(uri -> {
               assertThat(uri.toURI()).isEqualTo(uri("https://host/path.git/document/"));
            });
         }

         @Test
         public void ofUriWhenUnsupportedWrappedUri() {
            assertThatThrownBy(() -> GitDocumentUri.ofDocumentUri(uri(
                  "document:git:unknown://host/path/document")))
                  .isExactlyInstanceOf(IncompatibleUriException.class)
                  .hasMessage(
                        "Incompatible 'unknown://host/path/document' URI: 'uri=unknown://host/path/document' must be compatible with GitUri, HttpUri, SshUri or FileUri");
         }

      }

      @Nested
      class WhenLocalUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(GitDocumentUri.ofDocumentUri(uri("file:/localpath.git/document?forceLocalUri#branch"))).satisfies(
                  uri -> {
                     assertThat(uri.localUri()).hasValue(uri("file:/localpath.git"));
                     assertThat(uri.remoteUri()).isEmpty();
                     assertThat(uri.repositoryPath()).isEqualTo(Path.of("/localpath.git"));
                     assertThat(uri.basePath()).isEqualTo(Path.of(""));
                     assertThat(uri.branch()).hasValue("branch");
                     assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
                  });
         }

         @Test
         public void ofUriWhenNoFileScheme() {
            assertThat(GitDocumentUri.ofDocumentUri(uri("/localpath.git/document?forceLocalUri#branch"))).satisfies(
                  uri -> {
                     assertThat(uri.localUri()).hasValue(uri("/localpath.git"));
                     assertThat(uri.remoteUri()).isEmpty();
                     assertThat(uri.repositoryPath()).isEqualTo(Path.of("/localpath.git"));
                     assertThat(uri.basePath()).isEqualTo(Path.of(""));
                     assertThat(uri.branch()).hasValue("branch");
                     assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
                  });
         }

         @Test
         public void ofUriWhenNoFileSchemeRelativePath() {
            assertThatThrownBy(() -> GitDocumentUri.ofDocumentUri(uri(
                  "localpath.git/document?forceLocalUri#branch")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Incompatible 'localpath.git/document?forceLocalUri#branch' URI: 'uri=localpath.git/document?forceLocalUri#branch' must be compatible with GitUri, HttpUri, SshUri or FileUri");
         }

         @Test
         public void ofUriWhenRemoteUri() {
            assertThat(GitDocumentUri.ofDocumentUri(uri(
                  "file:/localpath.git/document?forceLocalUri&remoteUri=https://host/path.git#branch"))).satisfies(
                  uri -> {
                     assertThat(uri.localUri()).hasValue(uri("file:/localpath.git"));
                     assertThat(uri.remoteUri()).hasValue(uri("https://host/path.git"));
                     assertThat(uri.repositoryPath()).isEqualTo(Path.of("/localpath.git"));
                     assertThat(uri.basePath()).isEqualTo(Path.of(""));
                     assertThat(uri.branch()).hasValue("branch");
                     assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
                  });
         }

         @Test
         public void ofUriWhenOnlyRemoteUri() {
            assertThat(GitDocumentUri.ofDocumentUri(uri(
                  "file:/localpath.git/document?remoteUri=https://host/path.git#branch"))).satisfies(uri -> {
               assertThat(uri.localUri()).hasValue(uri("file:/localpath.git"));
               assertThat(uri.remoteUri()).hasValue(uri("https://host/path.git"));
               assertThat(uri.repositoryPath()).isEqualTo(Path.of("/localpath.git"));
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
               assertThat(uri.branch()).hasValue("branch");
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
         }

         @Test
         public void ofUriWhenRepositoryAndBasePath() {
            assertThat(GitDocumentUri.ofDocumentUri(uri(
                  "file:/localpath/basePath/document?forceLocalUri&repositoryPath=/localpath&basePath=basePath&remoteUri=https://host/path.git#branch"))).satisfies(
                  uri -> {
                     assertThat(uri.localUri()).hasValue(uri("file:/localpath"));
                     assertThat(uri.remoteUri()).hasValue(uri("https://host/path.git"));
                     assertThat(uri.repositoryPath()).isEqualTo(Path.of("/localpath"));
                     assertThat(uri.basePath()).isEqualTo(Path.of("basePath"));
                     assertThat(uri.branch()).hasValue("branch");
                     assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
                  });
         }

         @Test
         public void ofUriWhenLocalFsParameters() {
            assertThat(GitDocumentUri.ofDocumentUri(uri(
                  "file:/localpath.git/document?forceLocalUri&remoteUri=https://host/path.git&createStoragePathIfMissing&custom=value#branch"))).satisfies(
                  uri -> {
                     assertThat(uri.localUri()).hasValue(uri(
                           "file:/localpath.git?createStoragePathIfMissing=true&custom=value"));
                     assertThat(uri.remoteUri()).hasValue(uri("https://host/path.git"));
                     assertThat(uri.repositoryPath()).isEqualTo(Path.of("/localpath.git"));
                     assertThat(uri.basePath()).isEqualTo(Path.of(""));
                     assertThat(uri.branch()).hasValue("branch");
                     assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
                  });
         }

         @Test
         public void ofUriWhenRemoteUriParameters() {
            assertThat(GitDocumentUri.ofDocumentUri(uri(
                  "file:/localpath.git/document?forceLocalUri&remoteUri=https://host/path.git%3FinitIfMissing=false%23remotebranch#branch")))
                  .as("remoteUri parameters/branch must be ignored")
                  .satisfies(uri -> {
                     assertThat(uri.localUri()).hasValue(uri("file:/localpath.git"));
                     assertThat(uri.remoteUri()).hasValue(uri("https://host/path.git"));
                     assertThat(uri.repositoryPath()).isEqualTo(Path.of("/localpath.git"));
                     assertThat(uri.basePath()).isEqualTo(Path.of(""));
                     assertThat(uri.branch()).hasValue("branch");
                     assertThat(uri.query().parameter(INIT_IF_MISSING, true)).hasValue(true);
                     assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
                  });
         }

         @Test
         public void ofUriWhenLocalUriParameter() {
            assertThatThrownBy(() -> GitDocumentUri.ofDocumentUri(uri(
                  "file:/localpath.git/document?forceLocalUri&localUri=file:/otherpath.git#branch")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'file:/localpath.git/document?forceLocalUri&localUri=file:/otherpath.git#branch' URI: 'localUri' parameter is not allowed in local URI");
         }

         @Test
         public void ofUriWhenBadLocalUriScheme() {
            assertThatThrownBy(() -> GitDocumentUri.ofDocumentUri(uri(
                  "http://host/localpath.git/document?forceLocalUri#branch")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Incompatible 'http://host/localpath.git/document?forceLocalUri#branch' URI: 'FileUri[value=http://host/localpath.git/document?forceLocalUri#branch,restrictions=FileUriRestrictions[relativeFormat=false, relativePath=true, emptyPath=true, query=false, fragment=false],scheme=SimpleScheme[scheme='http'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='//host/localpath.git/document?forceLocalUri', encodedSchemeSpecific='//host/localpath.git/document?forceLocalUri'],authority=SimpleServerAuthority[userInfo='null', host='SimpleHost[host='host']', port='null'],path=SimplePath[path=/localpath.git/document, encodedPath=/localpath.git/document],query=KeyValueQuery[query={forceLocalUri=[null]}, encodedQuery={forceLocalUri=[null]}],fragment=SimpleFragment[fragment=branch, encodedFragment=branch]]' must be absolute, and scheme must be equal to 'file'");
         }

      }

   }

   @Nested
   public class OfUriWhenRepositoryUri {

      @Nested
      public class WhenRepositoryUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(GitDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(
                  "https://host/path.git/path/document"))).satisfies(uri -> {
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/document"));
            });
         }
      }

      @Nested
      public class WhenGitRepositoryUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(GitDocumentUri.ofDocumentUri(GitDocumentUri.ofDocumentUri(
                  "https://host/path.git/path/document"))).satisfies(uri -> {
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/document"));
            });
         }

      }

   }

   @Nested
   public class OfConfig {

      @Test
      public void ofConfigWhenNominal() {
         assertThat(GitDocumentUri.ofConfig(new GitDocumentConfigBuilder()
                                                  .remoteUri(uri("https://host/path.git"))
                                                  .build(),
                                            null,
                                            null,
                                            null,
                                            null,
                                            DocumentPath.of("document"))).satisfies(uri -> {
            assertThat(uri.localUri()).isEmpty();
            assertThat(uri.remoteUri()).hasValue(Uri.uri("https://host/path.git"));
            assertThat(uri.query().parameter(REPOSITORY_PATH, alwaysTrue())).hasValue(Path.of("/path.git"));
            assertThat(uri.query().parameter(BASE_PATH, alwaysTrue())).hasValue(Path.of(""));
            assertThat(uri.query().parameter(INITIAL_BRANCH, alwaysTrue())).hasValue(DEFAULT_BRANCH);
            assertThat(uri.query().parameter(BRANCH, alwaysTrue())).isEmpty();
            assertThat(uri
                             .query()
                             .parameter(INIT_IF_MISSING, alwaysTrue())).hasValue(DEFAULT_INIT_IF_MISSING);
            assertThat(uri.query().parameter(CLONE_IF_MISSING, alwaysTrue())).hasValue(
                  DEFAULT_CLONE_IF_MISSING);
            assertThat(uri.query().parameter(REMOTE, alwaysTrue())).hasValue(DEFAULT_REMOTE);
            assertThat(uri.query().parameter(LOCAL_URI, alwaysTrue())).isEmpty();
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
      }

      @Test
      public void ofConfigWhenRepositoryPathNotAutodetected() {
         assertThat(GitDocumentUri.ofConfig(new GitDocumentConfigBuilder()
                                                  .remoteUri(uri("https://host/path"))
                                                  .build(),
                                            null,
                                            null,
                                            null,
                                            null,
                                            DocumentPath.of("document"))).satisfies(uri -> {
            assertThat(uri.localUri()).isEmpty();
            assertThat(uri.remoteUri()).hasValue(Uri.uri("https://host/path"));
            assertThat(uri.query().parameter(REPOSITORY_PATH, alwaysTrue())).hasValue(Path.of("/path"));
            assertThat(uri.query().parameter(BASE_PATH, alwaysTrue())).hasValue(Path.of(""));
            assertThat(uri.query().parameter(INITIAL_BRANCH, alwaysTrue())).hasValue(DEFAULT_BRANCH);
            assertThat(uri.query().parameter(BRANCH, alwaysTrue())).isEmpty();
            assertThat(uri
                             .query()
                             .parameter(INIT_IF_MISSING, alwaysTrue())).hasValue(DEFAULT_INIT_IF_MISSING);
            assertThat(uri.query().parameter(CLONE_IF_MISSING, alwaysTrue())).hasValue(
                  DEFAULT_CLONE_IF_MISSING);
            assertThat(uri.query().parameter(REMOTE, alwaysTrue())).hasValue(DEFAULT_REMOTE);
            assertThat(uri.query().parameter(LOCAL_URI, alwaysTrue())).isEmpty();
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
      }

      @Test
      public void ofConfigWhenNoRemoteUri() {
         assertThatThrownBy(() -> GitDocumentUri.ofConfig(new GitDocumentConfigBuilder().build(),
                                                          null,
                                                          null,
                                                          null,
                                                          null,
                                                          DocumentPath.of("document")))
               .isExactlyInstanceOf(IllegalStateException.class)
               .hasMessage("'dynamicLocalRepository' must be defined");
         assertThat(GitDocumentUri.ofConfig(new GitDocumentConfigBuilder().build(),
                                            FsDocumentRepository.ofUri(RepositoryUri.ofRepositoryUri(
                                                  "file:/tmp/path")),
                                            null,
                                            null,
                                            null,
                                            DocumentPath.of("document"))).satisfies(uri -> {
            assertThat(uri.localUri()).hasValue(uri("file:/tmp/path"));
            assertThat(uri.remoteUri()).isEmpty();
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
      }

      @Test
      public void ofConfigWhenLocalRepository() {
         assertThat(GitDocumentUri.ofConfig(new GitDocumentConfigBuilder()
                                                  .remoteUri(uri("https://host/path.git"))
                                                  .localRepository(new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                                                                  .storagePath(
                                                                                                        Path.of(
                                                                                                              "/test/git"))
                                                                                                  .failFastIfMissingStoragePath(
                                                                                                        false)
                                                                                                  .build()))
                                                  .build(),
                                            null,
                                            null,
                                            null,
                                            null,
                                            DocumentPath.of("document"))).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo(
                  "https://host/path.git/document?repositoryPath=/path.git&basePath=&localUri=file:/test/git?failFastIfMissingStoragePath=false&remote=origin&initialBranch=main&cloneIfMissing=true&initIfMissing=true&forceLocalUri=false&pullMode=NEVER&pullFastForward=FAST_FORWARD&pullRebase=false&commitMode=NEVER&pushMode=NEVER&connectTimeout=PT10S&disableSslVerify=false&allowUnknownSshHosts=false");

            assertThat(uri.query().parameter(LOCAL_URI, alwaysTrue())).hasValue(uri(
                  "file:/test/git?failFastIfMissingStoragePath=false"));
         });
      }

      @Test
      public void ofConfigWhenAllParameters() {
         assertThat(GitDocumentUri.ofConfig(new GitDocumentConfigBuilder()
                                                  .remoteUri(uri("https://host/path.git"))
                                                  .forceLocalUri(false)
                                                  .initialBranch("initial-branch")
                                                  .branch("branch")
                                                  .initIfMissing(false)
                                                  .cloneIfMissing(false)
                                                  .remote("remote")
                                                  .localUri(uri(
                                                        "file:/test/git?failFastIfMissingStoragePath=false"))
                                                  .pullStrategy(new PullStrategyBuilder()
                                                                      .pullMode(PullMode.ALWAYS)
                                                                      .fastForward(NO_FAST_FORWARD)
                                                                      .rebase(true)
                                                                      .shallowDepth(1)
                                                                      .build())
                                                  .pushStrategy(new PushStrategyBuilder()
                                                                      .commitMode(PushMode.ALWAYS)
                                                                      .pushMode(PushMode.ALWAYS)
                                                                      .build())
                                                  .transport(new TransportBuilder()
                                                                   .connectTimeout(Duration.ofMinutes(1))
                                                                   .disableSslVerify(true)
                                                                   .allowUnknownSshHosts(true)
                                                                   .build())
                                                  .build(),
                                            null,
                                            null,
                                            null,
                                            null,
                                            DocumentPath.of("document"))).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo(
                  "https://host/path.git/document?repositoryPath=/path.git&basePath=&localUri=file:/test/git?failFastIfMissingStoragePath=false&remote=remote&initialBranch=initial-branch&cloneIfMissing=false&initIfMissing=false&forceLocalUri=false&pullMode=ALWAYS&pullFastForward=NO_FAST_FORWARD&pullRebase=true&pullShallowDepth=1&commitMode=ALWAYS&pushMode=ALWAYS&connectTimeout=PT1M&disableSslVerify=true&allowUnknownSshHosts=true#branch");
         });
      }

      @Test
      public void ofConfigWhenBadConfiguration() {
         assertThatThrownBy(() -> GitDocumentUri.ofConfig(null,
                                                          null,
                                                          null,
                                                          null,
                                                          null,
                                                          DocumentPath.of("document")))
               .isExactlyInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'config' must not be null");
      }

      @Test
      @Disabled("Branch is first read from remote URI then stripped")
      public void ofConfigWhenInvalidBranch() {
         assertThatThrownBy(() -> GitDocumentUri.ofConfig(new GitDocumentConfigBuilder()
                                                                .remoteUri(uri("https://host/path#main"))
                                                                .branch("branch")
                                                                .build(),
                                                          null,
                                                          null,
                                                          null,
                                                          null,
                                                          DocumentPath.of("document")))
               .isExactlyInstanceOf(InvalidUriException.class)
               .hasMessage("");
      }

      @Test
      public void ofConfigWhenLocalUri() {
         assertThat(GitDocumentUri.ofConfig(new GitDocumentConfigBuilder()
                                                  .localUri(uri("file:/test/git"))
                                                  .build(),
                                            null,
                                            null,
                                            null,
                                            null,
                                            DocumentPath.of("document"))).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo(
                  "file:/test/git/document?repositoryPath=/test/git&basePath=&remote=origin&initialBranch=main&cloneIfMissing=true&initIfMissing=true&forceLocalUri=true&pullMode=NEVER&pullFastForward=FAST_FORWARD&pullRebase=false&commitMode=NEVER&pushMode=NEVER&connectTimeout=PT10S&disableSslVerify=false&allowUnknownSshHosts=false");

            assertThat(uri.localUri()).hasValue(uri("file:/test/git"));
            assertThat(uri.remoteUri()).isEmpty();
            assertThat(uri.query().parameter(FORCE_LOCAL_URI, false)).hasValue(true);
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
      }

      @Test
      public void ofConfigWhenLocalUriWithParameters() {
         assertThat(GitDocumentUri.ofConfig(new GitDocumentConfigBuilder()
                                                  .localUri(uri(
                                                        "file:/test/git?failFastIfMissingStoragePath=false"))
                                                  .build(),
                                            null,
                                            null,
                                            null,
                                            null,
                                            DocumentPath.of("document"))).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo(
                  "file:/test/git/document?failFastIfMissingStoragePath=false&repositoryPath=/test/git&basePath=&remote=origin&initialBranch=main&cloneIfMissing=true&initIfMissing=true&forceLocalUri=true&pullMode=NEVER&pullFastForward=FAST_FORWARD&pullRebase=false&commitMode=NEVER&pushMode=NEVER&connectTimeout=PT10S&disableSslVerify=false&allowUnknownSshHosts=false");

            assertThat(uri.localUri()).hasValue(uri("file:/test/git?failFastIfMissingStoragePath=false"));
            assertThat(uri.remoteUri()).isEmpty();
            assertThat(uri.query().parameter(FORCE_LOCAL_URI, false)).hasValue(true);
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
      }

      @Test
      public void ofConfigWhenLocalUriButRemoteUri() {
         assertThat(GitDocumentUri.ofConfig(new GitDocumentConfigBuilder()
                                                  .localUri(uri("file:/test/git"))
                                                  .remoteUri(uri("https://host/path.git"))
                                                  .forceLocalUri(true)
                                                  .build(),
                                            null,
                                            null,
                                            null,
                                            null,
                                            DocumentPath.of("document"))).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo(
                  "file:/test/git/document?repositoryPath=/test/git&basePath=&remoteUri=https://host/path.git&remote=origin&initialBranch=main&cloneIfMissing=true&initIfMissing=true&forceLocalUri=true&pullMode=NEVER&pullFastForward=FAST_FORWARD&pullRebase=false&commitMode=NEVER&pushMode=NEVER&connectTimeout=PT10S&disableSslVerify=false&allowUnknownSshHosts=false");

            assertThat(uri.localUri()).hasValue(uri("file:/test/git"));
            assertThat(uri.remoteUri()).hasValue(uri("https://host/path.git"));
            assertThat(uri.query().parameter(FORCE_LOCAL_URI, false)).hasValue(true);
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
      }

      @Test
      public void ofConfigWhenLocalUriButRemoteUriWithParameters() {
         assertThat(GitDocumentUri.ofConfig(new GitDocumentConfigBuilder()
                                                  .localUri(uri("file:/test/git"))
                                                  .remoteUri(uri("https://host/path.git?custom"))
                                                  .forceLocalUri(true)
                                                  .build(),
                                            null,
                                            null,
                                            null,
                                            null,
                                            DocumentPath.of("document"))).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo(
                  "file:/test/git/document?repositoryPath=/test/git&basePath=&remoteUri=https://host/path.git?custom&remote=origin&initialBranch=main&cloneIfMissing=true&initIfMissing=true&forceLocalUri=true&pullMode=NEVER&pullFastForward=FAST_FORWARD&pullRebase=false&commitMode=NEVER&pushMode=NEVER&connectTimeout=PT10S&disableSslVerify=false&allowUnknownSshHosts=false");

            assertThat(uri.localUri()).hasValue(uri("file:/test/git"));
            assertThat(uri.remoteUri())
                  .as("remoteUri parameters are discarded")
                  .hasValue(uri("https://host/path.git"));
            assertThat(uri.query().parameter(FORCE_LOCAL_URI, false)).hasValue(true);
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
      }

      @Test
      public void ofConfigWhenLocalUriWithDynamicParameters() {
         assertThat(GitDocumentUri.ofConfig(new GitDocumentConfigBuilder()
                                                  .localUri(uri("file:/test/git"))
                                                  .build(),
                                            null,
                                            "dynremote",
                                            uri("https://host/path.git"),
                                            "branch",
                                            DocumentPath.of("document"))).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo(
                  "https://host/path.git/document?repositoryPath=/path.git&basePath=&localUri=file:/test/git&remote=dynremote&initialBranch=main&cloneIfMissing=true&initIfMissing=true&forceLocalUri=false&pullMode=NEVER&pullFastForward=FAST_FORWARD&pullRebase=false&commitMode=NEVER&pushMode=NEVER&connectTimeout=PT10S&disableSslVerify=false&allowUnknownSshHosts=false#branch");

            assertThat(uri.localUri()).hasValue(uri("file:/test/git"));
            assertThat(uri.remoteUri()).hasValue(uri("https://host/path.git"));
            assertThat(uri.query().parameter(FORCE_LOCAL_URI, false)).hasValue(false);
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
         assertThat(GitDocumentUri.ofConfig(new GitDocumentConfigBuilder()
                                                  .localUri(uri("file:/test/git"))
                                                  .forceLocalUri(true)
                                                  .build(),
                                            null,
                                            "dynremote",
                                            uri("https://host/path.git"),
                                            "branch",
                                            DocumentPath.of("document"))).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo(
                  "file:/test/git/document?repositoryPath=/test/git&basePath=&remoteUri=https://host/path.git&remote=dynremote&initialBranch=main&cloneIfMissing=true&initIfMissing=true&forceLocalUri=true&pullMode=NEVER&pullFastForward=FAST_FORWARD&pullRebase=false&commitMode=NEVER&pushMode=NEVER&connectTimeout=PT10S&disableSslVerify=false&allowUnknownSshHosts=false#branch");

            assertThat(uri.localUri()).hasValue(uri("file:/test/git"));
            assertThat(uri.remoteUri()).hasValue(uri("https://host/path.git"));
            assertThat(uri.query().parameter(FORCE_LOCAL_URI, false)).hasValue(true);
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
      }

   }

   @Nested
   public class ToConfig {

      @Test
      public void toConfigWhenNominal() {
         assertThat(GitDocumentUri
                          .ofDocumentUri("https://user:password@host/path.git/path/document")
                          .toConfig()).satisfies(config -> {
            assertThat(config.initialBranch()).hasValue(DEFAULT_BRANCH);
            assertThat(config.branch()).isEmpty();
            assertThat(config.localUri()).isEmpty();
            assertThat(config.remoteUri()).hasValue(uri("https://host/path.git"));
            assertThat(config.cloneIfMissing()).hasValue(DEFAULT_CLONE_IF_MISSING);
            assertThat(config.initIfMissing()).hasValue(DEFAULT_INIT_IF_MISSING);
            assertThat(config.remote()).hasValue(DEFAULT_REMOTE);
            assertThat(config.remoteAuthentication()).hasValue(Authentication.of("user", "password"));
         });
      }

      @Test
      public void toConfigWhenNoPassword() {
         assertThat(GitDocumentUri
                          .ofDocumentUri("https://user@host/path.git/path/document")
                          .toConfig()).satisfies(config -> {
            assertThat(config.remoteAuthentication()).hasValue(Authentication.of("user"));
         });
      }

      @Test
      public void toConfigWhenAllParameters() {
         assertThat(GitDocumentUri
                          .ofDocumentUri(
                                "https://user:password@host/path.git/path/document?branch=branch&localUri=file:/test/git%3FfailFastIfMissingStoragePath=false&cloneIfMissing=false&initIfMissing=false&remote=remote")
                          .toConfig()).satisfies(config -> {
            assertThat(config.branch()).hasValue("branch");
            assertThat(config.localUri()).hasValue(uri("file:/test/git?failFastIfMissingStoragePath=false"));
            assertThat(config.remoteUri()).hasValue(uri("https://host/path.git"));
            assertThat(config.remoteAuthentication()).hasValue(Authentication.of("user", "password"));
            assertThat(config.cloneIfMissing()).hasValue(false);
            assertThat(config.initIfMissing()).hasValue(false);
            assertThat(config.remote()).hasValue("remote");
         });
      }

      @Test
      public void toConfigWhenLocalUri() {
         assertThat(GitDocumentUri
                          .ofDocumentUri(
                                "file:/test/git/document?repositoryPath=/test/git&failFastIfMissingStoragePath=false&remoteUri=https://user:password@host/path.git&branch=branch&cloneIfMissing=false&initIfMissing=false&remote=remote")
                          .toConfig()).satisfies(config -> {
            assertThat(config.branch()).hasValue("branch");
            assertThat(config.localUri()).hasValue(uri("file:/test/git?failFastIfMissingStoragePath=false"));
            assertThat(config.remoteUri()).hasValue(uri("https://host/path.git"));
            assertThat(config.remoteAuthentication()).hasValue(Authentication.of("user", "password"));
            assertThat(config.cloneIfMissing()).hasValue(false);
            assertThat(config.initIfMissing()).hasValue(false);
            assertThat(config.remote()).hasValue("remote");
         });
      }

   }

   @Nested
   public class ExportUri {

      @Test
      public void exportUriWhenNominal() {
         assertThat(GitDocumentUri
                          .ofDocumentUri("https://host/path.git/path/document")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("https://host/path.git/path/document");
      }

      @Test
      public void exportUriWhenRepositoryPath() {
         assertThat(GitDocumentUri
                          .ofDocumentUri("https://host/path.git/path/document")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("https://host/path.git/path/document");
         assertThat(GitDocumentUri
                          .ofDocumentUri("https://host/path.git/path/document?repositoryPath=/path.git")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("https://host/path.git/path/document");
      }

      @Test
      public void exportUriWhenNotAutodetectableRepositoryPath() {
         assertThat(GitDocumentUri
                          .ofDocumentUri("https://host/path/base/path/document?repositoryPath=/path")
                          .exportUri(defaultParameters())
                          .stringValue())
               .as("auto-detected repositoryPath=/path/base and basePath='' so that these parameters must be exported")
               .isEqualTo("https://host/path/base/path/document?repositoryPath=/path");
      }

      @Test
      public void exportUriWhenBasePath() {
         assertThat(GitDocumentUri
                          .ofDocumentUri("https://host/path.git/base/path/document?basePath=base")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("https://host/path.git/base/path/document?basePath=base");
      }

      @Test
      public void exportUriWhenBranch() {
         assertThat(GitDocumentUri
                          .ofDocumentUri("https://host/path.git/path/document#main")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("https://host/path.git/path/document#main");
         assertThat(GitDocumentUri
                          .ofDocumentUri("https://host/path.git/path/document#main")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("https://host/path.git/path/document#main");
         assertThat(GitDocumentUri
                          .ofDocumentUri("https://host/path.git/path/document#main")
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo(
               "https://host/path.git/path/document?repositoryPath=/path.git&basePath=&remote=origin&initialBranch=main&cloneIfMissing=true&initIfMissing=true&forceLocalUri=false&pullMode=NEVER&pullFastForward=FAST_FORWARD&pullRebase=false&commitMode=NEVER&pushMode=NEVER&connectTimeout=PT10S&disableSslVerify=false&allowUnknownSshHosts=false#main");
         assertThat(GitDocumentUri
                          .ofDocumentUri("https://host/path.git/path/document?branch=main")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("https://host/path.git/path/document#main");
         assertThat(GitDocumentUri
                          .ofDocumentUri("https://host/path.git/path/document?branch=main")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("https://host/path.git/path/document#main");
         assertThat(GitDocumentUri
                          .ofDocumentUri("https://host/path.git/path/document?branch=main")
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo(
               "https://host/path.git/path/document?repositoryPath=/path.git&basePath=&remote=origin&initialBranch=main&cloneIfMissing=true&initIfMissing=true&forceLocalUri=false&pullMode=NEVER&pullFastForward=FAST_FORWARD&pullRebase=false&commitMode=NEVER&pushMode=NEVER&connectTimeout=PT10S&disableSslVerify=false&allowUnknownSshHosts=false#main");
         assertThat(GitDocumentUri
                          .ofDocumentUri("https://host/path.git/path/document#branch")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("https://host/path.git/path/document#branch");
         assertThat(GitDocumentUri
                          .ofDocumentUri("https://host/path.git/path/document#branch")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("https://host/path.git/path/document#branch");
         assertThat(GitDocumentUri
                          .ofDocumentUri("https://host/path.git/path/document#branch")
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo(
               "https://host/path.git/path/document?repositoryPath=/path.git&basePath=&remote=origin&initialBranch=main&cloneIfMissing=true&initIfMissing=true&forceLocalUri=false&pullMode=NEVER&pullFastForward=FAST_FORWARD&pullRebase=false&commitMode=NEVER&pushMode=NEVER&connectTimeout=PT10S&disableSslVerify=false&allowUnknownSshHosts=false#branch");
         assertThat(GitDocumentUri
                          .ofDocumentUri("https://host/path.git/path/document?branch=branch")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("https://host/path.git/path/document#branch");
         assertThat(GitDocumentUri
                          .ofDocumentUri("https://host/path.git/path/document?branch=branch")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("https://host/path.git/path/document#branch");
         assertThat(GitDocumentUri
                          .ofDocumentUri("https://host/path.git/path/document?branch=branch")
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo(
               "https://host/path.git/path/document?repositoryPath=/path.git&basePath=&remote=origin&initialBranch=main&cloneIfMissing=true&initIfMissing=true&forceLocalUri=false&pullMode=NEVER&pullFastForward=FAST_FORWARD&pullRebase=false&commitMode=NEVER&pushMode=NEVER&connectTimeout=PT10S&disableSslVerify=false&allowUnknownSshHosts=false#branch");
      }

      @Test
      public void exportUriWhenDefaultParameters() {
         assertThat(GitDocumentUri
                          .ofDocumentUri("https://user:password@host/path.git/path/document")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("https://host/path.git/path/document");
      }

      @Test
      public void exportUriWhenDefaultParametersWithForceDefaultValues() {
         assertThat(GitDocumentUri
                          .ofDocumentUri("https://user:password@host/path.git/path/document")
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo(
               "https://host/path.git/path/document?repositoryPath=/path.git&basePath=&remote=origin&initialBranch=main&cloneIfMissing=true&initIfMissing=true&forceLocalUri=false&pullMode=NEVER&pullFastForward=FAST_FORWARD&pullRebase=false&commitMode=NEVER&pushMode=NEVER&connectTimeout=PT10S&disableSslVerify=false&allowUnknownSshHosts=false");
      }

      @Test
      public void exportUriWhenSensitiveParameters() {
         assertThat(GitDocumentUri
                          .ofDocumentUri("https://user:password@host/path.git/path/document")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("https://user:password@host/path.git/path/document");
      }

      @Test
      public void exportUriWhenCustomQuery() {
         assertThat(GitDocumentUri
                          .ofDocumentUri(
                                "https://host/path.git/path/document?custom=value&cloneIfMissing=true")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("https://host/path.git/path/document?custom=value");
      }

      @Test
      public void exportUriWhenNoPassword() {
         assertThat(GitDocumentUri
                          .ofDocumentUri("https://user@host/path.git/path/document")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("https://host/path.git/path/document");
         assertThat(GitDocumentUri
                          .ofDocumentUri("https://user@host/path.git/path/document")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("https://user@host/path.git/path/document");
      }

      @Test
      public void exportUriWhenOfficialPorts() {
         assertThat(GitDocumentUri
                          .ofDocumentUri("https://host:443/path.git/path/document")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("https://host/path.git/path/document");
         assertThat(GitDocumentUri
                          .ofDocumentUri("https://host:444/path.git/path/document")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("https://host:444/path.git/path/document");
         assertThat(GitDocumentUri
                          .ofDocumentUri("http://host:80/path.git/path/document")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("http://host/path.git/path/document");
         assertThat(GitDocumentUri
                          .ofDocumentUri("http://host:81/path.git/path/document")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("http://host:81/path.git/path/document");
         assertThat(GitDocumentUri
                          .ofDocumentUri("git://host:9418/path.git/path/document")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("git://host/path.git/path/document");
         assertThat(GitDocumentUri
                          .ofDocumentUri("git://host:9419/path.git/path/document")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("git://host:9419/path.git/path/document");
         assertThat(GitDocumentUri
                          .ofDocumentUri("ssh://host:22/path.git/path/document")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("ssh://host/path.git/path/document");
         assertThat(GitDocumentUri
                          .ofDocumentUri("ssh://host:23/path.git/path/document")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("ssh://host:23/path.git/path/document");
      }

      @Test
      public void exportUriWhenVariousGitFormat() {
         assertThat(GitDocumentUri
                          .ofDocumentUri("https://user:password@host/path.git/path/document")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("https://user:password@host/path.git/path/document");
         assertThat(GitDocumentUri
                          .ofDocumentUri("https://host/path.git/path/document")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("https://host/path.git/path/document");
         assertThat(GitDocumentUri
                          .ofDocumentUri("http://user:password@host/path.git/path/document")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("http://user:password@host/path.git/path/document");
         assertThat(GitDocumentUri
                          .ofDocumentUri("http://host/path.git/path/document")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("http://host/path.git/path/document");
         assertThat(GitDocumentUri
                          .ofDocumentUri("git://user:password@host/path.git/path/document")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("git://user:password@host/path.git/path/document");
         assertThat(GitDocumentUri
                          .ofDocumentUri("git://host/path.git/path/document")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("git://host/path.git/path/document");
         assertThat(GitDocumentUri
                          .ofDocumentUri("ssh://user:password@host/path.git/path/document")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("ssh://user:password@host/path.git/path/document");
         assertThat(GitDocumentUri
                          .ofDocumentUri("ssh://host/path.git/path/document")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("ssh://host/path.git/path/document");
         assertThat(GitDocumentUri
                          .ofDocumentUri("file:/path.git/path/document")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("file:/path.git/path/document");
         assertThat(GitDocumentUri
                          .ofDocumentUri("file:///path.git/path/document")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("file:///path.git/path/document");
         assertThat(GitDocumentUri
                          .ofDocumentUri("/path.git/path/document")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("/path.git/path/document");
      }

      @Test
      public void exportUriWhenLocalUriParameter() {
         assertThat(GitDocumentUri
                          .ofDocumentUri(
                                "https://host/path.git/path/document?localUri=file:/test/git?failFastIfMissingStoragePath=false")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo(
               "https://host/path.git/path/document?localUri=file:/test/git?failFastIfMissingStoragePath=false");
      }

      @Test
      public void exportUriWhenLocalUri() {
         assertThat(GitDocumentUri
                          .ofDocumentUri(
                                "file:/test/git/path/document?repositoryPath=/test/git&failFastIfMissingStoragePath=false&remoteUri=https://host/path.git")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo(
               "file:/test/git/path/document?repositoryPath=/test/git&failFastIfMissingStoragePath=false&remoteUri=https://host/path.git");
      }

   }

   @Nested
   public class NormalizeResolveRelativize {

      @Test
      public void normalizeWhenNominal() {
         assertThat(GitDocumentUri
                          .ofDocumentUri("https://host/path.git/path/document/../.")
                          .normalize()).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo("https://host/path.git/path/");
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("/path"));
         });
      }

      @Test
      public void resolveWhenNominal() {
         assertThat(GitDocumentUri
                          .ofDocumentUri("https://host/path.git/path/document")
                          .resolve(uri("otherpath.git/path/document"))).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo("https://host/path.git/path/otherpath.git/path/document");
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("/path/otherpath.git/path/document"));
         });
         assertThat(GitDocumentUri
                          .ofDocumentUri("https://host/path.git/path/")
                          .resolve(uri("document"))).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo("https://host/path.git/path/document");
         });
      }

      @Test
      public void relativizeWhenNominal() {
         assertThatThrownBy(() -> GitDocumentUri
               .ofDocumentUri("https://host/path.git/path/document")
               .relativize(uri("https://host/path.git/otherpath"))).isExactlyInstanceOf(
               UnsupportedOperationException.class);
      }

   }

}