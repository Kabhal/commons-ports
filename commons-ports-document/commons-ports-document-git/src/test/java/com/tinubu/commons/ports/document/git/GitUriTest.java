/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      git://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.git;

import static com.tinubu.commons.ddd2.test.uri.ComponentUriAssert.assertThat;
import static com.tinubu.commons.ddd2.uri.DefaultUriRestrictions.PortRestriction.FORCE_MAPPED_PORT;
import static com.tinubu.commons.ddd2.uri.DefaultUriRestrictions.PortRestriction.REMOVE_MAPPED_PORT;
import static com.tinubu.commons.ddd2.uri.DefaultUriRestrictions.PortRestriction.REMOVE_PORT;
import static com.tinubu.commons.ddd2.uri.DefaultUriRestrictions.noRestrictions;
import static com.tinubu.commons.ddd2.uri.DefaultUriRestrictions.ofDefault;
import static com.tinubu.commons.ddd2.uri.DefaultUriRestrictions.ofRestrictions;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.ports.document.git.GitUri.DEFAULT_GIT_PORT;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.InstanceOfAssertFactories.type;

import java.net.URI;
import java.nio.file.Path;
import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ddd2.uri.ComponentUri;
import com.tinubu.commons.ddd2.uri.ComponentUri.UserInfo;
import com.tinubu.commons.ddd2.uri.DefaultUri;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ddd2.uri.parts.SimplePort;
import com.tinubu.commons.ddd2.uri.parts.UsernamePasswordUserInfo;
import com.tinubu.commons.ddd2.uri.uris.ArchiveUri;
import com.tinubu.commons.ports.document.domain.DocumentRepositoryFactory;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.fs.FsDocumentRepositoryLoader;

// FIXME ofDocumentUri(uri, optional type ?)
// FIXME FsDocumentUri -> not recognized by GitRepoUri -> introduce GitRepoUri : ClassRules(is branch GitRepoUri)
public class GitUriTest {

   // FsDocUri -> FsRepoUri -> AbstractRepoUri -> RepoUri
   // DefaultDocUri -> DefaultRepoUri -> AbstractRepoUri -> RepoUri

   // ArchiveRepositoryUri(ANY_URI/archive.zip[!/])
   // ArchiveDocumentUri(ANY_URI/archive.zip!/entry)

   // ArchiveRepositoryUri(<[*]DocumentUri(*.zip)>[!/])
   // ArchiveDocumentUri(<[*]DocumentUri(*.zip)>!/entry)

   @Test
   void FIXME() {
      var doc = DocumentUri.ofDocumentUri(ArchiveUri.archive("zip",
                                                             Uri.ofUri(
                                                                   "file:/Users/hugo/Development/commons-lang/test.zip?repositoryPath=/Users/hugo/Development/commons-lang#master"),
                                                             Path.of("/release.sh")));
      DocumentRepositoryFactory
            .instance()
            .documentRepository(doc, new GitDocumentRepositoryLoader(), new FsDocumentRepositoryLoader())
            .ifPresent(repository -> {
               System.out.println(doc);
               System.out.println(repository);
               System.out.println(repository.findDocumentEntryByUri(doc));
            });

   }

   static {
      Assertions.setMaxStackTraceElementsDisplayed(50);
   }

   @Test
   public void ofUriWhenNominal() {
      assertThat(GitUri.ofUri("git://user@host/path")).satisfies(uri -> {
         assertThat(uri).hasValue("git://user@host/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasNoPort();
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(DEFAULT_GIT_PORT);
      });
   }

   @Test
   public void ofUriWhenDefaultUri() {
      assertThat(GitUri.ofUri(DefaultUri.ofUri("git://user@host/path"))).satisfies(uri -> {
         assertThat(uri).hasValue("git://user@host/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasNoPort();
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(DEFAULT_GIT_PORT);
      });
   }

   @Test
   public void ofUriWhenArbitraryUri() {
      assertThat(GitUri.ofUri(ComponentUri
                                    .ofUri("git://user@host/path")
                                    .component()
                                    .userInfo(ui -> new UserInfo() {
                                       public Optional<? extends UserInfo> recreate(URI uri) {
                                          return optional(this);
                                       }

                                       public String value() {
                                          return "user";
                                       }

                                       public StringBuilder appendEncodedValue(StringBuilder builder) {
                                          builder.append("user@");
                                          return builder;
                                       }
                                    }))).satisfies(uri -> {
         assertThat(uri).hasValue("git://user@host/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasNoPort();
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(DEFAULT_GIT_PORT);
      });
      assertThat(GitUri.ofUri(ComponentUri.ofUri("git://host/path"))).satisfies(uri -> {
         assertThat(uri).hasValue("git://host/path");
         assertThat(uri).hasNoUserInfo();
         assertThat(uri).hasHost("host");
         assertThat(uri).hasNoPort();
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(DEFAULT_GIT_PORT);
      });
   }

   @Test
   public void ofUriWhenBadArguments() {
      assertThatThrownBy(() -> GitUri.ofUri((String) null))
            .isExactlyInstanceOf(InvariantValidationException.class)
            .hasMessage("Invariant validation error > 'uri' must not be null");
      assertThatThrownBy(() -> GitUri.ofUri((URI) null))
            .isExactlyInstanceOf(InvariantValidationException.class)
            .hasMessage("Invariant validation error > 'uri' must not be null");
      assertThatThrownBy(() -> GitUri.ofUri((Uri) null))
            .isExactlyInstanceOf(InvariantValidationException.class)
            .hasMessage("Invariant validation error > 'uri' must not be null");
   }

   @Test
   public void ofUriWhenDefaultPortAndNoRestrictions() {
      assertThat(GitUri.ofUri("git://user@host/path")).satisfies(uri -> {
         assertThat(uri).hasValue("git://user@host/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasNoPort();
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(DEFAULT_GIT_PORT);
      });
      assertThat(GitUri.ofUri("git://user@host:9418/path")).satisfies(uri -> {
         assertThat(uri).hasValue("git://user@host:9418/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPort(DEFAULT_GIT_PORT);
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(DEFAULT_GIT_PORT);
      });
      assertThat(GitUri.ofUri("git://user@host:42/path")).satisfies(uri -> {
         assertThat(uri).hasValue("git://user@host:42/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPort(42);
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(42);
      });
   }

   @Test
   public void ofUriWhenDefaultPortAndRemovePortRestriction() {
      assertThat(GitUri.ofUri("git://user@host/path",
                              ofDefault().portRestriction(REMOVE_PORT))).satisfies(uri -> {
         assertThat(uri).hasValue("git://user@host/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasNoPort().components().hasNoPort();
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(DEFAULT_GIT_PORT);
      });
      assertThat(GitUri.ofUri("git://user@host:9418/path",
                              ofDefault().portRestriction(REMOVE_PORT))).satisfies(uri -> {
         assertThat(uri).hasValue("git://user@host/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasNoPort().components().hasNoPort();
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(DEFAULT_GIT_PORT);
      });
      assertThat(GitUri.ofUri("git://user@host:42/path", ofDefault().portRestriction(REMOVE_PORT))).satisfies(
            uri -> {
               assertThat(uri).hasValue("git://user@host/path");
               assertThat(uri).hasUserInfo(false, "user");
               assertThat(uri).hasHost("host");
               assertThat(uri).hasNoPort().components().hasNoPort();
               assertThat(uri).hasPath(false, "/path");
               assertThat(uri).hasNoQuery();
               Assertions.assertThat(uri.portOrDefault()).isEqualTo(DEFAULT_GIT_PORT);
            });
   }

   @Test
   public void ofUriWhenDefaultPortAndRemoveMappedPortRestriction() {
      assertThat(GitUri.ofUri("git://user@host/path",
                              ofDefault().portRestriction(REMOVE_MAPPED_PORT))).satisfies(uri -> {
         assertThat(uri).hasValue("git://user@host/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasNoPort().components().hasNoPort();
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(DEFAULT_GIT_PORT);
      });
      assertThat(GitUri.ofUri("git://user@host:9418/path",
                              ofDefault().portRestriction(REMOVE_MAPPED_PORT))).satisfies(uri -> {
         assertThat(uri).hasValue("git://user@host/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasNoPort().components().hasNoPort();
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(DEFAULT_GIT_PORT);
      });
      assertThat(GitUri.ofUri("git://user@host:42/path",
                              ofDefault().portRestriction(REMOVE_MAPPED_PORT))).satisfies(uri -> {
         assertThat(uri).hasValue("git://user@host:42/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPort(42);
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(42);
      });
   }

   @Test
   public void ofUriWhenDefaultPortAndForceMappedPortRestriction() {
      assertThat(GitUri.ofUri("git://user@host/path",
                              ofDefault().portRestriction(FORCE_MAPPED_PORT))).satisfies(uri -> {
         assertThat(uri).hasValue("git://user@host:9418/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPort(DEFAULT_GIT_PORT).components().hasPort(SimplePort.of(DEFAULT_GIT_PORT));
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(DEFAULT_GIT_PORT);
      });
      assertThat(GitUri.ofUri("git://user@host:9418/path",
                              ofDefault().portRestriction(FORCE_MAPPED_PORT))).satisfies(uri -> {
         assertThat(uri).hasValue("git://user@host:9418/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPort(DEFAULT_GIT_PORT).components().hasPort(SimplePort.of(DEFAULT_GIT_PORT));
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(DEFAULT_GIT_PORT);
      });
      assertThat(GitUri.ofUri("git://user@host:42/path",
                              ofDefault().portRestriction(FORCE_MAPPED_PORT))).satisfies(uri -> {
         assertThat(uri).hasValue("git://user@host:42/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPort(42).components().hasPort(SimplePort.of(42));
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(42);
      });
   }

   @Test
   public void ofUriWhenPath() {
      assertThat(GitUri.ofUri("git://user@host/path")).satisfies(uri -> {
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
      });
   }

   @Test
   public void ofUriWhenQuery() {
      assertThatThrownBy(() -> GitUri.ofUri("git://user@host/path?query", ofRestrictions(true, false)))
            .isExactlyInstanceOf(InvalidUriException.class)
            .hasMessageContainingAll("Invalid 'git://user@host/path?query' URI", "must not have a query");
      assertThat(GitUri.ofUri("git://user@host/path?query", noRestrictions())).satisfies(uri -> {
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasQuery(false, "query");
      });
   }

   @Test
   public void ofUriWhenFragment() {
      assertThatThrownBy(() -> GitUri.ofUri("git://user@host/path#fragment", ofRestrictions(false, true)))
            .isExactlyInstanceOf(InvalidUriException.class)
            .hasMessageContainingAll("Invalid 'git://user@host/path#fragment' URI",
                                     "must not have a fragment");
      assertThat(GitUri.ofUri("git://user@host/path#fragment", noRestrictions())).satisfies(uri -> {
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasFragment(false, "fragment");
      });
   }

   @Test
   public void ofUriWhenRootPath() {
      assertThat(GitUri.ofUri("git://user@host/")).satisfies(uri -> {
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPath(false, "/");
         assertThat(uri).hasNoQuery();
      });
   }

   @Test
   public void ofUriWhenEmptyPath() {
      assertThat(GitUri.ofUri("git://user@host")).satisfies(uri -> {
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPath(false, "");
         assertThat(uri).hasNoQuery();
      });
   }

   @Test
   public void ofUriWhenNoUserInfo() {
      assertThat(GitUri.ofUri("git://host/path")).satisfies(uri -> {
         assertThat(uri).hasNoUserInfo();
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
      });
   }

   @Test
   public void ofUriWhenIncompatibleUri() {
      assertThatThrownBy(() -> GitUri.ofUri("other://host/path"))
            .isExactlyInstanceOf(IncompatibleUriException.class)
            .hasMessageContainingAll("Incompatible 'other://host/path' URI",
                                     "must be absolute, and scheme must be equal to 'git'");
      assertThatThrownBy(() -> GitUri.ofUri("git:/path"))
            .isExactlyInstanceOf(IncompatibleUriException.class)
            .hasMessageContainingAll("Incompatible 'git:/path' URI:", "must be hierarchical server-based");
      assertThatThrownBy(() -> GitUri.ofUri("git:///path"))
            .isExactlyInstanceOf(IncompatibleUriException.class)
            .hasMessageContainingAll("Incompatible 'git:///path' URI:",
                                     "must not be empty hierarchical server-based");
      assertThatThrownBy(() -> GitUri.ofUri("//host/path"))
            .isExactlyInstanceOf(IncompatibleUriException.class)
            .hasMessageContainingAll("Incompatible '//host/path' URI:",
                                     "must be absolute, and scheme must be equal to 'git'");
   }

   @Test
   public void ofUriWhenInvalidUri() {
      assertThatThrownBy(() -> GitUri.ofUri("git:"))
            .isExactlyInstanceOf(InvalidUriException.class)
            .hasMessage("Invalid 'git:' URI: Expected scheme-specific part at index 4: git:");
      assertThatThrownBy(() -> GitUri.ofUri("git://"))
            .isExactlyInstanceOf(InvalidUriException.class)
            .hasMessage("Invalid 'git://' URI: Expected authority at index 6: git://");
      assertThatThrownBy(() -> GitUri.ofUri("//"))
            .isExactlyInstanceOf(InvalidUriException.class)
            .hasMessage("Invalid '//' URI: Expected authority at index 2: //");
   }

   @Test
   public void normalizeWhenNominal() {
      assertThat(GitUri.ofUri("git://user@host/path/./").normalize()).satisfies(uri -> {
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPath(false, "/path/");
         assertThat(uri).hasNoQuery();
      });
   }

   @Test
   public void defaultPortWhenNominal() {
      assertThat(GitUri.ofUri("git://user@host/path")).satisfies(uri -> {
         Assertions.assertThat(uri.defaultPort()).isTrue();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(9418);
         Assertions.assertThat(uri.component().portOrDefault()).isEqualTo(SimplePort.of(9418));
      });
      assertThat(GitUri.ofUri("git://user@host:9418/path")).satisfies(uri -> {
         Assertions.assertThat(uri.defaultPort()).isTrue();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(9418);
         Assertions.assertThat(uri.component().portOrDefault()).isEqualTo(SimplePort.of(9418));
      });
      assertThat(GitUri.ofUri("git://user@host:42/path")).satisfies(uri -> {
         Assertions.assertThat(uri.defaultPort()).isFalse();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(42);
         Assertions.assertThat(uri.component().portOrDefault()).isEqualTo(SimplePort.of(42));
      });
   }

   @Test
   public void usernamePasswordWhenNominal() {
      assertThat(GitUri.ofUri("git://user:pass@host/path")).satisfies(uri -> {
         Assertions.assertThat(uri.username()).hasValue("user");
         Assertions.assertThat(uri.password()).hasValue("pass");
         assertThat(uri).components().hasUserInfoSatisfying(ui -> {
            Assertions.assertThat(ui).asInstanceOf(type(UsernamePasswordUserInfo.class)).satisfies(upui -> {
               Assertions.assertThat(upui.username(false)).isEqualTo("user");
               Assertions.assertThat(upui.password(false)).hasValue("pass");
            });
         });
      });
   }

   @Test
   public void usernamePasswordWhenNoUserInfo() {
      assertThat(GitUri.ofUri("git://host/path")).satisfies(uri -> {
         Assertions.assertThat(uri.username()).isEmpty();
         Assertions.assertThat(uri.password()).isEmpty();
         assertThat(uri).components().hasNoUserInfo();
      });
   }

   @Test
   public void usernamePasswordWhenPartial() {
      assertThat(GitUri.ofUri("git://user@host/path")).satisfies(uri -> {
         Assertions.assertThat(uri.username()).hasValue("user");
         Assertions.assertThat(uri.password()).isEmpty();
         assertThat(uri).components().hasUserInfoSatisfying(ui -> {
            Assertions.assertThat(ui).asInstanceOf(type(UsernamePasswordUserInfo.class)).satisfies(upui -> {
               Assertions.assertThat(upui.username(false)).isEqualTo("user");
               Assertions.assertThat(upui.password(false)).isEmpty();
            });
         });
      });
      assertThat(GitUri.ofUri("git://@host/path")).satisfies(uri -> {
         Assertions.assertThat(uri.username()).hasValue("");
         Assertions.assertThat(uri.password()).isEmpty();
         assertThat(uri).components().hasUserInfoSatisfying(ui -> {
            Assertions.assertThat(ui).asInstanceOf(type(UsernamePasswordUserInfo.class)).satisfies(upui -> {
               Assertions.assertThat(upui.username(false)).isEqualTo("");
               Assertions.assertThat(upui.password(false)).isEmpty();
            });
         });
      });
      assertThat(GitUri.ofUri("git://@host/path")).satisfies(uri -> {
         Assertions.assertThat(uri.username()).hasValue("");
         Assertions.assertThat(uri.password()).isEmpty();
         assertThat(uri).components().hasUserInfoSatisfying(ui -> {
            Assertions.assertThat(ui).asInstanceOf(type(UsernamePasswordUserInfo.class)).satisfies(upui -> {
               Assertions.assertThat(upui.username(false)).isEqualTo("");
               Assertions.assertThat(upui.password(false)).isEmpty();
            });
         });
      });
   }

}