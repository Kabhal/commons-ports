/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.fs;

import static com.tinubu.commons.lang.util.CollectionUtils.collection;
import static java.nio.file.attribute.PosixFilePermission.OWNER_EXECUTE;
import static java.nio.file.attribute.PosixFilePermission.OWNER_READ;
import static java.nio.file.attribute.PosixFilePermission.OWNER_WRITE;
import static org.apache.commons.lang3.StringUtils.stripStart;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.List;
import java.util.function.UnaryOperator;

import org.apache.commons.io.FileUtils;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentContent;
import com.tinubu.commons.ports.document.domain.DocumentMetadata;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.LoadedDocumentContent.LoadedDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata.OpenDocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.capability.UnsupportedCapabilityException;
import com.tinubu.commons.ports.document.domain.testsuite.CommonDocumentRepositoryTest;
import com.tinubu.commons.ports.document.fs.FsDocumentConfig.FsDocumentConfigBuilder;
import com.tinubu.commons.ports.document.fs.storagestrategy.DirectFsStorageStrategy;
import com.tinubu.commons.ports.document.fs.storagestrategy.FsStorageStrategy;
import com.tinubu.commons.ports.document.fs.storagestrategy.HexTreeFsStorageStrategy;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ZipArchiver;

public class FsDocumentRepositoryTest {

   static {
      Assertions.setMaxStackTraceElementsDisplayed(50);
   }

   @Nested
   @TestInstance(
         Lifecycle.PER_CLASS) /* To support non static @BeforeAll in inner classes. */ public class CommonTestsuite
         extends CommonDocumentRepositoryTest {

      private Path TEST_STORAGE_PATH;

      private DocumentRepository documentRepository;

      /**
       * {@inheritDoc}
       * At least the Linux filesystem updates the creation date on overwrite.
       */
      @Override
      public boolean isUpdatingCreationDateOnOverwrite() {
         return true;
      }

      @Override
      protected boolean isSupportingMetadataAttributes() {
         return false;
      }

      @BeforeAll
      public void createStoragePath() {
         try {
            TEST_STORAGE_PATH = Files.createTempDirectory("document-fs-test");

            System.out.printf("Creating '%s' filesystem test storage path%n", TEST_STORAGE_PATH);
         } catch (IOException e) {
            throw new IllegalStateException(e);
         }
      }

      @AfterAll
      public void deleteStoragePath() {
         try {
            System.out.printf("Deleting '%s' filesystem test storage path%n", TEST_STORAGE_PATH);

            FileUtils.deleteDirectory(TEST_STORAGE_PATH.toFile());
         } catch (IOException e) {
            throw new IllegalStateException(e);
         }
      }

      @BeforeEach
      public void configureDocumentRepository() {
         this.documentRepository = newFsDocumentRepository();
      }

      @AfterEach
      public void closeDocumentRepository() {
         this.documentRepository.close();
      }

      @Override
      protected DocumentRepository documentRepository() {
         return documentRepository;
      }

      @Override
      protected void instrumentIOException(Path documentPath, boolean enabled) {
         try {
            Path fsTarget = TEST_STORAGE_PATH.resolve(documentPath);

            if (!fsTarget.toFile().exists()) {
               throw new IllegalStateException(String.format("Can't instrument '%s' FS target for '%s'",
                                                             fsTarget,
                                                             documentPath));
            }

            if (enabled) {
               Files.setPosixFilePermissions(fsTarget, collection(HashSet::new));
            } else {
               Files.setPosixFilePermissions(fsTarget,
                                             collection(HashSet::new,
                                                        OWNER_READ,
                                                        OWNER_WRITE,
                                                        OWNER_EXECUTE));
            }
         } catch (IOException e) {
            throw new IllegalStateException(e);
         }
      }

      @Override
      protected Document zipTransformer(DocumentPath zipPath, List<Document> documents) {
         return new ZipArchiver(zipPath).compress(documents);
      }

      private FsDocumentRepository newFsDocumentRepository() {
         return new FsDocumentRepository(new FsDocumentConfigBuilder()
                                               .storagePath(TEST_STORAGE_PATH)
                                               .storageStrategy(DirectFsStorageStrategy.class)
                                               .build());
      }

      @Override
      protected UnaryOperator<DocumentMetadataBuilder> synchronizeExpectedMetadata(DocumentMetadata actual) {
         return builder -> builder
               .chain(super.synchronizeExpectedMetadata(actual))
               .attributes(actual.attributes())
               .contentType(actual.contentEncoding().orElse(null));
      }

      @Override
      protected UnaryOperator<LoadedDocumentContentBuilder> synchronizeExpectedContent(DocumentContent actual) {
         return builder -> builder
               .chain(super.synchronizeExpectedContent(actual))
               .contentEncoding(actual.contentEncoding().orElse(null));
      }

      @Nested
      public class DocumentRepositoryUriAdapter {

         @Test
         public void testToDocumentUriWhenNominal() {
            assertThat(documentRepository().toUri(DocumentPath.of("file.txt")).stringValue()).isEqualTo(
                  "file:" + TEST_STORAGE_PATH + "/file.txt");
            assertThat(documentRepository().toUri(DocumentPath.of("path/file.txt")).stringValue()).isEqualTo(
                  "file:" + TEST_STORAGE_PATH + "/path/file.txt");
         }

         @Test
         public void testSupportsDocumentUriWhenNominal() {
            assertThat(documentRepository().supportsUri(documentUri("file:"
                                                                    + TEST_STORAGE_PATH
                                                                    + "/path/test.txt"))).isTrue();
            assertThat(documentRepository().supportsUri(documentUri(TEST_STORAGE_PATH
                                                                    + "/path/test.txt"))).isTrue();
         }

         @Test
         public void testSupportsDocumentUriWhenRepositoryUri() {
            assertThatThrownBy(() -> documentRepository().supportsUri(documentUri("file:"
                                                                                  + TEST_STORAGE_PATH)))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessageContaining("parameter must not match URI");
            assertThatThrownBy(() -> documentRepository().supportsUri(documentUri(TEST_STORAGE_PATH.toString())))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessageContaining("parameter must not match URI");
         }

         @Test
         public void testSupportsDocumentUriWhenBadStoragePath() {
            assertThat(documentRepository().supportsUri(documentUri("file:/bad/path/test.txt"))).isFalse();
            assertThat(documentRepository().supportsUri(documentUri("/bad/path/test.txt"))).isFalse();
         }

         @Test
         public void testSupportsDocumentUriWhenAlternativeForms() {
            assertThat(documentRepository().supportsUri(documentUri("file://"
                                                                    + TEST_STORAGE_PATH
                                                                    + "/path/test.txt"))).isTrue();
            assertThat(documentRepository().supportsUri(documentUri("file:"
                                                                    + stripStart(TEST_STORAGE_PATH.toString(),
                                                                                 "/")
                                                                    + "/path/test.txt"))).isFalse();
            assertThat(documentRepository().supportsUri(documentUri(stripStart(TEST_STORAGE_PATH.toString(),
                                                                               "/")
                                                                    + "/path/test.txt"))).isFalse();
            assertThat(documentRepository().supportsUri(documentUri("file://host"
                                                                    + TEST_STORAGE_PATH
                                                                    + "/path/test.txt"))).isFalse();
            assertThatThrownBy(() -> documentRepository().supportsUri(documentUri("file:" + TEST_STORAGE_PATH
                                                                                  + "/path/test.txt#fragment")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessageContaining("must not have a fragment");
            assertThat(documentRepository().supportsUri(documentUri("file:" + TEST_STORAGE_PATH
                                                                    + "/path/test.txt?query"))).isTrue();
         }

         @Test
         public void testToRepositoryUriWhenNominal() {
            assertThat(documentRepository().toUri().stringValue()).isEqualTo("file:" + TEST_STORAGE_PATH);
         }

         @Test
         public void testSupportsRepositoryUriWhenNominal() {
            assertThat(documentRepository().supportsUri(repositoryUri("file:" + TEST_STORAGE_PATH))).isTrue();
            assertThat(documentRepository().supportsUri(repositoryUri(TEST_STORAGE_PATH.toString()))).isTrue();
         }

         @Test
         public void testSupportsRepositoryUriWhenBadStoragePath() {
            assertThat(documentRepository().supportsUri(repositoryUri("file:/bad"))).isFalse();
            assertThat(documentRepository().supportsUri(repositoryUri("/bad"))).isFalse();
         }

         @Test
         public void testFindDocumentByUriWhenNominal() {
            DocumentPath documentPath = DocumentPath.of("path/pathfile2.txt");

            try {
               createDocuments(documentPath);

               assertThat(documentRepository().findDocumentByUri(documentUri("file:"
                                                                             + TEST_STORAGE_PATH
                                                                             + "/path/pathfile2.txt"))).hasValueSatisfying(
                     document -> {
                        assertThat(document.documentId()).isEqualTo(DocumentPath.of("path/pathfile2.txt"));
                        assertThat(document
                                         .metadata()
                                         .documentPath()).isEqualTo(Path.of("path/pathfile2.txt"));
                     });

            } finally {
               deleteDocuments(documentPath);
            }
         }

         @Test
         public void testFindDocumentByUriWhenRepositoryUri() {
            assertThatExceptionOfType(InvalidUriException.class)
                  .isThrownBy(() -> documentRepository().findDocumentByUri(documentUri("file:"
                                                                                       + TEST_STORAGE_PATH)))
                  .withMessage(
                        "Invalid 'file:%1$s' URI: 'storagePath=%1$s' parameter must not match URI '%1$s' path",
                        TEST_STORAGE_PATH);
         }

      }

      @Nested
      public class WhenNotExistingStoragePath {

         @Nested
         public class WhenNotFailFastIfMissingStoragePath {

            @ParameterizedTest
            @ValueSource(classes = { DirectFsStorageStrategy.class, HexTreeFsStorageStrategy.class })
            public void testFindDocumentByIdWhenNotExistingStoragePath(Class<FsStorageStrategy> storageStrategy)
                  throws IOException {
               Path storagePath = Path.of(System.getProperty("java.io.tmpdir", "/tmp"), "test-document-fs");

               try {
                  DocumentRepository repository = new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                                                 .storagePath(storagePath)
                                                                                 .failFastIfMissingStoragePath(
                                                                                       false)
                                                                                 .createStoragePathIfMissing(
                                                                                       false)
                                                                                 .storageStrategy(
                                                                                       storageStrategy)
                                                                                 .build());

                  assertThat(repository.findDocumentById(DocumentPath.of("file.pdf"))).isEmpty();

                  DocumentRepository createStorageIfMissingRepository =
                        new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                       .storagePath(storagePath)
                                                       .failFastIfMissingStoragePath(false)
                                                       .createStoragePathIfMissing(true)
                                                       .storageStrategy(storageStrategy)
                                                       .build());

                  assertThat(createStorageIfMissingRepository.findDocumentById(DocumentPath.of("file.pdf"))).isEmpty();
               } finally {
                  FileUtils.deleteDirectory(storagePath.toFile());
               }
            }

            @ParameterizedTest
            @ValueSource(classes = { DirectFsStorageStrategy.class, HexTreeFsStorageStrategy.class })
            public void testSaveDocumentWhenNotExistingStoragePath(Class<FsStorageStrategy> storageStrategy)
                  throws IOException {
               Path storagePath = Path.of(System.getProperty("java.io.tmpdir", "/tmp"), "test-document-fs");

               try {
                  DocumentRepository repository = new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                                                 .storagePath(storagePath)
                                                                                 .failFastIfMissingStoragePath(
                                                                                       false)
                                                                                 .createStoragePathIfMissing(
                                                                                       false)
                                                                                 .storageStrategy(
                                                                                       storageStrategy)
                                                                                 .build());

                  assertThatExceptionOfType(DocumentAccessException.class).isThrownBy(() -> repository.saveDocument(
                        stubDocument(DocumentPath.of("file.pdf")).build(),
                        false));

                  DocumentRepository createStorageIfMissingRepository =
                        new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                       .storagePath(storagePath)
                                                       .failFastIfMissingStoragePath(false)
                                                       .createStoragePathIfMissing(true)
                                                       .storageStrategy(storageStrategy)
                                                       .build());

                  assertThat(createStorageIfMissingRepository.saveDocument(stubDocument(DocumentPath.of(
                        "file.pdf")).build(), false)).isPresent();
               } finally {
                  FileUtils.deleteDirectory(storagePath.toFile());
               }
            }

            @ParameterizedTest
            @ValueSource(classes = { DirectFsStorageStrategy.class, HexTreeFsStorageStrategy.class })
            public void testOpenDocumentWhenNotExistingStoragePath(Class<FsStorageStrategy> storageStrategy)
                  throws IOException {
               Path storagePath = Path.of(System.getProperty("java.io.tmpdir", "/tmp"), "test-document-fs");

               try {
                  DocumentRepository repository = new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                                                 .storagePath(storagePath)
                                                                                 .failFastIfMissingStoragePath(
                                                                                       false)
                                                                                 .createStoragePathIfMissing(
                                                                                       false)
                                                                                 .storageStrategy(
                                                                                       storageStrategy)
                                                                                 .build());

                  assertThatExceptionOfType(DocumentAccessException.class).isThrownBy(() -> repository.openDocument(
                        DocumentPath.of("file.pdf"),
                        false,
                        false,
                        OpenDocumentMetadataBuilder.empty()));

                  DocumentRepository createStorageIfMissingRepository =
                        new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                       .storagePath(storagePath)
                                                       .failFastIfMissingStoragePath(false)
                                                       .createStoragePathIfMissing(true)
                                                       .storageStrategy(storageStrategy)
                                                       .build());

                  assertThat(createStorageIfMissingRepository.openDocument(DocumentPath.of("file.pdf"),
                                                                           false,
                                                                           false,
                                                                           OpenDocumentMetadataBuilder.empty())).isPresent();
               } finally {
                  FileUtils.deleteDirectory(storagePath.toFile());
               }
            }
         }

         @Nested
         public class WhenFailFastIfMissingStoragePath {

            @ParameterizedTest
            @ValueSource(classes = { DirectFsStorageStrategy.class, HexTreeFsStorageStrategy.class })
            public void testRepositoryWhenFailFastIfMissingStoragePath(Class<FsStorageStrategy> storageStrategy)
                  throws IOException {
               Path storagePath = Path.of(System.getProperty("java.io.tmpdir", "/tmp"), "test-document-fs");

               try {
                  assertThatExceptionOfType(DocumentAccessException.class)
                        .isThrownBy(() -> new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                                         .storagePath(storagePath)
                                                                         .createStoragePathIfMissing(false)
                                                                         .failFastIfMissingStoragePath(true)
                                                                         .storageStrategy(storageStrategy)
                                                                         .build()))
                        .withMessage("Missing '%s' filesystem storage path", storagePath);

                  new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                 .storagePath(storagePath)
                                                 .createStoragePathIfMissing(true)
                                                 .failFastIfMissingStoragePath(true)
                                                 .storageStrategy(storageStrategy)
                                                 .build());
               } finally {
                  FileUtils.deleteDirectory(storagePath.toFile());
               }
            }

            @ParameterizedTest
            @ValueSource(classes = { DirectFsStorageStrategy.class, HexTreeFsStorageStrategy.class })
            public void testRepositoryWhenBasePathAndFailFastIfMissingStoragePath(Class<FsStorageStrategy> storageStrategy)
                  throws IOException {
               Path storagePath = Path.of(System.getProperty("java.io.tmpdir", "/tmp"), "test-document-fs");

               try {
                  assertThatExceptionOfType(DocumentAccessException.class)
                        .isThrownBy(() -> new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                                         .storagePath(storagePath)
                                                                         .basePath(Path.of("basepath"))
                                                                         .createStoragePathIfMissing(false)
                                                                         .failFastIfMissingStoragePath(true)
                                                                         .storageStrategy(storageStrategy)
                                                                         .build()))
                        .withMessage("Missing '%s' filesystem storage path", storagePath);

                  new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                 .storagePath(storagePath)
                                                 .basePath(Path.of("basepath"))
                                                 .createStoragePathIfMissing(true)
                                                 .failFastIfMissingStoragePath(true)
                                                 .storageStrategy(storageStrategy)
                                                 .build());
               } finally {
                  FileUtils.deleteDirectory(storagePath.toFile());
               }
            }

         }
      }

      @ParameterizedTest
      @ValueSource(classes = { DirectFsStorageStrategy.class })
      public void testSubPathWhenSupportedStorageStrategies(Class<FsStorageStrategy> storageStrategy) {
         Path documentPath = path("path/subpath/subpathfile1.pdf");

         try {
            createDocuments(documentPath);

            DocumentRepository repository = new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                                           .storagePath(TEST_STORAGE_PATH)
                                                                           .storageStrategy(storageStrategy)
                                                                           .build());
            repository = repository.subPath(Path.of("path"), false);

            assertThat(repository.findDocumentById(DocumentPath.of("subpath/subpathfile1.pdf"))).hasValueSatisfying(
                  document -> {
                     assertThat(document.documentId()).isEqualTo(DocumentPath.of("subpath/subpathfile1.pdf"));
                  });

         } finally {
            deleteDocuments(documentPath);
         }
      }

      @ParameterizedTest
      @ValueSource(classes = { HexTreeFsStorageStrategy.class })
      public void testSubPathWhenUnsupportedStorageStrategies(Class<FsStorageStrategy> storageStrategy) {
         Path documentPath = path("path/subpath/subpathfile1.pdf");

         try {
            createDocuments(documentPath);

            DocumentRepository repository = new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                                           .storagePath(TEST_STORAGE_PATH)
                                                                           .storageStrategy(storageStrategy)
                                                                           .build());
            assertThatExceptionOfType(UnsupportedCapabilityException.class)
                  .isThrownBy(() -> repository.subPath(Path.of("path"), false))
                  .withMessage("Unsupported 'SUBPATH' (sub-path repository) capability");
         } finally {
            deleteDocuments(documentPath);
         }
      }

   }

}