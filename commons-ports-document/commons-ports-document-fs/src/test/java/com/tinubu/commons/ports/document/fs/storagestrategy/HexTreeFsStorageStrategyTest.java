/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.fs.storagestrategy;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;

import java.nio.file.Path;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.fs.FsDocumentConfig.FsDocumentConfigBuilder;
import com.tinubu.commons.ports.document.fs.FsDocumentConfig.HexTreeStorageStrategyConfig;

class HexTreeFsStorageStrategyTest {

   @Test
   public void testStorageFileWhenNominal() {
      assertThat(newStorageStrategy(2).storageFile(DocumentPath.of("path", "file.ext"))).isEqualTo(Path.of(
            "/tmp/test/6a/82/path/file.ext"));

      assertThat(HexTreeFsStorageStrategy.pathHexaString(Path.of("path/file.ext"))).startsWith("6a82");
   }

   @Test
   public void testStorageFileWhenSimpleFile() {
      assertThat(newStorageStrategy(2).storageFile(DocumentPath.of("file.ext"))).isEqualTo(Path.of(
            "/tmp/test/a6/fd/file.ext"));
   }

   @Test
   public void testStorageFileWhenBadParameters() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newStorageStrategy(2).storageFile(null))
            .withMessage("Invariant validation error > 'documentId' must not be null");
   }

   @Test
   public void testStorageFileWhenBadTreeDepth() {

      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newStorageStrategy(-1).storageFile(DocumentPath.of("path/file.ext")))
            .withMessage(
                  "Invariant validation error > Context [HexTreeStorageStrategyConfig[treeDepth=-1]] > {treeDepth} 'treeDepth=-1' must be strictly positive");

      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newStorageStrategy(0).storageFile(DocumentPath.of("path/file.ext")))
            .withMessage(
                  "Invariant validation error > Context [HexTreeStorageStrategyConfig[treeDepth=0]] > {treeDepth} 'treeDepth=0' must be strictly positive");

      assertThat(newStorageStrategy(16).storageFile(DocumentPath.of("path/file.ext"))).isEqualTo(Path.of(
            "/tmp/test/6a/82/34/7e/f6/27/96/aa/c6/d2/21/cc/fc/82/3d/c4",
            "path/file.ext"));

      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newStorageStrategy(17).storageFile(DocumentPath.of("path/file.ext")))
            .withMessage(
                  "Invariant validation error > Context [HexTreeStorageStrategyConfig[treeDepth=17]] > {treeDepth} 'treeDepth=17' must be less than or equal to '16'");
   }

   private FsStorageStrategy newStorageStrategy(int treeDepth) {
      return FsStorageStrategyFactory.fsStorageStrategy(new FsDocumentConfigBuilder()
                                                              .storagePath(Path.of("/tmp/test"))
                                                              .storageStrategy(HexTreeFsStorageStrategy.class)
                                                              .hexTreeStorageStrategy(
                                                                    HexTreeStorageStrategyConfig.of(treeDepth))
                                                              .build());
   }

   @Test
   public void testDocumentIdWhenNominal() {
      assertThat(newStorageStrategy(2).documentId(Path.of("/tmp/test/6a/82", "path/file.ext"))).isEqualTo(
            DocumentPath.of("path/file.ext"));
   }

   @Test
   public void testDocumentIdWhenBadStoragePath() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newStorageStrategy(2).documentId(Path.of("/badStoragePath/6a/82/path/file.ext")))
            .withMessage(
                  "Invariant validation error > 'storageFile=/badStoragePath/6a/82/path/file.ext' must start with 'storagePath=/tmp/test'");
   }

   @Test
   public void testDocumentIdWhenBadFileFormat() {
      assertThatIllegalArgumentException()
            .isThrownBy(() -> newStorageStrategy(2).documentId(Path.of("/tmp/test/path/file.ext")))
            .withMessage("Unsupported '/tmp/test/path/file.ext' storage file path");
      assertThat(newStorageStrategy(2).documentId(Path.of("/tmp/test/6a/path/file.ext")))
            .as("the path name count must be greater that tree depth, the correctness of the path name is not checked, so 'path' is considered part of hexadecimal tree")
            .isEqualTo(DocumentPath.of("file.ext"));
      assertThatIllegalArgumentException()
            .isThrownBy(() -> newStorageStrategy(2).documentId(Path.of("/tmp/test/6a/file.ext")))
            .withMessage("Unsupported '/tmp/test/6a/file.ext' storage file path");
      assertThat(newStorageStrategy(2).documentId(Path.of("/tmp/test/6a/82/83/path/file.ext")))
            .as("the path name count must be greater that tree depth, the correctness of the path name is not checked, so '83' is considered part of document path")
            .isEqualTo(DocumentPath.of("83/path/file.ext"));
   }

}