/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.fs;

import static com.tinubu.commons.ddd2.uri.DefaultUriRestrictions.noRestrictions;
import static com.tinubu.commons.ddd2.uri.Uri.uri;
import static com.tinubu.commons.lang.util.CheckedPredicate.alwaysTrue;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.allParameters;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.defaultParameters;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.noParameters;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.sensitiveParameters;
import static com.tinubu.commons.ports.document.fs.FsDocumentConfig.FsDocumentConfigBuilder;
import static com.tinubu.commons.ports.document.fs.FsRepositoryUri.Parameters.BASE_PATH;
import static com.tinubu.commons.ports.document.fs.FsRepositoryUri.Parameters.CREATE_STORAGE_PATH_IF_MISSING;
import static com.tinubu.commons.ports.document.fs.FsRepositoryUri.Parameters.DELETE_REPOSITORY_ON_CLOSE;
import static com.tinubu.commons.ports.document.fs.FsRepositoryUri.Parameters.FAIL_FAST_IF_MISSING_STORAGE_PATH;
import static com.tinubu.commons.ports.document.fs.FsRepositoryUri.Parameters.STORAGE_PATH;
import static com.tinubu.commons.ports.document.fs.FsRepositoryUri.Parameters.STORAGE_STRATEGY;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.net.URI;
import java.nio.file.Path;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ddd2.uri.uris.FileUri;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.RepositoryUri;
import com.tinubu.commons.ports.document.fs.storagestrategy.DirectFsStorageStrategy;
import com.tinubu.commons.ports.document.fs.storagestrategy.HexTreeFsStorageStrategy;

public class FsRepositoryUriTest {

   @Nested
   public class OfUriWhenURI {

      @Test
      public void ofUriWhenNominal() {
         assertThat(FsRepositoryUri.ofRepositoryUri(uri("file:/document"))).satisfies(uri -> {
            assertThat(uri.storagePath()).isEqualTo(Path.of("/document"));
            assertThat(uri.basePath()).isEqualTo(Path.of(""));
         });
         assertThat(FsRepositoryUri.ofRepositoryUri("file:/document")).satisfies(uri -> {
            assertThat(uri.storagePath()).isEqualTo(Path.of("/document"));
            assertThat(uri.basePath()).isEqualTo(Path.of(""));
         });
      }

      @Test
      public void ofUriWhenUri() {
         assertThat(FsRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(Uri.ofUri(
               "file:/path?storagePath=/path")))).satisfies(uri -> {
            assertThat(uri.storagePath()).isEqualTo(Path.of("/path"));
            assertThat(uri.basePath()).isEqualTo(Path.of(""));

         });
         assertThat(FsRepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri(Uri.ofUri(
               "file:/path/document?storagePath=/path")))).satisfies(uri -> {
            assertThat(uri.storagePath()).isEqualTo(Path.of("/path"));
            assertThat(uri.basePath()).isEqualTo(Path.of(""));
         });
         assertThat(FsRepositoryUri.ofRepositoryUri(Uri.ofUri("file:/path?storagePath=/path"))).satisfies(uri -> {
            assertThat(uri.storagePath()).isEqualTo(Path.of("/path"));
            assertThat(uri.basePath()).isEqualTo(Path.of(""));
         });
      }

      @Test
      public void ofUriWhenFileUri() {
         assertThat(FsRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(FileUri.ofUri(
               "file:/path?storagePath=/path",
               noRestrictions())))).satisfies(uri -> {
            assertThat(uri.storagePath()).isEqualTo(Path.of("/path"));
            assertThat(uri.basePath()).isEqualTo(Path.of(""));
         });
         assertThat(FsRepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri(FileUri.ofUri(
               "file:/path/document?storagePath=/path",
               noRestrictions())))).satisfies(uri -> {
            assertThat(uri.storagePath()).isEqualTo(Path.of("/path"));
            assertThat(uri.basePath()).isEqualTo(Path.of(""));
         });
         assertThat(FsRepositoryUri.ofRepositoryUri(FileUri.ofUri("file:/path?storagePath=/path",
                                                                  noRestrictions()))).satisfies(uri -> {
            assertThat(uri.storagePath()).isEqualTo(Path.of("/path"));
            assertThat(uri.basePath()).isEqualTo(Path.of(""));
         });
      }

      @Test
      public void ofUriWhenNoScheme() {
         assertThat(FsRepositoryUri.ofRepositoryUri(uri("/document"))).satisfies(uri -> {
            assertThat(uri.storagePath()).isEqualTo(Path.of("/document"));
            assertThat(uri.basePath()).isEqualTo(Path.of(""));
         });
         assertThat(FsRepositoryUri.ofRepositoryUri("/document")).satisfies(uri -> {
            assertThat(uri.storagePath()).isEqualTo(Path.of("/document"));
            assertThat(uri.basePath()).isEqualTo(Path.of(""));
         });
      }

      @Test
      public void ofUriWhenBadParameters() {
         assertThatThrownBy(() -> FsRepositoryUri.ofRepositoryUri((URI) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
         assertThatThrownBy(() -> FsRepositoryUri.ofRepositoryUri((String) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
         assertThatThrownBy(() -> FsRepositoryUri.ofRepositoryUri((RepositoryUri) null,
                                                                  Path.of("/"),
                                                                  Path.of("")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
         assertThatThrownBy(() -> FsRepositoryUri.ofRepositoryUri(FsRepositoryUri.ofRepositoryUri("file:/"),
                                                                  null,
                                                                  Path.of("")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultStoragePath' must not be null");
         assertThatThrownBy(() -> FsRepositoryUri.ofRepositoryUri(FsRepositoryUri.ofRepositoryUri("file:/"),
                                                                  Path.of("/"),
                                                                  null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultBasePath' must not be null");
      }

      @Test
      public void ofUriWhenIllegalArgument() {
         assertThatExceptionOfType(InvalidUriException.class)
               .isThrownBy(() -> FsRepositoryUri.ofRepositoryUri("file:/?storageStrategy=Unknown"))
               .withMessage(
                     "Invalid '%s' URI: Conversion from 'java.lang.String' to 'java.lang.Class' failed for 'Unknown' value",
                     "file:/?storageStrategy=Unknown");
      }

      @Test
      public void ofUriWhenCaseSensitiveParameterKey() {
         assertThat(FsRepositoryUri.ofRepositoryUri("file:/path/document?StOrAgEpAtH=/")).satisfies(uri -> {
            assertThat(uri.storagePath()).isEqualTo(Path.of("/path/document"));
            assertThat(uri.basePath()).isEqualTo(Path.of(""));
         });
      }

      @Test
      public void ofUriWhenNormalized() {
         assertThat(FsRepositoryUri.ofRepositoryUri(uri("file:/path/../."))).satisfies(uri -> {
            assertThat(uri.storagePath()).isEqualTo(Path.of("/"));
            assertThat(uri.basePath()).isEqualTo(Path.of(""));
         });
         assertThat(FsRepositoryUri.ofRepositoryUri("file:/path/../.")).satisfies(uri -> {
            assertThat(uri.storagePath()).isEqualTo(Path.of("/"));
            assertThat(uri.basePath()).isEqualTo(Path.of(""));
         });
         assertThat(FsRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri("file:/path/../."),
                                                    Path.of("/"),
                                                    Path.of(""))).satisfies(uri -> {
            assertThat(uri.storagePath()).isEqualTo(Path.of("/"));
            assertThat(uri.basePath()).isEqualTo(Path.of(""));
         });
         assertThatThrownBy(() -> FsRepositoryUri.ofRepositoryUri(uri("file:/../path")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'file:/../path' URI: 'uri=FileUri[value=file:/../path,restrictions=FileUriRestrictions[relativeFormat=false, relativePath=true, emptyPath=true, query=false, fragment=true],scheme=SimpleScheme[scheme='file'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='/../path', encodedSchemeSpecific='/../path'],authority=<null>,path=SimplePath[path=/../path, encodedPath=/../path],query=KeyValueQuery[query={}, encodedQuery={}],fragment=<null>]' must not have traversal paths");
      }

      @Test
      public void ofUriWhenInvalidUriSyntax() {
         assertThatThrownBy(() -> FsRepositoryUri.ofRepositoryUri("git@git.com:user/repo.git"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'git@git.com:user/repo.git' URI: Illegal character in scheme name at index 3: git@git.com:user/repo.git");
         assertThatThrownBy(() -> FsRepositoryUri.ofRepositoryUri("git@git.com:user/repo.git"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'git@git.com:user/repo.git' URI: Illegal character in scheme name at index 3: git@git.com:user/repo.git");
      }

      @Test
      public void ofUriWhenIncompatibleUri() {
         assertThatThrownBy(() -> FsRepositoryUri.ofRepositoryUri("unknown:/path"))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessage(
                     "Incompatible 'unknown:/path' URI: 'FileUri[value=unknown:/path,restrictions=FileUriRestrictions[relativeFormat=false, relativePath=true, emptyPath=true, query=false, fragment=true],scheme=SimpleScheme[scheme='unknown'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='/path', encodedSchemeSpecific='/path'],authority=<null>,path=SimplePath[path=/path, encodedPath=/path],query=<null>,fragment=<null>]' must be absolute, and scheme must be equal to 'file'");
         assertThatThrownBy(() -> FsRepositoryUri.ofRepositoryUri("file:path"))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessage(
                     "Incompatible 'file:path' URI: 'FileUri[value=file:path,restrictions=FileUriRestrictions[relativeFormat=false, relativePath=true, emptyPath=true, query=false, fragment=true],scheme=SimpleScheme[scheme='file'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='path', encodedSchemeSpecific='path'],authority=<null>,path=<null>,query=<null>,fragment=<null>]' must be hierarchical");
         assertThatThrownBy(() -> FsRepositoryUri.ofRepositoryUri("file://host/path"))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessage(
                     "Incompatible 'file://host/path' URI: 'FileUri[value=file://host/path,restrictions=FileUriRestrictions[relativeFormat=false, relativePath=true, emptyPath=true, query=false, fragment=true],scheme=SimpleScheme[scheme='file'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='//host/path', encodedSchemeSpecific='//host/path'],authority=SimpleServerAuthority[userInfo='null', host='SimpleHost[host='host']', port='null'],path=SimplePath[path=/path, encodedPath=/path],query=<null>,fragment=<null>]' must be empty hierarchical server-based");
         assertThatThrownBy(() -> FsRepositoryUri.ofRepositoryUri("file://user@host/path"))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessage(
                     "Incompatible 'file://user@host/path' URI: 'FileUri[value=file://user@host/path,restrictions=FileUriRestrictions[relativeFormat=false, relativePath=true, emptyPath=true, query=false, fragment=true],scheme=SimpleScheme[scheme='file'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='//user@host/path', encodedSchemeSpecific='//user@host/path'],authority=SimpleServerAuthority[userInfo='SimpleUserInfo[userInfo='user', encodedUserInfo='user']', host='SimpleHost[host='host']', port='null'],path=SimplePath[path=/path, encodedPath=/path],query=<null>,fragment=<null>]' must be empty hierarchical server-based");
         assertThatThrownBy(() -> FsRepositoryUri.ofRepositoryUri("path"))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessage(
                     "Incompatible 'path' URI: 'path=SimplePath[path=path, encodedPath=path]' must be absolute");
      }

      @Test
      public void ofUriWhenInvalidUri() {
         assertThatThrownBy(() -> FsRepositoryUri.ofRepositoryUri("file:/path#fragment"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'file:/path#fragment' URI: 'FileUri[value=file:/path#fragment,restrictions=FileUriRestrictions[relativeFormat=false, relativePath=true, emptyPath=true, query=false, fragment=true],scheme=SimpleScheme[scheme='file'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='/path', encodedSchemeSpecific='/path'],authority=<null>,path=SimplePath[path=/path, encodedPath=/path],query=<null>,fragment=SimpleFragment[fragment=fragment, encodedFragment=fragment]]' must not have a fragment");
      }

      @Test
      public void ofUriWhenRoot() {
         assertThat(FsRepositoryUri.ofRepositoryUri(uri("file:/"))).satisfies(uri -> {
            assertThat(uri.storagePath()).isEqualTo(Path.of("/"));
            assertThat(uri.basePath()).isEqualTo(Path.of(""));
         });
         assertThat(FsRepositoryUri.ofRepositoryUri("file:/")).satisfies(uri -> {
            assertThat(uri.storagePath()).isEqualTo(Path.of("/"));
            assertThat(uri.basePath()).isEqualTo(Path.of(""));
         });
         assertThat(FsRepositoryUri.ofRepositoryUri(uri("/"))).satisfies(uri -> {
            assertThat(uri.storagePath()).isEqualTo(Path.of("/"));
            assertThat(uri.basePath()).isEqualTo(Path.of(""));
         });
         assertThat(FsRepositoryUri.ofRepositoryUri("/")).satisfies(uri -> {
            assertThat(uri.storagePath()).isEqualTo(Path.of("/"));
            assertThat(uri.basePath()).isEqualTo(Path.of(""));
         });
      }

      @Test
      public void ofUriWhenStoragePath() {
         assertThat(FsRepositoryUri.ofRepositoryUri(uri("file:/path"))).satisfies(uri -> {
            assertThat(uri.storagePath()).isEqualTo(Path.of("/path"));
            assertThat(uri.basePath()).isEqualTo(Path.of(""));
         });
         assertThat(FsRepositoryUri.ofRepositoryUri("file:/path")).satisfies(uri -> {
            assertThat(uri.storagePath()).isEqualTo(Path.of("/path"));
            assertThat(uri.basePath()).isEqualTo(Path.of(""));
         });
      }

      @Test
      public void ofUriWhenInvalidStoragePathParameter() {
         assertThatThrownBy(() -> FsRepositoryUri.ofRepositoryUri(
               "file:/path/subpath?storagePath=path/subpath"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid '%s' URI: 'storagePath=path/subpath' parameter must be absolute and must not contain traversal",
                     "file:/path/subpath?storagePath=path/subpath");
         assertThatThrownBy(() -> FsRepositoryUri.ofRepositoryUri("file:/path/subpath?storagePath=../path"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid '%s' URI: 'storagePath=../path' parameter must be absolute and must not contain traversal",
                     "file:/path/subpath?storagePath=../path");
         assertThatThrownBy(() -> FsRepositoryUri.ofRepositoryUri(
               "file:/path/subpath?storagePath=/path/subpath/.."))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid '%s' URI: 'storagePath=/path' parameter must match URI '/path/subpath' path",
                     "file:/path/subpath?storagePath=/path/subpath/..");
      }

      @Test
      public void ofUriWhenExactStoragePathParameter() {
         assertThatThrownBy(() -> FsRepositoryUri.ofRepositoryUri("file:/?storagePath=/path/subpath"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'file:/?storagePath=/path/subpath' URI: 'storagePath=/path/subpath' parameter must match URI '/' path");
         assertThat(FsRepositoryUri.ofRepositoryUri("file:/path/subpath?storagePath=/path/subpath")).satisfies(
               uri -> {
                  assertThat(uri.storagePath()).isEqualTo(Path.of("/path/subpath"));
                  assertThat(uri.basePath()).isEqualTo(Path.of(""));
               });
         assertThat(FsRepositoryUri.ofRepositoryUri("file:/path/subpath?storagePath=/path/./subpath/.")).satisfies(
               uri -> {
                  assertThat(uri.storagePath()).isEqualTo(Path.of("/path/subpath"));
                  assertThat(uri.basePath()).isEqualTo(Path.of(""));
               });
      }

      @Test
      public void ofUriWhenPartialStoragePathParameter() {
         assertThatThrownBy(() -> FsRepositoryUri.ofRepositoryUri("file:/?storagePath=/path"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'file:/?storagePath=/path' URI: 'storagePath=/path' parameter must match URI '/' path");
         assertThatThrownBy(() -> FsRepositoryUri.ofRepositoryUri("file:/path/subpath?storagePath=/path"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid '%s' URI: 'storagePath=/path' parameter must match URI '/path/subpath' path",
                     "file:/path/subpath?storagePath=/path");
         assertThatThrownBy(() -> FsRepositoryUri.ofRepositoryUri("file:/path/subpath?storagePath=/path/."))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid '%s' URI: 'storagePath=/path' parameter must match URI '/path/subpath' path",
                     "file:/path/subpath?storagePath=/path/.");
      }

      @Test
      public void ofUriWhenMismatchingStoragePathParameter() {
         assertThatThrownBy(() -> FsRepositoryUri.ofRepositoryUri("file:/path/subpath?storagePath=/otherpath"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid '%s' URI: 'storagePath=/otherpath' parameter must match URI '/path/subpath' path",
                     "file:/path/subpath?storagePath=/otherpath");
         assertThatThrownBy(() -> FsRepositoryUri.ofRepositoryUri(
               "file:/path/subpath?storagePath=/path/subpath/otherpath"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid '%s' URI: 'storagePath=/path/subpath/otherpath' parameter must match URI '/path/subpath' path",
                     "file:/path/subpath?storagePath=/path/subpath/otherpath");
      }

      @Test
      public void ofUriWhenNoStoragePathParameter() {
         assertThat(FsRepositoryUri.ofRepositoryUri("file:/path/subpath")).satisfies(uri -> {
            assertThat(uri.storagePath()).isEqualTo(Path.of("/path/subpath"));
            assertThat(uri.basePath()).isEqualTo(Path.of(""));
         });
         assertThat(FsRepositoryUri.ofRepositoryUri("file:/path/subpath?basePath=subpath")).satisfies(uri -> {
            assertThat(uri.storagePath()).isEqualTo(Path.of("/path"));
            assertThat(uri.basePath()).isEqualTo(Path.of("subpath"));
         });
         assertThat(FsRepositoryUri.ofRepositoryUri("file:/path/path?basePath=path"))
               .as("basePath is resolved from the end of the path, not from the start")
               .satisfies(uri -> {
                  assertThat(uri.storagePath()).isEqualTo(Path.of("/path"));
                  assertThat(uri.basePath()).isEqualTo(Path.of("path"));
               });
      }

      @Test
      public void ofUriWhenDefaultParameters() {
         assertThat(FsRepositoryUri.ofRepositoryUri("file:/path")).satisfies(uri -> {
            assertThat(uri.storagePath()).isEqualTo(Path.of("/path"));
            assertThat(uri.basePath()).isEqualTo(Path.of(""));
            assertThat(uri.query().parameter(STORAGE_PATH, alwaysTrue())).isEmpty();
            assertThat(uri.query().parameter(BASE_PATH, alwaysTrue())).hasValue(Path.of(""));
            assertThat(uri
                             .query()
                             .parameter(STORAGE_STRATEGY,
                                        alwaysTrue())).hasValue(DirectFsStorageStrategy.class);
            assertThat(uri.query().parameter(CREATE_STORAGE_PATH_IF_MISSING, alwaysTrue())).hasValue(true);
            assertThat(uri.query().parameter(FAIL_FAST_IF_MISSING_STORAGE_PATH, alwaysTrue())).hasValue(true);
            assertThat(uri.query().parameter(DELETE_REPOSITORY_ON_CLOSE, alwaysTrue())).hasValue(false);
         });
      }

      @Test
      public void ofUriWhenAllParameters() {
         assertThat(FsRepositoryUri.ofRepositoryUri("file:/path/subpath"
                                                    + "?storagePath=/path"
                                                    + "&basePath=subpath"
                                                    + "&storageStrategy=com.tinubu.commons.ports.document.fs.storagestrategy.HexTreeFsStorageStrategy"
                                                    + "&createStoragePathIfMissing=false"
                                                    + "&failFastIfMissingStoragePath=false"
                                                    + "&deleteRepositoryOnClose=true")).satisfies(uri -> {
            assertThat(uri.storagePath()).isEqualTo(Path.of("/path"));
            assertThat(uri.basePath()).isEqualTo(Path.of("subpath"));
            assertThat(uri.query().parameter(STORAGE_PATH, alwaysTrue())).hasValue(Path.of("/path"));
            assertThat(uri.query().parameter(BASE_PATH, alwaysTrue())).hasValue(Path.of("subpath"));
            assertThat(uri.query().parameter(STORAGE_STRATEGY, alwaysTrue())).hasValue(
                  HexTreeFsStorageStrategy.class);
            assertThat(uri.query().parameter(CREATE_STORAGE_PATH_IF_MISSING, alwaysTrue())).hasValue(false);
            assertThat(uri.query()
                             .parameter(FAIL_FAST_IF_MISSING_STORAGE_PATH, alwaysTrue())).hasValue(false);
            assertThat(uri.query().parameter(DELETE_REPOSITORY_ON_CLOSE, alwaysTrue())).hasValue(true);
         });
      }

      @Nested
      class WrappedUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(FsRepositoryUri.ofRepositoryUri(uri("document:fs:file:/path"))).satisfies(uri -> {
               assertThat(uri.storagePath()).isEqualTo(Path.of("/path"));
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
            });
         }

         @Test
         public void ofUriWhenBadRepositoryType() {
            assertThatThrownBy(() -> FsRepositoryUri.ofRepositoryUri(uri("document:unknown:file:/")))
                  .isInstanceOf(IncompatibleUriException.class)
                  .hasMessage(
                        "Incompatible 'document:unknown:file:/' URI: Unsupported 'unknown' repository type");
         }

         @Test
         public void ofUriWhenNormalized() {
            assertThat(FsRepositoryUri.ofRepositoryUri(uri("document:fs:file:/path/../."))).satisfies(uri -> {
               assertThat(uri.storagePath()).isEqualTo(Path.of("/"));
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
            });
            assertThat(FsRepositoryUri.ofRepositoryUri("document:fs:file:/path/../.")).satisfies(uri -> {
               assertThat(uri.storagePath()).isEqualTo(Path.of("/"));
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
            });
            assertThat(FsRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
                  "document:fs:file:/path/../."), Path.of("/"), Path.of(""))).satisfies(uri -> {
               assertThat(uri.storagePath()).isEqualTo(Path.of("/"));
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
            });
            assertThatThrownBy(() -> FsRepositoryUri.ofRepositoryUri(uri("document:fs:file:/../path")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'file:/../path' URI: 'uri=FileUri[value=file:/../path,restrictions=FileUriRestrictions[relativeFormat=false, relativePath=true, emptyPath=true, query=false, fragment=true],scheme=SimpleScheme[scheme='file'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='/../path', encodedSchemeSpecific='/../path'],authority=<null>,path=SimplePath[path=/../path, encodedPath=/../path],query=KeyValueQuery[query={}, encodedQuery={}],fragment=<null>]' must not have traversal paths");
         }

         @Test
         public void ofUriWhenUnsupportedWrappedUri() {
            assertThatThrownBy(() -> FsRepositoryUri.ofRepositoryUri(uri("document:fs:unknown://host/path")))
                  .isInstanceOf(IncompatibleUriException.class)
                  .hasMessage(
                        "Incompatible 'unknown://host/path' URI: 'FileUri[value=unknown://host/path,restrictions=FileUriRestrictions[relativeFormat=false, relativePath=true, emptyPath=true, query=false, fragment=true],scheme=SimpleScheme[scheme='unknown'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='//host/path', encodedSchemeSpecific='//host/path'],authority=SimpleServerAuthority[userInfo='null', host='SimpleHost[host='host']', port='null'],path=SimplePath[path=/path, encodedPath=/path],query=<null>,fragment=<null>]' must be absolute, and scheme must be equal to 'file'");
         }

      }
   }

   @Nested
   public class OfUriWhenRepositoryUri {

      @Test
      public void ofUriWhenBadDefaultStoragePath() {
         assertThatThrownBy(() -> FsRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri("file:/path"),
                                                                  null,
                                                                  Path.of("")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultStoragePath' must not be null");
         assertThatThrownBy(() -> FsRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri("file:/path"),
                                                                  Path.of(""),
                                                                  Path.of("")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultStoragePath=' must be absolute path");
         assertThatThrownBy(() -> FsRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri("file:/path"),
                                                                  Path.of("path"),
                                                                  Path.of("")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultStoragePath=path' must be absolute path");
         assertThat(FsRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri("file:/path"),
                                                    Path.of("/../path"),
                                                    Path.of("")))
               .as("defaultStoragePath is normalized to '/path', so that it doesn't fail")
               .isNotNull();
      }

      @Nested
      public class WhenRepositoryUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(FsRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri("file:/path"),
                                                       Path.of("/path"),
                                                       Path.of(""))).satisfies(uri -> {
               assertThat(uri.storagePath()).isEqualTo(Path.of("/path"));
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
            });
         }

         @Test
         public void ofUriWhenImplicitDefaultStoragePathAndBasePath() {
            assertThat(FsRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri("file:/path/subpath"))).satisfies(
                  uri -> {
                     assertThat(uri.storagePath()).isEqualTo(Path.of("/path/subpath"));
                     assertThat(uri.basePath()).isEqualTo(Path.of(""));
                  });
         }

         @Test
         public void ofUriWhenDefaultStoragePathAndBasePath() {
            assertThat(FsRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri("file:/path/subpath"),
                                                       Path.of("/path"),
                                                       Path.of("subpath"))).satisfies(uri -> {
               assertThat(uri.storagePath()).isEqualTo(Path.of("/path"));
               assertThat(uri.basePath()).isEqualTo(Path.of("subpath"));
            });
         }

         @Test
         public void ofUriWhenMismatchingDefaultStoragePathAndBasePath() {
            assertThatThrownBy(() -> FsRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
                  "file:/path/subpath"), Path.of("/path"), Path.of("")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'file:/path/subpath' URI: 'storagePath=/path' parameter must match URI '/path/subpath' path");
            assertThatThrownBy(() -> FsRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
                  "file:/path/subpath"), Path.of("/"), Path.of("path")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'file:/path/subpath' URI: 'basePath=path' parameter must complete storage path to match URI '/path/subpath' path");
            assertThatThrownBy(() -> FsRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
                  "file:/path/subpath"), Path.of("/"), Path.of("subpath")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'file:/path/subpath' URI: 'basePath=subpath' parameter must complete storage path to match URI '/path/subpath' path");
         }

         @Test
         public void ofUriWhenRoot() {
            assertThat(FsRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri("file:/"))).satisfies(uri -> {
               assertThat(uri.storagePath()).isEqualTo(Path.of("/"));
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
            });
            assertThat(FsRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri("file:/"),
                                                       Path.of("/"),
                                                       Path.of(""))).satisfies(uri -> {
               assertThat(uri.storagePath()).isEqualTo(Path.of("/"));
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
            });
         }

         @Test
         public void ofUriWhenExplicitStoragePathAndBasePath() {
            assertThat(FsRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
                                                             "file:/path/subpath?storagePath=/path&basePath=subpath"),
                                                       Path.of("/"),
                                                       Path.of(""))).satisfies(uri -> {
               assertThat(uri.storagePath()).isEqualTo(Path.of("/path"));
               assertThat(uri.basePath()).isEqualTo(Path.of("subpath"));
            });
         }

         @Test
         public void ofUriWhenMismatchingExplicitStoragePathAndBasePath() {
            assertThatThrownBy(() -> FsRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
                  "file:/path/subpath?storagePath=/path&basePath=")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'file:/path/subpath?storagePath=/path&basePath=' URI: 'storagePath=/path' parameter must match URI '/path/subpath' path");
            assertThatThrownBy(() -> FsRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
                  "file:/path/subpath?storagePath=/&basePath=path")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'file:/path/subpath?storagePath=/&basePath=path' URI: 'basePath=path' parameter must complete storage path to match URI '/path/subpath' path");
            assertThatThrownBy(() -> FsRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
                  "file:/path/subpath?storagePath=/&basePath=subpath")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'file:/path/subpath?storagePath=/&basePath=subpath' URI: 'basePath=subpath' parameter must complete storage path to match URI '/path/subpath' path");
         }

      }

      @Nested
      public class WhenDocumentUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(FsRepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri("file:/path/document"),
                                                       Path.of("/path"),
                                                       Path.of(""))).satisfies(uri -> {
               assertThat(uri.storagePath()).isEqualTo(Path.of("/path"));
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
            });
         }

         @Test
         public void ofUriWhenImplicitDefaultStoragePathAndBasePath() {
            assertThat(FsRepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri("file:/path/subpath/document"))).satisfies(
                  uri -> {
                     assertThat(uri.storagePath()).isEqualTo(Path.of("/path/subpath"));
                     assertThat(uri.basePath()).isEqualTo(Path.of(""));
                  });
         }

         @Test
         public void ofUriWhenDefaultStoragePathAndBasePath() {
            assertThat(FsRepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri("file:/path/subpath/document"),
                                                       Path.of("/path"),
                                                       Path.of("subpath"))).satisfies(uri -> {
               assertThat(uri.storagePath()).isEqualTo(Path.of("/path"));
               assertThat(uri.basePath()).isEqualTo(Path.of("subpath"));
            });
            assertThat(FsRepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri("file:/path/subpath/document"),
                                                       Path.of("/path"),
                                                       Path.of(""))).satisfies(uri -> {
               assertThat(uri.storagePath()).isEqualTo(Path.of("/path"));
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
            });
            assertThat(FsRepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri("file:/path/subpath/document"),
                                                       Path.of("/"),
                                                       Path.of(""))).satisfies(uri -> {
               assertThat(uri.storagePath()).isEqualTo(Path.of("/"));
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
            });
         }

         @Test
         public void ofUriWhenMismatchingDefaultStoragePathAndBasePath() {
            assertThatThrownBy(() -> FsRepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri(
                  "file:/path/subpath/document"), Path.of("/"), Path.of("subpath")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'file:/path/subpath/document' URI: 'basePath=subpath' parameter must complete storage path to start URI '/path/subpath/document' path");
         }

         @Test
         public void ofUriWhenRoot() {
            assertThat(FsRepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri("file:/document"))).satisfies(
                  uri -> {
                     assertThat(uri.storagePath()).isEqualTo(Path.of("/"));
                     assertThat(uri.basePath()).isEqualTo(Path.of(""));
                  });
            assertThat(FsRepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri("file:/document"),
                                                       Path.of("/"),
                                                       Path.of(""))).satisfies(uri -> {
               assertThat(uri.storagePath()).isEqualTo(Path.of("/"));
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
            });
         }

         @Test
         public void ofUriWhenExplicitStoragePathAndBasePath() {
            assertThat(FsRepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri(
                                                             "file:/path/subpath/document?storagePath=/path&basePath=subpath"),
                                                       Path.of("/"),
                                                       Path.of(""))).satisfies(uri -> {
               assertThat(uri.storagePath()).isEqualTo(Path.of("/path"));
               assertThat(uri.basePath()).isEqualTo(Path.of("subpath"));
            });
         }

         @Test
         public void ofUriWhenMismatchingExplicitStoragePathAndBasePath() {
            assertThatThrownBy(() -> FsRepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri(
                  "file:/path/subpath/document?storagePath=/&basePath=subpath")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'file:/path/subpath/document?storagePath=/&basePath=subpath' URI: 'basePath=subpath' parameter must complete storage path to start URI '/path/subpath/document' path");
         }

      }

      @Nested
      public class WhenFsRepositoryUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(FsRepositoryUri.ofRepositoryUri(FsRepositoryUri.ofRepositoryUri("file:/path"),
                                                       Path.of("/"),
                                                       Path.of(""))).satisfies(uri -> {
               assertThat(uri.storagePath()).isEqualTo(Path.of("/path"));
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
            });
         }

         @Test
         public void ofUriWhenImplicitDefaultStoragePathAndBasePath() {
            assertThat(FsRepositoryUri.ofRepositoryUri(FsRepositoryUri.ofRepositoryUri("file:/path/subpath"))).satisfies(
                  uri -> {
                     assertThat(uri.storagePath()).isEqualTo(Path.of("/path/subpath"));
                     assertThat(uri.basePath()).isEqualTo(Path.of(""));
                  });
         }

         @Test
         public void ofUriWhenDefaultStoragePathAndBasePath() {
            assertThat(FsRepositoryUri.ofRepositoryUri(FsRepositoryUri.ofRepositoryUri("file:/path/subpath"),
                                                       Path.of("/"),
                                                       Path.of("path"))).satisfies(uri -> {
               assertThat(uri.storagePath()).isEqualTo(Path.of("/path/subpath"));
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
            });
         }
      }

      @Nested
      public class WhenFsDocumentUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(FsRepositoryUri.ofRepositoryUri(FsDocumentUri.ofDocumentUri("file:/path/document",
                                                                                   Path.of("/path"),
                                                                                   Path.of("")))).satisfies(
                  uri -> {
                     assertThat(uri.storagePath()).isEqualTo(Path.of("/path"));
                     assertThat(uri.basePath()).isEqualTo(Path.of(""));
                  });
         }

         @Test
         public void ofUriWhenImplicitDefaultStoragePathAndBasePath() {
            assertThat(FsRepositoryUri.ofRepositoryUri(FsDocumentUri.ofDocumentUri(
                  "file:/path/subpath/document"))).satisfies(uri -> {
               assertThat(uri.storagePath()).isEqualTo(Path.of("/path/subpath"));
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
            });
         }

         @Test
         public void ofUriWhenDefaultStoragePathAndBasePath() {
            assertThat(FsRepositoryUri.ofRepositoryUri(FsDocumentUri.ofDocumentUri(
                  "file:/path/subpath/document"), Path.of("/path"), Path.of("subpath"))).satisfies(uri -> {
               assertThat(uri.storagePath()).isEqualTo(Path.of("/path/subpath"));
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
            });
         }
      }
   }

   @Nested
   public class OfConfig {

      @Test
      public void ofConfigWhenNominal() {
         assertThat(FsRepositoryUri.ofConfig(new FsDocumentConfigBuilder()
                                                   .storagePath(Path.of("/path"))
                                                   .basePath(Path.of("subpath"))
                                                   .storageStrategy(HexTreeFsStorageStrategy.class)
                                                   .failFastIfMissingStoragePath(false)
                                                   .createStoragePathIfMissing(true)
                                                   .deleteRepositoryOnClose(true)
                                                   .build(), Path.of("/"))).satisfies(uri -> {
            assertThat(uri.storagePath()).isEqualTo(Path.of("/path"));
            assertThat(uri.basePath()).isEqualTo(Path.of("subpath"));
            assertThat(uri.exportUri(allParameters(false)).toURI().toString()).isEqualTo("file:/path/subpath"
                                                                                         + "?storagePath=/path"
                                                                                         + "&basePath=subpath"
                                                                                         + "&createStoragePathIfMissing=true"
                                                                                         + "&failFastIfMissingStoragePath=false"
                                                                                         + "&storageStrategy=com.tinubu.commons.ports.document.fs.storagestrategy.HexTreeFsStorageStrategy"
                                                                                         + "&deleteRepositoryOnClose=true");

            assertThat(uri.query().parameter(STORAGE_PATH, alwaysTrue())).hasValue(Path.of("/path"));
            assertThat(uri.query().parameter(BASE_PATH, alwaysTrue())).hasValue(Path.of("subpath"));
            assertThat(uri.query().parameter(STORAGE_STRATEGY, alwaysTrue())).hasValue(
                  HexTreeFsStorageStrategy.class);
            assertThat(uri.query().parameter(CREATE_STORAGE_PATH_IF_MISSING, alwaysTrue())).hasValue(true);
            assertThat(uri.query()
                             .parameter(FAIL_FAST_IF_MISSING_STORAGE_PATH, alwaysTrue())).hasValue(false);
            assertThat(uri.query().parameter(DELETE_REPOSITORY_ON_CLOSE, alwaysTrue())).hasValue(true);
         });
      }

      @Test
      public void ofConfigWhenBadConfiguration() {
         assertThatThrownBy(() -> FsRepositoryUri.ofConfig(null, Path.of("/")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'config' must not be null");
         assertThatThrownBy(() -> FsRepositoryUri.ofConfig(new FsDocumentConfigBuilder().build(), null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'currentStoragePath' must not be null");
      }

   }

   @Nested
   public class ToConfig {

      @Test
      public void toConfigWhenNominal() {
         assertThat(FsRepositoryUri
                          .ofRepositoryUri("file:/path/subpath"
                                           + "?storagePath=/path"
                                           + "&basePath=subpath"
                                           + "&storageStrategy=com.tinubu.commons.ports.document.fs.storagestrategy.HexTreeFsStorageStrategy"
                                           + "&createStoragePathIfMissing=true"
                                           + "&failFastIfMissingStoragePath=false"
                                           + "&deleteRepositoryOnClose=true")
                          .toConfig()).satisfies(config -> {
            assertThat(config.storagePath()).isEqualTo(Path.of("/path"));
            assertThat(config.basePath()).isEqualTo(Path.of("subpath"));
            assertThat(config.storageStrategy()).isEqualTo(HexTreeFsStorageStrategy.class);
            assertThat(config.createStoragePathIfMissing()).isEqualTo(true);
            assertThat(config.failFastIfMissingStoragePath()).isEqualTo(false);
            assertThat(config.deleteRepositoryOnClose()).isEqualTo(true);
         });
      }

   }

   @Nested
   public class ToUri {

      @Test
      public void toUriWhenNominal() {
         assertThat(FsRepositoryUri
                          .ofRepositoryUri("file:/")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("file:/");
      }

      @Test
      public void toUriWhenStoragePath() {
         assertThat(FsRepositoryUri.ofRepositoryUri("file:/path").exportUri(noParameters())
                          .stringValue()).isEqualTo("file:/path");
      }

      @Test
      public void toUriWhenDefaultParameters() {
         assertThat(FsRepositoryUri
                          .ofRepositoryUri("file:/path")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("file:/path");
         assertThat(FsRepositoryUri
                          .ofRepositoryUri("file:/path?storagePath=/path")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("file:/path");
      }

      @Test
      public void toUriWhenDefaultParametersWithForceDefaultValues() {
         assertThat(FsRepositoryUri.ofRepositoryUri("file:/path").exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo(
               "file:/path?storagePath=/path&basePath=&createStoragePathIfMissing=true&failFastIfMissingStoragePath=true&storageStrategy=com.tinubu.commons.ports.document.fs.storagestrategy.DirectFsStorageStrategy&deleteRepositoryOnClose=false");
         assertThat(FsRepositoryUri
                          .ofRepositoryUri("file:/path?storagePath=/path")
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo(
               "file:/path?storagePath=/path&basePath=&createStoragePathIfMissing=true&failFastIfMissingStoragePath=true&storageStrategy=com.tinubu.commons.ports.document.fs.storagestrategy.DirectFsStorageStrategy&deleteRepositoryOnClose=false");
         assertThat(FsRepositoryUri.ofRepositoryUri("file:/?storagePath=/").exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo(
               "file:/?storagePath=/&basePath=&createStoragePathIfMissing=true&failFastIfMissingStoragePath=true&storageStrategy=com.tinubu.commons.ports.document.fs.storagestrategy.DirectFsStorageStrategy&deleteRepositoryOnClose=false");
      }

      @Test
      public void toUriWhenSensitiveParameters() {
         assertThat(FsRepositoryUri.ofRepositoryUri("file:/path").exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("file:/path");
         assertThat(FsRepositoryUri
                          .ofRepositoryUri("file:/path?storagePath=/path")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("file:/path");
      }

   }

   @Nested
   public class NormalizeResolveRelativize {

      @Test
      public void normalizeWhenNominal() {
         assertThat(FsRepositoryUri.ofRepositoryUri("file:/path/../.").normalize()).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo("file:/");
         });
      }

      @Test
      public void normalizeWhenNoScheme() {
         assertThat(FsRepositoryUri.ofRepositoryUri("/path/../.").normalize()).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo("/");
         });
      }

      @Test
      public void resolveWhenNominal() {
         assertThat(FsRepositoryUri
                          .ofRepositoryUri("file:/path")
                          .resolve(uri("otherpath"))).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo("file:/otherpath");
         });
         assertThat(FsRepositoryUri
                          .ofRepositoryUri("file:/path/")
                          .resolve(uri("otherpath"))).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo("file:/path/otherpath");
         });
      }

      @Test
      public void relativizeWhenNominal() {
         assertThatThrownBy(() -> FsRepositoryUri
               .ofRepositoryUri("file:/path")
               .relativize(uri("file:/path/otherpath"))).isInstanceOf(UnsupportedOperationException.class);
      }

      @Test
      public void relativizeWhenNoScheme() {
         assertThatThrownBy(() -> FsRepositoryUri
               .ofRepositoryUri("/path")
               .relativize(uri("/path/otherpath"))).isInstanceOf(UnsupportedOperationException.class);
      }

   }

}