/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.fs;

import static com.tinubu.commons.ddd2.uri.DefaultUriRestrictions.noRestrictions;
import static com.tinubu.commons.ddd2.uri.Uri.uri;
import static com.tinubu.commons.lang.util.CheckedPredicate.alwaysTrue;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.allParameters;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.defaultParameters;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.noParameters;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.sensitiveParameters;
import static com.tinubu.commons.ports.document.fs.FsRepositoryUri.Parameters.BASE_PATH;
import static com.tinubu.commons.ports.document.fs.FsRepositoryUri.Parameters.CREATE_STORAGE_PATH_IF_MISSING;
import static com.tinubu.commons.ports.document.fs.FsRepositoryUri.Parameters.DELETE_REPOSITORY_ON_CLOSE;
import static com.tinubu.commons.ports.document.fs.FsRepositoryUri.Parameters.FAIL_FAST_IF_MISSING_STORAGE_PATH;
import static com.tinubu.commons.ports.document.fs.FsRepositoryUri.Parameters.STORAGE_PATH;
import static com.tinubu.commons.ports.document.fs.FsRepositoryUri.Parameters.STORAGE_STRATEGY;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.net.URI;
import java.nio.file.Path;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ddd2.uri.uris.FileUri;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.fs.FsDocumentConfig.FsDocumentConfigBuilder;
import com.tinubu.commons.ports.document.fs.storagestrategy.HexTreeFsStorageStrategy;

// FIXME Test basepath combinations with/out storagePath
public class FsDocumentUriTest {

   @Nested
   public class OfUriWhenURI {

      @Test
      public void ofUriWhenNominal() {
         assertThat(FsDocumentUri.ofDocumentUri(uri("file:/document"), Path.of("/"), Path.of(""))).satisfies(
               uri -> {
                  assertThat(uri.storagePath()).isEqualTo(Path.of("/"));
                  assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
               });
         assertThat(FsDocumentUri.ofDocumentUri("file:/document",
                                                Path.of("/"),
                                                Path.of(""))).satisfies(uri -> {
            assertThat(uri.storagePath()).isEqualTo(Path.of("/"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
      }

      @Test
      public void ofUriWhenUri() {
         assertThat(FsDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(Uri.ofUri(
               "file:/path/document?storagePath=/path")))).satisfies(uri -> {
            assertThat(uri.storagePath()).isEqualTo(Path.of("/path"));
            assertThat(uri.basePath()).isEqualTo(Path.of(""));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
         assertThat(FsDocumentUri.ofDocumentUri(Uri.ofUri("file:/path/document?storagePath=/path"))).satisfies(
               uri -> {
                  assertThat(uri.storagePath()).isEqualTo(Path.of("/path"));
                  assertThat(uri.basePath()).isEqualTo(Path.of(""));
                  assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
               });
      }

      @Test
      public void ofUriWhenFileUri() {
         assertThat(FsDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(FileUri.ofUri(
               "file:/path/document?storagePath=/path",
               noRestrictions())))).satisfies(uri -> {
            assertThat(uri.storagePath()).isEqualTo(Path.of("/path"));
            assertThat(uri.basePath()).isEqualTo(Path.of(""));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
         assertThat(FsDocumentUri.ofDocumentUri(FileUri.ofUri("file:/path/document?storagePath=/path",
                                                              noRestrictions()))).satisfies(uri -> {
            assertThat(uri.storagePath()).isEqualTo(Path.of("/path"));
            assertThat(uri.basePath()).isEqualTo(Path.of(""));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
      }

      @Test
      public void ofUriWhenAuthority() {
         assertThatThrownBy(() -> FsDocumentUri.ofDocumentUri(uri("file://user:password@host/document"),
                                                              Path.of("/"),
                                                              Path.of("")))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessage(
                     "Incompatible 'file://user:password@host/document' URI: 'FileUri[value=file://user:password@host/document,restrictions=FileUriRestrictions[relativeFormat=false, relativePath=true, emptyPath=true, query=false, fragment=true],scheme=SimpleScheme[scheme='file'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='//user:password@host/document', encodedSchemeSpecific='//user:password@host/document'],authority=SimpleServerAuthority[userInfo='SimpleUserInfo[userInfo='user:password', encodedUserInfo='user:password']', host='SimpleHost[host='host']', port='null'],path=SimplePath[path=/document, encodedPath=/document],query=<null>,fragment=<null>]' must be empty hierarchical server-based");
      }

      @Test
      public void ofUriWhenBadParameters() {
         assertThatThrownBy(() -> FsDocumentUri.ofDocumentUri((URI) null, Path.of("/"), Path.of("")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
         assertThatThrownBy(() -> FsDocumentUri.ofDocumentUri(uri("file:/document"), null, Path.of("")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultStoragePath' must not be null");
         assertThatThrownBy(() -> FsDocumentUri.ofDocumentUri(uri("file:/document"), Path.of("/"), null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultBasePath' must not be null");
         assertThatThrownBy(() -> FsDocumentUri.ofDocumentUri((String) null, Path.of("/"), Path.of("")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
         assertThatThrownBy(() -> FsDocumentUri.ofDocumentUri("file:/document", null, Path.of("")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultStoragePath' must not be null");
         assertThatThrownBy(() -> FsDocumentUri.ofDocumentUri("file:/document", Path.of("/"), null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultBasePath' must not be null");
         assertThatThrownBy(() -> FsDocumentUri.ofDocumentUri((DocumentUri) null, Path.of("/"), Path.of("")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
         assertThatThrownBy(() -> FsDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri("file:/document"),
                                                              null,
                                                              Path.of("")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultStoragePath' must not be null");
         assertThatThrownBy(() -> FsDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri("file:/document"),
                                                              Path.of("/"),
                                                              null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultBasePath' must not be null");
      }

      @Test
      @Disabled(
            "There's no way to create an URI with an invalid Path, the only way is tu use \u0000 but it also fails the URI preemptively")
      public void ofUriWhenIllegalArgument() {
         assertThatExceptionOfType(InvalidUriException.class)
               .isThrownBy(() -> FsDocumentUri.ofDocumentUri("file:/document?storageStrategy=Unknown",
                                                             Path.of("/"),
                                                             Path.of("")))
               .withMessage(
                     "Invalid '%s' URI: Conversion from 'java.lang.String' to 'java.lang.Class' failed for 'Unknown' value",
                     "file:/document?storageStrategy=Unknown");
      }

      @Test
      public void ofUriWhenCaseSensitiveParameterKey() {
         assertThat(FsDocumentUri.ofDocumentUri("file:/path/document?StoragePath=/otherpath",
                                                Path.of("/"),
                                                Path.of(""))).satisfies(uri -> {
            assertThat(uri.storagePath()).isEqualTo(Path.of("/"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/document"));
         });
      }

      @Test
      public void ofUriWhenNormalized() {
         assertThat(FsDocumentUri.ofDocumentUri(uri("file:/path/.././document"),
                                                Path.of("/"),
                                                Path.of(""))).satisfies(uri -> {
            assertThat(uri.storagePath()).isEqualTo(Path.of("/"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
         assertThat(FsDocumentUri.ofDocumentUri("file:/path/.././document",
                                                Path.of("/"),
                                                Path.of(""))).satisfies(uri -> {
            assertThat(uri.storagePath()).isEqualTo(Path.of("/"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
         assertThat(FsDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri("file:/path/.././document"),
                                                Path.of("/"),
                                                Path.of(""))).satisfies(uri -> {
            assertThat(uri.storagePath()).isEqualTo(Path.of("/"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
         assertThatThrownBy(() -> FsDocumentUri.ofDocumentUri(uri("file:/../path/document"),
                                                              Path.of("/"),
                                                              Path.of("")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'file:/../path/document' URI: 'uri=FileUri[value=file:/../path/document,restrictions=FileUriRestrictions[relativeFormat=false, relativePath=true, emptyPath=true, query=false, fragment=true],scheme=SimpleScheme[scheme='file'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='/../path/document', encodedSchemeSpecific='/../path/document'],authority=<null>,path=SimplePath[path=/../path/document, encodedPath=/../path/document],query=KeyValueQuery[query={}, encodedQuery={}],fragment=<null>]' must not have traversal paths");
      }

      @Test
      public void ofUriWhenInvalidUriSyntax() {
         assertThatThrownBy(() -> FsDocumentUri.ofDocumentUri("git@git.com:user/repo.git",
                                                              Path.of(""),
                                                              Path.of("")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'git@git.com:user/repo.git' URI: Illegal character in scheme name at index 3: git@git.com:user/repo.git");
         assertThatThrownBy(() -> FsDocumentUri.ofDocumentUri("git@git.com:user/repo.git",
                                                              Path.of(""),
                                                              Path.of("")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'git@git.com:user/repo.git' URI: Illegal character in scheme name at index 3: git@git.com:user/repo.git");
      }

      @Test
      public void ofUriWhenIncompatibleUri() {
         assertThatThrownBy(() -> FsDocumentUri.ofDocumentUri(uri("unknown:/path/document"),
                                                              Path.of("/"),
                                                              Path.of("")))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessage(
                     "Incompatible 'unknown:/path/document' URI: 'FileUri[value=unknown:/path/document,restrictions=FileUriRestrictions[relativeFormat=false, relativePath=true, emptyPath=true, query=false, fragment=true],scheme=SimpleScheme[scheme='unknown'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='/path/document', encodedSchemeSpecific='/path/document'],authority=<null>,path=SimplePath[path=/path/document, encodedPath=/path/document],query=<null>,fragment=<null>]' must be absolute, and scheme must be equal to 'file'");
         assertThatThrownBy(() -> FsDocumentUri.ofDocumentUri("file:path/document",
                                                              Path.of("/"),
                                                              Path.of("")))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessage(
                     "Incompatible 'file:path/document' URI: 'FileUri[value=file:path/document,restrictions=FileUriRestrictions[relativeFormat=false, relativePath=true, emptyPath=true, query=false, fragment=true],scheme=SimpleScheme[scheme='file'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='path/document', encodedSchemeSpecific='path/document'],authority=<null>,path=<null>,query=<null>,fragment=<null>]' must be hierarchical");
         assertThatThrownBy(() -> FsDocumentUri.ofDocumentUri("file://host/path/document",
                                                              Path.of("/"),
                                                              Path.of("")))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessage(
                     "Incompatible 'file://host/path/document' URI: 'FileUri[value=file://host/path/document,restrictions=FileUriRestrictions[relativeFormat=false, relativePath=true, emptyPath=true, query=false, fragment=true],scheme=SimpleScheme[scheme='file'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='//host/path/document', encodedSchemeSpecific='//host/path/document'],authority=SimpleServerAuthority[userInfo='null', host='SimpleHost[host='host']', port='null'],path=SimplePath[path=/path/document, encodedPath=/path/document],query=<null>,fragment=<null>]' must be empty hierarchical server-based");
         assertThatThrownBy(() -> FsDocumentUri.ofDocumentUri("file://user@host/path/document",
                                                              Path.of("/"),
                                                              Path.of("")))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessage(
                     "Incompatible 'file://user@host/path/document' URI: 'FileUri[value=file://user@host/path/document,restrictions=FileUriRestrictions[relativeFormat=false, relativePath=true, emptyPath=true, query=false, fragment=true],scheme=SimpleScheme[scheme='file'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='//user@host/path/document', encodedSchemeSpecific='//user@host/path/document'],authority=SimpleServerAuthority[userInfo='SimpleUserInfo[userInfo='user', encodedUserInfo='user']', host='SimpleHost[host='host']', port='null'],path=SimplePath[path=/path/document, encodedPath=/path/document],query=<null>,fragment=<null>]' must be empty hierarchical server-based");
      }

      @Test
      public void ofUriWhenInvalidUri() {
         assertThatThrownBy(() -> FsDocumentUri.ofDocumentUri("file:/path/document#fragment",
                                                              Path.of("/"),
                                                              Path.of("")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'file:/path/document#fragment' URI: 'FileUri[value=file:/path/document#fragment,restrictions=FileUriRestrictions[relativeFormat=false, relativePath=true, emptyPath=true, query=false, fragment=true],scheme=SimpleScheme[scheme='file'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='/path/document', encodedSchemeSpecific='/path/document'],authority=<null>,path=SimplePath[path=/path/document, encodedPath=/path/document],query=<null>,fragment=SimpleFragment[fragment=fragment, encodedFragment=fragment]]' must not have a fragment");
      }

      @Test
      public void ofUriWhenStoragePath() {
         assertThat(FsDocumentUri.ofDocumentUri(uri("file:/path/document"),
                                                Path.of("/"),
                                                Path.of(""))).satisfies(uri -> {
            assertThat(uri.storagePath()).isEqualTo(Path.of("/"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/document"));
         });
         assertThat(FsDocumentUri.ofDocumentUri("file:/path/document", Path.of("/"), Path.of(""))).satisfies(
               uri -> {
                  assertThat(uri.storagePath()).isEqualTo(Path.of("/"));
                  assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/document"));
               });
      }

      @Test
      public void ofUriWhenBadDefaultStoragePath() {
         assertThatThrownBy(() -> FsDocumentUri.ofDocumentUri("file:/document", null, Path.of("")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultStoragePath' must not be null");
         assertThatThrownBy(() -> FsDocumentUri.ofDocumentUri("file:/document", Path.of(""), Path.of("")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultStoragePath=' must be absolute path");
         assertThatThrownBy(() -> FsDocumentUri.ofDocumentUri("file:/document", Path.of("path"), Path.of("")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultStoragePath=path' must be absolute path");
         assertThatThrownBy(() -> FsDocumentUri.ofDocumentUri("file:/document", Path.of("path"), Path.of("")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultStoragePath=path' must be absolute path");
         assertThat(FsDocumentUri.ofDocumentUri("file:/path/document", Path.of("/../path"), Path.of("")))
               .as("defaultStoragePath is normalized to /path, so that it doesn't fail")
               .isNotNull();
      }

      @Test
      public void ofUriWhenRootStoragePath() {
         assertThat(FsDocumentUri.ofDocumentUri(uri("file:/path/document"),
                                                Path.of("/"),
                                                Path.of(""))).satisfies(uri -> {
            assertThat(uri.storagePath()).isEqualTo(Path.of("/"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/document"));
         });
         assertThat(FsDocumentUri.ofDocumentUri("file:/path/document", Path.of("/"), Path.of(""))).satisfies(
               uri -> {
                  assertThat(uri.storagePath()).isEqualTo(Path.of("/"));
                  assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/document"));
               });
      }

      @Test
      public void ofUriWhenDefaultStoragePath() {
         assertThat(FsDocumentUri.ofDocumentUri(uri("file:/path/document"),
                                                Path.of("/path"),
                                                Path.of(""))).satisfies(uri -> {
            assertThat(uri.storagePath()).isEqualTo(Path.of("/path"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
         assertThat(FsDocumentUri.ofDocumentUri("file:/path/document",
                                                Path.of("/path"),
                                                Path.of(""))).satisfies(uri -> {
            assertThat(uri.storagePath()).isEqualTo(Path.of("/path"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
      }

      @Test
      public void ofUriWhenMismatchingDefaultStoragePath() {
         assertThat(FsDocumentUri.ofDocumentUri(uri("file:/path/document"),
                                                Path.of("/otherpath"),
                                                Path.of("")))
               .as("Default fs prefix is only applied if it matches actual URI")
               .satisfies(uri -> {
                  assertThat(uri.storagePath()).isEqualTo(Path.of("/path"));
                  assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
               });
         assertThat(FsDocumentUri.ofDocumentUri("file:/path/document", Path.of("/otherpath"), Path.of("")))
               .as("Default fs prefix is only applied if it matches actual URI")
               .satisfies(uri -> {
                  assertThat(uri.storagePath()).isEqualTo(Path.of("/path"));
                  assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
               });
      }

      @Test
      public void ofUriWhenInvalidStoragePathParameter() {
         assertThatThrownBy(() -> FsDocumentUri.ofDocumentUri(
               "file:/path/subpath/key?storagePath=/path/subpath/key"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'file:/path/subpath/key?storagePath=/path/subpath/key' URI: 'storagePath=/path/subpath/key' parameter must not match URI '/path/subpath/key' path");
         assertThatThrownBy(() -> FsDocumentUri.ofDocumentUri(
               "file:/path/subpath/document?storagePath=path/subpath"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid '%s' URI: 'storagePath=path/subpath' parameter must be absolute and must not contain traversal",
                     "file:/path/subpath/document?storagePath=path/subpath");
         assertThatThrownBy(() -> FsDocumentUri.ofDocumentUri(
               "file:/path/subpath/document?storagePath=../path"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid '%s' URI: 'storagePath=../path' parameter must be absolute and must not contain traversal",
                     "file:/path/subpath/document?storagePath=../path");
         assertThat(FsDocumentUri.ofDocumentUri("file:/path/subpath/document?storagePath=/path/subpath/..")).satisfies(
               uri -> {
                  assertThat(uri.storagePath()).isEqualTo(Path.of("/path"));
                  assertThat(uri.documentId()).isEqualTo(DocumentPath.of("subpath/document"));
               });
      }

      @Test
      public void ofUriWhenExactStoragePathParameter() {
         assertThatThrownBy(() -> FsDocumentUri.ofDocumentUri("file:/document?storagePath=/path/subpath"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'file:/document?storagePath=/path/subpath' URI: 'storagePath=/path/subpath' parameter must start URI '/document' path");
         assertThat(FsDocumentUri.ofDocumentUri("file:/path/subpath/document?storagePath=/path/subpath")).satisfies(
               uri -> {
                  assertThat(uri.storagePath()).isEqualTo(Path.of("/path/subpath"));
                  assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
               });
         assertThat(FsDocumentUri.ofDocumentUri("file:/path/subpath/document?storagePath=/path/./subpath/.")).satisfies(
               uri -> {
                  assertThat(uri.storagePath()).isEqualTo(Path.of("/path/subpath"));
                  assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
               });
      }

      @Test
      public void ofUriWhenPartialStoragePathParameter() {
         assertThatThrownBy(() -> FsDocumentUri.ofDocumentUri("file:/document/?storagePath=/path"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'file:/document/?storagePath=/path' URI: 'storagePath=/path' parameter must start URI '/document' path");
         assertThat(FsDocumentUri.ofDocumentUri("file:/path/subpath/document?storagePath=/path")).satisfies(
               uri -> {
                  assertThat(uri.storagePath()).isEqualTo(Path.of("/path"));
                  assertThat(uri.documentId()).isEqualTo(DocumentPath.of("subpath/document"));

               });
         assertThat(FsDocumentUri.ofDocumentUri("file:/path/subpath/document?storagePath=/path/.")).satisfies(
               uri -> {
                  assertThat(uri.storagePath()).isEqualTo(Path.of("/path"));
                  assertThat(uri.documentId()).isEqualTo(DocumentPath.of("subpath/document"));
               });
      }

      @Test
      public void ofUriWhenMismatchingStoragePathParameter() {
         assertThatThrownBy(() -> FsDocumentUri.ofDocumentUri(
               "file:/path/subpath/document?storagePath=/otherpath"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'file:/path/subpath/document?storagePath=/otherpath' URI: 'storagePath=/otherpath' parameter must start URI '/path/subpath/document' path");
         assertThatThrownBy(() -> FsDocumentUri.ofDocumentUri(
               "file:/path/subpath/document?storagePath=/path/subpath/otherpath"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'file:/path/subpath/document?storagePath=/path/subpath/otherpath' URI: 'storagePath=/path/subpath/otherpath' parameter must start URI '/path/subpath/document' path");
      }

      @Test
      public void ofUriWhenDefaultParameters() {
         assertThat(FsDocumentUri.ofDocumentUri("file:/document")).satisfies(uri -> {
            assertThat(uri.storagePath()).isEqualTo(Path.of("/"));
            assertThat(uri.query().parameter(STORAGE_PATH, alwaysTrue())).hasValue(Path.of("/"));
         });
      }

      @Test
      public void ofUriWhenAllParameters() {
         assertThat(FsDocumentUri.ofDocumentUri("file:/path/document?storagePath=/path")).satisfies(uri -> {
            assertThat(uri.storagePath()).isEqualTo(Path.of("/path"));
            assertThat(uri.query().parameter(STORAGE_PATH, alwaysTrue())).hasValue(Path.of("/path"));
         });
      }

      @Nested
      class WrappedUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(FsDocumentUri.ofDocumentUri(uri("document:fs:file:/document"))).satisfies(uri -> {
               assertThat(uri.storagePath()).isEqualTo(Path.of("/"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
         }

         @Test
         public void ofUriWhenDefaultStoragePath() {
            assertThat(FsDocumentUri.ofDocumentUri(uri("document:fs:file:/path/document"),
                                                   Path.of("/path"),
                                                   Path.of(""))).satisfies(uri -> {
               assertThat(uri.storagePath()).isEqualTo(Path.of("/path"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
         }

         @Test
         public void ofUriWhenBadRepositoryType() {
            assertThatThrownBy(() -> FsDocumentUri.ofDocumentUri(uri("document:unknown:file:/document")))
                  .isInstanceOf(IncompatibleUriException.class)
                  .hasMessage(
                        "Incompatible 'document:unknown:file:/document' URI: Unsupported 'unknown' repository type");
         }

         @Test
         public void ofUriWhenNormalized() {
            assertThat(FsDocumentUri.ofDocumentUri(uri("document:fs:file:/path/.././document"))).satisfies(uri -> {
               assertThat(uri.storagePath()).isEqualTo(Path.of("/"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
            assertThat(FsDocumentUri.ofDocumentUri("document:fs:file:/path/.././document")).satisfies(uri -> {
               assertThat(uri.storagePath()).isEqualTo(Path.of("/"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
            assertThat(FsDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(
                  "document:fs:file:/path/.././document"))).satisfies(uri -> {
               assertThat(uri.storagePath()).isEqualTo(Path.of("/"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
            assertThatThrownBy(() -> FsDocumentUri.ofDocumentUri(uri("document:fs:file:/../path/document")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'file:/../path/document' URI: 'uri=FileUri[value=file:/../path/document,restrictions=FileUriRestrictions[relativeFormat=false, relativePath=true, emptyPath=true, query=false, fragment=true],scheme=SimpleScheme[scheme='file'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='/../path/document', encodedSchemeSpecific='/../path/document'],authority=<null>,path=SimplePath[path=/../path/document, encodedPath=/../path/document],query=KeyValueQuery[query={}, encodedQuery={}],fragment=<null>]' must not have traversal paths");
         }

      }
   }

   @Nested
   public class OfUriWhenDocumentUri {

      @Test
      public void ofUriWhenBadDefaultStoragePath() {
         assertThatThrownBy(() -> FsDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri("file:/path"),
                                                              null,
                                                              Path.of("")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultStoragePath' must not be null");
         assertThatThrownBy(() -> FsDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri("file:/path"),
                                                              Path.of(""),
                                                              Path.of("")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultStoragePath=' must be absolute path");
         assertThatThrownBy(() -> FsDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri("file:/path"),
                                                              Path.of("path"),
                                                              Path.of("")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultStoragePath=path' must be absolute path");
         assertThat(FsDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri("file:/path/document"),
                                                Path.of("/../path"),
                                                Path.of("")))
               .as("defaultStoragePath is normalized to '/path', so that it doesn't fail")
               .isNotNull();
      }

      @Nested
      public class WhenDocumentUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(FsDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri("file:/path/document"),
                                                   Path.of("/path"),
                                                   Path.of(""))).satisfies(uri -> {
               assertThat(uri.storagePath()).isEqualTo(Path.of("/path"));
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
         }

         @Test
         public void ofUriWhenImplicitDefaultStoragePathAndBasePath() {
            assertThat(FsDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri("file:/path/subpath/document"))).satisfies(
                  uri -> {
                     assertThat(uri.storagePath()).isEqualTo(Path.of("/path/subpath"));
                     assertThat(uri.basePath()).isEqualTo(Path.of(""));
                     assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
                  });
         }

         @Test
         public void ofUriWhenDefaultStoragePathAndBasePath() {
            assertThat(FsDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri("file:/path/subpath/document"),
                                                   Path.of("/path"),
                                                   Path.of("subpath"))).satisfies(uri -> {
               assertThat(uri.storagePath()).isEqualTo(Path.of("/path"));
               assertThat(uri.basePath()).isEqualTo(Path.of("subpath"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
         }

         @Test
         public void ofUriWhenMismatchingDefaultStoragePathAndBasePath() {
            assertThatThrownBy(() -> FsDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(
                  "file:/path/subpath/document"), Path.of("/path/subpath/document"), Path.of("")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'file:/path/subpath/document' URI: 'storagePath=/path/subpath/document' parameter must not match URI '/path/subpath/document' path");
            assertThatThrownBy(() -> FsDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(
                  "file:/path/subpath/document"), Path.of("/path/subpath"), Path.of("document")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'file:/path/subpath/document' URI: 'basePath=document' parameter must complete storage path but not end URI '/path/subpath/document' path");
            assertThatThrownBy(() -> FsDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(
                  "file:/path/subpath/document"), Path.of("/"), Path.of("subpath")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'file:/path/subpath/document' URI: 'basePath=subpath' parameter must complete storage path to start URI '/path/subpath/document' path");
         }

         @Test
         public void ofUriWhenRoot() {
            assertThat(FsDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri("file:/document"))).satisfies(uri -> {
               assertThat(uri.storagePath()).isEqualTo(Path.of("/"));
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
            assertThat(FsDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri("file:/document"),
                                                   Path.of("/"),
                                                   Path.of(""))).satisfies(uri -> {
               assertThat(uri.storagePath()).isEqualTo(Path.of("/"));
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
            assertThatThrownBy(() -> FsDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri("file:/")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage("Invalid 'file:/' URI: Missing document id");
         }

         @Test
         public void ofUriWhenExplicitStoragePathAndBasePath() {
            assertThat(FsDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(
                                                         "file:/path/subpath/document?storagePath=/path&basePath=subpath"),
                                                   Path.of("/"),
                                                   Path.of(""))).satisfies(uri -> {
               assertThat(uri.storagePath()).isEqualTo(Path.of("/path"));
               assertThat(uri.basePath()).isEqualTo(Path.of("subpath"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
         }

         @Test
         public void ofUriWhenMismatchingExplicitStoragePathAndBasePath() {
            assertThatThrownBy(() -> FsDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(
                  "file:/path/subpath/document?storagePath=/path/subpath/document")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'file:/path/subpath/document?storagePath=/path/subpath/document' URI: 'storagePath=/path/subpath/document' parameter must not match URI '/path/subpath/document' path");
            assertThatThrownBy(() -> FsDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(
                  "file:/path/subpath/document?storagePath=/path/subpath&basePath=document")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'file:/path/subpath/document?storagePath=/path/subpath&basePath=document' URI: 'basePath=document' parameter must complete storage path but not end URI '/path/subpath/document' path");
            assertThatThrownBy(() -> FsDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(
                  "file:/path/subpath/document?storagePath=/&basePath=subpath")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'file:/path/subpath/document?storagePath=/&basePath=subpath' URI: 'basePath=subpath' parameter must complete storage path to start URI '/path/subpath/document' path");
         }

      }

      @Nested
      public class WhenFsDocumentUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(FsDocumentUri.ofDocumentUri(FsDocumentUri.ofDocumentUri("file:/path/document",
                                                                               Path.of("/path"),
                                                                               Path.of("")))).satisfies(uri -> {
               assertThat(uri.storagePath()).isEqualTo(Path.of("/path"));
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
         }

         @Test
         public void ofUriWhenImplicitDefaultStoragePathAndBasePath() {
            assertThat(FsDocumentUri.ofDocumentUri(FsDocumentUri.ofDocumentUri("file:/path/subpath/document"))).satisfies(
                  uri -> {
                     assertThat(uri.storagePath()).isEqualTo(Path.of("/path/subpath"));
                     assertThat(uri.basePath()).isEqualTo(Path.of(""));
                     assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
                  });
         }

         @Test
         public void ofUriWhenDefaultStoragePathAndBasePath() {
            assertThat(FsDocumentUri.ofDocumentUri(FsDocumentUri.ofDocumentUri("file:/path/subpath/document"),
                                                   Path.of("/path"),
                                                   Path.of("subpath"))).satisfies(uri -> {
               assertThat(uri.storagePath()).isEqualTo(Path.of("/path/subpath"));
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
         }
      }
   }

   @Nested
   public class OfConfig {

      @Test
      public void ofConfigWhenNominal() {
         assertThat(FsDocumentUri.ofConfig(new FsDocumentConfigBuilder()
                                                 .storagePath(Path.of("/path"))
                                                 .basePath(Path.of("subpath"))
                                                 .storageStrategy(HexTreeFsStorageStrategy.class)
                                                 .failFastIfMissingStoragePath(false)
                                                 .createStoragePathIfMissing(true)
                                                 .deleteRepositoryOnClose(true)
                                                 .build(),
                                           Path.of("/"),
                                           DocumentPath.of("document"))).satisfies(uri -> {
            assertThat(uri.storagePath()).isEqualTo(Path.of("/path"));
            assertThat(uri.basePath()).isEqualTo(Path.of("subpath"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            assertThat(uri.exportUri(allParameters(false)).toURI().toString()).isEqualTo(
                  "file:/path/subpath/document"
                  + "?storagePath=/path"
                  + "&basePath=subpath"
                  + "&createStoragePathIfMissing=true"
                  + "&failFastIfMissingStoragePath=false"
                  + "&storageStrategy=com.tinubu.commons.ports.document.fs.storagestrategy.HexTreeFsStorageStrategy"
                  + "&deleteRepositoryOnClose=true");

            assertThat(uri.query().parameter(STORAGE_PATH, alwaysTrue())).hasValue(Path.of("/path"));
            assertThat(uri.query().parameter(BASE_PATH, alwaysTrue())).hasValue(Path.of("subpath"));
            assertThat(uri.query().parameter(STORAGE_STRATEGY, alwaysTrue())).hasValue(
                  HexTreeFsStorageStrategy.class);
            assertThat(uri.query().parameter(CREATE_STORAGE_PATH_IF_MISSING, alwaysTrue())).hasValue(true);
            assertThat(uri.query()
                             .parameter(FAIL_FAST_IF_MISSING_STORAGE_PATH, alwaysTrue())).hasValue(false);
            assertThat(uri.query().parameter(DELETE_REPOSITORY_ON_CLOSE, alwaysTrue())).hasValue(true);
         });
      }

      @Test
      public void ofConfigWhenBadConfiguration() {
         assertThatThrownBy(() -> FsDocumentUri.ofConfig(null, Path.of("/")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'config' must not be null");
         assertThatThrownBy(() -> FsDocumentUri.ofConfig(new FsDocumentConfigBuilder().build(), null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'currentStoragePath' must not be null");
      }

   }

   @Nested
   public class ToUri {

      @Test
      public void toUriWhenNominal() {
         assertThat(FsDocumentUri.ofDocumentUri("file:/document").exportUri(noParameters())
                          .stringValue()).isEqualTo("file:/document");
      }

      @Test
      public void toUriWhenStoragePath() {
         assertThat(FsDocumentUri
                          .ofDocumentUri("file:/path/document", Path.of("/"), Path.of(""))
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("file:/path/document");
         assertThat(FsDocumentUri
                          .ofDocumentUri("file:/path/document", Path.of("/path"), Path.of(""))
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("file:/path/document");
      }

      @Test
      public void toUriWhenDefaultParameters() {
         assertThat(FsDocumentUri.ofDocumentUri("file:/path/document").exportUri(defaultParameters())
                          .stringValue()).isEqualTo("file:/path/document");
         assertThat(FsDocumentUri
                          .ofDocumentUri("file:/path/subpath/document?storagePath=/path")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("file:/path/subpath/document?storagePath=/path");
         assertThat(FsDocumentUri
                          .ofDocumentUri("file:/path/document", Path.of("/path"), Path.of(""))
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("file:/path/document");
         assertThat(FsDocumentUri
                          .ofDocumentUri("file:/path/subpath/document", Path.of("/path"), Path.of(""))
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("file:/path/subpath/document?storagePath=/path");
         assertThat(FsDocumentUri
                          .ofDocumentUri("file:/path/document", Path.of("/otherpath"), Path.of(""))
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("file:/path/document");
      }

      @Test
      public void toUriWhenDefaultParametersWithForceDefaultValues() {
         assertThat(FsDocumentUri
                          .ofDocumentUri("file:/document", Path.of("/"), Path.of(""))
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo(
               "file:/document?storagePath=/&basePath=&createStoragePathIfMissing=true&failFastIfMissingStoragePath=true&storageStrategy=com.tinubu.commons.ports.document.fs.storagestrategy.DirectFsStorageStrategy&deleteRepositoryOnClose=false");
         assertThat(FsDocumentUri
                          .ofDocumentUri("file:/document?storagePath=/")
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo(
               "file:/document?storagePath=/&basePath=&createStoragePathIfMissing=true&failFastIfMissingStoragePath=true&storageStrategy=com.tinubu.commons.ports.document.fs.storagestrategy.DirectFsStorageStrategy&deleteRepositoryOnClose=false");
         assertThat(FsDocumentUri
                          .ofDocumentUri("file:/path/document", Path.of("/path"), Path.of(""))
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo(
               "file:/path/document?storagePath=/path&basePath=&createStoragePathIfMissing=true&failFastIfMissingStoragePath=true&storageStrategy=com.tinubu.commons.ports.document.fs.storagestrategy.DirectFsStorageStrategy&deleteRepositoryOnClose=false");
         assertThat(FsDocumentUri
                          .ofDocumentUri("file:/path/document?storagePath=/path")
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo(
               "file:/path/document?storagePath=/path&basePath=&createStoragePathIfMissing=true&failFastIfMissingStoragePath=true&storageStrategy=com.tinubu.commons.ports.document.fs.storagestrategy.DirectFsStorageStrategy&deleteRepositoryOnClose=false");
      }

      @Test
      public void toUriWhenSensitiveParameters() {
         assertThat(FsDocumentUri.ofDocumentUri("file:/path/document").exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("file:/path/document");
         assertThat(FsDocumentUri
                          .ofDocumentUri("file:/path/subpath/document?storagePath=/path")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("file:/path/subpath/document?storagePath=/path");
      }

   }
}