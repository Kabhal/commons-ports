/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.fs;

import static com.tinubu.commons.ddd2.invariant.Validate.validate;
import static com.tinubu.commons.ddd2.invariant.rules.ClassRules.instanceOf;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotAbsolute;
import static com.tinubu.commons.lang.util.CheckedPredicate.alwaysTrue;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optionalPredicate;
import static com.tinubu.commons.lang.util.PathUtils.contains;
import static com.tinubu.commons.lang.util.PathUtils.emptyPath;
import static com.tinubu.commons.lang.util.PathUtils.endsWith;
import static com.tinubu.commons.lang.util.PathUtils.indexOf;
import static com.tinubu.commons.lang.util.PathUtils.isEmpty;
import static com.tinubu.commons.lang.util.PathUtils.isRoot;
import static com.tinubu.commons.lang.util.PathUtils.removeEnd;
import static com.tinubu.commons.lang.util.PathUtils.removeStart;
import static com.tinubu.commons.lang.util.PathUtils.startsWith;
import static com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery.ExportUriFilter.isAlwaysTrue;
import static com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery.ExportUriFilter.isRegisteredParameter;
import static com.tinubu.commons.ports.document.domain.uri.UriUtils.exportUriFilter;
import static com.tinubu.commons.ports.document.fs.Constants.DEFAULT_BASE_PATH;
import static com.tinubu.commons.ports.document.fs.Constants.DEFAULT_CREATE_STORAGE_PATH_IF_MISSING;
import static com.tinubu.commons.ports.document.fs.Constants.DEFAULT_DELETE_REPOSITORY_ON_CLOSE;
import static com.tinubu.commons.ports.document.fs.Constants.DEFAULT_FAIL_FAST_IF_MISSING_STORAGE_PATH;
import static com.tinubu.commons.ports.document.fs.Constants.DEFAULT_STORAGE_STRATEGY;
import static com.tinubu.commons.ports.document.fs.FsDocumentConfig.FsDocumentConfigBuilder;
import static com.tinubu.commons.ports.document.fs.FsRepositoryUri.Parameters.BASE_PATH;
import static com.tinubu.commons.ports.document.fs.FsRepositoryUri.Parameters.CREATE_STORAGE_PATH_IF_MISSING;
import static com.tinubu.commons.ports.document.fs.FsRepositoryUri.Parameters.DELETE_REPOSITORY_ON_CLOSE;
import static com.tinubu.commons.ports.document.fs.FsRepositoryUri.Parameters.FAIL_FAST_IF_MISSING_STORAGE_PATH;
import static com.tinubu.commons.ports.document.fs.FsRepositoryUri.Parameters.STORAGE_PATH;
import static com.tinubu.commons.ports.document.fs.FsRepositoryUri.Parameters.STORAGE_STRATEGY;
import static com.tinubu.commons.ports.document.fs.FsRepositoryUri.Parameters.allParameters;
import static com.tinubu.commons.ports.document.fs.FsRepositoryUri.Parameters.valueParameters;
import static java.util.function.Predicate.not;

import java.net.URI;
import java.nio.file.Path;
import java.util.List;
import java.util.function.BiPredicate;

import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.domain.type.Id;
import com.tinubu.commons.ddd2.invariant.Invariant;
import com.tinubu.commons.ddd2.invariant.Invariants;
import com.tinubu.commons.ddd2.invariant.Validate;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.invariant.rules.PathRules;
import com.tinubu.commons.ddd2.invariant.rules.domain.ids.UriRules.ComponentUriRules;
import com.tinubu.commons.ddd2.uri.ComponentUri;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ddd2.uri.parts.SimplePath;
import com.tinubu.commons.ddd2.uri.uris.FileUri;
import com.tinubu.commons.ddd2.uri.uris.FileUri.FileUriRestrictions;
import com.tinubu.commons.lang.convert.ConversionFailedException;
import com.tinubu.commons.lang.util.Pair;
import com.tinubu.commons.lang.util.PathUtils;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.uri.AbstractRepositoryUri;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.ExportUriOptions;
import com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery;
import com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery.Parameter;
import com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery.ValueParameter;
import com.tinubu.commons.ports.document.domain.uri.RepositoryUri;

/**
 * Filesystem URI repository URI. This URI must be used to identify a repository, use
 * {@link FsDocumentUri} to identify a document.
 * <p>
 * The storage path will be set to URI path, excluding base path, by default. Storage path can be set from
 * {@code storagePath} URI query string parameter.
 * The base path will be set to {@code ""} by default. Base path can be set from {@code basePath} URI query
 * string parameter.
 * <p>
 * Supported URI format :
 * <ul>
 *    <li>{@code file:</storage-path>[/<base-path>][?<parameters>]}</li>
 *    <li>Commons-ports wrapped URI : {@code document:fs:<fs-uri>}</li>
 * </ul>
 * <p>
 * Supported URI parameters :
 * <ul>
 * <li>{@code storagePath} (Path) [&lt;automatic&gt;] : absolute filesystem storage root path</li>
 * <li>{@code basePath} (Path) [&lt;automatic&gt;] : relative base path for all documents, can be empty</li>
 * <li>{@code createStoragePathIfMissing} (Boolean) [{@value Constants#DEFAULT_CREATE_STORAGE_PATH_IF_MISSING}] : whether to create storage path is missing</li>
 * <li>{@code failFastIfMissingStoragePath} (Boolean) [{@value Constants#DEFAULT_FAIL_FAST_IF_MISSING_STORAGE_PATH}] : whether to fail-fast if storage path is missing</li>
 * <li>{@code storageStrategy} (Class&lt;FsStorageStrategy&gt;[Class FQN]) [com.tinubu.commons.ports.document.fs.storagestrategy.DirectFsStorageStrategy] : document storage strategy</li>
 * <li>{@code deleteRepositoryOnClose} (Boolean) [{@value Constants#DEFAULT_DELETE_REPOSITORY_ON_CLOSE}] : whether to physically delete repository storage path on close - USE WITH CAUTION</li>
 * </ul>
 *
 * @see FsDocumentConfig
 * @see FsDocumentUri
 */
// FIXME Supports HexTree config
public class FsRepositoryUri extends AbstractRepositoryUri implements RepositoryUri {

   private static final String WRAPPED_URI_REPOSITORY_TYPE = "fs";
   /**
    * Feature flag : whether to use {@link #defaultStoragePath(Uri, boolean)} to override
    * {@link Parameters#STORAGE_PATH} default value in {@link #exportUri(ExportUriOptions)}. If set to
    * {@code true}, {@code storagePath} parameter won't be exported if it matches default behavior,
    * depending on export options.
    */
   private static final boolean EXPORT_URI_DEFAULT_STORAGE_PATH_AS_DEFAULT_VALUE = true;
   /**
    * Feature flag : whether to use {@link #defaultBasePath()} to override
    * {@link Parameters#BASE_PATH} default value in {@link #exportUri(ExportUriOptions)}. If set to
    * {@code true}, {@code basePath} parameter won't be exported if it matches default behavior,
    * depending on export options.
    */
   private static final boolean EXPORT_URI_DEFAULT_BASE_PATH_AS_DEFAULT_VALUE = true;

   protected final Path storagePath;
   protected final Path basePath;
   protected final DocumentPath documentId;

   protected FsRepositoryUri(FileUri uri, boolean documentUri) {
      super(uri, documentUri);

      try {
         var parsed = parseUri(documentUri);
         this.storagePath = parsed.getLeft().getLeft();
         this.basePath = parsed.getLeft().getRight();
         this.documentId = parsed.getRight();
      } catch (ConversionFailedException e) {
         throw new InvalidUriException(uri, e).subMessage(e);
      }
   }

   @Override
   @SuppressWarnings("unchecked")
   protected Fields<? extends FsRepositoryUri> defineDomainFields() {
      return Fields
            .<FsRepositoryUri>builder()
            .superFields((Fields<FsRepositoryUri>) super.defineDomainFields())
            .field("storagePath", v -> v.storagePath, PathRules.isAbsolute().andValue(hasNoTraversal()))
            .field("basePath", v -> v.basePath, PathRules.isNotAbsolute().andValue(hasNoTraversal()))
            .build();
   }

   @Override
   public Invariants domainInvariants() {
      return Invariants.of(Invariant
                                 .of(() -> uri,
                                     instanceOf(ComponentUri.class,
                                                ComponentUriRules.path(ComponentUriRules.PathRules.isAbsolute())))
                                 .groups(COMPATIBILITY_GROUP));
   }

   /**
    * Creates a repository URI from {@link Uri}. Specified URI must represent a "repository" URI.
    *
    * @param uri repository URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static FsRepositoryUri ofRepositoryUri(Uri uri) {
      Check.notNull(uri, "uri");

      validateUriCompatibility(uri, "uri", isCompatibleUriType(FileUri.class));

      return buildUri(() -> new FsRepositoryUri(fileUri(uri), false));
   }

   /**
    * Creates a repository URI from {@link URI}. Specified URI must represent a "repository" URI.
    *
    * @param uri repository URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static FsRepositoryUri ofRepositoryUri(URI uri) {
      Check.notNull(uri, "uri");

      return ofRepositoryUri(Uri.ofUri(uri));
   }

   /**
    * Creates a repository URI from URI string. Specified URI must represent a "repository" URI.
    *
    * @param uri repository URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static FsRepositoryUri ofRepositoryUri(String uri) {
      Check.notNull(uri, "uri");

      return ofRepositoryUri(Uri.ofUri(uri));
   }

   /**
    * Creates a repository URI from configuration.
    *
    * @param config repository configuration
    * @param currentStoragePath storage path to use if {@link FsDocumentConfig#storagePath()} is empty
    *
    * @return new instance
    */
   public static FsRepositoryUri ofConfig(FsDocumentConfig config, Path currentStoragePath) {
      Check.notNull(config, "config");
      Check.notNull(currentStoragePath, "currentStoragePath");

      return buildUri(() -> new FsRepositoryUri(fileUri(config, currentStoragePath, null), false));
   }

   /**
    * Creates a repository URI from {@link RepositoryUri}. Specified repository URI can can be a
    * {@link RepositoryUri} to represent a "repository" URI, or a {@link DocumentUri} to represent a
    * "document" URI.
    *
    * @param uri repository URI
    * @param defaultStoragePath default base path to use if {@code storagePath} query
    *       parameter not present
    * @param defaultBasePath default base path to use if {@code basePath} query
    *       parameter not present
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    * @implSpec this method is package-private because user code should always create
    *       {@link RepositoryUri} directly from a URI.
    */
   static FsRepositoryUri ofRepositoryUri(RepositoryUri uri, Path defaultStoragePath, Path defaultBasePath) {
      Check.notNull(uri, "uri");
      var normalizedDefaultStoragePath = Check.validate(nullable(defaultStoragePath, Path::normalize),
                                                        "defaultStoragePath",
                                                        PathRules.isAbsolute().andValue(hasNoTraversal()));
      var normalizedDefaultBasePath = Check.validate(nullable(defaultBasePath, Path::normalize),
                                                     "defaultBasePath",
                                                     isNotAbsolute().andValue(hasNoTraversal()));

      validateUriCompatibility(uri, "uri", isCompatibleRepositoryUriType(FsRepositoryUri.class));

      if (uri instanceof FsRepositoryUri) {
         return buildUri(() -> new FsRepositoryUri(mapQuery(fileUri(uri), (u, q) -> {
            q
                  .registerParameter(STORAGE_PATH.defaultValue(((FsRepositoryUri) uri).storagePath()))
                  .registerParameter(BASE_PATH.defaultValue(((FsRepositoryUri) uri).basePath()));
            return u;
         }), uri instanceof DocumentUri));
      } else {
         return buildUri(() -> new FsRepositoryUri(mapQuery(fileUri(uri), (u, q) -> {
            q
                  .registerParameter(STORAGE_PATH.defaultValue(normalizedDefaultStoragePath))
                  .registerParameter(BASE_PATH.defaultValue(normalizedDefaultBasePath));
            return u;
         }), uri instanceof DocumentUri));
      }
   }

   /**
    * Creates a repository URI from {@link RepositoryUri}. Specified repository URI can can be a
    * {@link RepositoryUri} to represent a "repository" URI, or a {@link DocumentUri} to represent a
    * "document" URI.
    * <p>
    * Default storage path is set to URI parent path.
    * Default base path is set to {@code ""}.
    *
    * @param uri repository URI
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    * @implSpec this method is package-private because user code should always create
    *       {@link RepositoryUri} directly from a URI.
    * @implNote Can't call {@link #ofRepositoryUri(RepositoryUri, Path, Path)} because
    *       defaultStoragePath can't be computed on wrapped URIs.
    */
   static FsRepositoryUri ofRepositoryUri(RepositoryUri uri) {
      Check.notNull(uri, "uri");

      validateUriCompatibility(uri, "uri", isCompatibleRepositoryUriType(FsRepositoryUri.class));

      if (uri instanceof FsRepositoryUri) {
         return buildUri(() -> new FsRepositoryUri(mapQuery(fileUri(uri), (u, q) -> {
            q
                  .registerParameter(STORAGE_PATH.defaultValue(((FsRepositoryUri) uri).storagePath()))
                  .registerParameter(BASE_PATH.defaultValue(((FsRepositoryUri) uri).basePath()));
            return u;
         }), uri instanceof DocumentUri));
      } else {
         return buildUri(() -> new FsRepositoryUri(mapQuery(fileUri(uri), (u, q) -> {
            q
                  .registerParameter(STORAGE_PATH.defaultValue(defaultStoragePath(u,
                                                                                  uri instanceof DocumentUri)))
                  .registerParameter(BASE_PATH.defaultValue(defaultBasePath()));
            return u;
         }), uri instanceof DocumentUri));
      }
   }

   public FileUri fileUri() {
      return (FileUri) uri;
   }

   @Override
   public FsRepositoryUri normalize() {
      return buildUri(() -> new FsRepositoryUri(fileUri().normalize(), documentId != null));
   }

   @Override
   public FsRepositoryUri resolve(Uri uri) {
      return buildUri(() -> new FsRepositoryUri(fileUri().resolve(uri), documentId != null));
   }

   @Override
   public FsRepositoryUri resolve(URI uri) {
      return buildUri(() -> new FsRepositoryUri(fileUri().resolve(uri), documentId != null));
   }

   @Override
   public FsRepositoryUri relativize(Uri uri) {
      throw new UnsupportedOperationException();
   }

   @Override
   public FsRepositoryUri relativize(URI uri) {
      throw new UnsupportedOperationException();
   }

   @Override
   public int compareTo(Id o) {
      if (!(o instanceof FsRepositoryUri)) {
         return -1;
      } else if (o == this) {
         return 0;
      } else {
         return fileUri().compareTo(((FsRepositoryUri) o).fileUri());
      }
   }

   /**
    * Returns a configuration from this repository URI.
    */
   public FsDocumentConfigBuilder toConfig() {
      return new FsDocumentConfigBuilder()
            .storagePath(storagePath)
            .basePath(basePath)
            .<FsDocumentConfigBuilder, Boolean>optionalChain(query().parameter(CREATE_STORAGE_PATH_IF_MISSING,
                                                                               alwaysTrue()),
                                                             FsDocumentConfigBuilder::createStoragePathIfMissing)
            .<FsDocumentConfigBuilder, Boolean>optionalChain(query().parameter(
                  FAIL_FAST_IF_MISSING_STORAGE_PATH,
                  alwaysTrue()), FsDocumentConfigBuilder::failFastIfMissingStoragePath)
            .<FsDocumentConfigBuilder, Class>optionalChain(query().parameter(STORAGE_STRATEGY, alwaysTrue()),
                                                           FsDocumentConfigBuilder::storageStrategy)
            .<FsDocumentConfigBuilder, Boolean>optionalChain(query().parameter(DELETE_REPOSITORY_ON_CLOSE,
                                                                               alwaysTrue()),
                                                             FsDocumentConfigBuilder::deleteRepositoryOnClose);
   }

   public Path storagePath() {
      return storagePath;
   }

   public Path basePath() {
      return basePath;
   }

   @Override
   public boolean supportsUri(RepositoryUri uri) {
      Check.notNull(uri, "uri");

      return supportsRepositoryUri(uri, FsRepositoryUri.class, FsRepositoryUri::ofRepositoryUri)
            .map(fsUri -> {

               if (!fsUri.storagePath().equals(storagePath())) {
                  return false;
               }
               if (!fsUri.basePath().equals(basePath())) {
                  return false;
               }

               return mapQuery(fileUri(),
                               (u, q) -> q.includeParameters(query(fsUri.fileUri()),
                                                             isRegisteredParameter()));
            })
            .orElse(false);
   }

   @Override
   public URI exportURI(ExportUriOptions options) {
      Check.notNull(options, "options");

      var filter = exportUriFilter(options, isAlwaysTrue());
      var exportQuery = exportQuery(filter).noQueryIfEmpty(true);
      var exportUri = fileUri().component().query(__ -> exportQuery);

      return exportUri.toURI();
   }

   private ParameterizedQuery exportQuery(BiPredicate<ParameterizedQuery, Parameter> filter) {
      var exportQuery = query();

      if (EXPORT_URI_DEFAULT_STORAGE_PATH_AS_DEFAULT_VALUE) {
         var storagePathParameter = STORAGE_PATH.defaultValue(defaultStoragePath(fileUri(), documentUri));
         exportQuery =
               exportQuery.registerParameter(ValueParameter.ofValue(storagePathParameter, storagePath));
      }
      if (EXPORT_URI_DEFAULT_BASE_PATH_AS_DEFAULT_VALUE) {
         var basePathParameter = BASE_PATH.defaultValue(defaultBasePath());
         exportQuery = exportQuery.registerParameter(ValueParameter.ofValue(basePathParameter, basePath));
      }

      return exportQuery.exportQuery(filter);
   }

   @Override
   public RepositoryUri exportUri(ExportUriOptions options) {
      return RepositoryUri.ofRepositoryUri(exportURI(options));
   }

   /**
    * Creates a parameterized {@link FileUri} from specified {@link Uri}.
    * <p>
    * If specified URI is wrapped, it is unwrapped before creating parameterized URI.
    * Specified URI is normalized and checked for remaining traversals, after unwrapping.
    *
    * @param uri URI
    *
    * @return new parameterized URI, with all registered parameters
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   protected static FileUri fileUri(Uri uri) {
      Check.notNull(uri, "uri");
      var unwrappedUri = unwrapUri(uri, WRAPPED_URI_REPOSITORY_TYPE);

      return parameterizedQueryUri(unwrappedUri,
                                   FileUriRestrictions
                                         .ofDefault()
                                         .relativeFormat(false)
                                         .relativePath(true)
                                         .emptyPath(true)
                                         .query(false),
                                   FileUri::ofUri,
                                   allParameters());
   }

   /**
    * Creates a parameterized URI from specified configuration.
    *
    * @param config repository configuration
    * @param currentStoragePath storage path to use if {@link FsDocumentConfig#storagePath()} is empty
    * @param documentId optional document id
    *
    * @return new parameterized URI, with all registered value parameters from configuration
    */
   protected static FileUri fileUri(FsDocumentConfig config,
                                    Path currentStoragePath,
                                    DocumentPath documentId) {
      currentStoragePath = validate(nullable(currentStoragePath, Path::normalize),
                                    "currentStoragePath",
                                    PathRules.hasNoTraversal()).orThrow();

      var path = config.storagePath().orElse(currentStoragePath.normalize()).resolve(config.basePath());

      return FileUri.hierarchical(SimplePath.of((documentId != null
                                                 ? path.resolve(documentId.value())
                                                 : path).toString()),
                                  ParameterizedQuery
                                        .ofEmpty()
                                        .noQueryIfEmpty(true)
                                        .registerParameters(valueParameters(config)));
   }

   /**
    * Default storage path to complement generic {@link DocumentUri} or {@link RepositoryUri}.
    * <p>
    * For a document URI, default storage path is set to URI parent path, otherwise, it is set to URI path.
    * <p>
    * If URI has no path, or no parent path, or is only a root path, default storage path is set to
    * {@code /}.
    *
    * @param uri generic URI
    * @param documentUri whether URI is a document URI
    *
    * @return default storage path
    */
   protected static Path defaultStoragePath(Uri uri, boolean documentUri) {
      return documentUri ? uri
            .path(false)
            .map(Path::of)
            .flatMap(path -> nullable(path.getParent()))
            .orElseGet(PathUtils::rootPath) : uri.path(false).map(Path::of).orElseGet(PathUtils::rootPath);
   }

   /**
    * Default base path to complement generic {@link DocumentUri} or {@link RepositoryUri}.
    */
   protected static Path defaultBasePath() {
      return emptyPath();
   }

   /**
    * Checks URI syntax and parses current parameterized URI to separate the storage path and base path, from
    * the document path.
    *
    * @param documentUri whether the current parameterized URI is a document URI, identifying a document
    *       instead of a repository
    *
    * @return parsed ((storage path, base path), document path) pairs
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @implSpec The minimum set of parameters are checked here for consistency, only the parameters
    *       used in class internal state : storage path, base path. Other parameters
    *       could be checked later, e.g. in {@link #toConfig()}.
    *       <p>
    *       Default base path value is only applied if it starts actual URI path, otherwise,
    *       it is discarded because it's a "default", not an explicit value for actual URI, and it could apply
    *       to whatever URI is passed in.
    */
   protected Pair<Pair<Path, Path>, DocumentPath> parseUri(boolean documentUri) {
      var uriPath = fileUri().component().path().map(ComponentUri.Path::pathValue).orElseThrow();

      if (documentUri && isRoot(uriPath)) {
         throw new InvalidUriException(fileUri()).subMessage("Missing document id");
      }

      @SuppressWarnings("unchecked")
      var storagePathParameterOrDefault = query()
            .parameter(STORAGE_PATH,
                       sp -> ((Parameter<Path>) sp).defaultValue().map(uriPath::startsWith).orElse(false))
            .map(Path::normalize)
            .map(storagePath -> {
               Validate
                     .satisfies(storagePath, PathRules.isAbsolute().andValue(hasNoTraversal()))
                     .orThrowMessage(message -> new InvalidUriException(fileUri()).subMessage(
                           "'%s=%s' parameter must be absolute and must not contain traversal",
                           STORAGE_PATH.key(),
                           storagePath.toString()));

               return storagePath;
            });

      @SuppressWarnings("unchecked")
      var basePathParameter =
            query().parameter(BASE_PATH, bp -> ((Parameter<Path>) bp).defaultValue().map(bpDefault -> {
               return PathUtils.contains(uriPath, bpDefault);
            }).orElse(false)).orElseGet(FsRepositoryUri::defaultBasePath).normalize();

      Validate
            .satisfies(basePathParameter, isNotAbsolute().andValue(hasNoTraversal()))
            .orThrowMessage(message -> new InvalidUriException(fileUri()).subMessage(
                  "'%s=%s' parameter must not be absolute and must not contain traversal",
                  BASE_PATH.key(),
                  basePathParameter.toString()));

      if (!documentUri) {
         storagePathParameterOrDefault.ifPresent(storagePathParameter -> {
            if (isEmpty(basePathParameter) && !uriPath.equals(storagePathParameter)) {
               throw new InvalidUriException(fileUri()).subMessage(
                     "'%s=%s' parameter must match URI '%s' path",
                     STORAGE_PATH.key(),
                     storagePathParameter.toString(),
                     uriPath.toString());
            }
         });
         optionalPredicate(basePathParameter, not(PathUtils::isEmpty)).ifPresent(basePath -> {
            if (storagePathParameterOrDefault.isPresent()) {
               if (!uriPath.equals(storagePathParameterOrDefault.get().resolve(basePath))) {
                  throw new InvalidUriException(fileUri()).subMessage(
                        "'%s=%s' parameter must complete storage path to match URI '%s' path",
                        BASE_PATH.key(),
                        basePath.toString(),
                        uriPath.toString());
               }
            }
            if (!endsWith(uriPath, basePath)) {
               throw new InvalidUriException(fileUri()).subMessage(
                     "'%s=%s' parameter must complete storage path to match URI '%s' path",
                     BASE_PATH.key(),
                     basePath.toString(),
                     uriPath.toString());
            }
         });
      } else {
         storagePathParameterOrDefault.ifPresent(storagePathParameter -> {
            if (!startsWith(uriPath, storagePathParameter)) {
               throw new InvalidUriException(fileUri()).subMessage(
                     "'%s=%s' parameter must start URI '%s' path",
                     STORAGE_PATH.key(),
                     storagePathParameter.toString(),
                     uriPath.toString());
            }
            if (uriPath.equals(storagePathParameter)) {
               throw new InvalidUriException(fileUri()).subMessage(
                     "'%s=%s' parameter must not match URI '%s' path",
                     STORAGE_PATH.key(),
                     storagePathParameter.toString(),
                     uriPath.toString());
            }
         });
         optionalPredicate(basePathParameter, not(PathUtils::isEmpty)).ifPresent(basePath -> {
            if (storagePathParameterOrDefault.isPresent()) {
               if (!startsWith(uriPath, storagePathParameterOrDefault.get().resolve(basePath))) {
                  throw new InvalidUriException(fileUri()).subMessage(
                        "'%s=%s' parameter must complete storage path to start URI '%s' path",
                        BASE_PATH.key(),
                        basePath.toString(),
                        uriPath.toString());
               }
            }
            if (!contains(uriPath, basePath) || endsWith(uriPath, basePath)) {
               throw new InvalidUriException(fileUri()).subMessage(
                     "'%s=%s' parameter must complete storage path but not end URI '%s' path",
                     BASE_PATH.key(),
                     basePath.toString(),
                     uriPath.toString());
            }
         });
      }

      var splitStoragePath = splitStoragePath(documentUri,
                                              uriPath,
                                              storagePathParameterOrDefault.orElse(null),
                                              optionalPredicate(basePathParameter,
                                                                not(PathUtils::isEmpty)).orElse(null));

      return Pair.of(Pair.of(splitStoragePath.getLeft().getLeft(), splitStoragePath.getLeft().getRight()),
                     splitStoragePath.getRight());
   }

   /**
    * Computes ((storagePath, basePath), documentId) from URI path and base path parameter.
    *
    * @param documentUri whether URI is document URI
    * @param uriPath URI path
    * @param basePath optional base path, possibly empty, or {@code null}
    *
    * @return (new storage path, document id)
    */
   private static Pair<Pair<Path, Path>, DocumentPath> splitStoragePath(boolean documentUri,
                                                                        Path uriPath,
                                                                        Path storagePath,
                                                                        Path basePath) {
      DocumentPath documentId;

      if (!documentUri) {
         if (storagePath != null && basePath == null) {
            basePath = removeStart(uriPath, storagePath).orElseThrow();
         } else if (storagePath == null && basePath != null) {
            storagePath = removeEnd(uriPath, basePath).orElseThrow();
         } else if (storagePath == null && basePath == null) {
            storagePath = uriPath;
            basePath = emptyPath();
         }
         documentId = null;
      } else {
         if (storagePath != null && basePath == null) {
            documentId = DocumentPath.of(removeStart(uriPath, storagePath).orElseThrow());
            basePath = emptyPath();
         } else if (storagePath == null && basePath == null) {
            storagePath = uriPath.getParent();
            documentId = DocumentPath.of(uriPath.getFileName());
            basePath = emptyPath();
         } else {
            if (storagePath == null) {
               int basePathIndex = indexOf(uriPath, basePath).orElseThrow();
               storagePath = uriPath
                     .getRoot()
                     .resolve(basePathIndex > 0 ? uriPath.subpath(0, basePathIndex) : emptyPath());
               documentId = DocumentPath.of(uriPath.subpath(basePathIndex + basePath.getNameCount(),
                                                            uriPath.getNameCount()));
            } else {
               documentId =
                     DocumentPath.of(removeStart(uriPath, storagePath.resolve(basePath)).orElseThrow());
            }
         }
      }

      return Pair.of(Pair.of(storagePath, basePath), documentId);
   }

   /**
    * Supported parameter registry.
    */
   static class Parameters {
      static final Parameter<Path> STORAGE_PATH = Parameter.registered("storagePath", Path.class);
      static final Parameter<Path> BASE_PATH =
            Parameter.registered("basePath", Path.class).defaultValue(DEFAULT_BASE_PATH);
      static final Parameter<Boolean> CREATE_STORAGE_PATH_IF_MISSING = Parameter
            .registered("createStoragePathIfMissing", Boolean.class)
            .defaultValue(DEFAULT_CREATE_STORAGE_PATH_IF_MISSING);
      static final Parameter<Boolean> FAIL_FAST_IF_MISSING_STORAGE_PATH = Parameter
            .registered("failFastIfMissingStoragePath", Boolean.class)
            .defaultValue(DEFAULT_FAIL_FAST_IF_MISSING_STORAGE_PATH);
      static final Parameter<Class> STORAGE_STRATEGY =
            Parameter.registered("storageStrategy", Class.class).defaultValue(DEFAULT_STORAGE_STRATEGY);
      static final Parameter<Boolean> DELETE_REPOSITORY_ON_CLOSE = Parameter
            .registered("deleteRepositoryOnClose", Boolean.class)
            .defaultValue(DEFAULT_DELETE_REPOSITORY_ON_CLOSE);

      @SuppressWarnings("rawtypes")
      static List<Parameter> allParameters() {
         return list(STORAGE_PATH,
                     BASE_PATH,
                     CREATE_STORAGE_PATH_IF_MISSING,
                     FAIL_FAST_IF_MISSING_STORAGE_PATH,
                     STORAGE_STRATEGY,
                     DELETE_REPOSITORY_ON_CLOSE);
      }

      @SuppressWarnings("rawtypes")
      static List<ValueParameter> valueParameters(FsDocumentConfig config) {
         return list(ValueParameter.ofValue(STORAGE_PATH, config.storagePath().orElse(null)),
                     ValueParameter.ofValue(BASE_PATH, config.basePath()),
                     ValueParameter.ofValue(CREATE_STORAGE_PATH_IF_MISSING,
                                            config.createStoragePathIfMissing()),
                     ValueParameter.ofValue(FAIL_FAST_IF_MISSING_STORAGE_PATH,
                                            config.failFastIfMissingStoragePath()),
                     ValueParameter.ofValue(STORAGE_STRATEGY, config.storageStrategy()),
                     ValueParameter.ofValue(DELETE_REPOSITORY_ON_CLOSE, config.deleteRepositoryOnClose()));
      }

   }

}
