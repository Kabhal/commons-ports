/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.fs.storagestrategy;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.startsWith;

import java.nio.file.Path;
import java.util.Objects;
import java.util.StringJoiner;

import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.fs.FsDocumentConfig;

/**
 * Generates a storage path that directly and exactly matches document identifier path.
 */
public class DirectFsStorageStrategy extends AbstractFsStorageStrategy {

   private final FsDocumentConfig fsDocumentConfig;

   public DirectFsStorageStrategy(FsDocumentConfig fsDocumentConfig, Path storagePath) {
      super(storagePath);
      this.fsDocumentConfig = validate(fsDocumentConfig, "fsDocumentConfig", isNotNull()).orThrow();
   }

   public DirectFsStorageStrategy(FsDocumentConfig fsDocumentConfig) {
      this(fsDocumentConfig, fsDocumentConfig.storagePath().orElse(null));
   }

   @Override
   public Path storageFile(DocumentPath documentId) {
      validate(documentId, "documentId", isNotNull()).orThrow();

      Path storageFile = storagePath.resolve(fsDocumentConfig.basePath()).resolve(documentId.value());

      checkPathTraversal(storageFile);

      return storageFile;
   }

   @Override
   public DocumentPath documentId(Path storageFile) {
      validate(storageFile,
               "storageFile",
               isNotNull().andValue(startsWith(value(storagePath, "storagePath")))).orThrow();

      return DocumentPath.of(storagePath.resolve(fsDocumentConfig.basePath()).relativize(storageFile));
   }

   @Override
   public Path storageDirectory(Path documentDirectory) {
      validate(documentDirectory, "documentDirectory", isNotNull()).orThrow();

      Path storageFile = storagePath.resolve(fsDocumentConfig.basePath()).resolve(documentDirectory);

      checkPathTraversal(storageFile);

      return storageFile;
   }

   @Override
   public Path documentDirectory(Path storageDirectory) {
      validate(storageDirectory,
               "storageDirectory",
               isNotNull().andValue(startsWith(value(storagePath, "storagePath")))).orThrow();

      return storagePath.resolve(fsDocumentConfig.basePath()).relativize(storageDirectory);
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      DirectFsStorageStrategy that = (DirectFsStorageStrategy) o;
      return Objects.equals(storagePath, that.storagePath);
   }

   @Override
   public int hashCode() {
      return Objects.hash(storagePath);
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", DirectFsStorageStrategy.class.getSimpleName() + "[", "]")
            .add("storagePath=" + storagePath)
            .toString();
   }
}
