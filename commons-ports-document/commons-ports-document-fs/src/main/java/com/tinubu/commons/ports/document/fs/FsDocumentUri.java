/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.fs;

import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotAbsolute;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.ports.document.fs.FsRepositoryUri.Parameters.BASE_PATH;
import static com.tinubu.commons.ports.document.fs.FsRepositoryUri.Parameters.STORAGE_PATH;

import java.net.URI;
import java.nio.file.Path;

import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.domain.type.Id;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.invariant.rules.PathRules;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ddd2.uri.uris.FileUri;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.ExportUriOptions;

/**
 * Filesystem URI document URI. This URI must be used to identify a document, use
 * {@link FsRepositoryUri} to identify a repository.
 * <p>
 * You can use this class when a {@link FsRepositoryUri} is required, but your current URI is a document
 * URI. In this case it's important that the storage path and/or base path is set, or it will be set to,
 * respectively :
 * <ul>
 *    <li>{@code storagePath}: URI path parent, excluding base path</li>
 *    <li>{@code basePath}: {@code ""}</li>
 * </ul>
 * Storage path is read from {@code storagePath} URI query string parameter, or from specified
 * {@code defaultStoragePath} parameter. Bucket base path is read from {@code bucketBasePath} URI query
 * string parameter, or from specified {@code defaultBucketBasePath} parameter.
 * <p>
 * Supported URI format :
 * <ul>
 *    <li>{@code file:</storage-path>[/<base-path>]/<document-id>[?<parameters>]}</li>
 *    <li>Commons-ports wrapped URI : {@code document:fs:<fs-uri>}</li>
 * </ul>
 *
 * @see FsRepositoryUri
 * @see FsDocumentConfig
 */
public class FsDocumentUri extends FsRepositoryUri implements DocumentUri {

   protected FsDocumentUri(FileUri uri) {
      super(uri, true);
   }

   @Override
   @SuppressWarnings("unchecked")
   protected Fields<? extends FsDocumentUri> defineDomainFields() {
      return Fields
            .<FsDocumentUri>builder()
            .superFields((Fields<FsDocumentUri>) super.defineDomainFields())
            .field("documentId", v -> v.documentId, isNotNull())
            .build();
   }

   /**
    * Creates a document URI from {@link Uri}.
    *
    * @param uri document URI
    * @param defaultStoragePath default base path to use if {@code storagePath} query
    *       parameter not present
    * @param defaultBasePath default base path to use if {@code basePath} query
    *       parameter not present
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static FsDocumentUri ofDocumentUri(Uri uri, Path defaultStoragePath, Path defaultBasePath) {
      Check.notNull(uri, "uri");
      var normalizedDefaultStoragePath = Check.validate(nullable(defaultStoragePath, Path::normalize),
                                                        "defaultStoragePath",
                                                        PathRules.isAbsolute().andValue(hasNoTraversal()));
      var normalizedDefaultBasePath = Check.validate(nullable(defaultBasePath, Path::normalize),
                                                     "defaultBasePath",
                                                     isNotAbsolute().andValue(hasNoTraversal()));

      validateUriCompatibility(uri, "uri", isCompatibleUriType(FileUri.class));

      return buildUri(() -> new FsDocumentUri(mapQuery(fileUri(uri), (u, q) -> {
         q
               .registerParameter(STORAGE_PATH.defaultValue(normalizedDefaultStoragePath))
               .registerParameter(BASE_PATH.defaultValue(normalizedDefaultBasePath));
         return u;
      })));
   }

   /**
    * Creates a document URI from {@link Uri}.
    * <p>
    * Default storage path is set to URI parent path.
    * Default base path is set to {@code ""}.
    *
    * @param uri document URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static FsDocumentUri ofDocumentUri(Uri uri) {
      Check.notNull(uri, "uri");

      return ofDocumentUri(uri, defaultStoragePath(uri, true), defaultBasePath());
   }

   /**
    * Creates a document URI from {@link URI}.
    *
    * @param uri document URI
    * @param defaultStoragePath default base path to use if {@code storagePath} query
    *       parameter not present
    * @param defaultBasePath default base path to use if {@code basePath} query
    *       parameter not present
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static FsDocumentUri ofDocumentUri(URI uri, Path defaultStoragePath, Path defaultBasePath) {
      Check.notNull(uri, "uri");
      Check.notNull(defaultStoragePath, "defaultStoragePath");
      Check.notNull(defaultBasePath, "defaultBasePath");

      return ofDocumentUri(Uri.ofUri(uri), defaultStoragePath, defaultBasePath);
   }

   /**
    * Creates a document URI from {@link URI}.
    * <p>
    * Default storage path is set to URI parent path.
    * Default base path is set to {@code ""}.
    *
    * @param uri document URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static FsDocumentUri ofDocumentUri(URI uri) {
      Check.notNull(uri, "uri");

      return ofDocumentUri(Uri.ofUri(uri));
   }

   /**
    * Creates a document URI from URI string.
    * URI syntax is checked as part of this operation.
    *
    * @param uri document URI
    * @param defaultStoragePath default base path to use if {@code storagePath} query
    *       parameter not present
    * @param defaultBasePath default base path to use if {@code basePath} query
    *       parameter not present
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static FsDocumentUri ofDocumentUri(String uri, Path defaultStoragePath, Path defaultBasePath) {
      Check.notNull(uri, "uri");
      Check.notNull(defaultStoragePath, "defaultStoragePath");
      Check.notNull(defaultBasePath, "defaultBasePath");

      return ofDocumentUri(Uri.ofUri(uri), defaultStoragePath, defaultBasePath);
   }

   /**
    * Creates a document URI from URI string.
    * URI syntax is checked as part of this operation.
    * <p>
    * Default storage path is set to URI parent path.
    * Default base path is set to {@code ""}.
    *
    * @param uri document URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static FsDocumentUri ofDocumentUri(String uri) {
      Check.notNull(uri, "uri");

      return ofDocumentUri(Uri.ofUri(uri));
   }

   /**
    * Creates a document URI from configuration.
    *
    * @param config repository configuration
    * @param currentStoragePath storage path to use if {@link FsDocumentConfig#storagePath()} is empty
    * @param documentId document id
    *
    * @return new instance
    */
   public static FsDocumentUri ofConfig(FsDocumentConfig config,
                                        Path currentStoragePath,
                                        DocumentPath documentId) {
      Check.notNull(config, "config");
      Check.notNull(currentStoragePath, "currentStoragePath");

      return buildUri(() -> new FsDocumentUri(fileUri(config, currentStoragePath, documentId)));
   }

   /**
    * Creates a document URI from {@link DocumentUri}.
    *
    * @param uri document URI
    * @param defaultStoragePath default base path to use if {@code storagePath} query
    *       parameter not present
    * @param defaultBasePath default base path to use if {@code basePath} query
    *       parameter not present
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    * @implSpec this method is package-private because user code should always create
    *       {@link DocumentUri} directly from a URI.
    */
   static FsDocumentUri ofDocumentUri(DocumentUri uri, Path defaultStoragePath, Path defaultBasePath) {
      Check.notNull(uri, "uri");
      var normalizedDefaultStoragePath = Check.validate(nullable(defaultStoragePath, Path::normalize),
                                                        "defaultStoragePath",
                                                        PathRules.isAbsolute().andValue(hasNoTraversal()));
      var normalizedDefaultBasePath = Check.validate(nullable(defaultBasePath, Path::normalize),
                                                     "defaultBasePath",
                                                     isNotAbsolute().andValue(hasNoTraversal()));

      validateUriCompatibility(uri, "uri", isCompatibleDocumentUriType(FsDocumentUri.class));

      if (uri instanceof FsRepositoryUri) {
         return buildUri(() -> new FsDocumentUri(mapQuery(fileUri(uri), (u, q) -> {
            q
                  .registerParameter(STORAGE_PATH.defaultValue(((FsRepositoryUri) uri).storagePath()))
                  .registerParameter(BASE_PATH.defaultValue(((FsRepositoryUri) uri).basePath()));
            return u;
         })));
      } else {
         return buildUri(() -> new FsDocumentUri(mapQuery(fileUri(uri), (u, q) -> {
            q
                  .registerParameter(STORAGE_PATH.defaultValue(normalizedDefaultStoragePath))
                  .registerParameter(BASE_PATH.defaultValue(normalizedDefaultBasePath));
            return u;
         })));
      }
   }

   /**
    * Creates a document URI from {@link DocumentUri}.
    * <p>
    * Default storage path is set to URI parent path.
    * Default base path is set to {@code ""}.
    *
    * @param uri document URI
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    * @implSpec this method is package-private because user code should always create
    *       {@link DocumentUri} directly from a URI.
    */
   static FsDocumentUri ofDocumentUri(DocumentUri uri) {
      Check.notNull(uri, "uri");

      return ofDocumentUri(uri, defaultStoragePath(uri, true), defaultBasePath());
   }

   public DocumentPath documentId() {
      return documentId;
   }

   @Override
   public DocumentUri exportUri(ExportUriOptions options) {
      return DocumentUri.ofDocumentUri(exportURI(options));
   }

   @Override
   public FsDocumentUri normalize() {
      return buildUri(() -> new FsDocumentUri(fileUri().normalize()));
   }

   @Override
   public FsDocumentUri resolve(Uri uri) {
      return buildUri(() -> new FsDocumentUri(fileUri().resolve(uri)));
   }

   @Override
   public FsDocumentUri resolve(URI uri) {
      return buildUri(() -> new FsDocumentUri(fileUri().resolve(uri)));
   }

   @Override
   public FsDocumentUri relativize(Uri uri) {
      throw new UnsupportedOperationException();
   }

   @Override
   public FsDocumentUri relativize(URI uri) {
      throw new UnsupportedOperationException();
   }

   @Override
   public int compareTo(Id o) {
      if (!(o instanceof FsDocumentUri)) {
         return -1;
      } else {
         return super.compareTo(o);
      }
   }

}
