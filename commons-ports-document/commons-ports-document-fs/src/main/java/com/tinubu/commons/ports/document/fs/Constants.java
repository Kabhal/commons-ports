/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.fs;

import static com.tinubu.commons.lang.util.PathUtils.emptyPath;

import java.nio.file.Path;

import com.tinubu.commons.ports.document.fs.storagestrategy.DirectFsStorageStrategy;
import com.tinubu.commons.ports.document.fs.storagestrategy.FsStorageStrategy;

public final class Constants {

   private Constants() { }

   public static final Path DEFAULT_BASE_PATH = emptyPath();
   public static final boolean DEFAULT_CREATE_STORAGE_PATH_IF_MISSING = true;
   public static final boolean DEFAULT_FAIL_FAST_IF_MISSING_STORAGE_PATH = true;
   public static final Class<? extends FsStorageStrategy> DEFAULT_STORAGE_STRATEGY =
         DirectFsStorageStrategy.class;
   public static final boolean DEFAULT_DELETE_REPOSITORY_ON_CLOSE = false;

   public static final int DEFAULT_HEX_TREE_DEPTH = 2;
   public static final int MAX_TREE_DEPTH = 16;

}
