/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.fs;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotAbsolute;
import static com.tinubu.commons.lang.util.CheckedSupplier.checkedSupplier;
import static com.tinubu.commons.lang.util.CollectionUtils.collectionConcat;
import static com.tinubu.commons.lang.util.CollectionUtils.collectionSubtraction;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.OptionalUtils.instanceOf;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.PathUtils.emptyPath;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.DOCUMENT_URI;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.METADATA_CONTENT_SIZE;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.METADATA_CREATION_DATE;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.METADATA_LAST_UPDATE_DATE;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.REPOSITORY_URI;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.SUBPATH;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.allFeatureCapabilities;
import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.CREATE_NEW;
import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING;
import static java.nio.file.StandardOpenOption.WRITE;
import static java.util.function.UnaryOperator.identity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.StringJoiner;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.AbstractFileFilter;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.lang3.time.StopWatch;

import com.tinubu.commons.ddd2.domain.event.RegistrableDomainEventService;
import com.tinubu.commons.ddd2.domain.event.SynchronousDomainEventService;
import com.tinubu.commons.ddd2.domain.repository.Repository;
import com.tinubu.commons.ddd2.domain.specification.Specification;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.lang.io.ReferencedFileOutputStream;
import com.tinubu.commons.lang.validation.CheckReturnValue;
import com.tinubu.commons.ports.document.domain.AbstractDocumentRepository;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentContent;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentEntry.DocumentEntryBuilder;
import com.tinubu.commons.ports.document.domain.DocumentEntrySpecification;
import com.tinubu.commons.ports.document.domain.DocumentMetadata;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent.InputStreamDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata;
import com.tinubu.commons.ports.document.domain.OutputStreamDocumentContent;
import com.tinubu.commons.ports.document.domain.OutputStreamDocumentContent.OutputStreamDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.ReferencedDocument;
import com.tinubu.commons.ports.document.domain.capability.RepositoryCapability;
import com.tinubu.commons.ports.document.domain.capability.UnsupportedCapabilityException;
import com.tinubu.commons.ports.document.domain.uri.DocumentRepositoryUriAdapter;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.ExportUriOptions;
import com.tinubu.commons.ports.document.domain.uri.RepositoryUri;
import com.tinubu.commons.ports.document.fs.FsDocumentConfig.FsDocumentConfigBuilder;
import com.tinubu.commons.ports.document.fs.storagestrategy.DirectFsStorageStrategy;
import com.tinubu.commons.ports.document.fs.storagestrategy.FsStorageStrategy;
import com.tinubu.commons.ports.document.fs.storagestrategy.FsStorageStrategyFactory;
import com.tinubu.commons.ports.document.fs.storagestrategy.HexTreeFsStorageStrategy;
import com.tinubu.commons.ports.document.transformer.DocumentTransformerUriAdapter;

/**
 * FS {@link DocumentRepository} adapter implementation.
 * <p>
 * If configured storage path is missing, the storage path will be created only at write time, or the write
 * operation will fail if repository configuration does not instruct to do so.
 * <p>
 * Limitations :
 * <ul>
 *    <li>the following Metadata are not persisted: documentPath, contentEncoding, contentType, attributes</li>
 *    <li>depending on OS/FS driver: Metadata creationDate, lastUpdateDate best precision is generally second</li>
 *    <li>depending on OS/FS driver: creationDate can be reset when overriding an existing document</li>
 * </ul>
 *
 * @implSpec Immutable class implementation
 */
// FIXME partial written files : create tmp and rename it at the end of the transfer ? (config tmp prefix + disable feature completely)
// FIXME create storage path permissions
// FIXME Create document file permissions
public class FsDocumentRepository extends AbstractDocumentRepository<FsDocumentRepository> {

   private static final String URI_SCHEME = "file";

   private final FsDocumentConfig fsDocumentConfig;
   private final FsStorageStrategy fsStorageStrategy;
   private final DocumentRepositoryUriAdapter uriAdapter;
   private final boolean createStoragePathIfMissing;
   private final boolean deleteRepositoryOnClose;
   private boolean closed = false;

   public FsDocumentRepository(FsDocumentConfig fsDocumentConfig,
                               RegistrableDomainEventService eventService) {
      super(eventService);

      this.fsDocumentConfig = validate(fsDocumentConfig, "fsDocumentConfig", isNotNull()).orThrow();

      this.fsStorageStrategy = FsStorageStrategyFactory.fsStorageStrategy(fsDocumentConfig,
                                                                          fsDocumentConfig
                                                                                .storagePath()
                                                                                .orElseGet(checkedSupplier(
                                                                                      FsDocumentRepository::createTemporaryStoragePath)));

      this.uriAdapter = new FsUriAdapter();
      this.createStoragePathIfMissing = fsDocumentConfig.createStoragePathIfMissing();
      this.deleteRepositoryOnClose = fsDocumentConfig.deleteRepositoryOnClose();

      if (fsDocumentConfig.failFastIfMissingStoragePath()) {
         mkdirStoragePath();
      }
   }

   public FsDocumentRepository(FsDocumentConfig fsDocumentConfig) {
      this(fsDocumentConfig, new SynchronousDomainEventService());
   }

   /**
    * Creates an instance from real filesystem directory. Storage path must exist.
    *
    * @param storagePath filesystem storage path
    *
    * @return new {@link FsDocumentRepository} instance
    *
    * @throws NoSuchFileException if specified storage path is not an existing directory
    */
   public static FsDocumentRepository ofFilesystem(Path storagePath) throws NoSuchFileException {
      return ofFilesystem(storagePath, false);
   }

   /**
    * Creates an instance from real filesystem directory. Storage path must exist.
    *
    * @param storagePath filesystem storage path
    * @param deleteRepositoryOnClose whether to recursively delete repository on close
    *
    * @return new {@link FsDocumentRepository} instance
    *
    * @throws NoSuchFileException if specified storage path is not an existing directory
    */
   public static FsDocumentRepository ofFilesystem(Path storagePath, boolean deleteRepositoryOnClose)
         throws NoSuchFileException {
      validate(storagePath, "storagePath", isNotNull()).orThrow();

      File directory = storagePath.toFile();
      if (directory.exists() && directory.isDirectory()) {
         return new FsDocumentRepository(new FsDocumentConfigBuilder()
                                               .storageStrategy(DirectFsStorageStrategy.class)
                                               .storagePath(storagePath)
                                               .failFastIfMissingStoragePath(true)
                                               .deleteRepositoryOnClose(deleteRepositoryOnClose)
                                               .build());
      } else {
         throw new NoSuchFileException(directory.getAbsolutePath(),
                                       null,
                                       String.format("'%s' is not an existing directory",
                                                     directory.getAbsolutePath()));
      }
   }

   /**
    * Creates a new {@link FsDocumentRepository} referencing a temporary directory and with
    * {@link FsDocumentConfigBuilder#deleteRepositoryOnClose(boolean)} set to {@code true}.
    *
    * @param builderConfigurer extra builder configurer to finalize configuration or override URI
    *       parameters
    *
    * @return new {@link FsDocumentRepository} instance
    */
   public static FsDocumentRepository ofTemporaryDirectory(UnaryOperator<FsDocumentConfigBuilder> builderConfigurer) {
      Check.notNull(builderConfigurer, "builderConfigurer");

      return new FsDocumentRepository(new FsDocumentConfigBuilder()
                                            .deleteRepositoryOnClose(true)
                                            .chain(builderConfigurer)
                                            .build());
   }

   /**
    * Creates a new {@link FsDocumentRepository} referencing a temporary directory and with
    * {@link FsDocumentConfigBuilder#deleteRepositoryOnClose(boolean)} set to {@code true}.
    *
    * @return new {@link FsDocumentRepository} instance
    */
   public static FsDocumentRepository ofTemporaryDirectory() {
      return ofTemporaryDirectory(identity());
   }

   /**
    * Creates a new temporary storage path.
    *
    * @return new temporary storage path
    */
   private static Path createTemporaryStoragePath() throws IOException {
      return Files.createTempDirectory("document-fs");
   }

   /**
    * Creates a new document repository from URI with parameters.
    * If specified URI is a document URI, and no {@code basePath} parameter is set, storage path is
    * arbitrary set to document parent path.
    *
    * @param uri URI
    * @param builderConfigurer extra builder configurer to finalize configuration or override URI
    *       parameters
    *
    * @return new document repository
    *
    * @throws InvalidUriException if specified URI is compatible with this repository but invalid
    * @throws IncompatibleUriException if specified URI is not compatible with this repository
    */
   public static FsDocumentRepository ofUri(RepositoryUri uri,
                                            UnaryOperator<FsDocumentConfigBuilder> builderConfigurer) {
      Check.notNull(uri, "uri");
      Check.notNull(builderConfigurer, "builderConfigurer");

      return new FsDocumentRepository(FsRepositoryUri
                                            .ofRepositoryUri(uri)
                                            .toConfig()
                                            .chain(builderConfigurer)
                                            .build());
   }

   /**
    * Creates a new document repository from URI with parameters.
    * If specified URI is a document URI, and no {@code basePath} parameter is set, storage path is
    * arbitrary set to document parent path.
    *
    * @param uri URI
    *
    * @return new document repository
    *
    * @throws InvalidUriException if specified URI is compatible with this repository but invalid
    * @throws IncompatibleUriException if specified URI is not compatible with this repository
    */
   public static FsDocumentRepository ofUri(RepositoryUri uri) {
      return ofUri(uri, identity());
   }

   /**
    * Creates a lazily referenced document, in a new document repository, from URI with parameters. URI must
    * be compatible with this repository and identify a document.
    * If specified URI is a document URI, and no {@code basePath} parameter is set, storage path is
    * arbitrary set to document parent path.
    *
    * @param documentUri document URI
    * @param builderConfigurer extra builder configurer to finalize configuration or override URI
    *       parameters
    *
    * @return referenced document in new document repository
    *
    * @throws InvalidUriException if specified URI is compatible with this repository but invalid
    * @throws IncompatibleUriException if specified URI is not compatible with this repository
    */
   public static ReferencedDocument<FsDocumentRepository> referencedDocument(DocumentUri documentUri,
                                                                             UnaryOperator<FsDocumentConfigBuilder> builderConfigurer) {
      Check.notNull(documentUri, "documentUri");
      Check.notNull(builderConfigurer, "builderConfigurer");

      var fsUri = FsDocumentUri.ofDocumentUri(documentUri);
      return ReferencedDocument.lazyLoad(new FsDocumentRepository(fsUri
                                                                        .toConfig()
                                                                        .chain(builderConfigurer)
                                                                        .build()), fsUri.documentId());
   }

   /**
    * Creates a lazily referenced document, in a new document repository, from URI with parameters. URI must
    * be compatible with this repository and identify a document.
    * If specified URI is a document URI, and no {@code basePath} parameter is set, storage path is
    * arbitrary set to document parent path.
    *
    * @param documentUri document URI
    *
    * @return referenced document in new document repository
    *
    * @throws InvalidUriException if specified URI is compatible with this repository but invalid
    * @throws IncompatibleUriException if specified URI is not compatible with this repository
    */
   public static ReferencedDocument<FsDocumentRepository> referencedDocument(DocumentUri documentUri) {
      return referencedDocument(documentUri, identity());
   }

   /**
    * Returns physical file-system base storage path for this repository.
    * Storage path is always absolute.
    *
    * @return physical file-system base storage path for this repository
    */
   public Path storagePath() {
      return fsStorageStrategy.storagePath();
   }

   /**
    * Base path relative to storage path. Can be {@code ""}.
    *
    * @return optional base path, or {@code ""}
    */
   public Path basePath() {
      return fsDocumentConfig.basePath();
   }

   /**
    * Returns physical storage strategy.
    *
    * @return physical storage strategy
    */
   public FsStorageStrategy storageStrategy() {
      return fsStorageStrategy;
   }

   /**
    * Whether storage path is temporary. Storage path is only considered temporary if no storage path were
    * provided in configuration and a default temporary storage path were generated.
    *
    * @return {@code true} if storage path is temporary
    */
   public boolean temporaryStoragePath() {
      return fsDocumentConfig.storagePath().isEmpty();
   }

   @Override
   public HashSet<RepositoryCapability> capabilities() {
      HashSet<RepositoryCapability> capabilities = collectionConcat(HashSet::new,
                                                                    allFeatureCapabilities(),
                                                                    list(METADATA_CREATION_DATE,
                                                                         METADATA_LAST_UPDATE_DATE,
                                                                         METADATA_CONTENT_SIZE));

      if (fsStorageStrategy instanceof DirectFsStorageStrategy) {
         return capabilities;
      } else if (fsStorageStrategy instanceof HexTreeFsStorageStrategy) {
         return collectionSubtraction(HashSet::new, capabilities, list(DOCUMENT_URI));
      } else {
         return collectionSubtraction(HashSet::new, capabilities, list(REPOSITORY_URI, DOCUMENT_URI));
      }
   }

   @Override
   public boolean sameRepositoryAs(Repository<Document, DocumentPath> documentRepository) {
      validate(documentRepository, "documentRepository", isNotNull()).orThrow();

      return documentRepository instanceof FsDocumentRepository
             && Objects.equals(((FsDocumentRepository) documentRepository).fsStorageStrategy,
                               fsStorageStrategy);
   }

   @Override
   public FsDocumentRepository subPath(Path subPath, boolean shareContext) {
      validate(subPath, "subPath", isNotAbsolute().andValue(hasNoTraversal())).orThrow();

      if (!fsDocumentConfig.storageStrategy().equals(DirectFsStorageStrategy.class)) {
         throw new UnsupportedCapabilityException(SUBPATH);
      }

      return new FsDocumentRepository(FsDocumentConfigBuilder
                                            .from(fsDocumentConfig)
                                            .storagePath(fsStorageStrategy.storagePath())
                                            .basePath(fsDocumentConfig.basePath().resolve(subPath))
                                            .build(), eventService);
   }

   @Override
   @CheckReturnValue
   public Optional<Document> openDocument(DocumentPath documentId,
                                          boolean overwrite,
                                          boolean append,
                                          OpenDocumentMetadata metadata) {
      validate(documentId, "documentId", isNotNull())
            .and(validate(metadata, "metadata", isNotNull()))
            .orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEvent(() -> {
         File documentFile = storageFile(documentId);

         if (!overwrite && !append && documentFile.exists()) {
            return optional();
         }

         mkdirStorageFile(documentFile);

         try {
            OutputStream documentOutputStream = new ReferencedFileOutputStream(Files.newOutputStream(
                  documentFile.toPath(),
                  WRITE,
                  overwrite || append ? CREATE : StandardOpenOption.CREATE_NEW,
                  append ? StandardOpenOption.APPEND : StandardOpenOption.TRUNCATE_EXISTING), documentFile);

            return optional(new DocumentBuilder()
                                  .documentId(documentId)
                                  .content(new OutputStreamDocumentContentBuilder()
                                                 .content(documentOutputStream,
                                                          metadata.contentEncoding().orElse(null))
                                                 .build())
                                  .chain(metadata.chainDocumentBuilder())
                                  .build());
         } catch (FileAlreadyExistsException e) {
            return optional();
         } catch (IOException e) {
            throw new DocumentAccessException(e);
         }
      }, d -> documentSaved(d.documentEntry(), watch));
   }

   @Override
   public Optional<Document> findDocumentById(DocumentPath documentId) {
      validate(documentId, "documentId", isNotNull()).orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEvent(() -> {
         File documentFile = storageFile(documentId);

         return fsDocumentEntry(documentFile,
                                documentId).flatMap(entry -> fsDocumentContent(documentFile).map(content -> new DocumentBuilder()
               .<DocumentBuilder>reconstitute()
               .documentEntry(entry)
               .content(content)
               .build()));
      }, d -> documentAccessed(d.documentEntry(), watch));
   }

   @Override
   public Stream<Document> findDocumentsBySpecification(Path basePath,
                                                        Specification<DocumentEntry> specification) {
      return autoCloseDocumentsOnStreamClose(findDocumentEntriesBySpecification(basePath,
                                                                                specification).flatMap(entry -> stream(
            fsDocumentContent(storageFile(entry.documentId())).map(content -> new DocumentBuilder()
                  .<DocumentBuilder>reconstitute()
                  .documentEntry(entry)
                  .content(content)
                  .build()))));
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryById(DocumentPath documentId) {
      validate(documentId, "documentId", isNotNull()).orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEntryEvent(() -> {
         File documentFile = storageFile(documentId);

         return fsDocumentEntry(documentFile, documentId);
      }, d -> documentAccessed(d, watch));
   }

   @Override
   public Stream<DocumentEntry> findDocumentEntriesBySpecification(Path basePath,
                                                                   Specification<DocumentEntry> specification) {
      validate(basePath, "basePath", isNull().orValue(isNotAbsolute().andValue(hasNoTraversal())))
            .and(validate(specification, "specification", isNotNull()))
            .orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEntryStreamEvent(() -> {
         Stream<DocumentEntry> documentEntries;

         if (fsStorageStrategy instanceof DirectFsStorageStrategy) {
            documentEntries = listDocumentEntries(storageDirectory(basePath), directoryFilter(specification));
         } else {
            documentEntries = listAllDocumentEntries();
         }

         return documentEntries.filter(specification);
      }, d -> documentAccessed(d, watch));
   }

   @Override
   @CheckReturnValue
   public Optional<DocumentEntry> saveDocument(Document document, boolean overwrite) {
      validate(document, "document", isNotNull()).orThrow();

      if (document.content() instanceof OutputStreamDocumentContent) {
         throw new IllegalArgumentException(
               "Unsupported operation for this document content implementation : " + document
                     .content()
                     .getClass()
                     .getName());
      }

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEntryEvent(() -> rawSaveDocument(document, overwrite),
                                      d -> documentSaved(d, watch));
   }

   @Override
   @CheckReturnValue
   public Optional<Document> saveAndReturnDocument(Document document, boolean overwrite) {
      validate(document, "document", isNotNull()).orThrow();

      if (document.content() instanceof OutputStreamDocumentContent) {
         throw new IllegalArgumentException(
               "Unsupported operation for this document content implementation : " + document
                     .content()
                     .getClass()
                     .getName());
      }

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEvent(() -> {
         File documentFile = storageFile(document.documentId());

         return rawSaveDocument(document, overwrite).flatMap(entry -> fsDocumentContent(documentFile).map(
               content -> new DocumentBuilder()
                     .<DocumentBuilder>reconstitute()
                     .documentEntry(entry)
                     .content(content)
                     .build()));
      }, d -> documentSaved(d.documentEntry(), watch));
   }

   private Optional<DocumentEntry> rawSaveDocument(Document document, boolean overwrite) {
      File documentFile = storageFile(document.documentId());
      Optional<DocumentEntry> savedDocument = optional();

      if (overwrite || !documentFile.exists()) {

         mkdirStorageFile(documentFile);

         try (InputStream documentInputStream = document.content().inputStreamContent();
              OutputStream documentOutputStream = Files.newOutputStream(documentFile.toPath(),
                                                                        WRITE,
                                                                        overwrite ? CREATE : CREATE_NEW,
                                                                        TRUNCATE_EXISTING)) {
            IOUtils.copy(documentInputStream, documentOutputStream);

            return fsDocumentEntry(documentFile, document.documentId());
         } catch (IOException e) {
            throw new DocumentAccessException(e);
         }
      }

      return savedDocument;
   }

   @Override
   @CheckReturnValue
   public Optional<DocumentEntry> deleteDocumentById(DocumentPath documentId) {
      validate(documentId, "documentId", isNotNull()).orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEntryEvent(() -> {
         File documentFile = storageFile(documentId);
         Optional<DocumentEntry> documentEntry = fsDocumentEntry(documentFile, documentId);

         boolean removed = documentEntry.isPresent() && documentFile.delete();

         return documentEntry.filter(__ -> removed);
      }, d -> documentDeleted(d, watch));
   }

   @Override
   @CheckReturnValue
   public List<DocumentEntry> deleteDocumentsBySpecification(Path basePath,
                                                             Specification<DocumentEntry> specification) {
      validate(basePath, "basePath", isNull().orValue(isNotAbsolute().andValue(hasNoTraversal())))
            .and(validate(specification, "specification", isNotNull()))
            .orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEntriesEvent(() -> {
         Stream<DocumentEntry> documentEntries;

         if (fsStorageStrategy instanceof DirectFsStorageStrategy) {
            documentEntries = listDocumentEntries(storageDirectory(basePath), directoryFilter(specification));
         } else {
            documentEntries = listAllDocumentEntries();
         }

         return list(documentEntries.filter(specification).filter(documentEntry -> {
            File documentFile = storageFile(documentEntry.documentId());
            return documentFile.delete();
         }));
      }, d -> documentDeleted(d, watch));
   }

   @Override
   public void close() {
      if (!closed) {
         closed = true;

         deleteRepositoryOnClose();
      }
   }

   @Override
   public ReferencedDocument<? extends DocumentRepository> referencedDocumentId(DocumentUri documentUri) {
      return uriAdapter.referencedDocumentId(documentUri);
   }

   @Override
   public boolean supportsUri(RepositoryUri uri) {
      return uriAdapter.supportsUri(uri);
   }

   @Override
   public RepositoryUri toUri(ExportUriOptions options) {
      return uriAdapter.toUri(options);
   }

   @Override
   public DocumentUri toUri(DocumentPath documentId, ExportUriOptions options) {
      return uriAdapter.toUri(documentId, options);
   }

   @Override
   public Optional<Document> findDocumentByUri(DocumentUri documentUri) {
      return uriAdapter.findDocumentByUri(documentUri);
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryByUri(DocumentUri documentUri) {
      return uriAdapter.findDocumentEntryByUri(documentUri);
   }

   /**
    * Real FS URI adapter to be used by {@link DocumentTransformerUriAdapter}.
    */
   public class FsUriAdapter implements DocumentRepositoryUriAdapter {

      @Override
      public ReferencedDocument<? extends DocumentRepository> referencedDocumentId(DocumentUri documentUri) {
         Check.notNull(documentUri, "documentUri");
         var fsDocumentUri = validateFsDocumentUri(documentUri);

         return ReferencedDocument.lazyLoad(FsDocumentRepository.this, fsDocumentUri.documentId());
      }

      @Override
      public boolean supportsUri(RepositoryUri uri) {
         Check.notNull(uri, "uri");

         return supportsFsUri(uri);
      }

      @Override
      public RepositoryUri toUri(ExportUriOptions options) {
         Check.notNull(options, "options");

         return FsRepositoryUri.ofConfig(fsDocumentConfig, storagePath()).exportUri(options);
      }

      @Override
      public DocumentUri toUri(DocumentPath documentId, ExportUriOptions options) {
         Check.notNull(documentId, "documentId");
         Check.notNull(options, "options");

         return FsDocumentUri.ofConfig(fsDocumentConfig, storagePath(), documentId).exportUri(options);
      }

      @Override
      public Optional<Document> findDocumentByUri(DocumentUri documentUri) {
         Check.notNull(documentUri, "documentUri");
         var fsDocumentUri = validateFsDocumentUri(documentUri);

         return findDocumentById(fsDocumentUri.documentId());
      }

      @Override
      public Optional<DocumentEntry> findDocumentEntryByUri(DocumentUri documentUri) {
         Check.notNull(documentUri, "documentUri");
         var fsDocumentUri = validateFsDocumentUri(documentUri);

         return findDocumentEntryById(fsDocumentUri.documentId());
      }

      private boolean supportsFsUri(RepositoryUri repositoryUri) {
         try {
            return (FsRepositoryUri
                          .ofConfig(fsDocumentConfig, storagePath())
                          .supportsUri(instanceOf(repositoryUri,
                                                  FsRepositoryUri.class).orElseGet(() -> FsRepositoryUri.ofRepositoryUri(
                                repositoryUri,
                                storagePath(),
                                basePath()))));
         } catch (IncompatibleUriException __) {
            return false;
         }
      }

      private FsDocumentUri validateFsDocumentUri(DocumentUri documentUri) {
         try {
            var fsUri =
                  instanceOf(documentUri, FsDocumentUri.class).orElseGet(() -> FsDocumentUri.ofDocumentUri(
                        documentUri,
                        storagePath(),
                        basePath()));

            if (!supportsFsUri(fsUri)) {
               throw new InvalidUriException(documentUri).message("'documentUri=%s' must be supported",
                                                                  documentUri.toString());
            }

            return fsUri;
         } catch (IncompatibleUriException e) {
            throw new InvalidUriException(documentUri).message("'documentUri=%s' must be supported: %s",
                                                               documentUri.toString(),
                                                               e.getMessage());
         }
      }
   }

   private Predicate<Path> directoryFilter(Specification<DocumentEntry> specification) {
      if (specification instanceof DocumentEntrySpecification) {
         DocumentEntrySpecification documentEntrySpecification = (DocumentEntrySpecification) specification;
         return documentEntrySpecification::satisfiedBySubPath;
      } else {
         return __ -> true;
      }
   }

   /**
    * List recursively all document entries.
    */
   private Stream<DocumentEntry> listAllDocumentEntries() {
      return listDocumentEntries(storageDirectory(emptyPath()), __ -> true);
   }

   /**
    * List document entries from specified base directory. Base directory represents a logical relative path.
    *
    * @param baseDirectory backend-space directory to search from
    * @param directoryFilter user-space directory filter. Use {@code __ -> false} to disable recursive
    *       search
    *
    * @apiNote Specified base directory is directly resolved against configured storage path without
    *       taking {@link FsStorageStrategy strategies} into account. This operation will ignore base
    *       directory for {@link FsStorageStrategy strategies} other than {@link DirectFsStorageStrategy}.
    */
   private Stream<DocumentEntry> listDocumentEntries(Path baseDirectory, Predicate<Path> directoryFilter) {
      notNull(baseDirectory, "baseDirectory");
      notNull(directoryFilter, "directoryFilter");

      if (!Files.isDirectory(baseDirectory)) {
         return stream();
      }

      return StreamSupport
            .stream(Spliterators.spliteratorUnknownSize(FileUtils.iterateFiles(baseDirectory.toFile(),
                                                                               TrueFileFilter.INSTANCE,
                                                                               filterDirectory(directoryFilter)),
                                                        Spliterator.ORDERED), false)
            .filter(this::isRegularFile)
            .flatMap(storageFile -> stream(this.fsDocumentEntry(storageFile, null)));
   }

   private IOFileFilter filterDirectory(Predicate<Path> directoryFilter) {
      return new AbstractFileFilter() {
         @Override
         public boolean accept(File file) {
            return directoryFilter.test(documentDirectory(file));
         }
      };
   }

   private boolean isRegularFile(File file) {
      try {
         return Files.readAttributes(file.toPath(), BasicFileAttributes.class).isRegularFile();
      } catch (IOException e) {
         throw new DocumentAccessException(e);
      }
   }

   /**
    * Low-level document entry generation from storage file.
    *
    * @param storageFile storage file
    * @param documentId optional document identifier, when known, to optimize reverse document id
    *       mapping
    *
    * @return document entry or {@link Optional#empty} if file not found
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   private Optional<DocumentEntry> fsDocumentEntry(File storageFile, DocumentPath documentId) {
      return fsDocumentMetadata(storageFile).map(metadata -> new DocumentEntryBuilder()
            .<DocumentEntryBuilder>reconstitute()
            .documentId(documentId != null ? documentId : documentId(storageFile))
            .metadata(metadata)
            .build());
   }

   /**
    * Low-level document content generation from storage file.
    *
    * @param storageFile storage file
    *
    * @return document content or {@link Optional#empty} if file not found
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   private Optional<DocumentContent> fsDocumentContent(File storageFile) {
      try {
         return optional(new InputStreamDocumentContentBuilder()
                               .<InputStreamDocumentContentBuilder>reconstitute()
                               .content(new FileInputStream(storageFile), storageFile.length())
                               .build());
      } catch (FileNotFoundException e) {
         return optional();
      } catch (Exception e) {
         throw new DocumentAccessException(e.getMessage(), e);
      }
   }

   /**
    * Low-level document metadata generation from storage file.
    *
    * @param storageFile storage file
    *
    * @return document metadata or {@link Optional#empty} if file not found
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   private Optional<DocumentMetadata> fsDocumentMetadata(File storageFile) {
      try {
         BasicFileAttributes basicFileAttributes =
               Files.readAttributes(storageFile.toPath(), BasicFileAttributes.class);
         Instant creationDate = basicFileAttributes.creationTime().toInstant();
         Instant lastUpdateDate = basicFileAttributes.lastModifiedTime().toInstant();

         return optional(new DocumentMetadataBuilder()
                               .<DocumentMetadataBuilder>reconstitute()
                               .documentPath(documentId(storageFile).value())
                               .contentSize(storageFile.length())
                               .creationDate(creationDate)
                               .lastUpdateDate(lastUpdateDate)
                               .build());
      } catch (NoSuchFileException e) {
         return optional();
      } catch (Exception e) {
         throw new DocumentAccessException(e.getMessage(), e);
      }
   }

   /**
    * Creates parent directories of specified storageFile. Does nothing if storageFile has no parent
    * directory.
    *
    * @param storageFile storageFile
    */
   private void mkdirStorageFile(File storageFile) {
      notNull(storageFile, "storageFile");

      mkdirStoragePath();

      File directory = storageFile.getParentFile();
      if (directory != null) {
         if (!(directory.mkdirs() || directory.isDirectory())) {
            throw new DocumentAccessException(String.format("Can't create '%s' filesystem storage directory",
                                                            directory));
         }
      }
   }

   /**
    * Creates storage path.
    */
   private void mkdirStoragePath() {
      File storagePath = fsStorageStrategy.storagePath().toFile();

      if (!storagePath.exists() && !createStoragePathIfMissing) {
         throw new DocumentAccessException(String.format("Missing '%s' filesystem storage path",
                                                         storagePath));
      } else {
         if (!(storagePath.mkdirs() || storagePath.isDirectory())) {
            throw new DocumentAccessException(String.format("Can't create '%s' filesystem storage path",
                                                            storagePath));
         }

      }
   }

   /**
    * Generate real local storage path for specified document.
    *
    * @param documentId document id
    *
    * @return real document local storage path
    *
    * @apiNote The returned file is protected against path traversal attacks.
    */
   private File storageFile(DocumentPath documentId) {
      return fsStorageStrategy.storageFile(documentId).toFile();
   }

   private Path storageDirectory(Path documentDirectory) {
      return fsStorageStrategy.storageDirectory(documentDirectory);
   }

   /**
    * Returns document identifier from storage document file. Reverses path creation logic to generate a
    * logical document path.
    *
    * @param storageFile storage document file
    *
    * @return document identifier from storage file
    */
   private DocumentPath documentId(File storageFile) {
      return fsStorageStrategy.documentId(storageFile.toPath());
   }

   private Path documentDirectory(File storageDirectory) {
      return fsStorageStrategy.documentDirectory(storageDirectory.toPath());
   }

   /**
    * Delete repository storage path if configured to do so.
    */
   private void deleteRepositoryOnClose() {
      if (deleteRepositoryOnClose) {
         Path storagePath = fsStorageStrategy.storagePath();
         try {
            FileUtils.deleteDirectory(storagePath.toFile());
         } catch (IllegalArgumentException e) {
            /* quiet. */
         } catch (IOException e) {
            throw new UncheckedIOException(String.format("Can't delete '%s' repository > %s",
                                                         storagePath,
                                                         e.getMessage()), e);
         }
      }
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", FsDocumentRepository.class.getSimpleName() + "[", "]")
            .add("fsStorageStrategy=" + fsStorageStrategy)
            .toString();
   }
}
