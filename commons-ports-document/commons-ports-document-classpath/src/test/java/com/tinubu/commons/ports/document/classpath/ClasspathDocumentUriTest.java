/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.classpath;

import static com.tinubu.commons.ddd2.uri.DefaultUriRestrictions.noRestrictions;
import static com.tinubu.commons.ddd2.uri.Uri.uri;
import static com.tinubu.commons.lang.util.CheckedPredicate.alwaysTrue;
import static com.tinubu.commons.ports.document.classpath.ClasspathRepositoryUri.Parameters.CLASSPATH_PREFIX;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.defaultParameters;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.noParameters;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.sensitiveParameters;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.net.URI;
import java.nio.file.Path;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;

public class ClasspathDocumentUriTest {

   static {
      Assertions.setMaxStackTraceElementsDisplayed(50);
   }

   @Nested
   public class OfUriWhenURI {

      @Test
      public void ofUriWhenNominal() {
         assertThat(ClasspathDocumentUri.ofDocumentUri(uri("classpath:/path/sub/document"))).satisfies(uri -> {
            assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path/sub"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
         assertThat(ClasspathDocumentUri.ofDocumentUri("classpath:/path/sub/document")).satisfies(uri -> {
            assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path/sub"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
      }

      @Test
      public void ofUriWhenUri() {
         assertThat(ClasspathDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(Uri.ofUri(
               "classpath:/path/document?classpathPrefix=/path")))).satisfies(uri -> {
            assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
         assertThat(ClasspathDocumentUri.ofDocumentUri(Uri.ofUri(
               "classpath:/path/document?classpathPrefix=/path"))).satisfies(uri -> {
            assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
      }

      @Test
      public void ofUriWhenClasspathUri() {
         assertThat(ClasspathDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(ClasspathUri.ofUri(
               "classpath:/path/document?classpathPrefix=/path",
               noRestrictions())))).satisfies(uri -> {
            assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
         assertThat(ClasspathDocumentUri.ofDocumentUri(ClasspathUri.ofUri(
               "classpath:/path/document?classpathPrefix=/path",
               noRestrictions()))).satisfies(uri -> {
            assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
      }

      @Test
      public void ofUriWhenBadParameters() {
         assertThatThrownBy(() -> ClasspathDocumentUri.ofDocumentUri((URI) null, Path.of("/")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
         assertThatThrownBy(() -> ClasspathDocumentUri.ofDocumentUri(uri("classpath:/document"), null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultClasspathPrefix' must not be null");
         assertThatThrownBy(() -> ClasspathDocumentUri.ofDocumentUri((String) null, Path.of("/")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
         assertThatThrownBy(() -> ClasspathDocumentUri.ofDocumentUri("classpath:/document", null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultClasspathPrefix' must not be null");
         assertThatThrownBy(() -> ClasspathDocumentUri.ofDocumentUri((DocumentUri) null, Path.of("/")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
      }

      @Test
      @Disabled(
            "There's no way to create an URI with an invalid Path, the only way is tu use \u0000 but it also fails the URI preemptively")
      public void ofUriWhenIllegalArgument() {
         assertThatExceptionOfType(InvalidUriException.class)
               .isThrownBy(() -> ClasspathDocumentUri.ofDocumentUri("classpath:/document?classpathPrefix=:",
                                                                    Path.of("/")))
               .withMessage(
                     "Invalid '%s' URI: Conversion from 'java.lang.String' to 'java.util.Path' failed for ':' value",
                     "classpath:/document?classpathPrefix=:");
      }

      @Test
      public void ofUriWhenCaseSensitiveParameterKey() {
         assertThat(ClasspathDocumentUri.ofDocumentUri("classpath:/path/document?ClAsSpAtHPrEfiX=/otherpath",
                                                       Path.of("/"))).satisfies(uri -> {
            assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/document"));
         });
      }

      @Test
      public void ofUriWhenNormalized() {
         assertThat(ClasspathDocumentUri.ofDocumentUri(uri("classpath:/path/.././document"),
                                                       Path.of("/"))).satisfies(uri -> {
            assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
         assertThat(ClasspathDocumentUri.ofDocumentUri("classpath:/path/.././document",
                                                       Path.of("/"))).satisfies(uri -> {
            assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
         assertThat(ClasspathDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(
               "classpath:/path/.././document"), Path.of("/"))).satisfies(uri -> {
            assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
         assertThatThrownBy(() -> ClasspathDocumentUri.ofDocumentUri(uri("classpath:/../path/document"),
                                                                     Path.of("/")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessageContainingAll("Invalid 'classpath:/../path/document' URI",
                                        "must not have traversal paths");
      }

      @Test
      public void ofUriWhenInvalidUriSyntax() {
         assertThatThrownBy(() -> ClasspathDocumentUri.ofDocumentUri("git@git.com:user/repo.git"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'git@git.com:user/repo.git' URI: Illegal character in scheme name at index 3: git@git.com:user/repo.git");
         assertThatThrownBy(() -> ClasspathDocumentUri.ofDocumentUri("git@git.com:user/repo.git"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'git@git.com:user/repo.git' URI: Illegal character in scheme name at index 3: git@git.com:user/repo.git");
      }

      @Test
      public void ofUriWhenIncompatibleUri() {
         assertThatThrownBy(() -> ClasspathDocumentUri.ofDocumentUri(uri("unknown:/path/document")))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessageContainingAll("Incompatible 'unknown:/path/document' URI",
                                        "must be absolute, and scheme must be equal to 'classpath'");
         assertThatThrownBy(() -> ClasspathDocumentUri.ofDocumentUri(uri(
               "classpath://user:password@host/document")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessageContainingAll("Incompatible 'classpath://user:password@host/document' URI",
                                        "must not have authority");
         assertThatThrownBy(() -> ClasspathDocumentUri.ofDocumentUri(uri("classpath:///document")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessageContainingAll("Incompatible 'classpath:///document' URI",
                                        "must not have authority");
      }

      @Test
      public void ofUriWhenInvalidUri() {
         assertThatThrownBy(() -> ClasspathDocumentUri.ofDocumentUri("classpath:path/document", Path.of("/")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessageContainingAll("Incompatible 'classpath:path/document' URI", "must be hierarchical");
         assertThatThrownBy(() -> ClasspathDocumentUri.ofDocumentUri("classpath:/path/document#fragment",
                                                                     Path.of("/")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessageContainingAll("Invalid 'classpath:/path/document#fragment' URI",
                                        "must not have a fragment");
      }

      @Test
      public void ofUriWhenClasspathPrefix() {
         assertThat(ClasspathDocumentUri.ofDocumentUri(uri("classpath:/path/document"),
                                                       Path.of("/"))).satisfies(uri -> {
            assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/document"));
         });
         assertThat(ClasspathDocumentUri.ofDocumentUri("classpath:/path/document", Path.of("/"))).satisfies(
               uri -> {
                  assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/"));
                  assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/document"));
               });
      }

      @Test
      public void ofUriWhenBadDefaultClasspathPrefix() {
         assertThatThrownBy(() -> ClasspathDocumentUri.ofDocumentUri("classpath:/document", null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultClasspathPrefix' must not be null");
         assertThatThrownBy(() -> ClasspathDocumentUri.ofDocumentUri("classpath:/document", Path.of("")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultClasspathPrefix=' must be absolute path");
         assertThatThrownBy(() -> ClasspathDocumentUri.ofDocumentUri("classpath:/document", Path.of("path")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultClasspathPrefix=path' must be absolute path");
         assertThatThrownBy(() -> ClasspathDocumentUri.ofDocumentUri("classpath:/document", Path.of("path")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultClasspathPrefix=path' must be absolute path");
         assertThat(ClasspathDocumentUri.ofDocumentUri("classpath:/path/document", Path.of("/../path")))
               .as("defaultClasspathPrefix is normalized to /path, so that it doesn't fail")
               .isNotNull();
      }

      @Test
      public void ofUriWhenRootClasspathPrefix() {
         assertThat(ClasspathDocumentUri.ofDocumentUri(uri("classpath:/path/document"),
                                                       Path.of("/"))).satisfies(uri -> {
            assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/document"));
         });
         assertThat(ClasspathDocumentUri.ofDocumentUri("classpath:/path/document", Path.of("/"))).satisfies(
               uri -> {
                  assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/"));
                  assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/document"));
               });
      }

      @Test
      public void ofUriWhenDefaultClasspathPrefix() {
         assertThat(ClasspathDocumentUri.ofDocumentUri(uri("classpath:/path/document"),
                                                       Path.of("/path"))).satisfies(uri -> {
            assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
         assertThat(ClasspathDocumentUri.ofDocumentUri("classpath:/path/document",
                                                       Path.of("/path"))).satisfies(uri -> {
            assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
      }

      @Test
      public void ofUriWhenMismatchingDefaultClasspathPrefix() {
         assertThat(ClasspathDocumentUri.ofDocumentUri(uri("classpath:/path/document"),
                                                       Path.of("/otherpath")))
               .as("Default classpath prefix is only applied if it matches actual URI")
               .satisfies(uri -> {
                  assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path"));
                  assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
               });
         assertThat(ClasspathDocumentUri.ofDocumentUri("classpath:/path/document", Path.of("/otherpath")))
               .as("Default classpath prefix is only applied if it matches actual URI")
               .satisfies(uri -> {
                  assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path"));
                  assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
               });
      }

      @Test
      public void ofUriWhenInvalidClasspathPrefixParameter() {
         assertThatThrownBy(() -> ClasspathDocumentUri.ofDocumentUri(
               "classpath:/path/subpath/key?classpathPrefix=/path/subpath/key",
               Path.of("/")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'classpath:/path/subpath/key?classpathPrefix=/path/subpath/key' URI: 'classpathPrefix=/path/subpath/key' parameter must start but not be equal to URI '/path/subpath/key' path");
         assertThatThrownBy(() -> ClasspathDocumentUri.ofDocumentUri(
               "classpath:/path/subpath/document?classpathPrefix=path/subpath",
               Path.of("/")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid '%s' URI: 'classpathPrefix=path/subpath' parameter must be absolute and must not contain traversal",
                     "classpath:/path/subpath/document?classpathPrefix=path/subpath");
         assertThatThrownBy(() -> ClasspathDocumentUri.ofDocumentUri(
               "classpath:/path/subpath/document?classpathPrefix=../path",
               Path.of("/")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid '%s' URI: 'classpathPrefix=../path' parameter must be absolute and must not contain traversal",
                     "classpath:/path/subpath/document?classpathPrefix=../path");
         assertThat(ClasspathDocumentUri.ofDocumentUri(
               "classpath:/path/subpath/document?classpathPrefix=/path/subpath/..",
               Path.of("/"))).satisfies(uri -> {
            assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("subpath/document"));
         });
      }

      @Test
      public void ofUriWhenExactClasspathPrefixParameter() {
         assertThatThrownBy(() -> ClasspathDocumentUri.ofDocumentUri(
               "classpath:/document?classpathPrefix=/path/subpath",
               Path.of("/")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'classpath:/document?classpathPrefix=/path/subpath' URI: 'classpathPrefix=/path/subpath' parameter must start but not be equal to URI '/document' path");
         assertThat(ClasspathDocumentUri.ofDocumentUri(
               "classpath:/path/subpath/document?classpathPrefix=/path/subpath",
               Path.of("/"))).satisfies(uri -> {
            assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path/subpath"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
         assertThat(ClasspathDocumentUri.ofDocumentUri(
               "classpath:/path/subpath/document?classpathPrefix=/path/./subpath/.",
               Path.of("/"))).satisfies(uri -> {
            assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path/subpath"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
      }

      @Test
      public void ofUriWhenPartialClasspathPrefixParameter() {
         assertThatThrownBy(() -> ClasspathDocumentUri.ofDocumentUri(
               "classpath:/document/?classpathPrefix=/path",
               Path.of("/")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'classpath:/document/?classpathPrefix=/path' URI: 'classpathPrefix=/path' parameter must start but not be equal to URI '/document' path");
         assertThat(ClasspathDocumentUri.ofDocumentUri(
               "classpath:/path/subpath/document?classpathPrefix=/path",
               Path.of("/"))).satisfies(uri -> {
            assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("subpath/document"));

         });
         assertThat(ClasspathDocumentUri.ofDocumentUri(
               "classpath:/path/subpath/document?classpathPrefix=/path/.",
               Path.of("/"))).satisfies(uri -> {
            assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("subpath/document"));
         });
      }

      @Test
      public void ofUriWhenMismatchingClasspathPrefixParameter() {
         assertThatThrownBy(() -> ClasspathDocumentUri.ofDocumentUri(
               "classpath:/path/subpath/document?classpathPrefix=/otherpath",
               Path.of("/")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'classpath:/path/subpath/document?classpathPrefix=/otherpath' URI: 'classpathPrefix=/otherpath' parameter must start but not be equal to URI '/path/subpath/document' path");
         assertThatThrownBy(() -> ClasspathDocumentUri.ofDocumentUri(
               "classpath:/path/subpath/document?classpathPrefix=/path/subpath/otherpath",
               Path.of("/")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'classpath:/path/subpath/document?classpathPrefix=/path/subpath/otherpath' URI: 'classpathPrefix=/path/subpath/otherpath' parameter must start but not be equal to URI '/path/subpath/document' path");
      }

      @Test
      public void ofUriWhenDefaultParameters() {
         assertThat(ClasspathDocumentUri.ofDocumentUri("classpath:/document",
                                                       Path.of("/"))).satisfies(uri -> {
            assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/"));
            assertThat(uri.query().parameter(CLASSPATH_PREFIX, alwaysTrue())).hasValue(Path.of("/"));
         });
      }

      @Test
      public void ofUriWhenAllParameters() {
         assertThat(ClasspathDocumentUri.ofDocumentUri("classpath:/path/document?classpathPrefix=/path",
                                                       Path.of("/"))).satisfies(uri -> {
            assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path"));
            assertThat(uri.query().parameter(CLASSPATH_PREFIX, alwaysTrue())).hasValue(Path.of("/path"));
         });
      }

      @Nested
      class WrappedUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(ClasspathDocumentUri.ofDocumentUri(uri("document:classpath:classpath:/document"))).satisfies(
                  uri -> {
                     assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/"));
                     assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
                  });
            assertThat(ClasspathDocumentUri.ofDocumentUri(uri(
                  "document:classpath:classpath:/path/.././document"))).satisfies(uri -> {
               assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
         }

         @Test
         public void ofUriWhenDefaultClasspathPrefix() {
            assertThat(ClasspathDocumentUri.ofDocumentUri(uri("document:classpath:classpath:/path/document"),
                                                          Path.of("/path"))).satisfies(uri -> {
               assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
         }

         @Test
         public void ofUriWhenBadRepositoryType() {
            assertThatThrownBy(() -> ClasspathDocumentUri.ofDocumentUri(uri(
                  "document:unknown:classpath:/document"), Path.of("/")))
                  .isInstanceOf(IncompatibleUriException.class)
                  .hasMessage(
                        "Incompatible 'document:unknown:classpath:/document' URI: Unsupported 'unknown' repository type");
         }

         @Test
         public void ofUriWhenNormalized() {
            assertThat(ClasspathDocumentUri.ofDocumentUri(uri(
                  "document:classpath:classpath:/path/.././document"), Path.of("/"))).satisfies(uri -> {
               assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
            assertThat(ClasspathDocumentUri.ofDocumentUri("document:classpath:classpath:/path/.././document",
                                                          Path.of("/"))).satisfies(uri -> {
               assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
            assertThat(ClasspathDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(
                  "document:classpath:classpath:/path/.././document"), Path.of("/"))).satisfies(uri -> {
               assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
            assertThatThrownBy(() -> ClasspathDocumentUri.ofDocumentUri(uri(
                  "document:classpath:classpath:/../path/document"), Path.of("/")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessageContainingAll("Invalid 'classpath:/../path/document' URI",
                                           "must not have traversal paths");
         }

      }
   }

   @Nested
   public class OfUriWhenDocumentUri {

      @Test
      public void ofUriWhenBadDefaultClasspathPrefix() {
         assertThatThrownBy(() -> ClasspathDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(
               "classpath:/path"), null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultClasspathPrefix' must not be null");
         assertThatThrownBy(() -> ClasspathDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(
               "classpath:/path"), Path.of("")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultClasspathPrefix=' must be absolute path");
         assertThatThrownBy(() -> ClasspathDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(
               "classpath:/path"), Path.of("path")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultClasspathPrefix=path' must be absolute path");
         assertThat(ClasspathDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri("classpath:/path/document"),
                                                       Path.of("/../path")))
               .as("defaultClasspathPrefix is normalized to '/path', so that it doesn't fail")
               .isNotNull();
      }

      @Nested
      public class WhenDocumentUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(ClasspathDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri("classpath:/path/document"),
                                                          Path.of("/path"))).satisfies(uri -> {
               assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
         }

         @Test
         public void ofUriWhenImplicitDefaultClasspathPrefix() {
            assertThat(ClasspathDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(
                  "classpath:/path/subpath/document"))).satisfies(uri -> {
               assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path/subpath"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
         }

         @Test
         public void ofUriWhenMismatchingDefaultStoragePathAndBasePath() {
            assertThatThrownBy(() -> ClasspathDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(
                  "classpath:/path/subpath/document"), Path.of("/path/subpath/document")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'classpath:/path/subpath/document' URI: 'classpathPrefix=/path/subpath/document' parameter must start but not be equal to URI '/path/subpath/document' path");
         }

         @Test
         public void ofUriWhenDefaultClasspathPrefix() {
            assertThat(ClasspathDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(
                  "classpath:/path/subpath/document"), Path.of("/path"))).satisfies(uri -> {
               assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("subpath/document"));
            });
         }

         @Test
         public void ofUriWhenRoot() {
            assertThat(ClasspathDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri("classpath:/document"))).satisfies(
                  uri -> {
                     assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/"));
                     assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
                  });
            assertThat(ClasspathDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri("classpath:/document"),
                                                          Path.of("/"))).satisfies(uri -> {
               assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
            assertThatThrownBy(() -> ClasspathDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(
                  "classpath:/")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage("Invalid 'classpath:/' URI: Missing document id");
         }

         @Test
         public void ofUriWhenExplicitClasspathPrefix() {
            assertThat(ClasspathDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(
                  "classpath:/path/subpath/document?classpathPrefix=/path"), Path.of("/"))).satisfies(uri -> {
               assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("subpath/document"));
            });
         }

         @Test
         public void ofUriWhenMismatchingExplicitClasspathPrefix() {
            assertThatThrownBy(() -> ClasspathDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(
                  "classpath:/path/subpath/document?classpathPrefix=/subpath")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'classpath:/path/subpath/document?classpathPrefix=/subpath' URI: 'classpathPrefix=/subpath' parameter must start but not be equal to URI '/path/subpath/document' path");
            assertThatThrownBy(() -> ClasspathDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(
                  "classpath:/path/subpath/document?classpathPrefix=/path/subpath/document")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'classpath:/path/subpath/document?classpathPrefix=/path/subpath/document' URI: 'classpathPrefix=/path/subpath/document' parameter must start but not be equal to URI '/path/subpath/document' path");
         }

      }

      @Nested
      public class WhenClasspathDocumentUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(ClasspathDocumentUri.ofDocumentUri(ClasspathDocumentUri.ofDocumentUri(
                  "classpath:/path/document",
                  Path.of("/path")))).satisfies(uri -> {
               assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
         }

         @Test
         public void ofUriWhenImplicitDefaultClasspathPrefix() {
            assertThat(ClasspathDocumentUri.ofDocumentUri(ClasspathDocumentUri.ofDocumentUri(
                  "classpath:/path/subpath/document"))).satisfies(uri -> {
               assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path/subpath"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
         }

         @Test
         public void ofUriWhenDefaultClasspathPrefix() {
            assertThat(ClasspathDocumentUri.ofDocumentUri(ClasspathDocumentUri.ofDocumentUri(
                  "classpath:/path/subpath/document"), Path.of("/path"))).satisfies(uri -> {
               assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path/subpath"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
         }

      }
   }

   @Nested
   public class OfConfig {

      @Test
      public void ofConfigWhenNominal() {
         assertThat(ClasspathDocumentUri.ofConfig(Path.of("/path"), DocumentPath.of("document"))).satisfies(
               uri -> {
                  assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path"));
                  assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));

                  assertThat(uri.query()
                                   .parameter(CLASSPATH_PREFIX, alwaysTrue())).hasValue(Path.of("/path"));
               });
      }

      @Test
      public void ofConfigWhenBadConfiguration() {
         assertThatThrownBy(() -> ClasspathDocumentUri.ofConfig(Path.of(""), DocumentPath.of("document")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'classpathPrefix=' must be absolute path");
         assertThatThrownBy(() -> ClasspathDocumentUri.ofConfig(Path.of("path"), DocumentPath.of("document")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'classpathPrefix=path' must be absolute path");
         assertThat(ClasspathDocumentUri.ofConfig(Path.of("/../path"), DocumentPath.of("document")))
               .as("classpathPrefix is normalized to /path, so it doesn't fail")
               .isNotNull();
      }

   }

   @Nested
   public class ToUri {

      @Test
      public void toUriWhenNominal() {
         assertThat(ClasspathDocumentUri
                          .ofDocumentUri("classpath:/document", Path.of("/"))
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("classpath:/document");
      }

      @Test
      public void toUriWhenClasspathPrefix() {
         assertThat(ClasspathDocumentUri
                          .ofDocumentUri("classpath:/path/document", Path.of("/"))
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("classpath:/path/document");
         assertThat(ClasspathDocumentUri
                          .ofDocumentUri("classpath:/path/document", Path.of("/path"))
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("classpath:/path/document");
      }

      @Test
      public void toUriWhenDefaultParameters() {
         assertThat(ClasspathDocumentUri
                          .ofDocumentUri("classpath:/path/document", Path.of("/"))
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("classpath:/path/document?classpathPrefix=/");
         assertThat(ClasspathDocumentUri
                          .ofDocumentUri("classpath:/path/subpath/document?classpathPrefix=/path")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("classpath:/path/subpath/document?classpathPrefix=/path");
         assertThat(ClasspathDocumentUri
                          .ofDocumentUri("classpath:/path/document", Path.of("/path"))
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("classpath:/path/document");
         assertThat(ClasspathDocumentUri
                          .ofDocumentUri("classpath:/path/document", Path.of("/otherpath"))
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("classpath:/path/document");
      }

      @Test
      public void toUriWhenDefaultParametersWithForceDefaultValues() {
         assertThat(ClasspathDocumentUri
                          .ofDocumentUri("classpath:/path/document", Path.of("/"))
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo("classpath:/path/document?classpathPrefix=/");
         assertThat(ClasspathDocumentUri
                          .ofDocumentUri("classpath:/path/document?classpathPrefix=/path", Path.of("/"))
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo("classpath:/path/document?classpathPrefix=/path");
         assertThat(ClasspathDocumentUri
                          .ofDocumentUri("classpath:/document?classpathPrefix=/", Path.of("/"))
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo("classpath:/document?classpathPrefix=/");
      }

      @Test
      public void toUriWhenSensitiveParameters() {
         assertThat(ClasspathDocumentUri
                          .ofDocumentUri("classpath:/path/document")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("classpath:/path/document");
         assertThat(ClasspathDocumentUri
                          .ofDocumentUri("classpath:/path/subpath/document?classpathPrefix=/path")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("classpath:/path/subpath/document?classpathPrefix=/path");
      }

   }
}