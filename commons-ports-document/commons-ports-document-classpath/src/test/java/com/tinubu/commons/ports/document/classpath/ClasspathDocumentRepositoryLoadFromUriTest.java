/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.classpath;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.nio.file.Path;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.lang.convert.ConversionFailedException;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.RepositoryUri;

public class ClasspathDocumentRepositoryLoadFromUriTest {

   @Nested
   class OfUri {

      @Test
      public void ofUriWhenNominal() {
         assertThat(ClasspathDocumentRepository.ofUri(RepositoryUri.ofRepositoryUri("classpath:/path/subpath"))).satisfies(
               repository -> {
                  assertThat(repository.classpathPrefix()).isEqualTo(Path.of("/path/subpath"));
               });

         assertThat(ClasspathDocumentRepository.ofUri(DocumentUri.ofDocumentUri("classpath:/path/subpath"))).satisfies(
               repository -> {
                  assertThat(repository.classpathPrefix()).isEqualTo(Path.of("/path"));
               });
      }

      @Test
      public void testOfUriWhenBadParameters() {
         assertThatExceptionOfType(InvariantValidationException.class)
               .isThrownBy(() -> ClasspathDocumentRepository.ofUri(null))
               .withMessage("Invariant validation error > 'uri' must not be null");
      }

      @Test
      public void ofUriWhenRoot() {
         assertThat(ClasspathDocumentRepository.ofUri(RepositoryUri.ofRepositoryUri("classpath:/"))).satisfies(
               repository -> {
                  assertThat(repository.classpathPrefix()).isEqualTo(Path.of("/"));
               });
         assertThatThrownBy(() -> ClasspathDocumentRepository.ofUri(DocumentUri.ofDocumentUri("classpath:/")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage("Invalid 'classpath:/' URI: Missing document id");
      }

      @Test
      public void ofUriWhenRelativePath() {
         assertThatThrownBy(() -> ClasspathDocumentRepository.ofUri(RepositoryUri.ofRepositoryUri(
               "classpath:path")))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessageContainingAll("Incompatible 'classpath:path' URI", "must be hierarchical");
         assertThatThrownBy(() -> ClasspathDocumentRepository.ofUri(DocumentUri.ofDocumentUri("classpath:path")))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessageContainingAll("Incompatible 'classpath:path' URI", "must be hierarchical");
      }

      @Test
      public void ofUriWhenIncompatibleUri() {
         assertThatThrownBy(() -> ClasspathDocumentRepository.ofUri(RepositoryUri.ofRepositoryUri(
               "other:/path")))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessageContainingAll("Incompatible 'other:/path' URI",
                                        "must be absolute, and scheme must be equal to 'classpath'");
         assertThatThrownBy(() -> ClasspathDocumentRepository.ofUri(RepositoryUri.ofRepositoryUri(
               "classpath://user@host/path")))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessageContainingAll("Incompatible 'classpath://user@host/path' URI",
                                        "must not have authority");
         assertThatThrownBy(() -> ClasspathDocumentRepository.ofUri(RepositoryUri.ofRepositoryUri(
               "classpath:path")))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessageContainingAll("Incompatible 'classpath:path' URI", "must be hierarchical");
         assertThatThrownBy(() -> ClasspathDocumentRepository.ofUri(RepositoryUri.ofRepositoryUri(
               "classpath://host/path")))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessageContainingAll("Incompatible 'classpath://host/path' URI",
                                        "must not have authority");
      }

      @Test
      public void ofUriWhenInvalidUri() {
         assertThatThrownBy(() -> ClasspathDocumentRepository.ofUri(RepositoryUri.ofRepositoryUri(
               "classpath:/path#fragment")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessageContainingAll("Invalid 'classpath:/path#fragment' URI", "must not have a fragment");
      }

      @Test
      @Disabled("Boolean.valueOf is too much tolerant so that there's no way to generate an error ;)")
      public void ofUriWhenIllegalArgument() {
         assertThatExceptionOfType(ConversionFailedException.class)
               .isThrownBy(() -> ClasspathDocumentRepository.ofUri(RepositoryUri.ofRepositoryUri(
                     "classpath:/path?classpathPrefix=Invalid")))
               .withMessage(
                     "Conversion from 'java.lang.String' to 'java.lang.Boolean' failed for 'Invalid' value");
      }

      @Test
      public void ofUriWhenCaseSensitiveParameterKey() {
         assertThat(ClasspathDocumentRepository.ofUri(RepositoryUri.ofRepositoryUri(
               "classpath:/path/subpath?CLASSPATHPREFIX=/path"))).satisfies(repository -> {
            assertThat(repository.classpathPrefix()).isEqualTo(Path.of("/path/subpath"));
         });
      }

      /* cf ClasspathRepoUriTest
      @Test
      public void ofUriWhenAllParameters() {
         assertThat(ClasspathDocumentRepository.ofUri(URI.create(
               "classpath:///path/subpath/document?classpathPrefix=/path"), true)).hasValueSatisfying(
               repository -> {

                  assertThat(repository.classpathPrefix()).isEqualTo(Path.of("/path"));
               });
      }

      @Test
      public void ofUriWhenHasRelativeClasspathPrefixParameter(boolean documentUri) {
         assertThat(ClasspathDocumentRepository.ofUri(URI.create(
               "classpath:///path/subpath?classpathPrefix=path/subpath"), documentUri)).isEmpty();
      }

      @Test
      public void ofUriWhenHasExactClasspathPrefixParameter(boolean documentUri) {
         assertThat(ClasspathDocumentRepository.ofUri(URI.create(
               "classpath:///path/subpath?classpathPrefix=/path/subpath"), documentUri)).satisfies(
               optionalRepository -> {
                  if (documentUri) {
                     assertThat(optionalRepository).isEmpty();
                  } else {
                     assertThat(optionalRepository).hasValueSatisfying(repository -> {
                        assertThat(repository.classpathPrefix()).isEqualTo(Path.of("/path/subpath"));
                     });
                  }
               });
      }

      @Test
      public void ofUriWhenHasPartialClasspathPrefixParameter(boolean documentUri) {
         assertThat(ClasspathDocumentRepository.ofUri(URI.create(
                                                            "classpath:///path/subpath/othersubpath?classpathPrefix=/path/subpath"),
                                                      documentUri)).satisfies(optionalRepository -> {
            if (documentUri) {
               assertThat(optionalRepository).hasValueSatisfying(repository -> {
                  assertThat(repository.classpathPrefix()).isEqualTo(Path.of("/path/subpath"));
               });
            } else {
               assertThat(optionalRepository)
                     .as("classpathPrefix must match URI path if specified")
                     .isEmpty();
            }
         });
      }

      @Test
      public void ofUriWhenHasMismatchingClasspathPrefixParameter(boolean documentUri) {
         assertThat(ClasspathDocumentRepository.ofUri(URI.create(
               "classpath:///path/subpath?classpathPrefix=/othersubpath"), documentUri)).isEmpty();
      }
      */
   }

   @Nested
   class ReferencedDocument {

      @Test
      public void referencedDocumentWhenNominal() {
         assertThat(ClasspathDocumentRepository.referencedDocument(DocumentUri.ofDocumentUri(
               "classpath:/path/subpath"))).satisfies(rd -> {
            assertThat(rd.documentRepository().classpathPrefix()).isEqualTo(Path.of("/path"));
            assertThat(rd.documentId()).isEqualTo(DocumentPath.of("subpath"));
         });
      }

      @Test
      public void referencedDocumentWhenRoot() {
         assertThatThrownBy(() -> ClasspathDocumentRepository.referencedDocument(DocumentUri.ofDocumentUri(
               "classpath:/")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage("Invalid 'classpath:/' URI: Missing document id");
      }

      @Test
      public void referencedDocumentWhenRelativePath() {
         assertThatThrownBy(() -> ClasspathDocumentRepository.referencedDocument(DocumentUri.ofDocumentUri(
               "classpath:path")))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessageContainingAll("Incompatible 'classpath:path' URI", "must be hierarchical");
      }

      @Test
      public void referencedDocumentWhenIncompatibleUri() {
         assertThatThrownBy(() -> ClasspathDocumentRepository.ofUri(DocumentUri.ofDocumentUri("other:/path")))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessageContainingAll("Incompatible 'other:/path' URI",
                                        "must be absolute, and scheme must be equal to 'classpath'");
         assertThatThrownBy(() -> ClasspathDocumentRepository.ofUri(DocumentUri.ofDocumentUri(
               "classpath://user@host/path")))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessageContainingAll("Incompatible 'classpath://user@host/path' URI",
                                        "must not have authority");
         assertThatThrownBy(() -> ClasspathDocumentRepository.ofUri(DocumentUri.ofDocumentUri(
               "classpath://host/path")))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessageContainingAll("Incompatible 'classpath://host/path' URI",
                                        "must not have authority");
      }

      @Test
      public void referencedDocumentWhenInvalidUri() {
         assertThatThrownBy(() -> ClasspathDocumentRepository.ofUri(DocumentUri.ofDocumentUri("classpath:path")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessageContainingAll("Incompatible 'classpath:path' URI", "must be hierarchical");
         assertThatThrownBy(() -> ClasspathDocumentRepository.ofUri(DocumentUri.ofDocumentUri(
               "classpath:/path#fragment")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessageContainingAll("Invalid 'classpath:/path#fragment' URI", "must not have a fragment");
      }

      @Test
      @Disabled("Boolean.valueOf is too much tolerant so that there's no way to generate an error ;)")
      public void referencedDocumentWhenIllegalArgument() {
         assertThatExceptionOfType(ConversionFailedException.class)
               .isThrownBy(() -> ClasspathDocumentRepository.referencedDocument(DocumentUri.ofDocumentUri(
                     "classpath:///path?classpathPrefix=Invalid")))
               .withMessage(
                     "Conversion from 'java.lang.String' to 'java.lang.Boolean' failed for 'Invalid' value");
      }

      @Test
      public void referencedDocumentWhenCaseSensitiveParameterKey() {
         assertThat(ClasspathDocumentRepository.referencedDocument(DocumentUri.ofDocumentUri(
               "classpath:/path/subpath/document?CLASSPATHPREFIX=/path"))).satisfies(rd -> {
            assertThat(rd.documentRepository().classpathPrefix()).isEqualTo(Path.of("/path/subpath"));
         });
      }
   }
}
