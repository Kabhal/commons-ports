/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.classpath;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.nio.file.Path;
import java.util.List;
import java.util.function.UnaryOperator;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.criterion.Criterion.CriterionBuilder;
import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentEntryCriteria.DocumentEntryCriteriaBuilder;
import com.tinubu.commons.ports.document.domain.DocumentMetadata;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.testsuite.ReadOnlyDocumentRepositoryTest;
import com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ZipArchiver;

public class ClasspathDocumentRepositoryTest extends ReadOnlyDocumentRepositoryTest {

   private static final Path TEST_CLASSPATH_PREFIX = path("/com/tinubu/commons/ports/document/classpath");

   private ClasspathDocumentRepository documentRepository;

   @Override
   protected boolean isSupportingContentLength() {
      return false;
   }

   @Override
   protected boolean isSupportingMetadataAttributes() {
      return false;
   }

   @BeforeEach
   public void configureDocumentRepository() {
      this.documentRepository = newClasspathDocumentRepository();
   }

   @AfterEach
   public void closeDocumentRepository() {
      this.documentRepository.close();
   }

   @Override
   protected DocumentRepository documentRepository() {
      return documentRepository;
   }

   @Override
   protected Document zipTransformer(DocumentPath zipPath, List<Document> documents) {
      return new ZipArchiver(zipPath).compress(documents);
   }

   private ClasspathDocumentRepository newClasspathDocumentRepository() {
      return newClasspathDocumentRepository(TEST_CLASSPATH_PREFIX);
   }

   private ClasspathDocumentRepository newClasspathDocumentRepository(Path classpathPrefix) {
      return new ClasspathDocumentRepository(classpathPrefix);
   }

   @Override
   protected UnaryOperator<DocumentMetadataBuilder> synchronizeExpectedMetadata(DocumentMetadata actual) {
      return builder -> builder
            .chain(super.synchronizeExpectedMetadata(actual))
            .attributes(actual.attributes())
            .contentType(actual.contentEncoding().orElse(null));
   }

   @Test
   public void testClassPathPrefixWhenNominal() {
      ClasspathDocumentRepository repository =
            newClasspathDocumentRepository(path("/com/tinubu/commons/ports/document/classpath"));

      assertThat(repository.findDocumentById(DocumentPath.of("path/pathfile1.pdf"))).isPresent();
   }

   @Test
   public void testClassPathPrefixWhenNull() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newClasspathDocumentRepository(null))
            .withMessage("Invariant validation error > 'classpathPrefix' must not be null");
   }

   @Test
   public void testClassPathPrefixWhenRelative() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newClasspathDocumentRepository(Path.of(
                  "com/tinubu/commons/ports/document/classpath")))
            .withMessage(
                  "Invariant validation error > 'classpathPrefix=com/tinubu/commons/ports/document/classpath' must be absolute path");
   }

   @Test
   public void testClassPathPrefixWhenEmpty() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newClasspathDocumentRepository(Path.of("")))
            .withMessage("Invariant validation error > 'classpathPrefix=' must be absolute path");
   }

   @Test
   public void testClassPathPrefixWhenRoot() {
      ClasspathDocumentRepository repository = newClasspathDocumentRepository(path("/"));

      assertThat(repository.findDocumentById(DocumentPath.of(
            "com/tinubu/commons/ports/document/classpath/path/pathfile1.pdf"))).isPresent();
   }

   @Test
   public void testFindDocumentEntriesBySpecificationWhenPathContain() {
      List<Document> documents = documentRepository()
            .findDocumentsBySpecification(new DocumentEntryCriteriaBuilder()
                                                .documentPath(CriterionBuilder.contain(path("path")))
                                                .build())
            .collect(toList());

      assertThat(documents)
            .extracting(entry -> entry.documentId().stringValue())
            .containsExactlyInAnyOrder("path/pathfile1.pdf",
                                       "path/pathfile2.txt",
                                       "path/subpath/subpathfile1.pdf",
                                       "path/subpath/subpathfile2.txt");
   }

   @Test
   public void testFindDocumentEntriesBySpecificationWhenPathNotContain() {
      List<Document> documents = documentRepository()
            .findDocumentsBySpecification(path("path"),
                                          new DocumentEntryCriteriaBuilder()
                                                .documentPath(CriterionBuilder.notContain(path("subpath")))
                                                .build())
            .collect(toList());

      assertThat(documents)
            .extracting(entry -> entry.documentId().stringValue())
            .containsExactlyInAnyOrder("path/pathfile1.pdf", "path/pathfile2.txt");
   }

   @Test
   public void testFindDocumentEntriesBySpecificationWhenPathMatch() {
      List<Document> documents = documentRepository()
            .findDocumentsBySpecification(new DocumentEntryCriteriaBuilder()
                                                .documentPath(CriterionBuilder.match(path("path/**/*.pdf")))
                                                .build())
            .collect(toList());

      assertThat(documents)
            .extracting(entry -> entry.documentId().stringValue())
            .containsExactlyInAnyOrder("path/subpath/subpathfile1.pdf");
   }

   @Nested
   public class DocumentRepositoryUriAdapter {

      @Test
      public void testSupportsDocumentUriWhenNominal() {
         assertThat(documentRepository().supportsUri(documentUri(
               "classpath:/com/tinubu/commons/ports/document/classpath/path/test.txt"))).isTrue();
      }

      @Test
      public void testSupportsDocumentUriWhenRepositoryUri() {
         assertThatThrownBy(() -> documentRepository().supportsUri(documentUri(
               "classpath:/com/tinubu/commons/ports/document/classpath")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'classpath:/com/tinubu/commons/ports/document/classpath' URI: 'classpathPrefix=/com/tinubu/commons/ports/document/classpath' parameter must start but not be equal to URI '/com/tinubu/commons/ports/document/classpath' path");
      }

      @Test
      public void testSupportsDocumentUriWhenBadPrefix() {
         assertThat(documentRepository().supportsUri(documentUri("classpath:/bad/path/test.txt"))).isFalse();
      }

      @Test
      public void testSupportsDocumentRepositoryUriWhenAlternativeForms() {
         assertThat(documentRepository().supportsUri(documentUri(
               "classpath:///com/tinubu/commons/ports/document/classpath/path/test.txt"))).isFalse();
         assertThat(documentRepository().supportsUri(documentUri(
               "classpath:com/tinubu/commons/ports/document/classpath/path/test.txt"))).isFalse();
         assertThat(documentRepository().supportsUri(documentUri(
               "classpath://host/com/tinubu/commons/ports/document/classpath/path/test.txt"))).isFalse();
         assertThatThrownBy(() -> documentRepository().supportsUri(documentUri(
               "classpath:/com/tinubu/commons/ports/document/classpath/path/test.txt#fragment")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessageContainingAll(
                     "Invalid 'classpath:/com/tinubu/commons/ports/document/classpath/path/test.txt#fragment' URI",
                     "must not have a fragment");
         assertThat(documentRepository().supportsUri(documentUri(
               "classpath:/com/tinubu/commons/ports/document/classpath/path/test.txt?query"))).isTrue();
      }

      @Test
      public void testSupportsRepositoryUriWhenNominal() {
         assertThat(documentRepository().supportsUri(repositoryUri(
               "classpath:/com/tinubu/commons/ports/document/classpath"))).isTrue();
      }

      @Test
      public void testSupportsRepositoryUriWhenBadPrefix() {
         assertThat(documentRepository().supportsUri(repositoryUri("classpath:/bad"))).isFalse();
      }

      @Test
      public void testToDocumentUriWhenNominal() {
         assertThat(documentRepository().toUri(DocumentPath.of("file.txt")).stringValue()).isEqualTo(
               "classpath:/com/tinubu/commons/ports/document/classpath/file.txt");
         assertThat(documentRepository().toUri(DocumentPath.of("path/file.txt")).stringValue()).isEqualTo(
               "classpath:/com/tinubu/commons/ports/document/classpath/path/file.txt");
      }

      @Test
      public void testToDocumentUriWhenNoParameters() {
         assertThat(documentRepository()
                          .toUri(DocumentPath.of("file.txt"), DefaultExportUriOptions.noParameters())
                          .stringValue()).isEqualTo(
               "classpath:/com/tinubu/commons/ports/document/classpath/file.txt");
         assertThat(documentRepository()
                          .toUri(DocumentPath.of("path/file.txt"), DefaultExportUriOptions.noParameters())
                          .stringValue()).isEqualTo(
               "classpath:/com/tinubu/commons/ports/document/classpath/path/file.txt");
      }

      @Test
      public void testToDocumentUriWhenDefaultParameters() {
         assertThat(documentRepository()
                          .toUri(DocumentPath.of("file.txt"), DefaultExportUriOptions.defaultParameters())
                          .stringValue()).isEqualTo(
               "classpath:/com/tinubu/commons/ports/document/classpath/file.txt");
         assertThat(documentRepository()
                          .toUri(DocumentPath.of("path/file.txt"),
                                 DefaultExportUriOptions.defaultParameters())
                          .stringValue()).isEqualTo(
               "classpath:/com/tinubu/commons/ports/document/classpath/path/file.txt?classpathPrefix=/com/tinubu/commons/ports/document/classpath");
      }

      @Test
      public void testToDocumentUriWhenSensitiveParameters() {
         assertThat(documentRepository()
                          .toUri(DocumentPath.of("file.txt"), DefaultExportUriOptions.sensitiveParameters())
                          .stringValue()).isEqualTo(
               "classpath:/com/tinubu/commons/ports/document/classpath/file.txt");
         assertThat(documentRepository()
                          .toUri(DocumentPath.of("path/file.txt"),
                                 DefaultExportUriOptions.sensitiveParameters())
                          .stringValue()).isEqualTo(
               "classpath:/com/tinubu/commons/ports/document/classpath/path/file.txt?classpathPrefix=/com/tinubu/commons/ports/document/classpath");
      }

      @Test
      public void testToRepositoryUriWhenNominal() {
         assertThat(documentRepository().toUri().stringValue()).isEqualTo(
               "classpath:/com/tinubu/commons/ports/document/classpath");
      }

      @Test
      public void testToRepositoryUriWhenNoParameters() {
         assertThat(documentRepository()
                          .toUri(DefaultExportUriOptions.noParameters())
                          .stringValue()).isEqualTo("classpath:/com/tinubu/commons/ports/document/classpath");
      }

      @Test
      public void testToRepositoryUriWhenDefaultParameters() {
         assertThat(documentRepository()
                          .toUri(DefaultExportUriOptions.defaultParameters())
                          .stringValue()).isEqualTo("classpath:/com/tinubu/commons/ports/document/classpath");
      }

      @Test
      public void testToRepositoryUriWhenSensitiveParameters() {
         assertThat(documentRepository()
                          .toUri(DefaultExportUriOptions.sensitiveParameters())
                          .stringValue()).isEqualTo("classpath:/com/tinubu/commons/ports/document/classpath");
      }

      @Test
      public void testFindDocumentByUriWhenNominal() {
         DocumentPath documentPath = DocumentPath.of("path/pathfile2.txt");

         try {
            createDocuments(documentPath);

            assertThat(documentRepository().findDocumentByUri(documentUri(
                  "classpath:/com/tinubu/commons/ports/document/classpath/path/pathfile2.txt"))).hasValueSatisfying(
                  document -> {
                     assertThat(document.documentId()).isEqualTo(DocumentPath.of("path/pathfile2.txt"));
                     assertThat(document.metadata().documentPath()).isEqualTo(Path.of("path/pathfile2.txt"));
                  });

         } finally {
            deleteDocuments(documentPath);
         }
      }

      @Test
      public void testFindDocumentByUriWhenRepositoryUri() {
         assertThatThrownBy(() -> documentRepository().findDocumentByUri(documentUri(
               "classpath:/com/tinubu/commons/ports/document/classpath")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'classpath:/com/tinubu/commons/ports/document/classpath' URI: 'classpathPrefix=/com/tinubu/commons/ports/document/classpath' parameter must start but not be equal to URI '/com/tinubu/commons/ports/document/classpath' path");
      }

      @Test
      public void testFindDocumentByUriWhenUnsupportedUri() {
         assertThatThrownBy(() -> documentRepository().findDocumentByUri(documentUri(
               "unknown:/com/tinubu/commons/ports/document/classpath/document")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessageContaining(
                     "'documentUri=DefaultDocumentUri[value=unknown:/com/tinubu/commons/ports/document/classpath/document]' must be supported");
         assertThatThrownBy(() -> documentRepository().findDocumentByUri(documentUri(
               "classpath:/otherpath/document")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessageContaining(
                     "'documentUri=DefaultDocumentUri[value=classpath:/otherpath/document]' must be supported");
      }

   }
}