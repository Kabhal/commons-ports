/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.classpath;

import static com.tinubu.commons.ddd2.test.uri.UriAssert.assertThat;
import static com.tinubu.commons.ddd2.uri.DefaultUriRestrictions.ofRestrictions;
import static com.tinubu.commons.ddd2.uri.Uri.uri;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.net.URI;
import java.nio.file.Path;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.uri.Uri;

public class ClasspathUriTest {

   static {
      Assertions.setMaxStackTraceElementsDisplayed(50);
   }

   @Test
   public void classpathUriWhenBadArguments() {
      assertThatThrownBy(() -> ClasspathUri.ofUri((String) null))
            .isInstanceOf(InvariantValidationException.class)
            .hasMessage("Invariant validation error > 'uri' must not be null");
      assertThatThrownBy(() -> ClasspathUri.ofUri((URI) null))
            .isInstanceOf(InvariantValidationException.class)
            .hasMessage("Invariant validation error > 'uri' must not be null");
      assertThatThrownBy(() -> ClasspathUri.ofUri((Uri) null))
            .isInstanceOf(InvariantValidationException.class)
            .hasMessage("Invariant validation error > 'uri' must not be null");
   }

   @Nested
   class OfUri {

      @Test
      public void classpathUriWhenNominal() {
         assertThat(ClasspathUri.ofUri("classpath:/path")).satisfies(uri -> {
            assertThat(uri).hasPath(false, "/path");
            Assertions.assertThat(uri.toPath()).isEqualTo(Path.of("/path"));
         });
      }

      @Test
      public void classpathUriWhenQuery() {
         assertThatThrownBy(() -> ClasspathUri.ofUri("classpath:/path?query"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'classpath:/path?query' URI: 'ClasspathUri[value=classpath:/path?query,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=true, fragment=true],scheme=SimpleScheme[scheme='classpath'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='/path?query', encodedSchemeSpecific='/path?query'],path=SimplePath[path=/path, encodedPath=/path],query=SimpleQuery[query=query, encodedQuery=query],fragment=<null>]' must not have a query");
         assertThat(ClasspathUri.ofUri("classpath:/path?query",
                                       ofRestrictions(false, false))).satisfies(uri -> {
            assertThat(uri).hasPath(false, "/path");
            assertThat(uri).hasQuery(false, "query");
         });
      }

      @Test
      public void classpathUriWhenFragment() {
         assertThatThrownBy(() -> ClasspathUri.ofUri("classpath:/path#fragment"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'classpath:/path#fragment' URI: 'ClasspathUri[value=classpath:/path#fragment,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=true, fragment=true],scheme=SimpleScheme[scheme='classpath'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='/path', encodedSchemeSpecific='/path'],path=SimplePath[path=/path, encodedPath=/path],query=<null>,fragment=SimpleFragment[fragment=fragment, encodedFragment=fragment]]' must not have a fragment");
         assertThat(ClasspathUri.ofUri("classpath:/path#fragment", ofRestrictions(false, false))).satisfies(
               uri -> {
                  assertThat(uri).hasPath(false, "/path");
                  assertThat(uri).hasFragment(false, "fragment");
               });
      }
   }

   @Test
   public void classpathUriWhenIncompatibleUri() {
      assertThatThrownBy(() -> ClasspathUri.ofUri("other:/"))
            .isInstanceOf(IncompatibleUriException.class)
            .hasMessageContainingAll(
                  "Incompatible 'other:/' URI: 'ClasspathUri[value=other:/,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=true, fragment=true],scheme=SimpleScheme[scheme='other'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='/', encodedSchemeSpecific='/'],path=SimplePath[path=/, encodedPath=/],query=<null>,fragment=<null>]' must be absolute, and scheme must be equal to 'classpath'");
      assertThatThrownBy(() -> ClasspathUri.ofUri("classpath://host/"))
            .isInstanceOf(IncompatibleUriException.class)
            .hasMessageContainingAll(
                  "Incompatible 'classpath://host/' URI: 'ClasspathUri[value=classpath://host/,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=true, fragment=true],scheme=SimpleScheme[scheme='classpath'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='//host/', encodedSchemeSpecific='//host/'],path=SimplePath[path=/, encodedPath=/],query=<null>,fragment=<null>]' must not have authority");
      assertThatThrownBy(() -> ClasspathUri.ofUri("classpath://host"))
            .isInstanceOf(IncompatibleUriException.class)
            .hasMessageContainingAll(
                  "Incompatible 'classpath://host' URI: 'ClasspathUri[value=classpath://host,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=true, fragment=true],scheme=SimpleScheme[scheme='classpath'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='//host', encodedSchemeSpecific='//host'],path=SimplePath[path=, encodedPath=],query=<null>,fragment=<null>]' must not have authority");
      assertThatThrownBy(() -> ClasspathUri.ofUri("/"))
            .isInstanceOf(IncompatibleUriException.class)
            .hasMessageContainingAll(
                  "Incompatible '/' URI: 'ClasspathUri[value=/,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=true, fragment=true],scheme=<null>,schemeSpecific=<null>,path=SimplePath[path=/, encodedPath=/],query=<null>,fragment=<null>]' must be absolute");
      assertThatThrownBy(() -> ClasspathUri.ofUri("//host/path"))
            .isInstanceOf(IncompatibleUriException.class)
            .hasMessageContainingAll(
                  "Incompatible '//host/path' URI: 'ClasspathUri[value=//host/path,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=true, fragment=true],scheme=<null>,schemeSpecific=<null>,path=SimplePath[path=/path, encodedPath=/path],query=<null>,fragment=<null>]' must be absolute");
      assertThatThrownBy(() -> ClasspathUri.ofUri("//host"))
            .isInstanceOf(IncompatibleUriException.class)
            .hasMessageContainingAll(
                  "Incompatible '//host' URI: 'ClasspathUri[value=//host,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=true, fragment=true],scheme=<null>,schemeSpecific=<null>,path=SimplePath[path=, encodedPath=],query=<null>,fragment=<null>]' must be absolute");
   }

   @Test
   public void classpathUriWhenInvalidUri() {
      assertThatThrownBy(() -> ClasspathUri.ofUri("classpath:"))
            .isInstanceOf(InvalidUriException.class)
            .hasMessage("Invalid 'classpath:' URI: Expected scheme-specific part at index 10: classpath:");
      assertThatThrownBy(() -> ClasspathUri.ofUri("classpath://"))
            .isInstanceOf(InvalidUriException.class)
            .hasMessage("Invalid 'classpath://' URI: Expected authority at index 12: classpath://");
      assertThatThrownBy(() -> ClasspathUri.ofUri("//"))
            .isInstanceOf(InvalidUriException.class)
            .hasMessage("Invalid '//' URI: Expected authority at index 2: //");
   }

   @Nested
   class NormalizeTest {

      @Test
      public void normalizeWhenNominal() {
         assertThat(ClasspathUri.ofUri("classpath:/path/../.").normalize()).satisfies(uri -> {
            assertThat(uri).hasPath(false, "/");
            assertThat(uri).hasNoQuery();
         });
      }

      @Test
      public void normalizeWhenQuery() {
         assertThat(ClasspathUri.ofUri("classpath:/path/../.?query", ofRestrictions(false, false))
                          .normalize()).satisfies(uri -> {
            assertThat(uri).hasPath(false, "/");
            assertThat(uri).hasQuery(false, "query");
         });
      }

   }

   @Nested
   class RelativizeTest {

      @Test
      public void relativizeWhenNominal() {
         assertThatThrownBy(() -> ClasspathUri
               .ofUri("classpath:/")
               .relativize(uri("classpath:/"))).isInstanceOf(UnsupportedOperationException.class);
      }

   }
}