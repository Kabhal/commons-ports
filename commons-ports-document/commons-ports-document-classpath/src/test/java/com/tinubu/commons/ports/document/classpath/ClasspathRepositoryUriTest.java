/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.classpath;

import static com.tinubu.commons.ddd2.uri.DefaultUriRestrictions.noRestrictions;
import static com.tinubu.commons.ddd2.uri.Uri.uri;
import static com.tinubu.commons.lang.util.CheckedPredicate.alwaysTrue;
import static com.tinubu.commons.ports.document.classpath.ClasspathRepositoryUri.Parameters.CLASSPATH_PREFIX;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.defaultParameters;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.noParameters;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.sensitiveParameters;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.net.URI;
import java.nio.file.Path;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.RepositoryUri;

public class ClasspathRepositoryUriTest {

   @Nested
   public class OfUriWhenURI {

      @Test
      public void ofUriWhenNominal() {
         assertThat(ClasspathRepositoryUri.ofRepositoryUri("classpath:/")).satisfies(uri -> {
            assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/"));
         });
         assertThat(ClasspathRepositoryUri.ofRepositoryUri("classpath:/")).satisfies(uri -> {
            assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/"));
         });
      }

      @Test
      public void ofUriWhenUri() {
         assertThat(ClasspathRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(Uri.ofUri(
               "classpath:/path?classpathPrefix=/path")))).satisfies(uri -> {
            assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path"));
         });
         assertThat(ClasspathRepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri(Uri.ofUri(
               "classpath:/path/document?classpathPrefix=/path")))).satisfies(uri -> {
            assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path"));
         });
         assertThat(ClasspathRepositoryUri.ofRepositoryUri(Uri.ofUri("classpath:/path?classpathPrefix=/path"))).satisfies(
               uri -> {
                  assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path"));
               });
      }

      @Test
      public void ofUriWhenClasspathUri() {
         assertThat(ClasspathRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(ClasspathUri.ofUri(
               "classpath:/path?classpathPrefix=/path",
               noRestrictions())))).satisfies(uri -> {
            assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path"));
         });
         assertThat(ClasspathRepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri(ClasspathUri.ofUri(
               "classpath:/path/document?classpathPrefix=/path",
               noRestrictions())))).satisfies(uri -> {
            assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path"));
         });
         assertThat(ClasspathRepositoryUri.ofRepositoryUri(ClasspathUri.ofUri(
               "classpath:/path?classpathPrefix=/path",
               noRestrictions()))).satisfies(uri -> {
            assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path"));
         });
      }

      @Test
      public void ofUriWhenBadParameters() {
         assertThatThrownBy(() -> ClasspathRepositoryUri.ofRepositoryUri((URI) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
         assertThatThrownBy(() -> ClasspathRepositoryUri.ofRepositoryUri((String) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
         assertThatThrownBy(() -> ClasspathRepositoryUri.ofRepositoryUri((RepositoryUri) null, Path.of("")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
         assertThatThrownBy(() -> ClasspathRepositoryUri.ofRepositoryUri(ClasspathRepositoryUri.ofRepositoryUri(
               "classpath:/"), null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultClasspathPrefix' must not be null");
      }

      @Test
      @Disabled(
            "There's no way to create an URI with an invalid Path, the only way is tu use \u0000 but it also fails the URI preemptively")
      public void ofUriWhenIllegalArgument() {
         assertThatExceptionOfType(InvalidUriException.class)
               .isThrownBy(() -> ClasspathRepositoryUri.ofRepositoryUri("classpath:/?classpathPrefix=:"))
               .withMessage(
                     "Invalid '%s' URI: Conversion from 'java.lang.String' to 'java.util.Path' failed for ':' value",
                     "classpath:/?classpathPrefix=:");
      }

      @Test
      public void ofUriWhenCaseSensitiveParameterKey() {
         assertThat(ClasspathRepositoryUri.ofRepositoryUri("classpath:/path?ClAsSpAtHPrEfiX=/")).satisfies(uri -> {
            assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path"));
         });
      }

      @Test
      public void ofUriWhenNormalized() {
         assertThat(ClasspathRepositoryUri.ofRepositoryUri(uri("classpath:/path/../."))).satisfies(uri -> {
            assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/"));
         });
         assertThat(ClasspathRepositoryUri.ofRepositoryUri("classpath:/path/../.")).satisfies(uri -> {
            assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/"));
         });
         assertThat(ClasspathRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
               "classpath:/path/../."), Path.of("/"))).satisfies(uri -> {
            assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/"));
         });
         assertThatThrownBy(() -> ClasspathRepositoryUri.ofRepositoryUri(uri("classpath:/../path")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessageContainingAll("Invalid 'classpath:/../path' URI", "must not have traversal paths");
      }

      @Test
      public void ofUriWhenInvalidSyntax() {
         assertThatThrownBy(() -> ClasspathRepositoryUri.ofRepositoryUri("git@git.com:user/repo.git"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'git@git.com:user/repo.git' URI: Illegal character in scheme name at index 3: git@git.com:user/repo.git");
         assertThatThrownBy(() -> ClasspathRepositoryUri.ofRepositoryUri("git@git.com:user/repo.git"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'git@git.com:user/repo.git' URI: Illegal character in scheme name at index 3: git@git.com:user/repo.git");
      }

      @Test
      public void ofUriWhenIncompatibleUri() {
         assertThatThrownBy(() -> ClasspathRepositoryUri.ofRepositoryUri("unknown:/path"))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessageContainingAll("Incompatible 'unknown:/path' URI",
                                        "must be absolute, and scheme must be equal to 'classpath'");
         assertThatThrownBy(() -> ClasspathRepositoryUri.ofRepositoryUri(uri("classpath://user:password@host")))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessageContainingAll("Incompatible 'classpath://user:password@host' URI",
                                        "must not have authority");
         assertThatThrownBy(() -> ClasspathRepositoryUri.ofRepositoryUri(uri("classpath:///path")))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessageContainingAll("Incompatible 'classpath:///path' URI", "must not have authority");
         assertThatThrownBy(() -> ClasspathRepositoryUri.ofRepositoryUri("classpath:path"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessageContainingAll("Incompatible 'classpath:path' URI", "must be hierarchical");
      }

      @Test
      public void ofUriWhenInvalidUri() {
         assertThatThrownBy(() -> ClasspathRepositoryUri.ofRepositoryUri("classpath:/path#fragment"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessageContainingAll("Invalid 'classpath:/path#fragment' URI", "must not have a fragment");
      }

      @Test
      public void ofUriWhenClasspathPrefix() {
         assertThat(ClasspathRepositoryUri.ofRepositoryUri(uri("classpath:/path"))).satisfies(uri -> {
            assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path"));
         });
         assertThat(ClasspathRepositoryUri.ofRepositoryUri("classpath:/path")).satisfies(uri -> {
            assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path"));
         });
      }

      @Test
      public void ofUriWhenInvalidClasspathPrefixParameter() {
         assertThatThrownBy(() -> ClasspathRepositoryUri.ofRepositoryUri(
               "classpath:/path/subpath?classpathPrefix=path/subpath"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid '%s' URI: 'classpathPrefix=path/subpath' parameter must be absolute and must not contain traversal",
                     "classpath:/path/subpath?classpathPrefix=path/subpath");
         assertThatThrownBy(() -> ClasspathRepositoryUri.ofRepositoryUri(
               "classpath:/path/subpath?classpathPrefix=../path"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid '%s' URI: 'classpathPrefix=../path' parameter must be absolute and must not contain traversal",
                     "classpath:/path/subpath?classpathPrefix=../path");
         assertThatThrownBy(() -> ClasspathRepositoryUri.ofRepositoryUri(
               "classpath:/path/subpath?classpathPrefix=/path/subpath/.."))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid '%s' URI: 'classpathPrefix=/path' parameter must match URI '/path/subpath' path",
                     "classpath:/path/subpath?classpathPrefix=/path/subpath/..");
      }

      @Test
      public void ofUriWhenExactClasspathPrefixParameter() {
         assertThatThrownBy(() -> ClasspathRepositoryUri.ofRepositoryUri(
               "classpath:/?classpathPrefix=/path/subpath"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'classpath:/?classpathPrefix=/path/subpath' URI: 'classpathPrefix=/path/subpath' parameter must match URI '/' path");
         assertThat(ClasspathRepositoryUri.ofRepositoryUri(
               "classpath:/path/subpath?classpathPrefix=/path/subpath")).satisfies(uri -> {
            assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path/subpath"));
         });
         assertThat(ClasspathRepositoryUri.ofRepositoryUri(
               "classpath:/path/subpath?classpathPrefix=/path/./subpath/.")).satisfies(uri -> {
            assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path/subpath"));
         });
      }

      @Test
      public void ofUriWhenPartialClasspathPrefixParameter() {
         assertThatThrownBy(() -> ClasspathRepositoryUri.ofRepositoryUri("classpath:/?classpathPrefix=/path"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'classpath:/?classpathPrefix=/path' URI: 'classpathPrefix=/path' parameter must match URI '/' path");
         assertThatThrownBy(() -> ClasspathRepositoryUri.ofRepositoryUri(
               "classpath:/path/subpath?classpathPrefix=/path"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid '%s' URI: 'classpathPrefix=/path' parameter must match URI '/path/subpath' path",
                     "classpath:/path/subpath?classpathPrefix=/path");
         assertThatThrownBy(() -> ClasspathRepositoryUri.ofRepositoryUri(
               "classpath:/path/subpath?classpathPrefix=/path/."))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid '%s' URI: 'classpathPrefix=/path' parameter must match URI '/path/subpath' path",
                     "classpath:/path/subpath?classpathPrefix=/path/.");
      }

      @Test
      public void ofUriWhenMismatchingClasspathPrefixParameter() {
         assertThatThrownBy(() -> ClasspathRepositoryUri.ofRepositoryUri(
               "classpath:/path/subpath?classpathPrefix=/otherpath"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid '%s' URI: 'classpathPrefix=/otherpath' parameter must match URI '/path/subpath' path",
                     "classpath:/path/subpath?classpathPrefix=/otherpath");
         assertThatThrownBy(() -> ClasspathRepositoryUri.ofRepositoryUri(
               "classpath:/path/subpath?classpathPrefix=/path/subpath/otherpath"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid '%s' URI: 'classpathPrefix=/path/subpath/otherpath' parameter must match URI '/path/subpath' path",
                     "classpath:/path/subpath?classpathPrefix=/path/subpath/otherpath");
      }

      @Test
      public void ofUriWhenDefaultParameters() {
         assertThat(ClasspathRepositoryUri.ofRepositoryUri("classpath:/")).satisfies(uri -> {
            assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/"));
            assertThat(uri.query().parameter(CLASSPATH_PREFIX, alwaysTrue())).isEmpty();
         });
      }

      @Test
      public void ofUriWhenAllParameters() {
         assertThat(ClasspathRepositoryUri.ofRepositoryUri("classpath:/path"
                                                           + "?classpathPrefix=/path")).satisfies(uri -> {
            assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path"));
            assertThat(uri.query().parameter(CLASSPATH_PREFIX, alwaysTrue())).hasValue(Path.of("/path"));
         });
      }

      @Nested
      class WrappedUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(ClasspathRepositoryUri.ofRepositoryUri(uri("document:classpath:classpath:/path"))).satisfies(
                  uri -> {
                     assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path"));
                  });
            assertThat(ClasspathRepositoryUri.ofRepositoryUri(uri(
                  "document:classpath:classpath:/path/sub/../."))).satisfies(uri -> {
               assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path"));
            });
         }

         @Test
         public void ofUriWhenBadRepositoryType() {
            assertThatThrownBy(() -> ClasspathRepositoryUri.ofRepositoryUri(uri("document:unknown:classpath:/")))
                  .isInstanceOf(IncompatibleUriException.class)
                  .hasMessage(
                        "Incompatible 'document:unknown:classpath:/' URI: Unsupported 'unknown' repository type");
         }

         @Test
         public void ofUriWhenNormalized() {
            assertThat(ClasspathRepositoryUri.ofRepositoryUri(uri("document:classpath:classpath:/path/../."))).satisfies(
                  uri -> {
                     assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/"));
                  });
            assertThat(ClasspathRepositoryUri.ofRepositoryUri("document:classpath:classpath:/path/../.")).satisfies(
                  repositoryUri -> {
                     assertThat(repositoryUri.classpathPrefix()).isEqualTo(Path.of("/"));
                  });
            assertThat(ClasspathRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
                  "document:classpath:classpath:/path/../."), Path.of("/"))).satisfies(uri -> {
               assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/"));
            });
            assertThatThrownBy(() -> ClasspathRepositoryUri.ofRepositoryUri(uri(
                  "document:classpath:classpath:/../path")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessageContainingAll("Invalid 'classpath:/../path' URI",
                                           "must not have traversal paths");
         }

         @Test
         public void ofUriWhenUnsupportedWrappedUri() {
            assertThatThrownBy(() -> ClasspathRepositoryUri.ofRepositoryUri(uri(
                  "document:classpath:unknown://host/path")))
                  .isInstanceOf(IncompatibleUriException.class)
                  .hasMessageContainingAll("Incompatible 'unknown://host/path' URI",
                                           "must be absolute, and scheme must be equal to 'classpath'");
         }

      }
   }

   @Nested
   public class OfUriWhenRepositoryUri {

      @Test
      public void ofUriWhenBadDefaultClasspathPrefix() {
         assertThatThrownBy(() -> ClasspathRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
               "classpath:/path"), null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultClasspathPrefix' must not be null");
         assertThatThrownBy(() -> ClasspathRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
               "classpath:/path"), Path.of("")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultClasspathPrefix=' must be absolute path");
         assertThatThrownBy(() -> ClasspathRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
               "classpath:/path"), Path.of("path")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultClasspathPrefix=path' must be absolute path");
         assertThat(ClasspathRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri("classpath:/path"),
                                                           Path.of("/../path")))
               .as("defaultClasspathPrefix is normalized to '/path', so that it doesn't fail")
               .isNotNull();
      }

      @Nested
      public class WhenRepositoryUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(ClasspathRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri("classpath:/path"),
                                                              Path.of("/path"))).satisfies(uri -> {
               assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path"));
            });
         }

         @Test
         public void ofUriWhenImplicitDefaultClasspathPrefix() {
            assertThat(ClasspathRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
                  "classpath:/path/subpath"))).satisfies(uri -> {
               assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path/subpath"));
            });
         }

         @Test
         public void ofUriWhenDefaultClasspathPrefix() {
            assertThat(ClasspathRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
                  "classpath:/path/subpath"), Path.of("/path/subpath"))).satisfies(uri -> {
               assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path/subpath"));
            });
         }

         @Test
         public void ofUriWhenMismatchingDefaultClasspathPrefix() {
            assertThatThrownBy(() -> ClasspathRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
                  "classpath:/path/subpath"), Path.of("/path")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'classpath:/path/subpath' URI: 'classpathPrefix=/path' parameter must match URI '/path/subpath' path");
            assertThatThrownBy(() -> ClasspathRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
                  "classpath:/path/subpath"), Path.of("/")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'classpath:/path/subpath' URI: 'classpathPrefix=/' parameter must match URI '/path/subpath' path");
         }

         @Test
         public void ofUriWhenRoot() {
            assertThat(ClasspathRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri("classpath:/"))).satisfies(
                  uri -> {
                     assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/"));
                  });
            assertThat(ClasspathRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri("classpath:/"),
                                                              Path.of("/"))).satisfies(uri -> {
               assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/"));
            });
         }

         @Test
         public void ofUriWhenExplicitClasspathPrefix() {
            assertThat(ClasspathRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
                  "classpath:/path/subpath?classpathPrefix=/path/subpath"), Path.of("/"))).satisfies(uri -> {
               assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path/subpath"));
            });
         }

         @Test
         public void ofUriWhenMismatchingExplicitClasspathPrefix() {
            assertThatThrownBy(() -> ClasspathRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
                  "classpath:/path/subpath?classpathPrefix=/path")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'classpath:/path/subpath?classpathPrefix=/path' URI: 'classpathPrefix=/path' parameter must match URI '/path/subpath' path");
            assertThatThrownBy(() -> ClasspathRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
                  "classpath:/path/subpath?classpathPrefix=/")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'classpath:/path/subpath?classpathPrefix=/' URI: 'classpathPrefix=/' parameter must match URI '/path/subpath' path");
            assertThatThrownBy(() -> ClasspathRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
                  "classpath:/path/subpath?classpathPrefix=/subpath")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'classpath:/path/subpath?classpathPrefix=/subpath' URI: 'classpathPrefix=/subpath' parameter must match URI '/path/subpath' path");
         }

      }

      @Nested
      public class WhenDocumentUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(ClasspathRepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri(
                  "classpath:/path/document"), Path.of("/path"))).satisfies(uri -> {
               assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path"));
            });
         }

         @Test
         public void ofUriWhenImplicitDefaultClasspathPrefix() {
            assertThat(ClasspathRepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri(
                  "classpath:/path/subpath/document"))).satisfies(uri -> {
               assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path/subpath"));
            });
         }

         @Test
         public void ofUriWhenDefaultClasspathPrefix() {
            assertThat(ClasspathRepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri(
                  "classpath:/path/subpath/document"), Path.of("/path/subpath"))).satisfies(uri -> {
               assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path/subpath"));
            });
            assertThat(ClasspathRepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri(
                  "classpath:/path/subpath/document"), Path.of("/path"))).satisfies(uri -> {
               assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path"));
            });
            assertThat(ClasspathRepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri(
                  "classpath:/path/subpath/document"), Path.of("/"))).satisfies(uri -> {
               assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/"));
            });
         }

         @Test
         public void ofUriWhenRoot() {
            assertThat(ClasspathRepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri("classpath:/document"))).satisfies(
                  uri -> {
                     assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/"));
                  });
            assertThat(ClasspathRepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri("classpath:/document"),
                                                              Path.of("/"))).satisfies(uri -> {
               assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/"));
            });
         }

         @Test
         public void ofUriWhenExplicitClasspathPrefix() {
            assertThat(ClasspathRepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri(
                  "classpath:/path/subpath/document?classpathPrefix=/path/subpath"))).satisfies(uri -> {
               assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path/subpath"));
            });
         }

      }

      @Nested
      public class WhenClasspathRepositoryUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(ClasspathRepositoryUri.ofRepositoryUri(ClasspathRepositoryUri.ofRepositoryUri(
                  "classpath:/path"), Path.of("/path"))).satisfies(uri -> {
               assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path"));
            });
         }

         @Test
         public void ofUriWhenImplicitDefaultClasspathPrefix() {
            assertThat(ClasspathRepositoryUri.ofRepositoryUri(ClasspathRepositoryUri.ofRepositoryUri(
                  "classpath:/path/subpath"))).satisfies(uri -> {
               assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path/subpath"));
            });
         }

         @Test
         public void ofUriWhenDefaultClasspathPrefix() {
            assertThat(ClasspathRepositoryUri.ofRepositoryUri(ClasspathRepositoryUri.ofRepositoryUri(
                  "classpath:/path/subpath"), Path.of("/path"))).satisfies(uri -> {
               assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path/subpath"));
            });
            assertThat(ClasspathRepositoryUri.ofRepositoryUri(ClasspathRepositoryUri.ofRepositoryUri(
                  "classpath:/path/subpath"), Path.of("/"))).satisfies(uri -> {
               assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path/subpath"));
            });
         }

         @Test
         public void ofUriWhenRoot() {
            assertThat(ClasspathRepositoryUri.ofRepositoryUri(ClasspathRepositoryUri.ofRepositoryUri(
                  "classpath:/"))).satisfies(uri -> {
               assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/"));
            });
            assertThat(ClasspathRepositoryUri.ofRepositoryUri(ClasspathRepositoryUri.ofRepositoryUri(
                  "classpath:/"), Path.of("/"))).satisfies(uri -> {
               assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/"));
            });
         }

         @Test
         public void ofUriWhenExplicitClasspathPrefix() {
            assertThat(ClasspathRepositoryUri.ofRepositoryUri(ClasspathRepositoryUri.ofRepositoryUri(
                  "classpath:/path/subpath?classpathPrefix=/path/subpath"), Path.of("/"))).satisfies(uri -> {
               assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path/subpath"));
            });
         }

      }

      @Nested
      public class WhenClasspathDocumentUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(ClasspathRepositoryUri.ofRepositoryUri(ClasspathDocumentUri.ofDocumentUri(
                  "classpath:/path/document"), Path.of("/path"))).satisfies(uri -> {
               assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path"));
            });
         }

         @Test
         public void ofUriWhenImplicitDefaultClasspathPrefix() {
            assertThat(ClasspathRepositoryUri.ofRepositoryUri(ClasspathDocumentUri.ofDocumentUri(
                  "classpath:/path/subpath/document"))).satisfies(uri -> {
               assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path/subpath"));
            });
         }

         @Test
         public void ofUriWhenDefaultClasspathPrefix() {
            assertThat(ClasspathRepositoryUri.ofRepositoryUri(ClasspathDocumentUri.ofDocumentUri(
                  "classpath:/path/subpath/document"), Path.of("/path/subpath"))).satisfies(uri -> {
               assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path/subpath"));
            });
            assertThat(ClasspathRepositoryUri.ofRepositoryUri(ClasspathDocumentUri.ofDocumentUri(
                  "classpath:/path/subpath/document"), Path.of("/path"))).satisfies(uri -> {
               assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path/subpath"));
            });
            assertThat(ClasspathRepositoryUri.ofRepositoryUri(ClasspathDocumentUri.ofDocumentUri(
                  "classpath:/path/subpath/document"), Path.of("/"))).satisfies(uri -> {
               assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path/subpath"));
            });
         }

         @Test
         public void ofUriWhenRoot() {
            assertThat(ClasspathRepositoryUri.ofRepositoryUri(ClasspathDocumentUri.ofDocumentUri(
                  "classpath:/document"))).satisfies(uri -> {
               assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/"));
            });
            assertThat(ClasspathRepositoryUri.ofRepositoryUri(ClasspathDocumentUri.ofDocumentUri(
                  "classpath:/document"), Path.of("/"))).satisfies(uri -> {
               assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/"));
            });
         }

         @Test
         public void ofUriWhenExplicitClasspathPrefix() {
            assertThat(ClasspathRepositoryUri.ofRepositoryUri(ClasspathDocumentUri.ofDocumentUri(
                  "classpath:/path/subpath/document?classpathPrefix=/path/subpath"))).satisfies(uri -> {
               assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path/subpath"));
            });
         }

      }

   }

   @Nested
   public class OfConfig {

      @Test
      public void ofConfigWhenNominal() {
         assertThat(ClasspathRepositoryUri.ofConfig(Path.of("/path"))).satisfies(uri -> {
            assertThat(uri.classpathPrefix()).isEqualTo(Path.of("/path"));

            assertThat(uri.query().parameter(CLASSPATH_PREFIX, alwaysTrue())).hasValue(Path.of("/path"));
         });
      }

      @Test
      public void ofConfigWhenBadConfiguration() {
         assertThatThrownBy(() -> ClasspathRepositoryUri.ofConfig(Path.of("")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'classpathPrefix=' must be absolute path");
         assertThatThrownBy(() -> ClasspathRepositoryUri.ofConfig(Path.of("path")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'classpathPrefix=path' must be absolute path");
         assertThat(ClasspathRepositoryUri.ofConfig(Path.of("/../path")))
               .as("classpathPrefix is normalized to /path, so it doesn't fail")
               .isNotNull();
      }

   }

   @Nested
   public class ToConfig {

      @Test
      public void toConfigWhenNominal() {
         assertThat(ClasspathRepositoryUri
                          .ofRepositoryUri("classpath:/path")
                          .classpathPrefix()).satisfies(cp -> {
            assertThat(cp).isEqualTo(Path.of("/path"));
         });
      }

   }

   @Nested
   public class ToUri {

      @Test
      public void toUriWhenNominal() {
         assertThat(ClasspathRepositoryUri
                          .ofRepositoryUri("classpath:/")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("classpath:/");
      }

      @Test
      public void toUriWhenClasspathPrefix() {
         assertThat(ClasspathRepositoryUri
                          .ofRepositoryUri("classpath:/path")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("classpath:/path");
      }

      @Test
      public void toUriWhenDefaultParameters() {
         assertThat(ClasspathRepositoryUri
                          .ofRepositoryUri("classpath:/path")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("classpath:/path");
         assertThat(ClasspathRepositoryUri
                          .ofRepositoryUri("classpath:/path?classpathPrefix=/path")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("classpath:/path");
      }

      @Test
      public void toUriWhenDefaultParametersWithForceDefaultValues() {
         assertThat(ClasspathRepositoryUri
                          .ofRepositoryUri("classpath:/path")
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo("classpath:/path?classpathPrefix=/path");
         assertThat(ClasspathRepositoryUri
                          .ofRepositoryUri("classpath:/path?classpathPrefix=/path")
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo("classpath:/path?classpathPrefix=/path");
         assertThat(ClasspathRepositoryUri
                          .ofRepositoryUri("classpath:/?classpathPrefix=/")
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo("classpath:/?classpathPrefix=/");

         assertThat(ClasspathRepositoryUri
                          .ofConfig(Path.of("/path"))
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo("classpath:/path?classpathPrefix=/path");
      }

      @Test
      public void toUriWhenSensitiveParameters() {
         assertThat(ClasspathRepositoryUri.ofRepositoryUri("classpath:/path").exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("classpath:/path");
         assertThat(ClasspathRepositoryUri
                          .ofRepositoryUri("classpath:/path?classpathPrefix=/path")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("classpath:/path");
      }

   }

   @Nested
   public class NormalizeResolveRelativize {

      @Test
      public void normalizeWhenNominal() {
         assertThat(ClasspathRepositoryUri
                          .ofRepositoryUri("classpath:/path/../.")
                          .normalize()).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo("classpath:/");
         });
      }

      @Test
      public void resolveWhenNominal() {
         assertThat(ClasspathRepositoryUri
                          .ofRepositoryUri("classpath:/path")
                          .resolve(uri("otherpath"))).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo("classpath:/otherpath");
         });
         assertThat(ClasspathRepositoryUri
                          .ofRepositoryUri("classpath:/path/")
                          .resolve(uri("otherpath"))).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo("classpath:/path/otherpath");
         });
      }

      @Test
      public void relativizeWhenNominal() {
         assertThatThrownBy(() -> ClasspathRepositoryUri
               .ofRepositoryUri("classpath:/path")
               .relativize(uri("classpath:/path/otherpath"))).isInstanceOf(UnsupportedOperationException.class);
      }

   }

}