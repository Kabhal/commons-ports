/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.classpath;

import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ports.document.classpath.ClasspathRepositoryUri.Parameters.CLASSPATH_PREFIX;

import java.net.URI;
import java.nio.file.Path;

import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.domain.type.Id;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.ExportUriOptions;

/**
 * Classpath URI document URI. This URI must be used to identify a document, use
 * {@link ClasspathRepositoryUri} to identify a repository.
 * <p>
 * You can use this class when a {@link ClasspathRepositoryUri} is required, but your current URI is a
 * document URI. In this case it's important that the classpath prefix is set, or it will be set to URI path
 * parent by default.
 * Classpath prefix is read from {@code classpathPrefix} URI query string parameter, or from specified
 * {@code defaultClasspathPrefix} parameter.
 * <p>
 * Supported URI format :
 * <ul>
 *    <li>{@code classpath:[</classpath-prefix>]/<document-id>[/?<parameters>]}</li>
 *    <li>Commons-ports wrapped URI : {@code document:classpath:<classpath-uri>}</li>
 * </ul>
 *
 * @see ClasspathRepositoryUri
 */
public class ClasspathDocumentUri extends ClasspathRepositoryUri implements DocumentUri {

   protected ClasspathDocumentUri(ClasspathUri uri) {
      super(uri, true);
   }

   @Override
   @SuppressWarnings("unchecked")
   protected Fields<? extends ClasspathDocumentUri> defineDomainFields() {
      return Fields
            .<ClasspathDocumentUri>builder()
            .superFields((Fields<ClasspathDocumentUri>) super.defineDomainFields())
            .field("documentId", v -> v.documentId, isNotNull())
            .build();
   }

   /**
    * Creates a document URI from {@link Uri}.
    *
    * @param uri document URI
    * @param defaultClasspathPrefix default classpath prefix to use if {@code classpathPrefix} query
    *       parameter not present
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static ClasspathDocumentUri ofDocumentUri(Uri uri, Path defaultClasspathPrefix) {
      Check.notNull(uri, "uri");
      var normalizedDefaultClasspathPrefix =
            validateAndNormalizeClasspathPrefix(defaultClasspathPrefix, "defaultClasspathPrefix");

      return buildUri(() -> new ClasspathDocumentUri(mapQuery(classpathUri(uri), (u, q) -> {
         q.registerParameter(CLASSPATH_PREFIX.defaultValue(normalizedDefaultClasspathPrefix));
         return u;
      })));
   }

   /**
    * Creates a document URI from {@link Uri}.
    *
    * @param uri document URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static ClasspathDocumentUri ofDocumentUri(Uri uri) {
      Check.notNull(uri, "uri");

      validateUriCompatibility(uri, "uri", isCompatibleUriType(ClasspathUri.class));

      return ofDocumentUri(uri, defaultClasspathPrefix(uri, true));
   }

   /**
    * Creates a document URI from {@link URI}.
    * <p>
    * Default classpath prefix is set to URI parent path.
    *
    * @param uri document URI
    * @param defaultClasspathPrefix default classpath prefix to use if {@code classpathPrefix} query
    *       parameter not present
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static ClasspathDocumentUri ofDocumentUri(URI uri, Path defaultClasspathPrefix) {
      Check.notNull(uri, "uri");
      Check.notNull(defaultClasspathPrefix, "defaultClasspathPrefix");

      return ofDocumentUri(Uri.ofUri(uri), defaultClasspathPrefix);
   }

   /**
    * Creates a document URI from {@link URI}.
    * <p>
    * Default classpath prefix is set to URI parent path.
    *
    * @param uri document URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static ClasspathDocumentUri ofDocumentUri(URI uri) {
      Check.notNull(uri, "uri");

      return ofDocumentUri(Uri.ofUri(uri));
   }

   /**
    * Creates a document URI from URI string.
    * URI syntax is checked as part of this operation.
    *
    * @param uri document URI
    * @param defaultClasspathPrefix default classpath prefix to use if {@code classpathPrefix} query
    *       parameter not present
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static ClasspathDocumentUri ofDocumentUri(String uri, Path defaultClasspathPrefix) {
      Check.notNull(uri, "uri");
      Check.notNull(defaultClasspathPrefix, "defaultClasspathPrefix");

      return ofDocumentUri(Uri.ofUri(uri), defaultClasspathPrefix);
   }

   /**
    * Creates a document URI from URI string.
    * URI syntax is checked as part of this operation.
    * <p>
    * Default classpath prefix is set to URI parent path.
    *
    * @param uri document URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static ClasspathDocumentUri ofDocumentUri(String uri) {
      Check.notNull(uri, "uri");

      return ofDocumentUri(Uri.ofUri(uri));
   }

   /**
    * Creates a document URI from configuration.
    *
    * @param classpathPrefix repository configuration
    * @param documentId document id
    *
    * @return new instance
    */
   public static ClasspathDocumentUri ofConfig(Path classpathPrefix, DocumentPath documentId) {
      var normalizedClasspathPrefix = validateAndNormalizeClasspathPrefix(classpathPrefix, "classpathPrefix");

      return buildUri(() -> new ClasspathDocumentUri(classpathUri(normalizedClasspathPrefix, documentId)));
   }

   /**
    * Creates a document URI from {@link DocumentUri}.
    *
    * @param uri document URI
    * @param defaultClasspathPrefix default classpath prefix to use if {@code classpathPrefix} query
    *       parameter not present
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    * @implSpec this method is package-private because user code should always create
    *       {@link DocumentUri} directly from a URI.
    */
   static ClasspathDocumentUri ofDocumentUri(DocumentUri uri, Path defaultClasspathPrefix) {
      Check.notNull(uri, "uri");
      var normalizedDefaultClasspathPrefix =
            validateAndNormalizeClasspathPrefix(defaultClasspathPrefix, "defaultClasspathPrefix");

      validateUriCompatibility(uri, "uri", isCompatibleDocumentUriType(ClasspathDocumentUri.class));

      if (uri instanceof ClasspathDocumentUri) {
         return buildUri(() -> new ClasspathDocumentUri(mapQuery(classpathUri(uri), (u, q) -> {
            q.registerParameter(CLASSPATH_PREFIX.defaultValue(((ClasspathDocumentUri) uri).classpathPrefix()));
            return u;
         })));
      } else {
         return buildUri(() -> new ClasspathDocumentUri(mapQuery(classpathUri(uri), (u, q) -> {
            q.registerParameter(CLASSPATH_PREFIX.defaultValue(normalizedDefaultClasspathPrefix));
            return u;
         })));
      }
   }

   /**
    * Creates a document URI from {@link DocumentUri}.
    * <p>
    * Default classpath prefix is set to URI parent path.
    *
    * @param uri document URI
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    * @implSpec this method is package-private because user code should always create
    *       {@link DocumentUri} directly from a URI.
    */
   static ClasspathDocumentUri ofDocumentUri(DocumentUri uri) {
      Check.notNull(uri, "uri");

      return ofDocumentUri(uri, defaultClasspathPrefix(uri, true));
   }

   public DocumentPath documentId() {
      return documentId;
   }

   @Override
   public DocumentUri exportUri(ExportUriOptions options) {
      return DocumentUri.ofDocumentUri(exportURI(options));
   }

   @Override
   public ClasspathDocumentUri normalize() {
      return buildUri(() -> new ClasspathDocumentUri(classpathUri().normalize()));
   }

   @Override
   public ClasspathDocumentUri resolve(Uri uri) {
      return buildUri(() -> new ClasspathDocumentUri(classpathUri().resolve(uri)));
   }

   @Override
   public ClasspathDocumentUri resolve(URI uri) {
      return buildUri(() -> new ClasspathDocumentUri(classpathUri().resolve(uri)));
   }

   @Override
   public ClasspathRepositoryUri relativize(Uri uri) {
      throw new UnsupportedOperationException();
   }

   @Override
   public ClasspathRepositoryUri relativize(URI uri) {
      throw new UnsupportedOperationException();
   }

   @Override
   public int compareTo(Id o) {
      if (!(o instanceof ClasspathDocumentUri)) {
         return -1;
      } else {
         return super.compareTo(o);
      }
   }

}
