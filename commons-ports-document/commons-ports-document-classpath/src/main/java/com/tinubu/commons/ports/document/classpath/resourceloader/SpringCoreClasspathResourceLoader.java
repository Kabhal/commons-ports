/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.classpath.resourceloader;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasFileName;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isAbsolute;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotEmpty;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.StreamUtils.stream;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

/**
 * {@link ClasspathResourceLoader} implementation using Spring core utils.
 *
 * @implSpec Classpath resource {@link ClasspathResource#isDirectory()} is returning false if the
 *       directory is in a JAR. This feature only works for local classpath.
 */
public class SpringCoreClasspathResourceLoader implements ClasspathResourceLoader {

   @Override
   public Optional<ClasspathResource> resource(Path resourcePath) {
      validate(resourcePath,
               "resourcePath",
               isNotEmpty().andValue(isAbsolute().andValue(hasFileName()))).orThrow();

      return optional(new ClassPathResource(resourcePath.toString()))
            .filter(ClassPathResource::exists)
            .map(this::resource);
   }

   @Override
   public Stream<ClasspathResource> resources(Path directoryPath) throws IOException {
      validate(directoryPath, "directoryPath", isNotEmpty().andValue(isAbsolute())).orThrow();

      return stream(new PathMatchingResourcePatternResolver().getResources("classpath*:"
                                                                           + directoryPath.resolve("*"))).map(
            this::resource);
   }

   private ClasspathResource resource(org.springframework.core.io.Resource resource) {
      return new ClasspathResource() {

         @Override
         public String name() {
            return resource.getFilename();
         }

         @Override
         public boolean isDirectory() {
            try {
               return resource.isFile() && resource.getFile().isDirectory();
            } catch (IOException e) {
               throw new IllegalStateException(e);
            }
         }

         @Override
         public InputStream content() throws IOException {
            return resource.getInputStream();
         }
      };
   }
}
