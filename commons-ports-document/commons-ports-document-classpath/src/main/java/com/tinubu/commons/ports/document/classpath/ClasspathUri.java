/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.tinubu.commons.ports.document.classpath;

import static com.tinubu.commons.ddd2.domain.type.support.DomainObjectSupport.hideField;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;

import java.net.URI;

import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.Invariant;
import com.tinubu.commons.ddd2.invariant.Invariants;
import com.tinubu.commons.ddd2.invariant.ParameterValue;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.invariant.rules.domain.ids.UriRules;
import com.tinubu.commons.ddd2.uri.AbstractComponentUri;
import com.tinubu.commons.ddd2.uri.DefaultUriRestrictions;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ddd2.uri.UriRestrictions;
import com.tinubu.commons.ddd2.uri.parts.SimpleScheme;

/**
 * Classpath {@link Uri}.
 * <p>
 * Supported URI formats :
 * <ul>
 *    <li>{@code classpath:</path>[/?<query>][#<fragment>]}</li>
 * </ul>
 * However, supported format depends on {@link DefaultUriRestrictions} restrictions. By default,
 * <ul>
 *    <li>{@link DefaultUriRestrictions#query() query} : query is not supported</li>
 *    <li>{@link DefaultUriRestrictions#fragment() fragment} : fragment is not supported</li>
 * </ul>
 */
public class ClasspathUri extends AbstractComponentUri<ClasspathUri> {

   protected static final String SCHEME = "classpath";

   protected ClasspathUri(URI uri, UriRestrictions restrictions,
                          Scheme scheme,
                          SchemeSpecific schemeSpecific,
                          Authority authority,
                          Path path,
                          Query query,
                          Fragment fragment,
                          boolean newObject) {
      super(uri, restrictions, scheme, schemeSpecific, authority, path, query, fragment, newObject);
   }

   protected ClasspathUri(UriRestrictions restrictions, Scheme scheme,
                          SchemeSpecific schemeSpecific,
                          Authority authority, Path path, Query query, Fragment fragment) {
      super(restrictions, scheme, schemeSpecific, authority, path, query, fragment);
   }

   protected ClasspathUri(URI uri, UriRestrictions restrictions) {
      super(uri, restrictions);
   }

   protected ClasspathUri(Uri uri, UriRestrictions restrictions) {
      super(uri, restrictions);
   }

   @Override
   @SuppressWarnings("unchecked")
   protected Fields<? extends ClasspathUri> defineDomainFields() {
      return Fields
            .<ClasspathUri>builder()
            .superFields((Fields<? super ClasspathUri>) super.defineDomainFields())
            .field("scheme", v -> v.scheme)
            .field("schemeSpecific", v -> v.schemeSpecific)
            .field("authority", v -> v.authority, hideField(), isNull())
            .field("path", v -> v.path)
            .field("query", v -> v.query)
            .field("fragment", v -> v.fragment)
            .build();
   }

   @Override
   public Invariants domainInvariants() {
      var uriCompatibility = Invariant
            .of(() -> this,
                UriRules.isAbsolute(ParameterValue.value(SCHEME)),
                UriRules.isHierarchical().andValue(UriRules.hasNoAuthority()))
            .groups(COMPATIBILITY_GROUP);

      return super.domainInvariants().withInvariants(uriCompatibility);
   }

   public static ClasspathUri ofUri(URI uri, UriRestrictions restrictions) {
      return buildUri(() -> new ClasspathUri(uri, restrictions));
   }

   public static ClasspathUri ofUri(URI uri) {
      return ofUri(uri, defaultRestrictions());
   }

   public static ClasspathUri ofUri(Uri uri, UriRestrictions restrictions) {
      return buildUri(() -> new ClasspathUri(uri, restrictions));
   }

   public static ClasspathUri ofUri(Uri uri) {
      return buildUri(() -> new ClasspathUri(uri, defaultRestrictions()));
   }

   public static ClasspathUri ofUri(String uri, UriRestrictions restrictions) {
      return ofUri(Uri.uri(uri), restrictions);
   }

   public static ClasspathUri ofUri(String uri) {
      return ofUri(uri, defaultRestrictions());
   }

   public static ClasspathUri hierarchical(Path path, Query query, Fragment fragment) {
      Check.notNull(path, "path");

      return buildUri(() -> new ClasspathUri(DefaultUriRestrictions.automatic(query, fragment),
                                             SimpleScheme.of(SCHEME),
                                             null,
                                             null, path, query, fragment));
   }

   public static ClasspathUri hierarchical(Path path, Query query) {
      return hierarchical(path, query, null);
   }

   public static ClasspathUri hierarchical(Path path) {
      return hierarchical(path, null, null);
   }

   public static ClasspathUri hierarchical(Path path, Fragment fragment) {
      return hierarchical(path, null, fragment);
   }

   protected static UriRestrictions defaultRestrictions() {
      return DefaultUriRestrictions.ofRestrictions(true, true);
   }

   @Override
   protected ClasspathUri recreate(URI uri, UriRestrictions restrictions,
                                   Scheme scheme,
                                   SchemeSpecific schemeSpecific,
                                   Authority authority,
                                   Path path,
                                   Query query,
                                   Fragment fragment,
                                   boolean newObject) {
      return buildUri(() -> new ClasspathUri(uri, restrictions,
                                             scheme,
                                             schemeSpecific,
                                             authority,
                                             path,
                                             query,
                                             fragment,
                                             newObject));
   }

   @Override
   public ClasspathUri relativize(Uri uri) {
      throw new UnsupportedOperationException();
   }

   @Override
   public ClasspathUri relativize(URI uri) {
      throw new UnsupportedOperationException();
   }

   /**
    * Returns this classpath as a {@link java.nio.file.Path}.
    *
    * @return path
    */
   public java.nio.file.Path toPath() {
      return java.nio.file.Path.of(path(false).orElseThrow());
   }

}
