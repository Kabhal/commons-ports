/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.classpath;

import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.PathUtils.isRoot;
import static com.tinubu.commons.ports.document.classpath.ClasspathRepositoryUri.Parameters.CLASSPATH_PREFIX;
import static com.tinubu.commons.ports.document.classpath.ClasspathRepositoryUri.Parameters.allParameters;
import static com.tinubu.commons.ports.document.classpath.ClasspathRepositoryUri.Parameters.valueParameters;
import static com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery.ExportUriFilter.isAlwaysTrue;
import static com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery.ExportUriFilter.isRegisteredParameter;
import static com.tinubu.commons.ports.document.domain.uri.UriUtils.exportUriFilter;

import java.net.URI;
import java.nio.file.Path;
import java.util.List;
import java.util.function.BiPredicate;

import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.domain.type.Id;
import com.tinubu.commons.ddd2.invariant.Validate;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.invariant.rules.PathRules;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ddd2.uri.parts.SimplePath;
import com.tinubu.commons.lang.convert.ConversionFailedException;
import com.tinubu.commons.lang.util.Pair;
import com.tinubu.commons.lang.util.PathUtils;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.uri.AbstractRepositoryUri;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.ExportUriOptions;
import com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery;
import com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery.Parameter;
import com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery.ValueParameter;
import com.tinubu.commons.ports.document.domain.uri.RepositoryUri;

/**
 * Classpath URI repository URI. This URI must be used to identify a repository, use
 * {@link ClasspathDocumentUri} to identify a document.
 * <p>
 * The classpath prefix will be set to URI path, if present, or {@code /} by default.
 * <p>
 * Supported URI format :
 * <ul>
 *    <li>{@code classpath:[</classpath-prefix>][/?<parameters>]}</li>
 *    <li>Commons-ports wrapped URI : {@code document:classpath:<classpath-uri>}</li>
 * </ul>
 * <p>
 * Supported URI parameters :
 * <ul>
 * <li>{@code classpathPrefix} (Path) [&lt;automatic&gt;] : absolute classpath path to store all documents</li>
 * </ul>
 *
 * @see ClasspathDocumentUri
 */
public class ClasspathRepositoryUri extends AbstractRepositoryUri implements RepositoryUri {

   private static final String WRAPPED_URI_REPOSITORY_TYPE = "classpath";

   /**
    * Feature flag : whether to use {@link #defaultClasspathPrefix(Uri, boolean)} to override
    * {@link Parameters#CLASSPATH_PREFIX} default value in {@link #exportUri(ExportUriOptions)}. If set to
    * {@code true}, {@code classpathPrefix} parameter won't be exported if it matches default behavior,
    * depending on export options.
    */
   private static final boolean EXPORT_URI_DEFAULT_CLASSPATH_PREFIX_AS_DEFAULT_VALUE = true;

   protected final Path classpathPrefix;
   protected final DocumentPath documentId;

   protected ClasspathRepositoryUri(ClasspathUri uri, boolean documentUri) {
      super(uri, documentUri);

      try {
         var parsed = parseUri(documentUri);
         this.classpathPrefix = parsed.getLeft();
         this.documentId = parsed.getRight();
      } catch (ConversionFailedException e) {
         throw new InvalidUriException(uri, e).subMessage(e);
      }
   }

   @Override
   @SuppressWarnings("unchecked")
   protected Fields<? extends ClasspathRepositoryUri> defineDomainFields() {
      return Fields
            .<ClasspathRepositoryUri>builder()
            .superFields((Fields<ClasspathRepositoryUri>) super.defineDomainFields())
            .field("classpathPrefix",
                   v -> v.classpathPrefix,
                   PathRules.isAbsolute().andValue(hasNoTraversal()))
            .build();
   }

   /**
    * Creates a repository URI from {@link Uri}. Specified URI must represent a "repository" URI.
    *
    * @param uri repository URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static ClasspathRepositoryUri ofRepositoryUri(Uri uri) {
      Check.notNull(uri, "uri");

      validateUriCompatibility(uri, "uri", isCompatibleUriType(ClasspathUri.class));

      return buildUri(() -> new ClasspathRepositoryUri(classpathUri(uri), false));
   }

   /**
    * Creates a repository URI from {@link URI}. Specified URI must represent a "repository" URI.
    *
    * @param uri repository URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static ClasspathRepositoryUri ofRepositoryUri(URI uri) {
      Check.notNull(uri, "uri");

      return ofRepositoryUri(Uri.ofUri(uri));
   }

   /**
    * Creates a repository URI from URI string. Specified URI must represent a "repository" URI.
    *
    * @param uri repository URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static ClasspathRepositoryUri ofRepositoryUri(String uri) {
      Check.notNull(uri, "uri");

      return ofRepositoryUri(Uri.ofUri(uri));
   }

   /**
    * Creates a repository URI from configuration.
    *
    * @param classpathPrefix repository configuration
    *
    * @return new instance
    */
   public static ClasspathRepositoryUri ofConfig(Path classpathPrefix) {
      var normalizedClasspathPrefix = validateAndNormalizeClasspathPrefix(classpathPrefix, "classpathPrefix");

      return buildUri(() -> new ClasspathRepositoryUri(classpathUri(normalizedClasspathPrefix, null), false));
   }

   /**
    * Creates a repository URI from {@link RepositoryUri}. Specified repository URI can can be a
    * {@liMnk RepositoryUri} to represent a "repository" URI, or a {@link DocumentUri} to represent a
    * "document" URI.
    *
    * @param uri repository URI
    * @param defaultClasspathPrefix default classpath prefix to use if {@code classpathPrefix} query
    *       parameter not present
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    * @implSpec this method is package-private because user code should always create
    *       {@link RepositoryUri} directly from a URI.
    */
   static ClasspathRepositoryUri ofRepositoryUri(RepositoryUri uri, Path defaultClasspathPrefix) {
      Check.notNull(uri, "uri");
      var normalizedDefaultClasspathPrefix =
            validateAndNormalizeClasspathPrefix(defaultClasspathPrefix, "defaultClasspathPrefix");

      validateUriCompatibility(uri, "uri", isCompatibleRepositoryUriType(ClasspathRepositoryUri.class));

      if (uri instanceof ClasspathRepositoryUri) {
         return buildUri(() -> new ClasspathRepositoryUri(mapQuery(classpathUri(uri), (u, q) -> {
            q.registerParameter(CLASSPATH_PREFIX.defaultValue(((ClasspathRepositoryUri) uri).classpathPrefix()));
            return u;
         }), uri instanceof DocumentUri));
      } else {
         return buildUri(() -> new ClasspathRepositoryUri(mapQuery(classpathUri(uri), (u, q) -> {
            q.registerParameter(CLASSPATH_PREFIX.defaultValue(normalizedDefaultClasspathPrefix));
            return u;
         }), uri instanceof DocumentUri));

      }
   }

   protected static Path validateAndNormalizeClasspathPrefix(Path classpathPrefix, String name) {
      return Check.validate(nullable(classpathPrefix, Path::normalize),
                            name,
                            PathRules.isAbsolute().andValue(hasNoTraversal()));
   }

   /**
    * Creates a repository URI from {@link RepositoryUri}.
    * <p>
    * Default classpath prefix is set to URI parent path.
    *
    * @param uri repository URI
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    * @implSpec this method is package-private because user code should always create
    *       {@link RepositoryUri} directly from a URI.
    */
   static ClasspathRepositoryUri ofRepositoryUri(RepositoryUri uri) {
      Check.notNull(uri, "uri");

      return ofRepositoryUri(uri, defaultClasspathPrefix(uri, uri instanceof DocumentUri));
   }

   public ClasspathUri classpathUri() {
      return (ClasspathUri) uri;
   }

   @Override
   public ClasspathRepositoryUri normalize() {
      return buildUri(() -> new ClasspathRepositoryUri(classpathUri().normalize(), documentUri));
   }

   @Override
   public ClasspathRepositoryUri resolve(Uri uri) {
      return buildUri(() -> new ClasspathRepositoryUri(classpathUri().resolve(uri), documentUri));
   }

   @Override
   public ClasspathRepositoryUri resolve(URI uri) {
      return buildUri(() -> new ClasspathRepositoryUri(classpathUri().resolve(uri), documentUri));
   }

   @Override
   public ClasspathRepositoryUri relativize(Uri uri) {
      throw new UnsupportedOperationException();
   }

   @Override
   public ClasspathRepositoryUri relativize(URI uri) {
      throw new UnsupportedOperationException();
   }

   @Override
   public int compareTo(Id o) {
      if (!(o instanceof ClasspathRepositoryUri)) {
         return -1;
      } else if (o == this) {
         return 0;
      } else {
         return classpathUri().compareTo(((ClasspathRepositoryUri) o).classpathUri());
      }
   }

   public Path classpathPrefix() {
      return classpathPrefix;
   }

   @Override
   public boolean supportsUri(RepositoryUri uri) {
      Check.notNull(uri, "uri");

      return supportsRepositoryUri(uri, ClasspathRepositoryUri.class, ClasspathRepositoryUri::ofRepositoryUri)
            .map(classpathUri -> {
               if (!classpathUri.classpathPrefix().equals(classpathPrefix())) {
                  return false;
               }

               return mapQuery(classpathUri(),
                               (u, q) -> q.includeParameters(query(classpathUri.classpathUri()),
                                                             isRegisteredParameter()));
            })
            .orElse(false);
   }

   @Override
   public URI exportURI(ExportUriOptions options) {
      Check.notNull(options, "options");

      var filter = exportUriFilter(options, isAlwaysTrue());
      var exportQuery = exportQuery(filter).noQueryIfEmpty(true);
      var exportUri = classpathUri().component().query(__ -> exportQuery);

      return exportUri.toURI();
   }

   private ParameterizedQuery exportQuery(BiPredicate<ParameterizedQuery, Parameter> filter) {
      var exportQuery = query();

      if (EXPORT_URI_DEFAULT_CLASSPATH_PREFIX_AS_DEFAULT_VALUE) {
         var classpathPrefixParameter =
               CLASSPATH_PREFIX.defaultValue(defaultClasspathPrefix(classpathUri(), documentUri));
         exportQuery = exportQuery.registerParameter(ValueParameter.ofValue(classpathPrefixParameter,
                                                                            classpathPrefix));
      }

      return exportQuery.exportQuery(filter);
   }

   @Override
   public RepositoryUri exportUri(ExportUriOptions options) {
      return RepositoryUri.ofRepositoryUri(exportURI(options));
   }

   /**
    * Creates a parameterized {@link ClasspathUri} from specified {@link Uri}.
    * <p>
    * If specified URI is wrapped, it is unwrapped before creating parameterized URI.
    * Specified URI is normalized and checked for remaining traversals, after unwrapping.
    *
    * @param uri URI
    *
    * @return new parameterized URI, with all registered parameters
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   protected static ClasspathUri classpathUri(Uri uri) {
      Check.notNull(uri, "uri");
      var unwrappedUri = unwrapUri(uri, WRAPPED_URI_REPOSITORY_TYPE);

      return parameterizedQueryUri(unwrappedUri, ClasspathUri::ofUri, allParameters());
   }

   /**
    * Creates a parameterized {@link ClasspathUri} from specified configuration.
    *
    * @param classpathPrefix repository configuration
    * @param documentId optional document id
    *
    * @return new parameterized URI, with all registered value parameters from configuration
    */
   protected static ClasspathUri classpathUri(Path classpathPrefix, DocumentPath documentId) {
      Check.notNull(classpathPrefix, "classpathPrefix");

      return ClasspathUri.hierarchical(SimplePath.of((documentId != null
                                                      ? classpathPrefix.resolve(documentId.value())
                                                      : classpathPrefix)),
                                       ParameterizedQuery
                                             .ofEmpty()
                                             .noQueryIfEmpty(true)
                                             .registerParameters(valueParameters(classpathPrefix)));
   }

   /**
    * Default classpath prefix to complement generic {@link DocumentUri} or {@link RepositoryUri}.
    * <p>
    * For a document URI, default classpath prefix is set to URI parent path, otherwise, it is set to URI
    * path.
    * <p>
    * If URI has no path, or no parent path, or is only a root path, default storage path is set to
    * {@code /}.
    *
    * @param uri generic URI
    * @param documentUri whether URI is a document URI
    *
    * @return default classpath prefix
    */
   protected static Path defaultClasspathPrefix(Uri uri, boolean documentUri) {
      return documentUri ? uri
            .path(false)
            .map(Path::of)
            .flatMap(path -> nullable(path.getParent()))
            .orElseGet(PathUtils::rootPath) : uri.path(false).map(Path::of).orElseGet(PathUtils::rootPath);
   }

   /**
    * Checks URI syntax and parses current parameterized URI to separate the classpath prefix, from the
    * document path.
    *
    * @param documentUri whether the current parameterized URI is a document URI, identifying a
    *       document
    *       instead of a repository
    *
    * @return parsed (classpath prefix, document path) pair
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @implSpec The minimum set of parameters are checked here for consistency, only the parameters
    *       used in class internal state : classpath prefix. Other parameters
    *       could be checked later.
    *       <p>
    *       Default classpath prefix value is only applied if it starts actual URI path, otherwise,
    *       it is discarded because it's a "default", not an explicit value for actual URI, and it could
    *       apply to whatever URI is passed in.
    */
   protected Pair<Path, DocumentPath> parseUri(boolean documentUri) {
      var uriPath = classpathUri().toPath();

      if (documentUri && isRoot(uriPath)) {
         throw new InvalidUriException(classpathUri()).subMessage("Missing document id");
      }

      @SuppressWarnings("unchecked")
      var classpathPrefixParameter = query()
            .parameter(CLASSPATH_PREFIX,
                       cpp -> ((Parameter<Path>) cpp).defaultValue().map(uriPath::startsWith).orElse(false))
            .orElseGet(() -> defaultClasspathPrefix(classpathUri(), documentUri))
            .normalize();

      Validate
            .satisfies(classpathPrefixParameter, PathRules.isAbsolute().andValue(hasNoTraversal()))
            .orThrowMessage(message -> new InvalidUriException(classpathUri()).subMessage(
                  "'%s=%s' parameter must be absolute and must not contain traversal",
                  CLASSPATH_PREFIX.key(),
                  classpathPrefixParameter.toString()));

      Path classpathPrefix;
      DocumentPath documentId;
      if (!documentUri) {
         if (!classpathPrefixParameter.equals(uriPath)) {
            throw new InvalidUriException(classpathUri()).subMessage(
                  "'%s=%s' parameter must match URI '%s' path",
                  CLASSPATH_PREFIX.key(),
                  classpathPrefixParameter.toString(),
                  uriPath.toString());
         }
         classpathPrefix = uriPath;
         documentId = null;
      } else {
         if (!uriPath.startsWith(classpathPrefixParameter) || uriPath.equals(classpathPrefixParameter)) {
            throw new InvalidUriException(classpathUri()).subMessage(
                  "'%s=%s' parameter must start but not be equal to URI '%s' path",
                  CLASSPATH_PREFIX.key(),
                  classpathPrefixParameter.toString(),
                  uriPath.toString());
         }
         classpathPrefix = classpathPrefixParameter;
         documentId = DocumentPath.of(classpathPrefix.relativize(uriPath));
      }

      return Pair.of(classpathPrefix, documentId);
   }

   /**
    * Supported parameter registry.
    */
   static class Parameters {
      static final Parameter<Path> CLASSPATH_PREFIX = Parameter.registered("classpathPrefix", Path.class);

      @SuppressWarnings("rawtypes")
      static List<Parameter> allParameters() {
         return list(CLASSPATH_PREFIX);
      }

      @SuppressWarnings("rawtypes")
      static List<ValueParameter> valueParameters(Path classpathPrefix) {
         return list(ValueParameter.ofValue(CLASSPATH_PREFIX, classpathPrefix));
      }

   }

}
