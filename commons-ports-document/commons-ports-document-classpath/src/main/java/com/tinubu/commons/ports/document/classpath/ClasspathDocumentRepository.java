/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.classpath;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isAbsolute;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotAbsolute;
import static com.tinubu.commons.lang.util.CollectionUtils.collection;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.instanceOf;
import static com.tinubu.commons.lang.util.PathUtils.emptyPath;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.DOCUMENT_URI;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.ITERABLE;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.QUERYABLE;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.READABLE;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.REPOSITORY_URI;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.WRITABLE;
import static java.util.Objects.requireNonNull;

import java.io.IOException;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.function.BiFunction;
import java.util.stream.Stream;

import org.apache.commons.lang3.time.StopWatch;

import com.tinubu.commons.ddd2.domain.event.RegistrableDomainEventService;
import com.tinubu.commons.ddd2.domain.event.SynchronousDomainEventService;
import com.tinubu.commons.ddd2.domain.repository.Repository;
import com.tinubu.commons.ddd2.domain.specification.Specification;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.validation.CheckReturnValue;
import com.tinubu.commons.ports.document.classpath.resourceloader.ClasspathResource;
import com.tinubu.commons.ports.document.classpath.resourceloader.ClasspathResourceLoader;
import com.tinubu.commons.ports.document.classpath.resourceloader.ClasspathResourceLoaderFactory;
import com.tinubu.commons.ports.document.domain.AbstractDocumentRepository;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentEntry.DocumentEntryBuilder;
import com.tinubu.commons.ports.document.domain.DocumentEntrySpecification;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata;
import com.tinubu.commons.ports.document.domain.ReferencedDocument;
import com.tinubu.commons.ports.document.domain.capability.RepositoryCapability;
import com.tinubu.commons.ports.document.domain.capability.UnsupportedCapabilityException;
import com.tinubu.commons.ports.document.domain.uri.DocumentRepositoryUriAdapter;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.ExportUriOptions;
import com.tinubu.commons.ports.document.domain.uri.RepositoryUri;
import com.tinubu.commons.ports.document.transformer.DocumentTransformerUriAdapter;

/**
 * Classpath {@link DocumentRepository} adapter implementation.
 * Limitations :
 * <ul>
 *    <li>No write operation supported</li>
 *    <li>the following Metadata are not persisted: documentPath, creationDate, lastUpdateDate, contentEncoding, contentType, contentSize, attributes</li>
 * </ul>
 * Supported URI parameters :
 * <ul>
 *    <li>{@code classpathPrefix (Path)} : Absolute classpath prefix path</li>
 * </ul>
 *
 * @implSpec Immutable class implementation
 */
public class ClasspathDocumentRepository extends AbstractDocumentRepository<ClasspathDocumentRepository> {
   private static final HashSet<RepositoryCapability> CAPABILITIES =
         collection(HashSet::new, READABLE, ITERABLE, QUERYABLE, REPOSITORY_URI, DOCUMENT_URI);

   private final Path classpathPrefix;
   private final DocumentRepositoryUriAdapter uriAdapter;
   private final ClasspathResourceLoader resourceLoader;

   public ClasspathDocumentRepository(Path classpathPrefix, RegistrableDomainEventService eventService) {
      super(eventService);

      this.uriAdapter = new ClasspathUriAdapter();
      this.classpathPrefix = validate(classpathPrefix,
                                      "classpathPrefix",
                                      isNotNull().andValue(isAbsolute().andValue(hasNoTraversal())))
            .orThrow()
            .normalize();
      resourceLoader = ClasspathResourceLoaderFactory.resourceLoader();
   }

   public ClasspathDocumentRepository(Path classpathPrefix) {
      this(classpathPrefix, new SynchronousDomainEventService());
   }

   public ClasspathDocumentRepository() {
      this(emptyPath());
   }

   /**
    * Creates a new document repository from URI with parameters.
    * If specified URI is a document URI, and no {@code classpathPrefix} parameter is set, classpath prefix is
    * arbitrary set to {@code /}.
    *
    * @param uri URI
    *
    * @return new document repository
    *
    * @throws InvalidUriException if specified URI is compatible with this repository but invalid
    * @throws IncompatibleUriException if specified URI is not compatible with this repository
    */
   public static ClasspathDocumentRepository ofUri(RepositoryUri uri) {
      Check.notNull(uri, "uri");

      return new ClasspathDocumentRepository(ClasspathRepositoryUri.ofRepositoryUri(uri).classpathPrefix());
   }

   /**
    * Creates a lazily referenced document, in a new document repository, from URI with parameters. URI must
    * be compatible with this repository and identify a document.
    * If specified URI is a document URI, and no {@code classpathPrefix} parameter is set, classpath prefix is
    * arbitrary set to {@code /}.
    *
    * @param documentUri document URI
    *
    * @return new document repository
    *
    * @throws InvalidUriException if specified URI is compatible with this repository but invalid
    * @throws IncompatibleUriException if specified URI is not compatible with this repository
    */
   public static ReferencedDocument<ClasspathDocumentRepository> referencedDocument(DocumentUri documentUri) {
      Check.notNull(documentUri, "documentUri");

      var uri = ClasspathDocumentUri.ofDocumentUri(documentUri);

      return ReferencedDocument.lazyLoad(new ClasspathDocumentRepository(uri.classpathPrefix()),
                                         uri.documentId());
   }

   @Getter
   public Path classpathPrefix() {
      return classpathPrefix;
   }

   @Override
   public HashSet<RepositoryCapability> capabilities() {
      return CAPABILITIES;
   }

   @Override
   public boolean sameRepositoryAs(Repository<Document, DocumentPath> documentRepository) {
      validate(documentRepository, "documentRepository", isNotNull()).orThrow();

      return documentRepository instanceof ClasspathDocumentRepository
             && Objects.equals(((ClasspathDocumentRepository) documentRepository).classpathPrefix,
                               classpathPrefix);
   }

   @Override
   public ClasspathDocumentRepository subPath(Path subPath, boolean shareContext) {
      validate(subPath, "subPath", isNotAbsolute().andValue(hasNoTraversal())).orThrow();

      return new ClasspathDocumentRepository(classpathPrefix.resolve(subPath), eventService);
   }

   @Override
   @CheckReturnValue
   public Optional<Document> openDocument(DocumentPath documentId,
                                          boolean overwrite,
                                          boolean append,
                                          OpenDocumentMetadata metadata) {
      throw new UnsupportedCapabilityException(WRITABLE);
   }

   @Override
   public Optional<Document> findDocumentById(DocumentPath documentId) {
      validate(documentId, "documentId", isNotNull()).orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEvent(() -> resource(documentId).map(r -> document(documentEntry(documentId), r)),
                                 d -> documentAccessed(d.documentEntry(), watch));
   }

   @Override
   public Stream<Document> findDocumentsBySpecification(Path basePath,
                                                        Specification<DocumentEntry> specification) {
      validate(basePath, "basePath", isNotAbsolute().andValue(hasNoTraversal()))
            .and(validate(specification, "documentId", isNotNull()))
            .orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentStreamEvent(() -> autoCloseDocumentsOnStreamClose(listResources(basePath,
                                                                                           specification,
                                                                                           (resource, documentEntry) -> document(
                                                                                                 documentEntry,
                                                                                                 resource))),
                                       d -> documentAccessed(d.documentEntry(), watch));
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryById(DocumentPath documentId) {
      validate(documentId, "documentId", isNotNull()).orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEntryEvent(() -> resource(documentId).map(r -> documentEntry(documentId)),
                                      d -> documentAccessed(d, watch));
   }

   @Override
   public Stream<DocumentEntry> findDocumentEntriesBySpecification(Path basePath,
                                                                   Specification<DocumentEntry> specification) {
      validate(basePath, "basePath", isNotAbsolute().andValue(hasNoTraversal()))
            .and(validate(specification, "specification", isNotNull()))
            .orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEntryStreamEvent(() -> listResources(basePath,
                                                                specification,
                                                                (__, documentEntry) -> documentEntry),
                                            d -> documentAccessed(d, watch));
   }

   @Override
   @CheckReturnValue
   public Optional<DocumentEntry> saveDocument(Document document, boolean overwrite) {
      throw new UnsupportedCapabilityException(WRITABLE);
   }

   @Override
   @CheckReturnValue
   public Optional<Document> saveAndReturnDocument(Document document, boolean overwrite) {
      throw new UnsupportedCapabilityException(WRITABLE);
   }

   @Override
   @CheckReturnValue
   public Optional<DocumentEntry> deleteDocumentById(DocumentPath documentId) {
      throw new UnsupportedCapabilityException(WRITABLE);
   }

   @Override
   public List<DocumentEntry> deleteDocumentsBySpecification(Path basePath,
                                                             Specification<DocumentEntry> specification) {
      throw new UnsupportedCapabilityException(WRITABLE);
   }

   @Override
   public ReferencedDocument<? extends DocumentRepository> referencedDocumentId(DocumentUri documentUri) {
      return uriAdapter.referencedDocumentId(documentUri);
   }

   @Override
   public boolean supportsUri(RepositoryUri uri) {
      return uriAdapter.supportsUri(uri);
   }

   @Override
   public RepositoryUri toUri(ExportUriOptions options) {
      return uriAdapter.toUri(options);
   }

   @Override
   public DocumentUri toUri(DocumentPath documentId, ExportUriOptions options) {
      return uriAdapter.toUri(documentId, options);
   }

   @Override
   public Optional<Document> findDocumentByUri(DocumentUri documentUri) {
      return uriAdapter.findDocumentByUri(documentUri);
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryByUri(DocumentUri documentUri) {
      return uriAdapter.findDocumentEntryByUri(documentUri);
   }

   /**
    * Real classpath URI adapter to be used by {@link DocumentTransformerUriAdapter}.
    */
   public class ClasspathUriAdapter implements DocumentRepositoryUriAdapter {

      @Override
      public ReferencedDocument<? extends DocumentRepository> referencedDocumentId(DocumentUri documentUri) {
         Check.notNull(documentUri, "documentUri");
         var classpathDocumentUri = validateClasspathDocumentUri(documentUri);

         return ReferencedDocument.lazyLoad(ClasspathDocumentRepository.this,
                                            classpathDocumentUri.documentId());
      }

      @Override
      public boolean supportsUri(RepositoryUri uri) {
         Check.notNull(uri, "uri");

         return supportsClasspathUri(uri);
      }

      @Override
      public RepositoryUri toUri(ExportUriOptions options) {
         Check.notNull(options, "options");

         return ClasspathRepositoryUri.ofConfig(classpathPrefix).exportUri(options);
      }

      @Override
      public DocumentUri toUri(DocumentPath documentId, ExportUriOptions options) {
         Check.notNull(documentId, "documentId");
         Check.notNull(options, "options");

         return ClasspathDocumentUri.ofConfig(classpathPrefix, documentId).exportUri(options);
      }

      @Override
      public Optional<Document> findDocumentByUri(DocumentUri documentUri) {
         Check.notNull(documentUri, "documentUri");
         var classpathDocumentUri = validateClasspathDocumentUri(documentUri);

         return findDocumentById(classpathDocumentUri.documentId());
      }

      @Override
      public Optional<DocumentEntry> findDocumentEntryByUri(DocumentUri documentUri) {
         Check.notNull(documentUri, "documentUri");
         var classpathDocumentUri = validateClasspathDocumentUri(documentUri);

         return findDocumentEntryById(classpathDocumentUri.documentId());
      }
   }

   private boolean supportsClasspathUri(RepositoryUri repositoryUri) {
      try {
         return (ClasspathRepositoryUri
                       .ofConfig(classpathPrefix)
                       .supportsUri(instanceOf(repositoryUri,
                                               ClasspathRepositoryUri.class).orElseGet(() -> ClasspathRepositoryUri.ofRepositoryUri(
                             repositoryUri,
                             classpathPrefix))));
      } catch (IncompatibleUriException __) {
         return false;
      }
   }

   private ClasspathDocumentUri validateClasspathDocumentUri(DocumentUri documentUri) {
      try {
         var classpathUri = instanceOf(documentUri,
                                       ClasspathDocumentUri.class).orElseGet(() -> ClasspathDocumentUri.ofDocumentUri(
               documentUri,
               classpathPrefix));

         if (!supportsClasspathUri(classpathUri)) {
            throw new InvalidUriException(documentUri).message("'documentUri=%s' must be supported",
                                                               documentUri.toString());
         }

         return classpathUri;
      } catch (IncompatibleUriException e) {
         throw new InvalidUriException(documentUri).message("'documentUri=%s' must be supported: %s",
                                                            documentUri.toString(),
                                                            e.getMessage());
      }
   }

   private DocumentEntry documentEntry(DocumentPath documentId) {
      return new DocumentEntryBuilder()
            .<DocumentEntryBuilder>reconstitute()
            .documentId(documentId)
            .metadata(new DocumentMetadataBuilder().documentPath(documentId.value()).build())
            .build();
   }

   private Document document(DocumentEntry documentEntry, ClasspathResource resource) {
      try {
         return new DocumentBuilder()
               .<DocumentBuilder>reconstitute()
               .documentEntry(documentEntry)
               .streamContent(resource.content())
               .build();
      } catch (IOException e) {
         throw new DocumentAccessException(String.format("Can't read '%s'",
                                                         documentEntry.documentId().stringValue()), e);
      }
   }

   private Optional<ClasspathResource> resource(DocumentPath documentId) {
      try {
         return resourceLoader.resource(classpathPrefix.resolve(documentId.value()));
      } catch (IOException e) {
         throw new DocumentAccessException(e);
      }

   }

   /**
    * Returns {@link ClasspathResource} directly present in specified path.
    *
    * @param path path to list, or {@code null}
    *
    * @return resource stream
    */
   private Stream<ClasspathResource> directResources(Path path) {
      try {
         return resourceLoader.resources(classpathPrefix.resolve(nullable(path, () -> Path.of(""))));
      } catch (IOException e) {
         throw new DocumentAccessException(e);
      }
   }

   /**
    * Searches for resources in specified base path and matching specification.
    *
    * @param basePath search base path
    * @param specification search specification, should be a {@link DocumentEntrySpecification} for
    *       search optimizations
    * @param mapper mapper for found resources
    *
    * @return found resources as document entries
    */
   private <T> Stream<T> listResources(Path basePath,
                                       Specification<DocumentEntry> specification,
                                       BiFunction<ClasspathResource, DocumentEntry, T> mapper) {
      if (specification instanceof DocumentEntrySpecification) {
         if (!((DocumentEntrySpecification) specification).satisfiedBySubPath(basePath)) {
            return stream();
         }
      }

      return directResources(basePath).flatMap(resource -> {
         Path currentPath = basePath.resolve(requireNonNull(resource.name()));

         try {
            if (resource.isDirectory()) {
               return listResources(currentPath, specification, mapper);
            } else {
               return stream(documentEntry(DocumentPath.of(currentPath)))
                     .filter(specification::satisfiedBy)
                     .map(documentEntry -> mapper.apply(resource, documentEntry));
            }
         } catch (IOException e) {
            throw new DocumentAccessException(e);
         }
      });
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", ClasspathDocumentRepository.class.getSimpleName() + "[", "]")
            .add("classPathPrefix=" + classpathPrefix)
            .toString();
   }
}
