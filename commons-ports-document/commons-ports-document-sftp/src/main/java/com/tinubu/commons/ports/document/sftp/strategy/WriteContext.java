
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.sftp.strategy;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.nio.file.Path;

import org.springframework.integration.file.remote.session.Session;

import com.jcraft.jsch.ChannelSftp.LsEntry;

/**
 * Write context.
 */
public class WriteContext {

   final Session<LsEntry> session;
   private final Path sftpFile;
   private final boolean exists;
   private final boolean overwrite;
   private final boolean append;

   public WriteContext(Session<LsEntry> session,
                       Path sftpFile,
                       boolean exists,
                       boolean overwrite,
                       boolean append) {

      this.session = notNull(session, "session");
      this.sftpFile = notNull(sftpFile, "sftpFile");
      this.exists = exists;
      this.overwrite = overwrite;
      this.append = append;
   }

   /** SFTP session. */
   public Session<LsEntry> session() {
      return session;
   }

   /** Initial target file to write on SFTP server. */
   public Path sftpFile() {
      return sftpFile;
   }

   /** Factorize the information on SFTP file existence. */
   public boolean exists() {
      return exists;
   }

   /**
    * Overwrite mode.
    */
   public boolean overwrite() {
      return overwrite;
   }

   /** Append mode. */
   public boolean append() {
      return append;
   }
}
