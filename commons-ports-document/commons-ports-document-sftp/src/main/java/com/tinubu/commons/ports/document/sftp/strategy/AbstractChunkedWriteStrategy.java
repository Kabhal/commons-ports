/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.sftp.strategy;

import java.io.IOException;

/**
 * Generic write strategy using append of chunks.
 * Memory consumption at any instant can be up to {@value #DEFAULT_APPEND_CHUNK_SIZE}  *
 * {@code number of concurrent SFTP operations}. Number of concurrent SFTP operations is limited by SFTP
 * connection pool size.
 */
public abstract class AbstractChunkedWriteStrategy implements WriteStrategy {

   /**
    * Default append chunk size in bytes.
    */
   private static final int DEFAULT_APPEND_CHUNK_SIZE = 2 * 1024 * 1024;

   private final int chunkSize;
   private byte[] chunk;
   private int chunkLength = 0;

   public AbstractChunkedWriteStrategy(int chunkSize) {
      this.chunkSize = chunkSize;
      chunk = new byte[chunkSize];
   }

   public AbstractChunkedWriteStrategy() {
      this(DEFAULT_APPEND_CHUNK_SIZE);
   }

   @Override
   public void write(byte[] byteArray, int position, int length) throws IOException {
      if (chunkLength + length <= chunkSize) {
         writeChunk(byteArray, position, length);
      } else {
         int fillLength = chunkSize - chunkLength;
         writeChunk(byteArray, position, fillLength);

         flushChunk();

         int remainingLength = length - fillLength;
         writeChunk(byteArray, position + fillLength, remainingLength);
      }
   }

   private void writeChunk(byte[] byteArray, int position, int length) {
      System.arraycopy(byteArray, position, chunk, chunkLength, length);
      chunkLength += length;
   }

   private void flushChunk() throws IOException {
      rawWrite(chunk, 0, chunkLength);
      chunkLength = 0;
   }

   protected abstract void rawWrite(byte[] byteArray, int position, int length) throws IOException;

   @Override
   public void finish() throws IOException {
      flushChunk();
   }
}
