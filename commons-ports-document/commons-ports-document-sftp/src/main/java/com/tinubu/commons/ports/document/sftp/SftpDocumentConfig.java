/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.sftp;

import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.ArrayRules.isNotEmptyBytes;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isGreaterThanOrEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.NumberRules.isStrictlyPositive;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.ports.document.sftp.Constants.DEFAULT_ALLOW_UNKNOWN_HOSTS;
import static com.tinubu.commons.ports.document.sftp.Constants.DEFAULT_BASE_PATH;
import static com.tinubu.commons.ports.document.sftp.Constants.DEFAULT_CONNECT_TIMEOUT;
import static com.tinubu.commons.ports.document.sftp.Constants.DEFAULT_HOST;
import static com.tinubu.commons.ports.document.sftp.Constants.DEFAULT_KEEP_ALIVE_INTERVAL;
import static com.tinubu.commons.ports.document.sftp.Constants.DEFAULT_PORT;
import static com.tinubu.commons.ports.document.sftp.Constants.DEFAULT_SESSION_CACHING;
import static com.tinubu.commons.ports.document.sftp.Constants.DEFAULT_SESSION_POOL_SIZE;
import static com.tinubu.commons.ports.document.sftp.Constants.DEFAULT_SESSION_POOL_WAIT_TIMEOUT;
import static com.tinubu.commons.ports.document.sftp.Constants.DEFAULT_SOCKET_TIMEOUT;
import static com.tinubu.commons.ports.document.sftp.Constants.DEFAULT_USERNAME;
import static com.tinubu.commons.ports.document.sftp.Constants.DEFAULT_WRITE_STRATEGY;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.time.Duration;
import java.util.Optional;

import org.apache.commons.io.IOUtils;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.rules.ClassRules;
import com.tinubu.commons.ddd2.valueformatter.HiddenValueFormatter;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.beans.Setter;
import com.tinubu.commons.lang.util.types.Base64String;
import com.tinubu.commons.ports.document.sftp.strategy.WriteStrategy;

/**
 * SFTP document repository configuration.
 */
// TODO createIfMissingBasePath + failFastIfMissingBasePath ?
public class SftpDocumentConfig extends AbstractValue {

   private final String host;
   private final int port;
   private final boolean allowUnknownHosts;
   private final String username;
   private final String password;
   private final byte[] privateKey;
   private final String privateKeyPassphrase;
   private final Path basePath;
   private final boolean sessionCaching;
   private final int sessionPoolSize;
   private final Duration sessionPoolWaitTimeout;
   private final Duration keepAliveInterval;
   private final Class<? extends WriteStrategy> writeStrategy;
   private final Duration connectTimeout;
   private final Duration socketTimeout;

   public SftpDocumentConfig(SftpDocumentConfigBuilder builder) {
      this.host = nullable(builder.host, DEFAULT_HOST);
      this.port = nullable(builder.port, DEFAULT_PORT);
      this.allowUnknownHosts = nullable(builder.allowUnknownHosts, DEFAULT_ALLOW_UNKNOWN_HOSTS);
      this.username = nullable(builder.username, DEFAULT_USERNAME);
      this.password = builder.password;
      this.privateKey = nullable(builder.privateKey, defaultPrivateKey().orElse(null));
      this.privateKeyPassphrase = builder.privateKeyPassphrase;
      this.basePath = nullable(builder.basePath, DEFAULT_BASE_PATH).normalize();
      this.sessionCaching = nullable(builder.sessionCaching, DEFAULT_SESSION_CACHING);
      this.sessionPoolSize = nullable(builder.sessionPoolSize, DEFAULT_SESSION_POOL_SIZE);
      this.sessionPoolWaitTimeout =
            nullable(builder.sessionPoolWaitTimeout, DEFAULT_SESSION_POOL_WAIT_TIMEOUT);
      this.keepAliveInterval = nullable(builder.keepAliveInterval, DEFAULT_KEEP_ALIVE_INTERVAL);
      this.writeStrategy = nullable(builder.writeStrategy, DEFAULT_WRITE_STRATEGY);
      this.connectTimeout = nullable(builder.connectTimeout, DEFAULT_CONNECT_TIMEOUT);
      this.socketTimeout = nullable(builder.socketTimeout, DEFAULT_SOCKET_TIMEOUT);
   }

   @Override
   public Fields<? extends SftpDocumentConfig> defineDomainFields() {
      return Fields
            .<SftpDocumentConfig>builder()
            .field("host", v -> v.host, isNotBlank())
            .field("port", v -> v.port, isStrictlyPositive())
            .field("allowUnknownHosts", v -> v.allowUnknownHosts)
            .field("username", v -> v.username, isNotBlank())
            .field("password",
                   v -> v.password,
                   new HiddenValueFormatter(),
                   isNotBlank().ifIsSatisfied(() -> privateKey == null))
            .field("privateKey",
                   v -> v.privateKey,
                   new HiddenValueFormatter(),
                   isNull().orValue(isNotEmptyBytes()))
            .field("privateKeyPassphrase",
                   v -> v.privateKeyPassphrase,
                   new HiddenValueFormatter(),
                   isNull().orValue(isNotBlank()))
            .field("basePath", v -> v.basePath, hasNoTraversal())
            .field("sessionCaching", v -> v.sessionCaching)
            .field("sessionPoolSize", v -> v.sessionPoolSize, isStrictlyPositive())
            .field("sessionWaitTimeout", v -> v.sessionPoolWaitTimeout,
                   isGreaterThanOrEqualTo(value(Duration.ZERO)))
            .field("keepAliveInterval",
                   v -> v.keepAliveInterval,
                   isGreaterThanOrEqualTo(value(Duration.ZERO)))
            .field("writeStrategy", v -> v.writeStrategy, ClassRules.isClassOf(value(WriteStrategy.class)))
            .field("connectTimeout", v -> v.connectTimeout, isGreaterThanOrEqualTo(value(Duration.ZERO)))
            .field("socketTimeout", v -> v.socketTimeout, isGreaterThanOrEqualTo(value(Duration.ZERO)))
            .build();
   }

   /**
    * SFTP server host. Default to {@link Constants#DEFAULT_HOST}.
    */
   public String host() {
      return host;
   }

   /**
    * SFTP server port. Default to {@link Constants#DEFAULT_PORT}.
    */
   public Integer port() {
      return port;
   }

   /**
    * Whether to allow unknown server hosts. Default to {@link Constants#DEFAULT_ALLOW_UNKNOWN_HOSTS}.
    */
   public boolean allowUnknownHosts() {
      return this.allowUnknownHosts;
   }

   /**
    * SFTP server authentication username.
    * Default to {@code ${user.name}}.
    */
   public String username() {
      return username;
   }

   /**
    * Optional SFTP server authentication password.
    * Use either privateKey or password for authentication. Use password authentication if both are set.
    */
   public Optional<String> password() {
      return nullable(password);
   }

   /**
    * Optional SFTP server authentication private key.
    * Use either privateKey or password for authentication. Use password authentication if both set.
    * Default to {@code ${user.home}/.ssh/id_rsa}.
    */
   public Optional<byte[]> privateKey() {
      return nullable(privateKey);
   }

   /**
    * Optional SFTP server authentication private key passphrase.
    */
   public Optional<String> privateKeyPassphrase() {
      return nullable(privateKeyPassphrase);
   }

   /**
    * Optional base path to store documents in SFTP server. Can be absolute or relative. If omitted, or set to
    * {@code ""}, the SFTP server
    * default directory for the connected user will be used.
    */
   public Path basePath() {
      return basePath;
   }

   /**
    * Whether to use session caching. Enabled by default.
    *
    * @return whether to use session caching
    */
   public boolean sessionCaching() {
      return sessionCaching;
   }

   /**
    * Optional SFTP caching session pool size. Value must be > 0. Default to
    * {@link Constants#DEFAULT_SESSION_POOL_SIZE}.
    *
    * @return SFTP caching session pool size
    */
   public int sessionPoolSize() {
      return sessionPoolSize;
   }

   /**
    * SFTP session wait timeout for available connection in pool. Timeout is disabled if set
    * to 0. Default to {@link Constants#DEFAULT_SESSION_POOL_WAIT_TIMEOUT}.
    *
    * @return SFTP caching session pool size
    */
   public Duration sessionPoolWaitTimeout() {
      return sessionPoolWaitTimeout;
   }

   /**
    * Keep-alive interval. Value must be greater than or equal to 0.
    * If interval duration is set to 0, keep-alive will be disabled.
    * Default interval is {@link Constants#DEFAULT_KEEP_ALIVE_INTERVAL}.
    *
    * @return keep-alive interval, never {@code null}
    */
   public Duration keepAliveInterval() {
      return keepAliveInterval;
   }

   /**
    * Write strategy to use.
    * Default strategy is {@link Constants#DEFAULT_WRITE_STRATEGY}.
    *
    * @return write strategy, never {@code null}
    */
   public Class<? extends WriteStrategy> writeStrategy() {
      return writeStrategy;
   }

   /**
    * SFTP server connection timeout. If timeout is set to 0, no connection timeout will be applied. Default
    * timeout is {@link Constants#DEFAULT_CONNECT_TIMEOUT}.
    *
    * @return SFTP server socket timeout
    */
   public Duration connectTimeout() {
      return connectTimeout;
   }

   /**
    * SFTP server socket (I/O) timeout. If timeout is set to 0, no socket timeout will be applied. Default
    * timeout is {@link Constants#DEFAULT_SOCKET_TIMEOUT}.
    *
    * @return SFTP server socket timeout
    */
   public Duration socketTimeout() {
      return socketTimeout;
   }

   /**
    * Loads a private key file at specified path.
    *
    * @param privateKeyFile private key file path
    * @param ignoreIfNotFound whether to ignore if specified file does not exist
    *
    * @return private key as byte array, or {@link Optional#empty()} if file does not exist and
    *       {@code ignoreIfNotFound} is set
    *
    * @throws IllegalStateException if file does not exist and {@code ignoreIfNotFound} is note set
    */
   public static Optional<byte[]> privateKey(Path privateKeyFile, boolean ignoreIfNotFound) {
      return nullable(fileContent(privateKeyFile, ignoreIfNotFound));
   }

   private static Optional<byte[]> defaultPrivateKey() {
      return privateKey(Path.of(System.getProperty("user.home")).resolve(".ssh/id_rsa"), true);
   }

   private static byte[] fileContent(Path path, boolean ignoreIfNotFound) {
      try (FileInputStream fis = new FileInputStream(path.toString())) {
         return IOUtils.toByteArray(fis);
      } catch (FileNotFoundException e) {
         if (!ignoreIfNotFound) {
            throw new IllegalStateException(String.format("Unknown '%s' path", path));
         } else {
            return null;
         }
      } catch (IOException e) {
         throw new IllegalStateException(e);
      }
   }

   public static class SftpDocumentConfigBuilder extends DomainBuilder<SftpDocumentConfig> {
      private String host;
      private Integer port;
      private Boolean allowUnknownHosts;
      private String username;
      private String password;
      private byte[] privateKey;
      private String privateKeyPassphrase;
      private Path basePath;
      private Boolean sessionCaching;
      private Integer sessionPoolSize;
      private Duration sessionPoolWaitTimeout;
      private Duration keepAliveInterval;
      private Class<? extends WriteStrategy> writeStrategy;
      private Duration connectTimeout;
      private Duration socketTimeout;

      public static SftpDocumentConfigBuilder from(SftpDocumentConfig config) {
         return new SftpDocumentConfigBuilder()
               .host(config.host)
               .port(config.port)
               .allowUnknownHosts(config.allowUnknownHosts)
               .username(config.username)
               .password(config.password)
               .privateKey(config.privateKey)
               .privateKeyPassphrase(config.privateKeyPassphrase)
               .basePath(config.basePath)
               .sessionCaching(config.sessionCaching)
               .sessionPoolSize(config.sessionPoolSize)
               .sessionPoolWaitTimeout(config.sessionPoolWaitTimeout)
               .keepAliveInterval(config.keepAliveInterval)
               .writeStrategy(config.writeStrategy)
               .connectTimeout(config.connectTimeout)
               .socketTimeout(config.socketTimeout);
      }

      @Getter
      public String host() {
         return host;
      }

      @Setter
      public SftpDocumentConfigBuilder host(String host) {
         this.host = host;
         return this;
      }

      @Getter
      public Integer port() {
         return port;
      }

      @Setter
      public SftpDocumentConfigBuilder port(Integer port) {
         this.port = port;
         return this;
      }

      @Getter
      public Boolean allowUnknownHosts() {
         return allowUnknownHosts;
      }

      @Setter
      public SftpDocumentConfigBuilder allowUnknownHosts(Boolean allowUnknownHosts) {
         this.allowUnknownHosts = allowUnknownHosts;
         return this;
      }

      @Getter
      public String username() {
         return username;
      }

      @Setter
      public SftpDocumentConfigBuilder username(String username) {
         this.username = username;
         return this;
      }

      @Getter
      public String password() {
         return password;
      }

      @Setter
      public SftpDocumentConfigBuilder password(String password) {
         this.password = password;
         return this;
      }

      @Getter
      public byte[] privateKey() {
         return privateKey;
      }

      @Setter
      public SftpDocumentConfigBuilder privateKey(Base64String privateKey) {
         this.privateKey = privateKey.decode();
         return this;
      }

      @Setter
      public SftpDocumentConfigBuilder privateKey(byte[] privateKey) {
         this.privateKey = privateKey;
         return this;
      }

      @Getter
      public String privateKeyPassphrase() {
         return privateKeyPassphrase;
      }

      @Setter
      public SftpDocumentConfigBuilder privateKeyPassphrase(String privateKeyPassphrase) {
         this.privateKeyPassphrase = privateKeyPassphrase;
         return this;
      }

      @Getter
      public Path basePath() {
         return basePath;
      }

      @Setter
      public SftpDocumentConfigBuilder basePath(Path basePath) {
         this.basePath = basePath;
         return this;
      }

      @Getter
      public Boolean sessionCaching() {
         return sessionCaching;
      }

      @Setter
      public SftpDocumentConfigBuilder sessionCaching(Boolean sessionCaching) {
         this.sessionCaching = sessionCaching;
         return this;
      }

      @Getter
      public Integer sessionPoolSize() {
         return sessionPoolSize;
      }

      @Setter
      public SftpDocumentConfigBuilder sessionPoolSize(Integer sessionPoolSize) {
         this.sessionPoolSize = sessionPoolSize;
         return this;
      }

      @Getter
      public Duration sessionPoolWaitTimeout() {
         return sessionPoolWaitTimeout;
      }

      @Setter
      public SftpDocumentConfigBuilder sessionPoolWaitTimeout(Duration sessionPoolWaitTimeout) {
         this.sessionPoolWaitTimeout = sessionPoolWaitTimeout;
         return this;
      }

      @Getter
      public Duration keepAliveInterval() {
         return keepAliveInterval;
      }

      @Setter
      public SftpDocumentConfigBuilder keepAliveInterval(Duration keepAliveInterval) {
         this.keepAliveInterval = keepAliveInterval;
         return this;
      }

      @Getter
      public Class<? extends WriteStrategy> writeStrategy() {
         return writeStrategy;
      }

      @Setter
      public SftpDocumentConfigBuilder writeStrategy(Class<? extends WriteStrategy> writeStrategy) {
         this.writeStrategy = writeStrategy;
         return this;
      }

      @Getter
      public Duration connectTimeout() {
         return connectTimeout;
      }

      @Setter
      public SftpDocumentConfigBuilder connectTimeout(Duration connectTimeout) {
         this.connectTimeout = connectTimeout;
         return this;
      }

      @Getter
      public Duration socketTimeout() {
         return socketTimeout;
      }

      @Setter
      public SftpDocumentConfigBuilder socketTimeout(Duration socketTimeout) {
         this.socketTimeout = socketTimeout;
         return this;
      }

      @Override
      protected SftpDocumentConfig buildDomainObject() {
         return new SftpDocumentConfig(this);
      }
   }
}
