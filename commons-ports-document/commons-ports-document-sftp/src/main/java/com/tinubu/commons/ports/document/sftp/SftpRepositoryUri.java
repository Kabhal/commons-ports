/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.sftp;

import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.ddd2.uri.DefaultUriRestrictions.PortRestriction.REMOVE_MAPPED_PORT;
import static com.tinubu.commons.lang.util.CheckedPredicate.alwaysTrue;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.PathUtils.emptyPath;
import static com.tinubu.commons.lang.util.PathUtils.rootPath;
import static com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery.ExportUriFilter.isAlwaysTrue;
import static com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery.ExportUriFilter.isRegisteredParameter;
import static com.tinubu.commons.ports.document.domain.uri.UriUtils.exportUriFilter;
import static com.tinubu.commons.ports.document.sftp.Constants.DEFAULT_ALLOW_UNKNOWN_HOSTS;
import static com.tinubu.commons.ports.document.sftp.Constants.DEFAULT_CONNECT_TIMEOUT;
import static com.tinubu.commons.ports.document.sftp.Constants.DEFAULT_KEEP_ALIVE_INTERVAL;
import static com.tinubu.commons.ports.document.sftp.Constants.DEFAULT_SESSION_CACHING;
import static com.tinubu.commons.ports.document.sftp.Constants.DEFAULT_SESSION_POOL_SIZE;
import static com.tinubu.commons.ports.document.sftp.Constants.DEFAULT_SESSION_POOL_WAIT_TIMEOUT;
import static com.tinubu.commons.ports.document.sftp.Constants.DEFAULT_SOCKET_TIMEOUT;
import static com.tinubu.commons.ports.document.sftp.Constants.DEFAULT_WRITE_STRATEGY;
import static com.tinubu.commons.ports.document.sftp.SftpRepositoryUri.Parameters.ALLOW_UNKNOWN_HOSTS;
import static com.tinubu.commons.ports.document.sftp.SftpRepositoryUri.Parameters.BASE_PATH;
import static com.tinubu.commons.ports.document.sftp.SftpRepositoryUri.Parameters.CONNECT_TIMEOUT;
import static com.tinubu.commons.ports.document.sftp.SftpRepositoryUri.Parameters.KEEP_ALIVE_INTERVAL;
import static com.tinubu.commons.ports.document.sftp.SftpRepositoryUri.Parameters.PRIVATE_KEY;
import static com.tinubu.commons.ports.document.sftp.SftpRepositoryUri.Parameters.PRIVATE_KEY_PASSPHRASE;
import static com.tinubu.commons.ports.document.sftp.SftpRepositoryUri.Parameters.SESSION_CACHING;
import static com.tinubu.commons.ports.document.sftp.SftpRepositoryUri.Parameters.SESSION_POOL_SIZE;
import static com.tinubu.commons.ports.document.sftp.SftpRepositoryUri.Parameters.SESSION_POOL_WAIT_TIMEOUT;
import static com.tinubu.commons.ports.document.sftp.SftpRepositoryUri.Parameters.SOCKET_TIMEOUT;
import static com.tinubu.commons.ports.document.sftp.SftpRepositoryUri.Parameters.WRITE_STRATEGY;
import static com.tinubu.commons.ports.document.sftp.SftpRepositoryUri.Parameters.allParameters;
import static com.tinubu.commons.ports.document.sftp.SftpRepositoryUri.Parameters.valueParameters;

import java.net.URI;
import java.nio.file.Path;
import java.time.Duration;
import java.util.List;
import java.util.Optional;
import java.util.function.BiPredicate;

import org.apache.commons.lang3.StringUtils;

import com.tinubu.commons.ddd2.domain.repository.Repository;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.domain.type.Id;
import com.tinubu.commons.ddd2.invariant.Validate;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ddd2.uri.parts.SimpleHost;
import com.tinubu.commons.ddd2.uri.parts.SimplePort;
import com.tinubu.commons.ddd2.uri.parts.SimpleServerAuthority;
import com.tinubu.commons.ddd2.uri.parts.UsernamePasswordUserInfo;
import com.tinubu.commons.ddd2.uri.uris.SftpUri;
import com.tinubu.commons.ddd2.uri.uris.SftpUri.SftpPath;
import com.tinubu.commons.ddd2.uri.uris.SftpUri.SftpUriRestrictions;
import com.tinubu.commons.lang.convert.ConversionFailedException;
import com.tinubu.commons.lang.util.Pair;
import com.tinubu.commons.lang.util.PathUtils;
import com.tinubu.commons.lang.util.types.Base64String;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.uri.AbstractRepositoryUri;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.ExportUriOptions;
import com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery;
import com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery.Parameter;
import com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery.ValueParameter;
import com.tinubu.commons.ports.document.domain.uri.RepositoryUri;
import com.tinubu.commons.ports.document.sftp.SftpDocumentConfig.SftpDocumentConfigBuilder;

/**
 * SFTP URI repository URI. This URI must be used to identify a repository, use {@link SftpDocumentUri} to
 * identify a document.
 * <p>
 * The base path will be set to URI path if present, excepting the leading {@code /}, otherwise base path is
 * set to {@code ""}.
 * <p>
 * Supported URI format :
 * <ul>
 *    <li>Relative base path : {@code sftp://[<user>[:<password>]@]<host>[:<port>][/<base-path>][?<parameters>]}</li>
 *    <li>Absolute base path (double {@code //}) : {@code sftp://[<user>[:<password>]@]<host>[:<port>][/</base-path>][?<parameters>]}</li>
 *    <li>Commons-ports wrapped URI : {@code document:sftp:<sftp-uri>}</li>
 * </ul>
 * SFTP URI supports both relative (single {@code /}) and absolute path (double {@code //}). Absolute path is deterministic while relative path
 * is relative to connected user home.
 * <p>
 * For security purpose, relative SFTP path containing traversals is not supported.
 * <p>
 * SFTP URI trailing {@code /} in URI path is kept if it matches current base path.
 * <p>
 * Supported URI parameters :
 * <ul>
 *    <li>{@code basePath} (Path) []</li>
 *    <li>{@code allowUnknownHosts} (Boolean) [{@value Constants#DEFAULT_ALLOW_UNKNOWN_HOSTS}]</li>
 *    <li>{@code privateKey} (String[Base64])</li>
 *    <li>{@code privateKeyPassphrase} (String)</li>
 *    <li>{@code connectTimeout} (Duration[ISO8601]) [0]</li>
 *    <li>{@code keepAliveInterval} (Duration[ISO8601]) [1m] </li>
 *    <li>{@code sessionCaching} (Boolean) [{@value Constants#DEFAULT_SESSION_CACHING}]</li>
 *    <li>{@code sessionPoolSize} (Integer) [{@value Constants#DEFAULT_SESSION_POOL_SIZE}]</li>
 *    <li>{@code sessionPoolWaitTimeout} (Duration[ISO8601]) [0]</li>
 *    <li>{@code socketTimeout} (Duration[ISO8601]) [0]</li>
 *    <li>{@code writeStrategy} (Class&lt;WriteStrategy&gt;[Class FQN]) [com.tinubu.commons.ports.document.sftp.strategy.DefaultChunkedWriteStrategy]</li>
 * </ul>
 *
 * @see SftpDocumentUri
 * @see SftpDocumentConfig
 */
public class SftpRepositoryUri extends AbstractRepositoryUri implements RepositoryUri {

   private static final String WRAPPED_URI_REPOSITORY_TYPE = "sftp";

   /**
    * Feature flag : whether to use {@link #defaultBasePath(Uri, boolean)} to override
    * {@link Parameters#BASE_PATH} default value in {@link #exportUri(ExportUriOptions)}.
    * If set to {@code true}, {@code basePath} parameter won't be exported if it matches default
    * behavior, depending on export options.
    */
   private static final boolean EXPORT_URI_DEFAULT_BASE_PATH_AS_DEFAULT_VALUE = true;

   protected final Path basePath;
   protected final DocumentPath documentId;

   protected SftpRepositoryUri(SftpUri uri, boolean documentUri) {
      super(uri, documentUri);

      try {
         var parsed = parseUri(documentUri);
         this.basePath = parsed.getLeft();
         this.documentId = parsed.getRight();
      } catch (ConversionFailedException e) {
         throw new InvalidUriException(uri, e).subMessage(e);
      }
   }

   @Override
   @SuppressWarnings("unchecked")
   protected Fields<? extends SftpRepositoryUri> defineDomainFields() {
      return Fields
            .<SftpRepositoryUri>builder()
            .superFields((Fields<SftpRepositoryUri>) super.defineDomainFields())
            .field("basePath", v -> v.basePath, hasNoTraversal())
            .build();
   }

   /**
    * Creates a repository URI from {@link Uri}. Specified URI must represent a "repository" URI.
    *
    * @param uri repository URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI has invalid syntax
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static SftpRepositoryUri ofRepositoryUri(Uri uri) {
      Check.notNull(uri, "uri");

      validateUriCompatibility(uri, "uri", isCompatibleUriType(SftpUri.class));

      return buildUri(() -> new SftpRepositoryUri(sftpUri(uri, false), false));
   }

   /**
    * Creates a repository URI from {@link URI}. Specified URI must represent a "repository" URI.
    *
    * @param uri repository URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI has invalid syntax
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static SftpRepositoryUri ofRepositoryUri(URI uri) {
      Check.notNull(uri, "uri");

      return ofRepositoryUri(Uri.ofUri(uri));
   }

   /**
    * Creates a repository URI from URI string. Specified URI must represent a "repository" URI.
    *
    * @param uri repository URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI has invalid syntax
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static SftpRepositoryUri ofRepositoryUri(String uri) {
      Check.notNull(uri, "uri");

      return ofRepositoryUri(Uri.ofUri(uri));
   }

   /**
    * Creates a repository URI from configuration.
    *
    * @param config repository configuration
    *
    * @return new instance
    */
   public static SftpRepositoryUri ofConfig(SftpDocumentConfig config) {
      Check.notNull(config, "config");

      return buildUri(() -> new SftpRepositoryUri(sftpUri(config, null), false));
   }

   /**
    * Creates a repository URI from {@link RepositoryUri}. Specified repository URI can can be a
    * {@link RepositoryUri} to represent a "repository" URI, or a {@link DocumentUri} to represent a
    * "document" URI.
    *
    * @param uri repository URI
    * @param defaultBasePath default base path to use if {@code basePath} query
    *       parameter not present
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    * @implSpec this method is package-private because user code should always create
    *       {@link RepositoryUri} directly from a URI.
    */
   static SftpRepositoryUri ofRepositoryUri(RepositoryUri uri, Path defaultBasePath) {
      Check.notNull(uri, "uri");
      var normalizedDefaultBasePath =
            Check.validate(nullable(defaultBasePath, Path::normalize), "defaultBasePath", hasNoTraversal());

      validateUriCompatibility(uri, "uri", isCompatibleRepositoryUriType(SftpRepositoryUri.class));

      if (uri instanceof SftpRepositoryUri) {
         return buildUri(() -> new SftpRepositoryUri(mapQuery(sftpUri(uri, false), (u, q) -> {
            q.registerParameter(BASE_PATH.defaultValue(((SftpRepositoryUri) uri).basePath()));
            return u;
         }), uri instanceof DocumentUri));
      } else {
         return buildUri(() -> new SftpRepositoryUri(mapQuery(sftpUri(uri, false), (u, q) -> {
            q.registerParameter(BASE_PATH.defaultValue(normalizedDefaultBasePath));
            return u;
         }), uri instanceof DocumentUri));
      }
   }

   /**
    * Creates a repository URI from {@link RepositoryUri}.
    * <p>
    * Default base path is set to {@code ""}.
    *
    * @param uri repository URI
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    * @implSpec this method is package-private because user code should always create
    *       {@link RepositoryUri} directly from a URI.
    */
   static SftpRepositoryUri ofRepositoryUri(RepositoryUri uri) {
      Check.notNull(uri, "uri");

      return ofRepositoryUri(uri, defaultBasePath(uri, uri instanceof DocumentUri));
   }

   public SftpUri sftpUri() {
      return (SftpUri) uri;
   }

   @Override
   public SftpRepositoryUri normalize() {
      return buildUri(() -> new SftpRepositoryUri(sftpUri().normalize(), documentUri));
   }

   @Override
   public SftpRepositoryUri resolve(Uri uri) {
      return buildUri(() -> new SftpRepositoryUri(sftpUri().resolve(uri), documentUri));
   }

   @Override
   public SftpRepositoryUri resolve(URI uri) {
      return buildUri(() -> new SftpRepositoryUri(sftpUri().resolve(uri), documentUri));
   }

   @Override
   public SftpRepositoryUri relativize(Uri uri) {
      throw new UnsupportedOperationException();

   }

   @Override
   public SftpRepositoryUri relativize(URI uri) {
      throw new UnsupportedOperationException();
   }

   @Override
   public int compareTo(Id o) {
      if (!(o instanceof SftpRepositoryUri)) {
         return -1;
      } else if (o == this) {
         return 0;
      } else {
         return sftpUri().compareTo(((SftpRepositoryUri) o).sftpUri());
      }
   }

   /**
    * Returns a pre-configured builder from this repository URI.
    */
   public SftpDocumentConfigBuilder toConfig() {
      return new SftpDocumentConfigBuilder()
            .<SftpDocumentConfigBuilder, String>optionalChain(sftpUri().host(),
                                                              SftpDocumentConfigBuilder::host)
            .<SftpDocumentConfigBuilder, Integer>optionalChain(sftpUri().port(),
                                                               SftpDocumentConfigBuilder::port)
            .<SftpDocumentConfigBuilder, String>optionalChain(sftpUri().username(),
                                                              SftpDocumentConfigBuilder::username)
            .<SftpDocumentConfigBuilder, String>optionalChain(sftpUri().password(),
                                                              SftpDocumentConfigBuilder::password)
            .basePath(basePath)
            .<SftpDocumentConfigBuilder, Boolean>optionalChain(query().parameter(ALLOW_UNKNOWN_HOSTS,
                                                                                 alwaysTrue()),
                                                               SftpDocumentConfigBuilder::allowUnknownHosts)
            .<SftpDocumentConfigBuilder, Base64String>optionalChain(query().parameter(PRIVATE_KEY,
                                                                                      alwaysTrue()),
                                                                    SftpDocumentConfigBuilder::privateKey)
            .<SftpDocumentConfigBuilder, String>optionalChain(query().parameter(PRIVATE_KEY_PASSPHRASE,
                                                                                alwaysTrue()),
                                                              SftpDocumentConfigBuilder::privateKeyPassphrase)
            .<SftpDocumentConfigBuilder, Duration>optionalChain(query().parameter(CONNECT_TIMEOUT,
                                                                                  alwaysTrue()),
                                                                SftpDocumentConfigBuilder::connectTimeout)
            .<SftpDocumentConfigBuilder, Duration>optionalChain(query().parameter(SOCKET_TIMEOUT,
                                                                                  alwaysTrue()),
                                                                SftpDocumentConfigBuilder::socketTimeout)
            .<SftpDocumentConfigBuilder, Duration>optionalChain(query().parameter(KEEP_ALIVE_INTERVAL,
                                                                                  alwaysTrue()),
                                                                SftpDocumentConfigBuilder::keepAliveInterval)
            .<SftpDocumentConfigBuilder, Boolean>optionalChain(query().parameter(SESSION_CACHING,
                                                                                 alwaysTrue()),
                                                               SftpDocumentConfigBuilder::sessionCaching)
            .<SftpDocumentConfigBuilder, Integer>optionalChain(query().parameter(SESSION_POOL_SIZE,
                                                                                 alwaysTrue()),
                                                               SftpDocumentConfigBuilder::sessionPoolSize)
            .<SftpDocumentConfigBuilder, Duration>optionalChain(query().parameter(SESSION_POOL_WAIT_TIMEOUT,
                                                                                  alwaysTrue()),
                                                                SftpDocumentConfigBuilder::sessionPoolWaitTimeout)
            .<SftpDocumentConfigBuilder, Class>optionalChain(query().parameter(WRITE_STRATEGY, alwaysTrue()),
                                                             SftpDocumentConfigBuilder::writeStrategy);
   }

   public Path basePath() {
      return basePath;
   }

   @Override
   public boolean supportsUri(RepositoryUri uri) {
      Check.notNull(uri, "uri");

      return supportsRepositoryUri(uri, SftpRepositoryUri.class, SftpRepositoryUri::ofRepositoryUri)
            .map(sftpUri -> {

               if (!sftpUri.sftpUri().username().equals(sftpUri().username())) {
                  return false;
               }
               if (!sftpUri.sftpUri().host().equals(sftpUri().host())) {
                  return false;
               }
               if (!sftpUri.sftpUri().port().equals(sftpUri().port())) {
                  return false;
               }
               if (!sftpUri.basePath().equals(basePath())) {
                  return false;
               }

               return mapQuery(sftpUri(),
                               (u, q) -> q.includeParameters(query(sftpUri.sftpUri()),
                                                             isRegisteredParameter()));
            })
            .orElse(false);
   }

   /**
    * {@inheritDoc}
    *
    * @implSpec if original repository URI path ends with {@code /} and matches current base path,
    *       trailing {@code /} is kept in URI path, but not in {@code basePath} parameter which is always
    *       normalized.
    *       Username is preserved in exported URI even if
    *       {@link ExportUriOptions#excludeSensitiveParameters()} is set, because username is part of
    *       repository {@link SftpDocumentRepository#sameRepositoryAs(Repository) identity}.
    */
   @Override
   public URI exportURI(ExportUriOptions options) {
      Check.notNull(options, "options");

      var filter = exportUriFilter(options, isAlwaysTrue());
      var exportQuery = exportQuery(filter).noQueryIfEmpty(true);
      var exportUri = sftpUri()
            .sftpRestrictions(r -> r.portRestriction(REMOVE_MAPPED_PORT))
            .component()
            .query(__ -> exportQuery);

      var exportPath = exportPath();
      if (exportPath.isPresent()) {
         exportUri = exportUri.component().path(__ -> exportPath.get());
      }

      if (options.excludeSensitiveParameters()) {
         exportUri = exportUri.component().conditionalUserInfo(nui -> {
            return nullable(nui)
                  .map(UsernamePasswordUserInfo.class::cast)
                  .map(ui -> UsernamePasswordUserInfo.of(ui.username(true), null))
                  .orElse(null);
         });
      }

      return exportUri.toURI();
   }

   private ParameterizedQuery exportQuery(BiPredicate<ParameterizedQuery, Parameter> filter) {
      var exportQuery = query();

      if (EXPORT_URI_DEFAULT_BASE_PATH_AS_DEFAULT_VALUE) {
         var basePathParameter = BASE_PATH.defaultValue(defaultBasePath(sftpUri(), documentUri));
         exportQuery = exportQuery.registerParameter(ValueParameter.ofValue(basePathParameter, basePath));
      }

      return exportQuery.exportQuery(filter);
   }

   /**
    * Generates export URI path.
    * To preserve trailing slashes fom original URI, this operation can return {@link Optional#empty()} to
    * keep original path if it is already correct.
    *
    * @return path to be set in export URI, or {@link Optional#empty()} if original URI path must be kept
    *       as-is (not updated).
    */
   private Optional<SftpPath> exportPath() {
      SftpPath exportPath = null;

      if (documentId != null) {
         exportPath = SftpPath.ofSftpPath(basePath.resolve(documentId.value()).toString());
      } else {
         var originalBasePath = sftpUri().component().sftpPath().sftpPathValue();
         if (!originalBasePath.equals(basePath)) {
            exportPath = SftpPath.ofSftpPath(basePath);
         }
      }

      return nullable(exportPath);
   }

   @Override
   public RepositoryUri exportUri(ExportUriOptions options) {
      return RepositoryUri.ofRepositoryUri(exportURI(options));
   }

   /**
    * Creates a parameterized {@link SftpUri} from specified {@link Uri}.
    * <p>
    * If specified URI is wrapped, it is unwrapped before creating parameterized URI.
    * Specified URI is normalized and checked for remaining traversals, after unwrapping.
    *
    * @param uri URI
    * @param documentUri whether URI is a document URI
    *
    * @return new parameterized URI, with all registered parameters
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    * @implNote URI normalization must be done after conversion to specialized Uri to benefit from
    *       specialized normalization.
    */
   protected static SftpUri sftpUri(Uri uri, boolean documentUri) {
      Check.notNull(uri, "uri");
      var unwrappedUri = unwrapUri(uri, WRAPPED_URI_REPOSITORY_TYPE);

      return parameterizedQueryUri(unwrappedUri, defaultRestrictions(documentUri),
                                   SftpUri::ofUri,
                                   allParameters());
   }

   protected static SftpUriRestrictions defaultRestrictions(boolean documentUri) {
      return SftpUriRestrictions
            .ofDefault()
            .portRestriction(REMOVE_MAPPED_PORT)
            .emptyPath(documentUri)
            .query(false);
   }

   /**
    * Creates a parameterized URI from specified configuration.
    *
    * @param config repository configuration
    * @param documentId optional document id
    *
    * @return new parameterized URI, with all registered value parameters from configuration
    */
   protected static SftpUri sftpUri(SftpDocumentConfig config, DocumentPath documentId) {
      return SftpUri
            .hierarchical(SimpleServerAuthority.of(UsernamePasswordUserInfo.of(config.username(),
                                                                               config
                                                                                     .password()
                                                                                     .orElse(null)),
                                                   SimpleHost.of(config.host()),
                                                   SimplePort.of(config.port())),
                          SftpPath.ofSftpPath((documentId != null ? config
                                .basePath()
                                .resolve(documentId.value()) : config.basePath()).toString()),
                          ParameterizedQuery
                                .ofEmpty()
                                .noQueryIfEmpty(true)
                                .registerParameters(valueParameters(config)))
            .restrictions(defaultRestrictions(documentId != null));
   }

   /**
    * Default base path value used to complement generic {@link DocumentUri} or
    * {@link RepositoryUri}.
    * Default base path is set to :
    * <ul>
    *    <li>document URI : {@code sftp://host/path/document} -&gt; {@code ""}</li>
    *    <li>document URI : {@code sftp://host//path/document} -&gt; {@code "/"}</li>
    *    <li>repository URI : {@code sftp://host} -&gt; {@code ""}</li>
    *    <li>repository URI : {@code sftp://host/} -&gt; {@code ""}</li>
    *    <li>repository URI : {@code sftp://host//} -&gt; {@code "/"}</li>
    *    <li>repository URI : {@code sftp://host/path/subpath} -&gt; {@code "path/subpath"}</li>
    *    <li>repository URI : {@code sftp://host//path/subpath} -&gt; {@code "/path/subpath"}</li>
    * </ul>
    *
    * @param uri generic URI
    * @param documentId whether URI is a document URI
    *
    * @return base path
    */
   protected static Path defaultBasePath(Uri uri, boolean documentId) {
      var path = uri.path(false);
      if (documentId) {
         if (path.isPresent() && path.get().startsWith("//")) {
            return rootPath();
         } else {
            return emptyPath();
         }
      } else {
         return Path.of(StringUtils.removeStart(path.orElse("/"), "/"));
      }
   }

   /**
    * Checks URI syntax and parses current parameterized URI to separate the base path, from the
    * document path.
    *
    * @param documentUri whether the current parameterized URI is a document URI, identifying a document
    *       instead of a repository
    *
    * @return parsed (base path, document path) pair
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @implSpec The minimum set of parameters are checked here for consistency, only the parameters
    *       used in class internal state : base path, document path. Other parameters
    *       could be checked later, e.g. in {@link #toConfig()}.
    *       <p>
    *       Default base path value is only applied if it starts actual URI path, otherwise,
    *       it is discarded because it's a "default", not an explicit value for actual URI, and it could apply
    *       to whatever URI is passed in.
    */
   protected Pair<Path, DocumentPath> parseUri(boolean documentUri) {
      var uriPath = optional(sftpUri().sftpPathValue()).filter(p -> !PathUtils.isEmpty(p));

      if (documentUri && uriPath.map(PathUtils::isRoot).orElse(true)) {
         throw new InvalidUriException(sftpUri()).subMessage("Missing document id");
      }

      @SuppressWarnings("unchecked")
      var basePathParameter = query()
            .parameter(BASE_PATH,
                       bp -> ((Parameter<Path>) bp)
                             .defaultValue()
                             .map(defaultBasePath -> uriPath
                                   .map(u -> PathUtils.startsWith(u, defaultBasePath)).orElse(true))
                             .orElse(false))
            .orElseGet(() -> defaultBasePath(sftpUri(), documentUri))
            .normalize();

      Validate
            .satisfies(basePathParameter, hasNoTraversal())
            .orThrowMessage(message -> new InvalidUriException(sftpUri()).subMessage(
                  "'%s=%s' parameter must not contain traversal",
                  BASE_PATH.key(),
                  basePathParameter.toString()));

      Path basePath;
      DocumentPath documentId;

      if (!documentUri) {
         basePath = uriPath.orElse(basePathParameter);
         if (!PathUtils.equals(basePath, basePathParameter)) {
            throw new InvalidUriException(sftpUri()).subMessage("'%s=%s' parameter must match URI '%s' path",
                                                                Parameters.BASE_PATH.key(),
                                                                basePathParameter.toString(),
                                                                basePath.toString());
         }
         documentId = null;
      } else {
         basePath = basePathParameter;
         var documentPath = uriPath.get();

         if (!PathUtils.startsWith(documentPath, basePath) || documentPath.equals(basePath)) {
            throw new InvalidUriException(sftpUri()).subMessage(
                  "'%s=%s' parameter must start but not be equal to URI '%s' path",
                  Parameters.BASE_PATH.key(),
                  basePath.toString(),
                  documentPath.toString());
         }
         documentId = DocumentPath.of(basePath.relativize(documentPath));
      }

      return Pair.of(basePath, documentId);
   }

   /**
    * Supported parameter registry.
    */
   static class Parameters {
      static final Parameter<Boolean> ALLOW_UNKNOWN_HOSTS = Parameter
            .registered("allowUnknownHosts", Boolean.class)
            .defaultValue(DEFAULT_ALLOW_UNKNOWN_HOSTS);
      static final Parameter<Base64String> PRIVATE_KEY =
            Parameter.registered("privateKey", Base64String.class, true);
      static final Parameter<String> PRIVATE_KEY_PASSPHRASE =
            Parameter.registered("privateKeyPassphrase", String.class, true);
      static final Parameter<Path> BASE_PATH = Parameter.registered("basePath", Path.class);
      static final Parameter<Duration> CONNECT_TIMEOUT =
            Parameter.registered("connectTimeout", Duration.class).defaultValue(DEFAULT_CONNECT_TIMEOUT);
      static final Parameter<Duration> KEEP_ALIVE_INTERVAL = Parameter
            .registered("keepAliveInterval", Duration.class)
            .defaultValue(DEFAULT_KEEP_ALIVE_INTERVAL);
      static final Parameter<Boolean> SESSION_CACHING =
            Parameter.registered("sessionCaching", Boolean.class).defaultValue(DEFAULT_SESSION_CACHING);
      static final Parameter<Integer> SESSION_POOL_SIZE =
            Parameter.registered("sessionPoolSize", Integer.class).defaultValue(DEFAULT_SESSION_POOL_SIZE);
      static final Parameter<Duration> SESSION_POOL_WAIT_TIMEOUT = Parameter
            .registered("sessionPoolWaitTimeout", Duration.class)
            .defaultValue(DEFAULT_SESSION_POOL_WAIT_TIMEOUT);
      static final Parameter<Duration> SOCKET_TIMEOUT =
            Parameter.registered("socketTimeout", Duration.class).defaultValue(DEFAULT_SOCKET_TIMEOUT);
      @SuppressWarnings("rawtypes")
      static final Parameter<Class> WRITE_STRATEGY =
            Parameter.registered("writeStrategy", Class.class).defaultValue(DEFAULT_WRITE_STRATEGY);

      @SuppressWarnings("rawtypes")
      static List<Parameter> allParameters() {
         return list(ALLOW_UNKNOWN_HOSTS,
                     PRIVATE_KEY,
                     PRIVATE_KEY_PASSPHRASE,
                     BASE_PATH,
                     CONNECT_TIMEOUT,
                     KEEP_ALIVE_INTERVAL,
                     SESSION_CACHING,
                     SESSION_POOL_SIZE,
                     SESSION_POOL_WAIT_TIMEOUT,
                     SOCKET_TIMEOUT,
                     WRITE_STRATEGY);
      }

      @SuppressWarnings("rawtypes")
      static List<ValueParameter> valueParameters(SftpDocumentConfig config) {
         return list(ValueParameter.ofValue(ALLOW_UNKNOWN_HOSTS, config.allowUnknownHosts()),
                     ValueParameter.ofValue(PRIVATE_KEY,
                                            config.privateKey().map(Base64String::encode).orElse(null)),
                     ValueParameter.ofValue(PRIVATE_KEY_PASSPHRASE,
                                            config.privateKeyPassphrase().orElse(null)),
                     ValueParameter.ofValue(BASE_PATH, config.basePath()),
                     ValueParameter.ofValue(CONNECT_TIMEOUT, config.connectTimeout()),
                     ValueParameter.ofValue(KEEP_ALIVE_INTERVAL, config.keepAliveInterval()),
                     ValueParameter.ofValue(SESSION_CACHING, config.sessionCaching()),
                     ValueParameter.ofValue(SESSION_POOL_SIZE, config.sessionPoolSize()),
                     ValueParameter.ofValue(SESSION_POOL_WAIT_TIMEOUT, config.sessionPoolWaitTimeout()),
                     ValueParameter.ofValue(SOCKET_TIMEOUT, config.socketTimeout()),
                     ValueParameter.ofValue(WRITE_STRATEGY, config.writeStrategy()));
      }

   }

}
