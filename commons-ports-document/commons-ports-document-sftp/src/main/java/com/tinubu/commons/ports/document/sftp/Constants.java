/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.sftp;

import static com.tinubu.commons.lang.util.PathUtils.emptyPath;

import java.nio.file.Path;
import java.time.Duration;

import com.tinubu.commons.ports.document.sftp.strategy.DefaultChunkedWriteStrategy;
import com.tinubu.commons.ports.document.sftp.strategy.WriteStrategy;

public final class Constants {
   private Constants() {
   }

   public static final String DEFAULT_HOST = "localhost";
   public static final int DEFAULT_PORT = 22;
   public static final Path DEFAULT_BASE_PATH = emptyPath();
   /** Whether to allow unknown server hosts by default. */
   public static final boolean DEFAULT_ALLOW_UNKNOWN_HOSTS = false;
   public static final String DEFAULT_USERNAME = System.getProperty("user.name");
   /**
    * Whether to enable SFTP caching by default.
    */
   public static final boolean DEFAULT_SESSION_CACHING = true;
   /**
    * Default SFTP caching session pool size. Value must be > 0.
    */
   public static final int DEFAULT_SESSION_POOL_SIZE = 10;
   /**
    * Default session wait timeout for available connection in pool. Session wait timeout disabled if
    * set to 0.
    */
   public static final Duration DEFAULT_SESSION_POOL_WAIT_TIMEOUT = Duration.ZERO;
   /** Keep-alive interval, before an alive message is sent to the server. */
   public static final Duration DEFAULT_KEEP_ALIVE_INTERVAL = Duration.ofMinutes(1);
   /** Default write strategy. */
   public static final Class<? extends WriteStrategy> DEFAULT_WRITE_STRATEGY =
         DefaultChunkedWriteStrategy.class;
   /**
    * Default connection timeout. Connection timeout disabled if set to 0.
    */
   public static final Duration DEFAULT_CONNECT_TIMEOUT = Duration.ZERO;
   /**
    * Default socket (I/O) timeout. Socket timeout disabled if set to 0.
    */
   public static final Duration DEFAULT_SOCKET_TIMEOUT = Duration.ZERO;

}
