/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.sftp;

import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.ports.document.sftp.SftpRepositoryUri.Parameters.BASE_PATH;

import java.net.URI;
import java.nio.file.Path;

import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.domain.type.Id;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ddd2.uri.uris.SftpUri;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.ExportUriOptions;

/**
 * SFTP URI document URI. This URI must be used to identify a document, use {@link SftpRepositoryUri} to
 * identify a repository.
 * <p>
 * You can use this class when a {@link SftpRepositoryUri} is required, but your current URI is a document
 * URI. In this case it's important that the base path is set, or it will be set by default to {@code ""}
 * for relative paths, or {@code /} for absolute paths.
 * Base path is read from {@code basePath} URI query string parameter.
 * <p>
 * Supported URI format :
 * <ul>
 *    <li>Relative base path : {@code sftp://[<user>[:<password>]@]<host>[:<port>][/<base-path>]/<document-id>[?<parameters>]}</li>
 *    <li>Absolute base path : {@code sftp://[<user>[:<password>]@]<host>[:<port>][/</base-path>]/<document-id>[?<parameters>]}</li>
 *    <li>Commons-ports wrapped URI : {@code document:sftp:<sftp-uri>}</li>
 * </ul>
 * SFTP URI supports both relative (single {@code /}) and absolute path (double {@code //}). Absolute path is deterministic while relative path
 * is relative to connected user home.
 * <p>
 * For security purpose, relative SFTP path containing traversals is not supported.
 *
 * @see SftpRepositoryUri
 * @see SftpDocumentConfig
 */
public class SftpDocumentUri extends SftpRepositoryUri implements DocumentUri {

   protected SftpDocumentUri(SftpUri uri) {
      super(uri, true);
   }

   @Override
   @SuppressWarnings("unchecked")
   protected Fields<? extends SftpDocumentUri> defineDomainFields() {
      return Fields
            .<SftpDocumentUri>builder()
            .superFields((Fields<SftpDocumentUri>) super.defineDomainFields())
            .field("documentId", v -> v.documentId, isNotNull())
            .build();
   }

   /**
    * Creates a document URI from {@link Uri}.
    *
    * @param uri document URI
    * @param defaultBasePath default base path to use if {@code basePath} query
    *       parameter not present
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI has invalid syntax
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static SftpDocumentUri ofDocumentUri(Uri uri, Path defaultBasePath) {
      Check.notNull(uri, "uri");
      var normalizedDefaultBasePath =
            Check.validate(nullable(defaultBasePath, Path::normalize), "defaultBasePath", hasNoTraversal());

      return buildUri(() -> new SftpDocumentUri(mapQuery(sftpUri(uri, true), (u, q) -> {
         q.registerParameter(BASE_PATH.defaultValue(normalizedDefaultBasePath));
         return u;
      })));
   }

   /**
    * Creates a document URI from {@link Uri}.
    * <p>
    * Default base path is set to {@code ""}.
    *
    * @param uri document URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI has invalid syntax
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static SftpDocumentUri ofDocumentUri(Uri uri) {
      Check.notNull(uri, "uri");

      validateUriCompatibility(uri, "uri", isCompatibleUriType(SftpUri.class));

      return ofDocumentUri(uri, defaultBasePath(uri, true));
   }

   /**
    * Creates a document URI from {@link URI}.
    *
    * @param uri document URI
    * @param defaultBasePath default base path to use if {@code basePath} query
    *       parameter not present
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI has invalid syntax
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static SftpDocumentUri ofDocumentUri(URI uri, Path defaultBasePath) {
      Check.notNull(uri, "uri");
      Check.notNull(defaultBasePath, "defaultBasePath");

      return ofDocumentUri(Uri.ofUri(uri), defaultBasePath);
   }

   /**
    * Creates a document URI from {@link URI}.
    * <p>
    * Default base path is set to {@code ""}.
    *
    * @param uri document URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI has invalid syntax
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static SftpDocumentUri ofDocumentUri(URI uri) {
      Check.notNull(uri, "uri");

      return ofDocumentUri(Uri.ofUri(uri));
   }

   /**
    * Creates a document URI from URI string.
    *
    * @param uri document URI
    * @param defaultBasePath default base path to use if {@code basePath} query
    *       parameter not present
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI has invalid syntax
    */
   public static SftpDocumentUri ofDocumentUri(String uri, Path defaultBasePath) {
      Check.notNull(uri, "uri");

      return ofDocumentUri(Uri.uri(uri), defaultBasePath);
   }

   /**
    * Creates a document URI from URI string.
    * <p>
    * Default base path is set to {@code ""}.
    *
    * @param uri document URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI has invalid syntax
    */
   public static SftpDocumentUri ofDocumentUri(String uri) {
      Check.notNull(uri, "uri");

      return ofDocumentUri(Uri.uri(uri));
   }

   /**
    * Creates a document URI from configuration.
    *
    * @param config repository configuration
    * @param documentId document id
    *
    * @return new instance
    */
   public static SftpDocumentUri ofConfig(SftpDocumentConfig config, DocumentPath documentId) {
      Check.notNull(config, "config");

      return buildUri(() -> new SftpDocumentUri(sftpUri(config, documentId)));
   }

   /**
    * Creates a document URI from {@link DocumentUri}.
    *
    * @param uri document URI
    * @param defaultBasePath default base path to use if {@code basePath} query
    *       parameter not present
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    * @implSpec this method is package-private because user code should always create
    *       {@link DocumentUri} directly from a URI.
    */
   static SftpDocumentUri ofDocumentUri(DocumentUri uri, Path defaultBasePath) {
      Check.notNull(uri, "uri");
      var normalizedDefaultBasePath =
            Check.validate(nullable(defaultBasePath, Path::normalize), "defaultBasePath", hasNoTraversal());

      validateUriCompatibility(uri, "uri", isCompatibleDocumentUriType(SftpDocumentUri.class));

      if (uri instanceof SftpDocumentUri) {
         return buildUri(() -> new SftpDocumentUri(mapQuery(sftpUri(uri, true), (u, q) -> {
            q.registerParameter(BASE_PATH.defaultValue(((SftpDocumentUri) uri).basePath()));
            return u;
         })));
      } else {
         return buildUri(() -> new SftpDocumentUri(mapQuery(sftpUri(uri, true), (u, q) -> {
            q.registerParameter(BASE_PATH.defaultValue(normalizedDefaultBasePath));
            return u;
         })));
      }
   }

   /**
    * Creates a document URI from {@link DocumentUri}.
    * <p>
    * Default base path is set to {@code ""}.
    *
    * @param uri document URI
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    * @implSpec this method is package-private because user code should always create
    *       {@link DocumentUri} directly from a URI.
    */
   static SftpDocumentUri ofDocumentUri(DocumentUri uri) {
      Check.notNull(uri, "uri");

      return ofDocumentUri(uri, defaultBasePath(uri, true));
   }

   public DocumentPath documentId() {
      return documentId;
   }

   @Override
   public DocumentUri exportUri(ExportUriOptions options) {
      return DocumentUri.ofDocumentUri(exportURI(options));
   }

   @Override
   public SftpDocumentUri normalize() {
      return buildUri(() -> new SftpDocumentUri(sftpUri().normalize()));
   }

   @Override
   public SftpDocumentUri resolve(Uri uri) {
      return buildUri(() -> new SftpDocumentUri(sftpUri().resolve(uri)));
   }

   @Override
   public SftpDocumentUri resolve(URI uri) {
      return buildUri(() -> new SftpDocumentUri(sftpUri().resolve(uri)));
   }

   @Override
   public SftpDocumentUri relativize(Uri uri) {
      throw new UnsupportedOperationException();
   }

   @Override
   public SftpDocumentUri relativize(URI uri) {
      throw new UnsupportedOperationException();
   }

   @Override
   public int compareTo(Id o) {
      if (!(o instanceof SftpDocumentUri)) {
         return -1;
      } else {
         return super.compareTo(o);
      }
   }

}
