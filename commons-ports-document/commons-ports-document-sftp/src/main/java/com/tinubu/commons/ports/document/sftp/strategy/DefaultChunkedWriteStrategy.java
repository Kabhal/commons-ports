/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.sftp.strategy;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.file.Path;

import com.tinubu.commons.ports.document.sftp.SftpOperations;

/**
 * Write strategy that assumes that the SFTP server supports transactional operations. Caution, no
 * special protection is enforced, so that operations can result in corrupted files on non-transactional
 * SFTP servers, for example if an append occurs on a file being deleted in the meantime.
 * The strategy will append multiple chunks of data to the target SFTP file.
 */
public class DefaultChunkedWriteStrategy extends AbstractChunkedWriteStrategy {

   private final WriteContext writeContext;

   public DefaultChunkedWriteStrategy(WriteContext writeContext, int chunkSize) {
      super(chunkSize);
      this.writeContext = notNull(writeContext, "writeContext");
   }

   public DefaultChunkedWriteStrategy(WriteContext writeContext) {
      this.writeContext = notNull(writeContext, "writeContext");
   }

   @Override
   public void start() throws IOException {
      if (writeContext.overwrite() && writeContext.exists() && !writeContext.append()) {
         writeContext.session().remove(writeContext.sftpFile().toString());
      }
      if (!(writeContext.append() && writeContext.exists())) {
         createSftpFile();
      }
   }

   @Override
   public void rawWrite(byte[] byteArray, int position, int length) throws IOException {
      writeContext
            .session()
            .append(new ByteArrayInputStream(byteArray, position, length),
                    writeContext.sftpFile().toString());
   }

   @Override
   public void finish() throws IOException {
      super.finish();
   }

   private void createSftpFile() throws IOException {
      createParentFolders(writeContext.sftpFile());
      writeContext
            .session()
            .write(new ByteArrayInputStream(new byte[0], 0, 0), writeContext.sftpFile().toString());
   }

   /**
    * Creates missing parent directories recursively for specified sftp file.
    */
   private void createParentFolders(Path sftpFile) throws IOException {
      if (sftpFile.getParent() != null) {
         SftpOperations.sftpMkdirs(writeContext.session(), sftpFile.getParent());
      }
   }
}
