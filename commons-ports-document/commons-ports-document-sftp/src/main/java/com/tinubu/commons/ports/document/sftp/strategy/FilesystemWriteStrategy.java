/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.sftp.strategy;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;

import com.tinubu.commons.ports.document.sftp.SftpOperations;

/**
 * Write strategy for SFTP servers that does not support SFTP append mode, so that chunked strategy can't be
 * used. Content is stored in a local temporary file before being sent in one step. Memory consumption is
 * reduced as content is directly streamed from filesystem.
 * Note that if SFTP server does not support append mode and write context set append, then behavior is
 * non-deterministic.
 */
public class FilesystemWriteStrategy implements WriteStrategy {

   private final WriteContext writeContext;

   private File intermediateFile;
   private FileOutputStream outputStream;

   public FilesystemWriteStrategy(WriteContext writeContext) {
      this.writeContext = notNull(writeContext, "writeContext");
   }

   @Override
   public void start() throws IOException {
      this.intermediateFile = File.createTempFile("commons-ports-write-strategy", null);
      this.outputStream = new FileOutputStream(intermediateFile);
   }

   @Override
   public void write(byte[] byteArray, int position, int length) throws IOException {
      outputStream.write(byteArray, position, length);
   }

   @Override
   public void finish() throws IOException {
      try {
         outputStream.close();
         createParentFolders(writeContext.sftpFile());
         try (InputStream intermediateInputStream = Files.newInputStream(intermediateFile.toPath())) {
            if (writeContext.append()) {
               writeContext.session().append(intermediateInputStream, writeContext.sftpFile().toString());
            } else {
               writeContext.session().write(intermediateInputStream, writeContext.sftpFile().toString());
            }
         }
      } finally {
         Files.delete(intermediateFile.toPath());
      }
   }

   /**
    * Creates missing parent directories recursively for specified sftp file.
    */
   private void createParentFolders(Path sftpFile) throws IOException {
      if (sftpFile.getParent() != null) {
         SftpOperations.sftpMkdirs(writeContext.session(), sftpFile.getParent());
      }
   }
}
