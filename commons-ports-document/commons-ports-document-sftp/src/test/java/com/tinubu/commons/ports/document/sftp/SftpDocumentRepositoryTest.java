/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.sftp;

import static com.tinubu.commons.ddd2.uri.Uri.uri;
import static com.tinubu.commons.ports.document.domain.testsuite.BaseDocumentRepositoryTest.documentUri;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.defaultParameters;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.sensitiveParameters;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatIllegalStateException;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.net.URI;
import java.nio.file.Path;
import java.time.Duration;

import org.apache.sshd.server.SshServer;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.sftp.SftpDocumentConfig.SftpDocumentConfigBuilder;
import com.tinubu.commons.ports.document.sftp.strategy.DefaultChunkedWriteStrategy;
import com.tinubu.commons.ports.document.sftp.strategy.FilesystemWriteStrategy;
import com.tinubu.commons.ports.document.sftp.strategy.NonTransactionalChunkedServerStrategy;

public class SftpDocumentRepositoryTest {

   /**
    * Test suite for general tests.
    */
   @Nested
   public class WhenNominal extends AbstractSftpDocumentRepositoryTest {

      @Override
      public SftpDocumentConfigBuilder sftpDocumentConfig(SshServer server) {
         return super
               .sftpDocumentConfig(server)
               .sessionPoolSize(4)
               .sessionPoolWaitTimeout(Duration.ofSeconds(5))
               .connectTimeout(Duration.ofSeconds(1))
               .socketTimeout(Duration.ofSeconds(1));
      }

      @Test
      public void testBadLibraryUsageWhenUnclosedDocumentContent() {
         Path documentPath = path("path/pathfile1.pdf");
         DocumentPath testDocument = DocumentPath.of(documentPath);

         try {
            createDocuments(documentPath);

            documentRepository().findDocumentById(testDocument);
            documentRepository().findDocumentById(testDocument);

         } finally {
            deleteDocuments(documentPath);
         }

         assertThatIllegalStateException()
               .isThrownBy(this::closeDocumentRepository)
               .withMessage(
                     "2 active sessions not released in caching pool on repository close, reasons can be : bad library usage => you MUST close (try-with-resources) EVERY DocumentRepository instances, and EVERY returned (Stream<Document|DocumentEntry> streams, or Document content's inputStream/outputStream)");
      }

      @Test
      public void testBadLibraryUsageWhenUnclosedDocumentStream() {
         Path documentPath = path("path/pathfile1.pdf");

         try {
            createDocuments(documentPath);

            documentRepository().findDocumentsBySpecification(__ -> true);
            documentRepository().findDocumentEntriesBySpecification(__ -> true);
         } finally {
            deleteDocuments(documentPath);
         }

         assertThatIllegalStateException()
               .isThrownBy(this::closeDocumentRepository)
               .withMessage(
                     "2 active sessions not released in caching pool on repository close, reasons can be : bad library usage => you MUST close (try-with-resources) EVERY DocumentRepository instances, and EVERY returned (Stream<Document|DocumentEntry> streams, or Document content's inputStream/outputStream)");
      }

      @Test
      public void testBadLibraryUsageWhenUnclosedDocumentAndFullPool() {
         Path documentPath = path("path/pathfile1.pdf");
         DocumentPath testDocument = DocumentPath.of(documentPath);

         try {
            createDocuments(documentPath);

            Document d1 =
                  documentRepository().findDocumentById(testDocument).orElseThrow(IllegalStateException::new);
            Document d2 =
                  documentRepository().findDocumentById(testDocument).orElseThrow(IllegalStateException::new);
            Document d3 =
                  documentRepository().findDocumentById(testDocument).orElseThrow(IllegalStateException::new);
            Document d4 =
                  documentRepository().findDocumentById(testDocument).orElseThrow(IllegalStateException::new);

            try {
               assertThatExceptionOfType(DocumentAccessException.class)
                     .isThrownBy(() -> documentRepository().findDocumentById(testDocument))
                     .withMessage(
                           "Timed out while waiting to acquire a pool entry, reasons can be : too much concurrent SFTP operations on the same repository => increase pool size in configuration | bad library usage => you MUST close (try-with-resources) EVERY DocumentRepository instances, and EVERY returned (Stream<Document|DocumentEntry> streams, or Document content's inputStream/outputStream)");
            } finally { // ensure deleteDocuments won't override failure if assertion fails
               d1.content().close();
               d2.content().close();
               d3.content().close();
               d4.content().close();
            }

         } finally {
            deleteDocuments(documentPath);
         }
      }

   }

   /**
    * Test suite with a non-caching session factory.
    */
   @Nested
   public class WhenNonCachingSessionFactory extends AbstractSftpDocumentRepositoryTest {
      @Override
      public SftpDocumentConfigBuilder sftpDocumentConfig(SshServer server) {
         return super.sftpDocumentConfig(server).sessionCaching(false);
      }
   }

   /**
    * Test suite with a relative base path.
    */
   @Nested
   public class WhenRelativeBasePath extends AbstractSftpDocumentRepositoryTest {

      @Override
      public SftpDocumentConfigBuilder sftpDocumentConfig(SshServer server) {
         return super.sftpDocumentConfig(server).basePath(path("basePath"));
      }

      @Nested
      public class DocumentRepositoryUriAdapter {

         @Test
         public void testSupportsDocumentUriWhenNominal() {
            assertThat(documentRepository().supportsUri(documentUri(testSftpUri().resolve(
                  "basePath/path/test.txt")))).isTrue();
         }

         @Test
         public void testSupportsDocumentUriWhenParameters() {
            assertThat(documentRepository().supportsUri(documentUri(testSftpUri().resolve(
                  "basePath/path/test.txt?basePath=basePath")))).isTrue();
            assertThatThrownBy(() -> documentRepository().supportsUri(documentUri(testSftpUri().resolve(
                  "basePath/path/test.txt?basePath=otherPath"))))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessageContaining(
                        "'basePath=otherPath' parameter must start but not be equal to URI 'basePath/path/test.txt' path");
            assertThat(documentRepository().supportsUri(documentUri(testSftpUri().resolve(
                  "basePath/path/test.txt?otherParameter=otherValue")))).isTrue();
         }

         @Test
         public void testSupportsDocumentRepositoryUriWhenAlternativeForms() {
            assertThatThrownBy(() -> documentRepository().supportsUri(documentUri(testSftpUri().resolve(
                  "basePath/path/test.txt#fragment"))))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessageContainingAll("must not have a fragment");
            assertThat(documentRepository().supportsUri(documentUri(testSftpUri().resolve(
                  "basePath/path/test.txt?query")))).isTrue();
            assertThat(documentRepository().supportsUri(documentUri("sftp://"
                                                                    + testSftpUsername()
                                                                    + "@"
                                                                    + testSftpHost()
                                                                    + "/basePath/path/test.txt")))
                  .as("Test service does not use default port")
                  .isFalse();
         }

         @Test
         public void testSupportsRepositoryUriWhenNominal() {
            assertThat(documentRepository().supportsUri(repositoryUri(testSftpUri().resolve("basePath")))).isTrue();
         }

         @Test
         public void testToRepositoryUriWhenNominal() {
            assertThat(documentRepository().toUri().toURI()).isEqualTo(testSftpUri().resolve("basePath"));
         }

         @Test
         public void testToRepositoryUriWhenParameters() {
            assertThat(documentRepository()
                             .toUri(defaultParameters())
                             .toURI()).isEqualTo(testSftpUri().resolve("basePath?allowUnknownHosts=true"));
         }

         @Test
         public void testToRepositoryUriWhenSensitiveParameters() {
            assertThat(documentRepository().toUri(sensitiveParameters()).toURI()).isEqualTo(URI
                                                                                                  .create(
                                                                                                        "sftp://"
                                                                                                        + testSftpUsername()
                                                                                                        + ":"
                                                                                                        + testSftpPassword()
                                                                                                        + "@"
                                                                                                        + testSftpHost()
                                                                                                        + ":"
                                                                                                        + testSftpPort()
                                                                                                        + "/")
                                                                                                  .resolve(
                                                                                                        "basePath?allowUnknownHosts=true&privateKey=LS0tLS1CRUdJTiBPUEVOU1NIIFBSSVZBVEUgS0VZLS0tLS0KYjNCbGJuTnphQzFyWlhrdGRqRUFBQUFBQkc1dmJtVUFBQUFFYm05dVpRQUFBQUFBQUFBQkFBQUJsd0FBQUFkemMyZ3RjbgpOaEFBQUFBd0VBQVFBQUFZRUEwcjJ4Mit6ZlJReVNBdDJjT0ZaZmNUTHRrb0J0dFB1Yy9UdVNrN0ZCTUppQ3BaT3Zudmx2ClJZZ0JlUkhabk95Vjc0Y2c5OXlCT09EOTNZQUN4cnJlV3JqTnlxQWQyTmY0Y0pLN0lUOTRzL29CZ2FVR01OQUlhWGNuWEYKWEdvQ1c4ZXoxYjlCQ2RUaCtnVDdERFNRbFQ3R1FSbDVUNDNYbWFyVXZkOHNBcC8vYmMxVmIzRHB6czh4OXEwOGpNV21rQwp1clZqbUZ1RFdVU25ZNTRHckI2V1drdk5NdlY3aFZNL0U1QVdsZ3NuTHY5Sm9JQjB2eU9KWUZjaDkvakgrckcrc0FkcTFECkNqTjFIN01Jb1ZVcm5pUmNSNWE5WC8xNENqQkVKWU5MTFkxTWdmUE1lTXl3L0ZLK2RBUGFkWUlkdDQ2SkJCVlpLY2QyeGwKR2NxZ2NTM3VqV3RwYzlnb3JRVnplUkRaWitEUGgrZmo2Z1UzbG5PQkRsWFNSMHdUUVZndXRpaUt1RUliKzFnWDl6S2NTVgpkK29NcXNST2FzY1B0Nm9CMXFtT2YzU29zZ0cvTHJFR0lYek9kKy9LZzhWT3V5d2g4RncvOUhvNDE3N0JFcW53QW5USlZXCkIwbjJGclZhWTVhSmpBSGpGQXIyUGVVWDUzT0tDUlRvL0JaV2ZSbFpBQUFGa05pbXh5WFlwc2NsQUFBQUIzTnphQzF5YzIKRUFBQUdCQU5LOXNkdnMzMFVNa2dMZG5EaFdYM0V5N1pLQWJiVDduUDA3a3BPeFFUQ1lncVdUcjU3NWIwV0lBWGtSMlp6cwpsZStISVBmY2dUamcvZDJBQXNhNjNscTR6Y3FnSGRqWCtIQ1N1eUUvZUxQNkFZR2xCakRRQ0dsM0oxeFZ4cUFsdkhzOVcvClFRblU0Zm9FK3d3MGtKVSt4a0VaZVUrTjE1bXExTDNmTEFLZi8yM05WVzl3NmM3UE1mYXRQSXpGcHBBcnExWTVoYmcxbEUKcDJPZUJxd2VsbHBMelRMMWU0VlRQeE9RRnBZTEp5Ny9TYUNBZEw4amlXQlhJZmY0eC9xeHZyQUhhdFF3b3pkUit6Q0tGVgpLNTRrWEVlV3ZWLzllQW93UkNXRFN5Mk5USUh6ekhqTXNQeFN2blFEMm5XQ0hiZU9pUVFWV1NuSGRzWlJuS29IRXQ3bzFyCmFYUFlLSzBGYzNrUTJXZmd6NGZuNCtvRk41WnpnUTVWMGtkTUUwRllMcllvaXJoQ0cvdFlGL2N5bkVsWGZxREtyRVRtckgKRDdlcUFkYXBqbjkwcUxJQnZ5NnhCaUY4em5mdnlvUEZUcnNzSWZCY1AvUjZPTmUrd1JLcDhBSjB5VlZnZEo5aGExV21PVwppWXdCNHhRSzlqM2xGK2R6aWdrVTZQd1dWbjBaV1FBQUFBTUJBQUVBQUFHQUR2U3RhYWt1MHl5b05CSGNJMWRsNEplM3FuCno0ekM1TEh1R2FibWJXOUlidVJxZnJmOEc5OXRaSEhIdDYwUnN0TE5tUEttZjR0N3VuNkk5TUl5Umg0VmhhS25ZZ2lDSVQKbkpHZFBDZlAvaDNRKzQxbG1oTTVkWnFpTFhoYzUwS21WS3l2R093UTdkbHF2QjBVVHR1ZG5Cb2g4WHIrQ21HMTYzU0Y1TgpOa1o2anlWU1dkMU9pb0hEenZqT1lrTW9wZlNqYlRRWjNFL1pIVXUwb2N1UnhXbVVQbDVTYnN4REo3T0gweVRTV3Zlek5jCjIwb2lmdW9idHIyWVNJdmNUVnROeHgzVmNCZzMwRm9LZTdJOVFERFQ0eWRucDg1ZDUrVko0MlEvQlViQy9sUmY5dE5qcWoKZlZFM1ZkNDY5bEduVFgwejkveHhqc0FDL0cyYkE5RDlwYkVNY1lvbHk2WGNmUzJlTUdoUm1HbUxVSU1hZ2N5MGdTNUFCbQphNFUxTDVxVC9BaVd2emliSWxabFJJTVFtbVZ4UHlVQU5LMXMyVGNzeWpBNHcxazJyS0NncEtDMjExTi9sVmx6bkVMcko5CjQwRE92TXpkL213WlBMWm43cHNYbFNZYVpNVjhKbHB4aVFTczEvaTNiUUhlNlZMTjlNREIyempvRnYreU9tZHVnOUFBQUEKd0U4alhTaGhaVTEzdWswZmNUNVhHSXRWWk1mZ2pzZHA2aXhlNUZ4VU1GSW1NU0hlN1lZUGFubGlaV3Erb2xJQVhJUlBrOApvNURiZW9PT1lKczNSWjhBTDkrVUhaQXlpMmphVGJiaG9kNWNsVlEzTWRZUDdDSmxBbHBrckJodWJldW1jYkNHc2Jnd29iClRxK003U29QczdyT2dDNHI1ZXpJSXFoTERZSFFnbjQ0emV5VlllV3UrVTYzWEE0ZWVlbWQwNE9DdnkwMzQwTnYwbm13Y2wKY2wzYU9iUVVyRldwejJuQ0UydHI4RW5FTmpvTWNZL0JPVDZWRzByQWdWd3RZNHFBQUFBTUVBOENla0p0MWdMZ0x3aTlFdwpFdy9TcE5QQlZuRHEyZ0swNnZvMFJYUDFwb2gwYUg1YU9zZXdiSVUya2J6RGpFSEJaRC9BNlJJb1JnbG1OUE5MaXl1TmxBCm1aa01WZW0rSFhHbjRGVTQ3TkxoMGpRZVVCb0xUZ3pHYTRFUGs3QjhBbWhvV0tWNHNML1Q1RGs2T3dPYWpxcTV0S1hWeFMKTEp0dm82T09ZOE9JdDgvOXlMMzJJT0p4TjdxaDhWVGJDSUllVVZXRzZNSjVsM0V5NkJMUU05Y3JPQmF2aXdMOHdPVU5YTgo4bFFseTRNWWFHRDYxaEdKZHl4UUNSN1JneE5IdC9BQUFBd1FEZ3BUeHFhY2R2bGF0aFhiKzkwT3FXSXRGV0VXRUxMOGhOCmRFTGs5dnpoVmJrUzlZL1RnL05TTmVBQWdBZm54Ky9Ed293UFZSTlFLdG5UT1F0M3Jxb2VGZzA3eC9KK3FYNDQ3b0dDZE8Kd244Y2hoTndkRFFEMkNHTTBNUEszTVN5azdqSnlLellBQmloVDFTbitvaCtjME1jajE3VHZuV2wrUTBEbEFpWXJ0c3R6bgpNZWRXaExnMFdzVklMRWNBREt4dWwva1JQL1pyOXBVdGVsb3RXT0VPV1FDSDU4aW1xSW9NTFZQVkMvUXY3RDNLeVRHL3ZLCjRSeFg1RmowSUtOeWNBQUFBVWFIVm5iMEJoYldWMGFIbHpkR1V1Ykc5allXd0JBZ01FQlFZSAotLS0tLUVORCBPUEVOU1NIIFBSSVZBVEUgS0VZLS0tLS0K"));
         }

         @Test
         public void testToDocumentUriWhenNominal() {
            assertThat(documentRepository()
                             .toUri(DocumentPath.of("file.txt"))
                             .toURI()).isEqualTo(testSftpUri().resolve("basePath/file.txt"));
            assertThat(documentRepository().toUri(DocumentPath.of("path/file.txt")).toURI()).isEqualTo(
                  testSftpUri().resolve("basePath/path/file.txt"));
         }

         @Test
         public void testToDocumentUriWhenParameters() {
            assertThat(documentRepository()
                             .toUri(DocumentPath.of("path/file.txt"), defaultParameters())
                             .toURI()).isEqualTo(testSftpUri().resolve(
                  "basePath/path/file.txt?allowUnknownHosts=true&basePath=basePath"));
         }

         @Test
         public void testToDocumentUriWhenSensitiveParameters() {
            assertThat(documentRepository()
                             .toUri(DocumentPath.of("path/file.txt"), sensitiveParameters())
                             .toURI()).isEqualTo(URI
                                                       .create("sftp://"
                                                               + testSftpUsername()
                                                               + ":"
                                                               + testSftpPassword()
                                                               + "@"
                                                               + testSftpHost()
                                                               + ":"
                                                               + testSftpPort()
                                                               + "/")
                                                       .resolve(
                                                             "basePath/path/file.txt?allowUnknownHosts=true&privateKey=LS0tLS1CRUdJTiBPUEVOU1NIIFBSSVZBVEUgS0VZLS0tLS0KYjNCbGJuTnphQzFyWlhrdGRqRUFBQUFBQkc1dmJtVUFBQUFFYm05dVpRQUFBQUFBQUFBQkFBQUJsd0FBQUFkemMyZ3RjbgpOaEFBQUFBd0VBQVFBQUFZRUEwcjJ4Mit6ZlJReVNBdDJjT0ZaZmNUTHRrb0J0dFB1Yy9UdVNrN0ZCTUppQ3BaT3Zudmx2ClJZZ0JlUkhabk95Vjc0Y2c5OXlCT09EOTNZQUN4cnJlV3JqTnlxQWQyTmY0Y0pLN0lUOTRzL29CZ2FVR01OQUlhWGNuWEYKWEdvQ1c4ZXoxYjlCQ2RUaCtnVDdERFNRbFQ3R1FSbDVUNDNYbWFyVXZkOHNBcC8vYmMxVmIzRHB6czh4OXEwOGpNV21rQwp1clZqbUZ1RFdVU25ZNTRHckI2V1drdk5NdlY3aFZNL0U1QVdsZ3NuTHY5Sm9JQjB2eU9KWUZjaDkvakgrckcrc0FkcTFECkNqTjFIN01Jb1ZVcm5pUmNSNWE5WC8xNENqQkVKWU5MTFkxTWdmUE1lTXl3L0ZLK2RBUGFkWUlkdDQ2SkJCVlpLY2QyeGwKR2NxZ2NTM3VqV3RwYzlnb3JRVnplUkRaWitEUGgrZmo2Z1UzbG5PQkRsWFNSMHdUUVZndXRpaUt1RUliKzFnWDl6S2NTVgpkK29NcXNST2FzY1B0Nm9CMXFtT2YzU29zZ0cvTHJFR0lYek9kKy9LZzhWT3V5d2g4RncvOUhvNDE3N0JFcW53QW5USlZXCkIwbjJGclZhWTVhSmpBSGpGQXIyUGVVWDUzT0tDUlRvL0JaV2ZSbFpBQUFGa05pbXh5WFlwc2NsQUFBQUIzTnphQzF5YzIKRUFBQUdCQU5LOXNkdnMzMFVNa2dMZG5EaFdYM0V5N1pLQWJiVDduUDA3a3BPeFFUQ1lncVdUcjU3NWIwV0lBWGtSMlp6cwpsZStISVBmY2dUamcvZDJBQXNhNjNscTR6Y3FnSGRqWCtIQ1N1eUUvZUxQNkFZR2xCakRRQ0dsM0oxeFZ4cUFsdkhzOVcvClFRblU0Zm9FK3d3MGtKVSt4a0VaZVUrTjE1bXExTDNmTEFLZi8yM05WVzl3NmM3UE1mYXRQSXpGcHBBcnExWTVoYmcxbEUKcDJPZUJxd2VsbHBMelRMMWU0VlRQeE9RRnBZTEp5Ny9TYUNBZEw4amlXQlhJZmY0eC9xeHZyQUhhdFF3b3pkUit6Q0tGVgpLNTRrWEVlV3ZWLzllQW93UkNXRFN5Mk5USUh6ekhqTXNQeFN2blFEMm5XQ0hiZU9pUVFWV1NuSGRzWlJuS29IRXQ3bzFyCmFYUFlLSzBGYzNrUTJXZmd6NGZuNCtvRk41WnpnUTVWMGtkTUUwRllMcllvaXJoQ0cvdFlGL2N5bkVsWGZxREtyRVRtckgKRDdlcUFkYXBqbjkwcUxJQnZ5NnhCaUY4em5mdnlvUEZUcnNzSWZCY1AvUjZPTmUrd1JLcDhBSjB5VlZnZEo5aGExV21PVwppWXdCNHhRSzlqM2xGK2R6aWdrVTZQd1dWbjBaV1FBQUFBTUJBQUVBQUFHQUR2U3RhYWt1MHl5b05CSGNJMWRsNEplM3FuCno0ekM1TEh1R2FibWJXOUlidVJxZnJmOEc5OXRaSEhIdDYwUnN0TE5tUEttZjR0N3VuNkk5TUl5Umg0VmhhS25ZZ2lDSVQKbkpHZFBDZlAvaDNRKzQxbG1oTTVkWnFpTFhoYzUwS21WS3l2R093UTdkbHF2QjBVVHR1ZG5Cb2g4WHIrQ21HMTYzU0Y1TgpOa1o2anlWU1dkMU9pb0hEenZqT1lrTW9wZlNqYlRRWjNFL1pIVXUwb2N1UnhXbVVQbDVTYnN4REo3T0gweVRTV3Zlek5jCjIwb2lmdW9idHIyWVNJdmNUVnROeHgzVmNCZzMwRm9LZTdJOVFERFQ0eWRucDg1ZDUrVko0MlEvQlViQy9sUmY5dE5qcWoKZlZFM1ZkNDY5bEduVFgwejkveHhqc0FDL0cyYkE5RDlwYkVNY1lvbHk2WGNmUzJlTUdoUm1HbUxVSU1hZ2N5MGdTNUFCbQphNFUxTDVxVC9BaVd2emliSWxabFJJTVFtbVZ4UHlVQU5LMXMyVGNzeWpBNHcxazJyS0NncEtDMjExTi9sVmx6bkVMcko5CjQwRE92TXpkL213WlBMWm43cHNYbFNZYVpNVjhKbHB4aVFTczEvaTNiUUhlNlZMTjlNREIyempvRnYreU9tZHVnOUFBQUEKd0U4alhTaGhaVTEzdWswZmNUNVhHSXRWWk1mZ2pzZHA2aXhlNUZ4VU1GSW1NU0hlN1lZUGFubGlaV3Erb2xJQVhJUlBrOApvNURiZW9PT1lKczNSWjhBTDkrVUhaQXlpMmphVGJiaG9kNWNsVlEzTWRZUDdDSmxBbHBrckJodWJldW1jYkNHc2Jnd29iClRxK003U29QczdyT2dDNHI1ZXpJSXFoTERZSFFnbjQ0emV5VlllV3UrVTYzWEE0ZWVlbWQwNE9DdnkwMzQwTnYwbm13Y2wKY2wzYU9iUVVyRldwejJuQ0UydHI4RW5FTmpvTWNZL0JPVDZWRzByQWdWd3RZNHFBQUFBTUVBOENla0p0MWdMZ0x3aTlFdwpFdy9TcE5QQlZuRHEyZ0swNnZvMFJYUDFwb2gwYUg1YU9zZXdiSVUya2J6RGpFSEJaRC9BNlJJb1JnbG1OUE5MaXl1TmxBCm1aa01WZW0rSFhHbjRGVTQ3TkxoMGpRZVVCb0xUZ3pHYTRFUGs3QjhBbWhvV0tWNHNML1Q1RGs2T3dPYWpxcTV0S1hWeFMKTEp0dm82T09ZOE9JdDgvOXlMMzJJT0p4TjdxaDhWVGJDSUllVVZXRzZNSjVsM0V5NkJMUU05Y3JPQmF2aXdMOHdPVU5YTgo4bFFseTRNWWFHRDYxaEdKZHl4UUNSN1JneE5IdC9BQUFBd1FEZ3BUeHFhY2R2bGF0aFhiKzkwT3FXSXRGV0VXRUxMOGhOCmRFTGs5dnpoVmJrUzlZL1RnL05TTmVBQWdBZm54Ky9Ed293UFZSTlFLdG5UT1F0M3Jxb2VGZzA3eC9KK3FYNDQ3b0dDZE8Kd244Y2hoTndkRFFEMkNHTTBNUEszTVN5azdqSnlLellBQmloVDFTbitvaCtjME1jajE3VHZuV2wrUTBEbEFpWXJ0c3R6bgpNZWRXaExnMFdzVklMRWNBREt4dWwva1JQL1pyOXBVdGVsb3RXT0VPV1FDSDU4aW1xSW9NTFZQVkMvUXY3RDNLeVRHL3ZLCjRSeFg1RmowSUtOeWNBQUFBVWFIVm5iMEJoYldWMGFIbHpkR1V1Ykc5allXd0JBZ01FQlFZSAotLS0tLUVORCBPUEVOU1NIIFBSSVZBVEUgS0VZLS0tLS0K&basePath=basePath"));
         }

      }

   }

   /**
    * Test suite with an absolute base path.
    */
   @Nested
   public class WhenAbsoluteBasePath extends AbstractSftpDocumentRepositoryTest {
      @Override
      public SftpDocumentConfigBuilder sftpDocumentConfig(SshServer server) {
         return super.sftpDocumentConfig(server).basePath(path("/basePath"));
      }

      @Nested
      public class DocumentRepositoryUriAdapter {

         @Test
         public void testToDocumentUriWhenNominal() {
            assertThat(documentRepository().toUri(DocumentPath.of("file.txt")).toURI()).isEqualTo(uri(
                  testSftpUri() + "/basePath/file.txt"));
            assertThat(documentRepository().toUri(DocumentPath.of("path/file.txt")).toURI()).isEqualTo(uri(
                  testSftpUri() + "/basePath/path/file.txt"));
         }

         @Test
         public void testSupportsDocumentUriWhenNominal() {
            assertThat(documentRepository().supportsUri(documentUri(testSftpUri()
                                                                    + "/basePath/path/test.txt"))).isTrue();
         }

         @Test
         public void testSupportsDocumentUriWhenParameters() {
            assertThat(documentRepository().supportsUri(documentUri(testSftpUri()
                                                                    + "/basePath/path/test.txt?basePath=/basePath"))).isTrue();
            assertThatThrownBy(() -> documentRepository().supportsUri(documentUri(testSftpUri()
                                                                                  + "/basePath/path/test.txt?basePath=/otherPath")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessageContaining(
                        "'basePath=/otherPath' parameter must start but not be equal to URI '/basePath/path/test.txt' path");
            assertThat(documentRepository().supportsUri(documentUri(testSftpUri()
                                                                    + "/basePath/path/test.txt?otherParameter=otherValue"))).isTrue();
         }

         @Test
         public void testSupportsDocumentRepositoryUriWhenAlternativeForms() {
            assertThatThrownBy(() -> documentRepository().supportsUri(documentUri(testSftpUri()
                                                                                  + "/basePath/path/test.txt#fragment")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessageContainingAll("must not have a fragment");
            assertThat(documentRepository().supportsUri(documentUri(testSftpUri()
                                                                    + "/basePath/path/test.txt?query"))).isTrue();
            assertThat(documentRepository().supportsUri(documentUri("sftp://"
                                                                    + testSftpUsername()
                                                                    + "@"
                                                                    + testSftpHost()
                                                                    + "//basePath/path/test.txt")))
                  .as("Test service does not use default SFTP port, so URI with no port specified should not be supported")
                  .isFalse();
         }

         @Test
         public void testToRepositoryUriWhenNominal() {
            assertThat(documentRepository().toUri().toURI()).isEqualTo(testSftpUri().resolve("////basePath"));
         }

         @Test
         public void testSupportsRepositoryUriWhenNominal() {
            assertThat(documentRepository().supportsUri(repositoryUri(testSftpUri().resolve("////basePath")))).isTrue();
         }

      }

   }

   /**
    * Test suite with an empty base path.
    */
   @Nested
   public class WhenEmptyBasePath extends AbstractSftpDocumentRepositoryTest {
      @Override
      public SftpDocumentConfigBuilder sftpDocumentConfig(SshServer server) {
         return super.sftpDocumentConfig(server).basePath(path(""));
      }

      @Nested
      public class DocumentRepositoryUriAdapter {

         @Test
         public void testToDocumentUriWhenNominal() {
            assertThat(documentRepository()
                             .toUri(DocumentPath.of("file.txt"))
                             .toURI()).isEqualTo(testSftpUri().resolve("/file.txt"));
            assertThat(documentRepository().toUri(DocumentPath.of("path/file.txt")).toURI()).isEqualTo(
                  testSftpUri().resolve("/path/file.txt"));
         }

         @Test
         public void testSupportsDocumentUriWhenNominal() {
            assertThat(documentRepository().supportsUri(documentUri(testSftpUri().resolve("/path/test.txt")))).isTrue();
         }

         @Test
         public void testSupportsDocumentUriWhenParameters() {
            assertThat(documentRepository().supportsUri(documentUri(testSftpUri().resolve(
                  "/path/test.txt?basePath=")))).isTrue();
            assertThatThrownBy(() -> documentRepository().supportsUri(documentUri(testSftpUri().resolve(
                  "/path/test.txt?basePath=otherPath"))))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessageContaining(
                        "'basePath=otherPath' parameter must start but not be equal to URI 'path/test.txt' path");
            assertThat(documentRepository().supportsUri(documentUri(testSftpUri().resolve(
                  "/path/test.txt?otherParameter=otherValue")))).isTrue();
         }

         @Test
         public void testSupportsDocumentRepositoryUriWhenAlternativeForms() {
            assertThatThrownBy(() -> documentRepository().supportsUri(documentUri(testSftpUri().resolve(
                  "/path/test.txt#fragment"))))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessageContainingAll("must not have a fragment");
            assertThat(documentRepository().supportsUri(documentUri(testSftpUri().resolve(
                  "/path/test.txt?query")))).isTrue();
            assertThat(documentRepository().supportsUri(documentUri("sftp://"
                                                                    + testSftpUsername()
                                                                    + "@"
                                                                    + testSftpHost()
                                                                    + "/basePath/path/test.txt")))
                  .as("Test service does not use default port")
                  .isFalse();
         }

         @Test
         public void testToRepositoryUriWhenNominal() {
            assertThat(documentRepository().toUri().toURI()).isEqualTo(testSftpUri(false));
         }

         @Test
         public void testSupportsRepositoryUriWhenNominal() {
            assertThat(documentRepository().supportsUri(repositoryUri(testSftpUri()))).isTrue();
         }

      }

   }

   /**
    * Test suite with a relative base path, including a sub path.
    */
   @Nested
   public class WhenServerBasePathAndSubPath extends AbstractSftpDocumentRepositoryTest {
      @Override
      public SftpDocumentConfigBuilder sftpDocumentConfig(SshServer server) {
         return super.sftpDocumentConfig(server).basePath(path("basePath/subPathPath"));
      }
   }

   /**
    * Test suite with a root absolute base path.
    */
   @Nested
   public class WhenRootBasePath extends AbstractSftpDocumentRepositoryTest {
      @Override
      public SftpDocumentConfigBuilder sftpDocumentConfig(SshServer server) {
         return super.sftpDocumentConfig(server).basePath(path("/"));
      }
   }

   /**
    * Test URI matching when port is not specified. No SFTP server is instantiated in this test branch.
    */
   @Nested
   public class WhenDefaultPort {

      @Test
      public void testSupportsDocumentUriWhenAlternativeForms() {
         try (SftpDocumentRepository documentRepository = new SftpDocumentRepository(new SftpDocumentConfigBuilder()
                                                                                           .host("localhost")
                                                                                           .username("user")
                                                                                           .password(
                                                                                                 "password")
                                                                                           .build())) {

            assertThat(documentRepository.supportsUri(documentUri("sftp://user@localhost/path/test.txt"))).isTrue();
         }
      }

      @Test
      public void testSupportsRepositoryUriWhenAlternativeForms() {
         try (SftpDocumentRepository documentRepository = new SftpDocumentRepository(new SftpDocumentConfigBuilder()
                                                                                           .host("localhost")
                                                                                           .username("user")
                                                                                           .password(
                                                                                                 "password")
                                                                                           .build())) {

            assertThat(documentRepository.supportsUri(documentUri("sftp://user@localhost/path"))).isTrue();
         }
      }

   }

   /**
    * Test suite when SFTP session pool size is 1.
    */
   @Nested
   public class WhenSessionPoolSizeOne extends AbstractSftpDocumentRepositoryTest {
      @Override
      public SftpDocumentConfigBuilder sftpDocumentConfig(SshServer server) {
         return super.sftpDocumentConfig(server).sessionPoolSize(1).keepAliveInterval(Duration.ofSeconds(60));
      }
   }

   /**
    * Test suite sing a {@link DefaultChunkedWriteStrategy} write strategy.
    */
   @Nested
   public class WhenDefaultChunkedWriteStrategy extends AbstractSftpDocumentRepositoryTest {
      @Override
      public SftpDocumentConfigBuilder sftpDocumentConfig(SshServer server) {
         return super.sftpDocumentConfig(server).writeStrategy(DefaultChunkedWriteStrategy.class);
      }
   }

   /**
    * Test suite sing a {@link NonTransactionalChunkedServerStrategy} write strategy.
    */
   @Nested
   public class WhenNonTransactionalChunkedWriteStrategy extends AbstractSftpDocumentRepositoryTest {
      @Override
      public SftpDocumentConfigBuilder sftpDocumentConfig(SshServer server) {
         return super.sftpDocumentConfig(server).writeStrategy(NonTransactionalChunkedServerStrategy.class);
      }
   }

   /**
    * Test suite sing a {@link FilesystemWriteStrategy} write strategy.
    */
   @Nested
   public class WhenFilesystemWriteStrategy extends AbstractSftpDocumentRepositoryTest {
      @Override
      public SftpDocumentConfigBuilder sftpDocumentConfig(SshServer server) {
         return super.sftpDocumentConfig(server).writeStrategy(FilesystemWriteStrategy.class);
      }
   }

}