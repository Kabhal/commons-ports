/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.sftp;

import static com.tinubu.commons.lang.util.CollectionUtils.collection;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.ports.document.sftp.SftpDocumentConfig.privateKey;
import static java.nio.file.attribute.PosixFilePermission.OWNER_EXECUTE;
import static java.nio.file.attribute.PosixFilePermission.OWNER_READ;
import static java.nio.file.attribute.PosixFilePermission.OWNER_WRITE;
import static java.util.Collections.singletonList;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.List;
import java.util.function.UnaryOperator;

import org.apache.commons.io.FileUtils;
import org.apache.sshd.common.file.virtualfs.VirtualFileSystemFactory;
import org.apache.sshd.server.SshServer;
import org.apache.sshd.server.keyprovider.SimpleGeneratorHostKeyProvider;
import org.apache.sshd.sftp.server.SftpSubsystemFactory;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentMetadata;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.testsuite.CommonDocumentRepositoryTest;
import com.tinubu.commons.ports.document.sftp.SftpDocumentConfig.SftpDocumentConfigBuilder;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ZipArchiver;

public abstract class AbstractSftpDocumentRepositoryTest extends CommonDocumentRepositoryTest {

   /** SFTP server storage path. */
   protected static Path TEST_STORAGE_PATH;
   protected static final String SSH_SERVER_USERNAME = "user";
   protected static final String SSH_SERVER_PASSWORD = "password";

   private static SshServer server;
   private SftpDocumentRepository documentRepository;

   protected SftpDocumentConfigBuilder sftpDocumentConfig(SshServer server) {
      try {
         return new SftpDocumentConfigBuilder()
               .host(server.getHost())
               .port(server.getPort()).allowUnknownHosts(true)
               .username(SSH_SERVER_USERNAME)
               .password(SSH_SERVER_PASSWORD)
               .privateKey(privateKey(new File(AbstractSftpDocumentRepositoryTest.class
                                                     .getResource("test-key")
                                                     .toURI()).toPath(), true).orElse(null));
      } catch (URISyntaxException e) {
         throw new RuntimeException(e);
      }
   }

   protected String testSftpUsername() {
      return SSH_SERVER_USERNAME;
   }

   protected String testSftpPassword() {
      return SSH_SERVER_PASSWORD;
   }

   protected String testSftpHost() {
      return nullable(server.getHost(), "localhost");
   }

   protected String testSftpPort() {
      return String.valueOf(server.getPort());
   }

   protected URI testSftpUri(boolean trailingSlash) {
      return URI.create("sftp://" + testSftpUsername() + "@" + testSftpHost() + ":" + testSftpPort() + (
            trailingSlash
            ? "/"
            : ""));
   }

   protected URI testSftpUri() {
      return testSftpUri(true);
   }

   @BeforeAll
   public static void startSftpServer() throws IOException {
      try {
         TEST_STORAGE_PATH = Files.createTempDirectory("chassis-document-sftp-test");

         System.out.printf("Creating %s as SFTP storage directory%n", TEST_STORAGE_PATH);

      } catch (IOException e) {
         throw new IllegalStateException(e);
      }

      server = SshServer.setUpDefaultServer();
      server.setPasswordAuthenticator((username, password, session) -> true);
      server.setHost("localhost");
      server.setPort(0);

      Path hostKey = Files.createTempFile("hostkey", ".ser");
      hostKey.toFile().deleteOnExit();

      server.setKeyPairProvider(new SimpleGeneratorHostKeyProvider(hostKey));
      server.setSubsystemFactories(singletonList(new SftpSubsystemFactory()));
      server.setFileSystemFactory(new VirtualFileSystemFactory(TEST_STORAGE_PATH));

      server.start();

      System.out.printf("Started SFTP server on '%s:%s' using '%s:%s' authentication%n",
                        server.getHost(),
                        server.getPort(),
                        SSH_SERVER_USERNAME,
                        SSH_SERVER_PASSWORD);
   }

   @AfterAll
   public static void stopSftpServer() throws IOException {
      server.stop(true);

      try {
         System.out.printf("Deleting test storage path %s%n", TEST_STORAGE_PATH);

         FileUtils.deleteDirectory(TEST_STORAGE_PATH.toFile());
      } catch (IOException e) {
         throw new IllegalStateException(e);
      }
   }

   @BeforeEach
   public void configureDocumentRepository() {
      this.documentRepository = newSftpDocumentRepository(server);
   }

   @AfterEach
   public void closeDocumentRepository() {
      this.documentRepository.close();
   }

   @Override
   protected boolean isSupportingMetadataAttributes() {
      return false;
   }

   /**
    * Test sshd server is not transactional and temporary file simulation is not fully correct, so some
    * transactional tests fail anyway.
    */
   @Override
   public boolean isTransactionalRepository() {
      return false;
   }

   @Override
   protected long chunkSize() {
      return 2 * 1024 * 1024;
   }

   @Override
   protected void instrumentIOException(Path documentPath, boolean enabled) {
      try {
         Path sftpTarget = TEST_STORAGE_PATH.resolve(documentPath);

         if (!sftpTarget.toFile().exists()) {
            sftpTarget = Path.of(sftpTarget + ".writing");
         }

         if (!sftpTarget.toFile().exists()) {
            throw new IllegalStateException(String.format("Can't instrument '%s' SFTP target for '%s'",
                                                          sftpTarget,
                                                          documentPath));
         }

         if (enabled) {
            Files.setPosixFilePermissions(sftpTarget, collection(HashSet::new));
         } else {
            Files.setPosixFilePermissions(sftpTarget,
                                          collection(HashSet::new, OWNER_READ, OWNER_WRITE, OWNER_EXECUTE));
         }
      } catch (IOException e) {
         throw new IllegalStateException(e);
      }
   }

   @Override
   protected DocumentRepository documentRepository() {
      return documentRepository;
   }

   @Override
   protected Document zipTransformer(DocumentPath zipPath, List<Document> documents) {
      return new ZipArchiver(zipPath).compress(documents);
   }

   private SftpDocumentRepository newSftpDocumentRepository(SshServer server) {
      return new SftpDocumentRepository(sftpDocumentConfig(server).build());
   }

   @Override
   protected UnaryOperator<DocumentMetadataBuilder> synchronizeExpectedMetadata(DocumentMetadata actual) {
      return builder -> builder
            .chain(super.synchronizeExpectedMetadata(actual))
            .attributes(actual.attributes())
            .contentType(actual.contentEncoding().orElse(null));
   }

}