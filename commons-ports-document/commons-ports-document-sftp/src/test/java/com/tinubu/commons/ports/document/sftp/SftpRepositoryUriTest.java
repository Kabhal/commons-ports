/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.sftp;

import static com.tinubu.commons.ddd2.uri.Uri.uri;
import static com.tinubu.commons.lang.util.CheckedPredicate.alwaysTrue;
import static com.tinubu.commons.lang.util.PathUtils.emptyPath;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.defaultParameters;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.noParameters;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.sensitiveParameters;
import static com.tinubu.commons.ports.document.sftp.Constants.DEFAULT_ALLOW_UNKNOWN_HOSTS;
import static com.tinubu.commons.ports.document.sftp.Constants.DEFAULT_BASE_PATH;
import static com.tinubu.commons.ports.document.sftp.Constants.DEFAULT_CONNECT_TIMEOUT;
import static com.tinubu.commons.ports.document.sftp.Constants.DEFAULT_HOST;
import static com.tinubu.commons.ports.document.sftp.Constants.DEFAULT_KEEP_ALIVE_INTERVAL;
import static com.tinubu.commons.ports.document.sftp.Constants.DEFAULT_SESSION_CACHING;
import static com.tinubu.commons.ports.document.sftp.Constants.DEFAULT_SESSION_POOL_SIZE;
import static com.tinubu.commons.ports.document.sftp.Constants.DEFAULT_SESSION_POOL_WAIT_TIMEOUT;
import static com.tinubu.commons.ports.document.sftp.Constants.DEFAULT_SOCKET_TIMEOUT;
import static com.tinubu.commons.ports.document.sftp.Constants.DEFAULT_USERNAME;
import static com.tinubu.commons.ports.document.sftp.Constants.DEFAULT_WRITE_STRATEGY;
import static com.tinubu.commons.ports.document.sftp.SftpRepositoryUri.Parameters.ALLOW_UNKNOWN_HOSTS;
import static com.tinubu.commons.ports.document.sftp.SftpRepositoryUri.Parameters.BASE_PATH;
import static com.tinubu.commons.ports.document.sftp.SftpRepositoryUri.Parameters.CONNECT_TIMEOUT;
import static com.tinubu.commons.ports.document.sftp.SftpRepositoryUri.Parameters.KEEP_ALIVE_INTERVAL;
import static com.tinubu.commons.ports.document.sftp.SftpRepositoryUri.Parameters.PRIVATE_KEY;
import static com.tinubu.commons.ports.document.sftp.SftpRepositoryUri.Parameters.PRIVATE_KEY_PASSPHRASE;
import static com.tinubu.commons.ports.document.sftp.SftpRepositoryUri.Parameters.SESSION_CACHING;
import static com.tinubu.commons.ports.document.sftp.SftpRepositoryUri.Parameters.SESSION_POOL_SIZE;
import static com.tinubu.commons.ports.document.sftp.SftpRepositoryUri.Parameters.SESSION_POOL_WAIT_TIMEOUT;
import static com.tinubu.commons.ports.document.sftp.SftpRepositoryUri.Parameters.SOCKET_TIMEOUT;
import static com.tinubu.commons.ports.document.sftp.SftpRepositoryUri.Parameters.WRITE_STRATEGY;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.net.URI;
import java.nio.file.Path;
import java.time.Duration;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.lang.util.types.Base64String;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.RepositoryUri;
import com.tinubu.commons.ports.document.sftp.SftpDocumentConfig.SftpDocumentConfigBuilder;
import com.tinubu.commons.ports.document.sftp.strategy.FilesystemWriteStrategy;

public class SftpRepositoryUriTest {

   static {
      Assertions.setMaxStackTraceElementsDisplayed(50);
   }

   @Nested
   public class OfUriWhenURI {

      @Test
      public void ofUriWhenNominal() {
         assertThat(SftpRepositoryUri.ofRepositoryUri(URI.create("sftp://user@host"))).satisfies(uri -> {
            assertThat(uri.basePath()).isEqualTo(emptyPath());
         });
         assertThat(SftpRepositoryUri.ofRepositoryUri("sftp://user@host")).satisfies(uri -> {
            assertThat(uri.basePath()).isEqualTo(emptyPath());
         });
      }

      @Test
      public void ofUriWhenUserInfos() {
         assertThat(SftpRepositoryUri.ofRepositoryUri(URI.create("sftp://user:password@host"))).satisfies(uri -> {
            assertThat(uri.sftpUri().username()).hasValue("user");
            assertThat(uri.sftpUri().password()).hasValue("password");
         });
         assertThat(SftpRepositoryUri.ofRepositoryUri(URI.create("sftp://user@host"))).satisfies(uri -> {
            assertThat(uri.sftpUri().username()).hasValue("user");
            assertThat(uri.sftpUri().password()).isEmpty();
         });
      }

      @Test
      public void ofUriWhenBadParameters() {
         assertThatThrownBy(() -> SftpRepositoryUri.ofRepositoryUri((URI) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
         assertThatThrownBy(() -> SftpRepositoryUri.ofRepositoryUri((String) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
         assertThatThrownBy(() -> SftpRepositoryUri.ofRepositoryUri((RepositoryUri) null, Path.of("")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
         assertThatThrownBy(() -> SftpRepositoryUri.ofRepositoryUri(SftpRepositoryUri.ofRepositoryUri(
               "sftp://host"), null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultBasePath' must not be null");
      }

      @Test
      public void ofUriWhenIllegalArgument() {
         assertThatExceptionOfType(InvalidUriException.class)
               .isThrownBy(() -> SftpRepositoryUri.ofRepositoryUri("sftp://host?connectTimeout=Invalid"))
               .withMessage(
                     "Invalid '%s' URI: Conversion from 'java.lang.String' to 'java.time.Duration' failed for 'Invalid' value",
                     "sftp://host?connectTimeout=Invalid");
      }

      @Test
      public void ofUriWhenCaseSensitiveParameterKey() {
         assertThat(SftpRepositoryUri.ofRepositoryUri("sftp://host?aLlOwUnKnOwNhOsTs=true")).satisfies(uri -> {
            assertThat(uri.query().parameter(ALLOW_UNKNOWN_HOSTS, alwaysTrue())).hasValue(false);
         });
      }

      @Test
      public void ofUriWhenNormalized() {
         assertThat(SftpRepositoryUri.ofRepositoryUri(URI.create("sftp://host/path/../."))).satisfies(uri -> {
            assertThat(uri.basePath()).isEqualTo(emptyPath());
         });
         assertThat(SftpRepositoryUri.ofRepositoryUri("sftp://host/path/../.")).satisfies(uri -> {
            assertThat(uri.basePath()).isEqualTo(emptyPath());
         });
         assertThat(SftpRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri("sftp://host/path/../."),
                                                      Path.of(""))).satisfies(uri -> {
            assertThat(uri.basePath()).isEqualTo(emptyPath());
         });

         assertThatThrownBy(() -> SftpRepositoryUri.ofRepositoryUri(URI.create("sftp://host/../path")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessageContainingAll("Invalid 'sftp://host/../path' URI", "must not have traversal paths");
      }

      @Test
      public void ofUriWhenInvalidUriSyntax() {
         assertThatThrownBy(() -> SftpRepositoryUri.ofRepositoryUri("git@git.com:user/repo.git"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'git@git.com:user/repo.git' URI: Illegal character in scheme name at index 3: git@git.com:user/repo.git");
         assertThatThrownBy(() -> SftpRepositoryUri.ofRepositoryUri("git@git.com:user/repo.git"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'git@git.com:user/repo.git' URI: Illegal character in scheme name at index 3: git@git.com:user/repo.git");
      }

      @Test
      public void ofUriWhenIncompatibleUri() {
         assertThatThrownBy(() -> SftpRepositoryUri.ofRepositoryUri(URI.create("unknown://host/path")))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessageContainingAll("Incompatible 'unknown://host/path' URI",
                                        "must be absolute, and scheme must be equal to 'sftp'");
      }

      @Nested
      class RelativeBasePath {

         @Test
         public void ofUriWhenBasePath() {
            assertThat(SftpRepositoryUri.ofRepositoryUri(URI.create("sftp://host"))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
            });
            assertThat(SftpRepositoryUri.ofRepositoryUri(URI.create("sftp://host/"))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
            });
            assertThat(SftpRepositoryUri.ofRepositoryUri(URI.create("sftp://host/path"))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of("path"));
            });
            assertThat(SftpRepositoryUri.ofRepositoryUri("sftp://host/path")).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of("path"));
            });
         }

         @Test
         public void ofUriWhenInvalidBasePathParameter() {
            assertThatThrownBy(() -> SftpRepositoryUri.ofRepositoryUri(
                  "sftp://host/path/subpath?basePath=../path"))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage("Invalid '%s' URI: 'basePath=../path' parameter must not contain traversal",
                              "sftp://host/path/subpath?basePath=../path");
            assertThatThrownBy(() -> SftpRepositoryUri.ofRepositoryUri(
                  "sftp://host/path/subpath?basePath=path/subpath/.."))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage("Invalid '%s' URI: 'basePath=path' parameter must match URI 'path/subpath' path",
                              "sftp://host/path/subpath?basePath=path/subpath/..");
         }

         @Test
         public void ofUriWhenExactBasePathParameter() {
            assertThat(SftpRepositoryUri.ofRepositoryUri("sftp://host?basePath=")).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
            });
            assertThat(SftpRepositoryUri.ofRepositoryUri("sftp://host?basePath=path/subpath")).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of("path/subpath"));
            });
            assertThat(SftpRepositoryUri.ofRepositoryUri("sftp://host/?basePath=")).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
            });
            assertThat(SftpRepositoryUri.ofRepositoryUri("sftp://host/?basePath=path/subpath")).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of("path/subpath"));
            });
            assertThat(SftpRepositoryUri.ofRepositoryUri("sftp://host/path/subpath?basePath=path/subpath")).satisfies(
                  uri -> {
                     assertThat(uri.basePath()).isEqualTo(Path.of("path/subpath"));
                  });
            assertThat(SftpRepositoryUri.ofRepositoryUri("sftp://host/path/subpath?basePath=path/./subpath/.")).satisfies(
                  uri -> {
                     assertThat(uri.basePath()).isEqualTo(Path.of("path/subpath"));
                  });
         }

         @Test
         public void ofUriWhenPartialBasePathParameter() {
            assertThatThrownBy(() -> SftpRepositoryUri.ofRepositoryUri("sftp://host/path/subpath?basePath="))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage("Invalid '%s' URI: 'basePath=' parameter must match URI 'path/subpath' path",
                              "sftp://host/path/subpath?basePath=");
            assertThatThrownBy(() -> SftpRepositoryUri.ofRepositoryUri(
                  "sftp://host/path/subpath?basePath=path"))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage("Invalid '%s' URI: 'basePath=path' parameter must match URI 'path/subpath' path",
                              "sftp://host/path/subpath?basePath=path");
            assertThatThrownBy(() -> SftpRepositoryUri.ofRepositoryUri(
                  "sftp://host/path/subpath?basePath=path/."))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage("Invalid '%s' URI: 'basePath=path' parameter must match URI 'path/subpath' path",
                              "sftp://host/path/subpath?basePath=path/.");
         }

         @Test
         public void ofUriWhenMismatchingBasePathParameter() {
            assertThatThrownBy(() -> SftpRepositoryUri.ofRepositoryUri(
                  "sftp://host/path/subpath?basePath=otherpath"))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid '%s' URI: 'basePath=otherpath' parameter must match URI 'path/subpath' path",
                        "sftp://host/path/subpath?basePath=otherpath");
            assertThatThrownBy(() -> SftpRepositoryUri.ofRepositoryUri(
                  "sftp://host/path/subpath?basePath=path/subpath/otherpath"))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid '%s' URI: 'basePath=path/subpath/otherpath' parameter must match URI 'path/subpath' path",
                        "sftp://host/path/subpath?basePath=path/subpath/otherpath");
         }
      }

      @Nested
      class AbsoluteBasePath {

         @Test
         public void ofUriWhenBasePath() {
            assertThat(SftpRepositoryUri.ofRepositoryUri(URI.create("sftp://host//"))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of("/"));
            });
            assertThat(SftpRepositoryUri.ofRepositoryUri(URI.create("sftp://host//path"))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of("/path"));
            });
            assertThat(SftpRepositoryUri.ofRepositoryUri("sftp://host//path")).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of("/path"));
            });
         }

         @Test
         public void ofUriWhenInvalidBasePathParameter() {
            assertThat(SftpRepositoryUri.ofRepositoryUri("sftp://host//path/subpath?basePath=/../path/subpath")).satisfies(
                  uri -> {
                     assertThat(uri.basePath()).isEqualTo(Path.of("/path/subpath"));
                  });
            assertThatThrownBy(() -> SftpRepositoryUri.ofRepositoryUri(
                  "sftp://host//path/subpath?basePath=/path/subpath/.."))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid '%s' URI: 'basePath=/path' parameter must match URI '/path/subpath' path",
                        "sftp://host//path/subpath?basePath=/path/subpath/..");
            assertThatThrownBy(() -> SftpRepositoryUri.ofRepositoryUri("sftp://host//?basePath=/path"))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage("Invalid '%s' URI: 'basePath=/path' parameter must match URI '/' path",
                              "sftp://host//?basePath=/path");
         }

         @Test
         public void ofUriWhenExactBasePathParameter() {
            assertThat(SftpRepositoryUri.ofRepositoryUri("sftp://host?basePath=/path/subpath")).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of("/path/subpath"));
            });
            assertThat(SftpRepositoryUri.ofRepositoryUri("sftp://host//?basePath=/")).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of("/"));
            });
            assertThat(SftpRepositoryUri.ofRepositoryUri("sftp://host//path/subpath?basePath=/path/subpath")).satisfies(
                  uri -> {
                     assertThat(uri.basePath()).isEqualTo(Path.of("/path/subpath"));
                  });
            assertThat(SftpRepositoryUri.ofRepositoryUri(
                  "sftp://host//path/subpath?basePath=/path/./subpath/.")).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of("/path/subpath"));
            });
         }

         @Test
         public void ofUriWhenPartialBasePathParameter() {
            assertThatThrownBy(() -> SftpRepositoryUri.ofRepositoryUri("sftp://host//path/subpath?basePath=/"))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage("Invalid '%s' URI: 'basePath=/' parameter must match URI '/path/subpath' path",
                              "sftp://host//path/subpath?basePath=/");
            assertThatThrownBy(() -> SftpRepositoryUri.ofRepositoryUri(
                  "sftp://host//path/subpath?basePath=/path"))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid '%s' URI: 'basePath=/path' parameter must match URI '/path/subpath' path",
                        "sftp://host//path/subpath?basePath=/path");
            assertThatThrownBy(() -> SftpRepositoryUri.ofRepositoryUri(
                  "sftp://host//path/subpath?basePath=/path/."))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid '%s' URI: 'basePath=/path' parameter must match URI '/path/subpath' path",
                        "sftp://host//path/subpath?basePath=/path/.");
         }

         @Test
         public void ofUriWhenMismatchingBasePathParameter() {
            assertThatThrownBy(() -> SftpRepositoryUri.ofRepositoryUri(
                  "sftp://host//path/subpath?basePath=/otherpath"))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid '%s' URI: 'basePath=/otherpath' parameter must match URI '/path/subpath' path",
                        "sftp://host//path/subpath?basePath=/otherpath");
            assertThatThrownBy(() -> SftpRepositoryUri.ofRepositoryUri(
                  "sftp://host//path/subpath?basePath=/path/subpath/otherpath"))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid '%s' URI: 'basePath=/path/subpath/otherpath' parameter must match URI '/path/subpath' path",
                        "sftp://host//path/subpath?basePath=/path/subpath/otherpath");
         }
      }

      @Nested
      class WrappedUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(SftpRepositoryUri.ofRepositoryUri(URI.create("document:sftp:sftp://host/path"))).satisfies(
                  uri -> {
                     assertThat(uri.basePath()).isEqualTo(Path.of("path"));
                  });
         }

         @Test
         public void ofUriWhenBadRepositoryType() {
            assertThatThrownBy(() -> SftpRepositoryUri.ofRepositoryUri(URI.create(
                  "document:unknown:sftp://host")))
                  .isInstanceOf(IncompatibleUriException.class)
                  .hasMessage(
                        "Incompatible 'document:unknown:sftp://host' URI: Unsupported 'unknown' repository type");
         }

         @Test
         public void ofUriWhenNormalized() {
            assertThat(SftpRepositoryUri.ofRepositoryUri(URI.create("document:sftp:sftp://host/path/../."))).satisfies(
                  uri -> {
                     assertThat(uri.basePath()).isEqualTo(Path.of(""));
                  });
            assertThat(SftpRepositoryUri.ofRepositoryUri("document:sftp:sftp://host/path/../.")).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
            });
            assertThat(SftpRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
                  "document:sftp:sftp://host/path/../."), Path.of(""))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
            });
            assertThatThrownBy(() -> SftpRepositoryUri.ofRepositoryUri(uri("document:sftp:sftp://host/../path")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessageContainingAll("Invalid 'sftp://host/../path' URI",
                                           "must not have traversal paths");
         }

         @Test
         public void ofUriWhenUnsupportedWrappedUri() {
            assertThatThrownBy(() -> SftpRepositoryUri.ofRepositoryUri(uri("document:sftp:unknown://host/path")))
                  .isInstanceOf(IncompatibleUriException.class)
                  .hasMessageContainingAll("Incompatible 'unknown://host/path' URI",
                                           "must be absolute, and scheme must be equal to 'sftp'");
         }

      }
   }

   @Nested
   public class OfUriWhenRepositoryUri {

      @Test
      public void ofUriWhenBadDefaultBasePath() {
         assertThatThrownBy(() -> SftpRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
               "sftp://host/path"), null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultBasePath' must not be null");
         assertThatThrownBy(() -> SftpRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
               "sftp://host/path"), Path.of("../path")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage(
                     "Invariant validation error > 'defaultBasePath=../path' must not have traversal paths");
      }

      @Nested
      public class WhenRepositoryUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(SftpRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri("sftp://host"),
                                                         Path.of("path"))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of("path"));
            });
            assertThat(SftpRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri("sftp://host/path"),
                                                         Path.of("path"))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of("path"));
            });
         }

         @Test
         public void ofUriWhenImplicitDefaultBasePath() {
            assertThat(SftpRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri("sftp://host"))).satisfies(
                  uri -> {
                     assertThat(uri.basePath()).isEqualTo(Path.of(""));
                  });
            assertThat(SftpRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
                  "sftp://host/path/subpath"))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of("path/subpath"));
            });
         }

         @Test
         public void ofUriWhenDefaultBasePath() {
            assertThat(SftpRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri("sftp://host"),
                                                         Path.of("path/subpath"))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of("path/subpath"));
            });
            assertThat(SftpRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
                  "sftp://host/path/subpath"), Path.of("path/subpath"))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of("path/subpath"));
            });
            assertThat(SftpRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri("sftp://host"),
                                                         Path.of(""))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
            });
         }

         @Test
         public void ofUriWhenMismatchingDefaultBasePath() {
            assertThat(SftpRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri("sftp://host/path"),
                                                         Path.of("otherpath")))
                  .as("Default base path is only applied if it starts actual URI")
                  .satisfies(uri -> {
                     assertThat(uri.basePath()).isEqualTo(Path.of("path"));
                  });
            assertThatThrownBy(() -> SftpRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
                  "sftp://host/path/subpath"), Path.of("path")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'sftp://host/path/subpath' URI: 'basePath=path' parameter must match URI 'path/subpath' path");
            assertThatThrownBy(() -> SftpRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
                  "sftp://host/path/subpath"), Path.of("")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'sftp://host/path/subpath' URI: 'basePath=' parameter must match URI 'path/subpath' path");
         }

         @Test
         public void ofUriWhenNoBasePath() {
            assertThat(SftpRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri("sftp://host/"))).satisfies(
                  uri -> {
                     assertThat(uri.basePath()).isEqualTo(Path.of(""));
                  });
            assertThat(SftpRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri("sftp://host"))).satisfies(
                  uri -> {
                     assertThat(uri.basePath()).isEqualTo(Path.of(""));
                  });
            assertThat(SftpRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri("sftp://host"),
                                                         Path.of(""))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
            });
         }

         @Test
         public void ofUriWhenExplicitBasePath() {
            assertThat(SftpRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
                  "sftp://host/path/subpath?basePath=path/subpath"))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of("path/subpath"));
            });
            assertThat(SftpRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
                  "sftp://host?basePath=path/subpath"))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of("path/subpath"));
            });
         }

         @Test
         public void ofUriWhenMismatchingExplicitBasePath() {
            assertThatThrownBy(() -> SftpRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
                  "sftp://host/path/subpath?basePath=otherpath")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'sftp://host/path/subpath?basePath=otherpath' URI: 'basePath=otherpath' parameter must match URI 'path/subpath' path");
            assertThatThrownBy(() -> SftpRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
                  "sftp://host/path/subpath?basePath=path")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'sftp://host/path/subpath?basePath=path' URI: 'basePath=path' parameter must match URI 'path/subpath' path");
            assertThatThrownBy(() -> SftpRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
                  "sftp://host/path/subpath?basePath=")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'sftp://host/path/subpath?basePath=' URI: 'basePath=' parameter must match URI 'path/subpath' path");
            assertThatThrownBy(() -> SftpRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
                  "sftp://host/path/subpath?basePath=subpath")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'sftp://host/path/subpath?basePath=subpath' URI: 'basePath=subpath' parameter must match URI 'path/subpath' path");
         }

      }

      @Nested
      public class WhenDocumentUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(SftpRepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri("sftp://host/document"),
                                                         Path.of(""))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
            });
            assertThat(SftpRepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri("sftp://host/path/document"),
                                                         Path.of("path"))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of("path"));
            });
         }

         @Test
         public void ofUriWhenImplicitDefaultBasePath() {
            assertThat(SftpRepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri("sftp://host/document"))).satisfies(
                  uri -> {
                     assertThat(uri.basePath()).isEqualTo(Path.of(""));
                  });
            assertThat(SftpRepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri(
                  "sftp://host/path/subpath/document"))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
            });
         }

         @Test
         public void ofUriWhenDefaultBasePath() {
            assertThat(SftpRepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri(
                  "sftp://host/path/subpath/document"), Path.of("path/subpath"))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of("path/subpath"));
            });
            assertThat(SftpRepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri(
                  "sftp://host/path/subpath/document"), Path.of("path"))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of("path"));
            });
            assertThat(SftpRepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri(
                  "sftp://host/path/subpath/document"), Path.of(""))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
            });
         }

         @Test
         public void ofUriWhenNoBasePath() {
            assertThat(SftpRepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri("sftp://host/document"))).satisfies(
                  uri -> {
                     assertThat(uri.basePath()).isEqualTo(Path.of(""));
                  });
            assertThat(SftpRepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri("sftp://host/document"),
                                                         Path.of(""))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
            });
         }

         @Test
         public void ofUriWhenExplicitBasePath() {
            assertThat(SftpRepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri(
                  "sftp://host/path/subpath/document?basePath=path/subpath"))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of("path/subpath"));
            });
            assertThat(SftpRepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri(
                  "sftp://host/path/subpath/document?basePath="))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
            });
         }

      }

      @Nested
      public class WhenSftpRepositoryUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(SftpRepositoryUri.ofRepositoryUri(SftpRepositoryUri.ofRepositoryUri("sftp://host"),
                                                         Path.of("path"))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
            });
            assertThat(SftpRepositoryUri.ofRepositoryUri(SftpRepositoryUri.ofRepositoryUri("sftp://host/path"),
                                                         Path.of(""))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of("path"));
            });
         }

         @Test
         public void ofUriWhenImplicitDefaultBasePath() {
            assertThat(SftpRepositoryUri.ofRepositoryUri(SftpRepositoryUri.ofRepositoryUri("sftp://host"))).satisfies(
                  uri -> {
                     assertThat(uri.basePath()).isEqualTo(Path.of(""));
                  });
            assertThat(SftpRepositoryUri.ofRepositoryUri(SftpRepositoryUri.ofRepositoryUri(
                  "sftp://host/path/subpath"))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of("path/subpath"));
            });
         }

         @Test
         public void ofUriWhenDefaultBasePath() {
            assertThat(SftpRepositoryUri.ofRepositoryUri(SftpRepositoryUri.ofRepositoryUri("sftp://host"),
                                                         Path.of("path/subpath"))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
            });
            assertThat(SftpRepositoryUri.ofRepositoryUri(SftpRepositoryUri.ofRepositoryUri(
                  "sftp://host/path/subpath"), Path.of("path/subpath"))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of("path/subpath"));
            });
            assertThat(SftpRepositoryUri.ofRepositoryUri(SftpRepositoryUri.ofRepositoryUri("sftp://host"),
                                                         Path.of(""))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
            });
            assertThat(SftpRepositoryUri.ofRepositoryUri(SftpRepositoryUri.ofRepositoryUri(
                  "sftp://host/path/subpath"), Path.of(""))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of("path/subpath"));
            });
         }

         @Test
         public void ofUriWhenNoBasePath() {
            assertThat(SftpRepositoryUri.ofRepositoryUri(SftpRepositoryUri.ofRepositoryUri("sftp://host/"))).satisfies(
                  uri -> {
                     assertThat(uri.basePath()).isEqualTo(Path.of(""));
                  });
            assertThat(SftpRepositoryUri.ofRepositoryUri(SftpRepositoryUri.ofRepositoryUri("sftp://host"))).satisfies(
                  uri -> {
                     assertThat(uri.basePath()).isEqualTo(Path.of(""));
                  });
         }

         @Test
         public void ofUriWhenExplicitBasePath() {
            assertThat(SftpRepositoryUri.ofRepositoryUri(SftpRepositoryUri.ofRepositoryUri(
                  "sftp://host/path/subpath?basePath=path/subpath"))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of("path/subpath"));
            });
         }

      }

      @Nested
      public class WhenSftpDocumentUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(SftpRepositoryUri.ofRepositoryUri(SftpDocumentUri.ofDocumentUri(
                  "sftp://host/path/document"), Path.of("path"))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
            });
         }

         @Test
         public void ofUriWhenImplicitDefaultBasePath() {
            assertThat(SftpRepositoryUri.ofRepositoryUri(SftpDocumentUri.ofDocumentUri(
                  "sftp://host/path/subpath/document"))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
            });
         }

         @Test
         public void ofUriWhenDefaultBasePath() {
            assertThat(SftpRepositoryUri.ofRepositoryUri(SftpDocumentUri.ofDocumentUri(
                  "sftp://host/path/subpath/document"), Path.of("path/subpath"))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
            });
            assertThat(SftpRepositoryUri.ofRepositoryUri(SftpDocumentUri.ofDocumentUri(
                  "sftp://host/path/subpath/document"), Path.of("path"))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
            });
            assertThat(SftpRepositoryUri.ofRepositoryUri(SftpDocumentUri.ofDocumentUri(
                  "sftp://host/path/subpath/document"), Path.of(""))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
            });
         }

         @Test
         public void ofUriWhenNoBasePath() {
            assertThat(SftpRepositoryUri.ofRepositoryUri(SftpDocumentUri.ofDocumentUri("sftp://host/document"))).satisfies(
                  uri -> {
                     assertThat(uri.basePath()).isEqualTo(Path.of(""));
                  });
            assertThat(SftpRepositoryUri.ofRepositoryUri(SftpDocumentUri.ofDocumentUri("sftp://host/document"),
                                                         Path.of(""))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
            });
         }

         @Test
         public void ofUriWhenExplicitBasePath() {
            assertThat(SftpRepositoryUri.ofRepositoryUri(SftpDocumentUri.ofDocumentUri(
                  "sftp://host/path/subpath/document?basePath=path/subpath"))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of("path/subpath"));
            });
            assertThat(SftpRepositoryUri.ofRepositoryUri(SftpDocumentUri.ofDocumentUri(
                  "sftp://host/path/subpath/document?basePath="))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
            });
         }

      }

   }

   @Nested
   public class OfConfig {

      @Test
      public void ofConfigWhenNominal() {
         assertThat(SftpRepositoryUri.ofConfig(new SftpDocumentConfigBuilder()
                                                     .privateKey("key".getBytes(UTF_8))
                                                     .build())).satisfies(uri -> {
            assertThat(uri.basePath()).isEqualTo(DEFAULT_BASE_PATH);
            assertThat(uri.sftpUri().username()).hasValue(DEFAULT_USERNAME);
            assertThat(uri.sftpUri().password()).isEmpty();
            assertThat(uri.sftpUri().host()).hasValue(DEFAULT_HOST);
            assertThat(uri.sftpUri().port()).isEmpty();
            assertThat(uri.query().parameter(BASE_PATH, alwaysTrue())).hasValue(DEFAULT_BASE_PATH);
            assertThat(uri.query().parameter(ALLOW_UNKNOWN_HOSTS, alwaysTrue())).hasValue(
                  DEFAULT_ALLOW_UNKNOWN_HOSTS);
            assertThat(uri.query().parameter(SOCKET_TIMEOUT, alwaysTrue())).hasValue(
                  DEFAULT_SOCKET_TIMEOUT);
            assertThat(uri.query().parameter(CONNECT_TIMEOUT, alwaysTrue())).hasValue(
                  DEFAULT_CONNECT_TIMEOUT);
            assertThat(uri.query().parameter(SESSION_CACHING, alwaysTrue())).hasValue(
                  DEFAULT_SESSION_CACHING);
            assertThat(uri.query().parameter(SESSION_POOL_SIZE, alwaysTrue())).hasValue(
                  DEFAULT_SESSION_POOL_SIZE);
            assertThat(uri.query().parameter(SESSION_POOL_WAIT_TIMEOUT, alwaysTrue())).hasValue(
                  DEFAULT_SESSION_POOL_WAIT_TIMEOUT);
            assertThat(uri.query().parameter(PRIVATE_KEY,
                                                      alwaysTrue())).hasValue(Base64String.encode("key".getBytes(
                  UTF_8)));
            assertThat(uri.query().parameter(PRIVATE_KEY_PASSPHRASE, alwaysTrue())).isEmpty();
            assertThat(uri.query().parameter(KEEP_ALIVE_INTERVAL, alwaysTrue())).hasValue(
                  DEFAULT_KEEP_ALIVE_INTERVAL);
            assertThat(uri.query().parameter(WRITE_STRATEGY, alwaysTrue())).hasValue(
                  DEFAULT_WRITE_STRATEGY);
         });
      }

      @Test
      public void ofConfigWhenAllParameters() {
         assertThat(SftpRepositoryUri.ofConfig(new SftpDocumentConfigBuilder()
                                                     .username("user")
                                                     .password("password")
                                                     .host("host")
                                                     .port(23)
                                                     .basePath(Path.of("path"))
                                                     .allowUnknownHosts(true)
                                                     .socketTimeout(Duration.ofSeconds(10))
                                                     .connectTimeout(Duration.ofSeconds(11))
                                                     .sessionCaching(false)
                                                     .sessionPoolSize(100)
                                                     .sessionPoolWaitTimeout(Duration.ofSeconds(12))
                                                     .privateKey("key".getBytes(UTF_8))
                                                     .privateKeyPassphrase("passphrase")
                                                     .keepAliveInterval(Duration.ofMinutes(5))
                                                     .writeStrategy(FilesystemWriteStrategy.class)
                                                     .build())).satisfies(uri -> {
            assertThat(uri.basePath()).isEqualTo(Path.of("path"));
            assertThat(uri.sftpUri().username()).hasValue("user");
            assertThat(uri.sftpUri().password()).hasValue("password");
            assertThat(uri.sftpUri().host()).hasValue("host");
            assertThat(uri.sftpUri().port()).hasValue(23);
            assertThat(uri.query().parameter(BASE_PATH, alwaysTrue())).hasValue(Path.of("path"));
            assertThat(uri.query().parameter(ALLOW_UNKNOWN_HOSTS, alwaysTrue())).hasValue(true);
            assertThat(uri.query().parameter(SOCKET_TIMEOUT,
                                                      alwaysTrue())).hasValue(Duration.ofSeconds(10));
            assertThat(uri.query().parameter(CONNECT_TIMEOUT,
                                                      alwaysTrue())).hasValue(Duration.ofSeconds(11));
            assertThat(uri.query().parameter(SESSION_CACHING, alwaysTrue())).hasValue(false);
            assertThat(uri.query().parameter(SESSION_POOL_SIZE, alwaysTrue())).hasValue(100);
            assertThat(uri.query().parameter(SESSION_POOL_WAIT_TIMEOUT, alwaysTrue())).hasValue(
                  Duration.ofSeconds(12));
            assertThat(uri.query().parameter(PRIVATE_KEY,
                                                      alwaysTrue())).hasValue(Base64String.encode("key".getBytes(
                  UTF_8)));
            assertThat(uri.query().parameter(PRIVATE_KEY_PASSPHRASE, alwaysTrue())).hasValue(
                  "passphrase");
            assertThat(uri.query().parameter(KEEP_ALIVE_INTERVAL,
                                                      alwaysTrue())).hasValue(Duration.ofMinutes(5));
            assertThat(uri.query().parameter(WRITE_STRATEGY, alwaysTrue())).hasValue(
                  FilesystemWriteStrategy.class);
         });
      }

      @Test
      public void ofConfigWhenRelativeBasePath() {
         assertThat(SftpRepositoryUri.ofConfig(new SftpDocumentConfigBuilder()
                                                     .basePath(Path.of("path"))
                                                     .privateKey("key".getBytes(UTF_8))
                                                     .build())).satisfies(uri -> {
            assertThat(uri.basePath()).isEqualTo(Path.of("path"));
         });
      }

      @Test
      public void ofConfigWhenAbsoluteBasePath() {
         assertThat(SftpRepositoryUri.ofConfig(new SftpDocumentConfigBuilder()
                                                     .basePath(Path.of("/path"))
                                                     .privateKey("key".getBytes(UTF_8))
                                                     .build())).satisfies(uri -> {
            assertThat(uri.basePath()).isEqualTo(Path.of("/path"));
         });
      }

   }

   @Nested
   public class ToConfig {

      @Test
      public void toConfigWhenNominal() {
         assertThat(SftpRepositoryUri.ofRepositoryUri("sftp://user@host").toConfig()).satisfies(config -> {
            assertThat(config.username()).isEqualTo("user");
            assertThat(config.password()).isNull();
            assertThat(config.host()).isEqualTo("host");
            assertThat(config.port()).isNull();
            assertThat(config.basePath()).isEqualTo(DEFAULT_BASE_PATH);
            assertThat(config.allowUnknownHosts()).isEqualTo(DEFAULT_ALLOW_UNKNOWN_HOSTS);
            assertThat(config.socketTimeout()).isEqualTo(DEFAULT_SOCKET_TIMEOUT);
            assertThat(config.connectTimeout()).isEqualTo(DEFAULT_CONNECT_TIMEOUT);
            assertThat(config.sessionCaching()).isEqualTo(DEFAULT_SESSION_CACHING);
            assertThat(config.sessionPoolSize()).isEqualTo(DEFAULT_SESSION_POOL_SIZE);
            assertThat(config.sessionPoolWaitTimeout()).isEqualTo(DEFAULT_SESSION_POOL_WAIT_TIMEOUT);
            assertThat(config.privateKey()).isNull();
            assertThat(config.privateKeyPassphrase()).isNull();
            assertThat(config.keepAliveInterval()).isEqualTo(DEFAULT_KEEP_ALIVE_INTERVAL);
            assertThat(config.writeStrategy()).isEqualTo(DEFAULT_WRITE_STRATEGY);
         });
      }

      @Test
      public void toConfigWhenAllParameters() {
         assertThat(SftpRepositoryUri
                          .ofRepositoryUri(
                                "sftp://user:password@host:23/path?allowUnknownHosts=true&socketTimeout=PT10S&connectTimeout=PT11S&sessionCaching=false&sessionPoolSize=100&sessionPoolWaitTimeout=PT12S&privateKey=a2V5&privateKeyPassphrase=passphrase&keepAliveInterval=PT5M&writeStrategy=com.tinubu.commons.ports.document.sftp.strategy.FilesystemWriteStrategy")
                          .toConfig()).satisfies(config -> {
            assertThat(config.username()).isEqualTo("user");
            assertThat(config.password()).isEqualTo("password");
            assertThat(config.host()).isEqualTo("host");
            assertThat(config.port()).isEqualTo(23);
            assertThat(config.basePath()).isEqualTo(Path.of("path"));
            assertThat(config.allowUnknownHosts()).isEqualTo(true);
            assertThat(config.socketTimeout()).isEqualTo(Duration.ofSeconds(10));
            assertThat(config.connectTimeout()).isEqualTo(Duration.ofSeconds(11));
            assertThat(config.sessionCaching()).isEqualTo(false);
            assertThat(config.sessionPoolSize()).isEqualTo(100);
            assertThat(config.sessionPoolWaitTimeout()).isEqualTo(Duration.ofSeconds(12));
            assertThat(config.privateKey()).isEqualTo("key".getBytes(UTF_8));
            assertThat(config.privateKeyPassphrase()).isEqualTo("passphrase");
            assertThat(config.keepAliveInterval()).isEqualTo(Duration.ofMinutes(5));
            assertThat(config.writeStrategy()).isEqualTo(FilesystemWriteStrategy.class);
         });
      }

      @Test
      public void toConfigWhenRelativeBasePath() {
         assertThat(SftpRepositoryUri
                          .ofRepositoryUri("sftp://user:password@host:23/path")
                          .toConfig()).satisfies(config -> {
            assertThat(config.basePath()).isEqualTo(Path.of("path"));
         });
      }

      @Test
      public void toConfigWhenAbsoluteBasePath() {
         assertThat(SftpRepositoryUri
                          .ofRepositoryUri("sftp://user:password@host:23//path")
                          .toConfig()).satisfies(config -> {
            assertThat(config.basePath()).isEqualTo(Path.of("/path"));
         });
      }

   }

   @Nested
   public class ToUri {

      @Test
      public void toUriWhenNominal() {
         assertThat(SftpRepositoryUri
                          .ofRepositoryUri("sftp://host")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("sftp://host");
      }

      @Test
      public void toUriWhenRelativeBasePath() {
         assertThat(SftpRepositoryUri
                          .ofRepositoryUri("sftp://host")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("sftp://host");
         assertThat(SftpRepositoryUri
                          .ofRepositoryUri("sftp://host/")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("sftp://host/");
         assertThat(SftpRepositoryUri
                          .ofRepositoryUri("sftp://host/path")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("sftp://host/path");
      }

      @Test
      public void toUriWhenAbsoluteBasePath() {
         assertThat(SftpRepositoryUri
                          .ofRepositoryUri("sftp://host//")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("sftp://host//");
         assertThat(SftpRepositoryUri.ofRepositoryUri("sftp://host//path").exportUri(noParameters())
                          .stringValue()).isEqualTo("sftp://host//path");
      }

      @Test
      public void toUriWhenDefaultParameters() {
         assertThat(SftpRepositoryUri.ofRepositoryUri("sftp://host/path")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("sftp://host/path");
         assertThat(SftpRepositoryUri
                          .ofRepositoryUri("sftp://host/path?basePath=path")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("sftp://host/path");
         assertThat(SftpRepositoryUri
                          .ofRepositoryUri("sftp://host/path?allowUnknownHosts=false")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("sftp://host/path");
         assertThat(SftpRepositoryUri
                          .ofRepositoryUri("sftp://host/path?allowUnknownHosts=true")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("sftp://host/path?allowUnknownHosts=true");
         assertThat(SftpRepositoryUri
                          .ofRepositoryUri("sftp://user:password@host/path")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("sftp://user@host/path");
      }

      @Test
      public void toUriWhenDefaultParametersWithForceDefaultValues() {
         assertThat(SftpRepositoryUri.ofRepositoryUri("sftp://host/path").exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo(
               "sftp://host/path?allowUnknownHosts=false&basePath=path&connectTimeout=PT0S&keepAliveInterval=PT1M&sessionCaching=true&sessionPoolSize=10&sessionPoolWaitTimeout=PT0S&socketTimeout=PT0S&writeStrategy=com.tinubu.commons.ports.document.sftp.strategy.DefaultChunkedWriteStrategy");
         assertThat(SftpRepositoryUri
                          .ofRepositoryUri("sftp://host/path?basePath=path")
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo(
               "sftp://host/path?basePath=path&allowUnknownHosts=false&connectTimeout=PT0S&keepAliveInterval=PT1M&sessionCaching=true&sessionPoolSize=10&sessionPoolWaitTimeout=PT0S&socketTimeout=PT0S&writeStrategy=com.tinubu.commons.ports.document.sftp.strategy.DefaultChunkedWriteStrategy");
         assertThat(SftpRepositoryUri
                          .ofRepositoryUri("sftp://host/path?allowUnknownHosts=false")
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo(
               "sftp://host/path?allowUnknownHosts=false&basePath=path&connectTimeout=PT0S&keepAliveInterval=PT1M&sessionCaching=true&sessionPoolSize=10&sessionPoolWaitTimeout=PT0S&socketTimeout=PT0S&writeStrategy=com.tinubu.commons.ports.document.sftp.strategy.DefaultChunkedWriteStrategy");
         assertThat(SftpRepositoryUri
                          .ofRepositoryUri("sftp://host/path?allowUnknownHosts=true")
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo(
               "sftp://host/path?allowUnknownHosts=true&basePath=path&connectTimeout=PT0S&keepAliveInterval=PT1M&sessionCaching=true&sessionPoolSize=10&sessionPoolWaitTimeout=PT0S&socketTimeout=PT0S&writeStrategy=com.tinubu.commons.ports.document.sftp.strategy.DefaultChunkedWriteStrategy");

         assertThat(SftpRepositoryUri
                          .ofConfig(new SftpDocumentConfigBuilder()
                                          .username("user")
                                          .basePath(Path.of("path"))
                                          .privateKey("key".getBytes(UTF_8))
                                          .build())
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo(
               "sftp://user@localhost/path?allowUnknownHosts=false&basePath=path&connectTimeout=PT0S&keepAliveInterval=PT1M&sessionCaching=true&sessionPoolSize=10&sessionPoolWaitTimeout=PT0S&socketTimeout=PT0S&writeStrategy=com.tinubu.commons.ports.document.sftp.strategy.DefaultChunkedWriteStrategy");

         assertThat(SftpRepositoryUri
                          .ofRepositoryUri("sftp://user:password@host/path")
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo(
               "sftp://user@host/path?allowUnknownHosts=false&basePath=path&connectTimeout=PT0S&keepAliveInterval=PT1M&sessionCaching=true&sessionPoolSize=10&sessionPoolWaitTimeout=PT0S&socketTimeout=PT0S&writeStrategy=com.tinubu.commons.ports.document.sftp.strategy.DefaultChunkedWriteStrategy");

      }

      @Test
      public void toUriWhenSensitiveParameters() {
         assertThat(SftpRepositoryUri.ofRepositoryUri("sftp://host/path").exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("sftp://host/path");
         assertThat(SftpRepositoryUri
                          .ofRepositoryUri("sftp://host/path?basePath=path")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("sftp://host/path");
         assertThat(SftpRepositoryUri
                          .ofRepositoryUri(
                                "sftp://user:password@host/path?privateKey=a2V5&privateKeyPassphrase=passphrase")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo(
               "sftp://user:password@host/path?privateKey=a2V5&privateKeyPassphrase=passphrase");
         assertThat(SftpRepositoryUri
                          .ofConfig(new SftpDocumentConfigBuilder()
                                          .username("user")
                                          .password("password")
                                          .privateKey("key".getBytes(UTF_8))
                                          .privateKeyPassphrase("passphrase")
                                          .build())
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo(
               "sftp://user:password@localhost?privateKey=a2V5&privateKeyPassphrase=passphrase");
      }

      @Test
      public void toUriWhenOriginalUriHasTerminalSlash() {
         assertThat(SftpRepositoryUri
                          .ofRepositoryUri("sftp://host")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("sftp://host");
         assertThat(SftpRepositoryUri
                          .ofRepositoryUri("sftp://host/")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("sftp://host/");
         assertThat(SftpRepositoryUri.ofRepositoryUri("sftp://host/path/").exportUri(noParameters())
                          .stringValue()).isEqualTo("sftp://host/path/");
         assertThat(SftpRepositoryUri
                          .ofRepositoryUri("sftp://host//")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("sftp://host//");
         assertThat(SftpRepositoryUri.ofRepositoryUri("sftp://host//path/").exportUri(noParameters())
                          .stringValue()).isEqualTo("sftp://host//path/");
      }

      @Test
      public void toUriWhenOriginalUriHasTerminalSlashAndBasePathParameter() {
         assertThat(SftpRepositoryUri
                          .ofRepositoryUri("sftp://host/path/?basePath=path/")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("sftp://host/path/");
         assertThat(SftpRepositoryUri
                          .ofRepositoryUri("sftp://host/path/?basePath=path")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("sftp://host/path/");
         assertThat(SftpRepositoryUri.ofRepositoryUri("sftp://host?basePath=path/").exportUri(noParameters())
                          .stringValue())
               .as("base path parameter is always normalized")
               .isEqualTo("sftp://host/path");
         assertThat(SftpRepositoryUri.ofRepositoryUri("sftp://host/?basePath=path/").exportUri(noParameters())
                          .stringValue())
               .as("trailing slash is added only if URI path match base path")
               .isEqualTo("sftp://host/path");
         assertThat(SftpRepositoryUri.ofRepositoryUri("sftp://host?basePath=").exportUri(noParameters())
                          .stringValue()).isEqualTo("sftp://host");
         assertThat(SftpRepositoryUri.ofRepositoryUri("sftp://host/?basePath=").exportUri(noParameters())
                          .stringValue()).isEqualTo("sftp://host/");

         assertThat(SftpRepositoryUri
                          .ofRepositoryUri("sftp://host//path/?basePath=/path/")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("sftp://host//path/");
         assertThat(SftpRepositoryUri
                          .ofRepositoryUri("sftp://host//path/?basePath=/path")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("sftp://host//path/");
         assertThat(SftpRepositoryUri.ofRepositoryUri("sftp://host?basePath=/path/").exportUri(noParameters())
                          .stringValue())
               .as("base path parameter is always normalized")
               .isEqualTo("sftp://host//path");
         assertThat(SftpRepositoryUri.ofRepositoryUri("sftp://host?basePath=/").exportUri(noParameters())
                          .stringValue()).isEqualTo("sftp://host//");
         assertThat(SftpRepositoryUri.ofRepositoryUri("sftp://host//?basePath=/").exportUri(noParameters())
                          .stringValue()).isEqualTo("sftp://host//");

      }

   }

   @Nested
   public class NormalizeResolveRelativize {

      @Test
      public void normalizeWhenNominal() {
         assertThat(SftpRepositoryUri.ofRepositoryUri("sftp://host/path/../.").normalize()).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo("sftp://host/");
         });
      }

      @Test
      public void resolveWhenNominal() {
         assertThat(SftpRepositoryUri
                          .ofRepositoryUri("sftp://host/path")
                          .resolve(uri("otherpath"))).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo("sftp://host/otherpath");
         });
         assertThat(SftpRepositoryUri
                          .ofRepositoryUri("sftp://host/path/")
                          .resolve(uri("otherpath"))).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo("sftp://host/path/otherpath");
         });
      }

      @Test
      public void relativizeWhenNominal() {
         assertThatThrownBy(() -> SftpRepositoryUri
               .ofRepositoryUri("sftp://host/path")
               .relativize(uri("sftp://host/path/otherpath"))).isInstanceOf(UnsupportedOperationException.class);
      }

   }

}