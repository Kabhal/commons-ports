/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.sftp;

import static com.tinubu.commons.ddd2.uri.Uri.uri;
import static com.tinubu.commons.lang.util.CheckedPredicate.alwaysTrue;
import static com.tinubu.commons.lang.util.PathUtils.emptyPath;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.defaultParameters;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.noParameters;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.sensitiveParameters;
import static com.tinubu.commons.ports.document.sftp.SftpRepositoryUri.Parameters.ALLOW_UNKNOWN_HOSTS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.net.URI;
import java.nio.file.Path;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;

public class SftpDocumentUriTest {

   static {
      Assertions.setMaxStackTraceElementsDisplayed(50);
   }

   @Nested
   public class OfUriWhenURI {

      @Test
      public void ofUriWhenNominal() {
         assertThat(SftpDocumentUri.ofDocumentUri(uri("sftp://user@host/document"))).satisfies(uri -> {
            assertThat(uri.basePath()).isEqualTo(emptyPath());
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
         assertThat(SftpDocumentUri.ofDocumentUri("sftp://user@host/document")).satisfies(uri -> {
            assertThat(uri.basePath()).isEqualTo(emptyPath());
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
      }

      @Test
      public void ofUriWhenUserInfos() {
         assertThat(SftpDocumentUri.ofDocumentUri(uri("sftp://user:password@host/document"))).satisfies(uri -> {
            assertThat(uri.sftpUri().username()).hasValue("user");
            assertThat(uri.sftpUri().password()).hasValue("password");
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
         assertThat(SftpDocumentUri.ofDocumentUri(uri("sftp://user@host/document"))).satisfies(uri -> {
            assertThat(uri.sftpUri().username()).hasValue("user");
            assertThat(uri.sftpUri().password()).isEmpty();
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
      }

      @Test
      public void ofUriWhenBadParameters() {
         assertThatThrownBy(() -> SftpDocumentUri.ofDocumentUri((URI) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
         assertThatThrownBy(() -> SftpDocumentUri.ofDocumentUri(uri("sftp://host/document"), null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultBasePath' must not be null");
         assertThatThrownBy(() -> SftpDocumentUri.ofDocumentUri((String) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
         assertThatThrownBy(() -> SftpDocumentUri.ofDocumentUri("sftp://host/document", null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultBasePath' must not be null");
         assertThatThrownBy(() -> SftpDocumentUri.ofDocumentUri((DocumentUri) null, Path.of("")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
         assertThatThrownBy(() -> SftpDocumentUri.ofDocumentUri(SftpDocumentUri.ofDocumentUri(
               "sftp://host/document"), null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultBasePath' must not be null");
      }

      @Test
      public void ofUriWhenIllegalArgument() {
         assertThatExceptionOfType(InvalidUriException.class)
               .isThrownBy(() -> SftpDocumentUri.ofDocumentUri("sftp://host/document?connectTimeout=Invalid"))
               .withMessage(
                     "Invalid '%s' URI: Conversion from 'java.lang.String' to 'java.time.Duration' failed for 'Invalid' value",
                     "sftp://host/document?connectTimeout=Invalid");
      }

      @Test
      public void ofUriWhenCaseSensitiveParameterKey() {
         assertThat(SftpDocumentUri.ofDocumentUri("sftp://host/document?aLlOwUnKnOwNhOsTs=true")).satisfies(
               uri -> {
                  assertThat(uri.query().parameter(ALLOW_UNKNOWN_HOSTS,
                                                            alwaysTrue())).hasValue(false);
               });
      }

      @Test
      public void ofUriWhenNormalized() {
         assertThat(SftpDocumentUri.ofDocumentUri(uri("sftp://host/path/.././document"))).satisfies(uri -> {
            assertThat(uri.basePath()).isEqualTo(emptyPath());
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
         assertThat(SftpDocumentUri.ofDocumentUri("sftp://host/path/.././document")).satisfies(uri -> {
            assertThat(uri.basePath()).isEqualTo(emptyPath());
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
         assertThat(SftpDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri("sftp://host/path/.././document"),
                                                  Path.of(""))).satisfies(uri -> {
            assertThat(uri.basePath()).isEqualTo(emptyPath());
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });

         assertThatThrownBy(() -> SftpDocumentUri.ofDocumentUri(uri("sftp://host/../path/document")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessageContainingAll("Invalid 'sftp://host/../path/document' URI",
                                        "must not have traversal paths");
      }

      @Test
      public void ofUriWhenInvalidUri() {
         assertThatThrownBy(() -> SftpDocumentUri.ofDocumentUri("sftp://user@host"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage("Invalid 'sftp://user@host' URI: 'path' must not be empty");
         assertThatThrownBy(() -> SftpDocumentUri.ofDocumentUri("git@git.com:user/repo.git"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'git@git.com:user/repo.git' URI: Illegal character in scheme name at index 3: git@git.com:user/repo.git");
         assertThatThrownBy(() -> SftpDocumentUri.ofDocumentUri("git@git.com:user/repo.git"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'git@git.com:user/repo.git' URI: Illegal character in scheme name at index 3: git@git.com:user/repo.git");
      }

      @Test
      public void ofUriWhenIncompatibleUri() {
         assertThatThrownBy(() -> SftpDocumentUri.ofDocumentUri(uri("unknown://host/path")))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessageContainingAll("Incompatible 'unknown://host/path' URI",
                                        "must be absolute, and scheme must be equal to 'sftp'");
      }

      @Nested
      class RelativeBasePath {

         @Test
         public void ofUriWhenBasePath() {
            assertThat(SftpDocumentUri.ofDocumentUri(uri("sftp://host/document"))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
            assertThat(SftpDocumentUri.ofDocumentUri(uri("sftp://host/document/"))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
            assertThat(SftpDocumentUri.ofDocumentUri(uri("sftp://host/path/document"))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/document"));
            });
            assertThat(SftpDocumentUri.ofDocumentUri("sftp://host/path/document")).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/document"));
            });
         }

         @Test
         public void ofUriWhenInvalidBasePathParameter() {
            assertThatThrownBy(() -> SftpDocumentUri.ofDocumentUri(
                  "sftp://host/path/subpath/document?basePath=../path"))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage("Invalid '%s' URI: 'basePath=../path' parameter must not contain traversal",
                              "sftp://host/path/subpath/document?basePath=../path");
            assertThat(SftpDocumentUri.ofDocumentUri(
                  "sftp://host/path/subpath/document?basePath=path/subpath/..")).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of("path"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("subpath/document"));
            });
         }

         @Test
         public void ofUriWhenExactBasePathParameter() {
            assertThat(SftpDocumentUri.ofDocumentUri("sftp://host/document?basePath=")).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
            assertThat(SftpDocumentUri.ofDocumentUri("sftp://host/document/?basePath=")).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
            assertThat(SftpDocumentUri.ofDocumentUri("sftp://host/path/subpath/document?basePath=path/subpath")).satisfies(
                  uri -> {
                     assertThat(uri.basePath()).isEqualTo(Path.of("path/subpath"));
                     assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
                  });
            assertThat(SftpDocumentUri.ofDocumentUri(
                  "sftp://host/path/subpath/document?basePath=path/./subpath/.")).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of("path/subpath"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
         }

         @Test
         public void ofUriWhenPartialBasePathParameter() {
            assertThat(SftpDocumentUri.ofDocumentUri("sftp://host/path/subpath/document?basePath=")).satisfies(
                  uri -> {
                     assertThat(uri.basePath()).isEqualTo(emptyPath());
                     assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/subpath/document"));
                  });
            assertThat(SftpDocumentUri.ofDocumentUri("sftp://host/path/subpath/document?basePath=path")).satisfies(
                  uri -> {
                     assertThat(uri.basePath()).isEqualTo(Path.of("path"));
                     assertThat(uri.documentId()).isEqualTo(DocumentPath.of("subpath/document"));
                  });
            assertThat(SftpDocumentUri.ofDocumentUri("sftp://host/path/subpath/document?basePath=path/.")).satisfies(
                  uri -> {
                     assertThat(uri.basePath()).isEqualTo(Path.of("path"));
                     assertThat(uri.documentId()).isEqualTo(DocumentPath.of("subpath/document"));
                  });
         }

         @Test
         public void ofUriWhenMismatchingBasePathParameter() {
            assertThatThrownBy(() -> SftpDocumentUri.ofDocumentUri(
                  "sftp://host/path/subpath/document?basePath=otherpath"))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid '%s' URI: 'basePath=otherpath' parameter must start but not be equal to URI 'path/subpath/document' path",
                        "sftp://host/path/subpath/document?basePath=otherpath");
            assertThatThrownBy(() -> SftpDocumentUri.ofDocumentUri(
                  "sftp://host/path/subpath/document?basePath=path/subpath/otherpath"))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid '%s' URI: 'basePath=path/subpath/otherpath' parameter must start but not be equal to URI 'path/subpath/document' path",
                        "sftp://host/path/subpath/document?basePath=path/subpath/otherpath");
         }
      }

      @Nested
      class AbsoluteBasePath {

         @Test
         public void ofUriWhenBasePath() {
            assertThat(SftpDocumentUri.ofDocumentUri(uri("sftp://host//document"))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of("/"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
            assertThat(SftpDocumentUri.ofDocumentUri(uri("sftp://host//path/document"))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of("/"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/document"));
            });
            assertThat(SftpDocumentUri.ofDocumentUri("sftp://host//path/document")).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of("/"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/document"));
            });
         }

         @Test
         public void ofUriWhenInvalidBasePathParameter() {
            assertThat(SftpDocumentUri.ofDocumentUri(
                  "sftp://host//path/subpath/document?basePath=/../path/subpath")).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of("/path/subpath"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
            assertThat(SftpDocumentUri.ofDocumentUri(
                  "sftp://host//path/subpath/document?basePath=/path/subpath/..")).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of("/path"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("subpath/document"));
            });
            assertThatThrownBy(() -> SftpDocumentUri.ofDocumentUri("sftp://host//document?basePath=/path"))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid '%s' URI: 'basePath=/path' parameter must start but not be equal to URI '/document' path",
                        "sftp://host//document?basePath=/path");
         }

         @Test
         public void ofUriWhenExactBasePathParameter() {
            assertThat(SftpDocumentUri.ofDocumentUri("sftp://host//document?basePath=/")).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of("/"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
            assertThat(SftpDocumentUri.ofDocumentUri(
                  "sftp://host//path/subpath/document?basePath=/path/subpath")).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of("/path/subpath"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
            assertThat(SftpDocumentUri.ofDocumentUri(
                  "sftp://host//path/subpath/document?basePath=/path/./subpath/.")).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of("/path/subpath"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
         }

         @Test
         public void ofUriWhenPartialBasePathParameter() {
            assertThat(SftpDocumentUri.ofDocumentUri("sftp://host//path/subpath/document?basePath=/")).satisfies(
                  uri -> {
                     assertThat(uri.basePath()).isEqualTo(Path.of("/"));
                     assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/subpath/document"));
                  });
            assertThat(SftpDocumentUri.ofDocumentUri("sftp://host//path/subpath/document?basePath=/path")).satisfies(
                  uri -> {
                     assertThat(uri.basePath()).isEqualTo(Path.of("/path"));
                     assertThat(uri.documentId()).isEqualTo(DocumentPath.of("subpath/document"));
                  });
            assertThat(SftpDocumentUri.ofDocumentUri("sftp://host//path/subpath/document?basePath=/path/.")).satisfies(
                  uri -> {
                     assertThat(uri.basePath()).isEqualTo(Path.of("/path"));
                     assertThat(uri.documentId()).isEqualTo(DocumentPath.of("subpath/document"));
                  });
         }

         @Test
         public void ofUriWhenMismatchingBasePathParameter() {
            assertThatThrownBy(() -> SftpDocumentUri.ofDocumentUri(
                  "sftp://host//path/subpath/document?basePath=/otherpath"))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid '%s' URI: 'basePath=/otherpath' parameter must start but not be equal to URI '/path/subpath/document' path",
                        "sftp://host//path/subpath/document?basePath=/otherpath");
            assertThatThrownBy(() -> SftpDocumentUri.ofDocumentUri(
                  "sftp://host//path/subpath/document?basePath=/path/subpath/otherpath"))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid '%s' URI: 'basePath=/path/subpath/otherpath' parameter must start but not be equal to URI '/path/subpath/document' path",
                        "sftp://host//path/subpath/document?basePath=/path/subpath/otherpath");
         }
      }

      @Nested
      class WrappedUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(SftpDocumentUri.ofDocumentUri(uri("document:sftp:sftp://host/path/document"))).satisfies(
                  uri -> {
                     assertThat(uri.basePath()).isEqualTo(Path.of(""));
                     assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/document"));
                  });
         }

         @Test
         public void ofUriWhenBadRepositoryType() {
            assertThatThrownBy(() -> SftpDocumentUri.ofDocumentUri(uri("document:unknown:sftp://host/document")))
                  .isInstanceOf(IncompatibleUriException.class)
                  .hasMessage(
                        "Incompatible 'document:unknown:sftp://host/document' URI: Unsupported 'unknown' repository type");
         }

         @Test
         public void ofUriWhenNormalized() {
            assertThat(SftpDocumentUri.ofDocumentUri(uri("document:sftp:sftp://host/path/.././document"))).satisfies(
                  uri -> {
                     assertThat(uri.basePath()).isEqualTo(Path.of(""));
                     assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
                  });
            assertThat(SftpDocumentUri.ofDocumentUri("document:sftp:sftp://host/path/.././document")).satisfies(
                  uri -> {
                     assertThat(uri.basePath()).isEqualTo(Path.of(""));
                     assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
                  });
            assertThat(SftpDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(
                  "document:sftp:sftp://host/path/.././document"), Path.of(""))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
         }

      }
   }

   @Nested
   public class OfUriWhenDocumentUri {

      @Test
      public void ofUriWhenBadDefaultBasePath() {
         assertThatThrownBy(() -> SftpDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri("sftp://host/path"),
                                                                null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultBasePath' must not be null");
         assertThatThrownBy(() -> SftpDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri("sftp://host/path"),
                                                                Path.of("../path")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage(
                     "Invariant validation error > 'defaultBasePath=../path' must not have traversal paths");
      }

      @Nested
      public class WhenDocumentUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(SftpDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri("sftp://host/document"),
                                                     Path.of(""))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
            assertThat(SftpDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri("sftp://host/path/document"),
                                                     Path.of("path"))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of("path"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
         }

         @Test
         public void ofUriWhenImplicitDefaultBasePath() {
            assertThat(SftpDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri("sftp://host/document"))).satisfies(
                  uri -> {
                     assertThat(uri.basePath()).isEqualTo(Path.of(""));
                     assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
                  });
            assertThat(SftpDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(
                  "sftp://host/path/subpath/document"))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/subpath/document"));
            });
         }

         @Test
         public void ofUriWhenDefaultBasePath() {
            assertThat(SftpDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(
                  "sftp://host/path/subpath/document"), Path.of("path/subpath"))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of("path/subpath"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
            assertThat(SftpDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(
                  "sftp://host/path/subpath/document"), Path.of("path"))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of("path"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("subpath/document"));
            });
            assertThat(SftpDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(
                  "sftp://host/path/subpath/document"), Path.of(""))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/subpath/document"));
            });
         }

         @Test
         public void ofUriWhenNoBasePath() {
            assertThat(SftpDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri("sftp://host/document"))).satisfies(
                  uri -> {
                     assertThat(uri.basePath()).isEqualTo(Path.of(""));
                     assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
                  });
            assertThat(SftpDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri("sftp://host/document"),
                                                     Path.of(""))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
         }

         @Test
         public void ofUriWhenExplicitBasePath() {
            assertThat(SftpDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(
                  "sftp://host/path/subpath/document?basePath=path/subpath"))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of("path/subpath"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
            assertThat(SftpDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(
                  "sftp://host/path/subpath/document?basePath="))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/subpath/document"));
            });
         }

      }

      @Nested
      public class WhenSftpDocumentUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(SftpDocumentUri.ofDocumentUri(SftpDocumentUri.ofDocumentUri("sftp://host/path/document"),
                                                     Path.of("path"))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/document"));
            });
         }

         @Test
         public void ofUriWhenImplicitDefaultBasePath() {
            assertThat(SftpDocumentUri.ofDocumentUri(SftpDocumentUri.ofDocumentUri(
                  "sftp://host/path/subpath/document"))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/subpath/document"));
            });
         }

         @Test
         public void ofUriWhenDefaultBasePath() {
            assertThat(SftpDocumentUri.ofDocumentUri(SftpDocumentUri.ofDocumentUri(
                  "sftp://host/path/subpath/document"), Path.of("path/subpath"))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/subpath/document"));
            });
            assertThat(SftpDocumentUri.ofDocumentUri(SftpDocumentUri.ofDocumentUri(
                  "sftp://host/path/subpath/document"), Path.of("path"))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/subpath/document"));
            });
            assertThat(SftpDocumentUri.ofDocumentUri(SftpDocumentUri.ofDocumentUri(
                  "sftp://host/path/subpath/document"), Path.of(""))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/subpath/document"));
            });
         }

         @Test
         public void ofUriWhenNoBasePath() {
            assertThat(SftpDocumentUri.ofDocumentUri(SftpDocumentUri.ofDocumentUri("sftp://host/document"))).satisfies(
                  uri -> {
                     assertThat(uri.basePath()).isEqualTo(Path.of(""));
                     assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
                  });
            assertThat(SftpDocumentUri.ofDocumentUri(SftpDocumentUri.ofDocumentUri("sftp://host/document"),
                                                     Path.of(""))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
         }

         @Test
         public void ofUriWhenExplicitBasePath() {
            assertThat(SftpDocumentUri.ofDocumentUri(SftpDocumentUri.ofDocumentUri(
                  "sftp://host/path/subpath/document?basePath=path/subpath"))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of("path/subpath"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
            assertThat(SftpDocumentUri.ofDocumentUri(SftpDocumentUri.ofDocumentUri(
                  "sftp://host/path/subpath/document?basePath="))).satisfies(uri -> {
               assertThat(uri.basePath()).isEqualTo(Path.of(""));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/subpath/document"));
            });
         }

      }

   }

   @Nested
   public class ToUri {

      @Test
      public void toUriWhenNominal() {
         assertThat(SftpDocumentUri.ofDocumentUri("sftp://host/document").exportUri(noParameters())
                          .stringValue()).isEqualTo("sftp://host/document");
      }

      @Test
      public void toUriWhenRelativeBasePath() {
         assertThat(SftpDocumentUri.ofDocumentUri("sftp://host/document").exportUri(noParameters())
                          .stringValue()).isEqualTo("sftp://host/document");
         assertThat(SftpDocumentUri.ofDocumentUri("sftp://host/document/").exportUri(noParameters())
                          .stringValue()).isEqualTo("sftp://host/document");
         assertThat(SftpDocumentUri.ofDocumentUri("sftp://host/path/document").exportUri(noParameters())
                          .stringValue()).isEqualTo("sftp://host/path/document");
      }

      @Test
      public void toUriWhenAbsoluteBasePath() {
         assertThat(SftpDocumentUri.ofDocumentUri("sftp://host//document").exportUri(noParameters())
                          .stringValue()).isEqualTo("sftp://host//document");
         assertThat(SftpDocumentUri.ofDocumentUri("sftp://host//path/document").exportUri(noParameters())
                          .stringValue()).isEqualTo("sftp://host//path/document");
      }

      @Test
      public void toUriWhenDefaultParameters() {
         assertThat(SftpDocumentUri.ofDocumentUri("sftp://host/path/document").exportUri(defaultParameters())
                          .stringValue()).isEqualTo("sftp://host/path/document");
         assertThat(SftpDocumentUri
                          .ofDocumentUri("sftp://host/path/document?basePath=path")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("sftp://host/path/document?basePath=path");
         assertThat(SftpDocumentUri
                          .ofDocumentUri("sftp://host/path/document?allowUnknownHosts=false")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("sftp://host/path/document");
         assertThat(SftpDocumentUri
                          .ofDocumentUri("sftp://host/path/document?allowUnknownHosts=true")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("sftp://host/path/document?allowUnknownHosts=true");
         assertThat(SftpDocumentUri
                          .ofDocumentUri("sftp://user:password@host/path/document")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("sftp://user@host/path/document");
      }

      @Test
      public void toUriWhenDefaultParametersWithForceDefaultValues() {
         assertThat(SftpDocumentUri
                          .ofDocumentUri("sftp://host/path/document")
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo(
               "sftp://host/path/document?allowUnknownHosts=false&basePath=&connectTimeout=PT0S&keepAliveInterval=PT1M&sessionCaching=true&sessionPoolSize=10&sessionPoolWaitTimeout=PT0S&socketTimeout=PT0S&writeStrategy=com.tinubu.commons.ports.document.sftp.strategy.DefaultChunkedWriteStrategy");
         assertThat(SftpDocumentUri
                          .ofDocumentUri("sftp://host/path/document?basePath=path")
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo(
               "sftp://host/path/document?basePath=path&allowUnknownHosts=false&connectTimeout=PT0S&keepAliveInterval=PT1M&sessionCaching=true&sessionPoolSize=10&sessionPoolWaitTimeout=PT0S&socketTimeout=PT0S&writeStrategy=com.tinubu.commons.ports.document.sftp.strategy.DefaultChunkedWriteStrategy");
         assertThat(SftpDocumentUri
                          .ofDocumentUri("sftp://host/path/document?allowUnknownHosts=false")
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo(
               "sftp://host/path/document?allowUnknownHosts=false&basePath=&connectTimeout=PT0S&keepAliveInterval=PT1M&sessionCaching=true&sessionPoolSize=10&sessionPoolWaitTimeout=PT0S&socketTimeout=PT0S&writeStrategy=com.tinubu.commons.ports.document.sftp.strategy.DefaultChunkedWriteStrategy");
         assertThat(SftpDocumentUri
                          .ofDocumentUri("sftp://host/path/document?allowUnknownHosts=true")
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo(
               "sftp://host/path/document?allowUnknownHosts=true&basePath=&connectTimeout=PT0S&keepAliveInterval=PT1M&sessionCaching=true&sessionPoolSize=10&sessionPoolWaitTimeout=PT0S&socketTimeout=PT0S&writeStrategy=com.tinubu.commons.ports.document.sftp.strategy.DefaultChunkedWriteStrategy");

         assertThat(SftpDocumentUri
                          .ofDocumentUri("sftp://user:password@host/path/document")
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo(
               "sftp://user@host/path/document?allowUnknownHosts=false&basePath=&connectTimeout=PT0S&keepAliveInterval=PT1M&sessionCaching=true&sessionPoolSize=10&sessionPoolWaitTimeout=PT0S&socketTimeout=PT0S&writeStrategy=com.tinubu.commons.ports.document.sftp.strategy.DefaultChunkedWriteStrategy");

      }

      @Test
      public void toUriWhenSensitiveParameters() {
         assertThat(SftpDocumentUri
                          .ofDocumentUri("sftp://host/path/document")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("sftp://host/path/document");
         assertThat(SftpDocumentUri
                          .ofDocumentUri("sftp://host/path/document?basePath=path")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("sftp://host/path/document?basePath=path");
         assertThat(SftpDocumentUri
                          .ofDocumentUri(
                                "sftp://user:password@host/path/document?privateKey=a2V5&privateKeyPassphrase=passphrase")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo(
               "sftp://user:password@host/path/document?privateKey=a2V5&privateKeyPassphrase=passphrase");
      }

      @Test
      public void toUriWhenOriginalUriHasTerminalSlash() {
         assertThat(SftpDocumentUri.ofDocumentUri("sftp://host/document").exportUri(noParameters())
                          .stringValue()).isEqualTo("sftp://host/document");
         assertThat(SftpDocumentUri.ofDocumentUri("sftp://host/document/").exportUri(noParameters())
                          .stringValue()).isEqualTo("sftp://host/document");
         assertThat(SftpDocumentUri.ofDocumentUri("sftp://host/path/document/").exportUri(noParameters())
                          .stringValue()).isEqualTo("sftp://host/path/document");
         assertThat(SftpDocumentUri.ofDocumentUri("sftp://host//document").exportUri(noParameters())
                          .stringValue()).isEqualTo("sftp://host//document");
         assertThat(SftpDocumentUri.ofDocumentUri("sftp://host//path/document/").exportUri(noParameters())
                          .stringValue()).isEqualTo("sftp://host//path/document");
      }

      @Test
      public void toUriWhenOriginalUriHasTerminalSlashAndBasePathParameter() {
         assertThat(SftpDocumentUri
                          .ofDocumentUri("sftp://host/path/document/?basePath=path/")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("sftp://host/path/document");
         assertThat(SftpDocumentUri
                          .ofDocumentUri("sftp://host/path/document/?basePath=path")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("sftp://host/path/document");
         assertThat(SftpDocumentUri.ofDocumentUri("sftp://host/document?basePath=").exportUri(noParameters())
                          .stringValue()).isEqualTo("sftp://host/document");
         assertThat(SftpDocumentUri.ofDocumentUri("sftp://host/document/?basePath=").exportUri(noParameters())
                          .stringValue()).isEqualTo("sftp://host/document");

         assertThat(SftpDocumentUri
                          .ofDocumentUri("sftp://host//path/document/?basePath=/path/")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("sftp://host//path/document");
         assertThat(SftpDocumentUri
                          .ofDocumentUri("sftp://host//path/document/?basePath=/path")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("sftp://host//path/document");
         assertThat(SftpDocumentUri
                          .ofDocumentUri("sftp://host//document?basePath=/")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("sftp://host//document");
      }

   }
}