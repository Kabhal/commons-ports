/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.memory;

import static com.tinubu.commons.ddd2.uri.DefaultUriRestrictions.noRestrictions;
import static com.tinubu.commons.ddd2.uri.Uri.uri;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.defaultParameters;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.noParameters;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.sensitiveParameters;
import static com.tinubu.commons.ports.document.memory.Constants.DEFAULT_CASE_INSENSITIVE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.net.URI;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.RepositoryUri;

public class MemoryRepositoryUriTest {

   @Nested
   public class OfUriWhenURI {

      @Test
      public void ofUriWhenNominal() {
         assertThat(MemoryRepositoryUri.ofRepositoryUri(URI.create("memory:/"))).satisfies(uri -> {
            assertThat(uri.caseInsensitive()).isFalse();
         });
         assertThat(MemoryRepositoryUri.ofRepositoryUri("memory:/")).satisfies(uri -> {
            assertThat(uri.caseInsensitive()).isFalse();
         });
      }

      @Test
      public void ofUriWhenUri() {
         assertThat(MemoryRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(Uri.ofUri(
               "memory:/?caseInsensitive=true")))).satisfies(uri -> {
            assertThat(uri.caseInsensitive()).isTrue();
         });
         assertThat(MemoryRepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri(Uri.ofUri(
               "memory:/document?caseInsensitive=true")))).satisfies(uri -> {
            assertThat(uri.caseInsensitive()).isTrue();
         });
         assertThat(MemoryRepositoryUri.ofRepositoryUri(Uri.ofUri("memory:/?caseInsensitive=true"))).satisfies(
               uri -> {
                  assertThat(uri.caseInsensitive()).isTrue();
               });
      }

      @Test
      public void ofUriWhenMemoryUri() {
         assertThat(MemoryRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(MemoryUri.ofUri(
               "memory:/?caseInsensitive=true",
               noRestrictions())))).satisfies(uri -> {
            assertThat(uri.caseInsensitive()).isTrue();
         });
         assertThat(MemoryRepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri(MemoryUri.ofUri(
               "memory:/document?caseInsensitive=true",
               noRestrictions())))).satisfies(uri -> {
            assertThat(uri.caseInsensitive()).isTrue();
         });
         assertThat(MemoryRepositoryUri.ofRepositoryUri(MemoryUri.ofUri("memory:/?caseInsensitive=true",
                                                                        noRestrictions()))).satisfies(uri -> {
            assertThat(uri.caseInsensitive()).isTrue();
         });
      }

      @Test
      public void ofUriWhenBadParameters() {
         assertThatThrownBy(() -> MemoryRepositoryUri.ofRepositoryUri((URI) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
         assertThatThrownBy(() -> MemoryRepositoryUri.ofRepositoryUri((String) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
         assertThatThrownBy(() -> MemoryRepositoryUri.ofRepositoryUri((RepositoryUri) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
      }

      @Test
      public void ofUriWhenCaseSensitiveParameterKey() {
         assertThat(MemoryRepositoryUri.ofRepositoryUri("memory:/?CaSeInSeNsItIvE=true")).satisfies(uri -> {
            assertThat(uri.caseInsensitive()).isFalse();
         });
      }

      @Test
      public void ofUriWhenInvalidUriSyntax() {
         assertThatThrownBy(() -> MemoryRepositoryUri.ofRepositoryUri("git@git.com:user/repo.git"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'git@git.com:user/repo.git' URI: Illegal character in scheme name at index 3: git@git.com:user/repo.git");
         assertThatThrownBy(() -> MemoryRepositoryUri.ofRepositoryUri("git@git.com:user/repo.git"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'git@git.com:user/repo.git' URI: Illegal character in scheme name at index 3: git@git.com:user/repo.git");
      }

      @Test
      public void ofUriWhenIncompatibleUri() {
         assertThatThrownBy(() -> MemoryRepositoryUri.ofRepositoryUri("unknown:/"))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessageContainingAll("Incompatible 'unknown:/' URI",
                                        "must be absolute, and scheme must be equal to 'memory'");
         assertThatThrownBy(() -> MemoryRepositoryUri.ofRepositoryUri(URI.create("memory://user:password@host")))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessageContainingAll("Incompatible 'memory://user:password@host' URI",
                                        "must not have authority");
         assertThatThrownBy(() -> MemoryRepositoryUri.ofRepositoryUri("memory:path"))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessageContainingAll("Incompatible 'memory:path' URI", "must be hierarchical");
         assertThatThrownBy(() -> MemoryRepositoryUri.ofRepositoryUri("memory://host/"))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessageContainingAll("Incompatible 'memory://host/' URI", "must not have authority");
      }

      @Test
      public void ofUriWhenInvalidUri() {
         assertThatThrownBy(() -> MemoryRepositoryUri.ofRepositoryUri("memory:/#fragment"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessageContainingAll("Invalid 'memory:/#fragment' URI", "must not have a fragment");
      }

      @Test
      public void ofUriWhenPath() {
         assertThatThrownBy(() -> MemoryRepositoryUri.ofRepositoryUri(URI.create("memory:/path")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage("Invalid 'memory:/path' URI: Invalid path, must be /");
         assertThatThrownBy(() -> MemoryRepositoryUri.ofRepositoryUri("memory:/path"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage("Invalid 'memory:/path' URI: Invalid path, must be /");
      }

      @Test
      public void ofUriWhenDefaultParameters() {
         assertThat(MemoryRepositoryUri.ofRepositoryUri("memory:/")).satisfies(uri -> {
            assertThat(uri.caseInsensitive()).isEqualTo(DEFAULT_CASE_INSENSITIVE);
         });
      }

      @Test
      public void ofUriWhenAllParameters() {
         assertThat(MemoryRepositoryUri.ofRepositoryUri("memory:/?caseInsensitive=true")).satisfies(uri -> {
            assertThat(uri.caseInsensitive()).isTrue();
         });
      }

      @Nested
      class WrappedUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(MemoryRepositoryUri.ofRepositoryUri(URI.create("document:memory:memory:/"))).satisfies(
                  uri -> {
                     assertThat(uri.caseInsensitive()).isFalse();
                  });
         }

         @Test
         public void ofUriWhenBadRepositoryType() {
            assertThatThrownBy(() -> MemoryRepositoryUri.ofRepositoryUri(URI.create(
                  "document:unknown:memory:/")))
                  .isInstanceOf(IncompatibleUriException.class)
                  .hasMessage(
                        "Incompatible 'document:unknown:memory:/' URI: Unsupported 'unknown' repository type");
         }

         @Test
         public void ofUriWhenUnsupportedWrappedUri() {
            assertThatThrownBy(() -> MemoryRepositoryUri.ofRepositoryUri(uri(
                  "document:memory:unknown://host/path")))
                  .isInstanceOf(IncompatibleUriException.class)
                  .hasMessageContainingAll("Incompatible 'unknown://host/path' URI",
                                           "must be absolute, and scheme must be equal to 'memory'");
         }

      }
   }

   @Nested
   public class OfUriWhenRepositoryUri {

      @Nested
      public class WhenRepositoryUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(MemoryRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri("memory:/"))).satisfies(
                  uri -> {
                     assertThat(uri.caseInsensitive()).isFalse();
                  });
         }
      }

      @Nested
      public class WhenDocumentUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(MemoryRepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri("memory:/path/document"))).satisfies(
                  uri -> {
                     assertThat(uri.caseInsensitive()).isFalse();
                  });
         }
      }

      @Nested
      public class WhenMemoryRepositoryUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(MemoryRepositoryUri.ofRepositoryUri(MemoryRepositoryUri.ofRepositoryUri("memory:/"))).satisfies(
                  uri -> {
                     assertThat(uri.caseInsensitive()).isFalse();
                  });
         }
      }

      @Nested
      public class WhenMemoryDocumentUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(MemoryRepositoryUri.ofRepositoryUri(MemoryDocumentUri.ofDocumentUri(
                  "memory:/path/document"))).satisfies(uri -> {
               assertThat(uri.caseInsensitive()).isFalse();
            });
         }
      }

   }

   @Nested
   public class OfConfig {

      @Test
      public void ofConfigWhenNominal() {
         assertThat(MemoryRepositoryUri.ofConfig(true)).satisfies(uri -> {
            assertThat(uri.caseInsensitive()).isTrue();
         });
         assertThat(MemoryRepositoryUri.ofConfig(false)).satisfies(uri -> {
            assertThat(uri.caseInsensitive()).isFalse();
         });
      }

   }

   @Nested
   public class ToConfig {

      @Test
      public void toConfigWhenNominal() {
         assertThat(MemoryRepositoryUri.ofRepositoryUri("memory:/").caseInsensitive()).isFalse();
      }

   }

   @Nested
   public class ToUri {

      @Test
      public void toUriWhenNominal() {
         assertThat(MemoryRepositoryUri
                          .ofRepositoryUri("memory:/")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("memory:/");
      }

      @Test
      public void toUriWhenDefaultParameters() {
         assertThat(MemoryRepositoryUri
                          .ofRepositoryUri("memory:/")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("memory:/");
         assertThat(MemoryRepositoryUri
                          .ofRepositoryUri("memory:/?caseInsensitive=false")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("memory:/");
         assertThat(MemoryRepositoryUri
                          .ofRepositoryUri("memory:/?caseInsensitive=true")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("memory:/?caseInsensitive=true");

         assertThat(MemoryRepositoryUri
                          .ofConfig(false)
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("memory:/");
         assertThat(MemoryRepositoryUri
                          .ofConfig(true)
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("memory:/?caseInsensitive=true");
      }

      @Test
      public void toUriWhenDefaultParametersWithForceDefaultValues() {
         assertThat(MemoryRepositoryUri.ofRepositoryUri("memory:/").exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo("memory:/?caseInsensitive=false");
         assertThat(MemoryRepositoryUri
                          .ofRepositoryUri("memory:/?caseInsensitive=false")
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo("memory:/?caseInsensitive=false");
         assertThat(MemoryRepositoryUri
                          .ofRepositoryUri("memory:/?caseInsensitive=true")
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo("memory:/?caseInsensitive=true");

         assertThat(MemoryRepositoryUri.ofConfig(false).exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo("memory:/?caseInsensitive=false");
         assertThat(MemoryRepositoryUri.ofConfig(true).exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo("memory:/?caseInsensitive=true");
      }

      @Test
      public void toUriWhenSensitiveParameters() {
         assertThat(MemoryRepositoryUri.ofRepositoryUri("memory:/").exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("memory:/");
         assertThat(MemoryRepositoryUri
                          .ofRepositoryUri("memory:/?caseInsensitive=true")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("memory:/?caseInsensitive=true");
      }

   }

   @Nested
   public class NormalizeResolveRelativize {

      @Test
      public void normalizeWhenNominal() {
         assertThat(MemoryRepositoryUri.ofRepositoryUri("memory:/path/../.").normalize()).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo("memory:/");
         });
      }

      @Test
      public void resolveWhenNominal() {
         assertThat(MemoryRepositoryUri.ofRepositoryUri("memory:/").resolve(uri("/"))).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo("memory:/");
         });
      }

      @Test
      public void relativizeWhenNominal() {
         assertThatThrownBy(() -> MemoryRepositoryUri
               .ofRepositoryUri("memory:/")
               .relativize(uri("memory:/"))).isInstanceOf(UnsupportedOperationException.class);
      }

   }

}