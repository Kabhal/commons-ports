/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.memory;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.lang.convert.ConversionFailedException;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.RepositoryUri;

public class MemoryDocumentRepositoryLoadFromUriTest {

   @Test
   public void ofUriWhenNominal() {
      assertThat(MemoryDocumentRepository.ofUri(RepositoryUri.ofRepositoryUri("memory:/"))).satisfies(
            repository -> {
               assertThat(repository.caseInsensitive()).isFalse();
            });
   }

   @Test
   public void referencedDocumentWhenNominal() {
      assertThat(MemoryDocumentRepository.referencedDocument(DocumentUri.ofDocumentUri("memory:/path/subpath"))).satisfies(
            rd -> {
               assertThat(rd.documentRepository().caseInsensitive()).isFalse();
               assertThat(rd.documentId()).isEqualTo(DocumentPath.of("path/subpath"));
            });
   }

   @Test
   public void ofUriWhenRoot() {
      assertThat(MemoryDocumentRepository.ofUri(RepositoryUri.ofRepositoryUri("memory:/"))).satisfies(
            repository -> assertThat(repository.caseInsensitive()).isFalse());
   }

   @Test
   public void ofUriWhenIncompatibleUri() {
      assertThatThrownBy(() -> MemoryDocumentRepository.ofUri(RepositoryUri.ofRepositoryUri("unknown:/host")))
            .isInstanceOf(IncompatibleUriException.class)
            .hasMessageContainingAll("Incompatible 'unknown:/host' URI",
                                     "must be absolute, and scheme must be equal to 'memory'");
   }

   @Test
   public void referencedDocumentWhenIncompatibleUri() {
      assertThatThrownBy(() -> MemoryDocumentRepository.referencedDocument(DocumentUri.ofDocumentUri(
            "unknown:/host")))
            .isInstanceOf(IncompatibleUriException.class)
            .hasMessageContainingAll("Incompatible 'unknown:/host' URI",
                                     "must be absolute, and scheme must be equal to 'memory'");
   }

   @Test
   @Disabled("Boolean.valueOf is too much tolerant so that there's no way to generate an error ;)")
   public void ofUriWhenIllegalArgument() {
      assertThatExceptionOfType(ConversionFailedException.class)
            .isThrownBy(() -> MemoryDocumentRepository.ofUri(RepositoryUri.ofRepositoryUri(
                  "memory:/?caseInsensitive=Invalid")))
            .withMessage(
                  "Conversion from 'java.lang.String' to 'java.lang.Boolean' failed for 'Invalid' value");
   }

   @Test
   @Disabled("Boolean.valueOf is too much tolerant so that there's no way to generate an error ;)")
   public void referencedDocumentWhenIllegalArgument() {
      assertThatExceptionOfType(ConversionFailedException.class)
            .isThrownBy(() -> MemoryDocumentRepository.referencedDocument(DocumentUri.ofDocumentUri(
                  "memory:/path?caseInsensitive=Invalid")))
            .withMessage(
                  "Conversion from 'java.lang.String' to 'java.lang.Boolean' failed for 'Invalid' value");
   }

   @Test
   public void ofUriWhenAllParameters() {
      assertThat(MemoryDocumentRepository.ofUri(RepositoryUri.ofRepositoryUri("memory:/?caseInsensitive=true"))).satisfies(
            repository -> {
               assertThat(repository.caseInsensitive()).isTrue();
            });
   }

   @Test
   public void referencedDocumentWhenAllParameters() {
      assertThat(MemoryDocumentRepository.referencedDocument(DocumentUri.ofDocumentUri(
            "memory:/path/subpath?caseInsensitive=true"))).satisfies(rd -> {
         assertThat(rd.documentRepository().caseInsensitive()).isTrue();
         assertThat(rd.documentId()).isEqualTo(DocumentPath.of("path/subpath"));
      });
   }

}
