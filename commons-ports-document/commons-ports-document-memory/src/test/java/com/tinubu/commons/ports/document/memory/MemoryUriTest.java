/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.memory;

import static com.tinubu.commons.ddd2.test.uri.UriAssert.assertThat;
import static com.tinubu.commons.ddd2.uri.Uri.uri;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.net.URI;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ddd2.uri.DefaultUriRestrictions;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.uri.Uri;

public class MemoryUriTest {

   static {
      Assertions.setMaxStackTraceElementsDisplayed(50);
   }

   @Test
   public void memoryUriWhenBadArguments() {
      assertThatThrownBy(() -> MemoryUri.ofUri((String) null))
            .isInstanceOf(InvariantValidationException.class)
            .hasMessage("Invariant validation error > 'uri' must not be null");
      assertThatThrownBy(() -> MemoryUri.ofUri((URI) null))
            .isInstanceOf(InvariantValidationException.class)
            .hasMessage("Invariant validation error > 'uri' must not be null");
      assertThatThrownBy(() -> MemoryUri.ofUri((Uri) null))
            .isInstanceOf(InvariantValidationException.class)
            .hasMessage("Invariant validation error > 'uri' must not be null");
   }

   @Nested
   class OfUri {

      @Test
      public void memoryUriWhenNominal() {
         assertThat(MemoryUri.ofUri("memory:/")).satisfies(uri -> {
            assertThat(uri).hasPath(false, "/");
            assertThat(uri).hasNoQuery();
         });
      }

      @Test
      public void memoryUriWhenPath() {
         assertThat(MemoryUri.ofUri("memory:/path")).satisfies(uri -> {
            assertThat(uri).hasPath(false, "/path");
            assertThat(uri).hasNoQuery();
         });
      }

      @Test
      public void memoryUriWhenQuery() {
         assertThatThrownBy(() -> MemoryUri.ofUri("memory:/?query"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'memory:/?query' URI: 'MemoryUri[value=memory:/?query,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=true, fragment=true],scheme=SimpleScheme[scheme='memory'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='/?query', encodedSchemeSpecific='/?query'],path=SimplePath[path=/, encodedPath=/],query=SimpleQuery[query=query, encodedQuery=query],fragment=<null>]' must not have a query");
         assertThat(MemoryUri.ofUri("memory:/?query",
                                    DefaultUriRestrictions.ofRestrictions(false, true))).satisfies(uri -> {
            assertThat(uri).hasPath(false, "/");
            assertThat(uri).hasQuery(false, "query");
         });
      }

      @Test
      public void memoryUriWhenFragment() {
         assertThatThrownBy(() -> MemoryUri.ofUri("memory:/#fragment"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'memory:/#fragment' URI: 'MemoryUri[value=memory:/#fragment,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=true, fragment=true],scheme=SimpleScheme[scheme='memory'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='/', encodedSchemeSpecific='/'],path=SimplePath[path=/, encodedPath=/],query=<null>,fragment=SimpleFragment[fragment=fragment, encodedFragment=fragment]]' must not have a fragment");
         assertThat(MemoryUri.ofUri("memory:/#fragment",
                                    DefaultUriRestrictions.ofRestrictions(true, false))).satisfies(uri -> {
            assertThat(uri).hasPath(false, "/");
            assertThat(uri).hasFragment(false, "fragment");
         });
      }
   }

   @Test
   public void memoryUriWhenIncompatibleUri() {
      assertThatThrownBy(() -> MemoryUri.ofUri("other:/"))
            .isInstanceOf(IncompatibleUriException.class)
            .hasMessageContainingAll(
                  "Incompatible 'other:/' URI: 'MemoryUri[value=other:/,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=true, fragment=true],scheme=SimpleScheme[scheme='other'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='/', encodedSchemeSpecific='/'],path=SimplePath[path=/, encodedPath=/],query=<null>,fragment=<null>]' must be absolute, and scheme must be equal to 'memory'");
      assertThatThrownBy(() -> MemoryUri.ofUri("memory://host/"))
            .isInstanceOf(IncompatibleUriException.class)
            .hasMessageContainingAll(
                  "Incompatible 'memory://host/' URI: 'MemoryUri[value=memory://host/,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=true, fragment=true],scheme=SimpleScheme[scheme='memory'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='//host/', encodedSchemeSpecific='//host/'],path=SimplePath[path=/, encodedPath=/],query=<null>,fragment=<null>]' must not have authority");
      assertThatThrownBy(() -> MemoryUri.ofUri("memory://host"))
            .isInstanceOf(IncompatibleUriException.class)
            .hasMessageContainingAll(
                  "Incompatible 'memory://host' URI: 'MemoryUri[value=memory://host,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=true, fragment=true],scheme=SimpleScheme[scheme='memory'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='//host', encodedSchemeSpecific='//host'],path=SimplePath[path=, encodedPath=],query=<null>,fragment=<null>]' must not have authority");
      assertThatThrownBy(() -> MemoryUri.ofUri("/"))
            .isInstanceOf(IncompatibleUriException.class)
            .hasMessageContainingAll(
                  "Incompatible '/' URI: 'MemoryUri[value=/,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=true, fragment=true],scheme=<null>,schemeSpecific=<null>,path=SimplePath[path=/, encodedPath=/],query=<null>,fragment=<null>]' must be absolute");
      assertThatThrownBy(() -> MemoryUri.ofUri("//host/path"))
            .isInstanceOf(IncompatibleUriException.class)
            .hasMessageContainingAll(
                  "Incompatible '//host/path' URI: 'MemoryUri[value=//host/path,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=true, fragment=true],scheme=<null>,schemeSpecific=<null>,path=SimplePath[path=/path, encodedPath=/path],query=<null>,fragment=<null>]' must be absolute");
      assertThatThrownBy(() -> MemoryUri.ofUri("//host"))
            .isInstanceOf(IncompatibleUriException.class)
            .hasMessageContainingAll(
                  "Incompatible '//host' URI: 'MemoryUri[value=//host,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=true, fragment=true],scheme=<null>,schemeSpecific=<null>,path=SimplePath[path=, encodedPath=],query=<null>,fragment=<null>]' must be absolute");
   }

   @Test
   public void memoryUriWhenInvalidUri() {
      assertThatThrownBy(() -> MemoryUri.ofUri("memory:"))
            .isInstanceOf(InvalidUriException.class)
            .hasMessage("Invalid 'memory:' URI: Expected scheme-specific part at index 7: memory:");
      assertThatThrownBy(() -> MemoryUri.ofUri("memory://"))
            .isInstanceOf(InvalidUriException.class)
            .hasMessage("Invalid 'memory://' URI: Expected authority at index 9: memory://");
      assertThatThrownBy(() -> MemoryUri.ofUri("//"))
            .isInstanceOf(InvalidUriException.class)
            .hasMessage("Invalid '//' URI: Expected authority at index 2: //");
   }

   @Nested
   class NormalizeTest {

      @Test
      public void normalizeWhenNominal() {
         assertThat(MemoryUri.ofUri("memory:/").normalize()).satisfies(uri -> {
            assertThat(uri).hasPath(false, "/");
            assertThat(uri).hasNoQuery();
         });
      }

      @Test
      public void normalizeWhenQuery() {
         assertThat(MemoryUri.ofUri("memory:/?query", DefaultUriRestrictions.ofRestrictions(false, true))
                          .normalize()).satisfies(uri -> {
            assertThat(uri).hasPath(false, "/");
            assertThat(uri).hasQuery(false, "query");
         });
      }

   }

   @Nested
   class RelativizeTest {

      @Test
      public void relativizeWhenNominal() {
         assertThatThrownBy(() -> MemoryUri.ofUri("memory:/").relativize(uri("memory:/"))).isInstanceOf(
               UnsupportedOperationException.class);
      }

   }
}