/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.memory;

import static com.tinubu.commons.ddd2.uri.DefaultUriRestrictions.noRestrictions;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.defaultParameters;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.noParameters;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.sensitiveParameters;
import static com.tinubu.commons.ports.document.memory.Constants.DEFAULT_CASE_INSENSITIVE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.net.URI;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;

public class MemoryDocumentUriTest {

   @Nested
   public class OfUriWhenURI {

      @Test
      public void ofUriWhenNominal() {
         assertThat(MemoryDocumentUri.ofDocumentUri(URI.create("memory:/document"))).satisfies(uri -> {
            assertThat(uri.caseInsensitive()).isFalse();
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
         assertThat(MemoryDocumentUri.ofDocumentUri("memory:/document")).satisfies(uri -> {
            assertThat(uri.caseInsensitive()).isFalse();
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
      }

      @Test
      public void ofUriWhenUri() {
         assertThat(MemoryDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(Uri.ofUri(
               "memory:/document?caseInsensitive=true")))).satisfies(uri -> {
            assertThat(uri.caseInsensitive()).isTrue();
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
         assertThat(MemoryDocumentUri.ofDocumentUri(Uri.ofUri("memory:/document?caseInsensitive=true"))).satisfies(
               uri -> {
                  assertThat(uri.caseInsensitive()).isTrue();
                  assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
               });
      }

      @Test
      public void ofUriWhenMemoryUri() {
         assertThat(MemoryDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(MemoryUri.ofUri(
               "memory:/document?caseInsensitive=true",
               noRestrictions())))).satisfies(uri -> {
            assertThat(uri.caseInsensitive()).isTrue();
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
         assertThat(MemoryDocumentUri.ofDocumentUri(MemoryUri.ofUri("memory:/document?caseInsensitive=true",
                                                                    noRestrictions()))).satisfies(uri -> {
            assertThat(uri.caseInsensitive()).isTrue();
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
      }

      @Test
      public void ofUriWhenBadParameters() {
         assertThatThrownBy(() -> MemoryDocumentUri.ofDocumentUri((URI) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
         assertThatThrownBy(() -> MemoryDocumentUri.ofDocumentUri((String) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
         assertThatThrownBy(() -> MemoryDocumentUri.ofDocumentUri((DocumentUri) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
      }

      @Test
      public void ofUriWhenCaseSensitiveParameterKey() {
         assertThat(MemoryDocumentUri.ofDocumentUri("memory:/path/document?CaSeInSeNsItIvE=true")).satisfies(
               uri -> {
                  assertThat(uri.caseInsensitive()).isFalse();
               });
      }

      @Test
      public void ofUriWhenNormalized() {
         assertThat(MemoryDocumentUri.ofDocumentUri(URI.create("memory:/path/.././document"))).satisfies(uri -> {
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
         assertThat(MemoryDocumentUri.ofDocumentUri("memory:/path/.././document")).satisfies(uri -> {
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
         assertThat(MemoryDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri("memory:/path/.././document"))).satisfies(
               uri -> {
                  assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
               });
         assertThatThrownBy(() -> MemoryDocumentUri.ofDocumentUri(URI.create("memory:/../path/document")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessageContainingAll("Invalid 'memory:/../path/document' URI",
                                        "must not have traversal paths");
      }

      @Test
      public void ofUriWhenInvalidUriSyntax() {
         assertThatThrownBy(() -> MemoryDocumentUri.ofDocumentUri("git@git.com:user/repo.git"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'git@git.com:user/repo.git' URI: Illegal character in scheme name at index 3: git@git.com:user/repo.git");
         assertThatThrownBy(() -> MemoryDocumentUri.ofDocumentUri("git@git.com:user/repo.git"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'git@git.com:user/repo.git' URI: Illegal character in scheme name at index 3: git@git.com:user/repo.git");
      }

      @Test
      public void ofUriWhenIncompatibleUri() {
         assertThatThrownBy(() -> MemoryDocumentUri.ofDocumentUri(URI.create("unknown:/path/document")))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessageContainingAll("Incompatible 'unknown:/path/document' URI",
                                        "must be absolute, and scheme must be equal to 'memory'");
         assertThatThrownBy(() -> MemoryDocumentUri.ofDocumentUri(URI.create(
               "memory://user:password@host/document")))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessageContainingAll("Incompatible 'memory://user:password@host/document' URI",
                                        "must not have authority");
         assertThatThrownBy(() -> MemoryDocumentUri.ofDocumentUri("memory:path/document"))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessageContainingAll("Incompatible 'memory:path/document' URI", "must be hierarchical");
         assertThatThrownBy(() -> MemoryDocumentUri.ofDocumentUri("memory://host/path/document"))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessageContainingAll("Incompatible 'memory://host/path/document' URI",
                                        "must not have authority");
      }

      @Test
      public void ofUriWhenInvalidUri() {
         assertThatThrownBy(() -> MemoryDocumentUri.ofDocumentUri("memory:/path/document#fragment"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessageContainingAll("Invalid 'memory:/path/document#fragment' URI",
                                        "must not have a fragment");
      }

      @Test
      public void ofUriWhenRoot() {
         assertThatThrownBy(() -> MemoryDocumentUri.ofDocumentUri("memory:/"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage("Invalid 'memory:/' URI: Missing document id");
      }

      @Test
      public void ofUriWhenDefaultParameters() {
         assertThat(MemoryDocumentUri.ofDocumentUri("memory:/document")).satisfies(uri -> {
            assertThat(uri.caseInsensitive()).isEqualTo(DEFAULT_CASE_INSENSITIVE);
         });
      }

      @Test
      public void ofUriWhenAllParameters() {
         assertThat(MemoryDocumentUri.ofDocumentUri("memory:/document?caseInsensitive=true")).satisfies(uri -> {
            assertThat(uri.caseInsensitive()).isTrue();
         });
      }

      @Nested
      class WrappedUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(MemoryDocumentUri.ofDocumentUri(URI.create("document:memory:memory:/document"))).satisfies(
                  uri -> {
                     assertThat(uri.caseInsensitive()).isFalse();
                     assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
                  });
         }

         @Test
         public void ofUriWhenBadRepositoryType() {
            assertThatThrownBy(() -> MemoryDocumentUri.ofDocumentUri(URI.create(
                  "document:unknown:memory:/document")))
                  .isInstanceOf(IncompatibleUriException.class)
                  .hasMessage(
                        "Incompatible 'document:unknown:memory:/document' URI: Unsupported 'unknown' repository type");
         }

         @Test
         public void ofUriWhenNormalized() {
            assertThat(MemoryDocumentUri.ofDocumentUri(URI.create("document:memory:memory:/path/.././document"))).satisfies(
                  uri -> {
                     assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
                  });
            assertThat(MemoryDocumentUri.ofDocumentUri("document:memory:memory:/path/.././document")).satisfies(
                  uri -> {
                     assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
                  });
            assertThat(MemoryDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(
                  "document:memory:memory:/path/.././document"))).satisfies(uri -> {
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
            assertThatThrownBy(() -> MemoryDocumentUri.ofDocumentUri(URI.create(
                  "document:memory:memory:/../path/document")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessageContainingAll("Invalid 'memory:/../path/document' URI",
                                           "must not have traversal paths");
         }

      }
   }

   @Nested
   public class OfUriWhenDocumentUri {

      @Nested
      public class WhenDocumentUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(MemoryDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri("memory:/path/document"))).satisfies(
                  uri -> {
                     assertThat(uri.caseInsensitive()).isFalse();
                     assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/document"));
                  });
         }

      }

      @Nested
      public class WhenMemoryDocumentUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(MemoryDocumentUri.ofDocumentUri(MemoryDocumentUri.ofDocumentUri("memory:/path/document"))).satisfies(
                  uri -> {
                     assertThat(uri.caseInsensitive()).isFalse();
                     assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/document"));
                  });
         }
      }
   }

   @Nested
   public class OfConfig {

      @Test
      public void ofConfigWhenNominal() {
         assertThat(MemoryDocumentUri.ofConfig(true, DocumentPath.of("document"))).satisfies(uri -> {
            assertThat(uri.caseInsensitive()).isTrue();
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
         });
      }

   }

   @Nested
   public class ToUri {

      @Test
      public void toUriWhenNominal() {
         assertThat(MemoryDocumentUri
                          .ofDocumentUri("memory:/document")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("memory:/document");
      }

      @Test
      public void toUriWhenDefaultParameters() {
         assertThat(MemoryDocumentUri.ofDocumentUri("memory:/path/document").exportUri(defaultParameters())
                          .stringValue()).isEqualTo("memory:/path/document");
         assertThat(MemoryDocumentUri
                          .ofDocumentUri("memory:/path/document?caseInsensitive=false")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("memory:/path/document");
         assertThat(MemoryDocumentUri
                          .ofDocumentUri("memory:/path/document?caseInsensitive=true")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("memory:/path/document?caseInsensitive=true");
      }

      @Test
      public void toUriWhenDefaultParametersWithForceDefaultValues() {
         assertThat(MemoryDocumentUri
                          .ofDocumentUri("memory:/path/document")
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo("memory:/path/document?caseInsensitive=false");
         assertThat(MemoryDocumentUri
                          .ofDocumentUri("memory:/path/document?caseInsensitive=false")
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo("memory:/path/document?caseInsensitive=false");
         assertThat(MemoryDocumentUri
                          .ofDocumentUri("memory:/path/document?caseInsensitive=true")
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo("memory:/path/document?caseInsensitive=true");
      }

      @Test
      public void toUriWhenSensitiveParameters() {
         assertThat(MemoryDocumentUri.ofDocumentUri("memory:/path/document").exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("memory:/path/document");
         assertThat(MemoryDocumentUri
                          .ofDocumentUri("memory:/path/document?caseInsensitive=true")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("memory:/path/document?caseInsensitive=true");
      }

   }
}