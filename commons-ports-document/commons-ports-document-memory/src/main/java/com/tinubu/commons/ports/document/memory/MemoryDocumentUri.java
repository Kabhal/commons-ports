/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.memory;

import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;

import java.net.URI;

import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.domain.type.Id;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.ExportUriOptions;

/**
 * Memory URI document URI. This URI must be used to identify a document, use
 * {@link MemoryRepositoryUri} to identify a repository.
 * <p>
 * You can use this class when a {@link MemoryRepositoryUri} is required, but your current URI is a
 * document URI.
 * <p>
 * Supported URI format :
 * <ul>
 *    <li>{@code memory:/<document-id>[/?<parameters>]}</li>
 *    <li>Commons-ports wrapped URI : {@code document:memory:<memory-uri>}</li>
 * </ul>
 *
 * @see MemoryRepositoryUri
 */
public class MemoryDocumentUri extends MemoryRepositoryUri implements DocumentUri {

   protected MemoryDocumentUri(MemoryUri uri) {
      super(uri, true);
   }

   @Override
   @SuppressWarnings("unchecked")
   protected Fields<? extends MemoryDocumentUri> defineDomainFields() {
      return Fields
            .<MemoryDocumentUri>builder()
            .superFields((Fields<MemoryDocumentUri>) super.defineDomainFields())
            .field("documentId", v -> v.documentId, isNotNull())
            .build();
   }

   /**
    * Creates a document URI from {@link Uri}.
    *
    * @param uri document URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static MemoryDocumentUri ofDocumentUri(Uri uri) {
      Check.notNull(uri, "uri");

      validateUriCompatibility(uri, "uri", isCompatibleUriType(MemoryUri.class));

      return buildUri(() -> new MemoryDocumentUri(memoryUri(uri)));
   }

   /**
    * Creates a document URI from {@link URI}.
    *
    * @param uri document URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static MemoryDocumentUri ofDocumentUri(URI uri) {
      Check.notNull(uri, "uri");

      return ofDocumentUri(Uri.ofUri(uri));
   }

   /**
    * Creates a document URI from URI string.
    * URI syntax is checked as part of this operation.
    *
    * @param uri document URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static MemoryDocumentUri ofDocumentUri(String uri) {
      Check.notNull(uri, "uri");

      return ofDocumentUri(Uri.ofUri(uri));
   }

   /**
    * Creates a document URI from configuration.
    *
    * @param caseInsensitive repository configuration
    * @param documentId document id
    *
    * @return new instance
    */
   public static MemoryDocumentUri ofConfig(boolean caseInsensitive, DocumentPath documentId) {
      return buildUri(() -> new MemoryDocumentUri(memoryUri(caseInsensitive, documentId)));
   }

   /**
    * Creates a document URI from {@link DocumentUri}.
    *
    * @param uri document URI
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    * @implSpec this method is package-private because user code should always create
    *       {@link DocumentUri} directly from a URI.
    */
   static MemoryDocumentUri ofDocumentUri(DocumentUri uri) {
      Check.notNull(uri, "uri");

      validateUriCompatibility(uri, "uri", isCompatibleDocumentUriType(MemoryDocumentUri.class));

      return buildUri(() -> new MemoryDocumentUri(memoryUri(uri)));
   }

   public DocumentPath documentId() {
      return documentId;
   }

   @Override
   public DocumentUri exportUri(ExportUriOptions options) {
      return DocumentUri.ofDocumentUri(exportURI(options));
   }

   @Override
   public MemoryDocumentUri normalize() {
      return buildUri(() -> new MemoryDocumentUri(memoryUri().normalize()));
   }

   @Override
   public MemoryDocumentUri resolve(Uri uri) {
      return buildUri(() -> new MemoryDocumentUri(memoryUri().resolve(uri)));
   }

   @Override
   public MemoryDocumentUri resolve(URI uri) {
      return buildUri(() -> new MemoryDocumentUri(memoryUri().resolve(uri)));
   }

   @Override
   public MemoryDocumentUri relativize(Uri uri) {
      throw new UnsupportedOperationException();
   }

   @Override
   public MemoryDocumentUri relativize(URI uri) {
      throw new UnsupportedOperationException();
   }

   @Override
   public int compareTo(Id o) {
      if (!(o instanceof MemoryDocumentUri)) {
         return -1;
      } else {
         return super.compareTo(o);
      }
   }
}
