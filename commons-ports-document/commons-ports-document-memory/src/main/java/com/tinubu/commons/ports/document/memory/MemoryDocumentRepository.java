/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.memory;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotAbsolute;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.instanceOf;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.OptionalUtils.optionalPredicate;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.ports.document.memory.Constants.DEFAULT_CASE_INSENSITIVE;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.time.StopWatch;

import com.tinubu.commons.ddd2.domain.event.RegistrableDomainEventService;
import com.tinubu.commons.ddd2.domain.event.SynchronousDomainEventService;
import com.tinubu.commons.ddd2.domain.repository.Repository;
import com.tinubu.commons.ddd2.domain.specification.Specification;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.datetime.ApplicationClock;
import com.tinubu.commons.lang.validation.CheckReturnValue;
import com.tinubu.commons.ports.document.domain.AbstractDocumentRepository;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentContent;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentMetadata;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent.InputStreamDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata;
import com.tinubu.commons.ports.document.domain.OutputStreamDocumentContent;
import com.tinubu.commons.ports.document.domain.ReferencedDocument;
import com.tinubu.commons.ports.document.domain.uri.DocumentRepositoryUriAdapter;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.ExportUriOptions;
import com.tinubu.commons.ports.document.domain.uri.RepositoryUri;
import com.tinubu.commons.ports.document.transformer.DocumentTransformerUriAdapter;

/**
 * In-memory {@link DocumentRepository} adapter implementation.
 * <p>
 * Supported URI parameters :
 * <ul>
 *    <li>{@code caseInsensitive (Boolean)} : whether repository is case-insensitive for document search</li>
 * </ul>
 *
 * @implSpec Immutable class implementation
 */
public class MemoryDocumentRepository extends AbstractDocumentRepository<MemoryDocumentRepository> {
   private final Map<DocumentPath, Document> documentStorage;

   private final boolean caseInsensitive;
   private final DocumentRepositoryUriAdapter uriAdapter;

   private MemoryDocumentRepository(Map<DocumentPath, Document> documentStorage,
                                    boolean caseInsensitive,
                                    RegistrableDomainEventService eventService) {
      super(eventService);

      this.documentStorage = nullable(documentStorage, new ConcurrentHashMap<>());
      this.caseInsensitive = caseInsensitive;
      this.uriAdapter = new MemoryUriAdapter();
   }

   public MemoryDocumentRepository(boolean caseInsensitive, RegistrableDomainEventService eventService) {
      this(null, caseInsensitive, eventService);
   }

   public MemoryDocumentRepository(boolean caseInsensitive) {
      this(caseInsensitive, new SynchronousDomainEventService());
   }

   public MemoryDocumentRepository() {
      this(DEFAULT_CASE_INSENSITIVE);
   }

   /**
    * Creates a new document repository from URI with parameters.
    *
    * @param uri URI
    *
    * @return new document repository
    *
    * @throws InvalidUriException if specified URI is compatible with this repository but invalid
    * @throws IncompatibleUriException if specified URI is not compatible with this repository
    */
   public static MemoryDocumentRepository ofUri(RepositoryUri uri) {
      Check.notNull(uri, "uri");

      return new MemoryDocumentRepository(MemoryRepositoryUri.ofRepositoryUri(uri).caseInsensitive());
   }

   /**
    * Creates a lazily referenced document, in a new document repository, from URI with parameters. URI must
    * be compatible with this repository and identify a document.
    *
    * @param documentUri document URI
    *
    * @return new document repository
    *
    * @throws InvalidUriException if specified URI is compatible with this repository but invalid
    * @throws IncompatibleUriException if specified URI is not compatible with this repository
    */
   public static ReferencedDocument<MemoryDocumentRepository> referencedDocument(DocumentUri documentUri) {
      Check.notNull(documentUri, "documentUri");

      var uri = MemoryDocumentUri.ofDocumentUri(documentUri);

      return ReferencedDocument.lazyLoad(new MemoryDocumentRepository(uri.caseInsensitive()),
                                         uri.documentId());
   }

   @Getter
   public boolean caseInsensitive() {
      return caseInsensitive;
   }

   @Override
   public boolean sameRepositoryAs(Repository<Document, DocumentPath> documentRepository) {
      validate(documentRepository, "documentRepository", isNotNull()).orThrow();

      return Objects.equals(this, documentRepository);
   }

   @Override
   public MemoryDocumentRepository subPath(Path subPath, boolean shareContext) {
      validate(subPath, "subPath", isNotAbsolute().andValue(hasNoTraversal())).orThrow();

      Map<DocumentPath, Document> subPathDocumentStorage = documentStorage
            .values()
            .stream()
            .flatMap(document -> stream(document
                                              .documentId()
                                              .subPath(subPath)
                                              .map(documentId -> DocumentBuilder
                                                    .from(document)
                                                    .documentId(documentId)
                                                    .build())))
            .collect(toMap(Document::documentId, identity()));

      return new MemoryDocumentRepository(subPathDocumentStorage, caseInsensitive, eventService);
   }

   @Override
   @CheckReturnValue
   public Optional<Document> openDocument(DocumentPath documentId,
                                          boolean overwrite,
                                          boolean append,
                                          OpenDocumentMetadata metadata) {

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEvent(() -> {
         if (!overwrite && !append && documentStorage.containsKey(documentId)) {
            return optional();
         }

         Document documentToSave = new DocumentBuilder()
               .documentId(documentId)
               .content(MemoryOutputStreamDocumentContent.create(metadata.contentEncoding().orElse(null)))
               .chain(metadata.chainDocumentBuilder())
               .build();

         if (append && documentStorage.containsKey(documentId)) {
            try {
               IOUtils.copy(documentStorage.get(documentId).readableContent().content().inputStreamContent(),
                            documentToSave.content().outputStreamContent());
            } catch (IOException e) {
               throw new DocumentAccessException(e);
            }
         }

         Document savedDocument = documentStorage.merge(adaptCase(documentId),
                                                        documentToSave,
                                                        (pv, nv) -> overwrite || append
                                                                    ? documentToSave
                                                                    : pv);

         return optionalPredicate(savedDocument, sd -> sd == documentToSave);
      }, d -> documentSaved(d.documentEntry(), watch));
   }

   @Override
   public Optional<Document> findDocumentById(DocumentPath documentId) {
      validate(documentId, "documentId", isNotNull()).orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEvent(() -> {
         return nullable(documentStorage.get(adaptCase(documentId))).map(d -> {
            DocumentContent adaptedContent = adaptContent(d.content());
            DocumentMetadata adaptedMetadata = DocumentMetadataBuilder
                  .from(d.metadata())
                  .contentSize(adaptedContent.contentSize().orElse(null))
                  .build();

            return DocumentBuilder
                  .from(d)
                  .<DocumentBuilder>conditionalChain(__ -> caseInsensitive, b -> b.documentId(documentId))
                  .metadata(adaptedMetadata)
                  .content(adaptedContent)
                  .build();
         });
      }, d -> documentAccessed(d.documentEntry(), watch));
   }

   /**
    * Potentially converts an {@link OutputStreamDocumentContent} to an
    * {@link InputStreamDocumentContentBuilder}.
    */
   private DocumentContent adaptContent(DocumentContent content) {
      if (content instanceof MemoryOutputStreamDocumentContent) {
         return new InputStreamDocumentContentBuilder()
               .content(new ByteArrayInputStream(content.content()),
                        content.contentEncoding().orElse(null),
                        (long) content.content().length)
               .build();
      } else {
         return content;
      }
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryById(DocumentPath documentId) {
      validate(documentId, "documentId", isNotNull()).orThrow();

      return findDocumentById(documentId).map(Document::documentEntry);
   }

   @Override
   public Stream<Document> findDocumentsBySpecification(Path basePath,
                                                        Specification<DocumentEntry> specification) {
      validate(basePath, "basePath", isNotAbsolute().andValue(hasNoTraversal()))
            .and(validate(specification, "specification", isNotNull()))
            .orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentStreamEvent(() -> stream(documentStorage.values())
                                             .filter(d -> specification.satisfiedBy(d.documentEntry()))
                                             .map(d -> DocumentBuilder.from(d).content(adaptContent(d.content())).build()),
                                       d -> documentAccessed(d.documentEntry(), watch));
   }

   @Override
   public Stream<DocumentEntry> findDocumentEntriesBySpecification(Path basePath,
                                                                   Specification<DocumentEntry> specification) {
      validate(basePath, "basePath", isNotAbsolute().andValue(hasNoTraversal()))
            .and(validate(specification, "specification", isNotNull()))
            .orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEntryStreamEvent(() -> stream(documentStorage.values())
            .map(Document::documentEntry)
            .filter(specification::satisfiedBy), d -> documentAccessed(d, watch));
   }

   @Override
   @CheckReturnValue
   public Optional<DocumentEntry> saveDocument(Document document, boolean overwrite) {
      validate(document, "document", isNotNull()).orThrow();

      if (document.content() instanceof OutputStreamDocumentContent) {
         throw new IllegalArgumentException(
               "Unsupported operation for this document content implementation : " + document
                     .content()
                     .getClass()
                     .getName());
      }

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEntryEvent(() -> rawSaveDocument(document, overwrite).map(Document::documentEntry),
                                      d -> documentSaved(d, watch));
   }

   @Override
   @CheckReturnValue
   public Optional<Document> saveAndReturnDocument(Document document, boolean overwrite) {
      validate(document, "document", isNotNull()).orThrow();

      if (document.content() instanceof OutputStreamDocumentContent) {
         throw new IllegalArgumentException(
               "Unsupported operation for this document content implementation : " + document
                     .content()
                     .getClass()
                     .getName());
      }

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEvent(() -> rawSaveDocument(document, overwrite),
                                 d -> documentSaved(d.documentEntry(), watch));
   }

   private Optional<Document> rawSaveDocument(Document document, boolean overwrite) {
      Document documentToSave = simulateSave(document);
      Document savedDocument = documentStorage.merge(adaptCase(document.documentId()),
                                                     documentToSave,
                                                     (pv, nv) -> overwrite ? documentToSave : pv);

      return optionalPredicate(savedDocument, sd -> sd == documentToSave);
   }

   @Override
   @CheckReturnValue
   public Optional<DocumentEntry> deleteDocumentById(DocumentPath documentId) {
      validate(documentId, "documentId", isNotNull()).orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEntryEvent(() -> nullable(documentStorage.remove(adaptCase(documentId))).map(
            Document::documentEntry), d -> documentDeleted(d, watch));
   }

   @Override
   @CheckReturnValue
   public List<DocumentEntry> deleteDocumentsBySpecification(Path basePath,
                                                             Specification<DocumentEntry> specification) {
      validate(basePath, "basePath", isNotAbsolute().andValue(hasNoTraversal()))
            .and(validate(specification, "specification", isNotNull()))
            .orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEntriesEvent(() -> list(stream(documentStorage.values())
                                                         .map(Document::documentEntry)
                                                         .filter(specification::satisfiedBy)
                                                         .filter(documentEntry -> documentStorage.remove(
                                                               adaptCase(documentEntry.documentId()))
                                                                                  != null)),
                                        d -> documentDeleted(d, watch));
   }

   @Override
   public ReferencedDocument<? extends DocumentRepository> referencedDocumentId(DocumentUri documentUri) {
      return uriAdapter.referencedDocumentId(documentUri);
   }

   @Override
   public boolean supportsUri(RepositoryUri uri) {
      return uriAdapter.supportsUri(uri);
   }

   @Override
   public RepositoryUri toUri(ExportUriOptions options) {
      return uriAdapter.toUri(options);
   }

   @Override
   public DocumentUri toUri(DocumentPath documentId, ExportUriOptions options) {
      return uriAdapter.toUri(documentId, options);
   }

   @Override
   public Optional<Document> findDocumentByUri(DocumentUri documentUri) {
      return uriAdapter.findDocumentByUri(documentUri);
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryByUri(DocumentUri documentUri) {
      return uriAdapter.findDocumentEntryByUri(documentUri);
   }

   /**
    * Real memory URI adapter to be used by {@link DocumentTransformerUriAdapter}.
    */
   public class MemoryUriAdapter implements DocumentRepositoryUriAdapter {

      @Override
      public ReferencedDocument<? extends DocumentRepository> referencedDocumentId(DocumentUri documentUri) {
         Check.notNull(documentUri, "documentUri");
         var memoryDocumentUri = validateMemoryDocumentUri(documentUri);

         return ReferencedDocument.lazyLoad(MemoryDocumentRepository.this, memoryDocumentUri.documentId());
      }

      @Override
      public boolean supportsUri(RepositoryUri uri) {
         Check.notNull(uri, "uri");

         return supportsMemoryUri(uri);
      }

      @Override
      public RepositoryUri toUri(ExportUriOptions options) {
         Check.notNull(options, "options");

         return MemoryRepositoryUri.ofConfig(caseInsensitive).exportUri(options);
      }

      @Override
      public DocumentUri toUri(DocumentPath documentId, ExportUriOptions options) {
         Check.notNull(documentId, "documentId");
         Check.notNull(options, "options");

         return MemoryDocumentUri.ofConfig(caseInsensitive, documentId).exportUri(options);
      }

      @Override
      public Optional<Document> findDocumentByUri(DocumentUri documentUri) {
         Check.notNull(documentUri, "documentUri");
         var memoryDocumentUri = validateMemoryDocumentUri(documentUri);

         return findDocumentById(memoryDocumentUri.documentId());
      }

      @Override
      public Optional<DocumentEntry> findDocumentEntryByUri(DocumentUri documentUri) {
         Check.notNull(documentUri, "documentUri");
         var memoryDocumentUri = validateMemoryDocumentUri(documentUri);

         return findDocumentEntryById(memoryDocumentUri.documentId());
      }

      private boolean supportsMemoryUri(RepositoryUri repositoryUri) {
         try {
            return (MemoryRepositoryUri
                          .ofConfig(caseInsensitive)
                          .supportsUri(instanceOf(repositoryUri,
                                                  MemoryRepositoryUri.class).orElseGet(() -> MemoryRepositoryUri.ofRepositoryUri(
                                repositoryUri))));
         } catch (IncompatibleUriException __) {
            return false;
         }
      }

      private MemoryDocumentUri validateMemoryDocumentUri(DocumentUri documentUri) {
         try {
            var memoryUri = instanceOf(documentUri,
                                       MemoryDocumentUri.class).orElseGet(() -> MemoryDocumentUri.ofDocumentUri(
                  documentUri));

            if (!supportsMemoryUri(memoryUri)) {
               throw new InvalidUriException(documentUri).message("'documentUri=%s' must be supported",
                                                                  documentUri.toString());
            }

            return memoryUri;
         } catch (IncompatibleUriException e) {
            throw new InvalidUriException(documentUri).message("'documentUri=%s' must be supported: %s",
                                                               documentUri.toString(),
                                                               e.getMessage());
         }
      }

   }

   private DocumentPath adaptCase(DocumentPath documentId) {
      if (caseInsensitive) {
         return documentId.value(documentId.stringValue().toUpperCase().toLowerCase());
      } else {
         return documentId;
      }
   }

   /**
    * Simulates a physical save.
    *
    * @param document document to save
    *
    * @return document updated for save
    *
    * @implNote Content must be loaded into memory in the case it's a stream content. A stream must be
    *       consumed by a save operation, and safely closed after that without compromising the saved content
    */
   private Document simulateSave(Document document) {
      return DocumentBuilder
            .from(document)
            .metadata(DocumentMetadataBuilder
                            .from(document.metadata())
                            .chain((DocumentMetadataBuilder b) -> b.lastUpdateDate(ApplicationClock.nowAsInstant()))
                            .build())
            .build()
            .loadContent();
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", MemoryDocumentRepository.class.getSimpleName() + "[", "]")
            .add("caseInsensitive=" + caseInsensitive)
            .toString();
   }

   /**
    * Special {@link OutputStreamDocumentContent} that store data in a {@link ByteArrayOutputStream} and
    * provide access to internal byte array.
    */
   public static class MemoryOutputStreamDocumentContent extends OutputStreamDocumentContent {

      private final ByteArrayOutputStream outputStream;

      protected MemoryOutputStreamDocumentContent(ByteArrayOutputStream outputStream,
                                                  Charset contentEncoding) {
         super(new OutputStreamDocumentContentBuilder().content(outputStream, contentEncoding));
         this.outputStream = outputStream;
      }

      public static MemoryOutputStreamDocumentContent create(Charset contentEncoding) {
         return new MemoryOutputStreamDocumentContent(new ByteArrayOutputStream(), contentEncoding);
      }

      public ByteArrayOutputStream outputStream() {
         return outputStream;
      }

      @Override
      public byte[] content() {
         return outputStream.toByteArray();
      }
   }

}
