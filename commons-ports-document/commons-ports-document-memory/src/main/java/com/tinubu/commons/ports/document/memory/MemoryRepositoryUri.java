/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.memory;

import static com.tinubu.commons.lang.util.CheckedPredicate.alwaysTrue;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.PathUtils.isRoot;
import static com.tinubu.commons.lang.util.PathUtils.rootPath;
import static com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery.ExportUriFilter.isAlwaysTrue;
import static com.tinubu.commons.ports.document.domain.uri.UriUtils.exportUriFilter;
import static com.tinubu.commons.ports.document.memory.Constants.DEFAULT_CASE_INSENSITIVE;
import static com.tinubu.commons.ports.document.memory.MemoryRepositoryUri.Parameters.CASE_INSENSITIVE;
import static com.tinubu.commons.ports.document.memory.MemoryRepositoryUri.Parameters.allParameters;

import java.net.URI;
import java.util.List;

import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.domain.type.Id;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ddd2.uri.parts.SimplePath;
import com.tinubu.commons.lang.convert.ConversionFailedException;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.uri.AbstractRepositoryUri;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.ExportUriOptions;
import com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery;
import com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery.Parameter;
import com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery.ValueParameter;
import com.tinubu.commons.ports.document.domain.uri.RepositoryUri;

/**
 * Memory URI repository URI. This URI must be used to identify a repository, use
 * {@link MemoryDocumentUri} to identify a document.
 * <p>
 * Supported URI format :
 * <ul>
 *    <li>{@code memory:/[?<parameters>]}</li>
 *    <li>Commons-ports wrapped URI : {@code document:memory:<memory-uri>}</li>
 * </ul>
 * <p>
 * Supported URI parameters :
 * <ul>
 *    <li>{@code caseInsensitive} (Boolean) [false] : whether repository is case-insensitive for document search</li>
 * </ul>
 *
 * @see MemoryDocumentUri
 */
public class MemoryRepositoryUri extends AbstractRepositoryUri implements RepositoryUri {

   private static final String WRAPPED_URI_REPOSITORY_TYPE = "memory";

   protected final DocumentPath documentId;
   protected final boolean caseInsensitive;

   protected MemoryRepositoryUri(MemoryUri uri, boolean documentUri) {
      super(uri, documentUri);

      try {
         this.documentId = parseUri(documentUri);
         this.caseInsensitive = query().parameter(CASE_INSENSITIVE, alwaysTrue()).orElseThrow();
      } catch (ConversionFailedException e) {
         throw new InvalidUriException(uri, e).subMessage(e);
      }
   }

   @Override
   @SuppressWarnings("unchecked")
   protected Fields<? extends MemoryRepositoryUri> defineDomainFields() {
      return Fields
            .<MemoryRepositoryUri>builder()
            .superFields((Fields<MemoryRepositoryUri>) super.defineDomainFields())
            .build();
   }

   /**
    * Creates a repository URI from {@link Uri}.
    *
    * @param uri repository URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static MemoryRepositoryUri ofRepositoryUri(Uri uri) {
      Check.notNull(uri, "uri");

      validateUriCompatibility(uri, "uri", isCompatibleUriType(MemoryUri.class));

      return buildUri(() -> new MemoryRepositoryUri(memoryUri(uri), false));
   }

   /**
    * Creates a repository URI from {@link URI}.
    *
    * @param uri repository URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static MemoryRepositoryUri ofRepositoryUri(URI uri) {
      Check.notNull(uri, "uri");

      return ofRepositoryUri(Uri.ofUri(uri));
   }

   /**
    * Creates a repository URI from URI string.
    *
    * @param uri repository URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static MemoryRepositoryUri ofRepositoryUri(String uri) {
      Check.notNull(uri, "uri");

      return ofRepositoryUri(Uri.ofUri(uri));
   }

   /**
    * Creates a repository URI from configuration.
    *
    * @param caseInsensitive repository configuration
    *
    * @return new instance
    */
   public static MemoryRepositoryUri ofConfig(boolean caseInsensitive) {
      return buildUri(() -> new MemoryRepositoryUri(memoryUri(caseInsensitive, null), false));
   }

   /**
    * Creates a repository URI from {@link RepositoryUri}. Specified repository URI can can be a
    * {@link RepositoryUri} to represent a "repository" URI, or a {@link DocumentUri} to represent a
    * "document" URI.
    *
    * @param uri repository URI
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    * @implSpec this method is package-private because user code should always create
    *       {@link RepositoryUri} directly from a URI.
    */
   static MemoryRepositoryUri ofRepositoryUri(RepositoryUri uri) {
      Check.notNull(uri, "uri");

      validateUriCompatibility(uri, "uri", isCompatibleRepositoryUriType(MemoryRepositoryUri.class));

      return buildUri(() -> new MemoryRepositoryUri(memoryUri(uri), uri instanceof DocumentUri));
   }

   @Override
   public MemoryRepositoryUri normalize() {
      return buildUri(() -> new MemoryRepositoryUri(memoryUri().normalize(), documentUri));
   }

   @Override
   public MemoryRepositoryUri resolve(Uri uri) {
      return buildUri(() -> new MemoryRepositoryUri(memoryUri().resolve(uri), documentUri));
   }

   @Override
   public MemoryRepositoryUri resolve(URI uri) {
      return buildUri(() -> new MemoryRepositoryUri(memoryUri().resolve(uri), documentUri));
   }

   @Override
   public MemoryRepositoryUri relativize(Uri uri) {
      throw new UnsupportedOperationException();
   }

   @Override
   public MemoryRepositoryUri relativize(URI uri) {
      throw new UnsupportedOperationException();
   }

   @Override
   public int compareTo(Id o) {
      if (!(o instanceof MemoryRepositoryUri)) {
         return -1;
      } else if (o == this) {
         return 0;
      } else {
         return memoryUri().compareTo(((MemoryRepositoryUri) o).memoryUri());
      }
   }

   public MemoryUri memoryUri() {
      return (MemoryUri) uri;
   }

   public boolean caseInsensitive() {
      return this.caseInsensitive;
   }

   @Override
   public boolean supportsUri(RepositoryUri uri) {
      Check.notNull(uri, "uri");

      return supportsRepositoryUri(uri, MemoryRepositoryUri.class, MemoryRepositoryUri::ofRepositoryUri)
            .map(memoryUri -> {

               if (!memoryUri.caseInsensitive() == caseInsensitive()) {
                  return false;
               }

               return true;
            })
            .orElse(false);
   }

   @Override
   public URI exportURI(ExportUriOptions options) {
      Check.notNull(options, "options");

      var filter = exportUriFilter(options, isAlwaysTrue());
      var exportQuery = query().exportQuery(filter).noQueryIfEmpty(true);
      var exportUri = memoryUri().component().query(__ -> exportQuery);

      return exportUri.toURI();
   }

   @Override
   public RepositoryUri exportUri(ExportUriOptions options) {
      return RepositoryUri.ofRepositoryUri(exportURI(options));
   }

   /**
    * Creates a parameterized {@link MemoryUri} from specified {@link Uri}.
    * <p>
    * If specified URI is wrapped, it is unwrapped before creating parameterized URI.
    * Specified URI is normalized and checked for remaining traversals, after unwrapping.
    *
    * @param uri URI
    *
    * @return new parameterized URI, with all registered parameters
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   protected static MemoryUri memoryUri(Uri uri) {
      Check.notNull(uri, "uri");
      var unwrappedUri = unwrapUri(uri, WRAPPED_URI_REPOSITORY_TYPE);

      return parameterizedQueryUri(unwrappedUri, MemoryUri::ofUri, allParameters());
   }

   /**
    * Creates a parameterized {@link MemoryUri} from specified configuration.
    *
    * @param caseInsensitive repository configuration
    * @param documentId optional document id
    *
    * @return new parameterized URI, with all registered value parameters from configuration
    */
   protected static MemoryUri memoryUri(boolean caseInsensitive, DocumentPath documentId) {
      return MemoryUri.hierarchical(SimplePath.of((documentId != null
                                                   ? rootPath().resolve(documentId.value())
                                                   : rootPath())),
                                    ParameterizedQuery
                                          .ofEmpty()
                                          .noQueryIfEmpty(true)
                                          .registerParameters(Parameters.valueParameters(caseInsensitive)));
   }

   /**
    * Checks URI syntax and parses current parameterized URI to separate the document path.
    *
    * @param documentUri whether the current parameterized URI is a document URI, identifying a
    *       document instead of a repository
    *
    * @return parsed document path
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    * @implSpec The minimum set of parameters are checked here for consistency, only the parameters
    *       used in class internal state. Other parameters could be checked later.
    */
   protected DocumentPath parseUri(boolean documentUri) {
      var uriPath = memoryUri().toPath();

      DocumentPath documentId;

      if (!documentUri) {
         if (!isRoot(uriPath)) {
            throw new InvalidUriException(memoryUri()).subMessage("Invalid path, must be /");
         }
         documentId = null;
      } else {
         if (isRoot(uriPath)) {
            throw new InvalidUriException(memoryUri()).subMessage("Missing document id");
         }
         documentId = DocumentPath.of(uriPath);
      }

      return documentId;
   }

   /**
    * Supported parameter registry.
    */
   static class Parameters {
      static final Parameter<Boolean> CASE_INSENSITIVE =
            Parameter.registered("caseInsensitive", Boolean.class).defaultValue(DEFAULT_CASE_INSENSITIVE);

      @SuppressWarnings("rawtypes")
      static List<Parameter> allParameters() {
         return list(CASE_INSENSITIVE);
      }

      @SuppressWarnings("rawtypes")
      static List<ValueParameter> valueParameters(boolean caseInsensitive) {
         return list(ValueParameter.ofValue(CASE_INSENSITIVE, caseInsensitive));
      }

   }

}
