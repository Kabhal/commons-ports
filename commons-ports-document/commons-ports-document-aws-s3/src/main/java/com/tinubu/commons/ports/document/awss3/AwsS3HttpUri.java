/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.tinubu.commons.ports.document.awss3;

import static com.tinubu.commons.ddd2.invariant.Validate.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd2.invariant.rules.EqualsRules.isIn;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.ddd2.invariant.rules.domain.ids.UriRules.hasNoUserInfo;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.ports.document.awss3.AwsS3Uri.AwsS3UriStyle.PATH;
import static com.tinubu.commons.ports.document.awss3.AwsS3Uri.AwsS3UriStyle.VIRTUAL_HOST;

import java.net.URI;

import com.tinubu.commons.ddd2.invariant.Invariant;
import com.tinubu.commons.ddd2.invariant.Invariants;
import com.tinubu.commons.ddd2.invariant.ParameterValue;
import com.tinubu.commons.ddd2.invariant.Validate;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ddd2.uri.UriRestrictions;
import com.tinubu.commons.ddd2.uri.parts.SimpleHost;
import com.tinubu.commons.ddd2.uri.parts.SimplePath;
import com.tinubu.commons.ddd2.uri.parts.SimpleServerAuthority;
import com.tinubu.commons.ddd2.uri.uris.HttpUri;
import com.tinubu.commons.ddd2.uri.uris.HttpUri.HttpUriRestrictions;

/**
 * AWS S3 HTTP {@link AwsS3Uri}.
 * <p>
 * Supported URI formats :
 * <ul>
 * <li>Using S3 parameters :
 * <ul>
 *    <li>Virtual-host style : {@code http[s]://<bucket>.s3[.<region>].amazonaws.com[/<key>][?<query>][#<fragment>]}</li>
 *    <li>Path style : {@code http[s]://s3[.<region>].amazonaws.com/<bucket>[/<key>][?<query>][#<fragment>]}</li>
 * </ul>
 * </li>
 * <li>Using URI parameters : {@code http[s]://<host>[</path>][?<query>][#<fragment>]}</li>
 * </ul>
 * {@code path} can be empty.
 * <p>
 * However, supported format depends on {@link HttpUriRestrictions} restrictions. By default,
 * <ul>
 *    <li>{@link HttpUriRestrictions#http() http} : HTTP protocol is not supported</li>
 *    <li>{@link HttpUriRestrictions#https() https} : HTTPS protocol is supported</li>
 *    <li>{@link HttpUriRestrictions#portRestriction() portRestriction} : no port restriction</li>
 *    <li>{@link HttpUriRestrictions#portMapper() portMapper} : default HTTP port = 80 / HTTPS port = 443</li>
 *    <li>{@link HttpUriRestrictions#query() query} : query is not supported</li>
 *    <li>{@link HttpUriRestrictions#fragment() fragment} : fragment is not supported</li>
 * </ul>
 */
public class AwsS3HttpUri extends AbstractAwsS3Uri<AwsS3HttpUri> {

   protected static final String HTTP_SCHEME = "http";
   protected static final String HTTPS_SCHEME = "https";

   protected AwsS3HttpUri(URI uri,
                          UriRestrictions restrictions,
                          Scheme scheme,
                          SchemeSpecific schemeSpecific,
                          Authority authority,
                          Path path,
                          Query query,
                          Fragment fragment,
                          boolean newObject) {
      super(uri, restrictions, scheme, schemeSpecific, authority, path, query, fragment, newObject);
   }

   protected AwsS3HttpUri(HttpUri uri) {
      this(Check.notNull(uri, "uri").toURI(),
           uri.restrictions(),
           uri.component().scheme().orElse(null),
           uri.component().schemeSpecific().orElse(null),
           uri.component().authority().orElse(null),
           uri.component().path().orElse(null),
           uri.component().query().orElse(null), uri.component().fragment().orElse(null), false);
   }

   /**
    * @implNote restrictions are applied to uri to validate them directly at uri level.
    */
   protected AwsS3HttpUri(HttpUri uri, UriRestrictions restrictions) {
      this(applyRestrictions(Check.notNull(uri, "uri"), Check.notNull(restrictions, "restrictions")));
   }

   protected AwsS3HttpUri(Uri uri, UriRestrictions restrictions) {
      this(HttpUri.ofUri(Check.notNull(uri, "uri")), restrictions);
   }

   @Override
   public Invariants domainInvariants() {
      return super
            .domainInvariants()
            .withInvariants(Invariants.of(Invariant.of(() -> this, hasNoUserInfo())));
   }

   public static AwsS3HttpUri ofUri(URI uri, UriRestrictions restrictions) {
      return buildUri(() -> new AwsS3HttpUri(HttpUri.ofUri(uri), restrictions));
   }

   public static AwsS3HttpUri ofUri(URI uri) {
      return ofUri(uri, defaultRestrictions());
   }

   public static AwsS3HttpUri ofUri(Uri uri, UriRestrictions restrictions) {
      return buildUri(() -> new AwsS3HttpUri(uri, restrictions));
   }

   public static AwsS3HttpUri ofUri(Uri uri) {
      return ofUri(uri, defaultRestrictions());
   }

   public static AwsS3HttpUri ofUri(String uri, UriRestrictions restrictions) {
      return ofUri(Uri.uri(uri), restrictions);
   }

   public static AwsS3HttpUri ofUri(String uri) {
      return ofUri(uri, defaultRestrictions());
   }

   @SuppressWarnings("unchecked")
   protected static <T extends HttpUri> T applyRestrictions(T uri, UriRestrictions restrictions) {
      if (uri.restrictions() != restrictions) {
         return (T) uri.restrictions(restrictions);
      } else {
         return uri;
      }
   }

   /**
    * Creates new {@link AwsS3HttpUri}.
    *
    * @param style URI style (VIRTUAL_HOST_STYLE | PATH_STYLE)
    * @param region optional region name, or {@code null}
    * @param bucket bucket name
    * @param key optional key, or {@code null}
    * @param query optional query, or {@code null}
    * @param secured whether to use a secured protocol (HTTPS)
    *
    * @return new instance
    */
   public static AwsS3HttpUri awsS3Uri(AwsS3UriStyle style,
                                       String region,
                                       String bucket,
                                       String key,
                                       Query query,
                                       boolean secured) {
      validate(style, "style", isIn(ParameterValue.value(list(VIRTUAL_HOST, PATH))))
            .and(validate(region, "region", isNull().orValue(isNotBlank())))
            .and(Validate.notBlank(bucket, "bucket"))
            .orThrow();

      var uriRegion = nullable(region).filter(r -> !r.equals(S3Utils.defaultRegion().id()));

      Host host;
      Path path;
      switch (style) {
         case VIRTUAL_HOST:
            host = SimpleHost.of(bucket + ".s3" + uriRegion.map(r -> "." + r).orElse("") + ".amazonaws.com");
            path = SimplePath.of(nullable(key).filter(k -> !k.isEmpty()).map(k -> "/" + k).orElse(""));
            break;
         case PATH:
            host = SimpleHost.of("s3" + uriRegion.map(r -> "." + r).orElse("") + ".amazonaws.com");
            path = SimplePath.of("/" + bucket + (key != null && !key.isEmpty() ? "/" + key : ""));
            break;
         default:
            throw new IllegalStateException();
      }

      return buildUri(() -> new AwsS3HttpUri(HttpUri.hierarchical(secured,
                                                                  SimpleServerAuthority.of(host),
                                                                  path,
                                                                  query)));
   }

   /**
    * Creates new {@link AwsS3HttpUri} using secured HTTPS protocol.
    *
    * @param style URI style (VIRTUAL_HOST_STYLE | PATH_STYLE)
    * @param region optional region name, or {@code null}
    * @param bucket bucket name
    * @param key optional key, or {@code null}
    * @param query optional query, or {@code null}
    *
    * @return new instance
    */
   public static AwsS3HttpUri awsS3Uri(AwsS3UriStyle style,
                                       String region,
                                       String bucket,
                                       String key,
                                       Query query) {
      return awsS3Uri(style, region, bucket, key, query, true);
   }

   @Override
   protected AwsS3HttpUri recreate(URI uri,
                                   UriRestrictions restrictions,
                                   Scheme scheme,
                                   SchemeSpecific schemeSpecific,
                                   Authority authority,
                                   Path path,
                                   Query query,
                                   Fragment fragment,
                                   boolean newObject) {
      return buildUri(() -> new AwsS3HttpUri(uri,
                                             restrictions,
                                             scheme,
                                             schemeSpecific,
                                             authority,
                                             path,
                                             query,
                                             fragment,
                                             newObject));
   }

   @Override
   public AwsS3UriStyle style() {
      return s3Uri.isPathStyle() ? PATH : VIRTUAL_HOST;
   }

   @Override
   public boolean secured() {
      return !protocol().equalsIgnoreCase(HTTP_SCHEME);
   }

}
