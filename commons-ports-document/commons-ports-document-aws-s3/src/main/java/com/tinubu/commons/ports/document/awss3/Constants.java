/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.awss3;

import static com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig.S3UriFormat.VIRTUAL_HOST_STYLE;

import com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig.S3ListingContentMode;
import com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig.S3UriFormat;

public final class Constants {

   private Constants() { }

   public static final boolean DEFAULT_CREATE_IF_MISSING_BUCKET = false;
   public static final boolean DEFAULT_FAIL_FAST_IF_MISSING_BUCKET = true;

   /** Absolute maximum number of keys that can be retrieved in a single call when listing objects. */
   public static final int MAX_LISTING_MAX_KEYS = 1000;

   /** Default number of keys that can be retrieved in a single call when listing objects. */
   public static final int DEFAULT_LISTING_MAX_KEYS = MAX_LISTING_MAX_KEYS;

   /** Minimum supported part upload chunk size value. */
   public static final int MIN_UPLOAD_CHUNK_SIZE = 5 * 1024 * 1024;

   /** Default part upload chunk size value in bytes. */
   public static final int DEFAULT_UPLOAD_CHUNK_SIZE = MIN_UPLOAD_CHUNK_SIZE;

   /** Default URI format for URI generation. */
   public static final S3UriFormat DEFAULT_URI_GENERATION_FORMAT = VIRTUAL_HOST_STYLE;

   public static final S3ListingContentMode DEFAULT_LISTING_CONTENT_MODE = S3ListingContentMode.FAST;
   public static final boolean DEFAULT_LOAD_ALL_TAGS = false;
   /** Whether to use path-style S3 URI by default. Should be false as path-style URI are deprecated at AWS. */
   public static final boolean DEFAULT_PATH_STYLE_ACCESS = false;

}
