/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.tinubu.commons.ports.document.awss3;

import java.net.URI;
import java.util.Optional;

import com.tinubu.commons.ddd2.uri.ComponentUri;
import com.tinubu.commons.ddd2.uri.Uri;

public interface AwsS3Uri extends ComponentUri {

   /**
    * Creates new {@link AwsS3Uri} using specified style.
    *
    * @param style new style
    * @param secured whether to use a secured protocol, if possible, depending on style
    *
    * @return new instance
    */
   AwsS3Uri toStyle(AwsS3UriStyle style, boolean secured);

   /**
    * Creates new {@link AwsS3Uri} using specified style, preserving {@link #secured()} state.
    *
    * @param style new style
    *
    * @return new instance
    */
   default AwsS3Uri toStyle(AwsS3UriStyle style) {
      return toStyle(style, secured());
   }

   /**
    * URI protocol, always lower-cased.
    *
    * @return URI protocol
    */
   String protocol();

   /**
    * Whether protocol is considered as "secure".
    * By convention, {@code s3} and {@code https} procotols are considered as secure.
    */
   boolean secured();

   AwsS3UriStyle style();

   String bucket();

   Optional<String> region();

   Optional<String> key();

   /**
    * Returns key as {@link Path}.
    * <p>
    * Caution : S3 keys can contain arbitrary {@code /}, hence be warned that {@link Path} implementation will
    * automatically normalize double-slashes or ending slashes.
    *
    * @return key
    */
   Optional<java.nio.file.Path> pathKey();

   @Override
   AwsS3Uri normalize();

   @Override
   AwsS3Uri resolve(Uri uri);

   @Override
   AwsS3Uri resolve(URI uri);

   @Override
   AwsS3Uri relativize(Uri uri);

   @Override
   AwsS3Uri relativize(URI uri);

   enum AwsS3UriStyle {
      /**
       * CLI style : {@code s3://<bucket>[/<key>]}.
       */
      S3,
      /**
       * HTTP Virtual-host style : {@code https://<bucket>.s3[.<region>].amazonaws.com[/<key>]}.
       */
      VIRTUAL_HOST,
      /**
       * HTTP Path style : {@code https://s3[.<region>].amazonaws.com/<bucket>[/<key>]}.
       */
      PATH
   }

}
