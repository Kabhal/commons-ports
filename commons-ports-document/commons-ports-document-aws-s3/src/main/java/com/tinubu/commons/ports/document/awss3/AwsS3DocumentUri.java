/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.awss3;

import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotAbsolute;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.ports.document.awss3.AwsS3RepositoryUri.Parameters.BUCKET_BASE_PATH;

import java.net.URI;
import java.nio.file.Path;

import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.domain.type.Id;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ddd2.uri.uris.HttpUri;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.ExportUriOptions;

/**
 * AWS S3 URI document URI. This URI must be used to identify a document, use {@link AwsS3RepositoryUri} to
 * identify a repository.
 * <p>
 * You can use this class when a {@link AwsS3RepositoryUri} is required, but your current URI is a document
 * URI. In this case it's important that the bucket base path is set, or it will be set to {@code ""} by
 * default.
 * Bucket base path is read from {@code bucketBasePath} URI query string parameter, or from specified
 * {@code defaultBucketBasePath} parameter.
 * <p>
 * Supported URI format :
 * <ul>
 *    <li>S3 Cli mode : {@code s3://<bucket>[/<bucket-base-path>]/<document-id>[/?<parameters>]}</li>
 *    <li>Virtual-hosted style HTTP mode : {@code http[s]://<bucket>.s3[.<region>].amazonaws.com[/<bucket-base-path>]/<document-id>[/?<parameters>]}</li>
 *    <li>Path style HTTP mode : {@code http[s]://s3[.<region>].amazonaws.com/<bucket>[/<bucket-base-path>]/<document-id>[/?<parameters>]}</li>
 *    <li>Commons-ports wrapped URI : {@code doc[ument]:s3:<aws-s3-uri>}</li>
 * </ul>
 *
 * @see AwsS3RepositoryUri
 * @see AwsS3DocumentConfig
 */
public class AwsS3DocumentUri extends AwsS3RepositoryUri implements DocumentUri {

   protected AwsS3DocumentUri(AwsS3Uri uri) {
      super(uri, true);
   }

   @Override
   @SuppressWarnings("unchecked")
   protected Fields<? extends AwsS3DocumentUri> defineDomainFields() {
      return Fields
            .<AwsS3DocumentUri>builder()
            .superFields((Fields<AwsS3DocumentUri>) super.defineDomainFields())
            .field("documentId", v -> v.documentId, isNotNull())
            .build();
   }

   /**
    * Creates a document URI from {@link Uri}.
    *
    * @param uri document URI
    * @param defaultBucketBasePath default bucket base path to use if {@code bucketBasePath} query
    *       parameter not present
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI has invalid syntax
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static AwsS3DocumentUri ofDocumentUri(Uri uri, Path defaultBucketBasePath) {
      Check.notNull(uri, "uri");
      var normalizedDefaultBucketBasePath = Check.validate(nullable(defaultBucketBasePath, Path::normalize),
                                                           "defaultBucketBasePath",
                                                           isNotAbsolute().andValue(hasNoTraversal()));

      return buildUri(() -> new AwsS3DocumentUri(mapQuery(awsS3Uri(uri), (u, q) -> {
         q.registerParameter(BUCKET_BASE_PATH.defaultValue(normalizedDefaultBucketBasePath));
         return u;
      })));
   }

   /**
    * Creates a document URI from {@link Uri}.
    *
    * @param uri document URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI has invalid syntax
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static AwsS3DocumentUri ofDocumentUri(Uri uri) {
      Check.notNull(uri, "uri");
      validateUriCompatibility(uri, "uri", isCompatibleUriType(AwsS3Uri.class, HttpUri.class));

      return ofDocumentUri(uri, defaultBucketBasePath(uri, true));
   }

   /**
    * Creates a document URI from {@link URI}.
    *
    * @param uri document URI
    * @param defaultBucketBasePath default bucket base path to use if {@code bucketBasePath} query
    *       parameter not present
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI has invalid syntax
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static AwsS3DocumentUri ofDocumentUri(URI uri, Path defaultBucketBasePath) {
      Check.notNull(uri, "uri");
      Check.notNull(defaultBucketBasePath, "defaultBucketBasePath");

      return ofDocumentUri(Uri.ofUri(uri), defaultBucketBasePath);
   }

   /**
    * Creates a document URI from {@link URI}.
    * <p>
    * Default bucket base path is set to {@code ""}.
    *
    * @param uri document URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI has invalid syntax
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static AwsS3DocumentUri ofDocumentUri(URI uri) {
      Check.notNull(uri, "uri");

      return ofDocumentUri(Uri.ofUri(uri));
   }

   /**
    * Creates a document URI from URI string.
    *
    * @param uri document URI
    * @param defaultBucketBasePath default bucket base path to use if {@code bucketBasePath} query
    *       parameter not present
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI has invalid syntax
    */
   public static AwsS3DocumentUri ofDocumentUri(String uri, Path defaultBucketBasePath) {
      Check.notNull(uri, "uri");
      Check.notNull(defaultBucketBasePath, "defaultBucketBasePath");

      return ofDocumentUri(Uri.uri(uri), defaultBucketBasePath);
   }

   /**
    * Creates a document URI from URI string.
    * <p>
    * Default bucket base path is set to {@code ""}.
    *
    * @param uri document URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI has invalid syntax
    */
   public static AwsS3DocumentUri ofDocumentUri(String uri) {
      Check.notNull(uri, "uri");

      return ofDocumentUri(Uri.uri(uri));
   }

   /**
    * Creates a document URI from configuration.
    *
    * @param config repository configuration
    * @param documentId document id
    *
    * @return new instance
    */
   public static AwsS3DocumentUri ofConfig(AwsS3DocumentConfig config, DocumentPath documentId) {
      Check.notNull(config, "config");

      return buildUri(() -> new AwsS3DocumentUri(awsS3Uri(config, documentId)));
   }

   /**
    * Creates a document URI from {@link DocumentUri}.
    *
    * @param uri document URI
    * @param defaultBucketBasePath default bucket base path to use if {@code bucketBasePath} query
    *       parameter not present
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    * @implSpec this method is package-private because user code should always create
    *       {@link DocumentUri} directly from a URI.
    */
   static AwsS3DocumentUri ofDocumentUri(DocumentUri uri, Path defaultBucketBasePath) {
      Check.notNull(uri, "uri");
      var normalizedDefaultBucketBasePath = Check.validate(nullable(defaultBucketBasePath, Path::normalize),
                                                           "defaultBucketBasePath",
                                                           isNotAbsolute().andValue(hasNoTraversal()));

      validateUriCompatibility(uri, "uri", isCompatibleDocumentUriType(AwsS3DocumentUri.class));

      if (uri instanceof AwsS3DocumentUri) {
         return buildUri(() -> new AwsS3DocumentUri(mapQuery(awsS3Uri(uri), (u, q) -> {
            q.registerParameter(BUCKET_BASE_PATH.defaultValue(((AwsS3DocumentUri) uri).bucketBasePath()));
            return u;
         })));
      } else {
         return buildUri(() -> new AwsS3DocumentUri(mapQuery(awsS3Uri(uri), (u, q) -> {
            q.registerParameter(BUCKET_BASE_PATH.defaultValue(normalizedDefaultBucketBasePath));
            return u;
         })));
      }
   }

   /**
    * Creates a document URI from {@link DocumentUri}.
    * <p>
    * Default bucket base path is set to {@code ""}.
    *
    * @param uri document URI
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    * @implSpec this method is package-private because user code should always create
    *       {@link DocumentUri} directly from a URI.
    */
   static AwsS3DocumentUri ofDocumentUri(DocumentUri uri) {
      return ofDocumentUri(uri, defaultBucketBasePath(uri, true));
   }

   public DocumentPath documentId() {
      return documentId;
   }

   @Override
   public DocumentUri exportUri(ExportUriOptions options) {
      return DocumentUri.ofDocumentUri(super.exportURI(options));
   }

   @Override
   public AwsS3DocumentUri normalize() {
      return buildUri(() -> new AwsS3DocumentUri(awsS3Uri().normalize()));
   }

   @Override
   public AwsS3DocumentUri resolve(Uri uri) {
      return buildUri(() -> new AwsS3DocumentUri(awsS3Uri().resolve(uri)));
   }

   @Override
   public AwsS3DocumentUri resolve(URI uri) {
      return buildUri(() -> new AwsS3DocumentUri(awsS3Uri().resolve(uri)));
   }

   @Override
   public AwsS3DocumentUri relativize(Uri uri) {
      throw new UnsupportedOperationException();
   }

   @Override
   public AwsS3DocumentUri relativize(URI uri) {
      throw new UnsupportedOperationException();
   }

   @Override
   public int compareTo(Id o) {
      if (!(o instanceof AwsS3DocumentUri)) {
         return -1;
      } else {
         return super.compareTo(o);
      }
   }

}
