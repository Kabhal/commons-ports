/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.awss3;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static java.util.stream.Collectors.toMap;

import java.nio.file.Path;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata;

import software.amazon.awssdk.core.exception.SdkException;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.S3Utilities;
import software.amazon.awssdk.services.s3.model.BucketAlreadyExistsException;
import software.amazon.awssdk.services.s3.model.BucketAlreadyOwnedByYouException;
import software.amazon.awssdk.services.s3.model.CreateBucketRequest;
import software.amazon.awssdk.services.s3.model.HeadBucketRequest;
import software.amazon.awssdk.services.s3.model.NoSuchBucketException;
import software.amazon.awssdk.services.s3.model.Tag;
import software.amazon.awssdk.services.s3.model.Tagging;

public final class S3Utils {
   private static final Logger log = LoggerFactory.getLogger(S3Utils.class);

   private static final Pattern CONTENT_DISPOSITION_PATTERN =
         Pattern.compile("Content-Disposition:\\s*attachment;\\s*filename=\"([^\"]+)\"");
   /**
    * Default S3 region as current AWS implementation (Do not change this constant while S3 does not change
    * its specification).
    */
   public static final Region DEFAULT_REGION = Region.US_EAST_1;

   /** Pre-created S3Utilities. */
   private static final S3Utilities DEFAULT_S3_UTILITIES =
         S3Utilities.builder().region(defaultRegion()).build();

   private S3Utils() { }

   public static Region defaultRegion() {
      return DEFAULT_REGION;
   }

   public static S3Utilities defaultS3Utilities() {
      return DEFAULT_S3_UTILITIES;
   }

   public static void tryCreateBucket(S3Client client, String bucket, boolean createIfMissingBucket) {
      if (createIfMissingBucket) {
         try {
            client.createBucket(CreateBucketRequest.builder().bucket(bucket).build());
            log.debug("Created missing '{}' bucket", bucket);
         } catch (BucketAlreadyExistsException | BucketAlreadyOwnedByYouException e) {
            /* Does nothing. */
         } catch (SdkException e) {
            throw new DocumentAccessException(e.getMessage(), e);
         }
      } else {
         try {
            client.headBucket(HeadBucketRequest.builder().bucket(bucket).build());
         } catch (NoSuchBucketException e) {
            throw new DocumentAccessException(String.format("Can't create '%s' missing bucket", bucket), e);
         } catch (SdkException e) {
            throw new DocumentAccessException(e.getMessage(), e);
         }
      }
   }

   public static Map<String, String> objectMetadata(OpenDocumentMetadata metadata,
                                                    List<Pattern> includes,
                                                    List<Pattern> excludes) {
      return map(LinkedHashMap::new,
                 metadata
                       .attributes()
                       .entrySet()
                       .stream()
                       .filter(e -> excludes == null || !matchKey(excludes, e.getKey()))
                       .filter(e -> includes == null || matchKey(includes, e.getKey()))
                       .collect(toMap(Entry::getKey, Entry::getValue)));
   }

   public static Tagging objectTags(OpenDocumentMetadata metadata,
                                    List<Pattern> includes,
                                    List<Pattern> excludes) {
      return Tagging
            .builder()
            .tagSet(list(metadata
                               .attributes()
                               .entrySet()
                               .stream()
                               .filter(e -> excludes == null || !matchKey(excludes, e.getKey()))
                               .filter(e -> includes == null || matchKey(includes, e.getKey()))
                               .map(e -> Tag.builder().key(e.getKey()).value(e.getValue()).build())))
            .build();
   }

   private static boolean matchKey(List<Pattern> patterns, String key) {
      return patterns.stream().anyMatch(pattern -> pattern.matcher(key).matches());
   }

   public static String contentDisposition(OpenDocumentMetadata metadata) {
      return metadata
            .documentPath()
            .map(documentPath -> "Content-Disposition: attachment; filename=\""
                                 + documentPath.toString()
                                 + "\"")
            .orElse("Content-Disposition: attachment");
   }

   /**
    * Extract filename from a Content-Disposition header.
    *
    * @param contentDisposition Content-Disposition header
    *
    * @return filename path
    */
   public static Optional<Path> documentPath(String contentDisposition) {
      if (contentDisposition == null) {
         return optional();
      }

      Matcher matcher = CONTENT_DISPOSITION_PATTERN.matcher(contentDisposition);
      if (matcher.matches()) {
         return optional(Path.of(matcher.group(1)));
      } else {
         return optional();
      }
   }

}
