/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.tinubu.commons.ports.document.awss3;

import static com.tinubu.commons.ddd2.domain.type.support.DomainObjectSupport.hideField;
import static com.tinubu.commons.ddd2.invariant.Validate.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.as;
import static com.tinubu.commons.ddd2.invariant.rules.OptionalRules.requiresValue;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.ports.document.awss3.S3Utils.defaultS3Utilities;

import java.net.URI;
import java.util.Optional;

import com.tinubu.commons.ddd2.domain.type.DomainObject;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.uri.AbstractComponentUri;
import com.tinubu.commons.ddd2.uri.ComponentUri;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ddd2.uri.UriRestrictions;
import com.tinubu.commons.ddd2.uri.uris.HttpUri.HttpUriRestrictions;

import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Uri;

public abstract class AbstractAwsS3Uri<U extends AbstractAwsS3Uri<U>> extends AbstractComponentUri<U>
      implements AwsS3Uri {

   protected S3Uri s3Uri;

   protected AbstractAwsS3Uri(URI uri,
                              UriRestrictions restrictions,
                              Scheme scheme,
                              SchemeSpecific schemeSpecific,
                              Authority authority,
                              Path path,
                              Query query,
                              Fragment fragment,
                              boolean newObject) {
      super(uri, restrictions, scheme, schemeSpecific, authority, path, query, fragment, newObject);
   }

   protected AbstractAwsS3Uri(URI uri, UriRestrictions restrictions) {
      super(uri, restrictions);
   }

   protected AbstractAwsS3Uri(UriRestrictions restrictions,
                              Scheme scheme,
                              SchemeSpecific schemeSpecific,
                              Authority authority,
                              Path path, Query query, Fragment fragment) {
      super(restrictions, scheme, schemeSpecific, authority, path, query, fragment);
   }

   protected AbstractAwsS3Uri(ComponentUri uri, UriRestrictions restrictions) {
      super(uri, restrictions);
   }

   protected AbstractAwsS3Uri(Uri uri, UriRestrictions restrictions) {
      super(uri, restrictions);
   }

   @Override
   @SuppressWarnings("unchecked")
   public <T extends DomainObject> T postValidate() {
      this.s3Uri =
            validate(s3Uri(uri), as(S3Uri::bucket, __ -> "bucket", requiresValue(isNotBlank()))).orThrow(
                  uriInvariantResultHandler(this));

      return (T) this;
   }

   @Override
   @SuppressWarnings("unchecked")
   protected Fields<? extends U> defineDomainFields() {
      return Fields.<U>builder().superFields((Fields<? super U>) super.defineDomainFields())
            .technicalField("s3Uri", v -> v.s3Uri, hideField())
            .build();
   }

   @Override
   public AwsS3Uri toStyle(AwsS3UriStyle style, boolean secured) {
      Check.notNull(style, "style");

      return AwsS3UriFactory
            .instance()
            .awsS3Uri(style,
                      region().orElse(null),
                      bucket(),
                      key().orElse(null),
                      component().query().orElse(null),
                      secured);
   }

   protected static UriRestrictions defaultRestrictions() {
      return HttpUriRestrictions.ofRestrictions(true, false, true, true);
   }

   @Override
   public U relativize(Uri uri) {
      throw new UnsupportedOperationException();
   }

   @Override
   public U relativize(URI uri) {
      throw new UnsupportedOperationException();
   }

   @Override
   public String protocol() {
      return scheme().orElseThrow().toLowerCase();
   }

   @Override
   public String bucket() {
      return s3Uri.bucket().orElseThrow();
   }

   @Override
   public Optional<String> region() {
      return s3Uri.region().map(Region::id);
   }

   @Override
   public Optional<String> key() {
      return s3Uri.key();
   }

   @Override
   public Optional<java.nio.file.Path> pathKey() {
      return key().map(java.nio.file.Path::of);
   }

   protected static S3Uri s3Uri(URI uri) {
      try {
         return defaultS3Utilities().parseUri(uri);
      } catch (Exception e) {
         throw new IncompatibleUriException(uri, e).subMessage(e);
      }
   }

}
