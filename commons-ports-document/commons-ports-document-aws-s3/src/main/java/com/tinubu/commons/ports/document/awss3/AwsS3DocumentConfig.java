/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.awss3;

import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.hasNoNullElements;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isGreaterThanOrEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isLessThanOrEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.EqualsRules.isIn;
import static com.tinubu.commons.ddd2.invariant.rules.NumberRules.isStrictlyPositive;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotAbsolute;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.ddd2.invariant.rules.UriRules.scheme;
import static com.tinubu.commons.lang.util.CollectionUtils.immutable;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.ports.document.awss3.Constants.DEFAULT_CREATE_IF_MISSING_BUCKET;
import static com.tinubu.commons.ports.document.awss3.Constants.DEFAULT_FAIL_FAST_IF_MISSING_BUCKET;
import static com.tinubu.commons.ports.document.awss3.Constants.DEFAULT_LISTING_CONTENT_MODE;
import static com.tinubu.commons.ports.document.awss3.Constants.DEFAULT_LISTING_MAX_KEYS;
import static com.tinubu.commons.ports.document.awss3.Constants.DEFAULT_LOAD_ALL_TAGS;
import static com.tinubu.commons.ports.document.awss3.Constants.DEFAULT_PATH_STYLE_ACCESS;
import static com.tinubu.commons.ports.document.awss3.Constants.DEFAULT_UPLOAD_CHUNK_SIZE;
import static com.tinubu.commons.ports.document.awss3.Constants.DEFAULT_URI_GENERATION_FORMAT;
import static com.tinubu.commons.ports.document.awss3.Constants.MAX_LISTING_MAX_KEYS;
import static com.tinubu.commons.ports.document.awss3.Constants.MIN_UPLOAD_CHUNK_SIZE;
import static com.tinubu.commons.ports.document.awss3.S3Utils.defaultRegion;

import java.net.URI;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.beans.Setter;
import com.tinubu.commons.ports.document.domain.DocumentEntry;

import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.auth.credentials.DefaultCredentialsProvider;

/**
 * Azure Blob document repository configuration.
 */
public class AwsS3DocumentConfig extends AbstractValue {

   private final String region;
   private final URI endpoint;
   private final String bucket;
   private final Path bucketBasePath;
   private final boolean createIfMissingBucket;
   private final boolean failFastIfMissingBucket;
   private final boolean pathStyleAccess;
   private final S3UriFormat uriGenerationFormat;
   private final int listingMaxKeys;
   private final S3ListingContentMode listingContentMode;
   private final int uploadChunkSize;
   private final AwsCredentialsProvider credentialsProvider;
   private final List<Pattern> matchTags;
   private final boolean loadAllTags;
   private final HttpClientConfig httpClientConfig;

   private AwsS3DocumentConfig(AwsS3DocumentConfigBuilder builder) {
      this.region = nullable(builder.region, defaultRegion().id());
      this.endpoint = builder.endpoint;
      this.bucket = builder.bucket;
      this.bucketBasePath = nullable(builder.bucketBasePath, Path.of(""));
      this.createIfMissingBucket = nullable(builder.createIfMissingBucket, DEFAULT_CREATE_IF_MISSING_BUCKET);
      this.failFastIfMissingBucket =
            nullable(builder.failFastIfMissingBucket, DEFAULT_FAIL_FAST_IF_MISSING_BUCKET);
      this.pathStyleAccess = nullable(builder.pathStyleAccess, DEFAULT_PATH_STYLE_ACCESS);
      this.uriGenerationFormat = nullable(builder.uriGenerationFormat, DEFAULT_URI_GENERATION_FORMAT);
      this.listingMaxKeys = nullable(builder.listingMaxKeys, DEFAULT_LISTING_MAX_KEYS);
      this.listingContentMode = nullable(builder.listingContentMode, DEFAULT_LISTING_CONTENT_MODE);
      this.uploadChunkSize = nullable(builder.uploadChunkSize, DEFAULT_UPLOAD_CHUNK_SIZE);
      this.credentialsProvider = nullable(builder.credentialsProvider, defaultCredentialsProvider());
      this.matchTags = immutable(nullable(builder.matchTags, list()));
      this.loadAllTags = nullable(builder.loadAllTags, DEFAULT_LOAD_ALL_TAGS);
      this.httpClientConfig = builder.httpClientConfig;
   }

   @Override
   public Fields<? extends AwsS3DocumentConfig> defineDomainFields() {
      return Fields
            .<AwsS3DocumentConfig>builder()
            .field("region", v -> v.region, isNotBlank())
            .field("endpoint", v -> v.endpoint, isNull().orValue(scheme(isIn(value(list("http", "https"))))))
            .field("bucket", v -> v.bucket, isNotBlank())
            .field("bucketBasePath",
                   v -> v.bucketBasePath,
                   isNotNull().andValue(isNotAbsolute().andValue(hasNoTraversal())))
            .field("createIfMissingBucket", v -> v.createIfMissingBucket)
            .field("failFastIfMissingBucket", v -> v.failFastIfMissingBucket)
            .field("pathStyleAccess", v -> v.pathStyleAccess)
            .field("uriGenerationFormat", v -> v.uriGenerationFormat, isNotNull())
            .field("listingMaxKeys",
                   v -> v.listingMaxKeys,
                   isStrictlyPositive().andValue(isLessThanOrEqualTo(value(MAX_LISTING_MAX_KEYS,
                                                                           "MAX_LISTING_MAX_KEYS"))))
            .field("listingContentMode", v -> v.listingContentMode, isNotNull())
            .field("uploadChunkSize",
                   v -> v.uploadChunkSize,
                   isGreaterThanOrEqualTo(value(MIN_UPLOAD_CHUNK_SIZE, "MIN_UPLOAD_CHUNK_SIZE")))
            .field("credentialsProvider", v -> v.credentialsProvider)
            .field("matchTags", v -> v.matchTags, hasNoNullElements())
            .field("loadAllTags", v -> v.loadAllTags)
            .field("httpClientConfig", v -> v.httpClientConfig)
            .build();
   }

   /**
    * S3 region. Use bucket-specific region for better performance. Default to {@code us-east-1}.
    */
   @Getter
   public String region() {
      return region;
   }

   /**
    * Optional S3 endpoint override. Override URI format is restricted to {@code http} and {@code https}
    * schemes.
    */
   @Getter
   public Optional<URI> endpoint() {
      return nullable(endpoint);
   }

   /**
    * S3 bucket name.
    */
   @Getter
   public String bucket() {
      return bucket;
   }

   /**
    * Optional base path, relative to bucket, to store all documents for the configured repository.
    * Path is never {@code null}, but can be empty to disable it. Traversal paths are not supported so that
    * this base path can be considered a "sandbox" for configured repository. Note that this base path is a
    * backend configuration detail and will never appear in retrieved/saved document identifiers and paths.
    */
   @Getter
   public Path bucketBasePath() {
      return bucketBasePath;
   }

   /**
    * Optional flag to automatically create missing bucket. Default to
    * {@link Constants#DEFAULT_CREATE_IF_MISSING_BUCKET}.
    * Bucket will be created at repository initialization if {@link #failFastIfMissingBucket()} is
    * set to {@code true}, otherwise bucket will be created lazily when writing on repository.
    */
   @Getter
   public boolean createIfMissingBucket() {
      return createIfMissingBucket;
   }

   /**
    * Optional flag to fail-fast if missing bucket. Default to
    * {@link Constants#DEFAULT_FAIL_FAST_IF_MISSING_BUCKET}.
    */
   @Getter
   public boolean failFastIfMissingBucket() {
      return failFastIfMissingBucket;
   }

   /**
    * Optional flag to use path-style access with {@link #endpoint()}. Default to
    * {@link Constants#DEFAULT_PATH_STYLE_ACCESS}.
    */
   @Getter
   public boolean pathStyleAccess() {
      return pathStyleAccess;
   }

   /**
    * Optional URI generation format to use by default. Default to
    * {@link Constants#DEFAULT_URI_GENERATION_FORMAT}.
    */
   @Getter
   public S3UriFormat uriGenerationFormat() {
      return uriGenerationFormat;
   }

   /**
    * Optional max keys (i.e. page size) in objects listing operations. Default to
    * {@link Constants#DEFAULT_LISTING_MAX_KEYS}.
    */
   @Getter
   public int listingMaxKeys() {
      return listingMaxKeys;
   }

   /**
    * Optional listing content mode to workaround S3 limitations.
    * Default to {@link S3ListingContentMode#FAST} : S3 does not return all metadata for objects, so that
    * {@link DocumentEntry} metadata are  incomplete for both filtering and returned entries. The library can
    * complete the metadata at the cost of one extra API call per object.
    */
   @Getter
   public S3ListingContentMode listingContentMode() {
      return listingContentMode;
   }

   /**
    * Optional multipart upload chunk size in bytes. Default to {@link Constants#DEFAULT_UPLOAD_CHUNK_SIZE}.
    */
   @Getter
   public int uploadChunkSize() {
      return uploadChunkSize;
   }

   /**
    * Credentials provider. Default to {@link DefaultCredentialsProvider}.
    *
    * @return credentials provider
    */
   @Getter
   public AwsCredentialsProvider credentialsProvider() {
      return credentialsProvider;
   }

   /**
    * Document metadata attributes are loaded from S3 object metadata by default.
    * Returns patterns matching object tag keys to load as document metadata attributes, and matching
    * document metadata attribute keys to save as object tags. By default, no tags are matched.
    * If an object metadata is already existing with the same key, its value will always be overridden with
    * tag value.
    * A tag/attribute matches if any pattern in the list matches the tag/attribute key.
    *
    * @return patterns to match tag keys to load/save
    */
   @Getter
   public List<Pattern> matchTags() {
      return matchTags;
   }

   /**
    * Returns whether to always load all available tags as document metadata attributes, and not just those
    * matching {@link #matchTags()}. Default to {@code false}.
    * If an object metadata is already existing with the same key, its value will always be overridden with
    * tag value.
    *
    * @return whether to always load all available tags, and not just those matching configured patterns
    */
   @Getter
   public boolean loadAllTags() {
      return loadAllTags;
   }

   /**
    * Returns HTTP client configuration.
    *
    * @return HTTP client configuration or {@code null} if no custom client configuration is set
    */
   @Getter
   public HttpClientConfig httpClientConfig() {
      return httpClientConfig;
   }

   /**
    * Default AWS S3 credentials provider.
    *
    * @implNote Do not use {@link DefaultCredentialsProvider#create()} here, as it's returning a
    *       singleton instance, and HTTP connection pool used for token retrieval could be closed by another
    *       AWS SDK instance.
    *       For the same reason, always build a new provided for each new S3 repository, do not precompute it
    *       and store it.
    */
   private static DefaultCredentialsProvider defaultCredentialsProvider() {
      return DefaultCredentialsProvider.builder().build();
   }

   /**
    * S3 listing operation metadata mode. S3 listing operation does not return metadata-complete objects, so
    * that costly extra calls must be made. this mode enables to control performance over data-completeness.
    */
   public enum S3ListingContentMode {
      /**
       * High performance mode : no metadata are completed for both filtering and returned entries. Use this
       * mode only if you don't rely on objects metadata.
       */
      FAST,
      /**
       * Low to very low performance mode : metadata are completed after filtering, for returned entries. Use
       * this mode only if you do not need complete metadata for specification filtering, but only for
       * returned entries -> performance will depend on the number of returned objects after filtering.
       */
      RETURN_COMPLETE,
      /**
       * Very low performance mode : metadata are completed for both filtering and returned entries. Use this
       * mode only if you need complete metadata -> performance will depend on the number of objects in the
       * bucket.
       */
      FILTERING_COMPLETE
   }

   /**
    * URI generation format.
    */
   public enum S3UriFormat {
      /**
       * S3 Cli mode : {@code s3://<bucket>[/<key>]}.
       */
      S3,
      /**
       * Virtual-hosted style HTTP mode : {@code https://<bucket>.s3[.<region>].amazonaws.com[/<key>]}.
       */
      VIRTUAL_HOST_STYLE,
      /**
       * Path style HTTP mode : {@code https://s3[.<region>].amazonaws.com/<bucket>[/<key>]}.
       */
      PATH_STYLE
   }

   public static class HttpClientConfig extends AbstractValue {

      /**
       * Max concurrent HTTP connections hold in pool.
       */
      private final Integer maxConnections;

      private HttpClientConfig(Builder builder) {
         this.maxConnections = builder.maxConnections;
      }

      @Override
      public Fields<? extends HttpClientConfig> defineDomainFields() {
         return Fields
               .<HttpClientConfig>builder()
               .field("maxConnections", v -> v.maxConnections, isNull().orValue(isStrictlyPositive()))
               .build();
      }

      /**
       * Optional max concurrent HTTP connections hold in pool.
       *
       * @return max concurrent HTTP connections hold in pool, or {@code null} if default AWS SDK value should
       *       be used
       */
      @Getter
      public Integer maxConnections() {
         return maxConnections;
      }

      public static class Builder extends DomainBuilder<HttpClientConfig> {

         private Integer maxConnections;

         public static Builder from(HttpClientConfig config) {
            notNull(config, "config");

            return new Builder().maxConnections(config.maxConnections);
         }

         @Setter
         public Builder maxConnections(Integer maxConnections) {
            this.maxConnections = maxConnections;
            return this;
         }

         @Override
         protected HttpClientConfig buildDomainObject() {
            return new HttpClientConfig(this);
         }
      }

   }

   public static class AwsS3DocumentConfigBuilder extends DomainBuilder<AwsS3DocumentConfig> {

      private String region;
      private URI endpoint;
      private String bucket;
      private Path bucketBasePath;
      private Boolean createIfMissingBucket;
      private Boolean failFastIfMissingBucket;
      private Boolean pathStyleAccess;
      private S3UriFormat uriGenerationFormat;
      private Integer listingMaxKeys;
      private S3ListingContentMode listingContentMode;
      private Integer uploadChunkSize;
      private AwsCredentialsProvider credentialsProvider;
      private List<Pattern> matchTags = list();
      private Boolean loadAllTags;
      private HttpClientConfig httpClientConfig;

      public static AwsS3DocumentConfigBuilder from(AwsS3DocumentConfig config) {
         notNull(config, "config");

         return new AwsS3DocumentConfigBuilder()
               .region(config.region)
               .endpoint(config.endpoint)
               .bucket(config.bucket)
               .bucketBasePath(config.bucketBasePath)
               .createIfMissingBucket(config.createIfMissingBucket)
               .failFastIfMissingBucket(config.failFastIfMissingBucket)
               .pathStyleAccess(config.pathStyleAccess)
               .uriGenerationFormat(config.uriGenerationFormat)
               .listingMaxKeys(config.listingMaxKeys)
               .listingContentMode(config.listingContentMode)
               .uploadChunkSize(config.uploadChunkSize)
               .credentialsProvider(config.credentialsProvider)
               .matchTags(config.matchTags)
               .loadAllTags(config.loadAllTags)
               .httpClientConfig(config.httpClientConfig);
      }

      @Setter
      public AwsS3DocumentConfigBuilder region(String region) {
         this.region = region;
         return this;
      }

      @Getter
      public String region() {
         return region;
      }

      @Setter
      public AwsS3DocumentConfigBuilder endpoint(URI endpoint) {
         this.endpoint = endpoint;
         return this;
      }

      @Getter
      public URI endpoint() {
         return endpoint;
      }

      @Setter
      public AwsS3DocumentConfigBuilder bucket(String bucket) {
         this.bucket = bucket;
         return this;
      }

      @Getter
      public String bucket() {
         return bucket;
      }

      @Setter
      public AwsS3DocumentConfigBuilder bucketBasePath(Path bucketBasePath) {
         this.bucketBasePath = bucketBasePath;
         return this;
      }

      @Getter
      public Path bucketBasePath() {
         return bucketBasePath;
      }

      @Setter
      public AwsS3DocumentConfigBuilder createIfMissingBucket(Boolean createIfMissingBucket) {
         this.createIfMissingBucket = createIfMissingBucket;
         return this;
      }

      @Getter
      public Boolean createIfMissingBucket() {
         return createIfMissingBucket;
      }

      @Setter
      public AwsS3DocumentConfigBuilder failFastIfMissingBucket(Boolean failFastIfMissingBucket) {
         this.failFastIfMissingBucket = failFastIfMissingBucket;
         return this;
      }

      @Getter
      public Boolean failFastIfMissingBucket() {
         return failFastIfMissingBucket;
      }

      @Setter
      public AwsS3DocumentConfigBuilder pathStyleAccess(Boolean pathStyleAccess) {
         this.pathStyleAccess = pathStyleAccess;
         return this;
      }

      @Getter
      public Boolean pathStyleAccess() {
         return pathStyleAccess;
      }

      @Setter
      public AwsS3DocumentConfigBuilder uriGenerationFormat(S3UriFormat uriGenerationFormat) {
         this.uriGenerationFormat = uriGenerationFormat;
         return this;
      }

      @Getter
      public S3UriFormat uriGenerationFormat() {
         return uriGenerationFormat;
      }

      @Setter
      public AwsS3DocumentConfigBuilder listingMaxKeys(int listingMaxKeys) {
         this.listingMaxKeys = listingMaxKeys;
         return this;
      }

      @Getter
      public Integer listingMaxKeys() {
         return listingMaxKeys;
      }

      @Setter
      public AwsS3DocumentConfigBuilder listingContentMode(S3ListingContentMode listingContentMode) {
         this.listingContentMode = listingContentMode;
         return this;
      }

      @Getter
      public S3ListingContentMode listingContentMode() {
         return listingContentMode;
      }

      @Setter
      public AwsS3DocumentConfigBuilder uploadChunkSize(Integer uploadChunkSize) {
         this.uploadChunkSize = uploadChunkSize;
         return this;
      }

      @Getter
      public Integer uploadChunkSize() {
         return uploadChunkSize;
      }

      @Setter
      public AwsS3DocumentConfigBuilder credentialsProvider(AwsCredentialsProvider credentialsProvider) {
         this.credentialsProvider = credentialsProvider;
         return this;
      }

      @Getter
      public AwsCredentialsProvider credentialsProvider() {
         return credentialsProvider;
      }

      @Setter
      public AwsS3DocumentConfigBuilder matchTags(List<Pattern> matchTags) {
         this.matchTags = list(matchTags);
         return this;
      }

      public AwsS3DocumentConfigBuilder matchTags(Pattern... matchTags) {
         return matchTags(list(matchTags));
      }

      public AwsS3DocumentConfigBuilder addMatchTag(Pattern matchTag) {
         matchTags.add(matchTag);
         return this;
      }

      @Getter
      public List<Pattern> matchTags() {
         return matchTags;
      }

      @Setter
      public AwsS3DocumentConfigBuilder loadAllTags(Boolean loadAllTags) {
         this.loadAllTags = loadAllTags;
         return this;
      }

      @Getter
      public Boolean loadAllTags() {
         return loadAllTags;
      }

      @Setter
      public AwsS3DocumentConfigBuilder httpClientConfig(HttpClientConfig httpClientConfig) {
         this.httpClientConfig = httpClientConfig;
         return this;
      }

      @Getter
      public HttpClientConfig httpClientConfig() {
         return httpClientConfig;
      }

      @Override
      protected AwsS3DocumentConfig buildDomainObject() {
         return new AwsS3DocumentConfig(this);
      }

   }
}
