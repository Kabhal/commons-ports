/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.awss3;

import java.util.function.BiPredicate;

import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig.S3UriFormat;
import com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions;
import com.tinubu.commons.ports.document.domain.uri.ExportUriOptions;
import com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery;
import com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery.Parameter;

public class AwsS3ExportUriOptions extends DefaultExportUriOptions {

   private final S3UriFormat s3UriFormat;

   protected AwsS3ExportUriOptions(ExportUriOptions uriOptions, S3UriFormat s3UriFormat) {
      super(uriOptions.exportParameters(),
            uriOptions.forceDefaultValues(),
            uriOptions.excludeSensitiveParameters(),
            uriOptions.filterParameters());
      this.s3UriFormat = Check.notNull(s3UriFormat, "s3UriFormat");
   }

   /**
    * Sets export URI options to export all parameters.
    *
    * @param s3UriFormat force S3 URI format
    * @param excludeSensitiveParameters whether to exclude sensitive parameters
    *
    * @return export URI options
    */
   public static AwsS3ExportUriOptions allParameters(S3UriFormat s3UriFormat,
                                                     boolean excludeSensitiveParameters) {
      return new AwsS3ExportUriOptions(DefaultExportUriOptions.allParameters(excludeSensitiveParameters),
                                       s3UriFormat);
   }

   /**
    * Sets export URI options to export all parameters, excluding sensitive parameters.
    *
    * @param s3UriFormat force S3 URI format
    *
    * @return export URI options
    */
   public static AwsS3ExportUriOptions allParameters(S3UriFormat s3UriFormat) {
      return new AwsS3ExportUriOptions(DefaultExportUriOptions.allParameters(), s3UriFormat);
   }

   /**
    * Sets export URI options to export no parameters.
    *
    * @param s3UriFormat force S3 URI format
    *
    * @return export URI options
    */
   public static AwsS3ExportUriOptions noParameters(S3UriFormat s3UriFormat) {
      return new AwsS3ExportUriOptions(DefaultExportUriOptions.noParameters(), s3UriFormat);
   }

   /**
    * Sets export URI options to export the minimum set of parameters to recreate a repository with the same
    * configuration.
    * Sensitive parameters are not exported.
    * Parameters with default values are not exported.
    *
    * @param s3UriFormat force S3 URI format
    *
    * @return export URI options
    */
   public static AwsS3ExportUriOptions defaultParameters(S3UriFormat s3UriFormat) {
      return new AwsS3ExportUriOptions(DefaultExportUriOptions.defaultParameters(), s3UriFormat);
   }

   /**
    * Sets export URI options to export the minimum set of parameters to recreate a repository with the same
    * configuration.
    * Sensitive parameters are not exported.
    *
    * @param s3UriFormat force S3 URI format
    * @param forceDefaultValues whether to force export parameters with default values
    *
    * @return export URI options
    */
   public static AwsS3ExportUriOptions defaultParameters(S3UriFormat s3UriFormat,
                                                         boolean forceDefaultValues) {
      return new AwsS3ExportUriOptions(DefaultExportUriOptions.defaultParameters(forceDefaultValues),
                                       s3UriFormat);
   }

   /**
    * Sets export URI options to export the minimum set of parameters to recreate a repository with the same
    * configuration.
    * Sensitive parameters are also exported.
    * Parameters with default values are not exported.
    *
    * @param s3UriFormat force S3 URI format
    *
    * @return export URI options
    */
   public static AwsS3ExportUriOptions sensitiveParameters(S3UriFormat s3UriFormat) {
      return new AwsS3ExportUriOptions(DefaultExportUriOptions.sensitiveParameters(), s3UriFormat);
   }

   /**
    * Sets export URI options to export the minimum set of parameters to recreate a repository with the same
    * configuration.
    * Sensitive parameters are also exported.
    *
    * @param s3UriFormat force S3 URI format
    * @param forceDefaultValues whether to force export parameters with default values
    *
    * @return export URI options
    */
   public static AwsS3ExportUriOptions sensitiveParameters(S3UriFormat s3UriFormat,
                                                           boolean forceDefaultValues) {
      return new AwsS3ExportUriOptions(DefaultExportUriOptions.sensitiveParameters(forceDefaultValues),
                                       s3UriFormat);
   }

   @Override
   public AwsS3ExportUriOptions exportParameters(boolean exportParameters) {
      return new AwsS3ExportUriOptions(super.exportParameters(exportParameters), s3UriFormat);
   }

   @Override
   public AwsS3ExportUriOptions forceDefaultValues(boolean forceDefaultValues) {
      return new AwsS3ExportUriOptions(super.forceDefaultValues(forceDefaultValues), s3UriFormat);
   }

   @Override
   public AwsS3ExportUriOptions excludeSensitiveParameters(boolean excludeSensitiveParameters) {
      return new AwsS3ExportUriOptions(super.excludeSensitiveParameters(excludeSensitiveParameters),
                                       s3UriFormat);
   }

   @Override
   public AwsS3ExportUriOptions filterParameters(BiPredicate<ParameterizedQuery, Parameter> filterParameters) {
      return new AwsS3ExportUriOptions(super.filterParameters(filterParameters), s3UriFormat);
   }

   public AwsS3ExportUriOptions s3UriFormat(S3UriFormat s3UriFormat) {
      return new AwsS3ExportUriOptions(this, s3UriFormat);
   }

   public S3UriFormat s3UriFormat() {
      return s3UriFormat;
   }
}
