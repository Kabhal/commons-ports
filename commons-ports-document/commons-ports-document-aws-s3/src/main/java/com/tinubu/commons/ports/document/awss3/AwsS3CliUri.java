/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.tinubu.commons.ports.document.awss3;

import static com.tinubu.commons.ddd2.invariant.rules.domain.ids.UriRules.hasNoPort;
import static com.tinubu.commons.ddd2.invariant.rules.domain.ids.UriRules.hasNoUserInfo;
import static com.tinubu.commons.ddd2.invariant.rules.domain.ids.UriRules.isNotEmptyHierarchicalServer;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;

import java.net.URI;

import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.Invariant;
import com.tinubu.commons.ddd2.invariant.Invariants;
import com.tinubu.commons.ddd2.invariant.ParameterValue;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.invariant.rules.domain.ids.UriRules;
import com.tinubu.commons.ddd2.uri.DefaultUriRestrictions;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ddd2.uri.UriRestrictions;
import com.tinubu.commons.ddd2.uri.parts.SimpleHost;
import com.tinubu.commons.ddd2.uri.parts.SimplePath;
import com.tinubu.commons.ddd2.uri.parts.SimpleScheme;
import com.tinubu.commons.ddd2.uri.parts.SimpleServerAuthority;

/**
 * AWS S3 CLI {@link AwsS3Uri}.
 * <p>
 * Supported URI formats :
 * <ul>
 *    <li>using S3 parameters : {@code s3://<bucket>[/<key>][?<query>][#<fragment>]}</li>
 *    <li>using URI parameters : {@code s3://<host>[</path>][?<query>][#<fragment>]}</li>
 * </ul>
 * However, supported format depends on {@link DefaultUriRestrictions} restrictions. By default,
 * <ul>
 *    <li>{@link DefaultUriRestrictions#query() query} : query is not supported</li>
 *    <li>{@link DefaultUriRestrictions#fragment() fragment} : fragment is not supported</li>
 * </ul>
 *
 * @implNote S3 keys can contain {@code /} so that implementation must take it into consideration.
 */
public class AwsS3CliUri extends AbstractAwsS3Uri<AwsS3CliUri> {

   protected static final String SCHEME = "s3";

   protected AwsS3CliUri(URI uri,
                         UriRestrictions restrictions,
                         Scheme scheme,
                         SchemeSpecific schemeSpecific,
                         Authority authority,
                         Path path,
                         Query query,
                         Fragment fragment,
                         boolean newObject) {
      super(uri, restrictions, scheme, schemeSpecific, authority, path, query, fragment, newObject);
   }

   protected AwsS3CliUri(UriRestrictions restrictions,
                         Scheme scheme,
                         SchemeSpecific schemeSpecific,
                         Authority authority, Path path, Query query, Fragment fragment) {
      super(restrictions, scheme, schemeSpecific, authority, path, query, fragment);
   }

   protected AwsS3CliUri(URI uri, UriRestrictions restrictions) {
      super(uri, restrictions);
   }

   protected AwsS3CliUri(Uri uri, UriRestrictions restrictions) {
      super(uri, restrictions);
   }

   @Override
   @SuppressWarnings("unchecked")
   protected Fields<? extends AwsS3CliUri> defineDomainFields() {
      return Fields
            .<AwsS3CliUri>builder()
            .superFields((Fields<? super AwsS3CliUri>) super.defineDomainFields())
            .field("scheme", v -> v.scheme)
            .field("schemeSpecific", v -> v.schemeSpecific)
            .field("authority", v -> v.authority)
            .field("path", v -> v.path)
            .field("query", v -> v.query)
            .field("fragment", v -> v.fragment)
            .build();
   }

   @Override
   public Invariants domainInvariants() {
      var uriCompatibility = Invariant
            .of(() -> this,
                UriRules.isAbsolute(ParameterValue.value(SCHEME)),
                UriRules.isHierarchicalServer().andValue(isNotEmptyHierarchicalServer()))
            .groups(COMPATIBILITY_GROUP);
      var uriValidity = Invariant.of(() -> this, hasNoUserInfo(), hasNoPort());

      return super.domainInvariants().withInvariants(Invariants.of(uriCompatibility, uriValidity));
   }

   public static AwsS3CliUri ofUri(URI uri, UriRestrictions restrictions) {
      return buildUri(() -> new AwsS3CliUri(uri, restrictions));
   }

   public static AwsS3CliUri ofUri(URI uri) {
      return ofUri(uri, defaultRestrictions());
   }

   public static AwsS3CliUri ofUri(Uri uri, UriRestrictions restrictions) {
      return buildUri(() -> new AwsS3CliUri(uri, restrictions));
   }

   public static AwsS3CliUri ofUri(Uri uri) {
      return ofUri(uri, defaultRestrictions());
   }

   public static AwsS3CliUri ofUri(String uri, UriRestrictions restrictions) {
      return ofUri(Uri.uri(uri), restrictions);
   }

   public static AwsS3CliUri ofUri(String uri) {
      return ofUri(uri, defaultRestrictions());
   }

   /**
    * Creates new {@link AwsS3CliUri}.
    *
    * @param bucket bucket name
    * @param key optional key, or {@code null}
    * @param query optional query, or {@code null}
    *
    * @return new instance
    */
   // FIXME test + key blank ?
   public static AwsS3CliUri awsS3Uri(String bucket, String key, Query query) {
      Check.notBlank(bucket, "bucket");

      return buildUri(() -> new AwsS3CliUri(DefaultUriRestrictions.automatic(query, null),
                                            SimpleScheme.of(SCHEME),
                                            null,
                                            SimpleServerAuthority.of(SimpleHost.of(bucket)),
                                            nullable(key)
                                                  .filter(k -> !k.isEmpty())
                                                  .map(k -> "/" + k)
                                                  .map(SimplePath::of).orElse(null), query, null));
   }

   @Override
   protected AwsS3CliUri recreate(URI uri,
                                  UriRestrictions restrictions,
                                  Scheme scheme,
                                  SchemeSpecific schemeSpecific,
                                  Authority authority,
                                  Path path,
                                  Query query,
                                  Fragment fragment,
                                  boolean newObject) {
      return buildUri(() -> new AwsS3CliUri(uri,
                                            restrictions,
                                            scheme,
                                            schemeSpecific,
                                            authority,
                                            path,
                                            query,
                                            fragment,
                                            newObject));
   }

   @Override
   public AwsS3UriStyle style() {
      return AwsS3UriStyle.S3;
   }

   @Override
   public boolean secured() {
      return true;
   }
}
