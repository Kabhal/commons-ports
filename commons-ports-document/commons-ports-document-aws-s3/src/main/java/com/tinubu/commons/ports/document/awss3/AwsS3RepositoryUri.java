/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.awss3;

import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotAbsolute;
import static com.tinubu.commons.lang.util.CheckedPredicate.alwaysFalse;
import static com.tinubu.commons.lang.util.CheckedPredicate.alwaysTrue;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.instanceOf;
import static com.tinubu.commons.lang.util.PathUtils.emptyPath;
import static com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig.S3UriFormat.S3;
import static com.tinubu.commons.ports.document.awss3.AwsS3RepositoryUri.Parameters.BUCKET_BASE_PATH;
import static com.tinubu.commons.ports.document.awss3.AwsS3RepositoryUri.Parameters.CREATE_IF_MISSING_BUCKET;
import static com.tinubu.commons.ports.document.awss3.AwsS3RepositoryUri.Parameters.ENDPOINT;
import static com.tinubu.commons.ports.document.awss3.AwsS3RepositoryUri.Parameters.FAIL_FAST_IF_MISSING_BUCKET;
import static com.tinubu.commons.ports.document.awss3.AwsS3RepositoryUri.Parameters.LISTING_CONTENT_MODE;
import static com.tinubu.commons.ports.document.awss3.AwsS3RepositoryUri.Parameters.LISTING_MAX_KEYS;
import static com.tinubu.commons.ports.document.awss3.AwsS3RepositoryUri.Parameters.LOAD_ALL_TAGS;
import static com.tinubu.commons.ports.document.awss3.AwsS3RepositoryUri.Parameters.MATCH_TAGS;
import static com.tinubu.commons.ports.document.awss3.AwsS3RepositoryUri.Parameters.PATH_STYLE_ACCESS;
import static com.tinubu.commons.ports.document.awss3.AwsS3RepositoryUri.Parameters.REGION;
import static com.tinubu.commons.ports.document.awss3.AwsS3RepositoryUri.Parameters.UPLOAD_CHUNK_SIZE;
import static com.tinubu.commons.ports.document.awss3.AwsS3RepositoryUri.Parameters.URI_GENERATION_FORMAT;
import static com.tinubu.commons.ports.document.awss3.AwsS3RepositoryUri.Parameters.allParameters;
import static com.tinubu.commons.ports.document.awss3.AwsS3RepositoryUri.Parameters.valueParameters;
import static com.tinubu.commons.ports.document.awss3.Constants.DEFAULT_CREATE_IF_MISSING_BUCKET;
import static com.tinubu.commons.ports.document.awss3.Constants.DEFAULT_FAIL_FAST_IF_MISSING_BUCKET;
import static com.tinubu.commons.ports.document.awss3.Constants.DEFAULT_LISTING_CONTENT_MODE;
import static com.tinubu.commons.ports.document.awss3.Constants.DEFAULT_LISTING_MAX_KEYS;
import static com.tinubu.commons.ports.document.awss3.Constants.DEFAULT_LOAD_ALL_TAGS;
import static com.tinubu.commons.ports.document.awss3.Constants.DEFAULT_PATH_STYLE_ACCESS;
import static com.tinubu.commons.ports.document.awss3.Constants.DEFAULT_UPLOAD_CHUNK_SIZE;
import static com.tinubu.commons.ports.document.awss3.Constants.DEFAULT_URI_GENERATION_FORMAT;
import static com.tinubu.commons.ports.document.awss3.S3Utils.defaultRegion;
import static com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery.ExportUriFilter.excludeParametersByName;
import static com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery.ExportUriFilter.isRegisteredParameter;
import static com.tinubu.commons.ports.document.domain.uri.UriUtils.exportUriFilter;

import java.net.URI;
import java.nio.file.Path;
import java.util.List;
import java.util.function.BiPredicate;
import java.util.regex.Pattern;

import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.domain.type.Id;
import com.tinubu.commons.ddd2.invariant.Validate;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.invariant.rules.PathRules;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ddd2.uri.uris.HttpUri;
import com.tinubu.commons.lang.convert.ConversionFailedException;
import com.tinubu.commons.lang.util.Pair;
import com.tinubu.commons.lang.util.PathUtils;
import com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig.AwsS3DocumentConfigBuilder;
import com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig.S3ListingContentMode;
import com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig.S3UriFormat;
import com.tinubu.commons.ports.document.awss3.AwsS3Uri.AwsS3UriStyle;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.uri.AbstractRepositoryUri;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.ExportUriOptions;
import com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery;
import com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery.Parameter;
import com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery.ValueParameter;
import com.tinubu.commons.ports.document.domain.uri.RepositoryUri;

/**
 * AWS S3 URI repository URI. This URI must be used to identify a repository, use {@link AwsS3DocumentUri} to
 * identify a document.
 * <p>
 * The bucket base path will be set to URI path, if present, or {@code ""} by default.
 * <p>
 * Supported URI format :
 * <ul>
 *    <li>S3 Cli mode : {@code s3://<bucket>[/<bucket-base-path>][?<parameters>]}</li>
 *    <li>Virtual-hosted style HTTP mode : {@code http[s]://<bucket>.s3[.<region>].amazonaws.com[/<bucket-base-path>][?<parameters>]}</li>
 *    <li>Path style HTTP mode : {@code http[s]://s3[.<region>].amazonaws.com/<bucket>[/<bucket-base-path>][?<parameters>]}</li>
 *    <li>Commons-ports wrapped URI : {@code doc[ument]:s3:<aws-s3-uri>}</li>
 * </ul>
 * <p>
 * Supported URI parameters :
 * <ul>
 * <li>{@code region} (String) [us-east-1] : S3 region</li>
 * <li>{@code bucketBasePath} (Path) [&lt;automatic&gt;] : relative base path, to store all documents, can be empty</li>
 * <li>{@code uriGenerationFormat} (S3UriFormat) [VIRTUAL_HOST_STYLE] : URI generation format to use by default</li>
 * <li>{@code endpoint} (URI) : S3 endpoint override</li>
 * <li>{@code pathStyleAccess} (Boolean) [{@value Constants#DEFAULT_PATH_STYLE_ACCESS}] : whether to use path-style access with specified endpoint</li>
 * <li>{@code createIfMissingBucket} (Boolean) [{@value Constants#DEFAULT_CREATE_IF_MISSING_BUCKET}] : whether to automatically create missing bucket</li>
 * <li>{@code failFastIfMissingBucket} (Boolean) [{@value Constants#DEFAULT_FAIL_FAST_IF_MISSING_BUCKET}] : whether to fail-fast if missing bucket</li>
 * <li>{@code listingContentMode} (S3ListingContentMode) [FAST] : listing content mode to workaround S3 limitations</li>
 * <li>{@code listingMaxKeys} (Integer) [{@value Constants#DEFAULT_LISTING_MAX_KEYS}] : max keys (i.e. page size) in objects listing operations</li>
 * <li>{@code uploadChunkSize} (Integer) [{@value Constants#DEFAULT_UPLOAD_CHUNK_SIZE}] : multipart upload chunk size in bytes</li>
 * <li>{@code loadAllTags} (Boolean) [{@value Constants#DEFAULT_LOAD_ALL_TAGS}] : whether to always load all available tags as document metadata attributes, not just those listed in {@code matchTags}</li>
 * <li>{@code matchTags} (String[]) [] : patterns matching object tag keys to load/save as document metadata attributes</li>
 * </ul>
 *
 * @implSpec Design choice : arbitrary endpoint URL is not compatible because it does specifically
 *       identify this repository by scheme. You have to use {@code endpoint} parameter instead.
 * @see AwsS3DocumentUri
 * @see AwsS3DocumentConfig
 */
public class AwsS3RepositoryUri extends AbstractRepositoryUri implements RepositoryUri {

   private static final String WRAPPED_URI_REPOSITORY_TYPE = "s3";
   /**
    * Feature flag : whether to use {@link #defaultBucketBasePath(Uri, boolean)} to override
    * {@link Parameters#BUCKET_BASE_PATH} default value in {@link #exportUri(ExportUriOptions)}.
    * If set to {@code true}, {@code bucketBasePath} parameter won't be exported if it matches default
    * behavior, depending on export options.
    */
   private static final boolean EXPORT_URI_DEFAULT_BUCKET_BASE_PATH_AS_DEFAULT_VALUE = true;

   protected final Path bucketBasePath;
   protected final DocumentPath documentId;
   protected final String region;

   protected AwsS3RepositoryUri(AwsS3Uri uri, boolean documentUri) {
      super(uri, documentUri);

      try {
         var parsed = parseUri(documentUri);
         this.bucketBasePath = parsed.getLeft();
         this.documentId = parsed.getRight();
         this.region =
               awsS3Uri().region().orElseGet(() -> query().parameter(REGION, alwaysTrue()).orElseThrow());
      } catch (ConversionFailedException e) {
         throw new InvalidUriException(uri, e).subMessage(e);
      }
   }

   @Override
   @SuppressWarnings("unchecked")
   protected Fields<? extends AwsS3RepositoryUri> defineDomainFields() {
      return Fields
            .<AwsS3RepositoryUri>builder()
            .superFields((Fields<AwsS3RepositoryUri>) super.defineDomainFields())
            .field("bucketBasePath",
                   v -> v.bucketBasePath,
                   PathRules.isNotAbsolute().andValue(hasNoTraversal()))
            .build();
   }

   /**
    * Creates a repository URI from {@link Uri}. Specified URI must represent a "repository" URI.
    *
    * @param uri repository URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI has invalid syntax
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static AwsS3RepositoryUri ofRepositoryUri(Uri uri) {
      Check.notNull(uri, "uri");
      validateUriCompatibility(uri, "uri", isCompatibleUriType(AwsS3Uri.class, HttpUri.class));

      return buildUri(() -> new AwsS3RepositoryUri(awsS3Uri(uri), false));
   }

   /**
    * Creates a repository URI from {@link URI}. Specified URI must represent a "repository" URI.
    *
    * @param uri repository URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI has invalid syntax
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static AwsS3RepositoryUri ofRepositoryUri(URI uri) {
      Check.notNull(uri, "uri");

      return ofRepositoryUri(Uri.ofUri(uri));
   }

   /**
    * Creates a repository URI from URI string. Specified URI must represent a "repository" URI.
    *
    * @param uri repository URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI has invalid syntax
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static AwsS3RepositoryUri ofRepositoryUri(String uri) {
      Check.notNull(uri, "uri");

      return ofRepositoryUri(Uri.ofUri(uri));
   }

   /**
    * Creates a repository URI from configuration.
    *
    * @param config repository configuration
    *
    * @return new instance
    */
   public static AwsS3RepositoryUri ofConfig(AwsS3DocumentConfig config) {
      Check.notNull(config, "config");

      return buildUri(() -> new AwsS3RepositoryUri(awsS3Uri(config, null), false));
   }

   /**
    * Creates a repository URI from {@link RepositoryUri}. Specified repository URI can can be a
    * {@link RepositoryUri} to represent a "repository" URI, or a {@link DocumentUri} to represent a
    * "document" URI.
    *
    * @param uri repository URI
    * @param defaultBucketBasePath default bucket base path to use if {@code bucketBasePath} query
    *       parameter not present
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    * @implSpec this method is package-private because user code should always create
    *       {@link RepositoryUri} directly from a URI.
    */
   static AwsS3RepositoryUri ofRepositoryUri(RepositoryUri uri, Path defaultBucketBasePath) {
      Check.notNull(uri, "uri");
      var normalizedDefaultBucketBasePath = Check.validate(nullable(defaultBucketBasePath, Path::normalize),
                                                           "defaultBucketBasePath",
                                                           isNotAbsolute().andValue(hasNoTraversal()));

      validateUriCompatibility(uri, "uri", isCompatibleRepositoryUriType(AwsS3RepositoryUri.class));

      if (uri instanceof AwsS3RepositoryUri) {
         return buildUri(() -> new AwsS3RepositoryUri(mapQuery(awsS3Uri(uri), (u, q) -> {
            q.registerParameter(BUCKET_BASE_PATH.defaultValue(((AwsS3RepositoryUri) uri).bucketBasePath()));
            return u;
         }), uri instanceof DocumentUri));
      } else {
         return buildUri(() -> new AwsS3RepositoryUri(mapQuery(awsS3Uri(uri), (u, q) -> {
            q.registerParameter(BUCKET_BASE_PATH.defaultValue(normalizedDefaultBucketBasePath));
            return u;
         }), uri instanceof DocumentUri));

      }
   }

   /**
    * Creates a repository URI from {@link RepositoryUri}.
    * <p>
    * Default bucket base path is set to {@code ""}.
    *
    * @param uri repository URI
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    * @implSpec this method is package-private because user code should always create
    *       {@link RepositoryUri} directly from a URI.
    */
   static AwsS3RepositoryUri ofRepositoryUri(RepositoryUri uri) {
      return ofRepositoryUri(uri, defaultBucketBasePath(uri, uri instanceof DocumentUri));
   }

   public AwsS3Uri awsS3Uri() {
      return (AwsS3Uri) uri;
   }

   @Override
   public AwsS3RepositoryUri normalize() {
      return buildUri(() -> new AwsS3RepositoryUri(awsS3Uri().normalize(), documentUri));
   }

   @Override
   public AwsS3RepositoryUri resolve(Uri uri) {
      return buildUri(() -> new AwsS3RepositoryUri(awsS3Uri().resolve(uri), documentUri));
   }

   @Override
   public AwsS3RepositoryUri resolve(URI uri) {
      return buildUri(() -> new AwsS3RepositoryUri(awsS3Uri().resolve(uri), documentUri));
   }

   @Override
   public AwsS3RepositoryUri relativize(Uri uri) {
      throw new UnsupportedOperationException();
   }

   @Override
   public AwsS3RepositoryUri relativize(URI uri) {
      throw new UnsupportedOperationException();
   }

   @Override
   public int compareTo(Id o) {
      if (!(o instanceof AwsS3RepositoryUri)) {
         return -1;
      } else if (o == this) {
         return 0;
      } else {
         return awsS3Uri().compareTo(((AwsS3RepositoryUri) o).awsS3Uri());
      }
   }

   /**
    * Returns a pre-configured builder from this repository URI.
    */
   public AwsS3DocumentConfigBuilder toConfig() {
      return new AwsS3DocumentConfigBuilder()
            .<AwsS3DocumentConfigBuilder>reconstitute()
            .<AwsS3DocumentConfigBuilder, String>optionalChain(awsS3Uri()
                                                                     .region()
                                                                     .or(() -> query().parameter(REGION,
                                                                                                 alwaysTrue())),
                                                               AwsS3DocumentConfigBuilder::region)
            .bucket(awsS3Uri().bucket())
            .pathStyleAccess(query().parameter(PATH_STYLE_ACCESS, alwaysTrue()).orElse(false))
            .<AwsS3DocumentConfigBuilder, S3UriFormat>optionalChain(query().parameter(URI_GENERATION_FORMAT,
                                                                                      alwaysTrue()),
                                                                    AwsS3DocumentConfigBuilder::uriGenerationFormat)
            .<AwsS3DocumentConfigBuilder, URI>optionalChain(query().parameter(ENDPOINT, alwaysTrue()),
                                                            AwsS3DocumentConfigBuilder::endpoint)
            .bucketBasePath(bucketBasePath)
            .<AwsS3DocumentConfigBuilder, Boolean>optionalChain(query().parameter(CREATE_IF_MISSING_BUCKET,
                                                                                  alwaysTrue()),
                                                                AwsS3DocumentConfigBuilder::createIfMissingBucket)
            .<AwsS3DocumentConfigBuilder, Boolean>optionalChain(query().parameter(FAIL_FAST_IF_MISSING_BUCKET,
                                                                                  alwaysTrue()),
                                                                AwsS3DocumentConfigBuilder::failFastIfMissingBucket)
            .<AwsS3DocumentConfigBuilder, S3ListingContentMode>optionalChain(query().parameter(
                  LISTING_CONTENT_MODE,
                  alwaysTrue()), AwsS3DocumentConfigBuilder::listingContentMode)
            .<AwsS3DocumentConfigBuilder, Integer>optionalChain(query().parameter(LISTING_MAX_KEYS,
                                                                                  alwaysTrue()),
                                                                AwsS3DocumentConfigBuilder::listingMaxKeys)
            .<AwsS3DocumentConfigBuilder, Integer>optionalChain(query().parameter(UPLOAD_CHUNK_SIZE,
                                                                                  alwaysTrue()),
                                                                AwsS3DocumentConfigBuilder::uploadChunkSize)
            .<AwsS3DocumentConfigBuilder, Boolean>optionalChain(query().parameter(LOAD_ALL_TAGS,
                                                                                  alwaysTrue()),
                                                                AwsS3DocumentConfigBuilder::loadAllTags)
            .matchTags(query().parameters(MATCH_TAGS, alwaysTrue()));
   }

   public String region() {
      return region;
   }

   public String bucket() {
      return awsS3Uri().bucket();
   }

   public Path bucketBasePath() {
      return bucketBasePath;
   }

   @Override
   public boolean supportsUri(RepositoryUri uri) {
      Check.notNull(uri, "uri");

      return supportsRepositoryUri(uri, AwsS3RepositoryUri.class, AwsS3RepositoryUri::ofRepositoryUri)
            .map(awsS3Uri -> {
               if (!awsS3Uri.region().equals(region())) {
                  return false;
               }
               if (!awsS3Uri.bucket().equals(bucket())) {
                  return false;
               }
               if (!awsS3Uri.bucketBasePath().equals(bucketBasePath())) {
                  return false;
               }

               return mapQuery(awsS3Uri(),
                               (u, q) -> q.includeParameters(query(awsS3Uri.awsS3Uri()),
                                                             isRegisteredParameter()));
            })
            .orElse(false);
   }

   @Override
   public URI exportURI(ExportUriOptions options) {
      Check.notNull(options, "options");

      var format = instanceOf(options, AwsS3ExportUriOptions.class).map(AwsS3ExportUriOptions::s3UriFormat)
            .orElseGet(() -> query().parameter(URI_GENERATION_FORMAT, alwaysTrue()).orElseThrow());

      var filter = exportUriFilter(options,
                                   excludeParametersByName("uriGenerationFormat").and(excludeParametersByName(
                                         embeddedS3UriParameters(format))));

      var exportQuery = exportQuery(filter).noQueryIfEmpty(true);
      var exportUri = awsS3Uri(format).component().query(__ -> exportQuery);

      if (options.excludeSensitiveParameters()) {
         exportUri = exportUri.component().conditionalUserInfo(__ -> null);
      }

      return exportUri.toURI();
   }

   private ParameterizedQuery exportQuery(BiPredicate<ParameterizedQuery, Parameter> filter) {
      var exportQuery = query();

      if (EXPORT_URI_DEFAULT_BUCKET_BASE_PATH_AS_DEFAULT_VALUE) {
         var bucketBasePathParameter =
               BUCKET_BASE_PATH.defaultValue(defaultBucketBasePath(awsS3Uri(), documentUri));
         exportQuery =
               exportQuery.registerParameter(ValueParameter.ofValue(bucketBasePathParameter, bucketBasePath));
      }

      return exportQuery.exportQuery(filter);
   }

   @Override
   public RepositoryUri exportUri(ExportUriOptions options) {
      return RepositoryUri.ofRepositoryUri(exportURI(options));
   }

   /**
    * @return parameter names that are part if URI components, and can be excluded from query string,
    *       depending on S3 URI format.
    */
   private static List<String> embeddedS3UriParameters(S3UriFormat s3UriFormat) {
      if (s3UriFormat == S3) {
         return list("bucket");
      } else {
         return list("bucket", "region");
      }
   }

   /**
    * Creates a parameterized {@link AwsS3Uri} from specified {@link Uri}.
    * <p>
    * If specified URI is wrapped, it is unwrapped before creating parameterized URI.
    * Specified URI is normalized and checked for remaining traversals, after unwrapping.
    *
    * @param uri URI
    *
    * @return new parameterized URI, with all registered parameters
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   protected static AwsS3Uri awsS3Uri(Uri uri) {
      Check.notNull(uri, "uri");
      var unwrappedUri = unwrapUri(uri, WRAPPED_URI_REPOSITORY_TYPE);

      return parameterizedQueryUri(unwrappedUri,
                                   defaultRestrictions(),
                                   AwsS3UriFactory.instance()::ofUri,
                                   allParameters());
   }

   /**
    * Creates a parameterized URI from current state and using specified URI format.
    *
    * @param format new URI format
    *
    * @return new S3 URI
    */
   protected AwsS3Uri awsS3Uri(S3UriFormat format) {
      Check.notNull(format, "format");

      return awsS3Uri().toStyle(s3UriStyle(format));
   }

   /**
    * Creates a parameterized URI from specified configuration.
    * <p>
    * Generated URI format is specified by {@link AwsS3DocumentConfig#uriGenerationFormat()}.
    *
    * @param config repository configuration
    * @param documentId optional document id
    *
    * @return new parameterized URI, with all registered value parameters from configuration
    */
   protected static AwsS3Uri awsS3Uri(AwsS3DocumentConfig config, DocumentPath documentId) {
      var format = config.uriGenerationFormat();
      var key = (documentId != null
                 ? config.bucketBasePath().resolve(documentId.value())
                 : config.bucketBasePath()).toString();
      var query =
            ParameterizedQuery.ofEmpty().noQueryIfEmpty(true).registerParameters(valueParameters(config));

      return AwsS3UriFactory
            .instance()
            .awsS3Uri(s3UriStyle(format), config.region(), config.bucket(), key, query);
   }

   /**
    * Default bucket base path value used to complement generic {@link DocumentUri} or
    * {@link RepositoryUri}.
    *
    * @param uri generic URI as {@link AwsS3Uri}
    * @param documentId whether URI is a document URI
    *
    * @return bucket base path
    */
   protected static Path defaultBucketBasePath(AwsS3Uri uri, boolean documentId) {
      if (documentId) {
         return emptyPath();
      } else {
         return uri.key().map(Path::of).orElseGet(PathUtils::emptyPath);
      }
   }

   /**
    * Default bucket base path value used to complement generic {@link DocumentUri} or
    * {@link RepositoryUri}.
    *
    * @param uri generic URI
    * @param documentId whether URI is a document URI
    *
    * @return bucket base path
    */
   protected static Path defaultBucketBasePath(Uri uri, boolean documentId) {
      return defaultBucketBasePath(AwsS3UriFactory.instance().ofUri(uri, defaultRestrictions()), documentId);
   }

   /**
    * Checks URI syntax and parses current parameterized URI to separate the bucket base path, from the
    * document path.
    *
    * @param documentUri whether the current parameterized URI is a document URI, identifying a document
    *       instead of a repository
    *
    * @return parsed (bucket base path, document path) pair
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @implSpec The minimum set of parameters are checked here for consistency, only the parameters
    *       used in class internal state : bucket, region, bucket base path, document path. Other parameters
    *       could be checked later, e.g. in {@link #toConfig()}.
    *       <p>
    *       Default bucket base path value is only applied if it starts actual URI path, otherwise,
    *       it is discarded because it's a "default", not an explicit value for actual URI, and it could apply
    *       to whatever URI is passed in.
    */
   protected Pair<Path, DocumentPath> parseUri(boolean documentUri) {
      var key = awsS3Uri().key().map(Path::of);

      if (documentUri && key.isEmpty()) {
         throw new InvalidUriException(awsS3Uri()).subMessage("Missing document id");
      }

      awsS3Uri().region().ifPresent(region -> {
         query().parameter(REGION, alwaysFalse()).ifPresent(regionParameter -> {
            if (!region.equals(regionParameter)) {
               throw new InvalidUriException(awsS3Uri()).subMessage(
                     "'%s=%s' parameter must match URI '%s' region",
                     REGION.key(),
                     regionParameter,
                     region);
            }
         });
      });

      Path bucketBasePath;
      DocumentPath documentId;

      var bucketBasePathParameter = query()
            .parameter(BUCKET_BASE_PATH,
                       p -> ((Parameter<Path>) p)
                             .defaultValue()
                             .map(defaultBasePath -> key
                                   .map(k -> PathUtils.startsWith(k, defaultBasePath))
                                   .orElse(true))
                             .orElse(false))
            .orElseGet(() -> defaultBucketBasePath(awsS3Uri(), documentUri))
            .normalize();

      Validate
            .satisfies(bucketBasePathParameter, isNotAbsolute().andValue(hasNoTraversal()))
            .orThrowMessage(message -> new InvalidUriException(awsS3Uri()).subMessage(
                  "'%s=%s' parameter must be relative and must not contain traversal",
                  BUCKET_BASE_PATH.key(),
                  bucketBasePathParameter.toString()));

      if (!documentUri) {
         bucketBasePath = key.orElse(bucketBasePathParameter);
         if (!PathUtils.equals(bucketBasePath, bucketBasePathParameter)) {
            throw new InvalidUriException(awsS3Uri()).subMessage("'%s=%s' parameter must match URI '%s' path",
                                                                 BUCKET_BASE_PATH.key(),
                                                                 bucketBasePathParameter.toString(),
                                                                 bucketBasePath.toString());
         }
         documentId = null;
      } else {
         bucketBasePath = bucketBasePathParameter;
         var documentPath = key.get();

         if (!PathUtils.startsWith(documentPath, bucketBasePath) || documentPath.equals(bucketBasePath)) {
            throw new InvalidUriException(awsS3Uri()).subMessage(
                  "'%s=%s' parameter must start but not be equal to URI '%s' path",
                  BUCKET_BASE_PATH.key(),
                  bucketBasePath.toString(),
                  documentPath.toString());
         }
         documentId = DocumentPath.of(bucketBasePath.relativize(documentPath));
      }

      return Pair.of(bucketBasePath, documentId);
   }

   /**
    * Maps URI format to style.
    *
    * @return current S3 URI style
    */
   protected static AwsS3UriStyle s3UriStyle(S3UriFormat format) {
      switch (format) {
         case S3:
            return AwsS3UriStyle.S3;
         case PATH_STYLE:
            return AwsS3UriStyle.PATH;
         case VIRTUAL_HOST_STYLE:
            return AwsS3UriStyle.VIRTUAL_HOST;
         default:
            throw new IllegalStateException("Unknown S3 URI format");
      }
   }

   /**
    * Supported parameter registry.
    */
   static class Parameters {
      static final Parameter<String> REGION =
            Parameter.registered("region", String.class).defaultValue(defaultRegion().id());
      static final Parameter<Boolean> PATH_STYLE_ACCESS =
            Parameter.registered("pathStyleAccess", Boolean.class).defaultValue(DEFAULT_PATH_STYLE_ACCESS);
      static final Parameter<S3UriFormat> URI_GENERATION_FORMAT = Parameter
            .registered("uriGenerationFormat", S3UriFormat.class)
            .defaultValue(DEFAULT_URI_GENERATION_FORMAT);
      static final Parameter<URI> ENDPOINT = Parameter.registered("endpoint", URI.class);
      static final Parameter<Path> BUCKET_BASE_PATH = Parameter.registered("bucketBasePath", Path.class);
      static final Parameter<Boolean> CREATE_IF_MISSING_BUCKET = Parameter
            .registered("createIfMissingBucket", Boolean.class)
            .defaultValue(DEFAULT_CREATE_IF_MISSING_BUCKET);
      static final Parameter<Boolean> FAIL_FAST_IF_MISSING_BUCKET = Parameter
            .registered("failFastIfMissingBucket", Boolean.class)
            .defaultValue(DEFAULT_FAIL_FAST_IF_MISSING_BUCKET);
      static final Parameter<S3ListingContentMode> LISTING_CONTENT_MODE = Parameter
            .registered("listingContentMode", S3ListingContentMode.class)
            .defaultValue(DEFAULT_LISTING_CONTENT_MODE);
      static final Parameter<Integer> LISTING_MAX_KEYS =
            Parameter.registered("listingMaxKeys", Integer.class).defaultValue(DEFAULT_LISTING_MAX_KEYS);
      static final Parameter<Integer> UPLOAD_CHUNK_SIZE =
            Parameter.registered("uploadChunkSize", Integer.class).defaultValue(DEFAULT_UPLOAD_CHUNK_SIZE);
      static final Parameter<Boolean> LOAD_ALL_TAGS =
            Parameter.registered("loadAllTags", Boolean.class).defaultValue(DEFAULT_LOAD_ALL_TAGS);
      static final Parameter<Pattern> MATCH_TAGS = Parameter.registered("matchTags", Pattern.class);

      @SuppressWarnings("rawtypes")
      static List<Parameter> allParameters() {
         return list(REGION,
                     PATH_STYLE_ACCESS,
                     URI_GENERATION_FORMAT,
                     ENDPOINT,
                     BUCKET_BASE_PATH,
                     CREATE_IF_MISSING_BUCKET,
                     FAIL_FAST_IF_MISSING_BUCKET,
                     LISTING_CONTENT_MODE,
                     LISTING_MAX_KEYS,
                     UPLOAD_CHUNK_SIZE,
                     LOAD_ALL_TAGS,
                     MATCH_TAGS);
      }

      @SuppressWarnings("rawtypes")
      static List<ValueParameter> valueParameters(AwsS3DocumentConfig config) {
         return list(ValueParameter.ofValue(REGION, config.region()),
                     ValueParameter.ofValue(PATH_STYLE_ACCESS, config.pathStyleAccess()),
                     ValueParameter.ofValue(URI_GENERATION_FORMAT, config.uriGenerationFormat()),
                     ValueParameter.ofValue(ENDPOINT, config.endpoint().orElse(null)),
                     ValueParameter.ofValue(BUCKET_BASE_PATH, config.bucketBasePath()),
                     ValueParameter.ofValue(CREATE_IF_MISSING_BUCKET, config.createIfMissingBucket()),
                     ValueParameter.ofValue(FAIL_FAST_IF_MISSING_BUCKET, config.failFastIfMissingBucket()),
                     ValueParameter.ofValue(LISTING_CONTENT_MODE, config.listingContentMode()),
                     ValueParameter.ofValue(LISTING_MAX_KEYS, config.listingMaxKeys()),
                     ValueParameter.ofValue(UPLOAD_CHUNK_SIZE, config.uploadChunkSize()),
                     ValueParameter.ofValue(LOAD_ALL_TAGS, config.loadAllTags()),
                     ValueParameter.ofValues(MATCH_TAGS, config.matchTags()));
      }

   }

}
