/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.tinubu.commons.ports.document.awss3;

import static com.tinubu.commons.ddd2.invariant.Validate.notNull;
import static com.tinubu.commons.ddd2.invariant.Validate.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.ddd2.uri.Uri.compatibleUri;
import static java.util.function.Function.identity;

import java.net.URI;

import com.tinubu.commons.ddd2.invariant.Validate;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.uri.ComponentUri.Query;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ddd2.uri.UriRestrictions;
import com.tinubu.commons.ports.document.awss3.AwsS3Uri.AwsS3UriStyle;

public class AwsS3UriFactory {

   private static class Singleton {
      private static final AwsS3UriFactory INSTANCE = new AwsS3UriFactory();
   }

   public static AwsS3UriFactory instance() {
      return Singleton.INSTANCE;
   }

   /**
    * Creates new {@link AwsS3Uri} by specified style.
    *
    * @param style style
    * @param region optional region name, or {@code null}
    * @param bucket bucket name
    * @param key optional key, or {@code null}
    * @param query optional query, or {@code null}
    * @param secured whether to use a secured protocol (HTTPS)
    */
   public AwsS3Uri awsS3Uri(AwsS3UriStyle style,
                            String region,
                            String bucket,
                            String key,
                            Query query,
                            boolean secured) {
      notNull(style, "style")
            .and(validate(region, "region", isNull().orValue(isNotBlank())))
            .and(Validate.notBlank(bucket, "bucket"))
            .orThrow();

      switch (style) {
         case S3:
            return AwsS3CliUri.awsS3Uri(bucket, key, query);
         case VIRTUAL_HOST:
            return AwsS3HttpUri.awsS3Uri(AwsS3UriStyle.VIRTUAL_HOST, region, bucket, key, query, secured);
         case PATH:
            return AwsS3HttpUri.awsS3Uri(AwsS3UriStyle.PATH, region, bucket, key, query, secured);
         default:
            throw new IllegalStateException(String.format("Unknown '%s' S3 URI style", style));
      }
   }

   /**
    * Creates new {@link AwsS3Uri} by specified style. Secured protocol is always used if possible, depending
    * on specified style.
    *
    * @param style style
    * @param region optional region name, or {@code null}
    * @param bucket bucket name
    * @param key optional key, or {@code null}
    * @param query optional query, or {@code null}
    */
   public AwsS3Uri awsS3Uri(AwsS3UriStyle style, String region, String bucket, String key, Query query) {
      return awsS3Uri(style, region, bucket, key, query, true);
   }

   public AwsS3Uri ofUri(URI uri, UriRestrictions restrictions) {
      notNull(uri, "uri").and(notNull(restrictions, "restrictions")).orThrow();

      return Uri
            .<AwsS3Uri>compatibleUri(uri, restrictions, AwsS3HttpUri::ofUri)
            .or(() -> compatibleUri(uri, restrictions, AwsS3CliUri::ofUri)).orElseThrow(identity());
   }

   public AwsS3Uri ofUri(URI uri) {
      Check.notNull(uri, "uri");

      return Uri
            .<AwsS3Uri>compatibleUri(uri, AwsS3HttpUri::ofUri)
            .or(() -> compatibleUri(uri, AwsS3CliUri::ofUri))
            .orElseThrow(identity());
   }

   public AwsS3Uri ofUri(String uri, UriRestrictions restrictions) {
      notNull(uri, "uri").and(notNull(restrictions, "restrictions")).orThrow();

      return Uri
            .<AwsS3Uri>compatibleUri(uri, restrictions, AwsS3HttpUri::ofUri)
            .or(() -> compatibleUri(uri, restrictions, AwsS3CliUri::ofUri))
            .orElseThrow(identity());
   }

   public AwsS3Uri ofUri(String uri) {
      Check.notNull(uri, "uri");

      return Uri
            .<AwsS3Uri>compatibleUri(uri, AwsS3HttpUri::ofUri)
            .or(() -> compatibleUri(uri, AwsS3CliUri::ofUri))
            .orElseThrow(identity());
   }

   public AwsS3Uri ofUri(Uri uri, UriRestrictions restrictions) {
      notNull(uri, "uri").and(notNull(restrictions, "restrictions")).orThrow();

      return Uri
            .<AwsS3Uri>compatibleUri(uri, restrictions, AwsS3HttpUri::ofUri)
            .or(() -> compatibleUri(uri, restrictions, AwsS3CliUri::ofUri)).orElseThrow(identity());
   }

   public AwsS3Uri ofUri(Uri uri) {
      Check.notNull(uri, "uri");

      return Uri
            .<AwsS3Uri>compatibleUri(uri, AwsS3HttpUri::ofUri)
            .or(() -> compatibleUri(uri, AwsS3CliUri::ofUri)).orElseThrow(identity());
   }
}
