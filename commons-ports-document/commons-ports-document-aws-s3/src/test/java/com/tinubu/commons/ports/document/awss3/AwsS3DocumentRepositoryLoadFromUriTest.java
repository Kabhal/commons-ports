/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.awss3;

import static org.assertj.core.api.Assertions.assertThat;

import java.nio.file.Path;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.RepositoryUri;

public class AwsS3DocumentRepositoryLoadFromUriTest {

   @Nested
   class OfUri {

      @Nested
      class FromRepositoryUri {

         @Test
         public void ofUriWhenNominal() {
            try (var repository = AwsS3DocumentRepository.ofUri(RepositoryUri.ofRepositoryUri(
                  "s3://bucket?failFastIfMissingBucket=false"))) {
               assertThat(repository.awsS3DocumentConfig.region()).isEqualTo("us-east-1");
               assertThat(repository.awsS3DocumentConfig.bucket()).isEqualTo("bucket");
               assertThat(repository.awsS3DocumentConfig.bucketBasePath()).isEqualTo(Path.of(""));
            }
         }

         @Test
         public void ofUriWhenBucketBasePath() {
            try (var repository = AwsS3DocumentRepository.ofUri(RepositoryUri.ofRepositoryUri(
                  "s3://bucket/path?failFastIfMissingBucket=false"))) {
               assertThat(repository.awsS3DocumentConfig.region()).isEqualTo("us-east-1");
               assertThat(repository.awsS3DocumentConfig.bucket()).isEqualTo("bucket");
               assertThat(repository.awsS3DocumentConfig.bucketBasePath()).isEqualTo(Path.of("path"));
            }

            try (var repository = AwsS3DocumentRepository.ofUri(RepositoryUri.ofRepositoryUri(
                  "s3://bucket?failFastIfMissingBucket=false&bucketBasePath=path/subpath"))) {
               assertThat(repository.awsS3DocumentConfig.region()).isEqualTo("us-east-1");
               assertThat(repository.awsS3DocumentConfig.bucket()).isEqualTo("bucket");
               assertThat(repository.awsS3DocumentConfig.bucketBasePath()).isEqualTo(Path.of("path/subpath"));
            }
         }

      }

      @Nested
      class FromDocumentUri {

         @Test
         public void ofUriWhenNominal() {
            try (var repository = AwsS3DocumentRepository.ofUri(DocumentUri.ofDocumentUri(
                  "s3://bucket/key?failFastIfMissingBucket=false"))) {
               assertThat(repository.awsS3DocumentConfig.region()).isEqualTo("us-east-1");
               assertThat(repository.awsS3DocumentConfig.bucket()).isEqualTo("bucket");
               assertThat(repository.awsS3DocumentConfig.bucketBasePath()).isEqualTo(Path.of(""));
            }
         }

         @Test
         public void ofUriWhenBucketBasePath() {
            try (var repository = AwsS3DocumentRepository.ofUri(DocumentUri.ofDocumentUri(
                  "s3://bucket/path/key?failFastIfMissingBucket=false"))) {
               assertThat(repository.awsS3DocumentConfig.region()).isEqualTo("us-east-1");
               assertThat(repository.awsS3DocumentConfig.bucket()).isEqualTo("bucket");
               assertThat(repository.awsS3DocumentConfig.bucketBasePath()).isEqualTo(Path.of(""));
            }

            try (var repository = AwsS3DocumentRepository.ofUri(DocumentUri.ofDocumentUri(
                  "s3://bucket/path/key?failFastIfMissingBucket=false&bucketBasePath=path"))) {
               assertThat(repository.awsS3DocumentConfig.region()).isEqualTo("us-east-1");
               assertThat(repository.awsS3DocumentConfig.bucket()).isEqualTo("bucket");
               assertThat(repository.awsS3DocumentConfig.bucketBasePath()).isEqualTo(Path.of("path"));
            }
         }

      }

      @Nested
      class FromAwsS3RepositoryUri {

         @Test
         public void ofUriWhenNominal() {
            try (var repository = AwsS3DocumentRepository.ofUri(AwsS3RepositoryUri.ofRepositoryUri(
                  "s3://bucket?failFastIfMissingBucket=false"))) {
               assertThat(repository.awsS3DocumentConfig.region()).isEqualTo("us-east-1");
               assertThat(repository.awsS3DocumentConfig.bucket()).isEqualTo("bucket");
               assertThat(repository.awsS3DocumentConfig.bucketBasePath()).isEqualTo(Path.of(""));
            }
         }

         @Test
         public void ofUriWhenBucketBasePath() {
            try (var repository = AwsS3DocumentRepository.ofUri(AwsS3RepositoryUri.ofRepositoryUri(
                  "s3://bucket/path?failFastIfMissingBucket=false"))) {
               assertThat(repository.awsS3DocumentConfig.region()).isEqualTo("us-east-1");
               assertThat(repository.awsS3DocumentConfig.bucket()).isEqualTo("bucket");
               assertThat(repository.awsS3DocumentConfig.bucketBasePath()).isEqualTo(Path.of("path"));
            }

            try (var repository = AwsS3DocumentRepository.ofUri(AwsS3RepositoryUri.ofRepositoryUri(
                  "s3://bucket?failFastIfMissingBucket=false&bucketBasePath=path/subpath"))) {
               assertThat(repository.awsS3DocumentConfig.region()).isEqualTo("us-east-1");
               assertThat(repository.awsS3DocumentConfig.bucket()).isEqualTo("bucket");
               assertThat(repository.awsS3DocumentConfig.bucketBasePath()).isEqualTo(Path.of("path/subpath"));
            }
         }

      }

      @Nested
      class FromAwsS3DocumentUri {

         @Test
         public void ofUriWhenNominal() {
            try (var repository = AwsS3DocumentRepository.ofUri(AwsS3DocumentUri.ofDocumentUri(
                  "s3://bucket/key?failFastIfMissingBucket=false",
                  Path.of("")))) {
               assertThat(repository.awsS3DocumentConfig.region()).isEqualTo("us-east-1");
               assertThat(repository.awsS3DocumentConfig.bucket()).isEqualTo("bucket");
               assertThat(repository.awsS3DocumentConfig.bucketBasePath()).isEqualTo(Path.of(""));
            }

         }

         @Test
         public void ofUriWhenBucketBasePath() {
            try (var repository = AwsS3DocumentRepository.ofUri(AwsS3DocumentUri.ofDocumentUri(
                  "s3://bucket/path/key?failFastIfMissingBucket=false",
                  Path.of("")))) {
               assertThat(repository.awsS3DocumentConfig.region()).isEqualTo("us-east-1");
               assertThat(repository.awsS3DocumentConfig.bucket()).isEqualTo("bucket");
               assertThat(repository.awsS3DocumentConfig.bucketBasePath()).isEqualTo(Path.of(""));
            }

            try (var repository = AwsS3DocumentRepository.ofUri(AwsS3DocumentUri.ofDocumentUri(
                  "s3://bucket/path/key?failFastIfMissingBucket=false&bucketBasePath=path",
                  Path.of("")))) {
               assertThat(repository.awsS3DocumentConfig.region()).isEqualTo("us-east-1");
               assertThat(repository.awsS3DocumentConfig.bucket()).isEqualTo("bucket");
               assertThat(repository.awsS3DocumentConfig.bucketBasePath()).isEqualTo(Path.of("path"));
            }

            try (var repository = AwsS3DocumentRepository.ofUri(AwsS3DocumentUri.ofDocumentUri(
                  "s3://bucket/path/key?failFastIfMissingBucket=false",
                  Path.of("path")))) {
               assertThat(repository.awsS3DocumentConfig.region()).isEqualTo("us-east-1");
               assertThat(repository.awsS3DocumentConfig.bucket()).isEqualTo("bucket");
               assertThat(repository.awsS3DocumentConfig.bucketBasePath()).isEqualTo(Path.of("path"));
            }

            try (var repository = AwsS3DocumentRepository.ofUri(AwsS3DocumentUri.ofDocumentUri(
                  "s3://bucket/path/key?failFastIfMissingBucket=false",
                  Path.of("path")))) {
               assertThat(repository.awsS3DocumentConfig.region()).isEqualTo("us-east-1");
               assertThat(repository.awsS3DocumentConfig.bucket()).isEqualTo("bucket");
               assertThat(repository.awsS3DocumentConfig.bucketBasePath())
                     .as("AwsS3DocumentUri default bucket base path must be used")
                     .isEqualTo(Path.of("path"));
            }

            try (var repository = AwsS3DocumentRepository.ofUri(AwsS3DocumentUri.ofDocumentUri(
                  "s3://bucket/path/subpath/key?failFastIfMissingBucket=false&bucketBasePath=path/subpath",
                  Path.of("path")))) {
               assertThat(repository.awsS3DocumentConfig.region()).isEqualTo("us-east-1");
               assertThat(repository.awsS3DocumentConfig.bucket()).isEqualTo("bucket");
               assertThat(repository.awsS3DocumentConfig.bucketBasePath())
                     .as("query parameter takes precedence over AwsS3DocumentUri default bucket base path")
                     .isEqualTo(Path.of("path/subpath"));
            }
         }

      }

   }

   @Nested
   class ReferencedDocument {

      @Test
      public void referencedDocumentRepositoryWhenDocumentUri() throws Exception {
         try (var referencedDocument = AwsS3DocumentRepository.referencedDocument(DocumentUri.ofDocumentUri(
               "s3://bucket/key?failFastIfMissingBucket=false"))) {
            assertThat(referencedDocument.documentRepository().awsS3DocumentConfig.region()).isEqualTo(
                  "us-east-1");
            assertThat(referencedDocument.documentRepository().awsS3DocumentConfig.bucket()).isEqualTo(
                  "bucket");
            assertThat(referencedDocument.documentRepository().awsS3DocumentConfig.bucketBasePath()).isEqualTo(
                  Path.of(""));

            assertThat(referencedDocument.documentId()).isEqualTo(DocumentPath.of("key"));
         }

         try (var referencedDocument = AwsS3DocumentRepository.referencedDocument(DocumentUri.ofDocumentUri(
               "s3://bucket/path/key?failFastIfMissingBucket=false&bucketBasePath=path"))) {
            assertThat(referencedDocument.documentRepository().awsS3DocumentConfig.region()).isEqualTo(
                  "us-east-1");
            assertThat(referencedDocument.documentRepository().awsS3DocumentConfig.bucket()).isEqualTo(
                  "bucket");
            assertThat(referencedDocument.documentRepository().awsS3DocumentConfig.bucketBasePath()).isEqualTo(
                  Path.of("path"));

            assertThat(referencedDocument.documentId()).isEqualTo(DocumentPath.of("key"));
         }
      }

      @Test
      public void referencedDocumentRepositoryWhenAwsS3DocumentUri() throws Exception {
         try (var referencedDocument = AwsS3DocumentRepository.referencedDocument(AwsS3DocumentUri.ofDocumentUri(
               "s3://bucket/key?failFastIfMissingBucket=false",
               Path.of("")))) {
            assertThat(referencedDocument.documentRepository().awsS3DocumentConfig.region()).isEqualTo(
                  "us-east-1");
            assertThat(referencedDocument.documentRepository().awsS3DocumentConfig.bucket()).isEqualTo(
                  "bucket");
            assertThat(referencedDocument.documentRepository().awsS3DocumentConfig.bucketBasePath()).isEqualTo(
                  Path.of(""));

            assertThat(referencedDocument.documentId()).isEqualTo(DocumentPath.of("key"));
         }

         try (var referencedDocument = AwsS3DocumentRepository.referencedDocument(AwsS3DocumentUri.ofDocumentUri(
               "s3://bucket/path/key?failFastIfMissingBucket=false&bucketBasePath=path",
               Path.of("")))) {
            assertThat(referencedDocument.documentRepository().awsS3DocumentConfig.region()).isEqualTo(
                  "us-east-1");
            assertThat(referencedDocument.documentRepository().awsS3DocumentConfig.bucket()).isEqualTo(
                  "bucket");
            assertThat(referencedDocument.documentRepository().awsS3DocumentConfig.bucketBasePath()).isEqualTo(
                  Path.of("path"));

            assertThat(referencedDocument.documentId()).isEqualTo(DocumentPath.of("key"));
         }

         try (var referencedDocument = AwsS3DocumentRepository.referencedDocument(AwsS3DocumentUri.ofDocumentUri(
               "s3://bucket/path/key?failFastIfMissingBucket=false",
               Path.of("path")))) {
            assertThat(referencedDocument.documentRepository().awsS3DocumentConfig.region()).isEqualTo(
                  "us-east-1");
            assertThat(referencedDocument.documentRepository().awsS3DocumentConfig.bucket()).isEqualTo(
                  "bucket");
            assertThat(referencedDocument.documentRepository().awsS3DocumentConfig.bucketBasePath()).isEqualTo(
                  Path.of("path"));

            assertThat(referencedDocument.documentId()).isEqualTo(DocumentPath.of("key"));
         }
      }
   }

}
