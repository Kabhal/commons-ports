/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.awss3;

import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.mimeType;
import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.parseMimeType;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_OCTET_STREAM;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_PDF;
import static com.tinubu.commons.lang.util.CollectionUtils.entry;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig.S3UriFormat.PATH_STYLE;
import static com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig.S3UriFormat.S3;
import static com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig.S3UriFormat.VIRTUAL_HOST_STYLE;
import static com.tinubu.commons.ports.document.awss3.AwsS3ExportUriOptions.defaultParameters;
import static com.tinubu.commons.ports.document.awss3.AwsS3ExportUriOptions.noParameters;
import static com.tinubu.commons.ports.document.awss3.S3Utils.tryCreateBucket;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.apache.commons.lang3.StringUtils.removeEnd;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.io.IOException;
import java.io.Writer;
import java.net.URI;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.UnaryOperator;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.event.Level;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.output.Slf4jLogConsumer;
import org.testcontainers.images.PullPolicy;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig.AwsS3DocumentConfigBuilder;
import com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig.HttpClientConfig;
import com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig.S3ListingContentMode;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentMetadata;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata.OpenDocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.testsuite.CommonDocumentRepositoryTest;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ZipArchiver;

import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.metrics.LoggingMetricPublisher;
import software.amazon.awssdk.metrics.LoggingMetricPublisher.Format;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.S3ClientBuilder;
import software.amazon.awssdk.services.s3.S3Configuration;
import software.amazon.awssdk.services.s3.S3Utilities;
import software.amazon.awssdk.services.s3.model.GetUrlRequest;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import software.amazon.awssdk.services.s3.model.Tag;
import software.amazon.awssdk.services.s3.model.Tagging;

@Testcontainers(disabledWithoutDocker = true)
public class AwsS3DocumentRepositoryTest {
   private static final Logger log = LoggerFactory.getLogger(AwsS3DocumentRepositoryTest.class);

   private static final DockerImageName LOCALSTACK_DOCKER_IMAGE = DockerImageName
         .parse(
               "localstack/localstack@sha256:b5c082a6d78d49fc4a102841648a8adeab4895f7d9a4ad042d7d485aed2da10d")
         .withTag("3.3.0");
   private static final String TEST_BUCKET = "test-bucket";
   /** @implNote Region must be set to {@code us-east-1} when using custom endpoint with S3. */
   private static final String TEST_REGION = "us-east-1";
   private static final int CHUNK_SIZE = 5 * 1024 * 1024;
   /** AWS S3 Metric publisher. Configure logback-test.xml to enable metric logging. */
   private static final LoggingMetricPublisher METRIC_PUBLISHER =
         LoggingMetricPublisher.create(Level.DEBUG, Format.PRETTY);

   @Container
   private static final GenericContainer<?> localstack = new GenericContainer<>(LOCALSTACK_DOCKER_IMAGE)
         .withImagePullPolicy(PullPolicy.alwaysPull())
         .withExposedPorts(4566)
         .withLogConsumer(new Slf4jLogConsumer(log));

   @AfterAll
   public static void afterAll() {
      localstack.close();
   }

   protected static URI s3Url() {
      return URI.create("http://" + s3UrlHost() + ":" + s3UrlPort());
   }

   protected static String s3UrlHost() {
      return localstack.getContainerIpAddress();
   }

   protected static Integer s3UrlPort() {
      return localstack.getMappedPort(4566);
   }

   private static AwsCredentialsProvider credentialsProvider() {
      return StaticCredentialsProvider.create(AwsBasicCredentials.create("test", "test"));
   }

   private abstract class AbstractAwsS3DocumentRepositoryTest extends CommonDocumentRepositoryTest {

      private AwsS3DocumentRepository documentRepository;

      @BeforeEach
      public void configureDocumentRepository() {
         assertThat(localstack.isRunning()).isTrue();

         documentRepository = new AwsS3DocumentRepository(awsS3Config().build(),
                                                          metricPublisher().andThen(awsS3ClientConfigurer()));
         documentRepository.deleteBucket(TEST_BUCKET);
      }

      protected Consumer<S3ClientBuilder> awsS3ClientConfigurer() {
         return __ -> { };
      }

      protected Consumer<S3ClientBuilder> metricPublisher() {
         return builder -> builder.overrideConfiguration(b -> b.addMetricPublisher(METRIC_PUBLISHER));
      }

      @AfterEach
      public void closeDocumentRepository() {
         this.documentRepository.close();
      }

      protected AwsS3DocumentConfigBuilder awsS3Config() {
         return new AwsS3DocumentConfigBuilder()
               .region(TEST_REGION)
               .pathStyleAccess(true)
               .endpoint(s3Url())
               .bucket(TEST_BUCKET)
               .createIfMissingBucket(true)
               .failFastIfMissingBucket(false)
               .uploadChunkSize(CHUNK_SIZE)
               .credentialsProvider(credentialsProvider())
               .listingContentMode(S3ListingContentMode.FILTERING_COMPLETE);
      }

      protected S3Client buildS3Client(AwsS3DocumentConfig s3Config, boolean createBucket) {
         S3ClientBuilder s3ClientBuilder = S3Client
               .builder()
               .forcePathStyle(s3Config.pathStyleAccess())
               .region(Region.of(s3Config.region()))
               .credentialsProvider(s3Config.credentialsProvider());

         s3Config.endpoint().ifPresent(s3ClientBuilder::endpointOverride);

         S3Client s3Client = s3ClientBuilder.build();

         if (createBucket) {
            tryCreateBucket(s3Client, s3Config.bucket(), true);
         }

         return s3Client;
      }

      protected Tag tag(String key, String value) {
         return Tag.builder().key(key).value(value).build();
      }

      @Override
      protected long chunkSize() {
         return CHUNK_SIZE;
      }

      @Override
      protected DocumentRepository documentRepository() {
         return documentRepository;
      }

      @Override
      protected Document zipTransformer(DocumentPath zipPath, List<Document> documents) {
         return new ZipArchiver(zipPath).compress(documents);
      }

      @Override
      protected Optional<MimeType> defaultContentType() {
         return optional(APPLICATION_OCTET_STREAM);
      }

      @Override
      @Disabled("Not supported")
      public void testOpenDocumentWhenOverwriteAndAppend() { }

      @Override
      @Disabled("Not supported")
      public void testOpenDocumentWhenNotOverwriteAndAppend() { }

      @Override
      protected UnaryOperator<DocumentMetadataBuilder> synchronizeExpectedMetadata(DocumentMetadata actual) {
         return builder -> builder.chain(super.synchronizeExpectedMetadata(actual));
      }

      protected URI testS3CliUri(String bucket, String path, String query) {
         URI uri = URI.create("s3://" + bucket + "/" + (query != null ? "?" + query : ""));

         if (path != null) {
            uri = resolveUri(uri, path);
         }

         return uri;
      }

      protected URI testS3CliUri(String bucket, String path) {
         return testS3CliUri(bucket, null, null);
      }

      protected URI testS3CliUri(String bucket) {
         return testS3CliUri(bucket, null, null);
      }

      protected URI testS3CliUri() {
         return testS3CliUri(TEST_BUCKET);
      }

      protected URI testS3WebUri(String region, String bucket, boolean pathStyle, String path, String query) {
         S3Utilities utils = S3Utilities
               .builder()
               .s3Configuration(S3Configuration.builder().pathStyleAccessEnabled(pathStyle).build())
               .region(Region.of(region))
               .build();
         URI uri = URI.create(utils
                                    .getUrl(GetUrlRequest
                                                  .builder()
                                                  .region(Region.of(region))
                                                  .bucket(bucket)
                                                  .key("/")
                                                  .build())
                                    .toString() + (query != null ? "?" + query : ""));

         if (path != null) {
            uri = resolveUri(uri, path);
         }

         return uri;
      }

      protected URI testS3WebUri(String region, String bucket, boolean pathStyle, String path) {
         return testS3WebUri(region, bucket, pathStyle, path, null);
      }

      protected URI testS3WebUri(String region, String bucket, boolean pathStyle) {
         return testS3WebUri(region, bucket, pathStyle, null, null);
      }

      protected URI testS3WebUri(boolean pathStyle) {
         return testS3WebUri(TEST_REGION, TEST_BUCKET, pathStyle);
      }

      protected URI testS3GlobalUri(String bucket, boolean pathStyle, String path, String query) {
         URI uri = URI.create(testS3WebUri(TEST_REGION, bucket, pathStyle)
                                    .toString()
                                    .replaceAll("s3\\.[^.]+\\.amazonaws.com", "s3.amazonaws.com") + (query
                                                                                                     != null
                                                                                                     ? "?"
                                                                                                       + query
                                                                                                     : ""));
         if (path != null) {
            uri = resolveUri(uri, path);
         }

         return uri;
      }

      protected URI testS3GlobalUri(String bucket, boolean pathStyle, String path) {
         return testS3GlobalUri(bucket, pathStyle, path, null);
      }

      protected URI testS3GlobalUri(String bucket, boolean pathStyle) {
         return testS3GlobalUri(bucket, pathStyle, null, null);
      }

      protected URI testS3GlobalUri(boolean pathStyle) {
         return testS3GlobalUri(TEST_BUCKET, pathStyle);
      }

   }

   @Nested
   public class WhenCreateIfMissingBucket extends AbstractAwsS3DocumentRepositoryTest {

      /**
       * Bucket is deleted after each test to test missing bucket lazy creation in all operations.
       * failFastIfMissingBucket should be set to false for tests to be complete.
       */
      protected AwsS3DocumentConfigBuilder awsS3Config() {
         return super.awsS3Config().createIfMissingBucket(true).failFastIfMissingBucket(false);
      }

      /**
       * Document must be > chunk size for multipart upload to be enabled.
       */
      @Test
      public void testOpenDocumentWhenNoMultipart() throws Exception {
         Path documentPath = path("path/pathfile1.pdf");
         DocumentPath testDocument = DocumentPath.of(documentPath);

         try {
            OpenDocumentMetadata metadata = new OpenDocumentMetadataBuilder()
                  .documentPath("path/alternative.pdf")
                  .contentType(mimeType(APPLICATION_PDF, UTF_8))
                  .build();

            String content = StringUtils.repeat('0', 10);
            try (var newDocument = documentRepository()
                  .openDocument(testDocument, true, false, metadata)
                  .orElseThrow()
                  .closeableContent()) {
               writeContent(newDocument, content);
            }

            Optional<Document> document = documentRepository().findDocumentById(testDocument);

            assertThat(document)
                  .usingValueComparator(contentAgnosticDocumentComparator())
                  .hasValue(new DocumentBuilder()
                                  .<DocumentBuilder>reconstitute()
                                  .documentId(testDocument)
                                  .metadata(new DocumentMetadataBuilder()
                                                  .<DocumentMetadataBuilder>reconstitute()
                                                  .documentPath(Path.of("path/alternative.pdf"))
                                                  .contentSize(isSupportingContentLength() ? 10L : null)
                                                  .contentType(parseMimeType("application/pdf;charset=UTF-8"))
                                                  .chain(synchronizeExpectedMetadata(document
                                                                                           .map(Document::metadata)
                                                                                           .orElse(null)))
                                                  .build())
                                  .loadedContent(content, UTF_8)
                                  .build());
         } finally {
            deleteDocuments(documentPath);
         }
      }

      /**
       * Document must be > chunk size for multipart upload to be enabled.
       */
      @Test
      public void testOpenDocumentWhenMultipart() throws Exception {
         Path documentPath = path("path/pathfile1.pdf");
         DocumentPath testDocument = DocumentPath.of(documentPath);

         try {
            OpenDocumentMetadata metadata = new OpenDocumentMetadataBuilder()
                  .documentPath("path/alternative.pdf")
                  .contentType(mimeType(APPLICATION_PDF, UTF_8))
                  .build();

            String content = StringUtils.repeat('0', 2 * CHUNK_SIZE + 1);
            try (var newDocument = documentRepository()
                  .openDocument(testDocument, true, false, metadata)
                  .orElseThrow()
                  .closeableContent()) {
               writeContent(newDocument, content);
            }

            Optional<Document> document = documentRepository().findDocumentById(testDocument);

            assertThat(document)
                  .usingValueComparator(contentAgnosticDocumentComparator())
                  .hasValue(new DocumentBuilder()
                                  .<DocumentBuilder>reconstitute()
                                  .documentId(testDocument)
                                  .metadata(new DocumentMetadataBuilder()
                                                  .<DocumentMetadataBuilder>reconstitute()
                                                  .documentPath(Path.of("path/alternative.pdf"))
                                                  .contentSize(isSupportingContentLength() ? 2 * CHUNK_SIZE
                                                                                             + 1L : null)
                                                  .contentType(parseMimeType("application/pdf;charset=UTF-8"))
                                                  .chain(synchronizeExpectedMetadata(document
                                                                                           .map(Document::metadata)
                                                                                           .orElse(null)))
                                                  .build())
                                  .loadedContent(content, UTF_8)
                                  .build());
         } finally {
            deleteDocuments(documentPath);
         }
      }
   }

   @Nested
   public class WhenNominalBucketBasePath extends AbstractAwsS3DocumentRepositoryTest {
      protected AwsS3DocumentConfigBuilder awsS3Config() {
         return super.awsS3Config().bucketBasePath(Path.of("basePath"));
      }

      @Nested
      public class DocumentRepositoryUriAdapter {

         @Test
         public void testSupportsDocumentUriWhenNominal() {
            assertThat(documentRepository().supportsUri(documentUri(testS3CliUri().resolve(
                  "basePath/path/test.txt")))).isTrue();

            assertThat(documentRepository().supportsUri(documentUri(testS3WebUri(false).resolve(
                  "basePath/path/test.txt")))).isTrue();
            assertThat(documentRepository().supportsUri(documentUri(testS3WebUri(true).resolve(
                  "basePath/path/test.txt")))).isTrue();
            assertThat(documentRepository().supportsUri(documentUri(testS3GlobalUri(false).resolve(
                  "basePath/path/test.txt")))).isTrue();
            assertThat(documentRepository().supportsUri(documentUri(testS3GlobalUri(true).resolve(
                  "basePath/path/test.txt")))).isTrue();
         }

         @Test
         public void testSupportsDocumentUriWhenParameters() {
            assertThat(documentRepository().supportsUri(documentUri(testS3CliUri().resolve(
                  "basePath/path/test.txt?bucketBasePath=basePath")))).isTrue();
            assertThatThrownBy(() -> documentRepository().supportsUri(documentUri(testS3CliUri().resolve(
                  "basePath/path/test.txt?bucketBasePath=otherPath"))))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 's3://test-bucket/basePath/path/test.txt?bucketBasePath=otherPath' URI: 'bucketBasePath=otherPath' parameter must start but not be equal to URI 'basePath/path/test.txt' path");
            assertThatThrownBy(() -> documentRepository().supportsUri(documentUri(testS3CliUri().resolve(
                  "basePath/path/test.txt?bucketBasePath"))))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 's3://test-bucket/basePath/path/test.txt?bucketBasePath' URI: 'bucketBasePath=true' parameter must start but not be equal to URI 'basePath/path/test.txt' path");
            assertThat(documentRepository().supportsUri(documentUri(testS3CliUri().resolve(
                  "basePath/path/test.txt?otherParameter=otherValue")))).isTrue();
         }

         @Test
         public void testSupportsDocumentUriWhenBadRegion() {
            assertThat(documentRepository().supportsUri(documentUri(testS3WebUri("eu-west-2",
                                                                                 TEST_BUCKET,
                                                                                 false).resolve(
                  "basePath/path/test.txt")))).isFalse();
            assertThat(documentRepository().supportsUri(documentUri(testS3WebUri("eu-west-2",
                                                                                 TEST_BUCKET,
                                                                                 true).resolve(
                  "basePath/path/test.txt")))).isFalse();
         }

         @Test
         public void testSupportsDocumentUriWhenBadBucket() {
            assertThat(documentRepository().supportsUri(documentUri(testS3CliUri("other").resolve(
                  "basePath/path/test.txt")))).isFalse();

            assertThat(documentRepository().supportsUri(documentUri(testS3WebUri(TEST_REGION,
                                                                                 "other",
                                                                                 false).resolve(
                  "basePath/path/test.txt")))).isFalse();
            assertThat(documentRepository().supportsUri(documentUri(testS3WebUri(TEST_REGION,
                                                                                 "other",
                                                                                 true).resolve(
                  "basePath/path/test.txt")))).isFalse();
            assertThat(documentRepository().supportsUri(documentUri(testS3GlobalUri("other", false).resolve(
                  "basePath/path/test.txt")))).isFalse();
            assertThat(documentRepository().supportsUri(documentUri(testS3GlobalUri("other", true).resolve(
                  "basePath/path/test.txt")))).isFalse();
         }

         @Test
         public void testSupportsDocumentUriWhenRepositoryUri() {
            assertThatThrownBy(() -> documentRepository().supportsUri(documentUri(testS3CliUri().resolve(
                  "basePath"))))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessageContaining(
                        "'bucketBasePath=basePath' parameter must start but not be equal to URI 'basePath' path");
            assertThatThrownBy(() -> documentRepository().supportsUri(documentUri(testS3WebUri(false).resolve(
                  "basePath"))))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessageContaining(
                        "'bucketBasePath=basePath' parameter must start but not be equal to URI 'basePath' path");
            assertThatThrownBy(() -> documentRepository().supportsUri(documentUri(testS3WebUri(true).resolve(
                  "basePath"))))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessageContaining(
                        "'bucketBasePath=basePath' parameter must start but not be equal to URI 'basePath' path");
            assertThatThrownBy(() -> documentRepository().supportsUri(documentUri(testS3GlobalUri(false).resolve(
                  "basePath"))))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessageContaining(
                        "'bucketBasePath=basePath' parameter must start but not be equal to URI 'basePath' path");
            assertThatThrownBy(() -> documentRepository().supportsUri(documentUri(testS3GlobalUri(true).resolve(
                  "basePath"))))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessageContaining(
                        "'bucketBasePath=basePath' parameter must start but not be equal to URI 'basePath' path");
         }

         @Test
         public void testSupportsDocumentRepositoryUriWhenAlternativeForms() {
            assertThatThrownBy(() -> documentRepository().supportsUri(documentUri(testS3CliUri().resolve(
                  "basePath/path/test.txt#fragment"))))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessageContainingAll("Invalid 's3://test-bucket/basePath/path/test.txt#fragment' URI",
                                           "must not have a fragment");
            assertThat(documentRepository().supportsUri(documentUri(testS3CliUri().resolve(
                  "basePath/path/test.txt?query")))).isTrue();
            assertThatThrownBy(() -> documentRepository().supportsUri(documentUri("s3://user@"
                                                                                  + TEST_BUCKET
                                                                                  + "/basePath/path/test.txt")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessageContainingAll("Invalid 's3://user@test-bucket/basePath/path/test.txt' URI",
                                           "must not have user info");

            assertThatThrownBy(() -> documentRepository().supportsUri(documentUri(testS3WebUri(false).resolve(
                  "basePath/path/test.txt#fragment"))))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessageContainingAll(
                        "Invalid 'https://test-bucket.s3.amazonaws.com/basePath/path/test.txt#fragment' URI",
                        "must not have a fragment");
            assertThatThrownBy(() -> documentRepository().supportsUri(documentUri(testS3WebUri(true).resolve(
                  "basePath/path/test.txt#fragment"))))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessageContainingAll(
                        "Invalid 'https://s3.amazonaws.com/test-bucket/basePath/path/test.txt#fragment' URI",
                        "must not have a fragment");
            assertThat(documentRepository().supportsUri(documentUri(testS3WebUri(false).resolve(
                  "basePath/path/test.txt?query")))).isTrue();
            assertThat(documentRepository().supportsUri(documentUri(testS3WebUri(true).resolve(
                  "basePath/path/test.txt?query")))).isTrue();
            assertThatThrownBy(() -> documentRepository().supportsUri(documentUri(
                  "http://user@s3.amazonaws.com/" + TEST_BUCKET + "/basePath/path/test.txt")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessageContainingAll(
                        "Invalid 'http://user@s3.amazonaws.com/test-bucket/basePath/path/test.txt' URI",
                        "must not have user info");
            assertThat(documentRepository().supportsUri(documentUri("https://"
                                                                    + TEST_BUCKET
                                                                    + "/basePath/path/test.txt"))).isFalse();
         }

         @Test
         public void testSupportsRepositoryUriWhenNominal() {
            assertThat(documentRepository().supportsUri(repositoryUri(testS3CliUri().resolve("basePath")))).isTrue();
            assertThat(documentRepository().supportsUri(repositoryUri(testS3WebUri(false).resolve("basePath")))).isTrue();
            assertThat(documentRepository().supportsUri(repositoryUri(testS3WebUri(true).resolve("basePath")))).isTrue();
            assertThat(documentRepository().supportsUri(repositoryUri(testS3GlobalUri(false).resolve(
                  "basePath")))).isTrue();
            assertThat(documentRepository().supportsUri(repositoryUri(testS3GlobalUri(true).resolve("basePath")))).isTrue();
         }

         @Test
         public void testSupportsRepositoryUriWhenBadRegion() {
            assertThat(documentRepository().supportsUri(repositoryUri(testS3WebUri("eu-west-2",
                                                                                   TEST_BUCKET,
                                                                                   false).resolve("basePath")))).isFalse();
            assertThat(documentRepository().supportsUri(repositoryUri(testS3WebUri("eu-west-2",
                                                                                   TEST_BUCKET,
                                                                                   true).resolve("basePath")))).isFalse();
         }

         @Test
         public void testSupportsRepositoryUriWhenBadBucket() {
            assertThat(documentRepository().supportsUri(repositoryUri(testS3CliUri("other").resolve("basePath")))).isFalse();

            assertThat(documentRepository().supportsUri(repositoryUri(testS3WebUri(TEST_REGION,
                                                                                   "other",
                                                                                   false).resolve("basePath")))).isFalse();
            assertThat(documentRepository().supportsUri(repositoryUri(testS3WebUri(TEST_REGION,
                                                                                   "other",
                                                                                   true).resolve("basePath")))).isFalse();
            assertThat(documentRepository().supportsUri(repositoryUri(testS3GlobalUri("other", false).resolve(
                  "basePath")))).isFalse();
            assertThat(documentRepository().supportsUri(repositoryUri(testS3GlobalUri("other", true).resolve(
                  "basePath")))).isFalse();
         }

         @Test
         public void testSupportsRepositoryUriWhenDocumentUri() {
            assertThat(documentRepository().supportsUri(documentUri(testS3CliUri().resolve(
                  "basePath/path/test.txt")))).isTrue();
            assertThat(documentRepository().supportsUri(documentUri(testS3WebUri(false).resolve(
                  "basePath/path/test.txt")))).isTrue();
            assertThat(documentRepository().supportsUri(documentUri(testS3WebUri(true).resolve(
                  "basePath/path/test.txt")))).isTrue();
            assertThat(documentRepository().supportsUri(documentUri(testS3GlobalUri(false).resolve(
                  "basePath/path/test.txt")))).isTrue();
            assertThat(documentRepository().supportsUri(documentUri(testS3GlobalUri(true).resolve(
                  "basePath/path/test.txt")))).isTrue();
         }

         @Test
         public void testToRepositoryUriWhenNominal() {
            assertThat(documentRepository()
                             .toUri()
                             .toURI()).isEqualTo(testS3WebUri(false).resolve("basePath"));
         }

         @Test
         public void testToRepositoryUriWhenS3Format() {
            assertThat(documentRepository().toUri(noParameters(S3)).toURI()).isEqualTo(testS3CliUri().resolve(
                  "basePath"));
         }

         @Test
         public void testToRepositoryUriWhenVirtualHostedStyleFormat() {
            assertThat(documentRepository().toUri(noParameters(VIRTUAL_HOST_STYLE)).toURI()).isEqualTo(
                  testS3WebUri(false).resolve("basePath"));
         }

         @Test
         public void testToRepositoryUriWhenPathStyleFormat() {
            assertThat(documentRepository().toUri(noParameters(PATH_STYLE)).toURI()).isEqualTo(testS3WebUri(
                  true).resolve("basePath"));
         }

         @Test
         public void testToRepositoryUriWhenParameters() {
            assertThat(documentRepository().toUri(defaultParameters(S3)).toURI()).isEqualTo(testS3CliUri(
                  TEST_BUCKET,
                  "basePath",
                  "pathStyleAccess=true&endpoint="
                  + s3Url()
                  + "&createIfMissingBucket=true&failFastIfMissingBucket=false&listingContentMode=FILTERING_COMPLETE"));
            assertThat(documentRepository().toUri(defaultParameters(VIRTUAL_HOST_STYLE)).toURI()).isEqualTo(
                  testS3WebUri("us-east-1",
                               TEST_BUCKET,
                               false,
                               "basePath",
                               "pathStyleAccess=true&endpoint="
                               + s3Url()
                               + "&createIfMissingBucket=true&failFastIfMissingBucket=false&listingContentMode=FILTERING_COMPLETE"));
            assertThat(documentRepository().toUri(defaultParameters(PATH_STYLE)).toURI()).isEqualTo(
                  testS3WebUri("us-east-1",
                               TEST_BUCKET,
                               true,
                               "basePath",
                               "pathStyleAccess=true&endpoint="
                               + s3Url()
                               + "&createIfMissingBucket=true&failFastIfMissingBucket=false&listingContentMode=FILTERING_COMPLETE"));
         }

         @Test
         public void testToDocumentUriWhenNominal() {
            assertThat(documentRepository()
                             .toUri(DocumentPath.of("file.txt"))
                             .toURI()).isEqualTo(testS3WebUri(false).resolve("basePath/file.txt"));
            assertThat(documentRepository().toUri(DocumentPath.of("path/file.txt")).toURI()).isEqualTo(
                  testS3WebUri(false).resolve("basePath/path/file.txt"));
         }

         @Test
         public void testToDocumentUriWhenS3Format() {
            assertThat(documentRepository()
                             .toUri(DocumentPath.of("file.txt"), noParameters(S3))
                             .toURI()).isEqualTo(testS3CliUri().resolve("basePath/file.txt"));
            assertThat(documentRepository()
                             .toUri(DocumentPath.of("path/file.txt"), noParameters(S3))
                             .toURI()).isEqualTo(testS3CliUri().resolve("basePath/path/file.txt"));
         }

         @Test
         public void testToDocumentUriWhenVirtualHostedStyleFormat() {
            assertThat(documentRepository()
                             .toUri(DocumentPath.of("file.txt"), noParameters(VIRTUAL_HOST_STYLE))
                             .toURI()).isEqualTo(testS3WebUri(false).resolve("basePath/file.txt"));
            assertThat(documentRepository()
                             .toUri(DocumentPath.of("path/file.txt"), noParameters(VIRTUAL_HOST_STYLE))
                             .toURI()).isEqualTo(testS3WebUri(false).resolve("basePath/path/file.txt"));
         }

         @Test
         public void testToDocumentUriWhenPathStyleFormat() {
            assertThat(documentRepository()
                             .toUri(DocumentPath.of("file.txt"), noParameters(PATH_STYLE))
                             .toURI()).isEqualTo(testS3WebUri(true).resolve("basePath/file.txt"));
            assertThat(documentRepository()
                             .toUri(DocumentPath.of("path/file.txt"), noParameters(PATH_STYLE))
                             .toURI()).isEqualTo(testS3WebUri(true).resolve("basePath/path/file.txt"));
         }

         @Test
         public void testFindDocumentByUriWhenNominal() {
            DocumentPath documentPath = DocumentPath.of("pathfile2.txt");

            try {
               createDocuments(documentPath);

               assertThat(documentRepository().findDocumentByUri(documentUri(testS3CliUri().resolve(
                     "basePath/pathfile2.txt")))).hasValueSatisfying(document -> {
                  assertThat(document.documentId()).isEqualTo(DocumentPath.of("pathfile2.txt"));
                  assertThat(document.metadata().documentPath()).isEqualTo(Path.of("pathfile2.txt"));
               });

            } finally {
               deleteDocuments(documentPath);
            }
         }

         @Test
         public void testFindDocumentByUriWhenRepositoryUri() {
            assertThatThrownBy(() -> documentRepository().findDocumentByUri(documentUri(testS3CliUri().resolve(
                  "basePath"))))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 's3://test-bucket/basePath' URI: 'bucketBasePath=basePath' parameter must start but not be equal to URI 'basePath' path");
         }

         @Test
         public void testFindDocumentByUriWhenUnsupportedUri() {
            assertThatThrownBy(() -> documentRepository().findDocumentByUri(documentUri(
                  "unknown://bucket/path")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessageContaining(
                        "'documentUri=DefaultDocumentUri[value=unknown://bucket/path]' must be supported");
            assertThatThrownBy(() -> documentRepository().findDocumentByUri(documentUri(testS3CliUri().resolve(
                  "otherpath"))))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessageContaining(
                        "'documentUri=DefaultDocumentUri[value=s3://test-bucket/otherpath]' must be supported");
         }

      }
   }

   @Nested
   public class WhenEmptyBucketBasePath extends AbstractAwsS3DocumentRepositoryTest {
      protected AwsS3DocumentConfigBuilder awsS3Config() {
         return super.awsS3Config().bucketBasePath(Path.of(""));
      }

      @Nested
      public class DocumentRepositoryUriAdapter {

         @Test
         public void testToDocumentUriWhenNominal() {
            assertThat(documentRepository()
                             .toUri(DocumentPath.of("file.txt"))
                             .toURI()).isEqualTo(testS3WebUri(false).resolve("file.txt"));
            assertThat(documentRepository().toUri(DocumentPath.of("path/file.txt")).toURI()).isEqualTo(
                  testS3WebUri(false).resolve("path/file.txt"));
         }

         @Test
         public void testSupportsDocumentUriWhenNominal() {
            assertThat(documentRepository().supportsUri(documentUri(testS3CliUri().resolve("path/test.txt")))).isTrue();

            assertThat(documentRepository().supportsUri(documentUri(testS3WebUri(false).resolve(
                  "path/test.txt")))).isTrue();
            assertThat(documentRepository().supportsUri(documentUri(testS3WebUri(true).resolve("path/test.txt")))).isTrue();
            assertThat(documentRepository().supportsUri(documentUri(testS3GlobalUri(false).resolve(
                  "path/test.txt")))).isTrue();
            assertThat(documentRepository().supportsUri(documentUri(testS3GlobalUri(true).resolve(
                  "path/test.txt")))).isTrue();
         }

         @Test
         public void testSupportsDocumentUriWhenBadRegion() {
            assertThat(documentRepository().supportsUri(documentUri(testS3WebUri("eu-west-2",
                                                                                 TEST_BUCKET,
                                                                                 false).resolve(
                  "path/test.txt")))).isFalse();
            assertThat(documentRepository().supportsUri(documentUri(testS3WebUri("eu-west-2",
                                                                                 TEST_BUCKET,
                                                                                 true).resolve("path/test.txt")))).isFalse();
         }

         @Test
         public void testSupportsDocumentUriWhenBadBucket() {
            assertThat(documentRepository().supportsUri(documentUri(testS3CliUri("other").resolve(
                  "path/test.txt")))).isFalse();

            assertThat(documentRepository().supportsUri(documentUri(testS3WebUri(TEST_REGION,
                                                                                 "other",
                                                                                 false).resolve(
                  "path/test.txt")))).isFalse();
            assertThat(documentRepository().supportsUri(documentUri(testS3WebUri(TEST_REGION,
                                                                                 "other",
                                                                                 true).resolve("path/test.txt")))).isFalse();
            assertThat(documentRepository().supportsUri(documentUri(testS3GlobalUri("other", false).resolve(
                  "path/test.txt")))).isFalse();
            assertThat(documentRepository().supportsUri(documentUri(testS3GlobalUri("other", true).resolve(
                  "path/test.txt")))).isFalse();
         }

         @Test
         public void testSupportsDocumentUriWhenRepositoryUri() {
            assertThatThrownBy(() -> documentRepository().supportsUri(documentUri(testS3CliUri())))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessageContaining("Missing document id");
            assertThatThrownBy(() -> documentRepository().supportsUri(documentUri(testS3WebUri(false))))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessageContaining("Missing document id");
            assertThatThrownBy(() -> documentRepository().supportsUri(documentUri(testS3WebUri(true))))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessageContaining("Missing document id");
            assertThatThrownBy(() -> documentRepository().supportsUri(documentUri(testS3GlobalUri(false))))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessageContaining("Missing document id");
            assertThatThrownBy(() -> documentRepository().supportsUri(documentUri(testS3GlobalUri(true))))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessageContaining("Missing document id");
         }

         @Test
         public void testSupportsDocumentRepositoryUriWhenAlternativeForms() {
            assertThatThrownBy(() -> documentRepository().supportsUri(documentUri(testS3CliUri().resolve(
                  "path/test.txt#fragment"))))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessageContainingAll("Invalid 's3://test-bucket/path/test.txt#fragment' URI",
                                           "must not have a fragment");
            assertThat(documentRepository().supportsUri(documentUri(testS3CliUri().resolve(
                  "path/test.txt?query")))).isTrue();
            assertThatThrownBy(() -> documentRepository().supportsUri(documentUri("s3://user@"
                                                                                  + TEST_BUCKET
                                                                                  + "/path/test.txt")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessageContainingAll("Invalid 's3://user@test-bucket/path/test.txt' URI",
                                           "must not have user info");

            assertThatThrownBy(() -> documentRepository().supportsUri(documentUri(testS3WebUri(false).resolve(
                  "path/test.txt#fragment"))))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessageContainingAll(
                        "Invalid 'https://test-bucket.s3.amazonaws.com/path/test.txt#fragment' URI",
                        "must not have a fragment");
            assertThatThrownBy(() -> documentRepository().supportsUri(documentUri(testS3WebUri(true).resolve(
                  "path/test.txt#fragment"))))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessageContainingAll(
                        "Invalid 'https://s3.amazonaws.com/test-bucket/path/test.txt#fragment' URI",
                        "must not have a fragment");
            assertThat(documentRepository().supportsUri(documentUri(testS3WebUri(false).resolve(
                  "path/test.txt?query")))).isTrue();
            assertThat(documentRepository().supportsUri(documentUri(testS3WebUri(true).resolve(
                  "path/test.txt?query")))).isTrue();
            assertThatThrownBy(() -> documentRepository().supportsUri(documentUri("http://user@"
                                                                                  + TEST_BUCKET
                                                                                  + "/path/test.txt")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessageContainingAll("Invalid 'http://user@test-bucket/path/test.txt' URI",
                                           "must not have user info");
            assertThat(documentRepository().supportsUri(documentUri("https://"
                                                                    + TEST_BUCKET
                                                                    + "/path/test.txt"))).isFalse();
         }

         @Test
         public void testToRepositoryUriWhenNominal() {
            assertThat(documentRepository()
                             .toUri()
                             .stringValue()).isEqualTo(removeEnd(testS3WebUri(false).toString(), "/"));
         }

         @Test
         public void testSupportsRepositoryUriWhenNominal() {
            assertThat(documentRepository().supportsUri(repositoryUri(testS3CliUri()))).isTrue();
            assertThat(documentRepository().supportsUri(repositoryUri(testS3WebUri(false)))).isTrue();
            assertThat(documentRepository().supportsUri(repositoryUri(testS3WebUri(true)))).isTrue();
            assertThat(documentRepository().supportsUri(repositoryUri(testS3GlobalUri(false)))).isTrue();
            assertThat(documentRepository().supportsUri(repositoryUri(testS3GlobalUri(true)))).isTrue();
         }

         @Test
         public void testSupportsRepositoryUriWhenBadRegion() {
            assertThat(documentRepository().supportsUri(repositoryUri(testS3WebUri("eu-west-2",
                                                                                   TEST_BUCKET,
                                                                                   false)))).isFalse();
            assertThat(documentRepository().supportsUri(repositoryUri(testS3WebUri("eu-west-2",
                                                                                   TEST_BUCKET,
                                                                                   true)))).isFalse();
         }

         @Test
         public void testSupportsRepositoryUriWhenBadBucket() {
            assertThat(documentRepository().supportsUri(repositoryUri(testS3CliUri("other")))).isFalse();

            assertThat(documentRepository().supportsUri(repositoryUri(testS3WebUri(TEST_REGION,
                                                                                   "other",
                                                                                   false)))).isFalse();
            assertThat(documentRepository().supportsUri(repositoryUri(testS3WebUri(TEST_REGION,
                                                                                   "other",
                                                                                   true)))).isFalse();
            assertThat(documentRepository().supportsUri(repositoryUri(testS3GlobalUri("other",
                                                                                      false)))).isFalse();
            assertThat(documentRepository().supportsUri(repositoryUri(testS3GlobalUri("other",
                                                                                      true)))).isFalse();
         }
      }

   }

   @Nested
   public class WhenSlashTerminatedBucketBasePath extends AbstractAwsS3DocumentRepositoryTest {
      protected AwsS3DocumentConfigBuilder awsS3Config() {
         return super.awsS3Config().bucketBasePath(Path.of("basePath/"));
      }
   }

   @Nested
   public class WhenBucketBasePathAndSubPath extends AbstractAwsS3DocumentRepositoryTest {
      protected AwsS3DocumentConfigBuilder awsS3Config() {
         return super.awsS3Config().bucketBasePath(Path.of("basePath/subBasePath"));
      }
   }

   @Nested
   public class WhenMatchTags extends AbstractAwsS3DocumentRepositoryTest {
      protected AwsS3DocumentConfigBuilder awsS3Config() {
         return super.awsS3Config().matchTags(Pattern.compile("tagkey.*"), Pattern.compile("otherkey"));
      }

      @Test
      public void testFindDocumentByIdWhenMatchTags() {
         Path documentPath = path("path/pathfile1.pdf");
         DocumentPath testDocument = DocumentPath.of(documentPath);

         try {

            AwsS3DocumentConfig s3Config = awsS3Config().build();
            S3Client s3Client = buildS3Client(s3Config, true);
            s3Client.putObject(PutObjectRequest
                                     .builder()
                                     .bucket(s3Config.bucket())
                                     .key(documentPath.toString())
                                     .contentLength(0L)
                                     .metadata(map(entry("tag0", "value0"),
                                                   entry("tagkey1", "metadatavalue1"),
                                                   entry("tagkey2", "metadatavalue2")))
                                     .tagging(Tagging
                                                    .builder()
                                                    .tagSet(tag("tag0", "tagvalue0"),
                                                            tag("tagkey1", "tagvalue1"),
                                                            tag("tagkey3", "tagvalue3"),
                                                            tag("extratag1", "extravalue1"))
                                                    .build())
                                     .build(), RequestBody.empty());

            assertThat(documentRepository().findDocumentById(testDocument)).hasValueSatisfying(document -> {
               assertThat(document.metadata().attributes()).isEqualTo(map(entry("tag0", "value0"),
                                                                          entry("tagkey1", "tagvalue1"),
                                                                          entry("tagkey2", "metadatavalue2"),
                                                                          entry("tagkey3", "tagvalue3")));
            });

         } finally {
            deleteDocuments(documentPath);
         }
      }

   }

   @Nested
   public class WhenLoadAllTags extends AbstractAwsS3DocumentRepositoryTest {
      protected AwsS3DocumentConfigBuilder awsS3Config() {
         return super
               .awsS3Config()
               .loadAllTags(true)
               .matchTags(Pattern.compile("tagkey.*"), Pattern.compile("otherkey"));
      }

      @Test
      public void testFindDocumentByIdWhenMatchTags() {
         Path documentPath = path("path/pathfile1.pdf");
         DocumentPath testDocument = DocumentPath.of(documentPath);

         try {

            AwsS3DocumentConfig s3Config = awsS3Config().build();
            S3Client s3Client = buildS3Client(s3Config, true);
            s3Client.putObject(PutObjectRequest
                                     .builder()
                                     .bucket(s3Config.bucket())
                                     .key(documentPath.toString())
                                     .contentLength(0L)
                                     .metadata(map(entry("tag0", "value0"),
                                                   entry("tagkey1", "metadatavalue1"),
                                                   entry("tagkey2", "metadatavalue2")))
                                     .tagging(Tagging
                                                    .builder()
                                                    .tagSet(tag("tag0", "tagvalue0"),
                                                            tag("tagkey1", "tagvalue1"),
                                                            tag("tagkey3", "tagvalue3"),
                                                            tag("extratag1", "extravalue1"))
                                                    .build())
                                     .build(), RequestBody.empty());

            assertThat(documentRepository().findDocumentById(testDocument)).hasValueSatisfying(document -> {
               assertThat(document.metadata().attributes()).isEqualTo(map(entry("tag0", "tagvalue0"),
                                                                          entry("tagkey1", "tagvalue1"),
                                                                          entry("tagkey2", "metadatavalue2"),
                                                                          entry("tagkey3", "tagvalue3"),
                                                                          entry("extratag1", "extravalue1")));
            });

         } finally {
            deleteDocuments(documentPath);
         }
      }

   }

   @Nested
   public class WhenFailFast {

      private AwsS3DocumentRepository documentRepository;

      @BeforeEach
      public void configureDocumentRepository() {
         assertThat(localstack.isRunning()).isTrue();

         AwsS3DocumentConfig config = new AwsS3DocumentConfigBuilder()
               .region(TEST_REGION)
               .endpoint(s3Url())
               .bucket(TEST_BUCKET)
               .pathStyleAccess(true)
               .createIfMissingBucket(true)
               .failFastIfMissingBucket(true)
               .credentialsProvider(credentialsProvider())
               .build();

         documentRepository = new AwsS3DocumentRepository(config);
         documentRepository.deleteBucket(TEST_BUCKET);
      }

      @Test
      public void testFailFastWhenMissingBucket() {
         AwsS3DocumentConfigBuilder config = new AwsS3DocumentConfigBuilder()
               .region(TEST_REGION)
               .endpoint(s3Url())
               .bucket(TEST_BUCKET)
               .pathStyleAccess(true)
               .failFastIfMissingBucket(true)
               .uploadChunkSize(CHUNK_SIZE)
               .credentialsProvider(credentialsProvider());

         assertThatExceptionOfType(DocumentAccessException.class)
               .isThrownBy(() -> new AwsS3DocumentRepository(config.createIfMissingBucket(false).build()))
               .withMessage("Can't create 'test-bucket' missing bucket");

         new AwsS3DocumentRepository(config.createIfMissingBucket(true).build());
      }

   }

   @Nested
   public class WhenMissingBucket {

      private AwsS3DocumentRepository documentRepository;

      @BeforeEach
      public void configureDocumentRepository() {
         assertThat(localstack.isRunning()).isTrue();

         documentRepository = new AwsS3DocumentRepository(awsS3Config());
         documentRepository.deleteBucket(TEST_BUCKET);
      }

      private AwsS3DocumentConfig awsS3Config() {
         return new AwsS3DocumentConfigBuilder()
               .region(TEST_REGION)
               .endpoint(s3Url())
               .bucket(TEST_BUCKET)
               .pathStyleAccess(true)
               .createIfMissingBucket(false)
               .failFastIfMissingBucket(false)
               .uploadChunkSize(CHUNK_SIZE)
               .credentialsProvider(credentialsProvider())
               .build();
      }

      @Test
      public void testOpenDocumentWithNoMultipartWhenMissingBucket() {
         assertThatExceptionOfType(DocumentAccessException.class)
               .isThrownBy(() -> documentRepository
                     .openDocument(DocumentPath.of("file.txt"),
                                   false,
                                   false,
                                   new OpenDocumentMetadataBuilder().contentEncoding(UTF_8).build())
                     .ifPresent(document -> {
                        try (Writer writer = document.content().writerContent()) {
                           writer.write("content");
                        } catch (IOException e) {
                           throw new IllegalStateException(e);
                        }
                     }))
               .withMessage("Can't create 'test-bucket' missing bucket");
      }

      /**
       * Document must be > chunk size for multipart upload to be enabled.
       */
      @Test
      public void testOpenDocumentWithMultipartWhenMissingBucket() {
         assertThatExceptionOfType(DocumentAccessException.class)
               .isThrownBy(() -> documentRepository
                     .openDocument(DocumentPath.of("file.txt"),
                                   false,
                                   false,
                                   new OpenDocumentMetadataBuilder().contentEncoding(UTF_8).build())
                     .ifPresent(document -> {
                        try (Writer writer = document.content().writerContent()) {
                           String content = StringUtils.repeat('0', 2 * CHUNK_SIZE + 1);
                           writer.write(content);
                        } catch (IOException e) {
                           throw new IllegalStateException(e);
                        }
                     }))
               .withMessage("Can't create 'test-bucket' missing bucket");
      }

      @Test
      public void testSaveDocumentWithNoMultipartWhenMissingBucket() {
         assertThatExceptionOfType(DocumentAccessException.class)
               .isThrownBy(() -> documentRepository.saveDocument(new DocumentBuilder()
                                                                       .documentId(DocumentPath.of("file.txt"))
                                                                       .loadedContent("content", UTF_8)
                                                                       .build(), false))
               .withMessage("Can't create 'test-bucket' missing bucket");
      }

      /**
       * Document must be > chunk size for multipart upload to be enabled.
       */
      @Test
      public void testSaveDocumentWithMultipartWhenMissingBucket() {
         String content = StringUtils.repeat('0', 2 * CHUNK_SIZE + 1);
         assertThatExceptionOfType(DocumentAccessException.class)
               .isThrownBy(() -> documentRepository.saveDocument(new DocumentBuilder()
                                                                       .documentId(DocumentPath.of("file.txt"))
                                                                       .streamContent(content, UTF_8)
                                                                       .build(), false))
               .withMessage("Can't create 'test-bucket' missing bucket");
      }
   }

   @Nested
   public class WhenOneMaxConnections extends AbstractAwsS3DocumentRepositoryTest {
      protected AwsS3DocumentConfigBuilder awsS3Config() {
         return super
               .awsS3Config()
               .httpClientConfig(new HttpClientConfig.Builder().maxConnections(1).build());
      }
   }

}
