/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.awss3;

import static com.tinubu.commons.ddd2.uri.DefaultUriRestrictions.noRestrictions;
import static com.tinubu.commons.ddd2.uri.Uri.uri;
import static com.tinubu.commons.lang.util.CheckedPredicate.alwaysFalse;
import static com.tinubu.commons.lang.util.CheckedPredicate.alwaysTrue;
import static com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig.S3ListingContentMode.FILTERING_COMPLETE;
import static com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig.S3UriFormat.PATH_STYLE;
import static com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig.S3UriFormat.S3;
import static com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig.S3UriFormat.VIRTUAL_HOST_STYLE;
import static com.tinubu.commons.ports.document.awss3.AwsS3RepositoryUri.Parameters.CREATE_IF_MISSING_BUCKET;
import static com.tinubu.commons.ports.document.awss3.AwsS3RepositoryUri.Parameters.ENDPOINT;
import static com.tinubu.commons.ports.document.awss3.AwsS3RepositoryUri.Parameters.FAIL_FAST_IF_MISSING_BUCKET;
import static com.tinubu.commons.ports.document.awss3.AwsS3RepositoryUri.Parameters.LISTING_CONTENT_MODE;
import static com.tinubu.commons.ports.document.awss3.AwsS3RepositoryUri.Parameters.LISTING_MAX_KEYS;
import static com.tinubu.commons.ports.document.awss3.AwsS3RepositoryUri.Parameters.LOAD_ALL_TAGS;
import static com.tinubu.commons.ports.document.awss3.AwsS3RepositoryUri.Parameters.MATCH_TAGS;
import static com.tinubu.commons.ports.document.awss3.AwsS3RepositoryUri.Parameters.PATH_STYLE_ACCESS;
import static com.tinubu.commons.ports.document.awss3.AwsS3RepositoryUri.Parameters.REGION;
import static com.tinubu.commons.ports.document.awss3.AwsS3RepositoryUri.Parameters.UPLOAD_CHUNK_SIZE;
import static com.tinubu.commons.ports.document.awss3.AwsS3RepositoryUri.Parameters.URI_GENERATION_FORMAT;
import static com.tinubu.commons.ports.document.awss3.Constants.DEFAULT_CREATE_IF_MISSING_BUCKET;
import static com.tinubu.commons.ports.document.awss3.Constants.DEFAULT_FAIL_FAST_IF_MISSING_BUCKET;
import static com.tinubu.commons.ports.document.awss3.Constants.DEFAULT_LISTING_CONTENT_MODE;
import static com.tinubu.commons.ports.document.awss3.Constants.DEFAULT_LISTING_MAX_KEYS;
import static com.tinubu.commons.ports.document.awss3.Constants.DEFAULT_LOAD_ALL_TAGS;
import static com.tinubu.commons.ports.document.awss3.Constants.DEFAULT_PATH_STYLE_ACCESS;
import static com.tinubu.commons.ports.document.awss3.Constants.DEFAULT_UPLOAD_CHUNK_SIZE;
import static com.tinubu.commons.ports.document.awss3.Constants.DEFAULT_URI_GENERATION_FORMAT;
import static com.tinubu.commons.ports.document.awss3.S3Utils.defaultRegion;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.defaultParameters;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.noParameters;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.sensitiveParameters;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Comparator.comparing;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.net.URI;
import java.net.URLEncoder;
import java.nio.file.Path;
import java.util.regex.Pattern;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig.AwsS3DocumentConfigBuilder;
import com.tinubu.commons.ports.document.awss3.BaseAwsS3Test.DefaultUriFormatProvider;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.RepositoryUri;

public class AwsS3RepositoryUriTest {

   static {
      Assertions.setMaxStackTraceElementsDisplayed(50);
   }

   @Nested
   public class OfUriWhenURI {

      @Test
      public void ofUriWhenNominal() {
         assertThat(AwsS3RepositoryUri.ofRepositoryUri(uri("s3://bucket"))).satisfies(uri -> {
            assertThat(uri.region()).isEqualTo("us-east-1");
            assertThat(uri.bucket()).isEqualTo("bucket");
            assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
         });
         assertThat(AwsS3RepositoryUri.ofRepositoryUri("s3://bucket")).satisfies(uri -> {
            assertThat(uri.region()).isEqualTo("us-east-1");
            assertThat(uri.bucket()).isEqualTo("bucket");
            assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
         });
      }

      @Test
      public void ofUriWhenUri() {
         assertThat(AwsS3RepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(Uri.ofUri(
               "s3://bucket/path?bucketBasePath=path")))).satisfies(uri -> {
            assertThat(uri.region()).isEqualTo("us-east-1");
            assertThat(uri.bucket()).isEqualTo("bucket");
            assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path"));
         });
         assertThat(AwsS3RepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri(Uri.ofUri(
               "s3://bucket/path/document?bucketBasePath=path")))).satisfies(uri -> {
            assertThat(uri.region()).isEqualTo("us-east-1");
            assertThat(uri.bucket()).isEqualTo("bucket");
            assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path"));
         });
         assertThat(AwsS3RepositoryUri.ofRepositoryUri(Uri.ofUri("s3://bucket/path?bucketBasePath=path"))).satisfies(
               uri -> {
                  assertThat(uri.region()).isEqualTo("us-east-1");
                  assertThat(uri.bucket()).isEqualTo("bucket");
                  assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path"));
               });
      }

      @Test
      public void ofUriWhenAwsS3CliUri() {
         assertThat(AwsS3RepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(AwsS3CliUri.ofUri(
               "s3://bucket/path?bucketBasePath=path",
               noRestrictions())))).satisfies(uri -> {
            assertThat(uri.region()).isEqualTo("us-east-1");
            assertThat(uri.bucket()).isEqualTo("bucket");
            assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path"));
         });
         assertThat(AwsS3RepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri(AwsS3CliUri.ofUri(
               "s3://bucket/path/document?bucketBasePath=path",
               noRestrictions())))).satisfies(uri -> {
            assertThat(uri.region()).isEqualTo("us-east-1");
            assertThat(uri.bucket()).isEqualTo("bucket");
            assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path"));
         });
         assertThat(AwsS3RepositoryUri.ofRepositoryUri(AwsS3CliUri.ofUri(
               "s3://bucket/path?bucketBasePath=path",
               noRestrictions()))).satisfies(uri -> {
            assertThat(uri.region()).isEqualTo("us-east-1");
            assertThat(uri.bucket()).isEqualTo("bucket");
            assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path"));
         });
      }

      @Test
      public void ofUriWhenAwsS3HttpUri() {
         assertThat(AwsS3RepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(AwsS3HttpUri.ofUri(
               "https://bucket.s3.us-east-1.amazonaws.com/path?bucketBasePath=path",
               noRestrictions())))).satisfies(uri -> {
            assertThat(uri.region()).isEqualTo("us-east-1");
            assertThat(uri.bucket()).isEqualTo("bucket");
            assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path"));
         });
         assertThat(AwsS3RepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri(AwsS3HttpUri.ofUri(
               "https://bucket.s3.us-east-1.amazonaws.com/path/document?bucketBasePath=path",
               noRestrictions())))).satisfies(uri -> {
            assertThat(uri.region()).isEqualTo("us-east-1");
            assertThat(uri.bucket()).isEqualTo("bucket");
            assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path"));
         });
         assertThat(AwsS3RepositoryUri.ofRepositoryUri(AwsS3HttpUri.ofUri(
               "https://bucket.s3.us-east-1.amazonaws.com/path?bucketBasePath=path",
               noRestrictions()))).satisfies(uri -> {
            assertThat(uri.region()).isEqualTo("us-east-1");
            assertThat(uri.bucket()).isEqualTo("bucket");
            assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path"));
         });
      }

      @Test
      public void ofUriWhenUserInfos() {
         assertThatThrownBy(() -> AwsS3RepositoryUri.ofRepositoryUri(uri("s3://user:password@bucket")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessageContainingAll("Invalid 's3://user:password@bucket' URI", "must not have user info");
         assertThatThrownBy(() -> AwsS3RepositoryUri.ofRepositoryUri(uri(
               "https://user:password@bucket.s3.amazonaws.com")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessageContainingAll("Invalid 'https://user:password@bucket.s3.amazonaws.com' URI",
                                        "must not have user info");
         assertThatThrownBy(() -> AwsS3RepositoryUri.ofRepositoryUri(uri(
               "https://user:password@s3.amazonaws.com/bucket")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessageContainingAll("Invalid 'https://user:password@s3.amazonaws.com/bucket' URI",
                                        "must not have user info");
      }

      @Test
      public void ofUriWhenBadParameters() {
         assertThatThrownBy(() -> AwsS3RepositoryUri.ofRepositoryUri((URI) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
         assertThatThrownBy(() -> AwsS3RepositoryUri.ofRepositoryUri((String) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
         assertThatThrownBy(() -> AwsS3RepositoryUri.ofRepositoryUri((RepositoryUri) null, Path.of("")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
         assertThatThrownBy(() -> AwsS3RepositoryUri.ofRepositoryUri(AwsS3RepositoryUri.ofRepositoryUri(
               "s3://bucket"), null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultBucketBasePath' must not be null");
      }

      @Test
      public void ofUriWhenNormalized() {
         assertThat(AwsS3RepositoryUri.ofRepositoryUri(uri("s3://bucket/path/../."))).satisfies(uri -> {
            assertThat(uri.region()).isEqualTo("us-east-1");
            assertThat(uri.bucket()).isEqualTo("bucket");
            assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
         });
         assertThat(AwsS3RepositoryUri.ofRepositoryUri("s3://bucket/path/../.")).satisfies(uri -> {
            assertThat(uri.region()).isEqualTo("us-east-1");
            assertThat(uri.bucket()).isEqualTo("bucket");
            assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
         });
         assertThat(AwsS3RepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri("s3://bucket/path/../."),
                                                       Path.of(""))).satisfies(uri -> {
            assertThat(uri.region()).isEqualTo("us-east-1");
            assertThat(uri.bucket()).isEqualTo("bucket");
            assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
         });
         assertThatThrownBy(() -> AwsS3RepositoryUri.ofRepositoryUri(uri("s3://bucket/../path")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessageContainingAll("Invalid 's3://bucket/../path' URI", "must not have traversal paths");
      }

      @Test
      public void ofUriWhenInvalidUriSyntax() {
         assertThatThrownBy(() -> AwsS3RepositoryUri.ofRepositoryUri("git@git.com:user/repo.git"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'git@git.com:user/repo.git' URI: Illegal character in scheme name at index 3: git@git.com:user/repo.git");
         assertThatThrownBy(() -> AwsS3RepositoryUri.ofRepositoryUri("git@git.com:user/repo.git"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'git@git.com:user/repo.git' URI: Illegal character in scheme name at index 3: git@git.com:user/repo.git");
      }

      @Test
      public void ofUriWhenIncompatibleUri() {
         assertThatThrownBy(() -> AwsS3RepositoryUri.ofRepositoryUri(uri("unknown://bucket/path")))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessageContainingAll("Incompatible 'unknown://bucket/path' URI",
                                        "must be absolute, and scheme must be equal to 's3'");
         assertThatThrownBy(() -> AwsS3RepositoryUri.ofRepositoryUri(uri("http://s3.eu-west-1.amazonaws.com")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage("Invalid 'http://s3.eu-west-1.amazonaws.com' URI: 'bucket' must be present");
      }

      @Test
      public void ofUriWhenCustomRegion() {
         assertThat(AwsS3RepositoryUri.ofRepositoryUri(uri("http://bucket.s3.custom.amazonaws.com"))).satisfies(
               uri -> {
                  assertThat(uri.region()).isEqualTo("custom");
               });
         assertThat(AwsS3RepositoryUri.ofRepositoryUri(uri(
               "http://bucket.s3.custom.amazonaws.com?region=custom"))).satisfies(uri -> {
            assertThat(uri.region()).isEqualTo("custom");
         });
         assertThat(AwsS3RepositoryUri.ofRepositoryUri(uri("s3://bucket?region=custom"))).satisfies(uri -> {
            assertThat(uri.region()).isEqualTo("custom");
         });
      }

      @Test
      public void ofUriWhenBucketBasePath() {
         assertThat(AwsS3RepositoryUri.ofRepositoryUri(uri("s3://bucket/path"))).satisfies(uri -> {
            assertThat(uri.region()).isEqualTo("us-east-1");
            assertThat(uri.bucket()).isEqualTo("bucket");
            assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path"));
         });
         assertThat(AwsS3RepositoryUri.ofRepositoryUri("s3://bucket/path")).satisfies(uri -> {
            assertThat(uri.region()).isEqualTo("us-east-1");
            assertThat(uri.bucket()).isEqualTo("bucket");
            assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path"));
         });
      }

      @Test
      public void ofUriWhenRegionParameter() {
         assertThat(AwsS3RepositoryUri.ofRepositoryUri(uri("s3://bucket/path?region=eu-west-1"))).satisfies(
               uri -> {
                  assertThat(uri.region()).isEqualTo("eu-west-1");
               });
         assertThat(AwsS3RepositoryUri.ofRepositoryUri("s3://bucket/path?region=eu-west-1")).satisfies(uri -> {
            assertThat(uri.region()).isEqualTo("eu-west-1");
         });
      }

      @Nested
      class EndpointParameter {

         @Test
         public void ofUriWhenEndpointAsEncodedParameter() {
            String endpoint = "https://localhost:8000/path?custom=custom&custom2=custom2";

            assertThat(AwsS3RepositoryUri.ofRepositoryUri(uri(
                  "https://bucket.s3.eu-west-1.amazonaws.com/path?endpoint="
                  + URLEncoder.encode(endpoint,
                                      UTF_8)
                  + "&bucketBasePath=path&failFastIfMissingBucket=false&custom3=custom3"))).satisfies(uri -> {
               assertThat(uri.region()).isEqualTo("eu-west-1");
               assertThat(uri.bucket()).isEqualTo("bucket");
               assertThat(uri.query().parameter(ENDPOINT, alwaysFalse()))
                     .map(URI::toString)
                     .hasValue(endpoint);
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path"));
               assertThat(uri.query().parameter(FAIL_FAST_IF_MISSING_BUCKET, alwaysFalse())).hasValue(false);
            });
         }

         @Test
         public void ifUriWhenEndpointAsNotEncodedParameter() {
            String endpoint = "https://localhost:8000/path?custom=custom&custom2=custom2";

            assertThat(AwsS3RepositoryUri.ofRepositoryUri(uri(
                  "https://bucket.s3.eu-west-1.amazonaws.com/path?endpoint="
                  + endpoint
                  + "&bucketBasePath=path&failFastIfMissingBucket=false&custom3=custom3"))).satisfies(uri -> {
               assertThat(uri.region()).isEqualTo("eu-west-1");
               assertThat(uri.bucket()).isEqualTo("bucket");
               assertThat(uri.query().parameter(ENDPOINT, alwaysFalse()))
                     .as("? are escaped so that custom parameter is present, but & are not escaped in URI so that custom2 is lost")
                     .map(URI::toString)
                     .hasValue("https://localhost:8000/path?custom=custom");
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path"));
               assertThat(uri.query().parameter(FAIL_FAST_IF_MISSING_BUCKET, alwaysFalse())).hasValue(false);
            });
         }

         @Test
         public void ofUriWhenEndpointAsAuthority() {
            assertThatThrownBy(() -> AwsS3RepositoryUri.ofRepositoryUri(uri(
                  "https://localhost:8000/path?region=eu-west-1&bucket=bucket&custom=custom&failFastIfMissingBucket=false")))
                  .isInstanceOf(IncompatibleUriException.class)
                  .hasMessageContainingAll(
                        "Incompatible 'https://localhost:8000/path?region=eu-west-1&bucket=bucket&custom=custom&failFastIfMissingBucket=false' URI",
                        "must be absolute, and scheme must be equal to 's3'");
         }

         @Test
         public void ofUriWhenEndpointSchemeNotSupported() {
            assertThatThrownBy(() -> AwsS3RepositoryUri.ofRepositoryUri(uri(
                  "unknown://localhost:8000/path?region=eu-west-1&bucket=bucket")))
                  .isInstanceOf(IncompatibleUriException.class)
                  .hasMessageContainingAll(
                        "Incompatible 'unknown://localhost:8000/path?region=eu-west-1&bucket=bucket' URI",
                        "must be absolute, and scheme must be equal to 's3'");
            var unknownScheme = AwsS3RepositoryUri.ofRepositoryUri(uri(
                  "https://bucket.s3.amazonaws.com/path?endpoint=unknown://override&region=eu-west-1&bucket=bucket&failFastIfMissingBucket=false"));
            assertThatExceptionOfType(InvariantValidationException.class)
                  .isThrownBy(() -> AwsS3DocumentRepository.ofUri(unknownScheme))
                  .withMessage(
                        "Invariant validation error > Context [AwsS3DocumentConfig[region=eu-west-1,endpoint=unknown://override,bucket=bucket,bucketBasePath=path,createIfMissingBucket=false,failFastIfMissingBucket=false,pathStyleAccess=false,uriGenerationFormat=VIRTUAL_HOST_STYLE,listingMaxKeys=1000,listingContentMode=FAST,uploadChunkSize=5242880,credentialsProvider=DefaultCredentialsProvider(providerChain=LazyAwsCredentialsProvider(delegate=Lazy(value=Uninitialized))),matchTags=[],loadAllTags=false,httpClientConfig=<null>]] > {endpoint} 'endpoint.scheme=unknown' must be in [http,https]");
            var unsupportedScheme = AwsS3RepositoryUri.ofRepositoryUri(uri(
                  "https://bucket.s3.amazonaws.com/path?endpoint=s3://bucket&region=eu-west-1&bucket=bucket&failFastIfMissingBucket=false"));
            assertThatExceptionOfType(InvariantValidationException.class)
                  .isThrownBy(() -> AwsS3DocumentRepository.ofUri(unsupportedScheme))
                  .withMessage(
                        "Invariant validation error > Context [AwsS3DocumentConfig[region=eu-west-1,endpoint=s3://bucket,bucket=bucket,bucketBasePath=path,createIfMissingBucket=false,failFastIfMissingBucket=false,pathStyleAccess=false,uriGenerationFormat=VIRTUAL_HOST_STYLE,listingMaxKeys=1000,listingContentMode=FAST,uploadChunkSize=5242880,credentialsProvider=DefaultCredentialsProvider(providerChain=LazyAwsCredentialsProvider(delegate=Lazy(value=Uninitialized))),matchTags=[],loadAllTags=false,httpClientConfig=<null>]] > {endpoint} 'endpoint.scheme=s3' must be in [http,https]");
         }

      }

      @Nested
      class WrappedUri {

         @ParameterizedTest
         @ArgumentsSource(DefaultUriFormatProvider.class)
         public void ofUriWhenNominal(URI uri) {
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(uri("document:s3:"
                                                              + uri))).satisfies(repositoryUri -> {
               assertThat(repositoryUri.region()).isEqualTo("us-east-1");
               assertThat(repositoryUri.bucket()).isEqualTo("bucket");
               assertThat(repositoryUri.bucketBasePath()).isEqualTo(Path.of(""));
            });
         }

         @ParameterizedTest
         @ArgumentsSource(DefaultUriFormatProvider.class)
         public void ofUriWhenBadRepositoryType(URI uri) {
            assertThatThrownBy(() -> AwsS3RepositoryUri.ofRepositoryUri(uri("document:unknown:" + uri)))
                  .isInstanceOf(IncompatibleUriException.class)
                  .hasMessage("Incompatible 'document:unknown:"
                              + uri
                              + "' URI: Unsupported 'unknown' repository type");
         }

         @Test
         public void ofUriWhenNormalized() {
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(uri("document:s3:"
                                                              + "s3://bucket/path/../."))).satisfies(
                  repositoryUri -> {
                     assertThat(repositoryUri.region()).isEqualTo("us-east-1");
                     assertThat(repositoryUri.bucket()).isEqualTo("bucket");
                     assertThat(repositoryUri.bucketBasePath()).isEqualTo(Path.of(""));
                  });
            assertThat(AwsS3RepositoryUri.ofRepositoryUri("document:s3:"
                                                          + "s3://bucket/path/../.")).satisfies(repositoryUri -> {
               assertThat(repositoryUri.region()).isEqualTo("us-east-1");
               assertThat(repositoryUri.bucket()).isEqualTo("bucket");
               assertThat(repositoryUri.bucketBasePath()).isEqualTo(Path.of(""));
            });
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri("document:s3:"
                                                                                        + "s3://bucket/path/../."),
                                                          Path.of(""))).satisfies(repositoryUri -> {
               assertThat(repositoryUri.region()).isEqualTo("us-east-1");
               assertThat(repositoryUri.bucket()).isEqualTo("bucket");
               assertThat(repositoryUri.bucketBasePath()).isEqualTo(Path.of(""));
            });
            assertThatThrownBy(() -> AwsS3RepositoryUri.ofRepositoryUri(uri("document:s3:"
                                                                            + "s3://bucket/../path")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessageContainingAll("Invalid 's3://bucket/../path' URI",
                                           "must not have traversal paths");
         }

         @Test
         public void ofUriWhenUnsupportedWrappedUri() {
            assertThatThrownBy(() -> AwsS3RepositoryUri.ofRepositoryUri(uri("document:s3:unknown://host/path")))
                  .isInstanceOf(IncompatibleUriException.class)
                  .hasMessageContainingAll("Incompatible 'unknown://host/path' URI",
                                           "must be absolute, and scheme must be equal to 's3'");
         }

      }
   }

   @Nested
   public class OfUriWhenRepositoryUri {

      @Test
      public void ofUriWhenBadDefaultBucketBasePath() {
         assertThatThrownBy(() -> AwsS3RepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
               "s3://bucket/path"), null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultBucketBasePath' must not be null");
         assertThatThrownBy(() -> AwsS3RepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
               "s3://bucket/path"), Path.of("/")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultBucketBasePath=/' must not be absolute path");
         assertThatThrownBy(() -> AwsS3RepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
               "s3://bucket/path"), Path.of("/path")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage(
                     "Invariant validation error > 'defaultBucketBasePath=/path' must not be absolute path");
         assertThatThrownBy(() -> AwsS3RepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
               "s3://bucket/path"), Path.of("../path")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage(
                     "Invariant validation error > 'defaultBucketBasePath=../path' must not have traversal paths");
      }

      @Nested
      public class WhenRepositoryUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri("s3://bucket"),
                                                          Path.of("path"))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path"));
            });
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri("s3://bucket/path"),
                                                          Path.of("path"))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path"));
            });
         }

         @Test
         public void ofUriWhenImplicitDefaultBucketBasePath() {
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri("s3://bucket"))).satisfies(
                  uri -> {
                     assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
                  });
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
                  "s3://bucket/path/subpath"))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path/subpath"));
            });
         }

         @Test
         public void ofUriWhenDefaultBucketBasePath() {
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri("s3://bucket"),
                                                          Path.of("path/subpath"))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path/subpath"));
            });
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
                  "s3://bucket/path/subpath"), Path.of("path/subpath"))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path/subpath"));
            });
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri("s3://bucket"),
                                                          Path.of(""))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
            });
         }

         @Test
         public void ofUriWhenMismatchingDefaultBucketBasePath() {
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri("s3://bucket/path"),
                                                          Path.of("otherpath")))
                  .as("Default bucket base path is only applied if it matches actual URI")
                  .satisfies(uri -> {
                     assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path"));
                  });
            assertThatThrownBy(() -> AwsS3RepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
                  "s3://bucket/path/subpath"), Path.of("path")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 's3://bucket/path/subpath' URI: 'bucketBasePath=path' parameter must match URI 'path/subpath' path");
            assertThatThrownBy(() -> AwsS3RepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
                  "s3://bucket/path/subpath"), Path.of("")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 's3://bucket/path/subpath' URI: 'bucketBasePath=' parameter must match URI 'path/subpath' path");
         }

         @Test
         public void ofUriWhenNoBucketBasePath() {
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri("s3://bucket/"))).satisfies(
                  uri -> {
                     assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
                  });
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri("s3://bucket"))).satisfies(
                  uri -> {
                     assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
                  });
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri("s3://bucket"),
                                                          Path.of(""))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
            });
         }

         @Test
         public void ofUriWhenExplicitBucketBasePath() {
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
                  "s3://bucket/path/subpath?bucketBasePath=path/subpath"))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path/subpath"));
            });
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
                  "s3://bucket?bucketBasePath=path/subpath"))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path/subpath"));
            });
         }

         @Test
         public void ofUriWhenMismatchingExplicitBucketBasePath() {
            assertThatThrownBy(() -> AwsS3RepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
                  "s3://bucket/path/subpath?bucketBasePath=otherpath")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 's3://bucket/path/subpath?bucketBasePath=otherpath' URI: 'bucketBasePath=otherpath' parameter must match URI 'path/subpath' path");
            assertThatThrownBy(() -> AwsS3RepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
                  "s3://bucket/path/subpath?bucketBasePath=path")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 's3://bucket/path/subpath?bucketBasePath=path' URI: 'bucketBasePath=path' parameter must match URI 'path/subpath' path");
            assertThatThrownBy(() -> AwsS3RepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
                  "s3://bucket/path/subpath?bucketBasePath=")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 's3://bucket/path/subpath?bucketBasePath=' URI: 'bucketBasePath=' parameter must match URI 'path/subpath' path");
            assertThatThrownBy(() -> AwsS3RepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(
                  "s3://bucket/path/subpath?bucketBasePath=subpath")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 's3://bucket/path/subpath?bucketBasePath=subpath' URI: 'bucketBasePath=subpath' parameter must match URI 'path/subpath' path");
         }

      }

      @Nested
      public class WhenDocumentUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri("s3://bucket/document"),
                                                          Path.of(""))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
            });
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri(
                  "s3://bucket/path/document"), Path.of("path"))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path"));
            });
         }

         @Test
         public void ofUriWhenImplicitDefaultBucketBasePath() {
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri("s3://bucket/document"))).satisfies(
                  uri -> {
                     assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
                  });
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri(
                  "s3://bucket/path/subpath/document"))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
            });
         }

         @Test
         public void ofUriWhenDefaultBucketBasePath() {
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri(
                  "s3://bucket/path/subpath/document"), Path.of("path/subpath"))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path/subpath"));
            });
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri(
                  "s3://bucket/path/subpath/document"), Path.of("path"))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path"));
            });
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri(
                  "s3://bucket/path/subpath/document"), Path.of(""))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
            });
         }

         @Test
         public void ofUriWhenNoBucketBasePath() {
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri("s3://bucket/document"))).satisfies(
                  uri -> {
                     assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
                  });
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri("s3://bucket/document"),
                                                          Path.of(""))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
            });
         }

         @Test
         public void ofUriWhenExplicitBucketBasePath() {
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri(
                  "s3://bucket/path/subpath/document?bucketBasePath=path/subpath"))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path/subpath"));
            });
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri(
                  "s3://bucket/path/subpath/document?bucketBasePath="))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
            });
         }

      }

      @Nested
      public class WhenAwsS3RepositoryUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(AwsS3RepositoryUri.ofRepositoryUri("s3://bucket"),
                                                          Path.of("path"))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
            });
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(AwsS3RepositoryUri.ofRepositoryUri(
                  "s3://bucket/path"), Path.of(""))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path"));
            });
         }

         @Test
         public void ofUriWhenImplicitDefaultBucketBasePath() {
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(AwsS3RepositoryUri.ofRepositoryUri("s3://bucket"))).satisfies(
                  uri -> {
                     assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
                  });
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(AwsS3RepositoryUri.ofRepositoryUri(
                  "s3://bucket/path/subpath"))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path/subpath"));
            });
         }

         @Test
         public void ofUriWhenDefaultBucketBasePath() {
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(AwsS3RepositoryUri.ofRepositoryUri("s3://bucket"),
                                                          Path.of("path/subpath"))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
            });
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(AwsS3RepositoryUri.ofRepositoryUri(
                  "s3://bucket/path/subpath"), Path.of("path/subpath"))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path/subpath"));
            });
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(AwsS3RepositoryUri.ofRepositoryUri("s3://bucket"),
                                                          Path.of(""))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
            });
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(AwsS3RepositoryUri.ofRepositoryUri(
                  "s3://bucket/path/subpath"), Path.of(""))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path/subpath"));
            });
         }

         @Test
         public void ofUriWhenNoBucketBasePath() {
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(AwsS3RepositoryUri.ofRepositoryUri("s3://bucket/"))).satisfies(
                  uri -> {
                     assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
                  });
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(AwsS3RepositoryUri.ofRepositoryUri("s3://bucket"))).satisfies(
                  uri -> {
                     assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
                  });
         }

         @Test
         public void ofUriWhenExplicitBucketBasePath() {
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(AwsS3RepositoryUri.ofRepositoryUri(
                  "s3://bucket/path/subpath?bucketBasePath=path/subpath"))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path/subpath"));
            });
         }

      }

      @Nested
      public class WhenAwsS3DocumentUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(AwsS3DocumentUri.ofDocumentUri(
                  "s3://bucket/path/document"), Path.of("path"))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
            });
         }

         @Test
         public void ofUriWhenImplicitDefaultBucketBasePath() {
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(AwsS3DocumentUri.ofDocumentUri(
                  "s3://bucket/path/subpath/document"))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
            });
         }

         @Test
         public void ofUriWhenDefaultBucketBasePath() {
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(AwsS3DocumentUri.ofDocumentUri(
                  "s3://bucket/path/subpath/document"), Path.of("path/subpath"))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
            });
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(AwsS3DocumentUri.ofDocumentUri(
                  "s3://bucket/path/subpath/document"), Path.of("path"))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
            });
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(AwsS3DocumentUri.ofDocumentUri(
                  "s3://bucket/path/subpath/document"), Path.of(""))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
            });
         }

         @Test
         public void ofUriWhenNoBucketBasePath() {
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(AwsS3DocumentUri.ofDocumentUri(
                  "s3://bucket/document"))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
            });
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(AwsS3DocumentUri.ofDocumentUri(
                  "s3://bucket/document"), Path.of(""))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
            });
         }

         @Test
         public void ofUriWhenExplicitBucketBasePath() {
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(AwsS3DocumentUri.ofDocumentUri(
                  "s3://bucket/path/subpath/document?bucketBasePath=path/subpath"))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path/subpath"));
            });
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(AwsS3DocumentUri.ofDocumentUri(
                  "s3://bucket/path/subpath/document?bucketBasePath="))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
            });
         }

      }

   }

   @Nested
   public class OfUriWhenVariousUriFormat {

      @ParameterizedTest
      @ArgumentsSource(DefaultUriFormatProvider.class)
      public void ofUriWhenNominal(URI uri) {
         assertThat(AwsS3RepositoryUri.ofRepositoryUri(uri)).satisfies(repositoryUri -> {
            assertThat(repositoryUri.region()).isEqualTo("us-east-1");
            assertThat(repositoryUri.bucket()).isEqualTo("bucket");
            assertThat(repositoryUri.bucketBasePath()).isEqualTo(Path.of(""));
         });

         assertThat(AwsS3RepositoryUri.ofRepositoryUri(uri.resolve("path/key"))).satisfies(repositoryUri -> {
            assertThat(repositoryUri.region()).isEqualTo("us-east-1");
            assertThat(repositoryUri.bucket()).isEqualTo("bucket");
            assertThat(repositoryUri.bucketBasePath()).isEqualTo(Path.of("path/key"));
         });
      }

      @ParameterizedTest
      @ArgumentsSource(DefaultUriFormatProvider.class)
      public void ofUriWhenIllegalArgument(URI uri) {
         assertThatExceptionOfType(InvalidUriException.class)
               .isThrownBy(() -> AwsS3RepositoryUri.ofRepositoryUri(uri.resolve("?listingContentMode=UNKNOWN")))
               .withMessage(
                     "Invalid '%s' URI: Conversion from 'java.lang.String' to 'com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig$S3ListingContentMode' failed for 'UNKNOWN' value",
                     uri.resolve("?listingContentMode=UNKNOWN"));
      }

      @ParameterizedTest
      @ArgumentsSource(DefaultUriFormatProvider.class)
      public void ofUriWhenCaseSensitiveParameterKey(URI uri) {
         assertThat(AwsS3RepositoryUri.ofRepositoryUri(uri.resolve("?ReGiOn=eu-west-1"))).satisfies(
               repositoryUri -> {
                  assertThat(repositoryUri.region()).isEqualTo("us-east-1");
               });
      }

      @ParameterizedTest
      @ArgumentsSource(DefaultUriFormatProvider.class)
      public void ofUriWhenInvalidBucketBasePath(URI uri) {
         assertThatThrownBy(() -> AwsS3RepositoryUri.ofRepositoryUri(uri.resolve(
               "path/subpath?bucketBasePath=/path/subpath")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid '%s' URI: 'bucketBasePath=/path/subpath' parameter must be relative and must not contain traversal",
                     uri.resolve("path/subpath?bucketBasePath=/path/subpath"));
         assertThatThrownBy(() -> AwsS3RepositoryUri.ofRepositoryUri(uri.resolve(
               "path/subpath?bucketBasePath=../path")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid '%s' URI: 'bucketBasePath=../path' parameter must be relative and must not contain traversal",
                     uri.resolve("path/subpath?bucketBasePath=../path"));
         assertThatThrownBy(() -> AwsS3RepositoryUri.ofRepositoryUri(uri.resolve(
               "path/subpath?bucketBasePath=path/subpath/..")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid '%s' URI: 'bucketBasePath=path' parameter must match URI 'path/subpath' path",
                     uri.resolve("path/subpath?bucketBasePath=path/subpath/.."));
      }

      @ParameterizedTest
      @ArgumentsSource(DefaultUriFormatProvider.class)
      public void ofUriWhenExactBucketBasePathParameter(URI uri) {
         assertThat(AwsS3RepositoryUri.ofRepositoryUri(uri.resolve("?bucketBasePath=path/subpath"))).satisfies(
               repositoryUri -> {
                  assertThat(repositoryUri.bucket()).isEqualTo("bucket");
                  assertThat(repositoryUri.bucketBasePath()).isEqualTo(Path.of("path/subpath"));
               });
         assertThat(AwsS3RepositoryUri.ofRepositoryUri(uri.resolve("path/subpath?bucketBasePath=path/subpath"))).satisfies(
               repositoryUri -> {
                  assertThat(repositoryUri.bucket()).isEqualTo("bucket");
                  assertThat(repositoryUri.bucketBasePath()).isEqualTo(Path.of("path/subpath"));
               });
         assertThat(AwsS3RepositoryUri.ofRepositoryUri(uri.resolve(
               "path/subpath?bucketBasePath=./path/./subpath/."))).satisfies(repositoryUri -> {
            assertThat(repositoryUri.bucket()).isEqualTo("bucket");
            assertThat(repositoryUri.bucketBasePath()).isEqualTo(Path.of("path/subpath"));
         });
      }

      @ParameterizedTest
      @ArgumentsSource(DefaultUriFormatProvider.class)
      public void ofUriWhenPartialBucketBasePathParameter(URI uri) {
         assertThat(AwsS3RepositoryUri.ofRepositoryUri(uri.resolve("?bucketBasePath=path"))).satisfies(
               repositoryUri -> {
                  assertThat(repositoryUri.bucket()).isEqualTo("bucket");
                  assertThat(repositoryUri.bucketBasePath()).isEqualTo(Path.of("path"));
               });
         assertThatThrownBy(() -> AwsS3RepositoryUri.ofRepositoryUri(uri.resolve(
               "path/subpath?bucketBasePath=path")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid '%s' URI: 'bucketBasePath=path' parameter must match URI 'path/subpath' path",
                     uri.resolve("path/subpath?bucketBasePath=path"));
         assertThatThrownBy(() -> AwsS3RepositoryUri.ofRepositoryUri(uri.resolve(
               "path/subpath?bucketBasePath=path/.")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid '%s' URI: 'bucketBasePath=path' parameter must match URI 'path/subpath' path",
                     uri.resolve("path/subpath?bucketBasePath=path/."));
      }

      @ParameterizedTest
      @ArgumentsSource(DefaultUriFormatProvider.class)
      public void ofUriWhenMismatchingBucketBasePathParameter(URI uri) {
         assertThatThrownBy(() -> AwsS3RepositoryUri.ofRepositoryUri(uri.resolve(
               "path/subpath?bucketBasePath=otherpath")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid '%s' URI: 'bucketBasePath=otherpath' parameter must match URI 'path/subpath' path",
                     uri.resolve("path/subpath?bucketBasePath=otherpath"));
         assertThatThrownBy(() -> AwsS3RepositoryUri.ofRepositoryUri(uri.resolve(
               "path/subpath?bucketBasePath=path/subpath/otherpath")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid '%s' URI: 'bucketBasePath=path/subpath/otherpath' parameter must match URI 'path/subpath' path",
                     uri.resolve("path/subpath?bucketBasePath=path/subpath/otherpath"));
      }

      @ParameterizedTest
      @ArgumentsSource(DefaultUriFormatProvider.class)
      public void ofUriWhenSameRegionParameter(URI uri) {
         assertThat(AwsS3RepositoryUri.ofRepositoryUri(uri.resolve("?region=us-east-1"))).satisfies(
               repositoryUri -> {
                  assertThat(repositoryUri.region()).isEqualTo("us-east-1");
                  assertThat(repositoryUri.bucket()).isEqualTo("bucket");
               });
      }

      @ParameterizedTest
      @ArgumentsSource(DefaultUriFormatProvider.class)
      public void ofUriDifferentRegionParameter(URI uri) {
         try {
            assertThat(AwsS3RepositoryUri.ofRepositoryUri(uri.resolve("?region=eu-west-1"))).satisfies(
                  repositoryUri -> {
                     assertThat(repositoryUri.region()).isEqualTo("eu-west-1");
                     assertThat(repositoryUri.bucket()).isEqualTo("bucket");
                  });
         } catch (InvalidUriException e) {
            assertThat(uri.toString()).contains("us-east-1");
            assertThat(e).hasMessage(
                  "Invalid '%s' URI: 'region=eu-west-1' parameter must match URI 'us-east-1' region",
                  uri.resolve("?region=eu-west-1"));
         }
      }

      @ParameterizedTest
      @ArgumentsSource(DefaultUriFormatProvider.class)
      public void ofUriWhenDefaultParameters(URI uri) {
         assertThat(AwsS3RepositoryUri.ofRepositoryUri(uri)).satisfies(repositoryUri -> {
            assertThat(repositoryUri.region()).isEqualTo(defaultRegion().id());
            assertThat(repositoryUri.query().parameter(REGION, alwaysTrue())).hasValue(defaultRegion().id());
            assertThat(repositoryUri.query().parameter(PATH_STYLE_ACCESS, alwaysTrue())).hasValue(
                  DEFAULT_PATH_STYLE_ACCESS);
            assertThat(repositoryUri.query().parameter(URI_GENERATION_FORMAT, alwaysTrue())).hasValue(
                  DEFAULT_URI_GENERATION_FORMAT);
            assertThat(repositoryUri.query().parameter(ENDPOINT, alwaysTrue())).isEmpty();
            assertThat(repositoryUri.bucketBasePath()).isEqualTo(Path.of(""));
            assertThat(repositoryUri.query().parameter(CREATE_IF_MISSING_BUCKET, alwaysTrue())).hasValue(
                  DEFAULT_CREATE_IF_MISSING_BUCKET);
            assertThat(repositoryUri.query().parameter(FAIL_FAST_IF_MISSING_BUCKET, alwaysTrue())).hasValue(
                  DEFAULT_FAIL_FAST_IF_MISSING_BUCKET);
            assertThat(repositoryUri.query().parameter(LISTING_CONTENT_MODE, alwaysTrue())).hasValue(
                  DEFAULT_LISTING_CONTENT_MODE);
            assertThat(repositoryUri.query().parameter(LISTING_MAX_KEYS, alwaysTrue())).hasValue(
                  DEFAULT_LISTING_MAX_KEYS);
            assertThat(repositoryUri.query().parameter(UPLOAD_CHUNK_SIZE, alwaysTrue())).hasValue(
                  DEFAULT_UPLOAD_CHUNK_SIZE);
            assertThat(repositoryUri.query().parameter(LOAD_ALL_TAGS, alwaysTrue())).hasValue(
                  DEFAULT_LOAD_ALL_TAGS);
            assertThat(repositoryUri.query().parameters(MATCH_TAGS, alwaysTrue())).isEmpty();
         });
      }

      @ParameterizedTest
      @ArgumentsSource(DefaultUriFormatProvider.class)
      public void ofUriWhenAllParameters(URI uri) {
         assertThat(AwsS3RepositoryUri.ofRepositoryUri(uri.resolve("path"
                                                                   + "?region=us-east-1"
                                                                   + "&pathStyleAccess=true"
                                                                   + "&uriGenerationFormat=S3"
                                                                   + "&endpoint=http://localhost"
                                                                   + "&bucketBasePath=path"
                                                                   + "&createIfMissingBucket=true"
                                                                   + "&failFastIfMissingBucket=false"
                                                                   + "&listingContentMode=FILTERING_COMPLETE"
                                                                   + "&listingMaxKeys=50"
                                                                   + "&uploadChunkSize=5242881"
                                                                   + "&loadAllTags=true"
                                                                   + "&matchTags=pattern1"
                                                                   + "&matchTags=pattern2"))).satisfies(
               repositoryUri -> {
                  assertThat(repositoryUri.region()).isEqualTo("us-east-1");
                  assertThat(repositoryUri.bucketBasePath()).isEqualTo(Path.of("path"));

                  assertThat(repositoryUri.query().parameter(REGION, alwaysFalse())).hasValue("us-east-1");
                  assertThat(repositoryUri.query()
                                   .parameter(PATH_STYLE_ACCESS, alwaysFalse())).hasValue(true);
                  assertThat(repositoryUri.query().parameter(URI_GENERATION_FORMAT, alwaysFalse())).hasValue(
                        S3);
                  assertThat(repositoryUri.query().parameter(ENDPOINT, alwaysFalse())).hasValue(uri(
                        "http://localhost"));
                  assertThat(repositoryUri.query()
                                   .parameter(CREATE_IF_MISSING_BUCKET, alwaysFalse())).hasValue(true);
                  assertThat(repositoryUri.query()
                                   .parameter(FAIL_FAST_IF_MISSING_BUCKET, alwaysFalse())).hasValue(false);
                  assertThat(repositoryUri.query().parameter(LISTING_CONTENT_MODE, alwaysFalse())).hasValue(
                        FILTERING_COMPLETE);
                  assertThat(repositoryUri.query().parameter(LISTING_MAX_KEYS, alwaysFalse())).hasValue(50);
                  assertThat(repositoryUri.query().parameter(UPLOAD_CHUNK_SIZE, alwaysFalse())).hasValue(
                        5242881);
                  assertThat(repositoryUri.query().parameter(LOAD_ALL_TAGS, alwaysFalse())).hasValue(true);
                  assertThat(repositoryUri.query().parameters(MATCH_TAGS, alwaysFalse()))
                        .usingElementComparator(comparing(Pattern::pattern))
                        .containsExactly(Pattern.compile("pattern1"), Pattern.compile("pattern2"));
               });
      }
   }

   @Nested
   public class OfConfig {

      @Test
      public void ofConfigWhenNominal() {
         assertThat(AwsS3RepositoryUri.ofConfig(new AwsS3DocumentConfigBuilder()
                                                      .bucket("bucket")
                                                      .build())).satisfies(uri -> {
            assertThat(uri.region()).isEqualTo(defaultRegion().id());
            assertThat(uri.bucket()).isEqualTo("bucket");
            assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));

            assertThat(uri.query().parameter(REGION, alwaysTrue())).hasValue(defaultRegion().id());
            assertThat(uri.query().parameter(PATH_STYLE_ACCESS, alwaysTrue())).hasValue(
                  DEFAULT_PATH_STYLE_ACCESS);
            assertThat(uri.query().parameter(URI_GENERATION_FORMAT, alwaysTrue())).hasValue(
                  DEFAULT_URI_GENERATION_FORMAT);
            assertThat(uri.query().parameter(ENDPOINT, alwaysTrue())).isEmpty();
            assertThat(uri.query().parameter(CREATE_IF_MISSING_BUCKET, alwaysTrue())).hasValue(
                  DEFAULT_CREATE_IF_MISSING_BUCKET);
            assertThat(uri.query().parameter(FAIL_FAST_IF_MISSING_BUCKET, alwaysTrue())).hasValue(
                  DEFAULT_FAIL_FAST_IF_MISSING_BUCKET);
            assertThat(uri.query().parameter(LISTING_CONTENT_MODE, alwaysTrue())).hasValue(
                  DEFAULT_LISTING_CONTENT_MODE);
            assertThat(uri.query().parameter(LISTING_MAX_KEYS, alwaysTrue())).hasValue(
                  DEFAULT_LISTING_MAX_KEYS);
            assertThat(uri.query().parameter(UPLOAD_CHUNK_SIZE, alwaysTrue())).hasValue(
                  DEFAULT_UPLOAD_CHUNK_SIZE);
            assertThat(uri.query().parameter(LOAD_ALL_TAGS, alwaysTrue())).hasValue(DEFAULT_LOAD_ALL_TAGS);
            assertThat(uri.query().parameters(MATCH_TAGS, alwaysTrue())).isEmpty();
         });
      }

      @Test
      public void ofConfigWhenAllParameters() {
         assertThat(AwsS3RepositoryUri.ofConfig(new AwsS3DocumentConfigBuilder()
                                                      .region("eu-west-1")
                                                      .bucket("bucket")
                                                      .pathStyleAccess(true)
                                                      .uriGenerationFormat(S3)
                                                      .endpoint(uri("http://localhost"))
                                                      .bucketBasePath(Path.of("path"))
                                                      .createIfMissingBucket(true)
                                                      .failFastIfMissingBucket(false)
                                                      .listingContentMode(FILTERING_COMPLETE)
                                                      .listingMaxKeys(50)
                                                      .uploadChunkSize(5242881)
                                                      .loadAllTags(true)
                                                      .matchTags(Pattern.compile("pattern1"),
                                                                 Pattern.compile("pattern2"))
                                                      .build())).satisfies(uri -> {
            assertThat(uri.region()).isEqualTo("eu-west-1");
            assertThat(uri.bucket()).isEqualTo("bucket");
            assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path"));

            assertThat(uri.query().parameter(REGION, alwaysFalse())).hasValue("eu-west-1");
            assertThat(uri.query().parameter(PATH_STYLE_ACCESS, alwaysFalse())).hasValue(true);
            assertThat(uri.query().parameter(URI_GENERATION_FORMAT, alwaysFalse())).hasValue(S3);
            assertThat(uri.query().parameter(ENDPOINT, alwaysFalse())).hasValue(uri("http://localhost"));
            assertThat(uri.query().parameter(CREATE_IF_MISSING_BUCKET, alwaysFalse())).hasValue(true);
            assertThat(uri.query().parameter(FAIL_FAST_IF_MISSING_BUCKET, alwaysFalse())).hasValue(false);
            assertThat(uri
                             .query()
                             .parameter(LISTING_CONTENT_MODE, alwaysFalse())).hasValue(FILTERING_COMPLETE);
            assertThat(uri.query().parameter(LISTING_MAX_KEYS, alwaysFalse())).hasValue(50);
            assertThat(uri.query().parameter(UPLOAD_CHUNK_SIZE, alwaysFalse())).hasValue(5242881);
            assertThat(uri.query().parameter(LOAD_ALL_TAGS, alwaysFalse())).hasValue(true);
            assertThat(uri.query().parameters(MATCH_TAGS, alwaysFalse()))
                  .usingElementComparator(comparing(Pattern::pattern))
                  .containsExactly(Pattern.compile("pattern1"), Pattern.compile("pattern2"));
         });
      }

   }

   @Nested
   public class ToConfig {

      @ParameterizedTest
      @ArgumentsSource(DefaultUriFormatProvider.class)
      public void toConfigWhenNominal(URI uri) {
         assertThat(AwsS3RepositoryUri.ofRepositoryUri(uri).toConfig()).satisfies(config -> {
            assertThat(config.region()).isEqualTo("us-east-1");
            assertThat(config.endpoint()).isNull();
            assertThat(config.bucket()).isEqualTo("bucket");
            assertThat(config.bucketBasePath()).isEqualTo(Path.of(""));
            assertThat(config.createIfMissingBucket()).isEqualTo(DEFAULT_CREATE_IF_MISSING_BUCKET);
            assertThat(config.failFastIfMissingBucket()).isEqualTo(DEFAULT_FAIL_FAST_IF_MISSING_BUCKET);
            assertThat(config.pathStyleAccess()).isEqualTo(DEFAULT_PATH_STYLE_ACCESS);
            assertThat(config.uriGenerationFormat()).isEqualTo(DEFAULT_URI_GENERATION_FORMAT);
            assertThat(config.listingMaxKeys()).isEqualTo(DEFAULT_LISTING_MAX_KEYS);
            assertThat(config.listingContentMode()).isEqualTo(DEFAULT_LISTING_CONTENT_MODE);
            assertThat(config.uploadChunkSize()).isEqualTo(DEFAULT_UPLOAD_CHUNK_SIZE);
            assertThat(config.matchTags()).isEmpty();
            assertThat(config.loadAllTags()).isEqualTo(DEFAULT_LOAD_ALL_TAGS);
         });
      }

   }

   @Nested
   public class ToUri {

      @ParameterizedTest
      @ArgumentsSource(DefaultUriFormatProvider.class)
      public void toUriWhenNominal(URI uri) {
         assertThat(AwsS3RepositoryUri
                          .ofRepositoryUri(uri)
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo(securedHttp(uri) + "://bucket.s3.amazonaws.com");
      }

      @ParameterizedTest
      @ArgumentsSource(DefaultUriFormatProvider.class)
      public void toUriWhenBucketBasePath(URI uri) {
         assertThat(AwsS3RepositoryUri
                          .ofRepositoryUri(uri.resolve("path"))
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo(securedHttp(uri) + "://bucket.s3.amazonaws.com/path");
      }

      @ParameterizedTest
      @ArgumentsSource(DefaultUriFormatProvider.class)
      public void toUriWhenAwsS3UriOptions(URI uri) {
         assertThat(AwsS3RepositoryUri.ofRepositoryUri(uri).exportUri(AwsS3ExportUriOptions.noParameters(S3))
                          .stringValue()).isEqualTo("s3://bucket");
         assertThat(AwsS3RepositoryUri
                          .ofRepositoryUri(uri)
                          .exportUri(AwsS3ExportUriOptions.noParameters(VIRTUAL_HOST_STYLE))
                          .stringValue()).isEqualTo(securedHttp(uri) + "://bucket.s3.amazonaws.com");
         assertThat(AwsS3RepositoryUri
                          .ofRepositoryUri(uri)
                          .exportUri(AwsS3ExportUriOptions.noParameters(PATH_STYLE))
                          .stringValue()).isEqualTo(securedHttp(uri) + "://s3.amazonaws.com/bucket");
      }

      @ParameterizedTest
      @ArgumentsSource(DefaultUriFormatProvider.class)
      public void toUriWhenS3Cli(URI uri) {
         assertThat(AwsS3RepositoryUri.ofRepositoryUri(uri).exportUri(AwsS3ExportUriOptions.noParameters(S3))
                          .stringValue()).isEqualTo("s3://bucket");
         assertThat(AwsS3RepositoryUri
                          .ofRepositoryUri(uri.resolve("path"))
                          .exportUri(AwsS3ExportUriOptions.noParameters(S3))
                          .stringValue()).isEqualTo("s3://bucket/path");
      }

      @ParameterizedTest
      @ArgumentsSource(DefaultUriFormatProvider.class)
      public void toUriWhenVirtualHostStyle(URI uri) {
         assertThat(AwsS3RepositoryUri
                          .ofRepositoryUri(uri)
                          .exportUri(AwsS3ExportUriOptions.noParameters(VIRTUAL_HOST_STYLE))
                          .stringValue()).isEqualTo(securedHttp(uri) + "://bucket.s3.amazonaws.com");
         assertThat(AwsS3RepositoryUri
                          .ofRepositoryUri(uri.resolve("path"))
                          .exportUri(AwsS3ExportUriOptions.noParameters(VIRTUAL_HOST_STYLE))
                          .stringValue()).isEqualTo(securedHttp(uri) + "://bucket.s3.amazonaws.com/path");
      }

      @ParameterizedTest
      @ArgumentsSource(DefaultUriFormatProvider.class)
      public void toUriWhenPathStyle(URI uri) {
         assertThat(AwsS3RepositoryUri
                          .ofRepositoryUri(uri)
                          .exportUri(AwsS3ExportUriOptions.noParameters(PATH_STYLE))
                          .stringValue()).isEqualTo(securedHttp(uri) + "://s3.amazonaws.com/bucket");
         assertThat(AwsS3RepositoryUri
                          .ofRepositoryUri(uri.resolve("path"))
                          .exportUri(AwsS3ExportUriOptions.noParameters(PATH_STYLE))
                          .stringValue()).isEqualTo(securedHttp(uri) + "://s3.amazonaws.com/bucket/path");
      }

      @ParameterizedTest
      @ArgumentsSource(DefaultUriFormatProvider.class)
      public void toUriWhenDefaultParameters(URI uri) {
         assertThat(AwsS3RepositoryUri
                          .ofRepositoryUri(uri.resolve("path"))
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo(securedHttp(uri) + "://bucket.s3.amazonaws.com/path");
         assertThat(AwsS3RepositoryUri
                          .ofRepositoryUri(uri.resolve("path?bucketBasePath=path"))
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo(securedHttp(uri) + "://bucket.s3.amazonaws.com/path");
         assertThat(AwsS3RepositoryUri
                          .ofRepositoryUri(uri.resolve("path?failFastIfMissingBucket=true"))
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo(securedHttp(uri) + "://bucket.s3.amazonaws.com/path");
         assertThat(AwsS3RepositoryUri
                          .ofRepositoryUri(uri.resolve("path?failFastIfMissingBucket=false"))
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo(securedHttp(uri)
                                                    + "://bucket.s3.amazonaws.com/path?failFastIfMissingBucket=false");

         assertThat(AwsS3RepositoryUri
                          .ofRepositoryUri(uri.resolve("path"))
                          .exportUri(AwsS3ExportUriOptions.defaultParameters(S3))
                          .stringValue()).isEqualTo("s3://bucket/path");

      }

      @ParameterizedTest
      @ArgumentsSource(DefaultUriFormatProvider.class)
      public void toUriWhenDefaultParametersWithForceDefaultValues(URI uri) {
         assertThat(AwsS3RepositoryUri
                          .ofRepositoryUri(uri.resolve("path"))
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo(securedHttp(uri)
                                                    + "://bucket.s3.amazonaws.com/path?pathStyleAccess=false&bucketBasePath=path&createIfMissingBucket=false&failFastIfMissingBucket=true&listingContentMode=FAST&listingMaxKeys=1000&uploadChunkSize=5242880&loadAllTags=false");
         assertThat(AwsS3RepositoryUri
                          .ofRepositoryUri(uri.resolve("path?bucketBasePath=path"))
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo(securedHttp(uri)
                                                    + "://bucket.s3.amazonaws.com/path?bucketBasePath=path&pathStyleAccess=false&createIfMissingBucket=false&failFastIfMissingBucket=true&listingContentMode=FAST&listingMaxKeys=1000&uploadChunkSize=5242880&loadAllTags=false");
         assertThat(AwsS3RepositoryUri
                          .ofRepositoryUri(uri.resolve("path?failFastIfMissingBucket=true"))
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo(securedHttp(uri)
                                                    + "://bucket.s3.amazonaws.com/path?failFastIfMissingBucket=true&pathStyleAccess=false&bucketBasePath=path&createIfMissingBucket=false&listingContentMode=FAST&listingMaxKeys=1000&uploadChunkSize=5242880&loadAllTags=false");
         assertThat(AwsS3RepositoryUri
                          .ofRepositoryUri(uri.resolve("path?failFastIfMissingBucket=false"))
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo(securedHttp(uri)
                                                    + "://bucket.s3.amazonaws.com/path?failFastIfMissingBucket=false&pathStyleAccess=false&bucketBasePath=path&createIfMissingBucket=false&listingContentMode=FAST&listingMaxKeys=1000&uploadChunkSize=5242880&loadAllTags=false");

         assertThat(AwsS3RepositoryUri
                          .ofConfig(new AwsS3DocumentConfigBuilder()
                                          .bucketBasePath(Path.of("path"))
                                          .bucket("bucket")
                                          .build())
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo("https"
                                                    + "://bucket.s3.amazonaws.com/path?pathStyleAccess=false&bucketBasePath=path&createIfMissingBucket=false&failFastIfMissingBucket=true&listingContentMode=FAST&listingMaxKeys=1000&uploadChunkSize=5242880&loadAllTags=false");

         assertThat(AwsS3RepositoryUri
                          .ofRepositoryUri(uri.resolve("path"))
                          .exportUri(AwsS3ExportUriOptions.defaultParameters(S3, true))
                          .stringValue()).isEqualTo(
               "s3://bucket/path?region=us-east-1&pathStyleAccess=false&bucketBasePath=path&createIfMissingBucket=false&failFastIfMissingBucket=true&listingContentMode=FAST&listingMaxKeys=1000&uploadChunkSize=5242880&loadAllTags=false");
      }

      @ParameterizedTest
      @ArgumentsSource(DefaultUriFormatProvider.class)
      public void toUriWhenSensitiveParameters(URI uri) {
         assertThat(AwsS3RepositoryUri
                          .ofRepositoryUri(uri.resolve("path"))
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo(securedHttp(uri) + "://bucket.s3.amazonaws.com/path");
         assertThat(AwsS3RepositoryUri
                          .ofRepositoryUri(uri.resolve("path?bucketBasePath=path"))
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo(securedHttp(uri) + "://bucket.s3.amazonaws.com/path");
      }

      private String securedHttp(URI uri) {
         return uri.getScheme().equals("http") ? "http" : "https";
      }

   }

   @Nested
   public class NormalizeResolveRelativize {

      @Test
      public void normalizeWhenNominal() {
         assertThat(AwsS3RepositoryUri
                          .ofRepositoryUri("s3://bucket/path/../.")
                          .normalize()).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo("s3://bucket/");
            assertThat(uri.bucket()).isEqualTo("bucket");
            assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
         });
      }

      @Test
      public void resolveWhenNominal() {
         assertThat(AwsS3RepositoryUri
                          .ofRepositoryUri("s3://bucket/path")
                          .resolve(uri("otherpath"))).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo("s3://bucket/otherpath");
            assertThat(uri.bucket()).isEqualTo("bucket");
            assertThat(uri.bucketBasePath()).isEqualTo(Path.of("otherpath"));
         });
         assertThat(AwsS3RepositoryUri
                          .ofRepositoryUri("s3://bucket/path/")
                          .resolve(uri("otherpath"))).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo("s3://bucket/path/otherpath");
            assertThat(uri.bucket()).isEqualTo("bucket");
            assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path/otherpath"));
         });
      }

      @Test
      public void relativizeWhenNominal() {
         assertThatThrownBy(() -> AwsS3RepositoryUri
               .ofRepositoryUri("s3://bucket/path")
               .relativize(uri("s3://bucket/path/otherpath"))).isInstanceOf(UnsupportedOperationException.class);
      }

   }
}