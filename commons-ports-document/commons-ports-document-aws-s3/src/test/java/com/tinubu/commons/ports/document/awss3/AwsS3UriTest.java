/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.awss3;

import static com.tinubu.commons.ddd2.test.uri.UriAssert.assertThat;
import static com.tinubu.commons.ddd2.uri.DefaultUriRestrictions.PortRestriction.REMOVE_MAPPED_PORT;
import static com.tinubu.commons.ddd2.uri.Uri.uri;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.net.URI;
import java.nio.file.Path;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ddd2.uri.DefaultUriRestrictions;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ddd2.uri.uris.HttpUri.HttpUriRestrictions;

public class AwsS3UriTest {

   static {
      Assertions.setMaxStackTraceElementsDisplayed(50);
   }

   @Nested
   class S3CliUri {

      @Test
      public void ofUriWhenNominal() {
         assertThat(AwsS3CliUri.ofUri("s3://bucket")).satisfies(uri -> {
            Assertions.assertThat(uri.region()).isEmpty();
            Assertions.assertThat(uri.bucket()).isEqualTo("bucket");
            Assertions.assertThat(uri.key()).isEmpty();
         });
         assertThat(AwsS3CliUri.ofUri(uri("s3://bucket"))).satisfies(uri -> {
            Assertions.assertThat(uri.region()).isEmpty();
            Assertions.assertThat(uri.bucket()).isEqualTo("bucket");
            Assertions.assertThat(uri.key()).isEmpty();
         });
         assertThat(AwsS3CliUri.ofUri(Uri.ofUri("s3://bucket"))).satisfies(uri -> {
            Assertions.assertThat(uri.region()).isEmpty();
            Assertions.assertThat(uri.bucket()).isEqualTo("bucket");
            Assertions.assertThat(uri.key()).isEmpty();
         });
      }

      @Test
      public void ofUriWhenBadArguments() {
         assertThatThrownBy(() -> AwsS3CliUri.ofUri((String) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
         assertThatThrownBy(() -> AwsS3CliUri.ofUri((URI) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
         assertThatThrownBy(() -> AwsS3CliUri.ofUri((Uri) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
      }

      @Test
      public void ofUriWhenIncompatibleUri() {
         assertThatThrownBy(() -> AwsS3CliUri.ofUri("unknown://host/path"))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessageContainingAll("Incompatible 'unknown://host/path' URI",
                                        "scheme must be equal to 's3'");
         assertThatThrownBy(() -> AwsS3CliUri.ofUri(uri("unknown://host/path")))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessageContainingAll("Incompatible 'unknown://host/path' URI",
                                        "scheme must be equal to 's3'");
         assertThatThrownBy(() -> AwsS3CliUri.ofUri(Uri.ofUri("unknown://host/path")))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessageContainingAll("Incompatible 'unknown://host/path' URI",
                                        "scheme must be equal to 's3'");
      }

      @Test
      public void ofUriWhenInvalidUri() {
         assertThatThrownBy(() -> AwsS3CliUri.ofUri("s3://bucket:80"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 's3://bucket:80' URI: 'AwsS3CliUri[value=s3://bucket:80,restrictions=HttpUriRestrictions[http=true, https=false, portRestriction=NONE, query=true, fragment=true],scheme=SimpleScheme[scheme='s3'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='//bucket:80', encodedSchemeSpecific='//bucket:80'],authority=SimpleServerAuthority[userInfo='null', host='SimpleHost[host='bucket']', port='SimplePort[port='80']'],path=SimplePath[path=, encodedPath=],query=<null>,fragment=<null>]' must not have a port");
         assertThatThrownBy(() -> AwsS3CliUri.ofUri("s3://bucket#fragment"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 's3://bucket#fragment' URI: 'AwsS3CliUri[value=s3://bucket#fragment,restrictions=HttpUriRestrictions[http=true, https=false, portRestriction=NONE, query=true, fragment=true],scheme=SimpleScheme[scheme='s3'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='//bucket', encodedSchemeSpecific='//bucket'],authority=SimpleServerAuthority[userInfo='null', host='SimpleHost[host='bucket']', port='null'],path=SimplePath[path=, encodedPath=],query=<null>,fragment=SimpleFragment[fragment=fragment, encodedFragment=fragment]]' must not have a fragment");
      }

      @Test
      public void ofUriWhenKey() {
         assertThat(AwsS3CliUri.ofUri("s3://bucket/path/key")).satisfies(uri -> {
            Assertions.assertThat(uri.region()).isEmpty();
            Assertions.assertThat(uri.bucket()).isEqualTo("bucket");
            Assertions.assertThat(uri.key()).hasValue("path/key");
            Assertions.assertThat(uri.pathKey()).hasValue(Path.of("path/key"));
         });
         assertThat(AwsS3CliUri.ofUri("s3://bucket/path/key/")).satisfies(uri -> {
            Assertions.assertThat(uri.region()).isEmpty();
            Assertions.assertThat(uri.bucket()).isEqualTo("bucket");
            Assertions.assertThat(uri.key()).hasValue("path/key/");
            Assertions.assertThat(uri.pathKey()).hasValue(Path.of("path/key"));
         });
      }

      @Test
      public void ofUriWhenRootKey() {
         assertThat(AwsS3CliUri.ofUri("s3://bucket/")).satisfies(uri -> {
            Assertions.assertThat(uri.region()).isEmpty();
            Assertions.assertThat(uri.bucket()).isEqualTo("bucket");
            Assertions.assertThat(uri.key()).isEmpty();
            Assertions.assertThat(uri.pathKey()).isEmpty();
         });
      }

      @Test
      public void ofUriWhenSlashedKey() {
         assertThat(AwsS3CliUri.ofUri("s3://bucket//")).satisfies(uri -> {
            Assertions.assertThat(uri.region()).isEmpty();
            Assertions.assertThat(uri.bucket()).isEqualTo("bucket");
            Assertions.assertThat(uri.key()).hasValue("/");
            Assertions.assertThat(uri.pathKey()).hasValue(Path.of("/"));
         });
         assertThat(AwsS3CliUri.ofUri("s3://bucket//path")).satisfies(uri -> {
            Assertions.assertThat(uri.region()).isEmpty();
            Assertions.assertThat(uri.bucket()).isEqualTo("bucket");
            Assertions.assertThat(uri.key()).hasValue("/path");
            Assertions.assertThat(uri.pathKey()).hasValue(Path.of("/path"));
         });
      }

      @Test
      public void ofUriWhenQuery() {
         assertThatThrownBy(() -> AwsS3CliUri.ofUri("s3://bucket?query"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 's3://bucket?query' URI: 'AwsS3CliUri[value=s3://bucket?query,restrictions=HttpUriRestrictions[http=true, https=false, portRestriction=NONE, query=true, fragment=true],scheme=SimpleScheme[scheme='s3'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='//bucket?query', encodedSchemeSpecific='//bucket?query'],authority=SimpleServerAuthority[userInfo='null', host='SimpleHost[host='bucket']', port='null'],path=SimplePath[path=, encodedPath=],query=SimpleQuery[query=query, encodedQuery=query],fragment=<null>]' must not have a query");
         assertThat(AwsS3CliUri.ofUri("s3://bucket?query",
                                      DefaultUriRestrictions.ofRestrictions(false, false))).satisfies(uri -> {
            Assertions.assertThat(uri.bucket()).isEqualTo("bucket");
            assertThat(uri).hasQuery(false, "query");
         });
      }

      @Test
      public void ofUriWhenFragment() {
         assertThatThrownBy(() -> AwsS3CliUri.ofUri("s3://bucket#fragment"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 's3://bucket#fragment' URI: 'AwsS3CliUri[value=s3://bucket#fragment,restrictions=HttpUriRestrictions[http=true, https=false, portRestriction=NONE, query=true, fragment=true],scheme=SimpleScheme[scheme='s3'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='//bucket', encodedSchemeSpecific='//bucket'],authority=SimpleServerAuthority[userInfo='null', host='SimpleHost[host='bucket']', port='null'],path=SimplePath[path=, encodedPath=],query=<null>,fragment=SimpleFragment[fragment=fragment, encodedFragment=fragment]]' must not have a fragment");
         assertThat(AwsS3CliUri.ofUri("s3://bucket#fragment",
                                      DefaultUriRestrictions.ofRestrictions(false, false))).satisfies(uri -> {
            Assertions.assertThat(uri.bucket()).isEqualTo("bucket");
            assertThat(uri).hasFragment(false, "fragment");
         });
      }

   }

   @Nested
   class VirtualHostUri {

      @Test
      public void ofUriWhenNominal() {
         assertThat(AwsS3HttpUri.ofUri("https://bucket.s3.eu-west-1.amazonaws.com")).satisfies(uri -> {
            Assertions.assertThat(uri.region()).hasValue("eu-west-1");
            Assertions.assertThat(uri.bucket()).isEqualTo("bucket");
            Assertions.assertThat(uri.key()).isEmpty();
         });
         assertThat(AwsS3HttpUri.ofUri(uri("https://bucket.s3.eu-west-1.amazonaws.com"))).satisfies(uri -> {
            Assertions.assertThat(uri.region()).hasValue("eu-west-1");
            Assertions.assertThat(uri.bucket()).isEqualTo("bucket");
            Assertions.assertThat(uri.key()).isEmpty();
         });
         assertThat(AwsS3HttpUri.ofUri(Uri.ofUri("https://bucket.s3.eu-west-1.amazonaws.com"))).satisfies(uri -> {
            Assertions.assertThat(uri.region()).hasValue("eu-west-1");
            Assertions.assertThat(uri.bucket()).isEqualTo("bucket");
            Assertions.assertThat(uri.key()).isEmpty();
         });
      }

      @Test
      public void ofUriWhenBadArguments() {
         assertThatThrownBy(() -> AwsS3HttpUri.ofUri((String) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
         assertThatThrownBy(() -> AwsS3HttpUri.ofUri((URI) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
         assertThatThrownBy(() -> AwsS3HttpUri.ofUri((Uri) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
      }

      @Test
      public void ofUriWhenIncompatibleUri() {
         assertThatThrownBy(() -> AwsS3HttpUri.ofUri("unknown://host/path"))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessage("Incompatible 'unknown://host/path' URI: 'scheme=unknown' must be in [http,https]");
         assertThatThrownBy(() -> AwsS3HttpUri.ofUri("https://invalid"))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessage(
                     "Incompatible 'https://invalid' URI: Invalid S3 URI: hostname does not appear to be a valid S3 endpoint: https://invalid");
         assertThatThrownBy(() -> AwsS3HttpUri.ofUri(uri("https://invalid")))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessage(
                     "Incompatible 'https://invalid' URI: Invalid S3 URI: hostname does not appear to be a valid S3 endpoint: https://invalid");
         assertThatThrownBy(() -> AwsS3HttpUri.ofUri(Uri.ofUri(("https://invalid"))))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessage(
                     "Incompatible 'https://invalid' URI: Invalid S3 URI: hostname does not appear to be a valid S3 endpoint: https://invalid");
      }

      @Test
      public void ofUriWhenInvalidUri() {
         assertThatThrownBy(() -> AwsS3HttpUri.ofUri("https://s3.eu-west-1.amazonaws.com"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage("Invalid 'https://s3.eu-west-1.amazonaws.com' URI: 'bucket' must be present");
         assertThatThrownBy(() -> AwsS3HttpUri.ofUri("https://bucket.s3.eu-west-1.amazonaws.com#fragment"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'https://bucket.s3.eu-west-1.amazonaws.com#fragment' URI: 'HttpUri[value=https://bucket.s3.eu-west-1.amazonaws.com#fragment,restrictions=HttpUriRestrictions[http=true, https=false, portRestriction=NONE, query=true, fragment=true],scheme=SimpleScheme[scheme='https'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='//bucket.s3.eu-west-1.amazonaws.com', encodedSchemeSpecific='//bucket.s3.eu-west-1.amazonaws.com'],authority=SimpleServerAuthority[userInfo='null', host='SimpleHost[host='bucket.s3.eu-west-1.amazonaws.com']', port='null'],path=SimplePath[path=, encodedPath=],query=<null>,fragment=SimpleFragment[fragment=fragment, encodedFragment=fragment]]' must not have a fragment");
      }

      @Test
      public void ofUriWhenAlternativePort() {
         assertThat(AwsS3HttpUri.ofUri("https://bucket.s3.eu-west-1.amazonaws.com:8080")).satisfies(uri -> {
            Assertions.assertThat(uri.value()).isEqualTo("https://bucket.s3.eu-west-1.amazonaws.com:8080");
            Assertions.assertThat(uri.region()).hasValue("eu-west-1");
            Assertions.assertThat(uri.bucket()).isEqualTo("bucket");
            Assertions.assertThat(uri.key()).isEmpty();
         });
      }

      @Test
      public void ofUriWhenDefaultPort() {
         assertThat(AwsS3HttpUri.ofUri("https://bucket.s3.eu-west-1.amazonaws.com:443")).satisfies(uri -> {
            Assertions.assertThat(uri.value()).isEqualTo("https://bucket.s3.eu-west-1.amazonaws.com:443");
            Assertions.assertThat(uri.region()).hasValue("eu-west-1");
            Assertions.assertThat(uri.bucket()).isEqualTo("bucket");
            Assertions.assertThat(uri.key()).isEmpty();
         });
         assertThat(AwsS3HttpUri.ofUri("https://bucket.s3.eu-west-1.amazonaws.com:443", DefaultUriRestrictions
                                             .ofDefault()
                                             .portRestriction(REMOVE_MAPPED_PORT))).satisfies(uri -> {
            Assertions.assertThat(uri.value()).isEqualTo("https://bucket.s3.eu-west-1.amazonaws.com");
            Assertions.assertThat(uri.region()).hasValue("eu-west-1");
            Assertions.assertThat(uri.bucket()).isEqualTo("bucket");
            Assertions.assertThat(uri.key()).isEmpty();
         });
      }

      @Test
      public void ofUriWhenWhenNotSecured() {
         assertThatThrownBy(() -> AwsS3HttpUri.ofUri("http://bucket.s3.eu-west-1.amazonaws.com"))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessage(
                     "Incompatible 'http://bucket.s3.eu-west-1.amazonaws.com' URI: 'scheme=http' must be in [https]");
         assertThat(AwsS3HttpUri.ofUri("http://bucket.s3.eu-west-1.amazonaws.com",
                                       HttpUriRestrictions.ofDefault().http(false))).satisfies(uri -> {
            Assertions.assertThat(uri.region()).hasValue("eu-west-1");
            Assertions.assertThat(uri.bucket()).isEqualTo("bucket");
            Assertions.assertThat(uri.key()).isEmpty();
         });
      }

      @Test
      public void ofUriWhenWhenNoRegion() {
         assertThat(AwsS3HttpUri.ofUri("https://bucket.s3.amazonaws.com")).satisfies(uri -> {
            Assertions.assertThat(uri.region()).isEmpty();
            Assertions.assertThat(uri.bucket()).isEqualTo("bucket");
            Assertions.assertThat(uri.key()).isEmpty();
         });
      }

      @Test
      public void ofUriWhenKey() {
         assertThat(AwsS3HttpUri.ofUri("https://bucket.s3.eu-west-1.amazonaws.com/path/key")).satisfies(uri -> {
            Assertions.assertThat(uri.region()).hasValue("eu-west-1");
            Assertions.assertThat(uri.bucket()).isEqualTo("bucket");
            Assertions.assertThat(uri.key()).hasValue("path/key");
         });
      }

      @Test
      public void ofUriWhenRootKey() {
         assertThat(AwsS3HttpUri.ofUri("https://bucket.s3.eu-west-1.amazonaws.com/")).satisfies(uri -> {
            Assertions.assertThat(uri.region()).hasValue("eu-west-1");
            Assertions.assertThat(uri.bucket()).isEqualTo("bucket");
            Assertions.assertThat(uri.key()).isEmpty();
            Assertions.assertThat(uri.pathKey()).isEmpty();
         });
      }

      @Test
      public void ofUriWhenSlashedKey() {
         assertThat(AwsS3HttpUri.ofUri("https://bucket.s3.eu-west-1.amazonaws.com//")).satisfies(uri -> {
            Assertions.assertThat(uri.region()).hasValue("eu-west-1");
            Assertions.assertThat(uri.bucket()).isEqualTo("bucket");
            Assertions.assertThat(uri.key()).hasValue("/");
            Assertions.assertThat(uri.pathKey()).hasValue(Path.of("/"));
         });
         assertThat(AwsS3HttpUri.ofUri("https://bucket.s3.eu-west-1.amazonaws.com//path")).satisfies(uri -> {
            Assertions.assertThat(uri.region()).hasValue("eu-west-1");
            Assertions.assertThat(uri.bucket()).isEqualTo("bucket");
            Assertions.assertThat(uri.key()).hasValue("/path");
            Assertions.assertThat(uri.pathKey()).hasValue(Path.of("/path"));
         });
      }

      @Test
      public void ofUriWhenQuery() {
         assertThatThrownBy(() -> AwsS3HttpUri.ofUri("https://bucket.s3.eu-west-1.amazonaws.com?query"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'https://bucket.s3.eu-west-1.amazonaws.com?query' URI: 'HttpUri[value=https://bucket.s3.eu-west-1.amazonaws.com?query,restrictions=HttpUriRestrictions[http=true, https=false, portRestriction=NONE, query=true, fragment=true],scheme=SimpleScheme[scheme='https'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='//bucket.s3.eu-west-1.amazonaws.com?query', encodedSchemeSpecific='//bucket.s3.eu-west-1.amazonaws.com?query'],authority=SimpleServerAuthority[userInfo='null', host='SimpleHost[host='bucket.s3.eu-west-1.amazonaws.com']', port='null'],path=SimplePath[path=, encodedPath=],query=SimpleQuery[query=query, encodedQuery=query],fragment=<null>]' must not have a query");
         assertThat(AwsS3HttpUri.ofUri("https://bucket.s3.eu-west-1.amazonaws.com?query",
                                       DefaultUriRestrictions.ofRestrictions(false,
                                                                             false))).satisfies(uri -> {
            Assertions.assertThat(uri.bucket()).isEqualTo("bucket");
            assertThat(uri).hasQuery(false, "query");
         });
      }

      @Test
      public void ofUriWhenFragment() {
         assertThatThrownBy(() -> AwsS3HttpUri.ofUri("https://bucket.s3.eu-west-1.amazonaws.com#fragment"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'https://bucket.s3.eu-west-1.amazonaws.com#fragment' URI: 'HttpUri[value=https://bucket.s3.eu-west-1.amazonaws.com#fragment,restrictions=HttpUriRestrictions[http=true, https=false, portRestriction=NONE, query=true, fragment=true],scheme=SimpleScheme[scheme='https'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='//bucket.s3.eu-west-1.amazonaws.com', encodedSchemeSpecific='//bucket.s3.eu-west-1.amazonaws.com'],authority=SimpleServerAuthority[userInfo='null', host='SimpleHost[host='bucket.s3.eu-west-1.amazonaws.com']', port='null'],path=SimplePath[path=, encodedPath=],query=<null>,fragment=SimpleFragment[fragment=fragment, encodedFragment=fragment]]' must not have a fragment");
         assertThat(AwsS3HttpUri.ofUri("https://bucket.s3.eu-west-1.amazonaws.com#fragment",
                                       DefaultUriRestrictions.ofRestrictions(false,
                                                                             false))).satisfies(uri -> {
            Assertions.assertThat(uri.bucket()).isEqualTo("bucket");
            assertThat(uri).hasFragment(false, "fragment");
         });
      }

   }

   @Nested
   class PathUri {

      @Test
      public void ofUriWhenNominal() {
         assertThat(AwsS3HttpUri.ofUri("https://s3.eu-west-1.amazonaws.com/bucket")).satisfies(uri -> {
            Assertions.assertThat(uri.region()).hasValue("eu-west-1");
            Assertions.assertThat(uri.bucket()).isEqualTo("bucket");
            Assertions.assertThat(uri.key()).isEmpty();
         });
         assertThat(AwsS3HttpUri.ofUri(uri("https://s3.eu-west-1.amazonaws.com/bucket"))).satisfies(uri -> {
            Assertions.assertThat(uri.region()).hasValue("eu-west-1");
            Assertions.assertThat(uri.bucket()).isEqualTo("bucket");
            Assertions.assertThat(uri.key()).isEmpty();
         });
         assertThat(AwsS3HttpUri.ofUri(Uri.ofUri("https://s3.eu-west-1.amazonaws.com/bucket"))).satisfies(uri -> {
            Assertions.assertThat(uri.region()).hasValue("eu-west-1");
            Assertions.assertThat(uri.bucket()).isEqualTo("bucket");
            Assertions.assertThat(uri.key()).isEmpty();
         });
      }

      @Test
      public void ofUriWhenBadArguments() {
         assertThatThrownBy(() -> AwsS3HttpUri.ofUri((String) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
         assertThatThrownBy(() -> AwsS3HttpUri.ofUri((URI) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
         assertThatThrownBy(() -> AwsS3HttpUri.ofUri((Uri) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
      }

      @Test
      public void ofUriWhenIncompatibleUri() {
         assertThatThrownBy(() -> AwsS3HttpUri.ofUri("unknown://host/path"))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessage("Incompatible 'unknown://host/path' URI: 'scheme=unknown' must be in [http,https]");
         assertThatThrownBy(() -> AwsS3HttpUri.ofUri("https://invalid"))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessage(
                     "Incompatible 'https://invalid' URI: Invalid S3 URI: hostname does not appear to be a valid S3 endpoint: https://invalid");
         assertThatThrownBy(() -> AwsS3HttpUri.ofUri(uri("https://invalid")))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessage(
                     "Incompatible 'https://invalid' URI: Invalid S3 URI: hostname does not appear to be a valid S3 endpoint: https://invalid");
         assertThatThrownBy(() -> AwsS3HttpUri.ofUri(Uri.ofUri("https://invalid")))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessage(
                     "Incompatible 'https://invalid' URI: Invalid S3 URI: hostname does not appear to be a valid S3 endpoint: https://invalid");
      }

      @Test
      public void ofUriWhenInvalidUri() {
         assertThatThrownBy(() -> AwsS3HttpUri.ofUri("https://s3.eu-west-1.amazonaws.com//"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage("Invalid 'https://s3.eu-west-1.amazonaws.com//' URI: 'bucket' must not be blank");
         assertThatThrownBy(() -> AwsS3HttpUri.ofUri("https://s3.eu-west-1.amazonaws.com/"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage("Invalid 'https://s3.eu-west-1.amazonaws.com/' URI: 'bucket' must be present");
         assertThatThrownBy(() -> AwsS3HttpUri.ofUri("https://s3.eu-west-1.amazonaws.com"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage("Invalid 'https://s3.eu-west-1.amazonaws.com' URI: 'bucket' must be present");
         assertThatThrownBy(() -> AwsS3HttpUri.ofUri("https://s3.eu-west-1.amazonaws.com/bucket#fragment"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'https://s3.eu-west-1.amazonaws.com/bucket#fragment' URI: 'HttpUri[value=https://s3.eu-west-1.amazonaws.com/bucket#fragment,restrictions=HttpUriRestrictions[http=true, https=false, portRestriction=NONE, query=true, fragment=true],scheme=SimpleScheme[scheme='https'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='//s3.eu-west-1.amazonaws.com/bucket', encodedSchemeSpecific='//s3.eu-west-1.amazonaws.com/bucket'],authority=SimpleServerAuthority[userInfo='null', host='SimpleHost[host='s3.eu-west-1.amazonaws.com']', port='null'],path=SimplePath[path=/bucket, encodedPath=/bucket],query=<null>,fragment=SimpleFragment[fragment=fragment, encodedFragment=fragment]]' must not have a fragment");
      }

      @Test
      public void ofUriWhenWhenNotSecured() {
         assertThatThrownBy(() -> AwsS3HttpUri.ofUri("http://s3.eu-west-1.amazonaws.com/bucket"))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessage(
                     "Incompatible 'http://s3.eu-west-1.amazonaws.com/bucket' URI: 'scheme=http' must be in [https]");
         assertThat(AwsS3HttpUri.ofUri("http://s3.eu-west-1.amazonaws.com/bucket",
                                       HttpUriRestrictions.ofDefault().http(false))).satisfies(uri -> {
            Assertions.assertThat(uri.region()).hasValue("eu-west-1");
            Assertions.assertThat(uri.bucket()).isEqualTo("bucket");
            Assertions.assertThat(uri.key()).isEmpty();
         });
      }

      @Test
      public void ofUriWhenWhenNoRegion() {
         assertThat(AwsS3HttpUri.ofUri("https://s3.amazonaws.com/bucket")).satisfies(uri -> {
            Assertions.assertThat(uri.region()).isEmpty();
            Assertions.assertThat(uri.bucket()).isEqualTo("bucket");
            Assertions.assertThat(uri.key()).isEmpty();
         });
      }

      @Test
      public void ofUriWhenKey() {
         assertThat(AwsS3HttpUri.ofUri("https://s3.eu-west-1.amazonaws.com/bucket/path/key")).satisfies(uri -> {
            Assertions.assertThat(uri.region()).hasValue("eu-west-1");
            Assertions.assertThat(uri.bucket()).isEqualTo("bucket");
            Assertions.assertThat(uri.key()).hasValue("path/key");
         });
      }

      @Test
      public void ofUriWhenQuery() {
         assertThatThrownBy(() -> AwsS3HttpUri.ofUri("https://s3.eu-west-1.amazonaws.com/bucket?query"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'https://s3.eu-west-1.amazonaws.com/bucket?query' URI: 'HttpUri[value=https://s3.eu-west-1.amazonaws.com/bucket?query,restrictions=HttpUriRestrictions[http=true, https=false, portRestriction=NONE, query=true, fragment=true],scheme=SimpleScheme[scheme='https'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='//s3.eu-west-1.amazonaws.com/bucket?query', encodedSchemeSpecific='//s3.eu-west-1.amazonaws.com/bucket?query'],authority=SimpleServerAuthority[userInfo='null', host='SimpleHost[host='s3.eu-west-1.amazonaws.com']', port='null'],path=SimplePath[path=/bucket, encodedPath=/bucket],query=SimpleQuery[query=query, encodedQuery=query],fragment=<null>]' must not have a query");
         assertThat(AwsS3HttpUri.ofUri("https://s3.eu-west-1.amazonaws.com/bucket?query",
                                       DefaultUriRestrictions.ofRestrictions(false,
                                                                             false))).satisfies(uri -> {
            Assertions.assertThat(uri.bucket()).isEqualTo("bucket");
            assertThat(uri).hasQuery(false, "query");
         });
      }

      @Test
      public void ofUriWhenFragment() {
         assertThatThrownBy(() -> AwsS3HttpUri.ofUri("https://s3.eu-west-1.amazonaws.com/bucket#fragment"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'https://s3.eu-west-1.amazonaws.com/bucket#fragment' URI: 'HttpUri[value=https://s3.eu-west-1.amazonaws.com/bucket#fragment,restrictions=HttpUriRestrictions[http=true, https=false, portRestriction=NONE, query=true, fragment=true],scheme=SimpleScheme[scheme='https'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='//s3.eu-west-1.amazonaws.com/bucket', encodedSchemeSpecific='//s3.eu-west-1.amazonaws.com/bucket'],authority=SimpleServerAuthority[userInfo='null', host='SimpleHost[host='s3.eu-west-1.amazonaws.com']', port='null'],path=SimplePath[path=/bucket, encodedPath=/bucket],query=<null>,fragment=SimpleFragment[fragment=fragment, encodedFragment=fragment]]' must not have a fragment");
         assertThat(AwsS3HttpUri.ofUri("https://s3.eu-west-1.amazonaws.com/bucket#fragment",
                                       DefaultUriRestrictions.ofRestrictions(false,
                                                                             false))).satisfies(uri -> {
            Assertions.assertThat(uri.bucket()).isEqualTo("bucket");
            assertThat(uri).hasFragment(false, "fragment");
         });
      }

   }

}