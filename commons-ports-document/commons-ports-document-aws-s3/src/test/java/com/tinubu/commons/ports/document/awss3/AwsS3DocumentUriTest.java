/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.awss3;

import static com.tinubu.commons.ddd2.uri.DefaultUriRestrictions.noRestrictions;
import static com.tinubu.commons.ddd2.uri.Uri.uri;
import static com.tinubu.commons.lang.util.CheckedPredicate.alwaysFalse;
import static com.tinubu.commons.lang.util.CheckedPredicate.alwaysTrue;
import static com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig.S3ListingContentMode.FILTERING_COMPLETE;
import static com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig.S3UriFormat.PATH_STYLE;
import static com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig.S3UriFormat.S3;
import static com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig.S3UriFormat.VIRTUAL_HOST_STYLE;
import static com.tinubu.commons.ports.document.awss3.AwsS3RepositoryUri.Parameters.CREATE_IF_MISSING_BUCKET;
import static com.tinubu.commons.ports.document.awss3.AwsS3RepositoryUri.Parameters.ENDPOINT;
import static com.tinubu.commons.ports.document.awss3.AwsS3RepositoryUri.Parameters.FAIL_FAST_IF_MISSING_BUCKET;
import static com.tinubu.commons.ports.document.awss3.AwsS3RepositoryUri.Parameters.LISTING_CONTENT_MODE;
import static com.tinubu.commons.ports.document.awss3.AwsS3RepositoryUri.Parameters.LISTING_MAX_KEYS;
import static com.tinubu.commons.ports.document.awss3.AwsS3RepositoryUri.Parameters.LOAD_ALL_TAGS;
import static com.tinubu.commons.ports.document.awss3.AwsS3RepositoryUri.Parameters.MATCH_TAGS;
import static com.tinubu.commons.ports.document.awss3.AwsS3RepositoryUri.Parameters.PATH_STYLE_ACCESS;
import static com.tinubu.commons.ports.document.awss3.AwsS3RepositoryUri.Parameters.REGION;
import static com.tinubu.commons.ports.document.awss3.AwsS3RepositoryUri.Parameters.UPLOAD_CHUNK_SIZE;
import static com.tinubu.commons.ports.document.awss3.AwsS3RepositoryUri.Parameters.URI_GENERATION_FORMAT;
import static com.tinubu.commons.ports.document.awss3.Constants.DEFAULT_CREATE_IF_MISSING_BUCKET;
import static com.tinubu.commons.ports.document.awss3.Constants.DEFAULT_FAIL_FAST_IF_MISSING_BUCKET;
import static com.tinubu.commons.ports.document.awss3.Constants.DEFAULT_LISTING_CONTENT_MODE;
import static com.tinubu.commons.ports.document.awss3.Constants.DEFAULT_LISTING_MAX_KEYS;
import static com.tinubu.commons.ports.document.awss3.Constants.DEFAULT_LOAD_ALL_TAGS;
import static com.tinubu.commons.ports.document.awss3.Constants.DEFAULT_PATH_STYLE_ACCESS;
import static com.tinubu.commons.ports.document.awss3.Constants.DEFAULT_UPLOAD_CHUNK_SIZE;
import static com.tinubu.commons.ports.document.awss3.Constants.DEFAULT_URI_GENERATION_FORMAT;
import static com.tinubu.commons.ports.document.awss3.S3Utils.defaultRegion;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.defaultParameters;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.noParameters;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.sensitiveParameters;
import static java.util.Comparator.comparing;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.net.URI;
import java.nio.file.Path;
import java.util.regex.Pattern;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig.AwsS3DocumentConfigBuilder;
import com.tinubu.commons.ports.document.awss3.BaseAwsS3Test.DefaultUriFormatProvider;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;

// FIXME VIRTUAL/PATH style + region
// FIXME forceDefaultValues + regions ?
// FIXME ajouter systématiquement la region en sortie même avec defaultParameters ?
public class AwsS3DocumentUriTest {

   static {
      Assertions.setMaxStackTraceElementsDisplayed(50);
   }

   @Nested
   public class OfUriWhenURI {

      @Test
      public void ofUriWhenNominal() {
         assertThat(AwsS3DocumentUri.ofDocumentUri(uri("s3://bucket/key"), Path.of(""))).satisfies(uri -> {
            assertThat(uri.region()).isEqualTo("us-east-1");
            assertThat(uri.bucket()).isEqualTo("bucket");
            assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("key"));
         });
         assertThat(AwsS3DocumentUri.ofDocumentUri("s3://bucket/key", Path.of(""))).satisfies(uri -> {
            assertThat(uri.region()).isEqualTo("us-east-1");
            assertThat(uri.bucket()).isEqualTo("bucket");
            assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("key"));
         });
      }

      @Test
      public void ofUriWhenUri() {
         assertThat(AwsS3DocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(Uri.ofUri(
               "s3://bucket/path/key?bucketBasePath=path")))).satisfies(uri -> {
            assertThat(uri.region()).isEqualTo("us-east-1");
            assertThat(uri.bucket()).isEqualTo("bucket");
            assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("key"));
         });
         assertThat(AwsS3DocumentUri.ofDocumentUri(Uri.ofUri("s3://bucket/path/key?bucketBasePath=path"))).satisfies(
               uri -> {
                  assertThat(uri.region()).isEqualTo("us-east-1");
                  assertThat(uri.bucket()).isEqualTo("bucket");
                  assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path"));
                  assertThat(uri.documentId()).isEqualTo(DocumentPath.of("key"));
               });
      }

      @Test
      public void ofUriWhenAwsS3CliUri() {
         assertThat(AwsS3DocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(AwsS3CliUri.ofUri(
               "s3://bucket/path/key?bucketBasePath=path",
               noRestrictions())))).satisfies(uri -> {
            assertThat(uri.region()).isEqualTo("us-east-1");
            assertThat(uri.bucket()).isEqualTo("bucket");
            assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("key"));
         });
         assertThat(AwsS3DocumentUri.ofDocumentUri(AwsS3CliUri.ofUri(
               "s3://bucket/path/key?bucketBasePath=path",
               noRestrictions()))).satisfies(uri -> {
            assertThat(uri.region()).isEqualTo("us-east-1");
            assertThat(uri.bucket()).isEqualTo("bucket");
            assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("key"));
         });
      }

      @Test
      public void ofUriWhenAwsS3HttpUri() {
         assertThat(AwsS3DocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(AwsS3HttpUri.ofUri(
               "https://bucket.s3.us-east-1.amazonaws.com/path/key?bucketBasePath=path",
               noRestrictions())))).satisfies(uri -> {
            assertThat(uri.region()).isEqualTo("us-east-1");
            assertThat(uri.bucket()).isEqualTo("bucket");
            assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("key"));
         });
         assertThat(AwsS3DocumentUri.ofDocumentUri(AwsS3HttpUri.ofUri(
               "https://bucket.s3.us-east-1.amazonaws.com/path/key?bucketBasePath=path",
               noRestrictions()))).satisfies(uri -> {
            assertThat(uri.region()).isEqualTo("us-east-1");
            assertThat(uri.bucket()).isEqualTo("bucket");
            assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("key"));
         });
      }

      @Test
      public void ofUriWhenUserInfos() {
         assertThatThrownBy(() -> AwsS3DocumentUri.ofDocumentUri(uri("s3://user:password@bucket/key"),
                                                                 Path.of("")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessageContainingAll("Invalid 's3://user:password@bucket/key' URI",
                                        "must not have user info");
         assertThatThrownBy(() -> AwsS3DocumentUri.ofDocumentUri(uri(
               "https://user:password@bucket.s3.amazonaws.com/key"), Path.of("")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessageContainingAll("Invalid 'https://user:password@bucket.s3.amazonaws.com/key' URI",
                                        "must not have user info");
         assertThatThrownBy(() -> AwsS3DocumentUri.ofDocumentUri(uri(
               "https://user:password@s3.amazonaws.com/bucket/key"), Path.of("")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessageContainingAll("Invalid 'https://user:password@s3.amazonaws.com/bucket/key' URI",
                                        "must not have user info");
      }

      @Test
      public void ofUriWhenBadParameters() {
         assertThatThrownBy(() -> AwsS3DocumentUri.ofDocumentUri((URI) null, Path.of("")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
         assertThatThrownBy(() -> AwsS3DocumentUri.ofDocumentUri(uri("s3://bucket/key"), null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultBucketBasePath' must not be null");
         assertThatThrownBy(() -> AwsS3DocumentUri.ofDocumentUri((String) null, Path.of("")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
         assertThatThrownBy(() -> AwsS3DocumentUri.ofDocumentUri("s3://bucket/key", null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultBucketBasePath' must not be null");
         assertThatThrownBy(() -> AwsS3DocumentUri.ofDocumentUri((DocumentUri) null, Path.of("")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
      }

      @Test
      public void ofUriWhenNormalized() {
         assertThat(AwsS3DocumentUri.ofDocumentUri(uri("s3://bucket/path/.././key"), Path.of(""))).satisfies(
               uri -> {
                  assertThat(uri.region()).isEqualTo("us-east-1");
                  assertThat(uri.bucket()).isEqualTo("bucket");
                  assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
                  assertThat(uri.documentId()).isEqualTo(DocumentPath.of("key"));
               });
         assertThat(AwsS3DocumentUri.ofDocumentUri("s3://bucket/path/.././key",
                                                   Path.of(""))).satisfies(uri -> {
            assertThat(uri.region()).isEqualTo("us-east-1");
            assertThat(uri.bucket()).isEqualTo("bucket");
            assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("key"));
         });
         assertThat(AwsS3DocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri("s3://bucket/path/.././key"),
                                                   Path.of(""))).satisfies(uri -> {
            assertThat(uri.region()).isEqualTo("us-east-1");
            assertThat(uri.bucket()).isEqualTo("bucket");
            assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("key"));
         });
         assertThatThrownBy(() -> AwsS3DocumentUri.ofDocumentUri(uri("s3://bucket/../key"), Path.of("")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessageContainingAll("Invalid 's3://bucket/../key' URI", "must not have traversal paths");
      }

      @Test
      public void ofUriWhenInvalidUriSyntax() {
         assertThatThrownBy(() -> AwsS3DocumentUri.ofDocumentUri("git@git.com:user/repo.git", Path.of("")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'git@git.com:user/repo.git' URI: Illegal character in scheme name at index 3: git@git.com:user/repo.git");
         assertThatThrownBy(() -> AwsS3DocumentUri.ofDocumentUri("git@git.com:user/repo.git", Path.of("")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'git@git.com:user/repo.git' URI: Illegal character in scheme name at index 3: git@git.com:user/repo.git");
      }

      @Test
      public void ofUriWhenIncompatibleUri() {
         assertThatThrownBy(() -> AwsS3DocumentUri.ofDocumentUri(uri("unknown://bucket/path"), Path.of("")))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessageContainingAll("Incompatible 'unknown://bucket/path' URI",
                                        "must be absolute, and scheme must be equal to 's3'");
         assertThatThrownBy(() -> AwsS3DocumentUri.ofDocumentUri(uri("http://s3.eu-west-1.amazonaws.com/key"),
                                                                 Path.of("")))
               .isInstanceOf(InvalidUriException.class)
               .as("key is mistakenly used as bucket using this format")
               .hasMessage("Invalid 'http://s3.eu-west-1.amazonaws.com/key' URI: Missing document id");
      }

      @Test
      public void ofUriWhenCustomRegion() {
         assertThat(AwsS3DocumentUri.ofDocumentUri(uri("http://bucket.s3.custom.amazonaws.com/key"),
                                                   Path.of(""))).satisfies(uri -> {
            assertThat(uri.region()).isEqualTo("custom");
         });
         assertThat(AwsS3DocumentUri.ofDocumentUri(uri(
               "http://bucket.s3.custom.amazonaws.com/key?region=custom"), Path.of(""))).satisfies(uri -> {
            assertThat(uri.region()).isEqualTo("custom");
         });
         assertThat(AwsS3DocumentUri.ofDocumentUri(uri("s3://bucket/key?region=custom"),
                                                   Path.of(""))).satisfies(uri -> {
            assertThat(uri.region()).isEqualTo("custom");
         });
      }

      @Test
      public void ofUriWhenBadDefaultBucketBasePath() {
         assertThatThrownBy(() -> AwsS3DocumentUri.ofDocumentUri("s3://bucket/key", null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultBucketBasePath' must not be null");
         assertThatThrownBy(() -> AwsS3DocumentUri.ofDocumentUri("s3://bucket/key", Path.of("/")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultBucketBasePath=/' must not be absolute path");
         assertThatThrownBy(() -> AwsS3DocumentUri.ofDocumentUri("s3://bucket/key", Path.of("/path")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage(
                     "Invariant validation error > 'defaultBucketBasePath=/path' must not be absolute path");
         assertThatThrownBy(() -> AwsS3DocumentUri.ofDocumentUri("s3://bucket/path/key", Path.of("../path")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage(
                     "Invariant validation error > 'defaultBucketBasePath=../path' must not have traversal paths");
      }

      @Test
      public void ofUriWhenEmptyDefaultBucketBasePath() {
         assertThat(AwsS3DocumentUri.ofDocumentUri(uri("s3://bucket/path/key"),
                                                   Path.of(""))).satisfies(uri -> {
            assertThat(uri.region()).isEqualTo("us-east-1");
            assertThat(uri.bucket()).isEqualTo("bucket");
            assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/key"));
         });
         assertThat(AwsS3DocumentUri.ofDocumentUri("s3://bucket/path/key", Path.of(""))).satisfies(uri -> {
            assertThat(uri.region()).isEqualTo("us-east-1");
            assertThat(uri.bucket()).isEqualTo("bucket");
            assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/key"));
         });
      }

      @Test
      public void ofUriWhenDefaultBucketBasePath() {
         assertThat(AwsS3DocumentUri.ofDocumentUri(uri("s3://bucket/path/key"), Path.of("path"))).satisfies(
               uri -> {
                  assertThat(uri.region()).isEqualTo("us-east-1");
                  assertThat(uri.bucket()).isEqualTo("bucket");
                  assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path"));
                  assertThat(uri.documentId()).isEqualTo(DocumentPath.of("key"));
               });
         assertThat(AwsS3DocumentUri.ofDocumentUri("s3://bucket/path/key",
                                                   Path.of("path"))).satisfies(uri -> {
            assertThat(uri.region()).isEqualTo("us-east-1");
            assertThat(uri.bucket()).isEqualTo("bucket");
            assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("key"));
         });
      }

      @Test
      public void ofUriWhenMismatchingDefaultBucketBasePath() {
         assertThat(AwsS3DocumentUri.ofDocumentUri(uri("s3://bucket/path/key"),
                                                   Path.of("otherpath"))).satisfies(uri -> {
            assertThat(uri.bucket()).isEqualTo("bucket");
            assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/key"));
         });
         assertThat(AwsS3DocumentUri.ofDocumentUri(uri("s3://bucket/path/key"),
                                                   Path.of("subpath"))).satisfies(uri -> {
            assertThat(uri.bucket()).isEqualTo("bucket");
            assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/key"));
         });
      }

      @Test
      public void ofUriWhenInvalidDefaultBucketBasePath() {
         assertThatThrownBy(() -> AwsS3DocumentUri.ofDocumentUri(uri("s3://bucket/path/key"),
                                                                 Path.of("path/key")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 's3://bucket/path/key' URI: 'bucketBasePath=path/key' parameter must start but not be equal to URI 'path/key' path");
      }

      @Test
      public void ofUriWhenRegionParameter() {
         assertThat(AwsS3DocumentUri.ofDocumentUri(uri("s3://bucket/key?region=eu-west-1"),
                                                   Path.of(""))).satisfies(uri -> {
            assertThat(uri.region()).isEqualTo("eu-west-1");
         });
      }

      @Nested
      class WrappedUri {

         @ParameterizedTest
         @ArgumentsSource(DefaultUriFormatProvider.class)
         public void ofUriWhenNominal(URI uri) {
            assertThat(AwsS3DocumentUri.ofDocumentUri(uri("document:s3:" + uri.resolve("key")),
                                                      Path.of(""))).satisfies(repositoryUri -> {
               assertThat(repositoryUri.region()).isEqualTo("us-east-1");
               assertThat(repositoryUri.bucket()).isEqualTo("bucket");
               assertThat(repositoryUri.bucketBasePath()).isEqualTo(Path.of(""));
               assertThat(repositoryUri.documentId()).isEqualTo(DocumentPath.of("key"));
            });
         }

         @ParameterizedTest
         @ArgumentsSource(DefaultUriFormatProvider.class)
         public void ofUriWhenDefaultBucketBasePath(URI uri) {
            assertThat(AwsS3DocumentUri.ofDocumentUri(uri("document:s3:" + uri.resolve("path/key")),
                                                      Path.of("path"))).satisfies(repositoryUri -> {
               assertThat(repositoryUri.region()).isEqualTo("us-east-1");
               assertThat(repositoryUri.bucket()).isEqualTo("bucket");
               assertThat(repositoryUri.bucketBasePath()).isEqualTo(Path.of("path"));
               assertThat(repositoryUri.documentId()).isEqualTo(DocumentPath.of("key"));
            });
         }

         @ParameterizedTest
         @ArgumentsSource(DefaultUriFormatProvider.class)
         public void ofUriWhenBadRepositoryType(URI uri) {
            assertThatThrownBy(() -> AwsS3DocumentUri.ofDocumentUri(uri("document:unknown:" + uri.resolve(
                  "key")), Path.of("")))
                  .isInstanceOf(IncompatibleUriException.class)
                  .hasMessage("Incompatible 'document:unknown:"
                              + uri.resolve("key")
                              + "' URI: Unsupported 'unknown' repository type");
         }

         @Test
         public void ofUriWhenNormalized() {
            assertThat(AwsS3DocumentUri.ofDocumentUri(uri("document:s3:" + "s3://bucket/key/.././key"),
                                                      Path.of(""))).satisfies(repositoryUri -> {
               assertThat(repositoryUri.region()).isEqualTo("us-east-1");
               assertThat(repositoryUri.bucket()).isEqualTo("bucket");
               assertThat(repositoryUri.bucketBasePath()).isEqualTo(Path.of(""));
               assertThat(repositoryUri.documentId()).isEqualTo(DocumentPath.of("key"));
            });
            assertThat(AwsS3DocumentUri.ofDocumentUri("document:s3:" + "s3://bucket/path/.././key",
                                                      Path.of(""))).satisfies(repositoryUri -> {
               assertThat(repositoryUri.region()).isEqualTo("us-east-1");
               assertThat(repositoryUri.bucket()).isEqualTo("bucket");
               assertThat(repositoryUri.bucketBasePath()).isEqualTo(Path.of(""));
               assertThat(repositoryUri.documentId()).isEqualTo(DocumentPath.of("key"));
            });
            assertThat(AwsS3DocumentUri.ofDocumentUri(AwsS3DocumentUri.ofDocumentUri("document:s3:"
                                                                                     + "s3://bucket/path/.././key",
                                                                                     Path.of("")),
                                                      Path.of(""))).satisfies(repositoryUri -> {
               assertThat(repositoryUri.region()).isEqualTo("us-east-1");
               assertThat(repositoryUri.bucket()).isEqualTo("bucket");
               assertThat(repositoryUri.bucketBasePath()).isEqualTo(Path.of(""));
               assertThat(repositoryUri.documentId()).isEqualTo(DocumentPath.of("key"));

            });
            assertThatThrownBy(() -> AwsS3DocumentUri.ofDocumentUri(uri("document:s3:"
                                                                        + "s3://bucket/../path/key"),
                                                                    Path.of("")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessageContainingAll("Invalid 's3://bucket/../path/key' URI",
                                           "must not have traversal paths");
         }

      }

   }

   @Nested
   public class OfUriWhenDocumentUri {

      @Test
      public void ofUriWhenBadDefaultBucketBasePath() {
         assertThatThrownBy(() -> AwsS3DocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri("s3://bucket/path"),
                                                                 null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultBucketBasePath' must not be null");
         assertThatThrownBy(() -> AwsS3DocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri("s3://bucket/path"),
                                                                 Path.of("/")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'defaultBucketBasePath=/' must not be absolute path");
         assertThatThrownBy(() -> AwsS3DocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri("s3://bucket/path"),
                                                                 Path.of("/path")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage(
                     "Invariant validation error > 'defaultBucketBasePath=/path' must not be absolute path");
         assertThatThrownBy(() -> AwsS3DocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(
               "s3://bucket/path/document"), Path.of("../path")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage(
                     "Invariant validation error > 'defaultBucketBasePath=../path' must not have traversal paths");
      }

      @Nested
      public class WhenDocumentUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(AwsS3DocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri("s3://bucket/path/document"),
                                                      Path.of("path"))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
         }

         @Test
         public void ofUriWhenImplicitDefaultBucketBasePath() {
            assertThat(AwsS3DocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(
                  "s3://bucket/path/subpath/document"))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/subpath/document"));
            });
         }

         @Test
         public void ofUriWhenMismatchingDefaultStoragePathAndBasePath() {
            assertThat(AwsS3DocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri("s3://bucket/path/document"),
                                                      Path.of("otherpath")))
                  .as("Default bucket base path is only applied if it matches actual URI")
                  .satisfies(uri -> {
                     assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
                  });
            assertThatThrownBy(() -> AwsS3DocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(
                  "s3://bucket/path/subpath/document"), Path.of("path/subpath/document")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 's3://bucket/path/subpath/document' URI: 'bucketBasePath=path/subpath/document' parameter must start but not be equal to URI 'path/subpath/document' path");
         }

         @Test
         public void ofUriWhenDefaultBucketBasePath() {
            assertThat(AwsS3DocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(
                  "s3://bucket/path/subpath/document"), Path.of("path"))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("subpath/document"));
            });
            assertThat(AwsS3DocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(
                  "s3://bucket/path/subpath/document"), Path.of("path/subpath"))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path/subpath"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
            assertThat(AwsS3DocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(
                  "s3://bucket/path/subpath/document"), Path.of(""))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/subpath/document"));
            });
         }

         @Test
         public void ofUriWhenNoBucketBasePath() {
            assertThat(AwsS3DocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri("s3://bucket/document"))).satisfies(
                  uri -> {
                     assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
                     assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
                  });
            assertThat(AwsS3DocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri("s3://bucket/document"),
                                                      Path.of(""))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
            assertThatThrownBy(() -> AwsS3DocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri("s3://bucket/")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage("Invalid 's3://bucket/' URI: Missing document id");
         }

         @Test
         public void ofUriWhenExplicitBucketBasePath() {
            assertThat(AwsS3DocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(
                  "s3://bucket/path/subpath/document?bucketBasePath=path/subpath"))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path/subpath"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
            assertThat(AwsS3DocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(
                  "s3://bucket/path/subpath/document?bucketBasePath=path"))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("subpath/document"));
            });
            assertThat(AwsS3DocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(
                  "s3://bucket/path/subpath/document?bucketBasePath="))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/subpath/document"));
            });
         }

         @Test
         public void ofUriWhenMismatchingExplicitBucketBasePath() {
            assertThatThrownBy(() -> AwsS3DocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(
                  "s3://bucket/path/subpath/document?bucketBasePath=otherpath")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 's3://bucket/path/subpath/document?bucketBasePath=otherpath' URI: 'bucketBasePath=otherpath' parameter must start but not be equal to URI 'path/subpath/document' path");
            assertThatThrownBy(() -> AwsS3DocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(
                  "s3://bucket/path/subpath/document?bucketBasePath=subpath")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 's3://bucket/path/subpath/document?bucketBasePath=subpath' URI: 'bucketBasePath=subpath' parameter must start but not be equal to URI 'path/subpath/document' path");
            assertThatThrownBy(() -> AwsS3DocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(
                  "s3://bucket/path/subpath/document?bucketBasePath=path/subpath/document")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 's3://bucket/path/subpath/document?bucketBasePath=path/subpath/document' URI: 'bucketBasePath=path/subpath/document' parameter must start but not be equal to URI 'path/subpath/document' path");
         }

      }

      @Nested
      public class WhenAwsS3DocumentUri {

         @Test
         public void ofUriWhenNominal() {
            assertThat(AwsS3DocumentUri.ofDocumentUri(AwsS3DocumentUri.ofDocumentUri(
                  "s3://bucket/path/document",
                  Path.of("path")))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path"));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("document"));
            });
         }

         @Test
         public void ofUriWhenImplicitDefaultBucketBasePath() {
            assertThat(AwsS3DocumentUri.ofDocumentUri(AwsS3DocumentUri.ofDocumentUri(
                  "s3://bucket/path/subpath/document"))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/subpath/document"));
            });
         }

         @Test
         public void ofUriWhenDefaultBucketBasePath() {
            assertThat(AwsS3DocumentUri.ofDocumentUri(AwsS3DocumentUri.ofDocumentUri(
                  "s3://bucket/path/subpath/document"), Path.of("path"))).satisfies(uri -> {
               assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
               assertThat(uri.documentId()).isEqualTo(DocumentPath.of("path/subpath/document"));
            });
         }

      }
   }

   @Nested
   public class OfUriWhenVariousUriFormat {

      @ParameterizedTest
      @ArgumentsSource(DefaultUriFormatProvider.class)
      public void ofUriWhenNominal(URI uri) {
         assertThat(AwsS3DocumentUri.ofDocumentUri(uri.resolve("key"),
                                                   Path.of(""))).satisfies(repositoryUri -> {
            assertThat(repositoryUri.region()).isEqualTo("us-east-1");
            assertThat(repositoryUri.bucket()).isEqualTo("bucket");
            assertThat(repositoryUri.bucketBasePath()).isEqualTo(Path.of(""));
            assertThat(repositoryUri.documentId()).isEqualTo(DocumentPath.of("key"));
         });

         assertThat(AwsS3DocumentUri.ofDocumentUri(uri.resolve("path/key"), Path.of(""))).satisfies(
               repositoryUri -> {
                  assertThat(repositoryUri.region()).isEqualTo("us-east-1");
                  assertThat(repositoryUri.bucket()).isEqualTo("bucket");
                  assertThat(repositoryUri.bucketBasePath()).isEqualTo(Path.of(""));
                  assertThat(repositoryUri.documentId()).isEqualTo(DocumentPath.of("path/key"));
               });
      }

      @ParameterizedTest
      @ArgumentsSource(DefaultUriFormatProvider.class)
      public void ofUriWhenIllegalArgument(URI uri) {
         assertThatExceptionOfType(InvalidUriException.class)
               .isThrownBy(() -> AwsS3DocumentUri.ofDocumentUri(uri.resolve("key?listingContentMode=UNKNOWN"),
                                                                Path.of("")))
               .withMessage(
                     "Invalid '%s' URI: Conversion from 'java.lang.String' to 'com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig$S3ListingContentMode' failed for 'UNKNOWN' value",
                     uri.resolve("key?listingContentMode=UNKNOWN"));
      }

      @ParameterizedTest
      @ArgumentsSource(DefaultUriFormatProvider.class)
      public void ofUriWhenCaseSensitiveParameterKey(URI uri) {
         assertThat(AwsS3DocumentUri.ofDocumentUri(uri.resolve("key?ReGiOn=eu-west-1"),
                                                   Path.of(""))).satisfies(repositoryUri -> {
            assertThat(repositoryUri.region()).isEqualTo("us-east-1");
         });
      }

      @ParameterizedTest
      @ArgumentsSource(DefaultUriFormatProvider.class)
      public void ofUriWhenInvalidBucketBasePathParameter(URI uri) {
         assertThatThrownBy(() -> AwsS3DocumentUri.ofDocumentUri(uri.resolve(
               "path/subpath/key?bucketBasePath=path/subpath/key"), Path.of("")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid '%s' URI: 'bucketBasePath=path/subpath/key' parameter must start but not be equal to URI 'path/subpath/key' path",
                     uri.resolve("path/subpath/key?bucketBasePath=path/subpath/key"));
         assertThatThrownBy(() -> AwsS3DocumentUri.ofDocumentUri(uri.resolve(
               "path/subpath/key?bucketBasePath=/path/subpath"), Path.of("")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid '%s' URI: 'bucketBasePath=/path/subpath' parameter must be relative and must not contain traversal",
                     uri.resolve("path/subpath/key?bucketBasePath=/path/subpath"));
         assertThatThrownBy(() -> AwsS3DocumentUri.ofDocumentUri(uri.resolve(
               "path/subpath/key?bucketBasePath=../path"), Path.of("")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid '%s' URI: 'bucketBasePath=../path' parameter must be relative and must not contain traversal",
                     uri.resolve("path/subpath/key?bucketBasePath=../path"));
         assertThatThrownBy(() -> AwsS3DocumentUri.ofDocumentUri(uri.resolve(
               "path/subpath/key?bucketBasePath=path/subpath/key/."), Path.of("")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid '%s' URI: 'bucketBasePath=path/subpath/key' parameter must start but not be equal to URI 'path/subpath/key' path",
                     uri.resolve("path/subpath/key?bucketBasePath=path/subpath/key/."));
      }

      @ParameterizedTest
      @ArgumentsSource(DefaultUriFormatProvider.class)
      public void ofUriWhenExactBucketBasePathParameter(URI uri) {
         assertThat(AwsS3DocumentUri.ofDocumentUri(uri.resolve("path/subpath/key?bucketBasePath=path/subpath"),
                                                   Path.of(""))).satisfies(repositoryUri -> {
            assertThat(repositoryUri.bucket()).isEqualTo("bucket");
            assertThat(repositoryUri.bucketBasePath()).isEqualTo(Path.of("path/subpath"));
            assertThat(repositoryUri.documentId()).isEqualTo(DocumentPath.of("key"));
         });
         assertThat(AwsS3DocumentUri.ofDocumentUri(uri.resolve(
                                                         "path/subpath/key?bucketBasePath=./path/./subpath/."),
                                                   Path.of(""))).satisfies(repositoryUri -> {
            assertThat(repositoryUri.bucket()).isEqualTo("bucket");
            assertThat(repositoryUri.bucketBasePath()).isEqualTo(Path.of("path/subpath"));
            assertThat(repositoryUri.documentId()).isEqualTo(DocumentPath.of("key"));
         });
      }

      @ParameterizedTest
      @ArgumentsSource(DefaultUriFormatProvider.class)
      public void ofUriWhenPartialBucketBasePathParameter(URI uri) {
         assertThat(AwsS3DocumentUri.ofDocumentUri(uri.resolve("path/subpath/key?bucketBasePath=path"),
                                                   Path.of(""))).satisfies(repositoryUri -> {
            assertThat(repositoryUri.bucket()).isEqualTo("bucket");
            assertThat(repositoryUri.bucketBasePath()).isEqualTo(Path.of("path"));
            assertThat(repositoryUri.documentId()).isEqualTo(DocumentPath.of("subpath/key"));
         });
         assertThat(AwsS3DocumentUri.ofDocumentUri(uri.resolve("path/subpath/key?bucketBasePath=path/."),
                                                   Path.of(""))).satisfies(repositoryUri -> {
            assertThat(repositoryUri.bucket()).isEqualTo("bucket");
            assertThat(repositoryUri.bucketBasePath()).isEqualTo(Path.of("path"));
            assertThat(repositoryUri.documentId()).isEqualTo(DocumentPath.of("subpath/key"));
         });
      }

      @ParameterizedTest
      @ArgumentsSource(DefaultUriFormatProvider.class)
      public void ofUriWhenMismatchingBucketBasePathParameter(URI uri) {
         assertThatThrownBy(() -> AwsS3DocumentUri.ofDocumentUri(uri.resolve(
               "path/subpath/key?bucketBasePath=otherpath"), Path.of("")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid '%s' URI: 'bucketBasePath=otherpath' parameter must start but not be equal to URI 'path/subpath/key' path",
                     uri.resolve("path/subpath/key?bucketBasePath=otherpath"));
         assertThatThrownBy(() -> AwsS3DocumentUri.ofDocumentUri(uri.resolve(
               "path/subpath/key?bucketBasePath=path/subpath/otherpath"), Path.of("")))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid '%s' URI: 'bucketBasePath=path/subpath/otherpath' parameter must start but not be equal to URI 'path/subpath/key' path",
                     uri.resolve("path/subpath/key?bucketBasePath=path/subpath/otherpath"));
      }

      @ParameterizedTest
      @ArgumentsSource(DefaultUriFormatProvider.class)
      public void ofUriWhenSameRegionParameter(URI uri) {
         assertThat(AwsS3DocumentUri.ofDocumentUri(uri.resolve("key?region=us-east-1"),
                                                   Path.of(""))).satisfies(repositoryUri -> {
            assertThat(repositoryUri.region()).isEqualTo("us-east-1");
            assertThat(repositoryUri.bucket()).isEqualTo("bucket");
            assertThat(repositoryUri.documentId()).isEqualTo(DocumentPath.of("key"));
         });
      }

      @ParameterizedTest
      @ArgumentsSource(DefaultUriFormatProvider.class)
      public void ofUriDifferentRegionParameter(URI uri) {
         try {
            assertThat(AwsS3DocumentUri.ofDocumentUri(uri.resolve("key?region=eu-west-1"),
                                                      Path.of(""))).satisfies(repositoryUri -> {
               assertThat(repositoryUri.region()).isEqualTo("eu-west-1");
               assertThat(repositoryUri.bucket()).isEqualTo("bucket");
               assertThat(repositoryUri.documentId()).isEqualTo(DocumentPath.of("key"));
            });
         } catch (InvalidUriException e) {
            assertThat(uri.toString()).contains("us-east-1");
            assertThat(e).hasMessage(
                  "Invalid '%s' URI: 'region=eu-west-1' parameter must match URI 'us-east-1' region",
                  uri.resolve("key?region=eu-west-1"));
         }
      }

      @ParameterizedTest
      @ArgumentsSource(DefaultUriFormatProvider.class)
      public void ofUriWhenDefaultParameters(URI uri) {
         assertThat(AwsS3DocumentUri.ofDocumentUri(uri.resolve("key"),
                                                   Path.of(""))).satisfies(repositoryUri -> {
            assertThat(repositoryUri.region()).isEqualTo(defaultRegion().id());
            assertThat(repositoryUri.query().parameter(REGION, alwaysTrue())).hasValue(defaultRegion().id());
            assertThat(repositoryUri.query().parameter(PATH_STYLE_ACCESS, alwaysTrue())).hasValue(
                  DEFAULT_PATH_STYLE_ACCESS);
            assertThat(repositoryUri.query().parameter(URI_GENERATION_FORMAT, alwaysTrue())).hasValue(
                  DEFAULT_URI_GENERATION_FORMAT);
            assertThat(repositoryUri.query().parameter(ENDPOINT, alwaysTrue())).isEmpty();
            assertThat(repositoryUri.bucketBasePath()).isEqualTo(Path.of(""));
            assertThat(repositoryUri.query().parameter(CREATE_IF_MISSING_BUCKET, alwaysTrue())).hasValue(
                  DEFAULT_CREATE_IF_MISSING_BUCKET);
            assertThat(repositoryUri.query().parameter(FAIL_FAST_IF_MISSING_BUCKET, alwaysTrue())).hasValue(
                  DEFAULT_FAIL_FAST_IF_MISSING_BUCKET);
            assertThat(repositoryUri.query().parameter(LISTING_CONTENT_MODE, alwaysTrue())).hasValue(
                  DEFAULT_LISTING_CONTENT_MODE);
            assertThat(repositoryUri.query().parameter(LISTING_MAX_KEYS, alwaysTrue())).hasValue(
                  DEFAULT_LISTING_MAX_KEYS);
            assertThat(repositoryUri.query().parameter(UPLOAD_CHUNK_SIZE, alwaysTrue())).hasValue(
                  DEFAULT_UPLOAD_CHUNK_SIZE);
            assertThat(repositoryUri.query().parameter(LOAD_ALL_TAGS, alwaysTrue())).hasValue(
                  DEFAULT_LOAD_ALL_TAGS);
            assertThat(repositoryUri.query().parameters(MATCH_TAGS, alwaysTrue())).isEmpty();
         });
      }

      @ParameterizedTest
      @ArgumentsSource(DefaultUriFormatProvider.class)
      public void ofUriWhenAllParameters(URI uri) {
         assertThat(AwsS3DocumentUri.ofDocumentUri(uri.resolve("path/key"
                                                               + "?region=us-east-1"
                                                               + "&pathStyleAccess=true"
                                                               + "&uriGenerationFormat=S3"
                                                               + "&endpoint=http://localhost"
                                                               + "&bucketBasePath=path"
                                                               + "&createIfMissingBucket=true"
                                                               + "&failFastIfMissingBucket=false"
                                                               + "&listingContentMode=FILTERING_COMPLETE"
                                                               + "&listingMaxKeys=50"
                                                               + "&uploadChunkSize=5242881"
                                                               + "&loadAllTags=true"
                                                               + "&matchTags=pattern1"
                                                               + "&matchTags=pattern2"),
                                                   Path.of(""))).satisfies(repositoryUri -> {
            assertThat(repositoryUri.region()).isEqualTo("us-east-1");
            assertThat(repositoryUri.bucketBasePath()).isEqualTo(Path.of("path"));

            assertThat(repositoryUri.query().parameter(REGION, alwaysFalse())).hasValue("us-east-1");
            assertThat(repositoryUri.query().parameter(PATH_STYLE_ACCESS, alwaysFalse())).hasValue(true);
            assertThat(repositoryUri.query().parameter(URI_GENERATION_FORMAT, alwaysFalse())).hasValue(S3);
            assertThat(repositoryUri.query().parameter(ENDPOINT, alwaysFalse())).hasValue(uri(
                  "http://localhost"));
            assertThat(repositoryUri.query()
                             .parameter(CREATE_IF_MISSING_BUCKET, alwaysFalse())).hasValue(true);
            assertThat(repositoryUri.query().parameter(FAIL_FAST_IF_MISSING_BUCKET, alwaysFalse())).hasValue(
                  false);
            assertThat(repositoryUri.query().parameter(LISTING_CONTENT_MODE, alwaysFalse())).hasValue(
                  FILTERING_COMPLETE);
            assertThat(repositoryUri.query().parameter(LISTING_MAX_KEYS, alwaysFalse())).hasValue(50);
            assertThat(repositoryUri.query().parameter(UPLOAD_CHUNK_SIZE, alwaysFalse())).hasValue(5242881);
            assertThat(repositoryUri.query().parameter(LOAD_ALL_TAGS, alwaysFalse())).hasValue(true);
            assertThat(repositoryUri.query().parameters(MATCH_TAGS, alwaysFalse()))
                  .usingElementComparator(comparing(Pattern::pattern))
                  .containsExactly(Pattern.compile("pattern1"), Pattern.compile("pattern2"));
         });
      }
   }

   @Nested
   public class OfConfig {

      @Test
      public void ofConfigWhenNominal() {
         assertThat(AwsS3DocumentUri.ofConfig(new AwsS3DocumentConfigBuilder().bucket("bucket").build(),
                                              DocumentPath.of("key"))).satisfies(uri -> {
            assertThat(uri.region()).isEqualTo(defaultRegion().id());
            assertThat(uri.bucket()).isEqualTo("bucket");
            assertThat(uri.bucketBasePath()).isEqualTo(Path.of(""));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("key"));

            assertThat(uri.query().parameter(REGION, alwaysTrue())).hasValue(defaultRegion().id());
            assertThat(uri.query().parameter(PATH_STYLE_ACCESS, alwaysTrue())).hasValue(
                  DEFAULT_PATH_STYLE_ACCESS);
            assertThat(uri.query().parameter(URI_GENERATION_FORMAT, alwaysTrue())).hasValue(
                  DEFAULT_URI_GENERATION_FORMAT);
            assertThat(uri.query().parameter(ENDPOINT, alwaysTrue())).isEmpty();
            assertThat(uri.query().parameter(CREATE_IF_MISSING_BUCKET, alwaysTrue())).hasValue(
                  DEFAULT_CREATE_IF_MISSING_BUCKET);
            assertThat(uri.query().parameter(FAIL_FAST_IF_MISSING_BUCKET, alwaysTrue())).hasValue(
                  DEFAULT_FAIL_FAST_IF_MISSING_BUCKET);
            assertThat(uri.query().parameter(LISTING_CONTENT_MODE, alwaysTrue())).hasValue(
                  DEFAULT_LISTING_CONTENT_MODE);
            assertThat(uri.query().parameter(LISTING_MAX_KEYS, alwaysTrue())).hasValue(
                  DEFAULT_LISTING_MAX_KEYS);
            assertThat(uri.query().parameter(UPLOAD_CHUNK_SIZE, alwaysTrue())).hasValue(
                  DEFAULT_UPLOAD_CHUNK_SIZE);
            assertThat(uri.query().parameter(LOAD_ALL_TAGS, alwaysTrue())).hasValue(DEFAULT_LOAD_ALL_TAGS);
            assertThat(uri.query().parameters(MATCH_TAGS, alwaysTrue())).isEmpty();
         });
      }

      @Test
      public void ofConfigWhenAllParameters() {
         assertThat(AwsS3DocumentUri.ofConfig(new AwsS3DocumentConfigBuilder()
                                                    .region("eu-west-1")
                                                    .bucket("bucket")
                                                    .pathStyleAccess(true)
                                                    .uriGenerationFormat(S3)
                                                    .endpoint(uri("http://localhost"))
                                                    .bucketBasePath(Path.of("path"))
                                                    .createIfMissingBucket(true)
                                                    .failFastIfMissingBucket(false)
                                                    .listingContentMode(FILTERING_COMPLETE)
                                                    .listingMaxKeys(50)
                                                    .uploadChunkSize(5242881)
                                                    .loadAllTags(true)
                                                    .matchTags(Pattern.compile("pattern1"),
                                                               Pattern.compile("pattern2"))
                                                    .build(), DocumentPath.of("key"))).satisfies(uri -> {
            assertThat(uri.region()).isEqualTo("eu-west-1");
            assertThat(uri.bucket()).isEqualTo("bucket");
            assertThat(uri.bucketBasePath()).isEqualTo(Path.of("path"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("key"));

            assertThat(uri.query().parameter(REGION, alwaysFalse())).hasValue("eu-west-1");
            assertThat(uri.query().parameter(PATH_STYLE_ACCESS, alwaysFalse())).hasValue(true);
            assertThat(uri.query().parameter(URI_GENERATION_FORMAT, alwaysFalse())).hasValue(S3);
            assertThat(uri.query().parameter(ENDPOINT, alwaysFalse())).hasValue(uri("http://localhost"));
            assertThat(uri.query().parameter(CREATE_IF_MISSING_BUCKET, alwaysFalse())).hasValue(true);
            assertThat(uri.query().parameter(FAIL_FAST_IF_MISSING_BUCKET, alwaysFalse())).hasValue(false);
            assertThat(uri
                             .query()
                             .parameter(LISTING_CONTENT_MODE, alwaysFalse())).hasValue(FILTERING_COMPLETE);
            assertThat(uri.query().parameter(LISTING_MAX_KEYS, alwaysFalse())).hasValue(50);
            assertThat(uri.query().parameter(UPLOAD_CHUNK_SIZE, alwaysFalse())).hasValue(5242881);
            assertThat(uri.query().parameter(LOAD_ALL_TAGS, alwaysFalse())).hasValue(true);
            assertThat(uri.query().parameters(MATCH_TAGS, alwaysFalse()))
                  .usingElementComparator(comparing(Pattern::pattern))
                  .containsExactly(Pattern.compile("pattern1"), Pattern.compile("pattern2"));
         });
      }

   }

   @Nested
   public class ToUri {

      @ParameterizedTest
      @ArgumentsSource(DefaultUriFormatProvider.class)
      public void toUriWhenNominal(URI uri) {
         assertThat(AwsS3DocumentUri
                          .ofDocumentUri(uri.resolve("key"), Path.of(""))
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo(securedHttp(uri) + "://bucket.s3.amazonaws.com/key");
      }

      @ParameterizedTest
      @ArgumentsSource(DefaultUriFormatProvider.class)
      public void toUriWhenBucketBasePath(URI uri) {
         assertThat(AwsS3DocumentUri
                          .ofDocumentUri(uri.resolve("path/key"), Path.of(""))
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo(securedHttp(uri) + "://bucket.s3.amazonaws.com/path/key");
         assertThat(AwsS3DocumentUri
                          .ofDocumentUri(uri.resolve("path/key"), Path.of("path"))
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo(securedHttp(uri) + "://bucket.s3.amazonaws.com/path/key");
      }

      @ParameterizedTest
      @ArgumentsSource(DefaultUriFormatProvider.class)
      public void toUriWhenAwsS3UriOptions(URI uri) {
         assertThat(AwsS3DocumentUri
                          .ofDocumentUri(uri.resolve("key"), Path.of(""))
                          .exportUri(AwsS3ExportUriOptions.noParameters(S3))
                          .stringValue()).isEqualTo("s3://bucket/key");
         assertThat(AwsS3DocumentUri
                          .ofDocumentUri(uri.resolve("key"), Path.of(""))
                          .exportUri(AwsS3ExportUriOptions.noParameters(VIRTUAL_HOST_STYLE))
                          .stringValue()).isEqualTo(securedHttp(uri) + "://bucket.s3.amazonaws.com/key");
         assertThat(AwsS3DocumentUri
                          .ofDocumentUri(uri.resolve("key"), Path.of(""))
                          .exportUri(AwsS3ExportUriOptions.noParameters(PATH_STYLE))
                          .stringValue()).isEqualTo(securedHttp(uri) + "://s3.amazonaws.com/bucket/key");
      }

      @ParameterizedTest
      @ArgumentsSource(DefaultUriFormatProvider.class)
      public void toUriWhenS3Cli(URI uri) {
         assertThat(AwsS3DocumentUri
                          .ofDocumentUri(uri.resolve("key"), Path.of(""))
                          .exportUri(AwsS3ExportUriOptions.noParameters(S3))
                          .stringValue()).isEqualTo("s3://bucket/key");
         assertThat(AwsS3DocumentUri
                          .ofDocumentUri(uri.resolve("path/key"), Path.of("path"))
                          .exportUri(AwsS3ExportUriOptions.noParameters(S3))
                          .stringValue()).isEqualTo("s3://bucket/path/key");
      }

      @ParameterizedTest
      @ArgumentsSource(DefaultUriFormatProvider.class)
      public void toUriWhenVirtualHostStyle(URI uri) {
         assertThat(AwsS3DocumentUri
                          .ofDocumentUri(uri.resolve("key"), Path.of(""))
                          .exportUri(AwsS3ExportUriOptions.noParameters(VIRTUAL_HOST_STYLE))
                          .stringValue()).isEqualTo(securedHttp(uri) + "://bucket.s3.amazonaws.com/key");
         assertThat(AwsS3DocumentUri
                          .ofDocumentUri(uri.resolve("path/key"), Path.of("path"))
                          .exportUri(AwsS3ExportUriOptions.noParameters(VIRTUAL_HOST_STYLE))
                          .stringValue()).isEqualTo(securedHttp(uri) + "://bucket.s3.amazonaws.com/path/key");
      }

      @ParameterizedTest
      @ArgumentsSource(DefaultUriFormatProvider.class)
      public void toUriWhenPathStyle(URI uri) {
         assertThat(AwsS3DocumentUri
                          .ofDocumentUri(uri.resolve("key"), Path.of(""))
                          .exportUri(AwsS3ExportUriOptions.noParameters(PATH_STYLE))
                          .stringValue()).isEqualTo(securedHttp(uri) + "://s3.amazonaws.com/bucket/key");
         assertThat(AwsS3DocumentUri
                          .ofDocumentUri(uri.resolve("path/key"), Path.of("path"))
                          .exportUri(AwsS3ExportUriOptions.noParameters(PATH_STYLE))
                          .stringValue()).isEqualTo(securedHttp(uri) + "://s3.amazonaws.com/bucket/path/key");
      }

      @ParameterizedTest
      @ArgumentsSource(DefaultUriFormatProvider.class)
      public void toUriWhenDefaultParameters(URI uri) {
         assertThat(AwsS3DocumentUri
                          .ofDocumentUri(uri.resolve("path/key"), Path.of(""))
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo(securedHttp(uri) + "://bucket.s3.amazonaws.com/path/key");
         assertThat(AwsS3DocumentUri
                          .ofDocumentUri(uri.resolve("path/key"), Path.of("path"))
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo(securedHttp(uri)
                                                    + "://bucket.s3.amazonaws.com/path/key?bucketBasePath=path");
         assertThat(AwsS3DocumentUri
                          .ofDocumentUri(uri.resolve("path/key?failFastIfMissingBucket=true"),
                                         Path.of("path"))
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo(securedHttp(uri)
                                                    + "://bucket.s3.amazonaws.com/path/key?bucketBasePath=path");
         assertThat(AwsS3DocumentUri
                          .ofDocumentUri(uri.resolve("path/key?failFastIfMissingBucket=false"),
                                         Path.of("path"))
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo(securedHttp(uri)
                                                    + "://bucket.s3.amazonaws.com/path/key?failFastIfMissingBucket=false&bucketBasePath=path");
      }

      @ParameterizedTest
      @ArgumentsSource(DefaultUriFormatProvider.class)
      public void toUriWhenDefaultParametersWithForceDefaultValues(URI uri) {
         assertThat(AwsS3DocumentUri
                          .ofDocumentUri(uri.resolve("path/key"), Path.of("path"))
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo(securedHttp(uri)
                                                    + "://bucket.s3.amazonaws.com/path/key?pathStyleAccess=false&bucketBasePath=path&createIfMissingBucket=false&failFastIfMissingBucket=true&listingContentMode=FAST&listingMaxKeys=1000&uploadChunkSize=5242880&loadAllTags=false");
         assertThat(AwsS3DocumentUri
                          .ofDocumentUri(uri.resolve("path/key?failFastIfMissingBucket=true"),
                                         Path.of("path"))
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo(securedHttp(uri)
                                                    + "://bucket.s3.amazonaws.com/path/key?failFastIfMissingBucket=true&pathStyleAccess=false&bucketBasePath=path&createIfMissingBucket=false&listingContentMode=FAST&listingMaxKeys=1000&uploadChunkSize=5242880&loadAllTags=false");
         assertThat(AwsS3DocumentUri
                          .ofDocumentUri(uri.resolve("path/key?failFastIfMissingBucket=false"),
                                         Path.of("path"))
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo(securedHttp(uri)
                                                    + "://bucket.s3.amazonaws.com/path/key?failFastIfMissingBucket=false&pathStyleAccess=false&bucketBasePath=path&createIfMissingBucket=false&listingContentMode=FAST&listingMaxKeys=1000&uploadChunkSize=5242880&loadAllTags=false");
      }

      @ParameterizedTest
      @ArgumentsSource(DefaultUriFormatProvider.class)
      public void toUriWhenSensitiveParameters(URI uri) {
         assertThat(AwsS3DocumentUri
                          .ofDocumentUri(uri.resolve("path/key"), Path.of(""))
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo(securedHttp(uri) + "://bucket.s3.amazonaws.com/path/key");
         assertThat(AwsS3DocumentUri
                          .ofDocumentUri(uri.resolve("path/key"), Path.of("path"))
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo(securedHttp(uri)
                                                    + "://bucket.s3.amazonaws.com/path/key?bucketBasePath=path");
      }

      private String securedHttp(URI uri) {
         return uri.getScheme().equals("http") ? "http" : "https";
      }

   }

}