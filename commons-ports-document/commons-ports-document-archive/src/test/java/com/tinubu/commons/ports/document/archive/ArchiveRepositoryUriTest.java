/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.archive;

import static com.tinubu.commons.ddd2.uri.Uri.uri;
import static com.tinubu.commons.lang.util.CheckedPredicate.alwaysTrue;
import static com.tinubu.commons.lang.util.CollectionUtils.entry;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.PathUtils.emptyPath;
import static com.tinubu.commons.ports.document.archive.ArchiveDocumentConfig.ArchiveDocumentConfigBuilder;
import static com.tinubu.commons.ports.document.archive.ArchiveRepositoryUri.Parameters.BASE_PATH;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.defaultParameters;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.noParameters;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.sensitiveParameters;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.net.URI;
import java.nio.file.Path;
import java.util.LinkedHashMap;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ddd2.uri.uris.ArchiveUri;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.ReferencedDocument;
import com.tinubu.commons.ports.document.domain.repository.TestDocumentRepository;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.RepositoryUri;

public class ArchiveRepositoryUriTest {

   @Nested
   public class OfUriWhenURI {

      @Test
      public void ofUriWhenNominal() {
         assertThat(ArchiveRepositoryUri.ofRepositoryUri(uri("zip:test:/document.zip"))).satisfies(uri -> {
            assertThat(uri.archiveScheme()).isEqualTo("zip");
            assertThat(uri.targetUri()).isEqualTo(DocumentUri.ofDocumentUri("test:/document.zip"));
            assertThat(uri.basePath()).isEqualTo(emptyPath());
         });
         assertThat(ArchiveRepositoryUri.ofRepositoryUri("zip:test:/document.zip")).satisfies(uri -> {
            assertThat(uri.archiveScheme()).isEqualTo("zip");
            assertThat(uri.targetUri()).isEqualTo(DocumentUri.ofDocumentUri("test:/document.zip"));
            assertThat(uri.basePath()).isEqualTo(emptyPath());
         });
      }

      @Test
      public void ofUriWhenUri() {
         assertThat(ArchiveRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(Uri.ofUri(
               "zip:test:/document.zip")))).satisfies(uri -> {
            assertThat(uri.targetUri()).isEqualTo(DocumentUri.ofDocumentUri("test:/document.zip"));
         });
         assertThat(ArchiveRepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri(Uri.ofUri(
               "zip:test:/document.zip!/archive/entry")))).satisfies(uri -> {
            assertThat(uri.targetUri()).isEqualTo(DocumentUri.ofDocumentUri("test:/document.zip"));
         });
         assertThat(ArchiveRepositoryUri.ofRepositoryUri(Uri.ofUri("zip:test:/document.zip"))).satisfies(uri -> {
            assertThat(uri.targetUri()).isEqualTo(DocumentUri.ofDocumentUri("test:/document.zip"));
         });
      }

      @Test
      public void ofUriWhenArchiveUri() {
         assertThat(ArchiveRepositoryUri.ofRepositoryUri(RepositoryUri.ofRepositoryUri(ArchiveUri.ofUri(
               "zip:test:/document.zip")))).satisfies(uri -> {
            assertThat(uri.targetUri()).isEqualTo(DocumentUri.ofDocumentUri("test:/document.zip"));
         });
         assertThat(ArchiveRepositoryUri.ofRepositoryUri(DocumentUri.ofDocumentUri(ArchiveUri.ofUri(
               "zip:test:/document.zip!/archive/entry")))).satisfies(uri -> {
            assertThat(uri.targetUri()).isEqualTo(DocumentUri.ofDocumentUri("test:/document.zip"));
         });
         assertThat(ArchiveRepositoryUri.ofRepositoryUri(ArchiveUri.ofUri("zip:test:/document.zip"))).satisfies(
               uri -> {
                  assertThat(uri.targetUri()).isEqualTo(DocumentUri.ofDocumentUri("test:/document.zip"));
               });
      }

      @Test
      public void ofUriWhenBadParameters() {
         assertThatThrownBy(() -> ArchiveRepositoryUri.ofRepositoryUri((URI) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
         assertThatThrownBy(() -> ArchiveRepositoryUri.ofRepositoryUri((String) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
         assertThatThrownBy(() -> ArchiveRepositoryUri.ofRepositoryUri((RepositoryUri) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
      }

      @Test
      @Disabled("not testable currently")
      public void ofUriWhenIllegalArgument() {
         assertThatExceptionOfType(InvalidUriException.class)
               .isThrownBy(() -> ArchiveRepositoryUri.ofRepositoryUri(
                     "zip:test:/document.zip?compressionLevel=UNKNOWN"))
               .withMessage(
                     "Invalid '%s' URI: Conversion from 'java.lang.String' to 'CompressionLevel' failed for 'UNKNOWN' value",
                     "zip:test:/document.zip?compressionLevel=UNKNOWN");
      }

      @Test
      public void ofUriWhenCaseSensitiveParameterKey() {
         assertThat(ArchiveRepositoryUri.ofRepositoryUri("zip:test:/document.zip!?BaSePaTh=other")).satisfies(
               uri -> {
                  assertThat(uri.basePath()).isEqualTo(emptyPath());
                  assertThat(uri.query().parameter(BASE_PATH, alwaysTrue())).hasValue(emptyPath());
               });
      }

      @Test
      public void ofUriWhenNormalized() {
         assertThat(ArchiveRepositoryUri.ofRepositoryUri(uri("zip:test:/../document.zip"))).satisfies(uri -> {
            assertThat(uri.targetUri()).isEqualTo(DocumentUri.ofDocumentUri("test:/../document.zip"));
         });
         assertThat(ArchiveRepositoryUri.ofRepositoryUri(uri("zip:test:/../document.zip!/../archive/entry"))).satisfies(
               uri -> {
                  assertThat(uri.targetUri()).isEqualTo(DocumentUri.ofDocumentUri("test:/../document.zip"));
               });
      }

      @Test
      public void ofUriWhenInvalidUriSyntax() {
         assertThatThrownBy(() -> ArchiveRepositoryUri.ofRepositoryUri("git@git.com:user/repo.git"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'git@git.com:user/repo.git' URI: Illegal character in scheme name at index 3: git@git.com:user/repo.git");
         assertThatThrownBy(() -> ArchiveRepositoryUri.ofRepositoryUri("git@git.com:user/repo.git"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'git@git.com:user/repo.git' URI: Illegal character in scheme name at index 3: git@git.com:user/repo.git");
      }

      @Test
      public void ofUriWhenIncompatibleUri() {
         assertThatThrownBy(() -> ArchiveRepositoryUri.ofRepositoryUri("unknown:/path"))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessage(
                     "Incompatible 'unknown:/path' URI: 'ArchiveUri[value=unknown:/path,restrictions=ArchiveUriRestrictions[schemes=[zip, jar], archiveEntry=false, archiveQuery=false],scheme=SimpleScheme[scheme='unknown'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='/path', encodedSchemeSpecific='/path']]' must be opaque");
         assertThatThrownBy(() -> ArchiveRepositoryUri.ofRepositoryUri("unknown:path"))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessage("Incompatible 'unknown:path' URI: 'scheme=unknown' must be in [zip,jar]");
         assertThatThrownBy(() -> ArchiveRepositoryUri.ofRepositoryUri("zip://host/path"))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessage(
                     "Incompatible 'zip://host/path' URI: 'ArchiveUri[value=zip://host/path,restrictions=ArchiveUriRestrictions[schemes=[zip, jar], archiveEntry=false, archiveQuery=false],scheme=SimpleScheme[scheme='zip'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='//host/path', encodedSchemeSpecific='//host/path']]' must be opaque");
         assertThatThrownBy(() -> ArchiveRepositoryUri.ofRepositoryUri("zip://user@host/path"))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessage(
                     "Incompatible 'zip://user@host/path' URI: 'ArchiveUri[value=zip://user@host/path,restrictions=ArchiveUriRestrictions[schemes=[zip, jar], archiveEntry=false, archiveQuery=false],scheme=SimpleScheme[scheme='zip'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='//user@host/path', encodedSchemeSpecific='//user@host/path']]' must be opaque");
         assertThatThrownBy(() -> ArchiveRepositoryUri.ofRepositoryUri("path"))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessage(
                     "Incompatible 'path' URI: 'ArchiveUri[value=path,restrictions=ArchiveUriRestrictions[schemes=[zip, jar], archiveEntry=false, archiveQuery=false],scheme=<null>,schemeSpecific=<null>]' must be opaque");
      }

      @Test
      public void ofUriWhenInvalidUri() {
         assertThatThrownBy(() -> ArchiveRepositoryUri.ofRepositoryUri("zip:test:/document.zip#fragment"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'zip:test:/document.zip#fragment' URI: 'ArchiveUri[value=zip:test:/document.zip#fragment,restrictions=ArchiveUriRestrictions[schemes=[zip, jar], archiveEntry=false, archiveQuery=false],scheme=SimpleScheme[scheme='zip'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='test:/document.zip', encodedSchemeSpecific='test:/document.zip']]' must not have a fragment");
      }

      @Test
      public void ofUriWhenBasePath() {
         assertThat(ArchiveRepositoryUri.ofRepositoryUri(uri("zip:test:/document.zip!?basePath=archive"))).satisfies(
               uri -> {
                  assertThat(uri.archiveScheme()).isEqualTo("zip");
                  assertThat(uri.targetUri()).isEqualTo(DocumentUri.ofDocumentUri("test:/document.zip"));
                  assertThat(uri.basePath()).isEqualTo(Path.of("archive"));
               });
         assertThat(ArchiveRepositoryUri.ofRepositoryUri("zip:test:/document.zip!?basePath=archive")).satisfies(
               uri -> {
                  assertThat(uri.archiveScheme()).isEqualTo("zip");
                  assertThat(uri.targetUri()).isEqualTo(DocumentUri.ofDocumentUri("test:/document.zip"));
                  assertThat(uri.basePath()).isEqualTo(Path.of("archive"));
               });
      }

      @Test
      public void ofUriWhenBadBasePath() {
         assertThatThrownBy(() -> ArchiveRepositoryUri.ofRepositoryUri(
               "zip:test:/document.zip!?basePath=/archive"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'zip:test:/document.zip!?basePath=/archive' URI: 'basePath=/archive' parameter must be relative and must not contain traversal");
         assertThatThrownBy(() -> ArchiveRepositoryUri.ofRepositoryUri(
               "zip:test:/document.zip!?basePath=../archive"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'zip:test:/document.zip!?basePath=../archive' URI: 'basePath=../archive' parameter must be relative and must not contain traversal");
      }

      @Test
      public void ofUriWhenDefaultParameters() {
         assertThat(ArchiveRepositoryUri.ofRepositoryUri("zip:test:/document.zip")).satisfies(uri -> {
            assertThat(uri.basePath()).isEqualTo(emptyPath());
         });
      }

      @Test
      public void ofUriWhenAllParameters() {
         assertThat(ArchiveRepositoryUri.ofRepositoryUri("zip:test:/document.zip!/"
                                                         + "?basePath=archive")).satisfies(uri -> {
            assertThat(uri.basePath()).isEqualTo(Path.of("archive"));
            assertThat(uri.query().parameter(BASE_PATH, alwaysTrue())).hasValue(Path.of("archive"));
         });
      }

      @Test
      public void ofUriWhenExtensions() {
         assertThat(ArchiveRepositoryUri.ofRepositoryUri("zip:test:/document.zip!/"
                                                         + "?basePath=archive&ext1=v1&ext2=v2")).satisfies(uri -> {
            assertThat(uri.basePath()).isEqualTo(Path.of("archive"));
            assertThat(uri.query().parameter(BASE_PATH, alwaysTrue())).hasValue(Path.of("archive"));
            assertThat(uri.query().parameter("ext1", String.class, alwaysTrue())).hasValue("v1");
            assertThat(uri.query().parameter("ext2", String.class, alwaysTrue())).hasValue("v2");
         });
      }

   }

   @Nested
   class WrappedUri {

      @Test
      public void ofUriWhenNominal() {
         assertThat(ArchiveRepositoryUri.ofRepositoryUri(uri("document:archive:zip:test:/document.zip!/"))).satisfies(
               uri -> {
                  assertThat(uri.targetUri()).isEqualTo(DocumentUri.ofDocumentUri("test:/document.zip"));
                  assertThat(uri.basePath()).isEqualTo(emptyPath());
               });
      }

      @Test
      public void ofUriWhenBasePath() {
         assertThat(ArchiveRepositoryUri.ofRepositoryUri(uri(
               "document:archive:zip:test:/document.zip!/?basePath=archive"))).satisfies(uri -> {
            assertThat(uri.targetUri()).isEqualTo(DocumentUri.ofDocumentUri("test:/document.zip"));
            assertThat(uri.basePath()).isEqualTo(Path.of("archive"));
         });
      }

      @Test
      public void ofUriWhenBadRepositoryType() {
         assertThatThrownBy(() -> ArchiveRepositoryUri.ofRepositoryUri(uri(
               "document:unknown:zip:test:/document.zip")))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessage(
                     "Incompatible 'document:unknown:zip:test:/document.zip' URI: Unsupported 'unknown' repository type");
      }

      @Test
      public void ofUriWhenNormalized() {
         assertThat(ArchiveRepositoryUri.ofRepositoryUri(uri("document:archive:zip:test:/../document.zip"))).satisfies(
               uri -> {
                  assertThat(uri.targetUri()).isEqualTo(DocumentUri.ofDocumentUri("test:/../document.zip"));
               });
      }

      @Test
      public void ofUriWhenUnsupportedWrappedUri() {
         assertThatThrownBy(() -> ArchiveRepositoryUri.ofRepositoryUri(uri(
               "document:archive:unknown:/host/path")))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessage(
                     "Incompatible 'unknown:/host/path' URI: 'ArchiveUri[value=unknown:/host/path,restrictions=ArchiveUriRestrictions[schemes=[zip, jar], archiveEntry=false, archiveQuery=false],scheme=SimpleScheme[scheme='unknown'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='/host/path', encodedSchemeSpecific='/host/path']]' must be opaque");
      }

   }

   @Nested
   public class OfConfig {

      @Test
      public void ofConfigWhenNominal() {
         assertThat(ArchiveRepositoryUri.ofConfig(new ArchiveDocumentConfigBuilder()
                                                        .archiveScheme("jar")
                                                        .archiveDocumentUri(DocumentUri.ofDocumentUri(
                                                              "test:/document.zip"))
                                                        .basePath(Path.of("archive"))
                                                        .build())).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo("jar:test:/document.zip!/?basePath=archive");
            assertThat(uri.archiveScheme()).isEqualTo("jar");
            assertThat(uri.targetUri()).isEqualTo(DocumentUri.ofDocumentUri("test:/document.zip"));
            assertThat(uri.basePath()).isEqualTo(Path.of("archive"));
         });
      }

      @Test
      public void ofConfigWhenArchiveDocument() {
         assertThat(ArchiveRepositoryUri.ofConfig(new ArchiveDocumentConfigBuilder()
                                                        .archiveScheme("jar")
                                                        .archiveDocument(ReferencedDocument.lazyLoad(new TestDocumentRepository(),
                                                                                                     DocumentPath.of(
                                                                                                           "/document.zip")))
                                                        .basePath(Path.of("archive"))
                                                        .build())).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo("jar:test:/document.zip!/?basePath=archive");
            assertThat(uri.archiveScheme()).isEqualTo("jar");
            assertThat(uri.targetUri()).isEqualTo(DocumentUri.ofDocumentUri("test:/document.zip"));
            assertThat(uri.basePath()).isEqualTo(Path.of("archive"));
         });
      }

      @Test
      public void ofConfigWhenBadConfiguration() {
         assertThatThrownBy(() -> ArchiveRepositoryUri.ofConfig(null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'config' must not be null");
      }

      @Test
      public void ofConfigWhenExtensions() {
         assertThat(ArchiveRepositoryUri.ofConfig(new ArchiveDocumentConfigBuilder()
                                                        .archiveScheme("jar")
                                                        .archiveDocument(ReferencedDocument.lazyLoad(new TestDocumentRepository(),
                                                                                                     DocumentPath.of(
                                                                                                           "/document.zip")))
                                                        .basePath(Path.of("archive"))
                                                        .extensions(map(LinkedHashMap::new,
                                                                        entry("ext1", "v1"),
                                                                        entry("ext2", "v2")))
                                                        .build())).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo("jar:test:/document.zip!/?basePath=archive&ext1=v1&ext2=v2");
            assertThat(uri.archiveScheme()).isEqualTo("jar");
            assertThat(uri.targetUri()).isEqualTo(DocumentUri.ofDocumentUri("test:/document.zip"));
            assertThat(uri.basePath()).isEqualTo(Path.of("archive"));
            assertThat(uri.query().parameter("ext1", String.class, alwaysTrue())).hasValue("v1");
            assertThat(uri.query().parameter("ext2", String.class, alwaysTrue())).hasValue("v2");
         });
      }

      @Test
      public void ofConfigWhenMultiValueExtensions() {
         assertThat(ArchiveRepositoryUri.ofConfig(new ArchiveDocumentConfigBuilder()
                                                        .archiveScheme("jar")
                                                        .archiveDocument(ReferencedDocument.lazyLoad(new TestDocumentRepository(),
                                                                                                     DocumentPath.of(
                                                                                                           "/document.zip")))
                                                        .basePath(Path.of("archive"))
                                                        .extensions(map(LinkedHashMap::new,
                                                                        entry("ext1", list("v1", "v2")),
                                                                        entry("ext2", "v2")))
                                                        .build())).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo(
                  "jar:test:/document.zip!/?basePath=archive&ext1=v1&ext1=v2&ext2=v2");
            assertThat(uri.archiveScheme()).isEqualTo("jar");
            assertThat(uri.targetUri()).isEqualTo(DocumentUri.ofDocumentUri("test:/document.zip"));
            assertThat(uri.basePath()).isEqualTo(Path.of("archive"));
            assertThat(uri.query().parameters("ext1", String.class, alwaysTrue())).isEqualTo(list("v1",
                                                                                                  "v2"));
            assertThat(uri.query().parameter("ext2", String.class, alwaysTrue())).hasValue("v2");
         });
      }

   }

   @Nested
   public class ToConfig {

      @Test
      public void toConfigWhenNominal() {
         assertThat(ArchiveRepositoryUri
                          .ofRepositoryUri("zip:test:/document.zip!/?basePath=archive")
                          .toConfig()).satisfies(config -> {
            assertThat(config.archiveScheme()).isEqualTo("zip");
            assertThat(config.archiveDocumentUri()).isEqualTo(DocumentUri.ofDocumentUri("test:/document.zip"));
            assertThat(config.archiveDocument()).isNull();
            assertThat(config.basePath()).isEqualTo(Path.of("archive"));
         });
      }

      @Test
      public void toConfigWhenJar() {
         assertThat(ArchiveRepositoryUri
                          .ofRepositoryUri("jar:test:/document.zip!/?basePath=archive")
                          .toConfig()).satisfies(config -> {
            assertThat(config.archiveScheme()).isEqualTo("jar");
            assertThat(config.archiveDocumentUri()).isEqualTo(DocumentUri.ofDocumentUri("test:/document.zip"));
            assertThat(config.archiveDocument()).isNull();
            assertThat(config.basePath()).isEqualTo(Path.of("archive"));
         });
      }

   }

   @Nested
   public class ToUri {

      @Test
      public void toUriWhenNominal() {
         assertThat(ArchiveRepositoryUri
                          .ofRepositoryUri("zip:test:/document.zip")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("zip:test:/document.zip!/");
      }

      @Test
      public void toUriWhenBasePath() {
         assertThat(ArchiveRepositoryUri
                          .ofRepositoryUri("zip:test:/document.zip!/?basePath=archive")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("zip:test:/document.zip!/");
         assertThat(ArchiveRepositoryUri
                          .ofRepositoryUri("zip:test:/document.zip!/?basePath=archive")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("zip:test:/document.zip!/?basePath=archive");
      }

      @Test
      public void toUriWhenJar() {
         assertThat(ArchiveRepositoryUri
                          .ofRepositoryUri("jar:test:/document.zip")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("jar:test:/document.zip!/");
      }

      @Test
      public void toUriWhenTargetUriHasQuery() {
         assertThat(ArchiveRepositoryUri
                          .ofRepositoryUri(ArchiveUri.ofUri(
                                "zip:test:/document.zip?failFastIfMissingStoragePath=false"))
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo(
               "zip:test:/document.zip?failFastIfMissingStoragePath=false!/");
      }

      @Test
      public void toUriWhenMultiValueExtensions() {
         assertThat(ArchiveRepositoryUri
                          .ofRepositoryUri("zip:test:/document.zip!/?ext1=v1&ext1=v2")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("zip:test:/document.zip!/?ext1=v1&ext1=v2");
      }

      @Test
      public void toUriWhenDefaultParameters() {
         assertThat(ArchiveRepositoryUri
                          .ofRepositoryUri("zip:test:/document.zip")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("zip:test:/document.zip!/");
         assertThat(ArchiveRepositoryUri
                          .ofRepositoryUri("zip:test:/document.zip!/?basePath=archive&ext1=v1&ext2=v2")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo(
               "zip:test:/document.zip!/?basePath=archive&ext1=v1&ext2=v2");
         assertThat(ArchiveRepositoryUri
                          .ofRepositoryUri("zip:test:/document.zip!?basePath=archive&ext1=v1&ext2=v2")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo(
               "zip:test:/document.zip!/?basePath=archive&ext1=v1&ext2=v2");
      }

      @Test
      public void toUriWhenDefaultParametersWithForceDefaultValues() {
         assertThat(ArchiveRepositoryUri
                          .ofRepositoryUri("zip:test:/document.zip!?ext1=v1&ext2=v2")
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo("zip:test:/document.zip!/?ext1=v1&ext2=v2&basePath=");
         assertThat(ArchiveRepositoryUri
                          .ofRepositoryUri("zip:test:/document.zip?query!?ext1=v1&ext2=v2")
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo("zip:test:/document.zip?query!/?ext1=v1&ext2=v2&basePath=");
      }

      @Test
      public void toUriWhenSensitiveParameters() {
         assertThat(ArchiveRepositoryUri
                          .ofRepositoryUri("zip:test:/document.zip!?ext1=v1&ext2=v2")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("zip:test:/document.zip!/?ext1=v1&ext2=v2");
         assertThat(ArchiveRepositoryUri
                          .ofRepositoryUri("zip:test:/document.zip?query!/?basePath=archive&ext1=v1&ext2=v2")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo(
               "zip:test:/document.zip?query!/?basePath=archive&ext1=v1&ext2=v2");
      }

   }

   @Nested
   public class NormalizeResolveRelativize {

      @Test
      public void normalizeWhenNominal() {
         assertThat(ArchiveRepositoryUri.ofRepositoryUri("zip:test:/../document.zip").normalize()).satisfies(
               uri -> {
                  assertThat(uri.value()).isEqualTo("zip:test:/../document.zip");
               });
      }

      @Test
      public void resolveWhenNominal() {
         assertThatThrownBy(() -> ArchiveRepositoryUri
               .ofRepositoryUri("zip:test:/document.zip")
               .resolve(uri("otherpath"))).isInstanceOf(UnsupportedOperationException.class);
      }

      @Test
      public void relativizeWhenNominal() {
         assertThatThrownBy(() -> ArchiveRepositoryUri
               .ofRepositoryUri("zip:test:/document.zip")
               .relativize(uri("zip:otherpath"))).isInstanceOf(UnsupportedOperationException.class);
      }

   }

   @Nested
   class SupportsUri {

      @Test
      public void supportsUriWhenNominal() {
         assertThat(ArchiveRepositoryUri
                          .ofRepositoryUri("zip:test:/document.zip")
                          .supportsUri(ArchiveRepositoryUri.ofRepositoryUri("zip:test:/document.zip"))).isTrue();
      }

      @Test
      public void supportsUriWhenDefaultRepositoryUri() {
         assertThat(ArchiveRepositoryUri
                          .ofRepositoryUri("zip:test:/document.zip")
                          .supportsUri(RepositoryUri.ofRepositoryUri("zip:test:/document.zip"))).isTrue();
      }

      @Test
      public void supportsUriWhenIncompatibleUriType() {
         assertThat(ArchiveRepositoryUri
                          .ofRepositoryUri("zip:test:/document.zip")
                          .supportsUri(RepositoryUri.ofRepositoryUri("test:/document.zip"))).isFalse();
      }

      @Test
      public void supportsUriWhenDifferentArchiveScheme() {
         assertThat(ArchiveRepositoryUri
                          .ofRepositoryUri("zip:test:/document.zip")
                          .supportsUri(ArchiveRepositoryUri.ofRepositoryUri("jar:test:/document.zip"))).isFalse();
      }

      @Test
      public void supportsUriWhenDifferentTargetUri() {
         assertThat(ArchiveRepositoryUri
                          .ofRepositoryUri("zip:test:/document.zip")
                          .supportsUri(ArchiveRepositoryUri.ofRepositoryUri("zip:other:/document.zip"))).isFalse();
         assertThat(ArchiveRepositoryUri
                          .ofRepositoryUri("zip:test:/document.zip")
                          .supportsUri(ArchiveRepositoryUri.ofRepositoryUri("zip:test:/document.zip?query"))).isFalse();
         assertThat(ArchiveRepositoryUri
                          .ofRepositoryUri("zip:test:/document.zip?query")
                          .supportsUri(ArchiveRepositoryUri.ofRepositoryUri("zip:test:/document.zip"))).isFalse();
         assertThat(ArchiveRepositoryUri
                          .ofRepositoryUri("zip:test:/document.zip%40fragment")
                          .supportsUri(ArchiveRepositoryUri.ofRepositoryUri("zip:test:/document.zip"))).isFalse();
      }

      @Test
      public void supportsUriWhenDifferentBasePath() {
         assertThat(ArchiveRepositoryUri
                          .ofRepositoryUri("zip:test:/document.zip!/?basePath=archive")
                          .supportsUri(ArchiveRepositoryUri.ofRepositoryUri("zip:test:/document.zip"))).isFalse();
         assertThat(ArchiveRepositoryUri
                          .ofRepositoryUri("zip:test:/document.zip!/?basePath=archive")
                          .supportsUri(ArchiveRepositoryUri.ofRepositoryUri("zip:test:/document.zip!/"))).isFalse();
         assertThat(ArchiveRepositoryUri
                          .ofRepositoryUri("zip:test:/document.zip!/?basePath=archive")
                          .supportsUri(ArchiveRepositoryUri.ofRepositoryUri(
                                "zip:test:/document.zip!/archive/entry"))).isFalse();
         assertThat(ArchiveRepositoryUri
                          .ofRepositoryUri("zip:test:/document.zip!/?basePath=archive")
                          .supportsUri(ArchiveRepositoryUri.ofRepositoryUri(
                                "zip:test:/document.zip!/archive/entry?basePath=archive"))).isTrue();
         assertThat(ArchiveRepositoryUri
                          .ofRepositoryUri("zip:test:/document.zip!/?basePath=archive")
                          .supportsUri(ArchiveRepositoryUri.ofRepositoryUri(
                                "zip:test:/document.zip!/archive/entry?basePath="))).isFalse();
         assertThat(ArchiveRepositoryUri
                          .ofRepositoryUri("zip:test:/document.zip!/?basePath=archive")
                          .supportsUri(ArchiveRepositoryUri.ofRepositoryUri(
                                "zip:test:/document.zip!/archive/entry?basePath=other"))).isFalse();
      }

      @Test
      public void supportsUriWhenDifferentArchiveQuery() {
         assertThat(ArchiveRepositoryUri
                          .ofRepositoryUri("zip:test:/document.zip!/?ext=v")
                          .supportsUri(ArchiveRepositoryUri.ofRepositoryUri("zip:test:/document.zip"))).isTrue();
         assertThat(ArchiveRepositoryUri
                          .ofRepositoryUri("zip:test:/document.zip!/?ext=v")
                          .supportsUri(ArchiveRepositoryUri.ofRepositoryUri("zip:test:/document.zip!/"))).isTrue();
         assertThat(ArchiveRepositoryUri
                          .ofRepositoryUri("zip:test:/document.zip!/?ext=v")
                          .supportsUri(ArchiveRepositoryUri.ofRepositoryUri(
                                "zip:test:/document.zip!/archive/entry"))).isTrue();
         assertThat(ArchiveRepositoryUri
                          .ofRepositoryUri("zip:test:/document.zip!/?ext=v")
                          .supportsUri(ArchiveRepositoryUri.ofRepositoryUri(
                                "zip:test:/document.zip!/archive/entry?ext=v"))).isTrue();
         assertThat(ArchiveRepositoryUri
                          .ofRepositoryUri("zip:test:/document.zip!/?ext=v")
                          .supportsUri(ArchiveRepositoryUri.ofRepositoryUri(
                                "zip:test:/document.zip!/archive/entry?ext=other"))).isFalse();
      }

   }
}