/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.archive.zip;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.ports.document.domain.DocumentEntryCriteria.DocumentEntryCriteriaBuilder.allDocuments;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.defaultParameters;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.noParameters;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.sensitiveParameters;
import static java.nio.charset.StandardCharsets.UTF_8;

import java.nio.file.Path;
import java.util.List;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ddd2.uri.parts.SimpleQuery;
import com.tinubu.commons.ddd2.uri.uris.ArchiveUri;
import com.tinubu.commons.ports.document.archive.ArchiveRepositoryUri;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.DocumentRepositoryFactory;
import com.tinubu.commons.ports.document.domain.repository.TestDocumentRepository;
import com.tinubu.commons.ports.document.domain.testsuite.BaseDocumentRepositoryTest;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.RepositoryUri;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ZipArchiver;

@Disabled("Manual dev")
public class ZipTest extends BaseDocumentRepositoryTest {

   @Override
   protected DocumentRepository documentRepository() {
      return TestDocumentRepository.sharedInstance("default");
   }

   @Override
   protected Document zipTransformer(DocumentPath zipPath, List<Document> documents) {
      return new ZipArchiver(zipPath).compress(documents);
   }

   protected DocumentEntry createArchive(Path archive, Path... documents) {
      return createArchive(archive, list(documents));
   }

   protected DocumentEntry createArchive(Path archive, List<Path> documents) {
      /*List<DocumentEntry> createdDocuments = list(documents
                                                        .stream()
                                                        .map(documentPath -> documentRepository()
                                                              .findDocumentEntryById(DocumentPath.of(
                                                                    documentPath))
                                                              .orElseGet(() -> createDocument(documentPath))));*/

      return createZipDocument(DocumentPath.of(archive),
                               stream(documents).map(DocumentPath::of).toArray(DocumentPath[]::new));
   }

   @Test
   public void nested() {
      createDocuments(path("file1.txt"), path("path/pathfile1.txt"));
      createArchive(path("sub.zip"), path("file1.txt"), path("path/pathfile1.txt"));
      createArchive(path("archive.zip"), path("sub.zip"));

      try (var zip = ZipArchiveDocumentRepository.ofUri(RepositoryUri.ofRepositoryUri(
            "zip:zip:test:/archive.zip!/sub.zip!/"))) {
         System.out.println(list(zip.findDocumentEntriesBySpecification(allDocuments())));
      }
   }

   @Test
   public void extensions() {
      createDocuments(path("file1.txt"), path("path/pathfile1.txt"));
      createArchive(path("archive.zip"), path("file1.txt"), path("path/pathfile1.txt"));

      var uri = ArchiveRepositoryUri.ofRepositoryUri(
            "zip:test:/archive.zip?basePath=archive&zip.password=changeit&zip.compression-level=high");

      System.out.println(uri.exportURI(defaultParameters()));
      System.out.println(uri.exportURI(noParameters()));
      System.out.println(uri.exportURI(sensitiveParameters()));

      System.out.println(uri.toConfig().extensions());

      System.out.println(uri.supportsUri(ArchiveRepositoryUri.ofRepositoryUri("zip:test:/archive.zip!?ext1=v1")));
      System.out.println(uri.supportsUri(ArchiveRepositoryUri.ofRepositoryUri("zip:test:/archive.zip!")));
      System.out.println(uri.supportsUri(ArchiveRepositoryUri.ofRepositoryUri("zip:test:/archive.zip!?ext2=v2")));

      var zipRepo = ZipArchiveDocumentRepository.ofUri(RepositoryUri.ofRepositoryUri(
            "zip:test:/archive.zip!?zip.password=changeit&zip.compression-level=high"));

      System.out.println(zipRepo);
      System.out.println(zipRepo.toUri(defaultParameters()));
      System.out.println(zipRepo.toUri(noParameters()));
      System.out.println(zipRepo.toUri(sensitiveParameters()));

      var zip = ZipArchiveDocumentRepository.ofUri(RepositoryUri.ofRepositoryUri(
            "zip:test:/archive.zip!/?zip.compression-level=high"));

      System.out.println(list(zip.findDocumentEntriesBySpecification(allDocuments())));
   }

   @Test
   public void t() throws Exception {

      createDocuments(path("file1.txt"), path("path/pathfile1.txt"));
      createArchive(path("archive.zip"), path("file1.txt"), path("path/pathfile1.txt"));

      var docUri = DocumentUri.ofDocumentUri("zip:test:/archive.zip!/file1.txt");
      try (var zipRepo = ZipArchiveDocumentRepository.ofUri(docUri)) {
         System.out.println(zipRepo);
         var doc1 = zipRepo.findDocumentByUri(docUri).orElseThrow();
         try (var content = doc1.closeableContent()) {
            System.out.println(doc1.content().stringContent(UTF_8));
         }
         var doc2 = zipRepo.findDocumentById(DocumentPath.of("/path/pathfile1.txt")).orElseThrow();
         try (var content = doc2.closeableContent()) {
            System.out.println(doc2.content().stringContent(UTF_8));
         }
      }

   }

   @Test
   public void fullAuto() throws Exception {

      createDocuments(path("file1.txt"), path("path/pathfile1.txt"));
      createArchive(path("archive.zip"), path("file1.txt"), path("path/pathfile1.txt"));

      var docUri = DocumentUri.ofDocumentUri(ArchiveUri.archive("JAR",
                                                                Uri.ofUri("test:/archive.zip"),
                                                                Path.of("/path/pathfile1.txt"),
                                                                SimpleQuery.of("basePath=path")));
      try (var zipRepo = DocumentRepositoryFactory.instance().documentRepository(docUri).orElseThrow()) {
         System.out.println(zipRepo);
         var doc1 = zipRepo.findDocumentByUri(docUri).orElseThrow();
         try (var content = doc1.closeableContent()) {
            System.out.println(doc1.content().stringContent(UTF_8));
         }
         var doc2 = zipRepo.findDocumentById(DocumentPath.of("/pathfile1.txt")).orElseThrow();
         try (var content = doc2.closeableContent()) {
            System.out.println(doc2.content().stringContent(UTF_8));
         }

         System.out.println(list(zipRepo.findDocumentEntriesBySpecification(allDocuments())));

      }

   }

}
