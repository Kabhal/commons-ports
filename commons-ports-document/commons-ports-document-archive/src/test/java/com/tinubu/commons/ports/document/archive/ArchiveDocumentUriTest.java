/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.archive;

import static com.tinubu.commons.ddd2.uri.Uri.uri;
import static com.tinubu.commons.lang.util.CheckedPredicate.alwaysTrue;
import static com.tinubu.commons.lang.util.CollectionUtils.entry;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.PathUtils.emptyPath;
import static com.tinubu.commons.ports.document.archive.ArchiveDocumentConfig.ArchiveDocumentConfigBuilder;
import static com.tinubu.commons.ports.document.archive.ArchiveRepositoryUri.Parameters.BASE_PATH;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.defaultParameters;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.noParameters;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.sensitiveParameters;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.net.URI;
import java.nio.file.Path;
import java.util.LinkedHashMap;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ddd2.uri.uris.ArchiveUri;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.ReferencedDocument;
import com.tinubu.commons.ports.document.domain.repository.TestDocumentRepository;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.RepositoryUri;

public class ArchiveDocumentUriTest {

   @Nested
   public class OfUriWhenURI {

      @Test
      public void ofUriWhenNominal() {
         assertThat(ArchiveDocumentUri.ofDocumentUri(uri("zip:test:/document.zip!/archive/entry"))).satisfies(
               uri -> {
                  assertThat(uri.archiveScheme()).isEqualTo("zip");
                  assertThat(uri.targetUri()).isEqualTo(DocumentUri.ofDocumentUri("test:/document.zip"));
                  assertThat(uri.basePath()).isEqualTo(emptyPath());
                  assertThat(uri.documentId()).isEqualTo(DocumentPath.of("/archive/entry"));
               });
         assertThat(ArchiveDocumentUri.ofDocumentUri("zip:test:/document.zip!/archive/entry")).satisfies(uri -> {
            assertThat(uri.archiveScheme()).isEqualTo("zip");
            assertThat(uri.targetUri()).isEqualTo(DocumentUri.ofDocumentUri("test:/document.zip"));
            assertThat(uri.basePath()).isEqualTo(emptyPath());
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("/archive/entry"));
         });
      }

      @Test
      public void ofUriWhenUri() {
         assertThat(ArchiveDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(Uri.ofUri(
               "zip:test:/document.zip!/archive/entry")))).satisfies(uri -> {
            assertThat(uri.targetUri()).isEqualTo(DocumentUri.ofDocumentUri("test:/document.zip"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("/archive/entry"));
         });
         assertThat(ArchiveDocumentUri.ofDocumentUri(Uri.ofUri("zip:test:/document.zip!/archive/entry"))).satisfies(
               uri -> {
                  assertThat(uri.targetUri()).isEqualTo(DocumentUri.ofDocumentUri("test:/document.zip"));
                  assertThat(uri.documentId()).isEqualTo(DocumentPath.of("/archive/entry"));
               });
      }

      @Test
      public void ofUriWhenArchiveUri() {
         assertThat(ArchiveDocumentUri.ofDocumentUri(DocumentUri.ofDocumentUri(ArchiveUri.ofUri(
               "zip:test:/document.zip!/archive/entry")))).satisfies(uri -> {
            assertThat(uri.targetUri()).isEqualTo(DocumentUri.ofDocumentUri("test:/document.zip"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("/archive/entry"));
         });
         assertThat(ArchiveDocumentUri.ofDocumentUri(ArchiveUri.ofUri("zip:test:/document.zip!/archive/entry"))).satisfies(
               uri -> {
                  assertThat(uri.targetUri()).isEqualTo(DocumentUri.ofDocumentUri("test:/document.zip"));
                  assertThat(uri.documentId()).isEqualTo(DocumentPath.of("/archive/entry"));
               });
      }

      @Test
      public void ofUriWhenBadParameters() {
         assertThatThrownBy(() -> ArchiveDocumentUri.ofDocumentUri((URI) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
         assertThatThrownBy(() -> ArchiveDocumentUri.ofDocumentUri((String) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
         assertThatThrownBy(() -> ArchiveDocumentUri.ofDocumentUri((RepositoryUri) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'uri' must not be null");
      }

      @Test
      @Disabled("not testable currently")
      public void ofUriWhenIllegalArgument() {
         assertThatExceptionOfType(InvalidUriException.class)
               .isThrownBy(() -> ArchiveDocumentUri.ofDocumentUri(
                     "zip:test:/document.zip?compressionLevel=UNKNOWN"))
               .withMessage(
                     "Invalid '%s' URI: Conversion from 'java.lang.String' to 'CompressionLevel' failed for 'UNKNOWN' value",
                     "zip:test:/document.zip?compressionLevel=UNKNOWN");
      }

      @Test
      public void ofUriWhenCaseSensitiveParameterKey() {
         assertThat(ArchiveDocumentUri.ofDocumentUri("zip:test:/document.zip!/archive/entry?BaSePaTh=other")).satisfies(
               uri -> {
                  assertThat(uri.basePath()).isEqualTo(emptyPath());
                  assertThat(uri.query().parameter(BASE_PATH, alwaysTrue())).hasValue(emptyPath());
               });
      }

      @Test
      public void ofUriWhenNormalized() {
         assertThat(ArchiveDocumentUri.ofDocumentUri(uri("zip:test:/../document.zip!/../archive/entry"))).satisfies(
               uri -> {
                  assertThat(uri.targetUri()).isEqualTo(DocumentUri.ofDocumentUri("test:/../document.zip"));
                  assertThat(uri.documentId()).isEqualTo(DocumentPath.of("/archive/entry"));
               });
      }

      @Test
      public void ofUriWhenInvalidUriSyntax() {
         assertThatThrownBy(() -> ArchiveDocumentUri.ofDocumentUri("git@git.com:user/repo.git"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'git@git.com:user/repo.git' URI: Illegal character in scheme name at index 3: git@git.com:user/repo.git");
         assertThatThrownBy(() -> ArchiveDocumentUri.ofDocumentUri("git@git.com:user/repo.git"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'git@git.com:user/repo.git' URI: Illegal character in scheme name at index 3: git@git.com:user/repo.git");
      }

      @Test
      public void ofUriWhenIncompatibleUri() {
         assertThatThrownBy(() -> ArchiveDocumentUri.ofDocumentUri("unknown:/path"))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessage(
                     "Incompatible 'unknown:/path' URI: 'ArchiveUri[value=unknown:/path,restrictions=ArchiveUriRestrictions[schemes=[zip, jar], archiveEntry=false, archiveQuery=false],scheme=SimpleScheme[scheme='unknown'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='/path', encodedSchemeSpecific='/path']]' must be opaque");
         assertThatThrownBy(() -> ArchiveDocumentUri.ofDocumentUri("unknown:path"))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessage("Incompatible 'unknown:path' URI: 'scheme=unknown' must be in [zip,jar]");
         assertThatThrownBy(() -> ArchiveDocumentUri.ofDocumentUri("zip://host/path"))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessage(
                     "Incompatible 'zip://host/path' URI: 'ArchiveUri[value=zip://host/path,restrictions=ArchiveUriRestrictions[schemes=[zip, jar], archiveEntry=false, archiveQuery=false],scheme=SimpleScheme[scheme='zip'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='//host/path', encodedSchemeSpecific='//host/path']]' must be opaque");
         assertThatThrownBy(() -> ArchiveDocumentUri.ofDocumentUri("zip://user@host/path"))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessage(
                     "Incompatible 'zip://user@host/path' URI: 'ArchiveUri[value=zip://user@host/path,restrictions=ArchiveUriRestrictions[schemes=[zip, jar], archiveEntry=false, archiveQuery=false],scheme=SimpleScheme[scheme='zip'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='//user@host/path', encodedSchemeSpecific='//user@host/path']]' must be opaque");
         assertThatThrownBy(() -> ArchiveDocumentUri.ofDocumentUri("path"))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessage(
                     "Incompatible 'path' URI: 'ArchiveUri[value=path,restrictions=ArchiveUriRestrictions[schemes=[zip, jar], archiveEntry=false, archiveQuery=false],scheme=<null>,schemeSpecific=<null>]' must be opaque");
      }

      @Test
      public void ofUriWhenInvalidUri() {
         assertThatThrownBy(() -> ArchiveDocumentUri.ofDocumentUri(
               "zip:test:/document.zip!/archive/entry#fragment"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'zip:test:/document.zip!/archive/entry#fragment' URI: 'ArchiveUri[value=zip:test:/document.zip!/archive/entry#fragment,restrictions=ArchiveUriRestrictions[schemes=[zip, jar], archiveEntry=false, archiveQuery=false],scheme=SimpleScheme[scheme='zip'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='test:/document.zip!/archive/entry', encodedSchemeSpecific='test:/document.zip!/archive/entry']]' must not have a fragment");
      }

      @Test
      public void ofUriWhenBasePath() {
         assertThat(ArchiveDocumentUri.ofDocumentUri(uri(
               "zip:test:/document.zip!/archive/entry?basePath=archive"))).satisfies(uri -> {
            assertThat(uri.archiveScheme()).isEqualTo("zip");
            assertThat(uri.targetUri()).isEqualTo(DocumentUri.ofDocumentUri("test:/document.zip"));
            assertThat(uri.basePath()).isEqualTo(Path.of("archive"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("/entry"));
         });
         assertThat(ArchiveDocumentUri.ofDocumentUri("zip:test:/document.zip!/archive/entry?basePath=archive")).satisfies(
               uri -> {
                  assertThat(uri.archiveScheme()).isEqualTo("zip");
                  assertThat(uri.targetUri()).isEqualTo(DocumentUri.ofDocumentUri("test:/document.zip"));
                  assertThat(uri.basePath()).isEqualTo(Path.of("archive"));
                  assertThat(uri.documentId()).isEqualTo(DocumentPath.of("/entry"));
               });
      }

      @Test
      public void ofUriWhenBadBasePath() {
         assertThatThrownBy(() -> ArchiveDocumentUri.ofDocumentUri(
               "zip:test:/document.zip!/archive/entry?basePath=/archive"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'zip:test:/document.zip!/archive/entry?basePath=/archive' URI: 'basePath=/archive' parameter must be relative and must not contain traversal");
         assertThatThrownBy(() -> ArchiveDocumentUri.ofDocumentUri(
               "zip:test:/document.zip!/archive/entry?basePath=../archive"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'zip:test:/document.zip!/archive/entry?basePath=../archive' URI: 'basePath=../archive' parameter must be relative and must not contain traversal");
      }

      @Test
      public void ofUriWhenNotMatchingBasePath() {
         assertThatThrownBy(() -> ArchiveDocumentUri.ofDocumentUri(
               "zip:test:/document.zip!/archive/entry?basePath=other"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'zip:test:/document.zip!/archive/entry?basePath=other' URI: 'basePath=other' parameter must start but not be equal to 'archive/entry' archive entry");
         assertThatThrownBy(() -> ArchiveDocumentUri.ofDocumentUri(
               "zip:test:/document.zip!/archive/entry?basePath=archive/entry"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'zip:test:/document.zip!/archive/entry?basePath=archive/entry' URI: 'basePath=archive/entry' parameter must start but not be equal to 'archive/entry' archive entry");
      }

      @Test
      public void ofUriWhenDefaultParameters() {
         assertThat(ArchiveDocumentUri.ofDocumentUri("zip:test:/document.zip!/archive/entry")).satisfies(uri -> {
            assertThat(uri.basePath()).isEqualTo(emptyPath());
            assertThat(uri.query().parameter(BASE_PATH, alwaysTrue())).hasValue(emptyPath());
         });
      }

      @Test
      public void ofUriWhenAllParameters() {
         assertThat(ArchiveDocumentUri.ofDocumentUri("zip:test:/document.zip!/archive/entry"
                                                     + "?basePath=archive")).satisfies(uri -> {
            assertThat(uri.basePath()).isEqualTo(Path.of("archive"));
            assertThat(uri.query().parameter(BASE_PATH, alwaysTrue())).hasValue(Path.of("archive"));
         });
      }

      @Test
      public void ofUriWhenExtensions() {
         assertThat(ArchiveDocumentUri.ofDocumentUri("zip:test:/document.zip!/archive/entry"
                                                     + "?basePath=archive&ext1=v1&ext2=v2")).satisfies(uri -> {
            assertThat(uri.query().parameter(BASE_PATH, alwaysTrue())).hasValue(Path.of("archive"));
            assertThat(uri.query().parameter("ext1", String.class, alwaysTrue())).hasValue("v1");
            assertThat(uri.query().parameter("ext2", String.class, alwaysTrue())).hasValue("v2");
         });
      }
   }

   @Nested
   class WrappedUri {

      @Test
      public void ofUriWhenNominal() {
         assertThat(ArchiveDocumentUri.ofDocumentUri(uri(
               "document:archive:zip:test:/document.zip!/archive/entry"))).satisfies(uri -> {
            assertThat(uri.targetUri()).isEqualTo(DocumentUri.ofDocumentUri("test:/document.zip"));
            assertThat(uri.basePath()).isEqualTo(emptyPath());
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("/archive/entry"));
         });
      }

      @Test
      public void ofUriWhenBasePath() {
         assertThat(ArchiveDocumentUri.ofDocumentUri(uri(
               "document:archive:zip:test:/document.zip!/archive/entry?basePath=archive"))).satisfies(uri -> {
            assertThat(uri.targetUri()).isEqualTo(DocumentUri.ofDocumentUri("test:/document.zip"));
            assertThat(uri.basePath()).isEqualTo(Path.of("archive"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("/entry"));
         });
      }

      @Test
      public void ofUriWhenBadRepositoryType() {
         assertThatThrownBy(() -> ArchiveDocumentUri.ofDocumentUri(uri(
               "document:unknown:zip:test:/document.zip!/archive/entry")))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessage(
                     "Incompatible 'document:unknown:zip:test:/document.zip!/archive/entry' URI: Unsupported 'unknown' repository type");
      }

      @Test
      public void ofUriWhenNormalized() {
         assertThat(ArchiveDocumentUri.ofDocumentUri(uri(
               "document:archive:zip:test:/../document.zip!/../archive/entry"))).satisfies(uri -> {
            assertThat(uri.targetUri()).isEqualTo(DocumentUri.ofDocumentUri("test:/../document.zip"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("/archive/entry"));
         });
      }

      @Test
      public void ofUriWhenUnsupportedWrappedUri() {
         assertThatThrownBy(() -> ArchiveDocumentUri.ofDocumentUri(uri(
               "document:archive:unknown:/host/path!/archive/entry")))
               .isInstanceOf(IncompatibleUriException.class)
               .hasMessage(
                     "Incompatible 'unknown:/host/path!/archive/entry' URI: 'ArchiveUri[value=unknown:/host/path!/archive/entry,restrictions=ArchiveUriRestrictions[schemes=[zip, jar], archiveEntry=false, archiveQuery=false],scheme=SimpleScheme[scheme='unknown'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='/host/path!/archive/entry', encodedSchemeSpecific='/host/path!/archive/entry']]' must be opaque");
      }

   }

   @Nested
   public class OfConfig {

      @Test
      public void ofConfigWhenNominal() {
         assertThat(ArchiveDocumentUri.ofConfig(new ArchiveDocumentConfigBuilder()
                                                      .archiveScheme("jar")
                                                      .archiveDocumentUri(DocumentUri.ofDocumentUri(
                                                            "test:/document.zip"))
                                                      .basePath(Path.of("archive"))
                                                      .build(), DocumentPath.of("entry"))).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo("jar:test:/document.zip!/archive/entry?basePath=archive");
            assertThat(uri.archiveScheme()).isEqualTo("jar");
            assertThat(uri.targetUri()).isEqualTo(DocumentUri.ofDocumentUri("test:/document.zip"));
            assertThat(uri.basePath()).isEqualTo(Path.of("archive"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("entry"));
         });
      }

      @Test
      public void ofConfigWhenArchiveDocument() {
         assertThat(ArchiveDocumentUri.ofConfig(new ArchiveDocumentConfigBuilder()
                                                      .archiveScheme("jar")
                                                      .archiveDocument(ReferencedDocument.lazyLoad(new TestDocumentRepository(),
                                                                                                   DocumentPath.of(
                                                                                                         "/document.zip")))
                                                      .basePath(Path.of("archive"))
                                                      .build(), DocumentPath.of("entry"))).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo("jar:test:/document.zip!/archive/entry?basePath=archive");
            assertThat(uri.archiveScheme()).isEqualTo("jar");
            assertThat(uri.targetUri()).isEqualTo(DocumentUri.ofDocumentUri("test:/document.zip"));
            assertThat(uri.basePath()).isEqualTo(Path.of("archive"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("entry"));
         });
      }

      @Test
      public void ofConfigWhenBadConfiguration() {
         assertThatThrownBy(() -> ArchiveDocumentUri.ofConfig(null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'config' must not be null");
      }

      @Test
      public void ofConfigWhenExtensions() {
         assertThat(ArchiveDocumentUri.ofConfig(new ArchiveDocumentConfigBuilder()
                                                      .archiveScheme("jar")
                                                      .archiveDocument(ReferencedDocument.lazyLoad(new TestDocumentRepository(),
                                                                                                   DocumentPath.of(
                                                                                                         "/document.zip")))
                                                      .basePath(Path.of("archive"))
                                                      .extensions(map(LinkedHashMap::new,
                                                                      entry("ext1", "v1"),
                                                                      entry("ext2", "v2")))
                                                      .build(), DocumentPath.of("entry"))).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo(
                  "jar:test:/document.zip!/archive/entry?basePath=archive&ext1=v1&ext2=v2");
            assertThat(uri.archiveScheme()).isEqualTo("jar");
            assertThat(uri.targetUri()).isEqualTo(DocumentUri.ofDocumentUri("test:/document.zip"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("entry"));
            assertThat(uri.query().parameter(BASE_PATH, alwaysTrue())).hasValue(Path.of("archive"));
            assertThat(uri.query().parameter("ext1", String.class, alwaysTrue())).hasValue("v1");
            assertThat(uri.query().parameter("ext2", String.class, alwaysTrue())).hasValue("v2");
         });
      }

      @Test
      public void ofConfigWhenMultiValueExtensions() {
         assertThat(ArchiveDocumentUri.ofConfig(new ArchiveDocumentConfigBuilder()
                                                      .archiveScheme("jar")
                                                      .archiveDocument(ReferencedDocument.lazyLoad(new TestDocumentRepository(),
                                                                                                   DocumentPath.of(
                                                                                                         "/document.zip")))
                                                      .basePath(Path.of("archive"))
                                                      .extensions(map(LinkedHashMap::new,
                                                                      entry("ext1", list("v1", "v2")),
                                                                      entry("ext2", "v2")))
                                                      .build(), DocumentPath.of("entry"))).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo(
                  "jar:test:/document.zip!/archive/entry?basePath=archive&ext1=v1&ext1=v2&ext2=v2");
            assertThat(uri.archiveScheme()).isEqualTo("jar");
            assertThat(uri.targetUri()).isEqualTo(DocumentUri.ofDocumentUri("test:/document.zip"));
            assertThat(uri.documentId()).isEqualTo(DocumentPath.of("entry"));
            assertThat(uri.query().parameter(BASE_PATH, alwaysTrue())).hasValue(Path.of("archive"));
            assertThat(uri.query().parameters("ext1", String.class, alwaysTrue())).isEqualTo(list("v1",
                                                                                                  "v2"));
            assertThat(uri.query().parameter("ext2", String.class, alwaysTrue())).hasValue("v2");
         });
      }

   }

   @Nested
   public class ToUri {

      @Test
      public void toUriWhenNominal() {
         assertThat(ArchiveDocumentUri
                          .ofDocumentUri("zip:test:/document.zip!/archive/entry")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("zip:test:/document.zip!/archive/entry");
      }

      @Test
      public void toUriWhenBasePath() {
         assertThat(ArchiveDocumentUri
                          .ofDocumentUri("zip:test:/document.zip!/archive/entry?basePath=archive")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("zip:test:/document.zip!/archive/entry");
         assertThat(ArchiveDocumentUri
                          .ofDocumentUri("zip:test:/document.zip!/archive/entry?basePath=archive")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("zip:test:/document.zip!/archive/entry?basePath=archive");
      }

      @Test
      public void toUriWhenJar() {
         assertThat(ArchiveDocumentUri
                          .ofDocumentUri("jar:test:/document.zip!/archive/entry")
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo("jar:test:/document.zip!/archive/entry");
      }

      @Test
      public void toUriWhenTargetUriHasQuery() {
         assertThat(ArchiveDocumentUri
                          .ofDocumentUri(ArchiveUri.ofUri(
                                "zip:test:/document.zip?failFastIfMissingStoragePath=false!/archive/entry"))
                          .exportUri(noParameters())
                          .stringValue()).isEqualTo(
               "zip:test:/document.zip?failFastIfMissingStoragePath=false!/archive/entry");
      }

      @Test
      public void toUriWhenMultiValueExtensions() {
         assertThat(ArchiveDocumentUri
                          .ofDocumentUri("zip:test:/document.zip!/archive/entry?ext1=v1&ext1=v2")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("zip:test:/document.zip!/archive/entry?ext1=v1&ext1=v2");
      }

      @Test
      public void toUriWhenDefaultParameters() {
         assertThat(ArchiveDocumentUri
                          .ofDocumentUri("zip:test:/document.zip!/archive/entry?ext1=v1&ext2=v2")
                          .exportUri(defaultParameters())
                          .stringValue()).isEqualTo("zip:test:/document.zip!/archive/entry?ext1=v1&ext2=v2");
      }

      @Test
      public void toUriWhenDefaultParametersWithForceDefaultValues() {
         assertThat(ArchiveDocumentUri
                          .ofDocumentUri("zip:test:/document.zip!/archive/entry?ext1=v1&ext2=v2")
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo(
               "zip:test:/document.zip!/archive/entry?ext1=v1&ext2=v2&basePath=");
         assertThat(ArchiveDocumentUri
                          .ofDocumentUri("zip:test:/document.zip?query!/archive/entry?ext1=v1&ext2=v2")
                          .exportUri(defaultParameters(true))
                          .stringValue()).isEqualTo(
               "zip:test:/document.zip?query!/archive/entry?ext1=v1&ext2=v2&basePath=");
      }

      @Test
      public void toUriWhenSensitiveParameters() {
         assertThat(ArchiveDocumentUri
                          .ofDocumentUri("zip:test:/document.zip!/archive/entry?ext1=v1&ext2=v2")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo("zip:test:/document.zip!/archive/entry?ext1=v1&ext2=v2");
         assertThat(ArchiveDocumentUri
                          .ofDocumentUri(
                                "zip:test:/document.zip?query!/archive/entry?ext1=v1&ext2=v2&basePath=archive")
                          .exportUri(sensitiveParameters())
                          .stringValue()).isEqualTo(
               "zip:test:/document.zip?query!/archive/entry?ext1=v1&ext2=v2&basePath=archive");
      }

   }

   @Nested
   public class NormalizeResolveRelativize {

      @Test
      public void normalizeWhenNominal() {
         assertThat(ArchiveDocumentUri
                          .ofDocumentUri("zip:test:/../document.zip!/archive/entry")
                          .normalize()).satisfies(uri -> {
            assertThat(uri.value()).isEqualTo("zip:test:/../document.zip!/archive/entry");
         });
      }

      @Test
      public void resolveWhenNominal() {
         assertThatThrownBy(() -> ArchiveDocumentUri
               .ofDocumentUri("zip:test:/document.zip!/archive/entry")
               .resolve(uri("otherpath"))).isInstanceOf(UnsupportedOperationException.class);
      }

      @Test
      public void relativizeWhenNominal() {
         assertThatThrownBy(() -> ArchiveDocumentUri
               .ofDocumentUri("zip:test:/document.zip!/archive/entry")
               .relativize(uri("zip:otherpath"))).isInstanceOf(UnsupportedOperationException.class);
      }

   }

}