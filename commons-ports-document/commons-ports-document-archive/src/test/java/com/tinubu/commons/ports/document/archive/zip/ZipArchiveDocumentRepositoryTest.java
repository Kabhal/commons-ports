/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.archive.zip;

import static org.assertj.core.api.Assertions.assertThat;

import java.nio.file.Path;
import java.util.List;
import java.util.function.UnaryOperator;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ports.document.archive.ArchiveDocumentConfig.ArchiveDocumentConfigBuilder;
import com.tinubu.commons.ports.document.archive.BaseArchiveDocumentRepositoryTest;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentContent;
import com.tinubu.commons.ports.document.domain.DocumentEntryCriteria.DocumentEntryCriteriaBuilder;
import com.tinubu.commons.ports.document.domain.DocumentMetadata;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.LoadedDocumentContent.LoadedDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.ReferencedDocument;
import com.tinubu.commons.ports.document.domain.testsuite.ReadOnlyDocumentRepositoryTest;

public class ZipArchiveDocumentRepositoryTest {

   static {
      Assertions.setMaxStackTraceElementsDisplayed(50);
   }

   @Nested
   class CommonTestSuite extends ReadOnlyDocumentRepositoryTest {

      private final BaseArchiveDocumentRepositoryTest baseTestsuite = new BaseArchiveDocumentRepositoryTest();

      @BeforeEach
      public void configureDocumentRepository() {
         baseTestsuite.configureDocumentRepository();
      }

      @AfterEach
      public void closeDocumentRepository() {
         baseTestsuite.closeDocumentRepository();
      }

      @Override
      protected DocumentRepository documentRepository() {
         return baseTestsuite.documentRepository();
      }

      @Override
      protected Document zipTransformer(DocumentPath zipPath, List<Document> documents) {
         return baseTestsuite.zipTransformer(zipPath, documents);
      }

      @Override
      protected boolean isSupportingMetadataAttributes() {
         return false;
      }

      @Override
      protected UnaryOperator<DocumentMetadataBuilder> synchronizeExpectedMetadata(DocumentMetadata actual) {
         return builder -> builder
               .chain(super.synchronizeExpectedMetadata(actual))
               .attributes(actual.attributes())
               .contentType(actual.contentEncoding().orElse(null));
      }

      @Override
      protected UnaryOperator<LoadedDocumentContentBuilder> synchronizeExpectedContent(DocumentContent actual) {
         return builder -> builder
               .chain(super.synchronizeExpectedContent(actual))
               .contentEncoding(actual.contentEncoding().orElse(null));
      }

      @Nested
      public class DocumentRepositoryUriAdapter {

         @Test
         public void testSupportsRepositoryUriWhenNominal() {
            assertThat(documentRepository().supportsUri(repositoryUri("zip:test:/archive.zip!/"))).isTrue();
            assertThat(documentRepository().supportsUri(repositoryUri("zip:test:/archive.zip!"))).isTrue();
            assertThat(documentRepository().supportsUri(repositoryUri("zip:test:/archive.zip"))).isTrue();
         }

         @Test
         public void testSupportsRepositoryUriWhenBadArchiveDocument() {
            assertThat(documentRepository().supportsUri(repositoryUri("zip:unk:/archive.zip"))).isFalse();
            assertThat(documentRepository().supportsUri(repositoryUri("zip:test:/unknown.zip"))).isFalse();
         }

         @Test
         public void testSupportsDocumentUriWhenNominal() {
            assertThat(documentRepository().supportsUri(documentUri("zip:test:/archive.zip!/rootfile2.txt"))).isTrue();
         }

         @Test
         public void testSupportsDocumentUriWhenMismatchingBasePath() {
            assertThat(documentRepository().supportsUri(repositoryUri("zip:test:/archive.zip!/?basePath=other"))).isFalse();
         }

         @Test
         public void testToRepositoryUriWhenNominal() {
            assertThat(documentRepository().toUri().stringValue()).isEqualTo("zip:test:/archive.zip!/");
         }

         @Test
         public void testToDocumentUriWhenNominal() {
            assertThat(documentRepository().toUri(DocumentPath.of("file.txt")).stringValue()).isEqualTo(
                  "zip:test:/archive.zip!/file.txt");
            assertThat(documentRepository().toUri(DocumentPath.of("path/file.txt")).stringValue()).isEqualTo(
                  "zip:test:/archive.zip!/path/file.txt");
         }

         @Test
         public void testFindDocumentByUriWhenNominal() {
            DocumentPath documentPath = DocumentPath.of("path/pathfile2.txt");

            try {
               createDocuments(documentPath);

               assertThat(documentRepository().findDocumentByUri(documentUri(
                     "zip:test:/archive.zip!/path/pathfile2.txt"))).hasValueSatisfying(document -> {
                  assertThat(document.documentId()).isEqualTo(DocumentPath.of("path/pathfile2.txt"));
                  assertThat(document.metadata().documentPath()).isEqualTo(Path.of("path/pathfile2.txt"));
               });

            } finally {
               deleteDocuments(documentPath);
            }
         }
      }
   }

   @Nested
   public class WhenBasePath extends BaseArchiveDocumentRepositoryTest {

      protected ZipArchiveDocumentRepository newZipDocumentRepository(ReferencedDocument<DocumentRepository> archive) {
         return new ZipArchiveDocumentRepository(new ArchiveDocumentConfigBuilder()
                                                       .archiveScheme("zip")
                                                       .archiveDocument(newArchiveDocumentRepository())
                                                       .basePath(Path.of("path"))
                                                       .build());

      }

      @Test
      public void testFindDocumentsBySpecification() {
         DocumentPath documentPath = DocumentPath.of("pathfile2.txt");

         try {
            createDocuments(documentPath);

            assertThat(documentRepository().findDocumentsBySpecification(DocumentEntryCriteriaBuilder.ofDocumentPaths(
                  Path.of("pathfile2.txt")))).satisfiesExactly(document -> {
               assertThat(document.documentId()).isEqualTo(DocumentPath.of("pathfile2.txt"));
               assertThat(document.metadata().documentPath()).isEqualTo(Path.of("path/pathfile2.txt"));
            });
         } finally {
            deleteDocuments(documentPath);
         }
      }

      @Test
      public void testFindDocumentByUri() {
         DocumentPath documentPath = DocumentPath.of("pathfile2.txt");

         try {
            createDocuments(documentPath);

            assertThat(documentRepository().findDocumentByUri(documentUri(
                  "zip:test:/archive.zip!/path/pathfile2.txt?basePath=path"))).hasValueSatisfying(document -> {
               assertThat(document.documentId()).isEqualTo(DocumentPath.of("pathfile2.txt"));
               assertThat(document.metadata().documentPath()).isEqualTo(Path.of("path/pathfile2.txt"));
            });

         } finally {
            deleteDocuments(documentPath);
         }
      }

   }

}