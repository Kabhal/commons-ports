/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.archive;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.StreamUtils.stream;

import java.nio.file.Path;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import com.tinubu.commons.ports.document.archive.ArchiveDocumentConfig.ArchiveDocumentConfigBuilder;
import com.tinubu.commons.ports.document.archive.zip.ZipArchiveDocumentRepository;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.ReferencedDocument;
import com.tinubu.commons.ports.document.domain.repository.TestDocumentRepository;
import com.tinubu.commons.ports.document.domain.testsuite.BaseDocumentRepositoryTest;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ZipArchiver;

public class BaseArchiveDocumentRepositoryTest extends BaseDocumentRepositoryTest {

   private DocumentRepository documentRepository;
   private DocumentRepository archiveRepository;

   @BeforeEach
   public void configureDocumentRepository() {
      var archive = newArchiveDocumentRepository();
      this.archiveRepository = archive.documentRepository();
      this.documentRepository = newZipDocumentRepository(archive);
   }

   @AfterEach
   public void closeDocumentRepository() {
      this.documentRepository.close();
   }

   @Override
   public DocumentRepository documentRepository() {
      return documentRepository;
   }

   public DocumentRepository archiveRepository() {
      return archiveRepository;
   }

   @Override
   public Document zipTransformer(DocumentPath zipPath, List<Document> documents) {
      return new ZipArchiver(zipPath).compress(documents);
   }

   protected DocumentEntry createArchive(DocumentRepository archiveRepository,
                                         Path archive,
                                         Path... documents) {
      return createArchive(archiveRepository, archive, list(documents));
   }

   protected DocumentEntry createArchive(DocumentRepository archiveRepository,
                                         Path archive,
                                         List<Path> documents) {
      List<DocumentEntry> createdDocuments = list(documents
                                                        .stream()
                                                        .map(documentPath -> archiveRepository
                                                              .findDocumentEntryById(DocumentPath.of(
                                                                    documentPath))
                                                              .orElseGet(() -> createDocument(
                                                                    archiveRepository,
                                                                    documentPath))));

      return createZipDocument(archiveRepository,
                               DocumentPath.of(archive),
                               stream(createdDocuments)
                                     .map(DocumentEntry::documentId)
                                     .toArray(DocumentPath[]::new));
   }

   protected ReferencedDocument<DocumentRepository> newArchiveDocumentRepository() {
      var archiveDocumentRepository = TestDocumentRepository.sharedInstance("test");

      createArchive(archiveDocumentRepository, path("test.zip"), path("path/pathfile2.txt"));
      createArchive(archiveDocumentRepository, path("test.jar"), path("path/pathfile2.txt"));
      createArchive(archiveDocumentRepository, path("test.war"), path("path/pathfile2.txt"));
      createArchive(archiveDocumentRepository, path("test.ear"), path("path/pathfile2.txt"));
      createArchive(archiveDocumentRepository, path("test.bad"), path("path/pathfile2.txt"));

      var archiveDocument = path("archive.zip");

      createArchive(archiveDocumentRepository,
                    archiveDocument,
                    path("rootfile1.pdf"),
                    path("rootfile2.txt"),
                    path("path/pathfile1.pdf"),
                    path("path/pathfile2.txt"),
                    path("path/subpath/subpathfile1.pdf"),
                    path("path/subpath/subpathfile2.txt"),
                    path("otherpath/pathfile1.pdf"),
                    path("otherpath/pathfile2.txt"),
                    path("otherpath/subpath/subpathfile1.pdf"),
                    path("otherpath/subpath/subpathfile2.txt"),
                    path("test.zip"),
                    path("test.jar"),
                    path("test.war"),
                    path("test.ear"),
                    path("test.bad"));

      return ReferencedDocument
            .<DocumentRepository>lazyLoad(archiveDocumentRepository, DocumentPath.of(archiveDocument))
            .autoCloseRepository();
   }

   protected ZipArchiveDocumentRepository newZipDocumentRepository(ReferencedDocument<DocumentRepository> archive) {
      return new ZipArchiveDocumentRepository(new ArchiveDocumentConfigBuilder()
                                                    .archiveScheme("zip")
                                                    .archiveDocument(archive)
                                                    .build());

   }

}
