/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.archive;

import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotAbsolute;
import static com.tinubu.commons.lang.util.CheckedPredicate.alwaysTrue;
import static com.tinubu.commons.lang.util.CollectionUtils.entry;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.PathUtils.addRoot;
import static com.tinubu.commons.lang.util.PathUtils.emptyPath;
import static com.tinubu.commons.ports.document.archive.ArchiveRepositoryUri.Parameters.BASE_PATH;
import static com.tinubu.commons.ports.document.archive.ArchiveRepositoryUri.Parameters.configParameters;
import static com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery.ExportUriFilter.isAlwaysTrue;
import static com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery.ExportUriFilter.isParameterDefaultValue;
import static com.tinubu.commons.ports.document.domain.uri.UriUtils.exportUriFilter;

import java.net.URI;
import java.nio.file.Path;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.domain.type.Id;
import com.tinubu.commons.ddd2.invariant.Validate;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.invariant.rules.PathRules;
import com.tinubu.commons.ddd2.invariant.rules.domain.ids.UriRules;
import com.tinubu.commons.ddd2.uri.ComponentUri;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ddd2.uri.uris.ArchiveUri;
import com.tinubu.commons.ddd2.uri.uris.ArchiveUri.ArchiveUriRestrictions;
import com.tinubu.commons.lang.convert.ConversionFailedException;
import com.tinubu.commons.lang.util.Pair;
import com.tinubu.commons.lang.util.PathUtils;
import com.tinubu.commons.ports.document.archive.ArchiveDocumentConfig.ArchiveDocumentConfigBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.uri.AbstractRepositoryUri;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.ExportUriOptions;
import com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery;
import com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery.Parameter;
import com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery.ValueParameter;
import com.tinubu.commons.ports.document.domain.uri.RepositoryUri;

/**
 * Archive URI repository URI. This URI must be used to identify a repository, use
 * {@link ArchiveDocumentUri} to identify a document.
 * <p>
 * Supported URI format :
 * <ul>
 *    <li>{@code <archive-scheme>:<target-uri>[?<parameters>]}</li>
 *    <li>Commons-ports wrapped URI : {@code doc[ument]:archive:<archive-uri>}</li>
 * </ul>
 * Nested archive URI is supported, nested archive URI {@code !} separator can, but does not need, to be URL-encoded.
 * <p>
 * Supported URI parameters :
 * <ul>
 *    <li>{@code caseInsensitive} (Boolean) [false] : whether repository is case-insensitive for document search</li>
 * </ul>
 *
 * @see ArchiveDocumentUri
 */
public class ArchiveRepositoryUri extends AbstractRepositoryUri implements RepositoryUri {

   private static final String WRAPPED_URI_REPOSITORY_TYPE = "archive";

   protected final String archiveScheme;
   protected final DocumentUri targetUri;
   protected final Path basePath;
   protected final DocumentPath documentId;

   protected ArchiveRepositoryUri(ArchiveUri uri, boolean documentUri) {
      super(uri, documentUri);

      try {
         this.archiveScheme = uri.archiveScheme();
         var parsedUri = parseUri(documentUri);
         this.targetUri = parsedUri.getLeft();
         this.basePath = parsedUri.getRight().getLeft();
         this.documentId = parsedUri.getRight().getRight();
      } catch (ConversionFailedException e) {
         throw new InvalidUriException(uri, e).subMessage(e);
      }
   }

   @Override
   @SuppressWarnings("unchecked")
   protected Fields<? extends ArchiveRepositoryUri> defineDomainFields() {
      return Fields
            .<ArchiveRepositoryUri>builder()
            .superFields((Fields<ArchiveRepositoryUri>) super.defineDomainFields())
            .field("targetUri", v -> v.targetUri, isNotNull())
            .field("basePath", v -> v.basePath, PathRules.isNotAbsolute().andValue(hasNoTraversal()))
            .build();
   }

   /**
    * Creates a repository URI from {@link Uri}.
    *
    * @param uri repository URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static ArchiveRepositoryUri ofRepositoryUri(Uri uri) {
      Check.notNull(uri, "uri");

      validateUriCompatibility(uri, "uri", isCompatibleUriType(ArchiveUri.class));

      return buildUri(() -> new ArchiveRepositoryUri(archiveUri(uri), false));
   }

   /**
    * Creates a repository URI from {@link URI}.
    *
    * @param uri repository URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static ArchiveRepositoryUri ofRepositoryUri(URI uri) {
      Check.notNull(uri, "uri");

      return ofRepositoryUri(Uri.ofUri(uri));
   }

   /**
    * Creates a repository URI from URI string.
    *
    * @param uri repository URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static ArchiveRepositoryUri ofRepositoryUri(String uri) {
      Check.notNull(uri, "uri");

      return ofRepositoryUri(Uri.ofUri(uri));
   }

   /**
    * Creates a repository URI from configuration.
    *
    * @param config repository configuration
    *
    * @return new instance
    */
   public static ArchiveRepositoryUri ofConfig(ArchiveDocumentConfig config) {
      Check.notNull(config, "config");

      return buildUri(() -> new ArchiveRepositoryUri(archiveUri(config, null), false));
   }

   /**
    * Creates a repository URI from {@link RepositoryUri}. Specified repository URI can can be a
    * {@link RepositoryUri} to represent a "repository" URI, or a {@link DocumentUri} to represent a
    * "document" URI.
    *
    * @param uri repository URI
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    * @implSpec this method is package-private because user code should always create
    *       {@link RepositoryUri} directly from a URI.
    * @implNote Operation must be public because of inheritance
    */
   public static ArchiveRepositoryUri ofRepositoryUri(RepositoryUri uri) {
      Check.notNull(uri, "uri");

      validateUriCompatibility(uri, "uri", isCompatibleRepositoryUriType(ArchiveRepositoryUri.class));

      return buildUri(() -> new ArchiveRepositoryUri(archiveUri(uri), uri instanceof DocumentUri));
   }

   @Override
   public ArchiveRepositoryUri normalize() {
      return buildUri(() -> new ArchiveRepositoryUri(archiveUri().normalize(), documentId != null));
   }

   @Override
   public ArchiveRepositoryUri resolve(Uri uri) {
      throw new UnsupportedOperationException();
   }

   @Override
   public ArchiveRepositoryUri resolve(URI uri) {
      throw new UnsupportedOperationException();
   }

   @Override
   public ArchiveRepositoryUri relativize(Uri uri) {
      throw new UnsupportedOperationException();
   }

   @Override
   public ArchiveRepositoryUri relativize(URI uri) {
      throw new UnsupportedOperationException();
   }

   @Override
   public int compareTo(Id o) {
      if (!(o instanceof ArchiveRepositoryUri)) {
         return -1;
      } else if (o == this) {
         return 0;
      } else {
         return archiveUri().compareTo(((ArchiveRepositoryUri) o).archiveUri());
      }
   }

   /**
    * Returns a pre-configured builder from this repository URI.
    */
   public ArchiveDocumentConfigBuilder toConfig() {
      var query = query();

      var extensions = map(query
                                 .registeredParameters()
                                 .stream()
                                 .filter(p -> !p.registered())
                                 .filter(p -> p instanceof ValueParameter)
                                 .map(ValueParameter.class::cast)
                                 .map(p -> {
                                    var values = p.valuesOrDefault();

                                    if (values.isEmpty()) {
                                       return null;
                                    } else if (values.size() == 1) {
                                       return Pair.of(p.key(), values.get(0));
                                    } else {
                                       return Pair.of(p.key(), values);
                                    }
                                 }));

      return new ArchiveDocumentConfigBuilder()
            .<ArchiveDocumentConfigBuilder>reconstitute()
            .archiveScheme(archiveScheme)
            .archiveDocumentUri(targetUri)
            .basePath(basePath)
            .extensions(extensions);
   }

   @Override
   // FIXME There's no custom Query/ParameterizedQuery support in ArchiveUri, so we reinstantiate ParameterizedQuery each time and it's costly.
   public <T extends ComponentUri> ParameterizedQuery query() {
      return ((ArchiveUri) uri)
            .archiveQuery()
            .map(ParameterizedQuery::of)
            .orElseGet(ParameterizedQuery::ofEmpty)
            .registerParameters(parameters());
   }

   public ArchiveRepositoryUri restrictions(ArchiveUriRestrictions restrictions) {
      new ArchiveRepositoryUri(archiveUri().restrictions(restrictions), documentUri);
      return this;
   }

   public ArchiveUri archiveUri() {
      return (ArchiveUri) uri;
   }

   public String archiveScheme() {
      return archiveScheme;
   }

   public DocumentUri targetUri() {
      return this.targetUri;
   }

   /**
    * Base path to access archive entries in archive. Default to {@code ""}.
    *
    * @return archive entry base path, or {@code ""}
    */
   public Path basePath() {
      return basePath;
   }

   @Override
   public boolean supportsUri(RepositoryUri uri) {
      Check.notNull(uri, "uri");

      return supportsRepositoryUri(uri, ArchiveRepositoryUri.class, ArchiveRepositoryUri::ofRepositoryUri)
            .map(archiveRepositoryUri -> {
               if (!archiveRepositoryUri.archiveScheme().equalsIgnoreCase(archiveScheme())) {
                  return false;
               }

               if (!targetUri().supportsUri(archiveRepositoryUri.targetUri())) {
                  return false;
               }

               if (!basePath().equals(archiveRepositoryUri.basePath())) {
                  return false;
               }

               if (!query().includeParameters(archiveRepositoryUri.query(), isAlwaysTrue())) {
                  return false;
               }

               return true;
            })
            .orElse(false);
   }

   @Override
   public URI exportURI(ExportUriOptions options) {
      Check.notNull(options, "options");

      var filter = exportUriFilter(options, isAlwaysTrue());
      var exportQuery = query().markAllParametersAsRegistered().exportQuery(filter).noQueryIfEmpty(true);
      var exportUri = archiveUri().archiveQuery(exportQuery);

      return exportUri.toURI();
   }

   public RepositoryUri exportUri(ExportUriOptions options) {
      return RepositoryUri.ofRepositoryUri(exportURI(options));
   }

   /**
    * Creates a parameterized {@link ArchiveUri} from specified {@link Uri}.
    * <p>
    * If specified URI is wrapped, it is unwrapped before creating parameterized URI.
    * Specified URI is normalized and checked for remaining traversals, after unwrapping.
    *
    * @param uri URI
    *
    * @return new parameterized URI, with all registered parameters
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   protected static ArchiveUri archiveUri(Uri uri) {
      Check.notNull(uri, "uri");
      var unwrappedUri = unwrapUri(uri, WRAPPED_URI_REPOSITORY_TYPE);

      var archiveUri = ArchiveUri.ofUri(unwrappedUri, archiveDefaultRestrictions()).normalize();

      return Validate
            .validate(archiveUri, "uri", UriRules.hasNoTraversal())
            .orThrowMessage(message -> new InvalidUriException(uri).subMessage(message));
   }

   private static ArchiveUriRestrictions archiveDefaultRestrictions() {
      return ArchiveUriRestrictions.ofDefault().archiveQuery(false);
   }

   /**
    * Creates a parameterized (non-dynamic as it is unsupported) {@link ArchiveUri} from specified
    * configuration.
    *
    * @param config repository configuration
    * @param documentId optional document id
    *
    * @return new parameterized URI, with all registered value parameters from configuration
    */
   protected static ArchiveUri archiveUri(ArchiveDocumentConfig config, DocumentPath documentId) {

      var targetUri = config.archiveDocumentUri();

      var query = ParameterizedQuery
            .ofEmpty()
            .noQueryIfEmpty(true)
            .registerParameters(configParameters(config))
            .exportQuery(isParameterDefaultValue().negate());

      if (documentId == null) {
         return ArchiveUri.archive(config.archiveScheme(), targetUri, query);
      } else {
         return ArchiveUri.archive(config.archiveScheme(), targetUri, addRoot(config.basePath().resolve(documentId.value())), query);
      }

   }

   /**
    * Checks URI syntax and parses current parameterized URI to separate the document path.
    *
    * @param documentUri whether the current parameterized URI is a document URI, identifying a
    *       document instead of a repository
    *
    * @return parsed (target URI, (base path, archive entry)) pairs
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    * @implSpec The minimum set of parameters are checked here for consistency, only the parameters
    *       used in class internal state (basePath). Other parameters could be checked later.
    */
   protected Pair<DocumentUri, Pair<Path, DocumentPath>> parseUri(boolean documentUri) {
      DocumentUri targetUri;
      DocumentPath archiveEntry;

      targetUri = DocumentUri.ofDocumentUri(archiveUri().targetUri());

      var query = query();
      var basePathParameter = query.parameter(BASE_PATH, alwaysTrue()).orElseThrow().normalize();

      Validate
            .satisfies(basePathParameter, isNotAbsolute().andValue(hasNoTraversal()))
            .orThrowMessage(message -> new InvalidUriException(archiveUri()).subMessage(
                  "'%s=%s' parameter must be relative and must not contain traversal",
                  BASE_PATH.key(),
                  basePathParameter.toString()));

      if (!documentUri) {
         archiveEntry = null;
      } else {
         archiveEntry = archiveUri()
               .archiveEntry()
               .map(DocumentPath::of)
               .orElseThrow(() -> new InvalidUriException(archiveUri()).subMessage("Missing archive entry"));

         if ((!PathUtils.startsWith(archiveEntry.value(), basePathParameter)
              || PathUtils.equals(archiveEntry.value(), basePathParameter))) {
            throw new InvalidUriException(archiveUri()).subMessage(
                  "'%s=%s' parameter must start but not be equal to '%s' archive entry",
                  BASE_PATH.key(),
                  basePathParameter.toString(),
                  archiveEntry.value().toString());
         } else {
            archiveEntry = archiveEntry.value(PathUtils
                                                    .removeStart(archiveEntry.value(), basePathParameter)
                                                    .orElseThrow());
         }
      }

      return Pair.of(targetUri, Pair.of(basePathParameter, archiveEntry));
   }

   @SuppressWarnings("rawtypes")
   protected List<Parameter> parameters() {
      return list(BASE_PATH);
   }

   /**
    * Supported parameter registry.
    */
   static class Parameters {
      static final Parameter<Path> BASE_PATH =
            Parameter.registered("basePath", java.nio.file.Path.class).defaultValue(emptyPath());

      @SuppressWarnings({ "rawtypes", "unchecked" })
      static List<ValueParameter> configParameters(ArchiveDocumentConfig config) {
         Map<String, ValueParameter> parameters = map(LinkedHashMap::new,
                                                      entry(BASE_PATH.key(),
                                                            ValueParameter.ofValue(BASE_PATH,
                                                                                   config.basePath())));

         config.extensions().forEach((k, v) -> {
            if (v instanceof List) {
               var vList = (List) v;
               if (!vList.isEmpty()) {
                  var extensionParameter =
                        (Parameter<Object>) Parameter.registered(k, vList.get(0).getClass());
                  parameters.put(k, ValueParameter.ofValues(extensionParameter, vList));
               }
            } else {
               var extensionParameter = (Parameter<Object>) Parameter.registered(k, v.getClass());
               parameters.put(k, ValueParameter.ofValue(extensionParameter, v));
            }
         });

         return list(parameters.values());
      }

   }

}
