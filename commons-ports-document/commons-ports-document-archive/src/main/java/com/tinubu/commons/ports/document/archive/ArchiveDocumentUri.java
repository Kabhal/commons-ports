/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.archive;

import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;

import java.net.URI;

import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.domain.type.Id;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ddd2.uri.uris.ArchiveUri;
import com.tinubu.commons.ddd2.uri.uris.ArchiveUri.ArchiveUriRestrictions;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.ExportUriOptions;

/**
 * Archive URI document URI. This URI must be used to identify a document, use
 * {@link ArchiveRepositoryUri} to identify a repository.
 * <p>
 * You can use this class when a {@link ArchiveRepositoryUri} is required, but your current URI is a
 * document URI.
 * <p>
 * Supported URI format :
 * <ul>
 *    <li>{@code <archive-scheme>:<target-uri>/<document-id>[?<parameters>]}</li>
 *    <li>Commons-ports wrapped URI : {@code doc[ument]:archive:<archive-uri>}</li>
 * </ul>
 * <p>
 * Nested archive URI is supported, nested archive URI {@code !} separator can, but does not need, to be URL-encoded.
 *
 * @see ArchiveRepositoryUri
 */
public class ArchiveDocumentUri extends ArchiveRepositoryUri implements DocumentUri {

   protected ArchiveDocumentUri(ArchiveUri uri) {
      super(uri, true);
   }

   @Override
   @SuppressWarnings("unchecked")
   protected Fields<? extends ArchiveDocumentUri> defineDomainFields() {
      return Fields
            .<ArchiveDocumentUri>builder()
            .superFields((Fields<ArchiveDocumentUri>) super.defineDomainFields())
            .field("documentId", v -> v.documentId, isNotNull())
            .build();
   }

   /**
    * Creates a document URI from {@link Uri}.
    *
    * @param uri document URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static ArchiveDocumentUri ofDocumentUri(Uri uri) {
      Check.notNull(uri, "uri");

      validateUriCompatibility(uri, "uri", isCompatibleUriType(ArchiveUri.class));

      return buildUri(() -> new ArchiveDocumentUri(archiveUri(uri)));
   }

   /**
    * Creates a document URI from {@link URI}.
    *
    * @param uri document URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static ArchiveDocumentUri ofDocumentUri(URI uri) {
      Check.notNull(uri, "uri");

      return ofDocumentUri(Uri.ofUri(uri));
   }

   /**
    * Creates a document URI from URI string.
    * URI syntax is checked as part of this operation.
    *
    * @param uri document URI
    *
    * @return new instance
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    */
   public static ArchiveDocumentUri ofDocumentUri(String uri) {
      Check.notNull(uri, "uri");

      return ofDocumentUri(Uri.ofUri(uri));
   }

   /**
    * Creates a repository URI from configuration.
    *
    * @param config repository configuration
    * @param documentId document id
    *
    * @return new instance
    */
   public static ArchiveDocumentUri ofConfig(ArchiveDocumentConfig config, DocumentPath documentId) {
      Check.notNull(config, "config");
      Check.notNull(documentId, "documentId");

      return buildUri(() -> new ArchiveDocumentUri(archiveUri(config, documentId)));
   }

   /**
    * Creates a document URI from {@link DocumentUri}.
    *
    * @param uri document URI
    *
    * @throws InvalidUriException if URI is compatible but invalid
    * @throws IncompatibleUriException if URI is not compatible with this repository
    * @implSpec this method is package-private because user code should always create
    *       {@link DocumentUri} directly from a URI.
    * @implNote Operation must be public because of inheritance
    */
   public static ArchiveDocumentUri ofDocumentUri(DocumentUri uri) {
      Check.notNull(uri, "uri");

      validateUriCompatibility(uri, "uri", isCompatibleDocumentUriType(ArchiveDocumentUri.class));

      return buildUri(() -> new ArchiveDocumentUri(archiveUri(uri)));
   }

   public ArchiveDocumentUri restrictions(ArchiveUriRestrictions restrictions) {
      new ArchiveDocumentUri(archiveUri().restrictions(restrictions));
      return this;
   }

   public DocumentPath documentId() {
      return documentId;
   }

   @Override
   public DocumentUri exportUri(ExportUriOptions options) {
      return DocumentUri.ofDocumentUri(exportURI(options));
   }

   @Override
   public ArchiveDocumentUri normalize() {
      return buildUri(() -> new ArchiveDocumentUri(archiveUri().normalize()));
   }

   @Override
   public ArchiveDocumentUri resolve(Uri uri) {
      throw new UnsupportedOperationException();
   }

   @Override
   public ArchiveDocumentUri resolve(URI uri) {
      throw new UnsupportedOperationException();
   }

   @Override
   public ArchiveDocumentUri relativize(Uri uri) {
      throw new UnsupportedOperationException();
   }

   @Override
   public ArchiveDocumentUri relativize(URI uri) {
      throw new UnsupportedOperationException();
   }

   @Override
   public int compareTo(Id o) {
      if (!(o instanceof ArchiveDocumentUri)) {
         return -1;
      } else {
         return super.compareTo(o);
      }
   }
}
