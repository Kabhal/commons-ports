/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.archive;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotAbsolute;
import static com.tinubu.commons.lang.util.CollectionUtils.collection;
import static com.tinubu.commons.lang.util.ExceptionUtils.sneakyThrow;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.instanceOf;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.PathUtils.emptyPath;
import static com.tinubu.commons.lang.util.PathUtils.removeStart;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.DOCUMENT_URI;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.ITERABLE;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.METADATA_CONTENT_SIZE;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.METADATA_LAST_UPDATE_DATE;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.QUERYABLE;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.READABLE;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.REPOSITORY_URI;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.SUBPATH;

import java.nio.file.Path;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.apache.commons.lang3.time.StopWatch;

import com.tinubu.commons.ddd2.domain.event.RegistrableDomainEventService;
import com.tinubu.commons.ddd2.domain.event.SynchronousDomainEventService;
import com.tinubu.commons.ddd2.domain.repository.Repository;
import com.tinubu.commons.ddd2.domain.specification.Specification;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.lang.util.PathUtils;
import com.tinubu.commons.lang.validation.CheckReturnValue;
import com.tinubu.commons.ports.document.archive.ArchiveDocumentConfig.ArchiveDocumentConfigBuilder;
import com.tinubu.commons.ports.document.domain.AbstractDocumentRepository;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentEntry.DocumentEntryBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata;
import com.tinubu.commons.ports.document.domain.ReferencedDocument;
import com.tinubu.commons.ports.document.domain.capability.RepositoryCapability;
import com.tinubu.commons.ports.document.domain.capability.UnsupportedCapabilityException;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.ExportUriOptions;
import com.tinubu.commons.ports.document.domain.uri.RepositoryUri;
import com.tinubu.commons.ports.document.transformer.DocumentTransformer.OneToManyDocumentTransformer;

/**
 * Archive {@link DocumentRepository} adapter implementation.
 * <p>
 * Limitations :
 * <ul>
 *    <li>repository is not {@link RepositoryCapability#WRITABLE}</li>
 * </ul>
 *
 * @implSpec Immutable class implementation
 */
// FIXME review close resources strtegy and implem
// FIXME all repos : tests + what if documents are not in "basePath.relativize(physical name)" ? -> IllegalArgumentException et çà fait nawak surement !
public abstract class ArchiveDocumentRepository<T extends ArchiveDocumentRepository<T>>
      extends AbstractDocumentRepository<T> {
   private static final HashSet<RepositoryCapability> CAPABILITIES = collection(HashSet::new,
                                                                                REPOSITORY_URI,
                                                                                DOCUMENT_URI,
                                                                                READABLE,
                                                                                ITERABLE,
                                                                                QUERYABLE,
                                                                                SUBPATH,
                                                                                METADATA_LAST_UPDATE_DATE,
                                                                                METADATA_CONTENT_SIZE);

   protected final OneToManyDocumentTransformer archiveExtractor;
   protected final ArchiveDocumentConfig config;
   protected final ReferencedDocument<? extends DocumentRepository> archive;

   protected Predicate<Document> archiveLoadStrategy = __ -> true;
   protected List<Document> archiveEntries;

   /**
    * Archive will be closed on {@link #close}, so that both referenced document repository and document
    * are
    * closed internally. Document repository will be closed only if
    * {@link ReferencedDocument#autoCloseRepository()} is set.
    */
   protected ArchiveDocumentRepository(OneToManyDocumentTransformer archiveExtractor,
                                       ArchiveDocumentConfig config,
                                       RegistrableDomainEventService eventService) {
      super(eventService);

      this.archiveExtractor = Check.notNull(archiveExtractor, "archiveExtractor");
      this.config = Check.notNull(config, "config");
      this.archive = config.archiveDocument();
   }

   protected ArchiveDocumentRepository(OneToManyDocumentTransformer archiveExtractor,
                                       ArchiveDocumentConfig config) {
      this(archiveExtractor, config, new SynchronousDomainEventService());
   }

   protected abstract T recreate(OneToManyDocumentTransformer archiveExtractor,
                                 ArchiveDocumentConfig config,
                                 RegistrableDomainEventService eventService);

   /**
    * @implNote Currently reloading archive from source for each call, because source can be
    *       remote, and it's not always possible to keep opened the document input stream for later reading
    *       without timeout. Moreover, it's not possible to read multiple time the same entry from archive
    *       because stream would be already consumed, so that archive extractor is also applied each time. The
    *       alternative is to read archive and store it entirely in memory, but it can consume a lot of
    *       memory.
    */
   public List<Document> archiveEntries() {
      boolean reloadDocument = archive.document().map(archiveLoadStrategy::test).orElse(true);

      var archiveDocument = archive
            .document(true, reloadDocument)
            .orElseThrow(() -> new DocumentAccessException(String.format("Can't load '%s' archive document",
                                                                         archive.documentDisplayId())));

      if (archiveEntries == null || reloadDocument) {
         try {
            archiveEntries = archiveExtractor.transform(archiveDocument);
         } catch (Exception e) {
            throw new DocumentAccessException(String.format("Can't extract entries from '%s' archive document",
                                                            archiveDocument.documentId().stringValue()), e);
         }
      }

      return archiveEntries;
   }

   @Override
   public HashSet<RepositoryCapability> capabilities() {
      return CAPABILITIES;
   }

   @Override
   public boolean sameRepositoryAs(Repository<Document, DocumentPath> documentRepository) {
      validate(documentRepository, "documentRepository", isNotNull()).orThrow();

      if (this.getClass().isAssignableFrom(documentRepository.getClass())) {
         var archiveDocumentRepository = (ArchiveDocumentRepository<T>) documentRepository;

         return archiveDocumentRepository.archive
                      .documentRepository()
                      .sameRepositoryAs(archive.documentRepository()) && archiveDocumentRepository.archive
                      .documentId()
                      .sameValueAs(archive.documentId());
      } else {
         return false;
      }
   }

   @Override
   public T subPath(Path subPath, boolean shareContext) {
      validate(subPath, "subPath", isNotAbsolute().andValue(hasNoTraversal())).orThrow();

      return recreate(archiveExtractor,
                      ArchiveDocumentConfigBuilder
                            .from(config)
                            .basePath(config.basePath().resolve(subPath))
                            .build(),
                      eventService);
   }

   @Override
   @CheckReturnValue
   public Optional<Document> openDocument(DocumentPath documentId,
                                          boolean overwrite,
                                          boolean append,
                                          OpenDocumentMetadata metadata) {
      throw new UnsupportedCapabilityException(RepositoryCapability.OPEN);
   }

   @Override
   public Optional<Document> findDocumentById(DocumentPath documentId) {
      validate(documentId, "documentId", isNotNull()).orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEvent(() -> {
         return stream(archiveEntries())
               .filter(archiveEntry -> documentId(archiveEntry.documentId(), null)
                     .map(documentId::equals)
                     .orElse(false))
               .map(archiveEntry -> document(archiveEntry, null).orElseThrow())
               .findAny();
      }, d -> documentAccessed(d.documentEntry(), watch));
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryById(DocumentPath documentId) {
      validate(documentId, "documentId", isNotNull()).orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEntryEvent(() -> {
         return stream(archiveEntries())
               .flatMap(archiveEntry -> stream(documentEntry(archiveEntry.documentEntry(), null)))
               .filter(archiveEntry -> archiveEntry.documentId().equals(documentId))
               .findAny();
      }, d -> documentAccessed(d, watch));
   }

   @Override
   public Stream<Document> findDocumentsBySpecification(Path basePath,
                                                        Specification<DocumentEntry> specification) {
      validate(basePath, "basePath", isNotAbsolute().andValue(hasNoTraversal()))
            .and(validate(specification, "specification", isNotNull()))
            .orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentStreamEvent(() -> {
         return stream(archiveEntries())
               .filter(archiveEntry -> documentEntry(archiveEntry.documentEntry(), basePath)
                     .map(specification::satisfiedBy)
                     .orElse(false))
               .map(archiveEntry -> document(archiveEntry, basePath).orElseThrow());
      }, d -> documentAccessed(d.documentEntry(), watch));
   }

   @Override
   public Stream<DocumentEntry> findDocumentEntriesBySpecification(Path basePath,
                                                                   Specification<DocumentEntry> specification) {
      validate(basePath, "basePath", isNotAbsolute().andValue(hasNoTraversal()))
            .and(validate(specification, "specification", isNotNull()))
            .orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEntryStreamEvent(() -> {
         return stream(archiveEntries())
               .flatMap(archiveEntry -> stream(documentEntry(archiveEntry.documentEntry(), basePath)))
               .filter(specification::satisfiedBy);
      }, d -> documentAccessed(d, watch));
   }

   @Override
   public void close() {
      try {
         archive.close();
      } catch (Exception e) {
         throw sneakyThrow(e);
      }
   }

   /** Returns document path from an archive entry. */
   private Optional<DocumentPath> documentId(DocumentPath archiveDocumentId, Path basePath) {
      var prefix = config.basePath().resolve(nullable(basePath, emptyPath()));

      if (!PathUtils.startsWith(archiveDocumentId.value(), prefix)) {
         return optional();
      } else {
         return optional(DocumentPath.of(removeStart(archiveDocumentId.value(),
                                                     config.basePath()).orElseThrow()));
      }
   }

   /** Returns document entry from an archive entry. */
   private Optional<DocumentEntry> documentEntry(DocumentEntry archiveDocumentEntry, Path basePath) {
      var documentId = documentId(archiveDocumentEntry.documentId(), basePath);

      if (documentId.isEmpty()) {
         return optional();
      } else {
         return optional(DocumentEntryBuilder
                               .from(archiveDocumentEntry)
                               .documentId(documentId.orElseThrow())
                               .build());
      }
   }

   /** Returns document from an archive entry. */
   private Optional<Document> document(Document archiveDocumentEntry, Path basePath) {
      var documentId = documentId(archiveDocumentEntry.documentId(), basePath);

      if (documentId.isEmpty()) {
         return optional();
      } else {
         return optional(DocumentBuilder
                               .from(archiveDocumentEntry)
                               .documentId(documentId.orElseThrow())
                               .build());
      }
   }

   @Override
   @CheckReturnValue
   public Optional<DocumentEntry> saveDocument(Document document, boolean overwrite) {
      throw new UnsupportedCapabilityException(RepositoryCapability.WRITABLE);
   }

   @Override
   @CheckReturnValue
   public Optional<Document> saveAndReturnDocument(Document document, boolean overwrite) {
      throw new UnsupportedCapabilityException(RepositoryCapability.WRITABLE);
   }

   @Override
   @CheckReturnValue
   public Optional<DocumentEntry> deleteDocumentById(DocumentPath documentId) {
      throw new UnsupportedCapabilityException(RepositoryCapability.WRITABLE);
   }

   @Override
   @CheckReturnValue
   public List<DocumentEntry> deleteDocumentsBySpecification(Path basePath,
                                                             Specification<DocumentEntry> specification) {
      throw new UnsupportedCapabilityException(RepositoryCapability.WRITABLE);
   }

   @Override
   public ReferencedDocument<? extends DocumentRepository> referencedDocumentId(DocumentUri documentUri) {
      Check.notNull(documentUri, "documentUri");
      var archiveDocumentUri = validateArchiveDocumentUri(documentUri);

      return ReferencedDocument.lazyLoad(ArchiveDocumentRepository.this, archiveDocumentUri.documentId());
   }

   @Override
   public boolean supportsUri(RepositoryUri uri) {
      Check.notNull(uri, "uri");

      return supportsArchiveUri(uri);
   }

   @Override
   public RepositoryUri toUri(ExportUriOptions options) {
      Check.notNull(options, "options");

      return ArchiveRepositoryUri.ofConfig(config).exportUri(options);
   }

   @Override
   public DocumentUri toUri(DocumentPath documentId, ExportUriOptions options) {
      Check.notNull(documentId, "documentId");
      Check.notNull(options, "options");

      return ArchiveDocumentUri.ofConfig(config, documentId).exportUri(options);
   }

   @Override
   public Optional<Document> findDocumentByUri(DocumentUri documentUri) {
      Check.notNull(documentUri, "documentUri");
      var archiveDocumentUri = validateArchiveDocumentUri(documentUri);

      return findDocumentById(archiveDocumentUri.documentId());
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryByUri(DocumentUri documentUri) {
      Check.notNull(documentUri, "documentUri");
      var archiveDocumentUri = validateArchiveDocumentUri(documentUri);

      return findDocumentEntryById(archiveDocumentUri.documentId());
   }

   private boolean supportsArchiveUri(RepositoryUri repositoryUri) {
      try {
         return ArchiveRepositoryUri
               .ofConfig(config)
               .supportsUri(instanceOf(repositoryUri,
                                       ArchiveRepositoryUri.class).orElseGet(() -> ArchiveRepositoryUri.ofRepositoryUri(
                     repositoryUri)));
      } catch (IncompatibleUriException __) {
         return false;
      }
   }

   private ArchiveDocumentUri validateArchiveDocumentUri(DocumentUri documentUri) {
      try {
         var archiveDocumentUri = instanceOf(documentUri,
                                             ArchiveDocumentUri.class).orElseGet(() -> ArchiveDocumentUri.ofDocumentUri(
               documentUri));

         if (!supportsArchiveUri(archiveDocumentUri)) {
            throw new InvalidUriException(documentUri).message("'documentUri=%s' must be supported",
                                                               documentUri.toString());
         }

         return archiveDocumentUri;
      } catch (IncompatibleUriException e) {
         throw new InvalidUriException(documentUri).message("'documentUri=%s' must be supported: %s",
                                                            documentUri.toString(),
                                                            e.getMessage());
      }
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", ArchiveDocumentRepository.class.getSimpleName() + "[", "]")
            .add("config=" + config)
            .toString();
   }

}
