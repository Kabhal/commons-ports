/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.archive;

import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isMutuallyExclusive;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.allSatisfies;
import static com.tinubu.commons.ddd2.invariant.rules.MapRules.entrySet;
import static com.tinubu.commons.ddd2.invariant.rules.MapRules.hasNoBlankKey;
import static com.tinubu.commons.ddd2.invariant.rules.MapRules.hasNoNullValue;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotAbsolute;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.lang.util.CollectionUtils.immutable;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.ports.document.archive.Constants.DEFAULT_ARCHIVE_SCHEME;
import static com.tinubu.commons.ports.document.archive.Constants.DEFAULT_BASE_PATH;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.defaultParameters;

import java.nio.file.Path;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.rules.CollectionRules;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.beans.Setter;
import com.tinubu.commons.lang.util.CollectionUtils;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.DocumentRepositoryFactory;
import com.tinubu.commons.ports.document.domain.DocumentRepositoryLoader;
import com.tinubu.commons.ports.document.domain.ReferencedDocument;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.RepositoryUri;

/**
 * Archive document repository configuration.
 */
public class ArchiveDocumentConfig extends AbstractValue {

   protected static final DocumentRepositoryFactory DOCUMENT_REPOSITORY_FACTORY =
         DocumentRepositoryFactory.instance();

   protected final String archiveScheme;
   protected final DocumentUri archiveDocumentUri;
   protected final List<DocumentRepositoryLoader> loaders;
   protected final ReferencedDocument<? extends DocumentRepository> archiveDocument;
   protected final Path basePath;
   protected final Map<String, Object> extensions;

   public ArchiveDocumentConfig(ArchiveDocumentConfigBuilder builder) {
      this.archiveScheme = nullable(builder.archiveScheme, DEFAULT_ARCHIVE_SCHEME);
      this.archiveDocumentUri = builder.archiveDocumentUri;
      this.loaders = nullable(builder.loaders).map(CollectionUtils::immutable).orElse(null);
      this.archiveDocument = builder.archiveDocument;
      this.basePath = nullable(builder.basePath, DEFAULT_BASE_PATH).normalize();
      this.extensions = immutable(builder.extensions);
   }

   @Override
   public Fields<? extends ArchiveDocumentConfig> defineDomainFields() {
      return Fields
            .<ArchiveDocumentConfig>builder()
            .field("archiveScheme", v -> v.archiveScheme, isNotBlank())
            .field("archiveDocumentUri",
                   v -> v.archiveDocumentUri,
                   isNull().orValue(isMutuallyExclusive(value(archiveDocument, "archiveDocument"))))
            .field("loaders", v -> v.loaders, isNull().orValue(CollectionRules.hasNoNullElements()))
            .field("archiveDocument",
                   v -> v.archiveDocument,
                   isNull().orValue(isMutuallyExclusive(value(archiveDocumentUri, "archiveDocumentUri"))))
            .field("basePath", v -> v.basePath, isNull().orValue(isNotAbsolute().andValue(hasNoTraversal())))
            .field("extensions",
                   v -> v.extensions,
                   entrySet(allSatisfies(hasNoBlankKey().andValue(hasNoNullValue()))))
            .build();
   }

   public String archiveScheme() {
      return archiveScheme;
   }

   public DocumentUri archiveDocumentUri() {
      if (archiveDocumentUri == null) {
         return archiveDocument.toUri(defaultParameters(false));
      } else {
         return archiveDocumentUri;
      }
   }

   public Optional<List<DocumentRepositoryLoader>> loaders() {
      return nullable(loaders);
   }

   /**
    * Archive document to access entries from.
    * Specified {@link ReferencedDocument} will be closed with {@link ArchiveDocumentRepository#close()},
    * respecting the {@link ReferencedDocument#autoCloseRepository()} flag.
    * <p>
    * If {@link #archiveDocumentUri()} is set instead of archive document, dynamically creates referenced
    * document with {@link ReferencedDocument#autoCloseRepository()} set to {@code true}.
    *
    * @throws InvalidUriException if no registered document repository loader for archive document URI.
    */
   public ReferencedDocument<? extends DocumentRepository> archiveDocument() {
      if (archiveDocument == null) {
         var archiveDocumentRepository = archiveDocumentRepository(archiveDocumentUri,
                                                                   loaders).orElseThrow(() -> new InvalidUriException(
               archiveDocumentUri).subMessage(String.format(
               "No registered document repository loader for '%s'",
               archiveDocumentUri.toURI().toString())));

         return archiveDocumentRepository.referencedDocumentId(archiveDocumentUri).autoCloseRepository();
      } else {
         return archiveDocument;
      }
   }

   /**
    * Optional base path relative to archive root. This path must be relative. Set to {@code ""} by default.
    */
   public Path basePath() {
      return basePath;
   }

   /**
    * Extensions for specific archive implementations.
    *
    * @return extensions map
    */
   public Map<String, Object> extensions() {
      return extensions;
   }

   /**
    * Searches first registered document repository for specified URI.
    *
    * @param uri target repository URI
    * @param loaders optional restricted set of loaders, or {@code null}
    *
    * @return target document repository
    */
   private static Optional<DocumentRepository> archiveDocumentRepository(RepositoryUri uri,
                                                                         List<DocumentRepositoryLoader> loaders) {
      if (loaders != null) {
         return DOCUMENT_REPOSITORY_FACTORY.documentRepository(uri, loaders);
      } else {
         return DOCUMENT_REPOSITORY_FACTORY.documentRepository(uri);
      }
   }

   public static class ArchiveDocumentConfigBuilder extends DomainBuilder<ArchiveDocumentConfig> {

      private String archiveScheme;
      private DocumentUri archiveDocumentUri;
      private List<DocumentRepositoryLoader> loaders;
      private ReferencedDocument<? extends DocumentRepository> archiveDocument;
      private Path basePath;
      private Map<String, Object> extensions = map(LinkedHashMap::new);

      public static ArchiveDocumentConfigBuilder from(ArchiveDocumentConfig config) {
         return new ArchiveDocumentConfigBuilder()
               .<ArchiveDocumentConfigBuilder>reconstitute()
               .archiveScheme(config.archiveScheme)
               .archiveDocumentUri(config.archiveDocumentUri)
               .loaders(config.loaders)
               .archiveDocument(config.archiveDocument)
               .basePath(config.basePath);
      }

      @Getter
      public String archiveScheme() {
         return archiveScheme;
      }

      @Setter
      public ArchiveDocumentConfigBuilder archiveScheme(String archiveScheme) {
         this.archiveScheme = archiveScheme;
         return this;
      }

      @Getter
      public DocumentUri archiveDocumentUri() {
         return archiveDocumentUri;
      }

      @Setter
      public ArchiveDocumentConfigBuilder archiveDocumentUri(DocumentUri archiveDocumentUri) {
         this.archiveDocumentUri = archiveDocumentUri;
         return this;
      }

      @Getter
      public List<DocumentRepositoryLoader> loaders() {
         return loaders;
      }

      @Setter
      public ArchiveDocumentConfigBuilder loaders(List<DocumentRepositoryLoader> loaders) {
         this.loaders = loaders;
         return this;
      }

      @Getter
      public ReferencedDocument<? extends DocumentRepository> archiveDocument() {
         return archiveDocument;
      }

      @Setter
      public ArchiveDocumentConfigBuilder archiveDocument(ReferencedDocument<? extends DocumentRepository> archiveDocument) {
         this.archiveDocument = archiveDocument;
         return this;
      }

      @Setter
      public ArchiveDocumentConfigBuilder basePath(Path basePath) {
         this.basePath = basePath;
         return this;
      }

      @Getter
      public Path basePath() {
         return basePath;
      }

      @Setter
      public ArchiveDocumentConfigBuilder extensions(Map<String, Object> extensions) {
         this.extensions = map(LinkedHashMap::new, extensions);
         return this;
      }

      public ArchiveDocumentConfigBuilder extensions(String extensionName, Object extensionValue) {
         this.extensions.put(extensionName, extensionValue);
         return this;
      }

      @Getter
      public Map<String, Object> extensions() {
         return extensions;
      }

      @Override
      protected ArchiveDocumentConfig buildDomainObject() {
         return new ArchiveDocumentConfig(this);
      }
   }
}
