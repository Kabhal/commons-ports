/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.archive.zip;

import static com.tinubu.commons.lang.util.CheckedPredicate.alwaysFalse;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.ports.document.domain.uri.ParameterizedQuery.ExportUriFilter.excludeParametersByName;
import static com.tinubu.commons.ports.document.transformer.transformers.archive.zip.ZipConfiguration.CompressionLevel.NORMAL;
import static com.tinubu.commons.ports.document.transformer.transformers.archive.zip.ZipConfiguration.EncryptionMethod.ZIP_STANDARD;
import static java.nio.charset.StandardCharsets.UTF_8;

import java.nio.charset.Charset;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.StringJoiner;

import com.tinubu.commons.ddd2.domain.event.RegistrableDomainEventService;
import com.tinubu.commons.ddd2.domain.event.SynchronousDomainEventService;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.uri.uris.ArchiveUri.ArchiveUriRestrictions;
import com.tinubu.commons.lang.convert.ConversionService;
import com.tinubu.commons.lang.convert.services.StandardConversionService;
import com.tinubu.commons.lang.util.Pair;
import com.tinubu.commons.ports.document.archive.ArchiveDocumentConfig;
import com.tinubu.commons.ports.document.archive.ArchiveDocumentRepository;
import com.tinubu.commons.ports.document.archive.ArchiveDocumentUri;
import com.tinubu.commons.ports.document.archive.ArchiveRepositoryUri;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.DocumentRepositoryFactory;
import com.tinubu.commons.ports.document.domain.DocumentRepositoryLoader;
import com.tinubu.commons.ports.document.domain.ReferencedDocument;
import com.tinubu.commons.ports.document.domain.capability.RepositoryCapability;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.ExportUriOptions;
import com.tinubu.commons.ports.document.domain.uri.RepositoryUri;
import com.tinubu.commons.ports.document.transformer.DocumentTransformer.OneToManyDocumentTransformer;
import com.tinubu.commons.ports.document.transformer.transformers.archive.zip.ZipArchiveMetadata;
import com.tinubu.commons.ports.document.transformer.transformers.archive.zip.ZipConfiguration.CompressionLevel;
import com.tinubu.commons.ports.document.transformer.transformers.archive.zip.ZipConfiguration.EncryptionMethod;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ZipUnarchiver;

/**
 * ZIP Archive {@link DocumentRepository} adapter implementation.
 * <p>
 * Limitations :
 * <ul>
 *    <li>repository is not {@link RepositoryCapability#WRITABLE}</li>
 * </ul>
 * <p>
 * {@link ArchiveDocumentConfig} supported extensions :
 * <ul>
 *    <li>{@value #PASSWORD_EXTENSION} (String|char[]): archive password</li>
 *    <li>{@value #METADATA_ENCODING_EXTENSION} (String|{@link Charset}): archive metadata encoding</li>
 *    <li>{@value #COMPRESSION_LEVEL_EXTENSION} (String|{@link CompressionLevel}: archive compression level</li>
 *    <li>{@value #ENCRYPTION_METHOD_EXTENSION} (String|{@link EncryptionMethod}: archive encryption method</li>
 * </ul>
 *
 * @implSpec Immutable class implementation
 */
// FIXME new extractor does not support addEntry like lower-level Archive structure :/
public class ZipArchiveDocumentRepository extends ArchiveDocumentRepository<ZipArchiveDocumentRepository> {

   private static final List<String> DEFAULT_SUPPORTED_SCHEMES = list("zip", "jar");

   /** Password key in {@link ArchiveDocumentConfig#extensions()}. */
   private static final String PASSWORD_EXTENSION = "zip.password";
   /** Metadata encoding key in {@link ArchiveDocumentConfig#extensions()}. */
   private static final String METADATA_ENCODING_EXTENSION = "zip.metadata-encoding";
   /** Compression level key in {@link ArchiveDocumentConfig#extensions()}. */
   private static final String COMPRESSION_LEVEL_EXTENSION = "zip.compression-level";
   /** Encryption method key in {@link ArchiveDocumentConfig#extensions()}. */
   private static final String ENCRYPTION_METHOD_EXTENSION = "zip.encryption-method";

   /** Default metadata encoding. */
   private static final Charset DEFAULT_METADATA_ENCODING = UTF_8;
   /** Default compression level. */
   private static final CompressionLevel DEFAULT_COMPRESSION_LEVEL = NORMAL;
   /** Default encryption method. */
   private static final EncryptionMethod DEFAULT_ENCRYPTION_METHOD = ZIP_STANDARD;

   private static final ConversionService CONVERSION_SERVICE = new StandardConversionService();

   public ZipArchiveDocumentRepository(OneToManyDocumentTransformer archiveExtractor,
                                       ArchiveDocumentConfig config,
                                       RegistrableDomainEventService eventService) {
      super(archiveExtractor, config, eventService);
   }

   public ZipArchiveDocumentRepository(ArchiveDocumentConfig config,
                                       RegistrableDomainEventService eventService) {
      this(zipArchiveExtractor(config), config, eventService);
   }

   public ZipArchiveDocumentRepository(ArchiveDocumentConfig config) {
      this(config, new SynchronousDomainEventService());
   }

   private static OneToManyDocumentTransformer zipArchiveExtractor(ArchiveDocumentConfig config) {
      var password = nullable(config.extensions().get(PASSWORD_EXTENSION))
            .map(v -> CONVERSION_SERVICE.convert(v, char[].class))
            .orElse(null);
      var metadataEncoding = nullable(config.extensions().get(METADATA_ENCODING_EXTENSION))
            .map(v -> CONVERSION_SERVICE.convert(v, Charset.class))
            .orElse(DEFAULT_METADATA_ENCODING);

      var zipMetadata = ZipArchiveMetadata.ofDefault().password(password).metadataEncoding(metadataEncoding);

            /*var compressionLevel = nullable(config.extensions().get(COMPRESSION_LEVEL_EXTENSION))
            .map(v -> CONVERSION_SERVICE.convert(v, CompressionLevel.class))
            .orElse(DEFAULT_COMPRESSION_LEVEL);
      var encryptionMethod = nullable(config.extensions().get(ENCRYPTION_METHOD_EXTENSION))
            .map(v -> CONVERSION_SERVICE.convert(v, EncryptionMethod.class))
            .orElse(DEFAULT_ENCRYPTION_METHOD);
      var zipConfiguration =
            new ZipConfiguration().compressionLevel(compressionLevel).encryptionMethod(encryptionMethod);
            */

      return new ZipUnarchiver(zipMetadata);
   }

   /**
    * Creates a new document repository from URI with parameters.
    *
    * @param uri URI
    * @param supportedSchemes explicit list of supported schemes if actual archive is not in
    *       {@link #DEFAULT_SUPPORTED_SCHEMES default list}, or {@code null} to use default list
    * @param loaders optional list of loaders supported to retrieve the archive, or
    *       {@code null} to use all {@link DocumentRepositoryFactory#repositoryLoaderRegistry() registered}
    *       loaders
    *
    * @return new document repository
    *
    * @throws InvalidUriException if specified URI is compatible with this repository but invalid
    * @throws IncompatibleUriException if specified URI is not compatible with this repository
    */
   public static ZipArchiveDocumentRepository ofUri(RepositoryUri uri,
                                                    List<String> supportedSchemes,
                                                    List<DocumentRepositoryLoader> loaders) {
      Check.notNull(uri, "uri");

      var archiveRepositoryUri = ArchiveRepositoryUri
            .ofRepositoryUri(uri)
            .restrictions(ArchiveUriRestrictions
                                .ofDefault()
                                .schemes(nullable(supportedSchemes, DEFAULT_SUPPORTED_SCHEMES))
                                .archiveQuery(false));

      var archiveQuery = archiveRepositoryUri.query();

      var passwordExtension = archiveQuery
            .parameter(PASSWORD_EXTENSION, char[].class, alwaysFalse())
            .map(v -> Pair.of(PASSWORD_EXTENSION, v));
      var compressionLevel = archiveQuery
            .parameter(COMPRESSION_LEVEL_EXTENSION, CompressionLevel.class, alwaysFalse())
            .map(v -> Pair.of(COMPRESSION_LEVEL_EXTENSION, v));
      var encryptionMethod = archiveQuery
            .parameter(ENCRYPTION_METHOD_EXTENSION, EncryptionMethod.class, alwaysFalse())
            .map(v -> Pair.of(ENCRYPTION_METHOD_EXTENSION, v));

      LinkedHashMap<String, Object> extensions = map(LinkedHashMap::new,
                                                     passwordExtension.orElse(null),
                                                     compressionLevel.orElse(null),
                                                     encryptionMethod.orElse(null));

      var archiveConfig = archiveRepositoryUri.toConfig().loaders(loaders).extensions(extensions).build();

      return new ZipArchiveDocumentRepository(archiveConfig);
   }

   /**
    * Creates a new document repository from URI with parameters.
    * Use all {@link DocumentRepositoryFactory#repositoryLoaderRegistry() registered} loaders.
    *
    * @param uri URI
    *
    * @return new document repository
    *
    * @throws InvalidUriException if specified URI is compatible with this repository but invalid
    * @throws IncompatibleUriException if specified URI is not compatible with this repository
    */
   public static ZipArchiveDocumentRepository ofUri(RepositoryUri uri) {
      Check.notNull(uri, "uri");

      return ofUri(uri, null, null);
   }

   /**
    * Creates a lazily referenced document, in a new document repository, from URI with parameters. URI must
    * be compatible with this repository and identify a document.
    *
    * @param documentUri document URI
    * @param supportedSchemes explicit list of supported schemes if actual archive is not in
    *       {@link #DEFAULT_SUPPORTED_SCHEMES default list}, or {@code null} to use default list
    * @param loaders optional list of loaders supported to retrieve the archive, or
    *       {@code null} to use all {@link DocumentRepositoryFactory#repositoryLoaderRegistry() registered}
    *       loaders
    *
    * @return new document repository
    *
    * @throws InvalidUriException if specified URI is compatible with this repository but invalid
    * @throws IncompatibleUriException if specified URI is not compatible with this repository
    */
   // FIXME autoCloseRepository set in returned referenced Document ? update everywhere + doc
   // FIXME renamed referencedDocumentOfUri everywhere ? and DocumentRepositoryUriAdapter::referencedDocumentId -> referencedDocument ?
   public static ReferencedDocument<ZipArchiveDocumentRepository> referencedDocument(DocumentUri documentUri,
                                                                                     List<String> supportedSchemes,
                                                                                     List<DocumentRepositoryLoader> loaders) {
      Check.notNull(documentUri, "documentUri");

      var archiveDocumentUri = ArchiveDocumentUri
            .ofDocumentUri(documentUri)
            .restrictions(ArchiveUriRestrictions
                                .ofDefault()
                                .schemes(nullable(supportedSchemes, DEFAULT_SUPPORTED_SCHEMES))
                                .archiveQuery(false));

      var archiveConfig = archiveDocumentUri.toConfig().loaders(loaders).build();

      var archiveDocumentRepository = new ZipArchiveDocumentRepository(archiveConfig);

      return ReferencedDocument.lazyLoad(archiveDocumentRepository, archiveDocumentUri.documentId());
   }

   /**
    * Creates a lazily referenced document, in a new document repository, from URI with parameters. URI must
    * be compatible with this repository and identify a document.
    * Use all {@link DocumentRepositoryFactory#repositoryLoaderRegistry() registered} loaders.
    *
    * @param documentUri document URI
    *
    * @return new document repository
    *
    * @throws InvalidUriException if specified URI is compatible with this repository but invalid
    * @throws IncompatibleUriException if specified URI is not compatible with this repository
    */
   public static ReferencedDocument<ZipArchiveDocumentRepository> referencedDocument(DocumentUri documentUri) {
      return referencedDocument(documentUri, null, null);
   }

   @Override
   protected ZipArchiveDocumentRepository recreate(OneToManyDocumentTransformer archiveExtractor,
                                                   ArchiveDocumentConfig config,
                                                   RegistrableDomainEventService eventService) {
      return new ZipArchiveDocumentRepository(archiveExtractor, config, eventService);
   }

   @Override
   public RepositoryUri toUri(ExportUriOptions options) {
      Check.notNull(options, "options");

      if (options.excludeSensitiveParameters()) {
         options =
               options.filterParameters(filter -> filter.and(excludeParametersByName(PASSWORD_EXTENSION)));
      }

      return super.toUri(options);
   }

   @Override
   public DocumentUri toUri(DocumentPath documentId, ExportUriOptions options) {
      Check.notNull(documentId, "documentId");
      Check.notNull(options, "options");

      if (options.excludeSensitiveParameters()) {
         options =
               options.filterParameters(filter -> filter.and(excludeParametersByName(PASSWORD_EXTENSION)));
      }

      return super.toUri(documentId, options);
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", ZipArchiveDocumentRepository.class.getSimpleName() + "[", "]")
            .add("config=" + config)
            .toString();
   }

}
