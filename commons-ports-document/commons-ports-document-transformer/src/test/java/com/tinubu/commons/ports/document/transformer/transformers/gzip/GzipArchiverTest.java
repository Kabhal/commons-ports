/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.transformer.transformers.gzip;

import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.mimeType;
import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.parseMimeType;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.time.ZonedDateTime;

import org.apache.commons.codec.binary.Base64OutputStream;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.lang.datetime.ApplicationClock;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.OutputStreamDocumentContent.OutputStreamDocumentContentBuilder;

class GzipArchiverTest {

   private static final ZonedDateTime now = ZonedDateTime.now();

   @BeforeEach
   public void initializeApplicationClock() {
      ApplicationClock.setFixedClock(now);
   }

   @Test
   public void testSupportsWhenNominal() {
      GzipUnarchiver gzipUnarchiver = new GzipUnarchiver();

      assertThat(gzipUnarchiver.supports(stubDocument("test.gz", "content").build())).isTrue();
      assertThat(gzipUnarchiver.supports(stubDocument("test.gzip", "content").build())).isTrue();
      assertThat(gzipUnarchiver.supports(stubDocument("test.txt", "content")
                                               .<DocumentBuilder>chain(m -> m.contentType(mimeType(
                                                     "application",
                                                     "gzip")))
                                               .build())).isTrue();
      assertThat(gzipUnarchiver.supports(stubDocument("test.txt", "content")
                                               .<DocumentBuilder>chain(m -> m.contentType(mimeType(
                                                     "application",
                                                     "x-gzip")))
                                               .build())).isTrue();
   }

   @Test
   public void testSupportsWhenBadContentType() {
      GzipUnarchiver gzipUnarchiver = new GzipUnarchiver();

      assertThat(gzipUnarchiver.supports(stubDocument("test.txt", "content").build())).isFalse();
      assertThat(gzipUnarchiver.supports(stubDocument("test.zip", "content")
                                               .<DocumentBuilder>chain(m -> m.contentType(mimeType("text",
                                                                                                   "plain")))
                                               .build())).isFalse();
   }

   @Test
   public void testTransformWhenNominal() {
      Document document = new DocumentBuilder()
            .documentId(DocumentPath.of("test.txt"))
            .loadedContent("content", StandardCharsets.UTF_8)
            .contentType(parseMimeType("text/plain"))
            .build();

      Document gzip = new GzipArchiver().transform(document);

      assertThat(gzip.documentId()).isEqualTo(DocumentPath.of("test.txt.gz"));
      assertThat(gzip.metadata().simpleContentType()).hasValue(parseMimeType("application/gzip"));
      assertThat(gzip.metadata().documentPath()).isEqualTo(Path.of("test.txt.gz"));
      assertThat(gzip.content().contentSize()).hasValue(27L);
      assertThat(gzip.content().contentEncoding()).isEmpty();

      Document gunzip = new GzipUnarchiver().transform(gzip);

      assertThat(gunzip.documentId()).isEqualTo(DocumentPath.of("test.txt"));
      assertThat(gunzip.metadata().simpleContentType()).hasValue(parseMimeType("text/plain"));
      assertThat(gunzip.metadata().documentPath()).isEqualTo(Path.of("test.txt"));
      assertThat(gunzip.content().stringContent(UTF_8)).isEqualTo("content");
   }

   @Test
   public void testTransformWhenInvalidParameters() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> new GzipUnarchiver().transform(null))
            .withMessage("Invariant validation error > 'document' must not be null");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> new GzipUnarchiver().transform(stubDocument("test.txt", "content").build()))
            .withMessage(
                  "Invariant validation error > 'document.contentType=text/plain' must be in [application/gzip]");
   }

   @Test
   public void testTransformWhenEmptyDocuments() {
      Document document = new DocumentBuilder()
            .documentId(DocumentPath.of("test.txt"))
            .loadedContent("", StandardCharsets.UTF_8)
            .contentType(parseMimeType("text/plain"))
            .build();

      Document gzip = new GzipArchiver().transform(document);

      assertThat(gzip.documentId()).isEqualTo(DocumentPath.of("test.txt.gz"));
      assertThat(gzip.metadata().simpleContentType()).hasValue(parseMimeType("application/gzip"));
      assertThat(gzip.metadata().documentPath()).isEqualTo(Path.of("test.txt.gz"));
      assertThat(gzip.content().contentSize()).hasValue(20L);
      assertThat(gzip.content().contentEncoding()).isEmpty();

      Document gunzip = new GzipUnarchiver().transform(gzip);

      assertThat(gunzip.documentId()).isEqualTo(DocumentPath.of("test.txt"));
      assertThat(gunzip.metadata().simpleContentType()).hasValue(parseMimeType("text/plain"));
      assertThat(gunzip.metadata().documentPath()).isEqualTo(Path.of("test.txt"));
      assertThat(gunzip.content().stringContent(UTF_8)).isEqualTo("");
   }

   @Test
   public void testTransformWithOutputStreamContentWhenNominal() {
      try (ByteArrayOutputStream result = new ByteArrayOutputStream()) {
         Document document = stubOutputDocument(DocumentPath.of("test.txt"), result).build();
         Document gzip = new GzipArchiver().transform(document);

         try (Writer os = new OutputStreamWriter(gzip.content().outputStreamContent())) {
            os.write(
                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua");
         } catch (IOException e) {
            throw new IllegalStateException(e);
         }

         assertThat(gzip.documentId()).isEqualTo(DocumentPath.of("test.txt.gz"));
         assertThat(gzip.metadata().simpleContentType()).hasValue(parseMimeType("application/gzip"));
         assertThat(gzip.metadata().documentPath()).isEqualTo(Path.of("test.txt.gz"));
         assertThat(gzip.content().contentSize()).isEmpty();
         assertThat(gzip.content().contentEncoding()).isEmpty();
         assertThat(base64(result.toByteArray())).isEqualTo(
               "H4sIAAAAAAAA/yXMwQ1DMQgE0Va2gJSSJohBXysZ7BjoP5ZyHr15r2MO7myHrrkOkgVxqxfGirRRVn0gys0cjAc2eWOaXgBjpy9Fme+LGYNK7Sh0Ycrn7mH1XxtcnhDI5LflB+Zg0PZ6AAAA");
      } catch (IOException e) {
         throw new IllegalStateException(e);
      }
   }

   private String base64(byte[] content) {
      try (ByteArrayOutputStream result = new ByteArrayOutputStream();
           Base64OutputStream bOs = new Base64OutputStream(result, true, -1, null)) {
         IOUtils.write(content, bOs);
         return result.toString();
      } catch (IOException e) {
         throw new IllegalStateException(e);
      }
   }

   private DocumentBuilder stubDocument(DocumentPath documentId, String content) {
      DocumentBuilder documentBuilder =
            new DocumentBuilder().documentId(documentId).loadedContent(content, StandardCharsets.UTF_8);

      if (documentId.stringValue().endsWith(".gz") || documentId.stringValue().endsWith(".gzip")) {
         documentBuilder = documentBuilder.contentType(mimeType("application", "gzip"));
      }

      return documentBuilder;
   }

   private DocumentBuilder stubDocument(String documentId, String content) {
      return stubDocument(DocumentPath.of(documentId), content);
   }

   private DocumentBuilder stubOutputDocument(DocumentPath documentId, OutputStream outputStream) {
      DocumentBuilder documentBuilder = new DocumentBuilder()
            .documentId(documentId)
            .content(new OutputStreamDocumentContentBuilder()
                           .content(outputStream, StandardCharsets.UTF_8)
                           .build());

      if (documentId.stringValue().endsWith(".gz") || documentId.stringValue().endsWith(".gzip")) {
         documentBuilder = documentBuilder.contentType(mimeType("application", "gzip"));
      }

      return documentBuilder;
   }

}