/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.transformer.transformers.zip;

import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.mimeType;
import static com.tinubu.commons.ports.document.transformer.transformers.DocumentNameStrategy.DOCUMENT_ID_NAME_STRATEGY;
import static com.tinubu.commons.ports.document.transformer.transformers.DocumentNameStrategy.DOCUMENT_ID_PATH_STRATEGY;
import static com.tinubu.commons.ports.document.transformer.transformers.DocumentNameStrategy.DOCUMENT_LOGICAL_NAME_STRATEGY;
import static com.tinubu.commons.ports.document.transformer.transformers.DocumentNameStrategy.DOCUMENT_LOGICAL_PATH_STRATEGY;
import static com.tinubu.commons.ports.document.transformer.transformers.archive.zip.ZipArchiveFactory.ZipLibrary.JDK;
import static com.tinubu.commons.ports.document.transformer.transformers.archive.zip.ZipArchiveFactory.ZipLibrary.ZIP4J;
import static com.tinubu.commons.ports.document.transformer.transformers.archive.zip.ZipConfiguration.CompressionLevel.HIGH;
import static com.tinubu.commons.ports.document.transformer.transformers.archive.zip.ZipConfiguration.CompressionLevel.LOW;
import static com.tinubu.commons.ports.document.transformer.transformers.archive.zip.ZipConfiguration.CompressionLevel.MAXIMUM;
import static com.tinubu.commons.ports.document.transformer.transformers.archive.zip.ZipConfiguration.CompressionLevel.MINIMUM;
import static com.tinubu.commons.ports.document.transformer.transformers.archive.zip.ZipConfiguration.CompressionLevel.NONE;
import static com.tinubu.commons.ports.document.transformer.transformers.archive.zip.ZipConfiguration.CompressionLevel.NORMAL;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;

import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.List;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.transformer.DocumentTransformer.OneToOneDocumentTransformer;
import com.tinubu.commons.ports.document.transformer.transformers.archive.zip.ZipArchiveFactory;
import com.tinubu.commons.ports.document.transformer.transformers.archive.zip.ZipArchiveFactory.ZipLibrary;
import com.tinubu.commons.ports.document.transformer.transformers.archive.zip.ZipArchiveMetadata;
import com.tinubu.commons.ports.document.transformer.transformers.archive.zip.ZipConfiguration;
import com.tinubu.commons.ports.document.transformer.transformers.archive.zip.ZipConfiguration.EncryptionMethod;

class ZipArchiverSingleEntryArchiveTest extends AbstractZipTest {

   @ParameterizedTest
   @EnumSource(ZipLibrary.class)
   public void testSupportsWhenNominal(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      OneToOneDocumentTransformer zipArchiver =
            new ZipArchiver(DocumentPath.of("test.zip")).asSingleEntryArchive();

      assertThat(zipArchiver.supports(stubDocument(DocumentPath.of("test.zip")).build())).isTrue();
      assertThat(zipArchiver.supports(stubDocument(DocumentPath.of("test.txt"))
                                            .<DocumentBuilder>chain(m -> m.contentType(mimeType("application",
                                                                                                "zip")))
                                            .build())).isTrue();
   }

   @ParameterizedTest
   @EnumSource(ZipLibrary.class)
   public void testTransformWhenNominal(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      Document zip = new ZipArchiver(DocumentPath.of("archive.zip"))
            .asSingleEntryArchive()
            .transform(stubDocument(DocumentPath.of("path/test.txt")).build());

      assertThat(zip.documentId()).isEqualTo(DocumentPath.of("archive.zip"));
      assertThat(zip.metadata().contentType()).hasValue(mimeType("application", "zip"));
      if (library != ZIP4J) {
         assertThat(zip.metadata().contentSize()).hasValue(177L);
      } else {
         assertThat(zip.metadata().contentSize()).hasValue(155L);
      }
      assertThat(zip.metadata().creationDate()).hasValue(now.toInstant());
      assertThat(zip.metadata().lastUpdateDate()).hasValue(now.toInstant());

      List<Document> unzippedDocuments = new ZipUnarchiver().transform(zip);

      assertThat(unzippedDocuments).hasSize(1);
      assertThat(unzippedDocuments.get(0)).satisfies(test -> {
         assertThat(test.documentId()).isEqualTo(DocumentPath.of("path/test.txt"));
         assertThat(test.metadata().documentPath()).isEqualTo(Path.of("path/test.txt"));
         assertThat(test.metadata().contentType()).hasValue(mimeType("text", "plain"));
         assertThat(test.metadata().contentSize()).hasValue(13L);
         assertThatValidEntryMetadataDates(library, test);
         assertThat(test.content().stringContent(StandardCharsets.UTF_8)).isEqualTo("path/test.txt");
      });
   }

   @ParameterizedTest
   @EnumSource(ZipLibrary.class)
   public void testTransformWhenBadExtension(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      Document zip = new ZipArchiver(DocumentPath.of("archive.zop"))
            .asSingleEntryArchive()
            .transform(stubDocument(DocumentPath.of("path/test.txt")).build());

      assertThat(zip.documentId()).isEqualTo(DocumentPath.of("archive.zop"));
      assertThat(zip.metadata().contentType()).hasValue(mimeType("application", "zip"));
   }

   @ParameterizedTest
   @EnumSource(ZipLibrary.class)
   public void testTransformWhenNoExtension(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      Document zip = new ZipArchiver(DocumentPath.of("archive"))
            .asSingleEntryArchive()
            .transform(stubDocument(DocumentPath.of("path/test.txt")).build());

      assertThat(zip.documentId()).isEqualTo(DocumentPath.of("archive"));
      assertThat(zip.metadata().contentType()).hasValue(mimeType("application", "zip"));
   }

   @ParameterizedTest
   @EnumSource(ZipLibrary.class)
   public void testTransformWhenJar(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      Document zip = new ZipArchiver(DocumentPath.of("archive.jar"))
            .asSingleEntryArchive()
            .transform(stubDocument(DocumentPath.of("path/test.txt")).build());

      assertThat(zip.documentId()).isEqualTo(DocumentPath.of("archive.jar"));
      assertThat(zip.metadata().contentType()).hasValue(mimeType("application", "java-archive"));
   }

   @ParameterizedTest
   @EnumSource(ZipLibrary.class)
   public void testTransformWhenWar(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      Document zip = new ZipArchiver(DocumentPath.of("archive.war"))
            .asSingleEntryArchive()
            .transform(stubDocument(DocumentPath.of("path/test.txt")).build());

      assertThat(zip.documentId()).isEqualTo(DocumentPath.of("archive.war"));
      assertThat(zip.metadata().contentType()).hasValue(mimeType("application", "java-archive"));
   }

   @ParameterizedTest
   @EnumSource(ZipLibrary.class)
   public void testTransformWhenEar(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      Document zip = new ZipArchiver(DocumentPath.of("archive.ear"))
            .asSingleEntryArchive()
            .transform(stubDocument(DocumentPath.of("path/test.txt")).build());

      assertThat(zip.documentId()).isEqualTo(DocumentPath.of("archive.ear"));
      assertThat(zip.metadata().contentType()).hasValue(mimeType("application", "java-archive"));
   }

   @ParameterizedTest
   @EnumSource(ZipLibrary.class)
   public void testTransformWhenUnknownDocumentsEncoding(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      Document zip = new ZipArchiver(DocumentPath.of("archive.zip"))
            .asSingleEntryArchive()
            .transform(stubDocument(DocumentPath.of("path/test.txt"))
                             .loadedContent("path/test.txt".getBytes(StandardCharsets.UTF_8))
                             .build());

      assertThat(zip.documentId()).isEqualTo(DocumentPath.of("archive.zip"));
      assertThat(zip.metadata().contentType()).hasValue(mimeType("application", "zip"));
      if (library != ZIP4J) {
         assertThat(zip.metadata().contentSize()).hasValue(177L);
      } else {
         assertThat(zip.metadata().contentSize()).hasValue(155L);
      }
      assertThat(zip.metadata().creationDate()).hasValue(now.toInstant());
      assertThat(zip.metadata().lastUpdateDate()).hasValue(now.toInstant());

      List<Document> unzippedDocuments = new ZipUnarchiver().transform(zip);

      assertThat(unzippedDocuments).hasSize(1);
      assertThat(unzippedDocuments.get(0)).satisfies(test -> {
         assertThat(test.documentId()).isEqualTo(DocumentPath.of("path/test.txt"));
         assertThat(test.metadata().documentPath()).isEqualTo(Path.of("path/test.txt"));
         assertThat(test.metadata().contentType()).hasValue(mimeType("text", "plain"));
         assertThat(test.metadata().contentSize()).hasValue(13L);
         assertThatValidEntryMetadataDates(library, test);
         assertThat(test.content().stringContent(StandardCharsets.UTF_8)).isEqualTo("path/test.txt");
      });
   }

   @ParameterizedTest
   @EnumSource(ZipLibrary.class)
   public void testTransformWhenInvalidParameters(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> new ZipArchiver(null).asSingleEntryArchive())
            .withMessage("Invariant validation error > 'zipDocumentId' must not be null");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> new ZipArchiver(DocumentPath.of("archive.zip"), null).asSingleEntryArchive())
            .withMessage("Invariant validation error > 'zipEntryNameStrategy' must not be null");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> new ZipArchiver(DocumentPath.of("archive.zip"))
                  .asSingleEntryArchive().transform(null))
            .withMessage("Invariant validation error > 'document' must not be null");
   }

   @ParameterizedTest
   @EnumSource(ZipLibrary.class)
   public void testTransformWhenEmptyDocuments(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      Document zip = new ZipArchiver(DocumentPath.of("archive.zip"))
            .asSingleEntryArchive()
            .transform(stubDocument(DocumentPath.of("test.txt"), "").build());

      assertThat(zip.documentId()).isEqualTo(DocumentPath.of("archive.zip"));
      assertThat(zip.metadata().contentType()).hasValue(mimeType("application", "zip"));
      if (library != ZIP4J) {
         assertThat(zip.metadata().contentSize()).hasValue(154L);
      } else {
         assertThat(zip.metadata().contentSize()).hasValue(132L);
      }
      assertThat(zip.metadata().creationDate()).hasValue(now.toInstant());
      assertThat(zip.metadata().lastUpdateDate()).hasValue(now.toInstant());

      List<Document> unzippedDocuments = new ZipUnarchiver().transform(zip);

      assertThat(unzippedDocuments).hasSize(1);
      assertThat(unzippedDocuments.get(0)).satisfies(test -> {
         assertThat(test.metadata().contentSize()).hasValue(0L);
      });
   }

   @ParameterizedTest
   @EnumSource(ZipLibrary.class)
   public void testTransformWhenDocumentIdNameStrategy(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      Document zip = new ZipArchiver(DocumentPath.of("archive.zip"), DOCUMENT_ID_PATH_STRATEGY)
            .asSingleEntryArchive()
            .transform(stubDocument(DocumentPath.of("path/test.txt")).build());

      List<Document> unzippedDocuments = new ZipUnarchiver().transform(zip);

      assertThat(unzippedDocuments).hasSize(1);
      assertThat(unzippedDocuments.get(0).metadata().documentPath()).isEqualTo(Path.of("path", "test.txt"));
   }

   @ParameterizedTest
   @EnumSource(ZipLibrary.class)
   public void testTransformWhenDocumentIdFilenameStrategy(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      Document zip = new ZipArchiver(DocumentPath.of("archive.zip"), DOCUMENT_ID_NAME_STRATEGY)
            .asSingleEntryArchive()
            .transform(stubDocument(DocumentPath.of("path/test.txt")).build());

      List<Document> unzippedDocuments = new ZipUnarchiver().transform(zip);

      assertThat(unzippedDocuments).hasSize(1);
      assertThat(unzippedDocuments.get(0).metadata().documentPath()).isEqualTo(Path.of("test.txt"));
   }

   @ParameterizedTest
   @EnumSource(ZipLibrary.class)
   public void testTransformWhenDocumentPathStrategy(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      Document zip = new ZipArchiver(DocumentPath.of("archive.zip"), DOCUMENT_LOGICAL_PATH_STRATEGY)
            .asSingleEntryArchive()
            .transform(stubDocument(DocumentPath.of("path/test.txt")).build());

      List<Document> unzippedDocuments = new ZipUnarchiver().transform(zip);

      assertThat(unzippedDocuments).hasSize(1);
      assertThat(unzippedDocuments.get(0).metadata().documentPath()).isEqualTo(Path.of("path/test.txt"));
   }

   @ParameterizedTest
   @EnumSource(ZipLibrary.class)
   public void testTransformWhenDocumentNameStrategy(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      Document zip = new ZipArchiver(DocumentPath.of("archive.zip"), DOCUMENT_LOGICAL_NAME_STRATEGY)
            .asSingleEntryArchive()
            .transform(stubDocument(DocumentPath.of("path/test.txt")).build());

      List<Document> unzippedDocuments = new ZipUnarchiver().transform(zip);

      assertThat(unzippedDocuments).hasSize(1);
      assertThat(unzippedDocuments.get(0).metadata().documentPath()).isEqualTo(Path.of("test.txt"));
   }

   @ParameterizedTest
   @EnumSource(value = ZipLibrary.class)
   public void testTransformWhenPassword(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      if (library != JDK) {
         Document zip = new ZipArchiver(DocumentPath.of("archive.zip"),
                                        DOCUMENT_LOGICAL_NAME_STRATEGY,
                                        ZipArchiveMetadata.ofDefault().password("changeit".toCharArray()))
               .asSingleEntryArchive()
               .transform(stubDocument(DocumentPath.of("path/test.txt")).build());

         Document loadedZip = zip.loadContent();

         List<Document> unzippedDocuments =
               new ZipUnarchiver(ZipArchiveMetadata.ofDefault().password("changeit".toCharArray())).transform(
                     loadedZip);

         assertThat(unzippedDocuments).hasSize(1);
         assertThat(unzippedDocuments.get(0).metadata().documentPath()).isEqualTo(Path.of("test.txt"));

         assertThatExceptionOfType(DocumentAccessException.class).isThrownBy(() -> new ZipUnarchiver(
               ZipArchiveMetadata.ofDefault().password("wrong password".toCharArray())).transform(loadedZip));
         assertThatExceptionOfType(DocumentAccessException.class).isThrownBy(() -> new ZipUnarchiver(
               ZipArchiveMetadata.ofDefault()).transform(loadedZip));
      }
   }

   @ParameterizedTest
   @EnumSource(ZipLibrary.class)
   public void testTransformWhenEmptyPassword(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      if (library != JDK) {
         assertThatIllegalArgumentException()
               .isThrownBy(() -> new ZipArchiver(DocumentPath.of("archive.zip"),
                                                 DOCUMENT_LOGICAL_NAME_STRATEGY,
                                                 ZipArchiveMetadata
                                                       .ofDefault()
                                                       .password("".toCharArray())).asSingleEntryArchive())
               .withMessage("Invariant validation error > 'password' must not be blank");
      }
   }

   @ParameterizedTest
   @EnumSource(value = ZipLibrary.class)
   public void testTransformWhenStandardEncryption(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      if (library != JDK) {
         Document zip = new ZipArchiver(DocumentPath.of("archive.zip"),
                                        DOCUMENT_LOGICAL_NAME_STRATEGY,
                                        ZipArchiveMetadata.ofDefault().password("changeit".toCharArray()),
                                        new ZipConfiguration().encryptionMethod(EncryptionMethod.ZIP_STANDARD))
               .asSingleEntryArchive()
               .transform(stubDocument(DocumentPath.of("path/test.txt")).build());

         List<Document> unzippedDocuments =
               new ZipUnarchiver(ZipArchiveMetadata.ofDefault().password("changeit".toCharArray())).transform(
                     zip);

         assertThat(unzippedDocuments).hasSize(1);
         assertThat(unzippedDocuments.get(0).metadata().documentPath()).isEqualTo(Path.of("test.txt"));
      }
   }

   @ParameterizedTest
   @EnumSource(value = ZipLibrary.class)
   public void testTransformWhenAes128Encryption(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      if (library != JDK) {
         Document zip = new ZipArchiver(DocumentPath.of("archive.zip"),
                                        DOCUMENT_LOGICAL_NAME_STRATEGY,
                                        ZipArchiveMetadata.ofDefault().password("changeit".toCharArray()),
                                        new ZipConfiguration().encryptionMethod(EncryptionMethod.AES_128))
               .asSingleEntryArchive()
               .transform(stubDocument(DocumentPath.of("path/test.txt")).build());

         List<Document> unzippedDocuments =
               new ZipUnarchiver(ZipArchiveMetadata.ofDefault().password("changeit".toCharArray())).transform(
                     zip);

         assertThat(unzippedDocuments).hasSize(1);
         assertThat(unzippedDocuments.get(0).metadata().documentPath()).isEqualTo(Path.of("test.txt"));
      }
   }

   @ParameterizedTest
   @EnumSource(value = ZipLibrary.class)
   public void testTransformWhenAes256Encryption(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      if (library != JDK) {
         Document zip = new ZipArchiver(DocumentPath.of("archive.zip"),
                                        DOCUMENT_LOGICAL_NAME_STRATEGY,
                                        ZipArchiveMetadata.ofDefault().password("changeit".toCharArray()),
                                        new ZipConfiguration().encryptionMethod(EncryptionMethod.AES_256))
               .asSingleEntryArchive()
               .transform(stubDocument(DocumentPath.of("path/test.txt")).build());

         List<Document> unzippedDocuments =
               new ZipUnarchiver(ZipArchiveMetadata.ofDefault().password("changeit".toCharArray())).transform(
                     zip);

         assertThat(unzippedDocuments).hasSize(1);
         assertThat(unzippedDocuments.get(0).metadata().documentPath()).isEqualTo(Path.of("test.txt"));
      }
   }

   @ParameterizedTest
   @EnumSource(value = ZipLibrary.class)
   public void testTransformWhenNoCompressionLevel(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      Document zip = new ZipArchiver(DocumentPath.of("archive.zip"),
                                     DOCUMENT_LOGICAL_NAME_STRATEGY, ZipArchiveMetadata.ofDefault(),
                                     new ZipConfiguration().compressionLevel(NONE))
            .asSingleEntryArchive()
            .transform(stubDocument(DocumentPath.of("path/test.txt")).build());

      List<Document> unzippedDocuments = new ZipUnarchiver().transform(zip);

      assertThat(unzippedDocuments).hasSize(1);
      assertThat(unzippedDocuments.get(0).metadata().documentPath()).isEqualTo(Path.of("test.txt"));
   }

   @ParameterizedTest
   @EnumSource(value = ZipLibrary.class)
   public void testTransformWhenMinimumCompressionLevel(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      Document zip = new ZipArchiver(DocumentPath.of("archive.zip"),
                                     DOCUMENT_LOGICAL_NAME_STRATEGY, ZipArchiveMetadata.ofDefault(),
                                     new ZipConfiguration().compressionLevel(MINIMUM))
            .asSingleEntryArchive()
            .transform(stubDocument(DocumentPath.of("path/test.txt")).build());

      List<Document> unzippedDocuments = new ZipUnarchiver().transform(zip);

      assertThat(unzippedDocuments).hasSize(1);
      assertThat(unzippedDocuments.get(0).metadata().documentPath()).isEqualTo(Path.of("test.txt"));
   }

   @ParameterizedTest
   @EnumSource(value = ZipLibrary.class)
   public void testTransformWhenLowCompressionLevel(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      Document zip = new ZipArchiver(DocumentPath.of("archive.zip"),
                                     DOCUMENT_LOGICAL_NAME_STRATEGY, ZipArchiveMetadata.ofDefault(),
                                     new ZipConfiguration().compressionLevel(LOW))
            .asSingleEntryArchive()
            .transform(stubDocument(DocumentPath.of("path/test.txt")).build());

      List<Document> unzippedDocuments = new ZipUnarchiver().transform(zip);

      assertThat(unzippedDocuments).hasSize(1);
      assertThat(unzippedDocuments.get(0).metadata().documentPath()).isEqualTo(Path.of("test.txt"));
   }

   @ParameterizedTest
   @EnumSource(value = ZipLibrary.class)
   public void testTransformWhenNormalCompressionLevel(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      Document zip = new ZipArchiver(DocumentPath.of("archive.zip"),
                                     DOCUMENT_LOGICAL_NAME_STRATEGY, ZipArchiveMetadata.ofDefault(),
                                     new ZipConfiguration().compressionLevel(NORMAL))
            .asSingleEntryArchive()
            .transform(stubDocument(DocumentPath.of("path/test.txt")).build());

      List<Document> unzippedDocuments = new ZipUnarchiver().transform(zip);

      assertThat(unzippedDocuments).hasSize(1);
      assertThat(unzippedDocuments.get(0).metadata().documentPath()).isEqualTo(Path.of("test.txt"));
   }

   @ParameterizedTest
   @EnumSource(value = ZipLibrary.class)
   public void testTransformWhenHighCompressionLevel(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      Document zip = new ZipArchiver(DocumentPath.of("archive.zip"),
                                     DOCUMENT_LOGICAL_NAME_STRATEGY, ZipArchiveMetadata.ofDefault(),
                                     new ZipConfiguration().compressionLevel(HIGH))
            .asSingleEntryArchive()
            .transform(stubDocument(DocumentPath.of("path/test.txt")).build());

      List<Document> unzippedDocuments = new ZipUnarchiver().transform(zip);

      assertThat(unzippedDocuments).hasSize(1);
      assertThat(unzippedDocuments.get(0).metadata().documentPath()).isEqualTo(Path.of("test.txt"));
   }

   @ParameterizedTest
   @EnumSource(value = ZipLibrary.class)
   public void testTransformWhenMaximumCompressionLevel(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      Document zip = new ZipArchiver(DocumentPath.of("archive.zip"),
                                     DOCUMENT_LOGICAL_NAME_STRATEGY, ZipArchiveMetadata.ofDefault(),
                                     new ZipConfiguration().compressionLevel(MAXIMUM))
            .asSingleEntryArchive()
            .transform(stubDocument(DocumentPath.of("path/test.txt")).build());

      List<Document> unzippedDocuments = new ZipUnarchiver().transform(zip);

      assertThat(unzippedDocuments).hasSize(1);
      assertThat(unzippedDocuments.get(0).metadata().documentPath()).isEqualTo(Path.of("test.txt"));
   }

   private DocumentBuilder stubDocument(DocumentPath documentId, String content) {
      return new DocumentBuilder().documentId(documentId).loadedContent(content, StandardCharsets.UTF_8);
   }

   private DocumentBuilder stubDocument(DocumentPath documentId) {
      return stubDocument(documentId, documentId.stringValue());
   }

}