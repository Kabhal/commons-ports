/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.transformer.transformers.base64;

import static java.nio.charset.StandardCharsets.US_ASCII;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentContent;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent.InputStreamDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.OutputStreamDocumentContent.OutputStreamDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.processor.DocumentContentProcessor;

class Base64DecoderTest {

   @Test
   public void testRawDecoderWhenNominal() {
      Document document = stubDocument("test",
                                       "TG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdCwgc2VkIGRvIGVpdXNtb2QgdGVtcG9yIGluY2lkaWR1bnQgdXQgbGFib3JlIGV0IGRvbG9yZSBtYWduYSBhbGlxdWE=");

      Document encodedDocument = Base64Decoder.decoder().process(document);

      assertThat(encodedDocument.content().contentSize()).isEmpty();
      assertThat(encodedDocument.content().contentEncoding()).isEmpty();
      assertThat(encodedDocument.content().stringContent(UTF_8)).isEqualTo(
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua");
   }

   @Test
   public void testDecoderWhenEmpty() {
      Document document = stubDocument("test", "");

      Document encodedDocument = Base64Decoder.decoder().process(document);

      assertThat(encodedDocument.content().contentSize()).isEmpty();
      assertThat(encodedDocument.content().contentEncoding()).isEmpty();
      assertThat(encodedDocument.content().stringContent(UTF_8)).isEqualTo("");
   }

   @Test
   public void testDecoderWhenOneByte() {
      Document document = stubDocument("test", "YQ==");

      Document encodedDocument = Base64Decoder.decoder().process(document);

      assertThat(encodedDocument.content().stringContent(UTF_8)).isEqualTo("a");
   }

   @Test
   public void testDecoderWhenCustomEncoding() {
      Document document = stubDocument("test",
                                       "TG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdCwgc2VkIGRvIGVpdXNtb2QgdGVtcG9yIGluY2lkaWR1bnQgdXQgbGFib3JlIGV0IGRvbG9yZSBtYWduYSBhbGlxdWE=");
      document =
            document.process((DocumentContentProcessor) documentContent -> new InputStreamDocumentContentBuilder()
                  .content(documentContent.inputStreamContent(),
                           US_ASCII,
                           documentContent.contentSize().orElse(null))
                  .build());

      Document encodedDocument = Base64Decoder.decoder().process(document);

      assertThat(encodedDocument.content().contentEncoding()).isEmpty();
      assertThat(encodedDocument.content().stringContent(UTF_8)).isEqualTo(
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua");
   }

   @Test
   public void testDecoderWhenLineLength() {
      Document document = stubDocument("test",
                                       "TG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdCwgc2VkIGRvIGVpdXNtb2QgdGVt\r\n"
                                       + "cG9yIGluY2lkaWR1bnQgdXQgbGFib3JlIGV0IGRvbG9yZSBtYWduYSBhbGlxdWE=\r\n");

      Document encodedDocument = Base64Decoder.decoder().process(document);

      assertThat(encodedDocument.content().stringContent(UTF_8)).isEqualTo(
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua");
   }

   @Test
   public void testDecoderWhenExactMatchAndMultipleOfFourLineLength() {
      Document document = stubDocument("test",
                                       "TG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdCwgc2VkIGRvIGVpdXNtb2QgdGVtcG9yIGluY2lkaWR1bnQgdXQgbGFib3JlIGV0IGRvbG9yZSBtYWduYSBhbGlxdWEr\r\n");

      Document encodedDocument = Base64Decoder.decoder().process(document);

      assertThat(encodedDocument.content().stringContent(UTF_8)).isEqualTo(
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua+");
   }

   @Test
   public void testDecoderWhenExactMatchAndNotMultipleOfFourLineLength() {
      Document document = stubDocument("test",
                                       "TG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdCwgc2VkIGRvIGVpdXNtb2QgdGVtcG9yIGluY2lkaWR1bnQgdXQgbGFib3JlIGV0IGRvbG9yZSBtYWduYSBhbGlxdWEr\r\nKw==\r\n");

      Document encodedDocument = Base64Decoder.decoder().process(document);

      assertThat(encodedDocument.content().stringContent(UTF_8)).isEqualTo(
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua++");
   }

   @Test
   public void testDecoderWhenLineLengthAndLineSeparator() {
      Document document = stubDocument("test",
                                       "TG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdCwgc2VkIGRvIGVpdXNtb2QgdGVt\n"
                                       + "cG9yIGluY2lkaWR1bnQgdXQgbGFib3JlIGV0IGRvbG9yZSBtYWduYSBhbGlxdWE=\n");

      Document encodedDocument = Base64Decoder.decoder().process(document);

      assertThat(encodedDocument.content().stringContent(UTF_8)).isEqualTo(
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua");
   }

   @Test
   public void testDecoderWhenLineLengthAndNoFinalLineSeparator() {
      Document document = stubDocument("test",
                                       "TG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdCwgc2VkIGRvIGVpdXNtb2QgdGVt\r\n"
                                       + "cG9yIGluY2lkaWR1bnQgdXQgbGFib3JlIGV0IGRvbG9yZSBtYWduYSBhbGlxdWE=");

      Document encodedDocument = Base64Decoder.decoder().process(document);

      assertThat(encodedDocument.content().stringContent(UTF_8)).isEqualTo(
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua");
   }

   @Test
   public void testDecoderWhenContentEncoding() {
      Document document = stubDocument("test",
                                       "TG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdCwgc2VkIGRvIGVpdXNtb2QgdGVtcG9yIGluY2lkaWR1bnQgdXQgbGFib3JlIGV0IGRvbG9yZSBtYWduYSBhbGlxdWE=");

      Document encodedDocument = Base64Decoder.decoder(US_ASCII).process(document);

      assertThat(encodedDocument.content().contentEncoding()).hasValue(US_ASCII);
      assertThat(encodedDocument.content().stringContent()).isEqualTo(
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua");
   }

   @Test
   public void testDecoderWhenContentEncodingAndSize() {
      Document document = stubDocument("test",
                                       "TG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdCwgc2VkIGRvIGVpdXNtb2QgdGVtcG9yIGluY2lkaWR1bnQgdXQgbGFib3JlIGV0IGRvbG9yZSBtYWduYSBhbGlxdWE=");

      Document encodedDocument = Base64Decoder.decoder(US_ASCII, 122L).process(document);

      assertThat(encodedDocument.content().contentEncoding()).hasValue(US_ASCII);
      assertThat(encodedDocument.content().contentSize()).hasValue(122L);
      assertThat(encodedDocument.content().stringContent()).isEqualTo(
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua");
   }

   @Test
   public void testDecoderWithOutputStreamContentWhenNominal() {
      try (OutputStream result = new ByteArrayOutputStream()) {
         Document document = stubOutputDocument(result).process(Base64Decoder.decoder());

         try (Writer os = new OutputStreamWriter(document.content().outputStreamContent())) {
            os.write(
                  "TG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdCwgc2VkIGRvIGVpdXNtb2QgdGVtcG9yIGluY2lkaWR1bnQgdXQgbGFib3JlIGV0IGRvbG9yZSBtYWduYSBhbGlxdWE=");
         } catch (IOException e) {
            throw new IllegalStateException(e);
         }

         assertThat(document.content().contentSize()).isEmpty();
         assertThat(document.content().contentEncoding()).isEmpty();
         assertThat(result.toString()).isEqualTo(
               "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua");
      } catch (IOException e) {
         throw new IllegalStateException(e);
      }
   }

   private static Document stubDocument(DocumentPath documentId, String content) {
      return new DocumentBuilder()
            .documentId(documentId)
            .streamContent(content, StandardCharsets.UTF_8)
            .build();
   }

   private static Document stubDocument(String documentId, String content) {
      return stubDocument(DocumentPath.of(documentId), content);
   }

   private static Document stubDocument(DocumentContent content) {
      return new DocumentBuilder().documentId(DocumentPath.of("test")).content(content).build();
   }

   private static Document stubOutputDocument(OutputStream outputStream) {
      return stubOutputDocument(new OutputStreamDocumentContentBuilder()
                                      .content(outputStream, StandardCharsets.UTF_8)
                                      .build());
   }

   private static Document stubOutputDocument(DocumentContent content) {
      return new DocumentBuilder().documentId(DocumentPath.of("test")).content(content).build();
   }

}