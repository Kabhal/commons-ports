/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.transformer.transformers.base64;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;

import org.apache.commons.codec.CodecPolicy;
import org.apache.commons.codec.binary.Base64InputStream;
import org.apache.commons.codec.binary.Base64OutputStream;

import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentContent;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent.InputStreamDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.LoadedDocumentContent;
import com.tinubu.commons.ports.document.domain.OutputStreamDocumentContent;
import com.tinubu.commons.ports.document.domain.OutputStreamDocumentContent.OutputStreamDocumentContentBuilder;

/**
 * Base64 encoder as defined by <a href="http://www.ietf.org/rfc/rfc2045.txt">RFC 2045</a>.
 * <p>
 * This encoder directly streams encoded content without extra memory consumption.
 * <p>
 * Original document content encoding and content size is replaced with Base64 size and
 * {@link AbstractBase64Transformer#BASE64_ENCODING} encoding.
 * <p>
 * Note that document metadata content type is not changed by this transformer, as Base64 is considered a
 * content-transfer transformer.
 */
public class Base64Encoder extends AbstractBase64Transformer {

   protected final int lineLength;
   protected final byte[] lineSeparator;

   protected Base64Encoder(int lineLength, byte[] lineSeparator) {
      this.lineLength = lineLength;
      this.lineSeparator = nullable(lineSeparator, CHUNK_SEPARATOR);
   }

   /**
    * Base64 encoder with configurable line max length and line separators.
    *
    * @param lineLength max line length, no max line length if <= 0
    * @param lineSeparator line separator to use if there's a maximum line length. Set to {@code \r\n}
    *       if unset
    *
    * @return Base64 encoder
    */
   public static Base64Encoder encoder(int lineLength, byte[] lineSeparator) {
      return new Base64Encoder(lineLength, lineSeparator);
   }

   /**
    * Base64 encoder with configurable line max length.
    *
    * @param lineLength max line length, no max line length if <= 0
    *
    * @return Base64 encoder
    */
   public static Base64Encoder encoder(int lineLength) {
      return new Base64Encoder(lineLength, CHUNK_SEPARATOR);
   }

   /**
    * Base64 encoder without line max length.
    *
    * @return Base64 encoder
    */
   public static Base64Encoder encoder() {
      return new Base64Encoder(-1, null);
   }

   /**
    * Base64 encoder with Base64 for MIME configuration.
    *
    * @return Base64 encoder
    */
   public static Base64Encoder mimeEncoder() {
      return new Base64Encoder(MIME_CHUNK_SIZE, CHUNK_SEPARATOR);
   }

   /**
    * Base64 encoder with Base64 for PEM configuration.
    *
    * @return Base64 encoder
    */
   public static Base64Encoder pemEncoder() {
      return new Base64Encoder(PEM_CHUNK_SIZE, CHUNK_SEPARATOR);
   }

   @Override
   public DocumentContent process(DocumentContent documentContent) {
      validate(documentContent, "documentContent", isNotNull()).orThrow();

      Long contentSize = documentContent
            .contentSize()
            .map(cs -> encodedLength(cs, lineLength, lineSeparator))
            .orElse(null);

      if (documentContent instanceof InputStreamDocumentContent
          || documentContent instanceof LoadedDocumentContent) {
         try {
            return InputStreamDocumentContentBuilder
                  .from(documentContent)
                  .content(new Base64InputStream(documentContent.inputStreamContent(),
                                                 true,
                                                 lineLength,
                                                 lineSeparator,
                                                 CodecPolicy.STRICT))
                  .contentEncoding(BASE64_ENCODING)
                  .contentSize(contentSize)
                  .build();
         } catch (Exception e) {
            throw new DocumentAccessException(e.getMessage(), e);
         }
      } else if (documentContent instanceof OutputStreamDocumentContent) {
         try {
            return OutputStreamDocumentContentBuilder.from(documentContent)
                  .contentEncoding(BASE64_ENCODING)
                  .contentMapper(o -> new Base64OutputStream(o,
                                                             true,
                                                             lineLength,
                                                             lineSeparator,
                                                             CodecPolicy.STRICT))
                  .build();
         } catch (Exception e) {
            throw new DocumentAccessException(e.getMessage(), e);
         }
      } else {
         throw new IllegalStateException(String.format("Unsupported '%s' content type",
                                                       documentContent.getClass().getName()));
      }
   }

}
