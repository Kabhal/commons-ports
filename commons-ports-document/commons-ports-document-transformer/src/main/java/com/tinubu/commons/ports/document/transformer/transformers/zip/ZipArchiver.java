/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.transformer.transformers.zip;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.allSatisfies;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_JAVA_ARCHIVE;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_ZIP;
import static com.tinubu.commons.lang.util.CollectionUtils.list;

import java.io.InputStream;
import java.io.UncheckedIOException;
import java.nio.file.Path;
import java.time.Instant;
import java.util.List;

import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.transformer.DocumentTransformer;
import com.tinubu.commons.ports.document.transformer.DocumentTransformer.ManyToOneDocumentTransformer;
import com.tinubu.commons.ports.document.transformer.transformers.DocumentNameStrategy;
import com.tinubu.commons.ports.document.transformer.transformers.archive.Archive;
import com.tinubu.commons.ports.document.transformer.transformers.archive.ArchiveEntry;
import com.tinubu.commons.ports.document.transformer.transformers.archive.ArchiveException;
import com.tinubu.commons.ports.document.transformer.transformers.archive.zip.ZipArchiveFactory;
import com.tinubu.commons.ports.document.transformer.transformers.archive.zip.ZipArchiveMetadata;
import com.tinubu.commons.ports.document.transformer.transformers.archive.zip.ZipConfiguration;

/**
 * ZIP archive compressor.
 * <p>
 * Memory consumption is equal to the size of archived data.
 */
public class ZipArchiver implements ManyToOneDocumentTransformer {

   /** Supported mime-types for zip files. */
   private static final List<MimeType> SUPPORTED_MIME_TYPES = list(APPLICATION_ZIP, APPLICATION_JAVA_ARCHIVE);
   /** Default zip entry naming strategy when not specified. */
   private static final DocumentNameStrategy DEFAULT_ENTRY_NAMING_STRATEGY =
         DocumentNameStrategy.DOCUMENT_ID_PATH_STRATEGY;
   public static final PresetMimeTypeRegistry MIME_TYPE_REGISTRY = PresetMimeTypeRegistry.ofPresets();

   private final DocumentPath zipDocumentId;
   private final DocumentNameStrategy zipEntryNameStrategy;
   private final ZipArchiveMetadata options;
   private final ZipConfiguration config;

   public ZipArchiver(DocumentPath zipDocumentId,
                      DocumentNameStrategy zipEntryNameStrategy, ZipArchiveMetadata options,
                      ZipConfiguration config) {
      this.zipDocumentId = validate(zipDocumentId, "zipDocumentId", isNotNull()).orThrow();
      this.zipEntryNameStrategy =
            validate(zipEntryNameStrategy, "zipEntryNameStrategy", isNotNull()).orThrow();
      this.options = validate(options, "options", isNotNull()).orThrow();
      this.config = validate(config, "config", isNotNull()).orThrow();
   }

   public ZipArchiver(DocumentPath zipDocumentId,
                      DocumentNameStrategy zipEntryNameStrategy, ZipArchiveMetadata options) {
      this(zipDocumentId, zipEntryNameStrategy, options, new ZipConfiguration());
   }

   public ZipArchiver(DocumentPath zipDocumentId, DocumentNameStrategy zipEntryNameStrategy) {
      this(zipDocumentId, zipEntryNameStrategy, ZipArchiveMetadata.ofDefault());
   }

   public ZipArchiver(DocumentPath zipDocumentId) {
      this(zipDocumentId, DEFAULT_ENTRY_NAMING_STRATEGY);
   }

   /** Zip document id. */
   public DocumentPath zipDocumentId() {
      return zipDocumentId;
   }

   /**
    * Zip entry naming strategy.
    *
    * @see DocumentNameStrategy DocumentNameStrategy for preset strategies
    */
   public DocumentNameStrategy zipEntryNameStrategy() {
      return zipEntryNameStrategy;
   }

   /**
    * Checks if specified documents can be added to zip archive.
    *
    * @param documents documents to check
    *
    * @return {@code true} if all specified documents are supported
    */
   @Override
   public boolean supports(List<Document> documents) {
      return validate(documents, "documents", isValidDocuments()).success();
   }

   /**
    * Creates a new zip document containing all specified documents.
    * An empty zip document is returned if there are no documents.
    *
    * @param documents documents to archive
    *
    * @return zip document
    *
    * @throws DocumentAccessException if an I/O error occurs
    */
   @Override
   public Document transform(List<Document> documents) throws DocumentAccessException {
      validate(documents, "documents", isValidDocuments()).orThrow();

      return zipDocuments(documents);
   }

   /**
    * Alias for {@link DocumentTransformer#transform(Object)}.
    *
    * @param documents documents to compress
    *
    * @return Zip compressed document
    *
    * @throws DocumentAccessException if an I/O error occurs
    */
   public Document compress(List<Document> documents) {
      return transform(documents);
   }

   protected InvariantRule<List<Document>> isValidDocuments() {
      return allSatisfies(isValidDocument());
   }

   protected InvariantRule<Document> isValidDocument() {
      return isNotNull();
   }

   protected Document zipDocuments(List<Document> documents) {
      try {
         Archive zipArchive = ZipArchiveFactory.fromEntries(documents
                                                                  .stream()
                                                                  .map(document -> documentEntry(document,
                                                                                                 zipEntryNameStrategy)),
                                                            options,
                                                            config);
         return new DocumentBuilder()
               .documentId(zipDocumentId)
               .contentType(zipContentType(zipDocumentId))
               .streamContent(zipArchive.content(), zipArchive.contentSize().orElse(null))
               .build();
      } catch (ArchiveException e) {
         throw new DocumentAccessException(e.getMessage(), e);
      } catch (UncheckedIOException e) {
         throw new DocumentAccessException(e);
      }
   }

   /**
    * Selects archive content-type depending on extension.
    * This enables different mime-types/extensions for ZIP archive.
    *
    * @param documentId zip document id
    *
    * @return zip content type
    *
    * @see #SUPPORTED_MIME_TYPES
    */
   private MimeType zipContentType(DocumentPath documentId) {
      return MIME_TYPE_REGISTRY
            .mimeType(documentId.value())
            .filter(SUPPORTED_MIME_TYPES::contains)
            .orElse(APPLICATION_ZIP);
   }

   protected static ArchiveEntry documentEntry(Document document,
                                               DocumentNameStrategy documentEntryNameStrategy) {
      return new ArchiveEntry() {
         @Override
         public Path name() {
            return documentEntryNameStrategy.name(document);
         }

         @Override
         public InputStream content() {
            return document.content().inputStreamContent();
         }

         @Override
         public Long contentSize() {
            return document.metadata().contentSize().orElse(null);
         }

         @Override
         public Instant creationDate() {
            return document.metadata().creationDate().orElse(null);
         }

         @Override
         public Instant lastUpdateDate() {
            return document.metadata().lastUpdateDate().orElse(null);
         }
      };
   }

   /**
    * Zip single document {@link OneToOneDocumentTransformer} adapter.
    * This will convert a single document to a single ZIP archive with a single entry.
    * <p>
    * Memory consumption is equal to the size of archived data.
    *
    * @return one to one transformer from the specified document
    */
   public OneToOneDocumentTransformer asSingleEntryArchive() {
      return new OneToOneDocumentTransformer() {

         @Override
         public boolean supports(Document document) {
            return ZipArchiver.this.supports(list(document));
         }

         @Override
         public Document transform(Document document) throws DocumentAccessException {
            validate(document, "document", isValidDocument()).orThrow();

            return ZipArchiver.this.transform(list(document));
         }
      };
   }

}
