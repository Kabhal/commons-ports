/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.transformer;

import static java.util.Collections.singletonList;

import java.util.List;

import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.processor.DocumentProcessor;

/**
 * General transformer interface.
 *
 * @param <S> source object type, e.g. {@link Document}, or {@code List<Document>}
 * @param <T> target object type, e.g. {@link Document}, or {@code List<Document>}
 */
public interface DocumentTransformer<S, T> {

   /**
    * Returns {@code true} if the transformer supports this source.
    *
    * @param source source to check
    *
    * @return {@code true} if the transformer supports this source
    */
   boolean supports(S source);

   /**
    * Main transformation operation.
    *
    * @param source source to transform
    *
    * @throws DocumentAccessException if an I/O error occurs
    */
   T transform(S source) throws DocumentAccessException;

   /**
    * General purpose document list to document transformer.
    */
   interface ManyToOneDocumentTransformer extends DocumentTransformer<List<Document>, Document> {}

   /**
    * General purpose document to document list transformer.
    */
   interface OneToManyDocumentTransformer extends DocumentTransformer<Document, List<Document>> {}

   /**
    * General purpose document to document transformer.
    */
   interface OneToOneDocumentTransformer extends DocumentTransformer<Document, Document>, DocumentProcessor {

      @Override
      default Document process(Document document) {
         return transform(document);
      }

      /**
       * Generic adapter from {@link OneToOneDocumentTransformer} to {@link OneToManyDocumentTransformer}.
       *
       * @return one to many document transformer from this transformer
       */
      default OneToManyDocumentTransformer asOneToManyTransformer() {
         return new OneToManyDocumentTransformer() {
            @Override
            public boolean supports(Document source) {
               return OneToOneDocumentTransformer.this.supports(source);
            }

            @Override
            public List<Document> transform(Document source) throws DocumentAccessException {
               return singletonList(OneToOneDocumentTransformer.this.transform(source));
            }
         };
      }

   }
}
