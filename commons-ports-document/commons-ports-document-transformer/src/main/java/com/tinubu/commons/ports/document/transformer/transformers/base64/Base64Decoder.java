/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.transformer.transformers.base64;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;

import java.nio.charset.Charset;

import org.apache.commons.codec.CodecPolicy;
import org.apache.commons.codec.binary.Base64InputStream;
import org.apache.commons.codec.binary.Base64OutputStream;

import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentContent;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent.InputStreamDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.LoadedDocumentContent;
import com.tinubu.commons.ports.document.domain.OutputStreamDocumentContent;
import com.tinubu.commons.ports.document.domain.OutputStreamDocumentContent.OutputStreamDocumentContentBuilder;

/**
 * Base64 decoder as defined by <a href="http://www.ietf.org/rfc/rfc2045.txt">RFC 2045</a>.
 * <p>
 * This decoder directly streams decoded content without extra memory consumption.
 * <p>
 * Decoded document content encoding and content size is always unset by default. You can provide these
 * values
 * to the constructor if you have them from a side channel.
 * <p>
 * Note that document metadata content type is not changed by this transformer, as Base64 is considered a
 * content-transfer transformer.
 */
public class Base64Decoder extends AbstractBase64Transformer {

   private final Charset contentEncoding;
   private final Long contentSize;

   protected Base64Decoder(Charset contentEncoding, Long contentSize) {
      this.contentEncoding = contentEncoding;
      this.contentSize = contentSize;
   }

   protected Base64Decoder() {
      this(null, null);
   }

   /**
    * Base64 decoder.
    *
    * @return Base64 decoder
    */
   public static Base64Decoder decoder() {
      return new Base64Decoder();
   }

   /**
    * Base64 decoder.
    *
    * @return Base64 decoder
    */
   public static Base64Decoder decoder(Charset contentEncoding, Long contentSize) {
      return new Base64Decoder(contentEncoding, contentSize);
   }

   /**
    * Base64 decoder.
    *
    * @return Base64 decoder
    */
   public static Base64Decoder decoder(Charset contentEncoding) {
      return new Base64Decoder(contentEncoding, null);
   }

   @Override
   public DocumentContent process(DocumentContent documentContent) {
      validate(documentContent, "documentContent", isNotNull()).orThrow();

      if (documentContent instanceof InputStreamDocumentContent
          || documentContent instanceof LoadedDocumentContent) {
         try {
            return InputStreamDocumentContentBuilder
                  .from(documentContent)
                  .content(new Base64InputStream(documentContent.inputStreamContent(),
                                                 false,
                                                 -1,
                                                 null,
                                                 CodecPolicy.STRICT))
                  .contentEncoding(contentEncoding)
                  .contentSize(contentSize)
                  .build();
         } catch (Exception e) {
            throw new DocumentAccessException(e.getMessage(), e);
         }
      } else if (documentContent instanceof OutputStreamDocumentContent) {
         try {
            return OutputStreamDocumentContentBuilder.from(documentContent)
                  .contentEncoding(contentEncoding)
                  .contentMapper(o -> new Base64OutputStream(o, false, -1, null, CodecPolicy.STRICT))
                  .build();
         } catch (Exception e) {
            throw new DocumentAccessException(e.getMessage(), e);
         }
      } else {
         throw new IllegalStateException(String.format("Unsupported '%s' content type",
                                                       documentContent.getClass().getName()));
      }
   }

}
