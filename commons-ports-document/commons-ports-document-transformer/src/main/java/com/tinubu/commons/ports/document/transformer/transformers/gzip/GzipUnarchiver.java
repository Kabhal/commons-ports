/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.transformer.transformers.gzip;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.EqualsRules.isIn;
import static com.tinubu.commons.ddd2.invariant.rules.MimeTypeRules.withStrippedExperimental;
import static com.tinubu.commons.ddd2.invariant.rules.MimeTypeRules.withStrippedParameters;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_GZIP;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.ports.document.domain.rules.DocumentRules.DocumentMainRules.metadata;
import static com.tinubu.commons.ports.document.domain.rules.DocumentRules.DocumentMetadataRules.contentType;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.GZIPInputStream;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry;
import com.tinubu.commons.lang.util.MechanicalSympathy;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent;
import com.tinubu.commons.ports.document.domain.LoadedDocumentContent;
import com.tinubu.commons.ports.document.domain.OutputStreamDocumentContent;
import com.tinubu.commons.ports.document.domain.OutputStreamDocumentContent.OutputStreamDocumentContentBuilder;
import com.tinubu.commons.ports.document.transformer.DocumentTransformer;
import com.tinubu.commons.ports.document.transformer.DocumentTransformer.OneToOneDocumentTransformer;

/**
 * Gzip decompressor.
 * <p>
 * Maximum memory consumption is equal to :
 * <ul>
 *    <li>For {@link InputStreamDocumentContent}/{@link LoadedDocumentContent} document : the size of the buffer ({@link #BUFFER_SIZE} bytes)</li>
 *    <li>For {@link OutputStreamDocumentContent} document : total decompressed data size</li>
 * </ul>
 */
// FIXME add optional contentEncoding, contentSize in constructor for gunziped document
public class GzipUnarchiver implements OneToOneDocumentTransformer {

   /** Read/write buffer size. */
   private static final int BUFFER_SIZE = MechanicalSympathy.generalBufferSize();

   /**
    * Checks if specified document represent a gzip archive.
    * The check relies on content type that must match any {@link PresetMimeTypeRegistry#APPLICATION_GZIP}.
    *
    * @param document document to check
    *
    * @return {@code true} if specified document is a zip archive
    */
   @Override
   public boolean supports(Document document) {
      return validate(document, isGzipDocument()).success();
   }

   /**
    * Gunzip specified document.
    *
    * @param document gzip document
    *
    * @return decompressed document
    */
   @Override
   public Document transform(Document document) throws DocumentAccessException {
      validate(document, "document", isGzipDocument()).orThrow();

      if (document.content() instanceof InputStreamDocumentContent
          || document.content() instanceof LoadedDocumentContent) {
         try {
            GZIPInputStream zis = new GZIPInputStream(document.content().inputStreamContent(), BUFFER_SIZE);
            return new DocumentBuilder()
                  .documentId(decompressedDocumentId(document.documentId()))
                  .streamContent(zis)
                  .build();
         } catch (IOException e) {
            throw new DocumentAccessException(e);
         }
      } else if (document.content() instanceof OutputStreamDocumentContent) {
         return new DocumentBuilder()
               .documentId(decompressedDocumentId(document.documentId()))
               .content(OutputStreamDocumentContentBuilder.from(document.content()).sourceEncoding(null)
                              .contentEncoding(null)
                              .contentMapper(GunzipOutputStreamWrapper::new).build()).build();
      } else {
         throw new IllegalStateException(String.format("Unsupported '%s' content type",
                                                       document.content().getClass().getName()));
      }

   }

   /**
    * Generates the decompressed document id from original document by removing extension if present.
    *
    * @param documentId original document id
    *
    * @return decompressed document id
    */
   private DocumentPath decompressedDocumentId(DocumentPath documentId) {
      String gzipDocumentName = documentId.value().toString();

      return DocumentPath.of(FilenameUtils.removeExtension(gzipDocumentName));
   }

   /**
    * Alias for {@link DocumentTransformer#transform(Object)}.
    *
    * @param gzipDocument Gzip document to decompress
    *
    * @return decompressed document
    *
    * @throws DocumentAccessException if an I/O error occurs
    */
   public Document decompress(Document gzipDocument) {
      return transform(gzipDocument);
   }

   protected InvariantRule<Document> isGzipDocument() {
      return metadata(contentType(withStrippedParameters(withStrippedExperimental(isIn(value(list(
            APPLICATION_GZIP)))))));
   }

   public static class GunzipOutputStreamWrapper extends FilterOutputStream {

      private final ByteArrayOutputStream buffer;
      private boolean closed = false;

      public GunzipOutputStreamWrapper(OutputStream delegate) {
         super(validate(delegate, "delegate", isNotNull()).orThrow());
         this.buffer = new ByteArrayOutputStream();
      }

      @Override
      public void write(int b) throws IOException {
         buffer.write(b);
      }

      @Override
      public void write(byte[] b) throws IOException {
         buffer.write(b);
      }

      @Override
      public void write(byte[] b, int off, int len) throws IOException {
         buffer.write(b, off, len);
      }

      @Override
      public void close() throws IOException {
         if (!closed) {
            closed = true;

            byte[] bufferArray = buffer.toByteArray();
            this.buffer.close();

            try (GZIPInputStream zis = new GZIPInputStream(new ByteArrayInputStream(bufferArray),
                                                           BUFFER_SIZE)) {
               IOUtils.copy(zis, out);
            } catch (IOException e) {
               throw new DocumentAccessException(e);
            }

            super.close();
         }
      }
   }

}
