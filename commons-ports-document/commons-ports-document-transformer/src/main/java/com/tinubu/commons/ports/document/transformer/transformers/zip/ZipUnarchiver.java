/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.transformer.transformers.zip;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.EqualsRules.isIn;
import static com.tinubu.commons.ddd2.invariant.rules.MimeTypeRules.withStrippedParameters;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_JAVA_ARCHIVE;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_ZIP;
import static com.tinubu.commons.lang.util.CheckedPredicate.alwaysTrue;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.StreamUtils.findOneOrElseEmpty;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.ports.document.domain.rules.DocumentRules.DocumentMainRules.metadata;
import static com.tinubu.commons.ports.document.domain.rules.DocumentRules.DocumentMetadataRules.contentType;

import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.transformer.DocumentTransformer;
import com.tinubu.commons.ports.document.transformer.DocumentTransformer.OneToManyDocumentTransformer;
import com.tinubu.commons.ports.document.transformer.transformers.archive.Archive;
import com.tinubu.commons.ports.document.transformer.transformers.archive.ArchiveEntry;
import com.tinubu.commons.ports.document.transformer.transformers.archive.ArchiveException;
import com.tinubu.commons.ports.document.transformer.transformers.archive.zip.ZipArchiveFactory;
import com.tinubu.commons.ports.document.transformer.transformers.archive.zip.ZipArchiveMetadata;
import com.tinubu.commons.ports.document.transformer.transformers.archive.zip.ZipConfiguration;

/**
 * ZIP decompressor.
 * <p>
 * Memory consumption is equal to the sum of the size of unarchived entries data.
 */
public class ZipUnarchiver implements OneToManyDocumentTransformer {

   /** Supported mime-types for zip files. */
   private static final List<MimeType> SUPPORTED_MIME_TYPES = list(APPLICATION_ZIP, APPLICATION_JAVA_ARCHIVE);
   private static final ZipArchiveMetadata DEFAULT_ZIP_METADATA = ZipArchiveMetadata.ofDefault();

   /** Provides mandatory ZIP configuration, but it is no use in this decompressor context. */
   private static final ZipConfiguration NOOP_ZIP_CONFIGURATION = new ZipConfiguration();

   private final ZipArchiveMetadata metadata;

   public ZipUnarchiver(ZipArchiveMetadata metadata) {
      this.metadata = validate(metadata, "metadata", isNotNull()).orThrow();
   }

   public ZipUnarchiver() {
      this(DEFAULT_ZIP_METADATA);
   }

   /**
    * Checks if specified document represent a zip archive. The check only relies on content type that are
    * supported.
    *
    * @param document document to check
    *
    * @return {@code true} if specified document is a zip archive
    *
    * @see #SUPPORTED_MIME_TYPES
    */
   @Override
   public boolean supports(Document document) {
      return validate(document, isZipDocument()).success();
   }

   /**
    * Unzip specified document.
    *
    * @param document zip document
    *
    * @return unzipped documents or an empty list
    */
   @Override
   public List<Document> transform(Document document) throws DocumentAccessException {
      validate(document, "document", isZipDocument()).orThrow();

      return extractDocuments(document, alwaysTrue());
   }

   /**
    * Alias for {@link DocumentTransformer#transform(Object)}.
    *
    * @param zipDocument Zip document to uncompress
    *
    * @return uncompressed documents
    *
    * @throws DocumentAccessException if a transformation error occurs
    */
   public List<Document> uncompress(Document zipDocument) {
      return transform(zipDocument);
   }

   protected InvariantRule<Document> isZipDocument() {
      return metadata(contentType(withStrippedParameters(isIn(value(SUPPORTED_MIME_TYPES)))));
   }

   /**
    * Unzip a single entry from archive as a {@link OneToOneDocumentTransformer} adapter.
    * <p>
    * Memory consumption is equal to the size of unarchived entry data.
    *
    * @param entryDocument archive entry identifier to extract
    *
    * @return one to one transformer from the specified archive entry
    *
    * @throws DocumentAccessException if specified entry is not found, or if a transformation error
    *       occurs
    */
   public OneToOneDocumentTransformer asSingleEntryArchive(DocumentPath entryDocument) {
      validate(entryDocument, "entryDocument", isNotNull()).orThrow();

      return new OneToOneDocumentTransformer() {

         @Override
         public boolean supports(Document document) {
            return ZipUnarchiver.this.supports(document);
         }

         @Override
         public Document transform(Document document) throws DocumentAccessException {
            validate(document, "document", isZipDocument()).orThrow();

            return stream(ZipUnarchiver.this.extractDocuments(document,
                                                              path -> entryDocument.value().equals(path)))
                  .findFirst()
                  .orElseThrow(() -> new DocumentAccessException(String.format("'%s' entry not found in '%s'",
                                                                               entryDocument.stringValue(),
                                                                               document
                                                                                     .documentId()
                                                                                     .stringValue())));
         }
      };
   }

   /**
    * Unzip a single entry from archive as a {@link OneToOneDocumentTransformer} adapter.
    * This implementation assumes that there's only and exactly one entry in the archive.
    * <p>
    * Memory consumption is equal to the size of unarchived entry data.
    *
    * @return one to one transformer from the specified archive entry
    *
    * @throws DocumentAccessException if there's not exactly one entry in the archive, or if a
    *       transformation error occurs
    */
   public OneToOneDocumentTransformer asSingleEntryArchive() {
      return new OneToOneDocumentTransformer() {

         @Override
         public boolean supports(Document document) {
            return ZipUnarchiver.this.supports(document);
         }

         @Override
         public Document transform(Document document) throws DocumentAccessException {
            validate(document, "document", isZipDocument()).orThrow();

            return findOneOrElseEmpty(stream(ZipUnarchiver.this.extractDocuments(document,
                                                                                 __ -> true))).orElseThrow(() -> new DocumentAccessException(
                  String.format("Exactly one entry must be present in '%s'",
                                document.documentId().stringValue())));
         }
      };
   }

   protected List<Document> extractDocuments(Document document, Predicate<? super Path> entryFilter) {
      List<Document> documents = new ArrayList<>();

      try (InputStream documentContent = document.content().inputStreamContent()) {
         Archive zipArchive = ZipArchiveFactory.fromContent(documentContent,
                                                            document.content().contentSize().orElse(null),
                                                            metadata,
                                                            NOOP_ZIP_CONFIGURATION);

         try (Stream<ArchiveEntry> zipArchiveEntries = zipArchive.unzipEntries(entryFilter)) {
            for (ArchiveEntry zipArchiveEntry : (Iterable<ArchiveEntry>) zipArchiveEntries::iterator) {
               documents.add(new DocumentBuilder()
                                   .<DocumentBuilder>reconstitute()
                                   .documentId(DocumentPath.of(zipArchiveEntry.name()))
                                   .metadata(new DocumentMetadataBuilder()
                                                   .<DocumentMetadataBuilder>reconstitute()
                                                   .documentPath(zipArchiveEntry.name())
                                                   .contentSize(zipArchiveEntry.contentSize())
                                                   .creationDate(zipArchiveEntry.creationDate())
                                                   .lastUpdateDate(zipArchiveEntry.lastUpdateDate())
                                                   .build())
                                   .streamContent(zipArchiveEntry.content(), zipArchiveEntry.contentSize())
                                   .build());
            }
         }
      } catch (ArchiveException e) {
         throw new DocumentAccessException(e.getMessage(), e);
      } catch (UncheckedIOException e) {
         throw new DocumentAccessException(e);
      } catch (IOException e) {
         throw new DocumentAccessException(e);
      }

      return documents;
   }

}
