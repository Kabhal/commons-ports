/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.transformer.transformers.archive.zip;

import static com.tinubu.commons.ddd2.invariant.rules.ArrayRules.isNotBlankChars;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ports.document.transformer.transformers.archive.ArchiveMetadata;

/**
 * ZIP archive metadata.
 */
public class ZipArchiveMetadata implements ArchiveMetadata {

   /** Default encoding used to encode archive metadata : entry names, comments. */
   private static final Charset DEFAULT_METADATA_ENCODING = StandardCharsets.UTF_8;

   /**
    * Optional archive password. Password must not be empty. Password must be UTF-8 encoded.
    */
   protected final char[] password;

   /**
    * Archive entry name and comments encoding (not the file content encoding).
    */
   protected final Charset metadataEncoding;

   protected ZipArchiveMetadata() {
      this(null, DEFAULT_METADATA_ENCODING);
   }

   protected ZipArchiveMetadata(char[] password, Charset metadataEncoding) {
      this.password = Check.validate(password, "password", isNull().orValue(isNotBlankChars()));
      this.metadataEncoding = Check.notNull(metadataEncoding, "metadataEncoding");
   }

   public static ZipArchiveMetadata ofDefault() {
      return new ZipArchiveMetadata();
   }

   public char[] password() {
      return password;
   }

   public ZipArchiveMetadata password(char[] password) {
      return new ZipArchiveMetadata(password, metadataEncoding);
   }

   @Override
   public Charset metadataEncoding() {
      return metadataEncoding;
   }

   public ZipArchiveMetadata metadataEncoding(Charset metadataEncoding) {
      return new ZipArchiveMetadata(password, metadataEncoding);
   }
}
