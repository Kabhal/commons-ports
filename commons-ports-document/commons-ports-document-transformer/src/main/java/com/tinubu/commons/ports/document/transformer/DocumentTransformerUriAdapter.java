/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.transformer;

import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObject;
import static com.tinubu.commons.ddd2.uri.Uri.compatibleUri;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.nio.file.Path;
import java.util.List;
import java.util.Optional;

import com.tinubu.commons.ddd2.invariant.Validate;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.uri.uris.ArchiveUri;
import com.tinubu.commons.ddd2.uri.uris.ArchiveUri.ArchiveUriRestrictions;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.ReferencedDocument;
import com.tinubu.commons.ports.document.domain.processor.DocumentProcessor;
import com.tinubu.commons.ports.document.domain.uri.DocumentRepositoryUriAdapter;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.ExportUriOptions;
import com.tinubu.commons.ports.document.domain.uri.RepositoryUri;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ZipUnarchiver;

/**
 * {@link DocumentRepositoryUriAdapter} that wraps a delegated adapter, and supports special transformer URIs.
 * Supported URIs are :
 * <ul>
 *    <li>{@code zip URI} : {@code zip:<target-uri>[!/[<archive-entry>]]}</li>
 *    <li>{@code jar URI} : {@code jar:<target-uri>[!/[<archive-entry>]]}</li>
 *    <li>{@code war URI} : {@code war:<target-uri>[!/[<archive-entry>]]}</li>
 *    <li>{@code ear URI} : {@code ear:<target-uri>[!/[<archive-entry>]]}</li>
 * </ul>
 * <p>
 * If an {@code <entry-path>} is specified, the entry path will be extracted from url archive and returned,
 * otherwise, the archive itself is returned as a document.
 * <p>
 * The delegated URI adapter must support the specified URI {@code <url>}.
 */
public class DocumentTransformerUriAdapter implements DocumentRepositoryUriAdapter {

   private static final List<String> JAVA_ARCHIVE_SCHEMES = list("jar", "war", "ear");

   private final DocumentRepositoryUriAdapter uriAdapter;

   public DocumentTransformerUriAdapter(DocumentRepositoryUriAdapter uriAdapter) {
      this.uriAdapter = notNull(uriAdapter, "uriAdapter");
   }

   @Override
   public ReferencedDocument<? extends DocumentRepository> referencedDocumentId(DocumentUri documentUri) {
      return uriAdapter.referencedDocumentId(documentUri);
   }

   @Override
   public RepositoryUri toUri(ExportUriOptions uriOptions) {
      return uriAdapter.toUri(uriOptions);
   }

   @Override
   public DocumentUri toUri(DocumentPath documentId, ExportUriOptions options) {
      return uriAdapter.toUri(documentId, options);
   }

   @Override
   public boolean supportsUri(RepositoryUri uri) {
      Check.notNull(uri, "uri");

      if (uri instanceof DocumentUri) {
         return parseZipUri((DocumentUri) uri)
               .or(() -> parseJavaArchiveUri((DocumentUri) uri))
               .map(archiveUri -> uriAdapter.supportsUri(DocumentUri.ofDocumentUri(archiveUri.targetUri())))
               .orElseGet(() -> uriAdapter.supportsUri(uri));
      } else {
         return uriAdapter.supportsUri(uri);
      }
   }

   @Override
   public Optional<Document> findDocumentByUri(DocumentUri documentUri) {
      Check.notNull(documentUri, "documentUri");
      Validate
            .satisfies(documentUri,
                       "documentUri",
                       this::supportsUri,
                       "'%s' must be supported",
                       validatingObject())
            .orThrowMessage(message -> new InvalidUriException(documentUri).message(message));

      return findTransformedDocument(documentUri, uriAdapter);
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryByUri(DocumentUri documentUri) {
      Check.notNull(documentUri, "documentUri");
      Validate
            .satisfies(documentUri,
                       "documentUri",
                       this::supportsUri,
                       "'%s' must be supported",
                       validatingObject())
            .orThrowMessage(message -> new InvalidUriException(documentUri).message(message));

      return findTransformedDocumentEntry(documentUri, uriAdapter);
   }

   /**
    * Parses a ZIP URI.
    *
    * @param zipUri ZIP URI
    *
    * @return parsed URL and entry parts, or {@link Optional#empty} if specified URI is not a ZIP URI
    */
   public static Optional<ArchiveUri> parseZipUri(DocumentUri zipUri) {
      return compatibleUri(zipUri,
                           ArchiveUriRestrictions.ofDefault().schemes(list("zip")),
                           ArchiveUri::ofUri).optional();
   }

   /**
    * Parses a Java archive (jar, war, ear) URI.
    *
    * @param jarUri Java archive URI
    *
    * @return parsed URL and entry parts, or {@link Optional#empty} if specified URI is not a Java archive URI
    */
   public static Optional<ArchiveUri> parseJavaArchiveUri(DocumentUri jarUri) {
      return compatibleUri(jarUri,
                           ArchiveUriRestrictions.ofDefault().schemes(list(JAVA_ARCHIVE_SCHEMES)),
                           ArchiveUri::ofUri).optional();
   }

   public static Optional<Document> findTransformedDocument(DocumentUri documentUri,
                                                            DocumentRepositoryUriAdapter uriAdapter) {
      return parseZipUri(documentUri)
            .or(() -> parseJavaArchiveUri(documentUri))
            .map(archiveUri -> uriAdapter
                  .findDocumentByUri(DocumentUri.ofDocumentUri(archiveUri.targetUri()))
                  .map(entryDocument -> {
                     return archiveUri.archiveEntry().map(archiveEntry -> {
                        try {
                           return entryDocument.process(unzipEntry(archiveEntry));
                        } finally {
                           entryDocument.content().close();
                        }
                     }).orElse(entryDocument);
                  }))
            .orElseGet(() -> uriAdapter.findDocumentByUri(documentUri));
   }

   public static Optional<DocumentEntry> findTransformedDocumentEntry(DocumentUri documentUri,
                                                                      DocumentRepositoryUriAdapter uriAdapter) {
      return parseZipUri(documentUri)
            .or(() -> parseJavaArchiveUri(documentUri))
            .map(archiveUri -> uriAdapter
                  .findDocumentByUri(DocumentUri.ofDocumentUri(archiveUri.targetUri()))
                  .map(entryDocument -> {
                     try {
                        if (archiveUri.archiveEntry().isPresent()) {
                           entryDocument = entryDocument.process(unzipEntry(archiveUri.archiveEntry().get()));
                        }
                        return entryDocument.documentEntry();
                     } finally {
                        entryDocument.content().close();
                     }
                  }))
            .orElseGet(() -> uriAdapter.findDocumentEntryByUri(documentUri));
   }

   private static DocumentProcessor unzipEntry(Path entryPath) {
      return new ZipUnarchiver().asSingleEntryArchive(DocumentPath.of(entryPath));
   }

}
