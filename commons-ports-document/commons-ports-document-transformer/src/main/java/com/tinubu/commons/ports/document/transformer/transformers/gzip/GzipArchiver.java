/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.transformer.transformers.gzip;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_GZIP;

import java.io.ByteArrayOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.GZIPOutputStream;

import com.tinubu.commons.lang.util.MechanicalSympathy;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent;
import com.tinubu.commons.ports.document.domain.LoadedDocumentContent;
import com.tinubu.commons.ports.document.domain.OutputStreamDocumentContent;
import com.tinubu.commons.ports.document.domain.OutputStreamDocumentContent.OutputStreamDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.processor.common.ContentTypeDocumentRenamer;
import com.tinubu.commons.ports.document.transformer.DocumentTransformer;
import com.tinubu.commons.ports.document.transformer.DocumentTransformer.OneToOneDocumentTransformer;

/**
 * Gzip compressor.
 * <p>
 * Maximum memory consumption is equal to :
 * <ul>
 *    <li>For {@link InputStreamDocumentContent}/{@link LoadedDocumentContent} document : total compressed data size</li>
 *    <li>For {@link OutputStreamDocumentContent} document : the size of the internal buffer</li>
 * </ul>
 */
public class GzipArchiver implements OneToOneDocumentTransformer {

   /** Read/write buffer size. */
   private static final int BUFFER_SIZE = MechanicalSympathy.generalBufferSize();

   /**
    * Supports any document.
    *
    * @param document document to check
    *
    * @return {@code true} if specified document is supported
    */
   @Override
   public boolean supports(Document document) {
      return true;
   }

   /**
    * Gzip specified document.
    *
    * @param document document
    *
    * @return compressed document
    */
   @Override
   public Document transform(Document document) throws DocumentAccessException {
      validate(document, "document", isNotNull()).orThrow();

      if (document.content() instanceof InputStreamDocumentContent
          || document.content() instanceof LoadedDocumentContent) {
         try (ByteArrayOutputStream os = new ByteArrayOutputStream();
              GZIPOutputStream zis = new GZIPOutputStream(os, BUFFER_SIZE)) {
            int count;
            byte[] data = new byte[BUFFER_SIZE];

            try (InputStream src = document.content().inputStreamContent()) {
               while ((count = src.read(data, 0, BUFFER_SIZE)) != -1) {
                  zis.write(data, 0, count);
               }
               zis.finish();

               return new DocumentBuilder()
                     .documentId(document.documentId())
                     .contentType(APPLICATION_GZIP)
                     .streamContent(os.toByteArray())
                     .build().process(new ContentTypeDocumentRenamer(true, true));
            }
         } catch (IOException e) {
            throw new DocumentAccessException(e);
         }
      } else if (document.content() instanceof OutputStreamDocumentContent) {
         return new DocumentBuilder()
               .documentId(document.documentId())
               .contentType(APPLICATION_GZIP)
               .content(OutputStreamDocumentContentBuilder
                              .from(document.content()).contentEncoding(null).sourceEncoding(null)
                              .contentMapper(GzipOutputStreamWrapper::new)
                              .build())
               .build().process(new ContentTypeDocumentRenamer(true, true));
      } else {
         throw new IllegalStateException(String.format("Unsupported '%s' content type",
                                                       document.content().getClass().getName()));
      }
   }

   /**
    * Alias for {@link DocumentTransformer#transform(Object)}.
    *
    * @param document document to compress
    *
    * @return compressed document
    *
    * @throws DocumentAccessException if an I/O error occurs
    */
   public Document compress(Document document) {
      return transform(document);
   }

   public static class GzipOutputStreamWrapper extends FilterOutputStream {

      private final GZIPOutputStream zipOs;
      private boolean closed = false;

      public GzipOutputStreamWrapper(OutputStream delegate) {
         super(validate(delegate, "delegate", isNotNull()).orThrow());
         try {
            this.zipOs = new GZIPOutputStream(out, BUFFER_SIZE);
         } catch (IOException e) {
            throw new DocumentAccessException(e);
         }
      }

      @Override
      public void write(int b) throws IOException {
         zipOs.write(b);
      }

      @Override
      public void write(byte[] b) throws IOException {
         zipOs.write(b);
      }

      @Override
      public void write(byte[] b, int off, int len) throws IOException {
         zipOs.write(b, off, len);
      }

      @Override
      public void flush() throws IOException {
         zipOs.flush();
         super.flush();
      }

      @Override
      public void close() throws IOException {
         if (!closed) {
            closed = true;

            zipOs.close();
            super.close();
         }
      }
   }
}
