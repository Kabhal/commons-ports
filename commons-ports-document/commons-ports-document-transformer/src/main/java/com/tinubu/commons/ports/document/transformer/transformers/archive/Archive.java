/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.transformer.transformers.archive;

import java.io.InputStream;
import java.io.UncheckedIOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Represents a low-level archive.
 */
public interface Archive {

   /**
    * Returns archive raw content.
    *
    * @return archive raw content
    */
   InputStream content();

   /**
    * Returns archive raw content size in bytes.
    *
    * @return archive raw content size
    */
   Optional<Long> contentSize();

   /**
    * Returns archive options.
    *
    * @return archive options
    */
   ArchiveMetadata metadata();

   /**
    * Unzip archive.
    *
    * @return uncompressed zip entries
    *
    * @throws ArchiveException if an error occur in zip library
    * @throws UncheckedIOException if an I/O error occurs
    */
   Stream<ArchiveEntry> unzipEntries();

   /**
    * Unzip archive.
    * <p>
    * You can provide an entry filter to discard some zip entries. It is more performant to use this
    * pre-filter than filtering resulting {@link Stream}.
    *
    * @param entryFilter ZIP archive entry path pre-filter
    *
    * @return uncompressed zip entries
    *
    * @throws ArchiveException if an error occur in zip library
    * @throws UncheckedIOException if an I/O error occurs
    */
   Stream<ArchiveEntry> unzipEntries(Predicate<? super Path> entryFilter);

   /**
    * Adds a new entry to this existing archive. Depending on implementation, the archive can be recreated
    * with new entry.
    *
    * @param entry new entry to add to archive
    *
    * @return updated archive
    *
    * @throws ArchiveException if an error occur in zip library
    * @throws UncheckedIOException if an I/O error occurs
    */
   Archive addEntry(ArchiveEntry entry);

   /**
    * Adds new entries to this existing archive. Depending on implementation, the archive can be recreated
    * with new entries.
    *
    * @param entries new entries to add to archive
    *
    * @return updated archive
    *
    * @throws ArchiveException if an error occur in zip library
    * @throws UncheckedIOException if an I/O error occurs
    */
   Archive addEntries(List<ArchiveEntry> entries);
}
