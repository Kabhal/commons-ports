/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.transformer.transformers.base64;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.processor.DocumentContentProcessor;
import com.tinubu.commons.ports.document.transformer.DocumentTransformer.OneToOneDocumentTransformer;

public abstract class AbstractBase64Transformer
      implements OneToOneDocumentTransformer, DocumentContentProcessor {

   /**
    * MIME chunk size per RFC 2045 section 6.8.
    * <p>
    * The {@value} character limit does not count the trailing CRLF, but counts all other characters,
    * including any equal signs.
    *
    * @see <a href="http://www.ietf.org/rfc/rfc2045.txt">RFC 2045 section 6.8</a>
    */
   protected static final int MIME_CHUNK_SIZE = 76;

   /**
    * PEM chunk size per RFC 1421 section 4.3.2.4.
    * <p>
    * The {@value} character limit does not count the trailing CRLF, but counts all other characters,
    * including any equal signs.
    *
    * @see <a href="http://tools.ietf.org/html/rfc1421">RFC 1421 section 4.3.2.4</a>
    */
   protected static final int PEM_CHUNK_SIZE = 64;

   /**
    * Chunk separator per RFC 2045 section 2.1.
    *
    * @see <a href="http://www.ietf.org/rfc/rfc2045.txt">RFC 2045 section 2.1</a>
    */
   protected static final byte[] CHUNK_SEPARATOR = { '\r', '\n' };

   protected static Charset BASE64_ENCODING = StandardCharsets.UTF_8;

   @Override
   public boolean supports(Document source) {
      return true;
   }

   @Override
   public Document process(Document document) {
      return DocumentContentProcessor.super.process(document);
   }

   @Override
   public Document transform(Document document) throws DocumentAccessException {
      return process(document);
   }

   private static long ceilDiv(long x, long y) {
      return -Math.floorDiv(-x, y);
   }

   /**
    * Base64 encoded resulting length. Padding is assumed to be used.
    *
    * @param contentLength original content length in bytes
    *
    * @return Base64 encoded length in bytes
    */
   private static long encodedLength(long contentLength) {
      long blocks = ceilDiv(contentLength, 3);
      return blocks * 4;
   }

   /**
    * Base64 encoded resulting length, taking line chunking separators into account. Padding is assumed to be
    * used.
    *
    * @param contentLength original content length in bytes
    *
    * @return Base64 encoded length in bytes
    */
   protected static long encodedLength(long contentLength, int maxLineLength, byte[] lineSeparator) {
      long encodedLength = encodedLength(contentLength);

      if (maxLineLength > 0) {
         return encodedLength + ((encodedLength + maxLineLength - 1) / maxLineLength * lineSeparator.length);
      } else {
         return encodedLength;
      }
   }

}
