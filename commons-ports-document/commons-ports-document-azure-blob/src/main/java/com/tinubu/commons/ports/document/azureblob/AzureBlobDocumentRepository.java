/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.azureblob;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotAbsolute;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.lang.util.CollectionUtils.collectionConcat;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.ports.document.azureblob.AzureBlobDocumentRepository.BlobType.APPEND;
import static com.tinubu.commons.ports.document.azureblob.AzureBlobDocumentRepository.BlobType.BLOCK;
import static com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import static com.tinubu.commons.ports.document.domain.DocumentEntry.DocumentEntryBuilder;
import static com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import static com.tinubu.commons.ports.document.domain.InputStreamDocumentContent.InputStreamDocumentContentBuilder;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.DOCUMENT_URI;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.ITERABLE;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.METADATA_ATTRIBUTES;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.METADATA_CONTENT_ENCODING;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.METADATA_CONTENT_SIZE;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.METADATA_CONTENT_TYPE;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.METADATA_CREATION_DATE;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.METADATA_LAST_UPDATE_DATE;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.OPEN;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.QUERYABLE;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.READABLE;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.REPOSITORY_URI;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.WRITABLE;
import static java.nio.charset.StandardCharsets.ISO_8859_1;
import static java.util.stream.Collectors.toList;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.time.OffsetDateTime;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.azure.core.util.Context;
import com.azure.storage.blob.BlobClient;
import com.azure.storage.blob.BlobContainerClient;
import com.azure.storage.blob.BlobServiceClient;
import com.azure.storage.blob.BlobServiceClientBuilder;
import com.azure.storage.blob.models.AppendBlobRequestConditions;
import com.azure.storage.blob.models.BlobHttpHeaders;
import com.azure.storage.blob.models.BlobItem;
import com.azure.storage.blob.models.BlobProperties;
import com.azure.storage.blob.models.BlobRequestConditions;
import com.azure.storage.blob.models.BlobStorageException;
import com.azure.storage.blob.models.ParallelTransferOptions;
import com.azure.storage.blob.options.AppendBlobCreateOptions;
import com.azure.storage.blob.options.BlobParallelUploadOptions;
import com.azure.storage.blob.options.BlockBlobOutputStreamOptions;
import com.azure.storage.blob.specialized.AppendBlobClient;
import com.azure.storage.blob.specialized.BlobClientBase;
import com.azure.storage.blob.specialized.BlobOutputStream;
import com.azure.storage.blob.specialized.BlockBlobAsyncClient;
import com.azure.storage.blob.specialized.BlockBlobClient;
import com.azure.storage.common.StorageSharedKeyCredential;
import com.azure.storage.common.implementation.Constants;
import com.tinubu.commons.ddd2.domain.event.RegistrableDomainEventService;
import com.tinubu.commons.ddd2.domain.event.SynchronousDomainEventService;
import com.tinubu.commons.ddd2.domain.repository.Repository;
import com.tinubu.commons.ddd2.domain.specification.Specification;
import com.tinubu.commons.lang.mapper.EnumMapper;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.lang.mimetype.MimeTypeFactory;
import com.tinubu.commons.lang.util.Pair;
import com.tinubu.commons.lang.validation.CheckReturnValue;
import com.tinubu.commons.ports.document.azureblob.AzureBlobDocumentConfig.AzureBlobDocumentConfigBuilder;
import com.tinubu.commons.ports.document.domain.AbstractDocumentRepository;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentEntrySpecification;
import com.tinubu.commons.ports.document.domain.DocumentMetadata;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata;
import com.tinubu.commons.ports.document.domain.OutputStreamDocumentContent;
import com.tinubu.commons.ports.document.domain.OutputStreamDocumentContent.OutputStreamDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.ReferencedDocument;
import com.tinubu.commons.ports.document.domain.capability.RepositoryCapability;
import com.tinubu.commons.ports.document.domain.capability.UnsupportedCapabilityException;
import com.tinubu.commons.ports.document.domain.uri.DocumentRepositoryUriAdapter;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.ExportUriOptions;
import com.tinubu.commons.ports.document.domain.uri.RepositoryUri;
import com.tinubu.commons.ports.document.transformer.DocumentTransformerUriAdapter;

/**
 * Azure Blob {@link DocumentRepository} adapter implementation.
 * <p>
 * If configured container is missing, the container will be created only at write time, or the write
 * operation will fail if repository configuration does not instruct to do so.
 * <p>
 * Limitations :
 * <ul>
 *    <li>the following Metadata are not persisted: documentPath</li>
 *    <li>"append mode" is only not supported in {@link #openDocument} for append blob types</li>
 * </ul>
 *
 * @implSpec Immutable class implementation
 */
public class AzureBlobDocumentRepository extends AbstractDocumentRepository<AzureBlobDocumentRepository> {
   private static final Logger log = LoggerFactory.getLogger(AzureBlobDocumentRepository.class);

   /**
    * Document attribute used to force the Blob type on Azure storage when creating new documents with
    * {@link #openDocument(DocumentPath, boolean, boolean, OpenDocumentMetadata)} or
    * {@link #saveDocument(Document, boolean)}.
    * Supported attribute values are {@link BlobType#attributeValue()}.
    *
    * @see <a
    *       href="https://learn.microsoft.com/fr-fr/rest/api/storageservices/understanding-block-blobs--append-blobs--and-page-blobs">Azure
    *       blob types</a>
    */
   private static final String BLOB_TYPE_ATTRIBUTE = "azure.blob.type";

   /**
    * Azure block (chunk) size to transfer at a time (max supported is
    * {@value BlockBlobAsyncClient#MAX_STAGE_BLOCK_BYTES_LONG}) in bytes.
    * Memory consumption for an upload operation can be up to {@value #BLOCK_SIZE} * {@value MAX_CONCURRENCY}.
    */
   private static final long BLOCK_SIZE = 2L * 1024 * 1024;

   /**
    * The maximum number of workers that will upload chunks concurrently for a single upload operation. Note
    * that there's no shared thread pools between operations, so that saving multiple documents in concurrency
    * can lead to an indefinite number of working threads.
    */
   private static final int MAX_CONCURRENCY = 5;
   private static final HashSet<RepositoryCapability> CAPABILITIES = collectionConcat(HashSet::new,
                                                                                      list(WRITABLE,
                                                                                           READABLE,
                                                                                           ITERABLE,
                                                                                           QUERYABLE,
                                                                                           OPEN),
                                                                                      list(METADATA_CREATION_DATE,
                                                                                           METADATA_LAST_UPDATE_DATE,
                                                                                           METADATA_CONTENT_TYPE,
                                                                                           METADATA_CONTENT_ENCODING,
                                                                                           METADATA_CONTENT_SIZE,
                                                                                           METADATA_ATTRIBUTES));
   private static final Encoder BASE64_ENCODER = Base64.getEncoder();
   private static final Decoder BASE64_DECODER = Base64.getDecoder();

   private final AzureBlobDocumentConfig azureBlobDocumentConfig;
   private final Consumer<BlobServiceClientBuilder> clientConfigurer;
   private final BlobContainerClient blobContainerClient;
   /** Hash of a set of server connection parameters identifying a similar repository. */
   private final int sameRepositoryHash;
   private final DocumentRepositoryUriAdapter uriAdapter;
   /** Default {@link BlobType} when {@link #BLOB_TYPE_ATTRIBUTE} is not set in document metadata. */
   private final BlobType defaultBlobType;
   /**
    * Optional base path, relative to container, or empty path if unset.
    */
   private final Path containerBasePath;

   private AzureBlobDocumentRepository(AzureBlobDocumentConfig azureBlobDocumentConfig,
                                       RegistrableDomainEventService eventService,
                                       Consumer<BlobServiceClientBuilder> clientConfigurer,
                                       BlobContainerClient blobContainerClient) {
      super(eventService);

      this.azureBlobDocumentConfig =
            validate(azureBlobDocumentConfig, "azureBlobDocumentConfig", isNotNull()).orThrow();

      this.clientConfigurer = validate(clientConfigurer, "clientConfigurer", isNotNull()).orThrow();
      this.blobContainerClient = nullable(blobContainerClient,
                                          blobContainerClient(blobServiceClient(azureBlobDocumentConfig,
                                                                                clientConfigurer),
                                                              azureBlobDocumentConfig));
      this.sameRepositoryHash = sameRepositoryHash(azureBlobDocumentConfig);
      this.uriAdapter = new AzureBlobUriAdapter();
      this.defaultBlobType = azureBlobDocumentConfig.defaultBlobType();
      this.containerBasePath = azureBlobDocumentConfig.containerBasePath();
   }

   public AzureBlobDocumentRepository(AzureBlobDocumentConfig azureBlobDocumentConfig,
                                      RegistrableDomainEventService eventService) {
      this(azureBlobDocumentConfig, eventService, __ -> { }, null);
   }

   public AzureBlobDocumentRepository(AzureBlobDocumentConfig azureBlobDocumentConfig) {
      this(azureBlobDocumentConfig, new SynchronousDomainEventService());
   }

   public AzureBlobDocumentRepository(AzureBlobDocumentConfig azureBlobDocumentConfig,
                                      RegistrableDomainEventService eventService,
                                      Consumer<BlobServiceClientBuilder> clientConfigurer) {
      this(azureBlobDocumentConfig, eventService, clientConfigurer, null);
   }

   public AzureBlobDocumentRepository(AzureBlobDocumentConfig azureBlobDocumentConfig,
                                      Consumer<BlobServiceClientBuilder> clientConfigurer) {
      this(azureBlobDocumentConfig, new SynchronousDomainEventService(), clientConfigurer);
   }

   @Override
   public HashSet<RepositoryCapability> capabilities() {
      return CAPABILITIES;
   }

   @Override
   public boolean sameRepositoryAs(Repository<Document, DocumentPath> documentRepository) {
      validate(documentRepository, "documentRepository", isNotNull()).orThrow();

      return documentRepository instanceof AzureBlobDocumentRepository
             && Objects.equals(((AzureBlobDocumentRepository) documentRepository).sameRepositoryHash,
                               sameRepositoryHash);
   }

   @Override
   public AzureBlobDocumentRepository subPath(Path subPath, boolean shareContext) {
      validate(subPath, "subPath", isNotAbsolute().andValue(hasNoTraversal())).orThrow();

      AzureBlobDocumentConfig subPathConfig = AzureBlobDocumentConfigBuilder
            .from(azureBlobDocumentConfig)
            .containerBasePath(azureBlobDocumentConfig.containerBasePath().resolve(subPath))
            .build();

      if (shareContext) {
         return new AzureBlobDocumentRepository(subPathConfig,
                                                eventService,
                                                clientConfigurer,
                                                blobContainerClient);
      } else {
         return new AzureBlobDocumentRepository(subPathConfig, eventService, clientConfigurer);
      }
   }

   @Override
   @CheckReturnValue
   public Optional<Document> openDocument(DocumentPath documentId,
                                          boolean overwrite,
                                          boolean append,
                                          OpenDocumentMetadata metadata) {
      validate(documentId, "documentId", isNotNull())
            .and(validate(metadata, "metadata", isNotNull()))
            .orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEvent(() -> {
         Path blobFile = blobFile(documentId);

         Document document = new DocumentBuilder()
               .documentId(documentId)
               .content(new OutputStreamDocumentContentBuilder()
                              .content(new ByteArrayOutputStream(), metadata.contentEncoding().orElse(null))
                              .build())
               .chain(metadata.chainDocumentBuilder())
               .build();

         try {
            BlobClient blobClient = blobClient(blobFile);

            BlobType blobType = blobType(blobClient)
                  .or(() -> blobType(document.metadata()))
                  .orElse(append ? APPEND : defaultBlobType);

            BlobOutputStream blobOutputStream;
            switch (blobType) {
               case APPEND:
                  AppendBlobClient appendBlobClient = blobClient.getAppendBlobClient();

                  if (!append || !appendBlobClient.exists()) {
                     appendBlobClient.createWithResponse(appendBlobCreateOptions(document.metadata(),
                                                                                 overwrite),
                                                         null,
                                                         Context.NONE);
                  }

                  blobOutputStream =
                        appendBlobClient.getBlobOutputStream(appendBlobRequestConditions(overwrite));

                  break;

               case BLOCK:
                  if (append) {
                     throw new IllegalArgumentException(String.format(
                           "Append mode not supported for '%s' blobs",
                           blobType));
                  }

                  BlockBlobClient blockBlobClient = blobClient.getBlockBlobClient();

                  if (!overwrite && blockBlobClient.exists()) {
                     return optional();
                  }

                  blobOutputStream =
                        blockBlobClient.getBlobOutputStream(blockBlobOutputStreamOptions(document.metadata(),
                                                                                         overwrite));
                  break;
               default:
                  throw new IllegalStateException("Unknown Blob type");
            }

            return optional(DocumentBuilder
                                  .from(document)
                                  .content(new OutputStreamDocumentContentBuilder()
                                                 .content(new BufferedOutputStream(blobOutputStream),
                                                          metadata.contentEncoding().orElse(null))
                                                 .build())
                                  .build());
         } catch (UncheckedIOException e) {
            throw new DocumentAccessException(e);
         } catch (BlobStorageException e) {
            if (blobAlreadyExists(e)) {
               return optional();
            } else {
               throw new DocumentAccessException(e.getMessage(), e);
            }
         }
      }, d -> documentSaved(d.documentEntry(), watch));
   }

   @Override
   public Optional<Document> findDocumentById(DocumentPath documentId) {
      validate(documentId, "documentId", isNotNull()).orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEvent(() -> {
         Path blobFile = blobFile(documentId);

         return azureBlobDocumentEntry(blobFile, documentId).flatMap(entry -> azureBlobDocumentContent(
               blobFile).map(content -> new DocumentBuilder()
               .<DocumentBuilder>reconstitute()
               .documentEntry(entry)
               .content(content)
               .build()));
      }, d -> documentAccessed(d.documentEntry(), watch));
   }

   @Override
   public Stream<Document> findDocumentsBySpecification(Path basePath,
                                                        Specification<DocumentEntry> specification) {
      return autoCloseDocumentsOnStreamClose(findDocumentEntriesBySpecification(basePath,
                                                                                specification).flatMap(entry -> stream(
            azureBlobDocumentContent(blobFile(entry.documentId())).map(content -> new DocumentBuilder()
                  .<DocumentBuilder>reconstitute()
                  .documentEntry(entry)
                  .content(content)
                  .build()))));
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryById(DocumentPath documentId) {
      validate(documentId, "documentId", isNotNull()).orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEntryEvent(() -> {
         Path blobFile = blobFile(documentId);

         return azureBlobDocumentEntry(blobFile, documentId);
      }, d -> documentAccessed(d, watch));
   }

   @Override
   public Stream<DocumentEntry> findDocumentEntriesBySpecification(Path basePath,
                                                                   Specification<DocumentEntry> specification) {
      validate(basePath, "basePath", isNotAbsolute().andValue(hasNoTraversal()))
            .and(validate(specification, "specification", isNotNull()))
            .orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEntryStreamEvent(() -> listDocumentEntries(containerBasePath.resolve(basePath),
                                                                      directoryFilter(specification)).filter(
            specification::satisfiedBy), d -> documentAccessed(d, watch));
   }

   @Override
   @CheckReturnValue
   public Optional<DocumentEntry> saveDocument(Document document, boolean overwrite) {
      validate(document, "document", isNotNull()).orThrow();

      if (document.content() instanceof OutputStreamDocumentContent) {
         throw new IllegalArgumentException(
               "Unsupported operation for this document content implementation : " + document
                     .content()
                     .getClass()
                     .getName());
      }

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEntryEvent(() -> rawSaveDocument(document, overwrite),
                                      d -> documentSaved(d, watch));
   }

   @Override
   @CheckReturnValue
   public Optional<Document> saveAndReturnDocument(Document document, boolean overwrite) {
      validate(document, "document", isNotNull()).orThrow();

      if (document.content() instanceof OutputStreamDocumentContent) {
         throw new IllegalArgumentException(
               "Unsupported operation for this document content implementation : " + document
                     .content()
                     .getClass()
                     .getName());
      }

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEvent(() -> {
         Path blobFile = blobFile(document.documentId());

         return rawSaveDocument(document, overwrite).flatMap(entry -> azureBlobDocumentContent(blobFile).map(
               content -> new DocumentBuilder()
                     .<DocumentBuilder>reconstitute()
                     .documentEntry(entry)
                     .content(content)
                     .build()));
      }, d -> documentSaved(d.documentEntry(), watch));
   }

   private Optional<DocumentEntry> rawSaveDocument(Document document, boolean overwrite) {
      Path blobFile = blobFile(document.documentId());

      long contentLength = document
            .content()
            .contentSize()
            .orElseThrow(() -> new IllegalStateException(String.format(
                  "Unknown '%s' document content size. Content size is required to upload content to Azure blob",
                  document.documentId().stringValue())));

      Optional<DocumentEntry> savedDocument;
      try (InputStream inputStream = new BufferedInputStream(document.content().inputStreamContent())) {

         switch (blobType(document.metadata()).orElse(defaultBlobType)) {
            case APPEND:
               AppendBlobClient appendBlobClient = appendBlobClient(blobFile);

               appendBlobClient.createWithResponse(appendBlobCreateOptions(document.metadata(), overwrite),
                                                   null,
                                                   Context.NONE);
               appendBlobClient.appendBlock(inputStream, contentLength);

               savedDocument = azureBlobDocumentEntry(appendBlobClient, document.documentId());
               break;
            case BLOCK:
               BlobClient blobClient = blobClient(blobFile);

               blobClient
                     .uploadWithResponse(blockBlobParallelUploadOptions(document.metadata(),
                                                                        inputStream,
                                                                        overwrite), null, Context.NONE)
                     .getValue();

               savedDocument = azureBlobDocumentEntry(blobClient, document.documentId());
               break;
            default:
               throw new IllegalStateException("Unknown Blob type");
         }
      } catch (IOException | UncheckedIOException e) {
         throw new DocumentAccessException(e.getMessage(), e);
      } catch (BlobStorageException e) {
         if (blobAlreadyExists(e)) {
            savedDocument = optional();
         } else {
            throw new DocumentAccessException(e.getMessage(), e);
         }
      }

      return savedDocument;
   }

   @Override
   @CheckReturnValue
   public Optional<DocumentEntry> deleteDocumentById(DocumentPath documentId) {
      validate(documentId, "documentId", isNotNull()).orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEntryEvent(() -> {

         Path blobFile = blobFile(documentId);
         BlobClient blobClient = blobClient(blobFile);
         Optional<DocumentEntry> deletedDocument = azureBlobDocumentEntry(blobFile, documentId);

         if (azureBlobDocumentEntry(blobFile, documentId).isPresent()) {

            try {
               blobClient.delete();
            } catch (BlobStorageException e) {
               if (blobNotFound(e)) {
                  deletedDocument = optional();
               } else {
                  throw new DocumentAccessException(e.getMessage(), e);
               }
            }
         }

         return deletedDocument;
      }, d -> documentDeleted(d, watch));
   }

   @Override
   @CheckReturnValue
   public List<DocumentEntry> deleteDocumentsBySpecification(Path basePath,
                                                             Specification<DocumentEntry> specification) {
      validate(basePath, "basePath", isNotAbsolute().andValue(hasNoTraversal()))
            .and(validate(specification, "specification", isNotNull()))
            .orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEntriesEvent(() -> list(listDocumentEntries(containerBasePath.resolve(basePath),
                                                                       directoryFilter(specification))
                                                         .filter(specification::satisfiedBy)
                                                         .filter(documentEntry -> {
                                                            Path blobFile =
                                                                  blobFile(documentEntry.documentId());
                                                            BlobClient blobClient = blobClient(blobFile);

                                                            try {
                                                               blobClient.delete();
                                                            } catch (BlobStorageException e) {
                                                               if (blobNotFound(e)) {
                                                                  return false;
                                                               } else {
                                                                  throw new DocumentAccessException(e.getMessage(),
                                                                                                    e);
                                                               }
                                                            }

                                                            return true;
                                                         })), d -> documentDeleted(d, watch));
   }

   @Override
   public ReferencedDocument<? extends DocumentRepository> referencedDocumentId(DocumentUri documentUri) {
      return uriAdapter.referencedDocumentId(documentUri);
   }

   @Override
   public boolean supportsUri(RepositoryUri uri) {
      return uriAdapter.supportsUri(uri);
   }

   @Override
   public RepositoryUri toUri(ExportUriOptions options) {
      return uriAdapter.toUri(options);
   }

   @Override
   public DocumentUri toUri(DocumentPath documentId, ExportUriOptions options) {
      return uriAdapter.toUri(documentId, options);
   }

   @Override
   public Optional<Document> findDocumentByUri(DocumentUri documentUri) {
      return uriAdapter.findDocumentByUri(documentUri);
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryByUri(DocumentUri documentUri) {
      return uriAdapter.findDocumentEntryByUri(documentUri);
   }

   /**
    * Real Azure Blob URI adapter to be used by {@link DocumentTransformerUriAdapter}.
    */
   public class AzureBlobUriAdapter implements DocumentRepositoryUriAdapter {

      @Override
      public ReferencedDocument<? extends DocumentRepository> referencedDocumentId(DocumentUri documentUri) {
         throw new UnsupportedCapabilityException(DOCUMENT_URI);
      }

      @Override
      public boolean supportsUri(RepositoryUri uri) {
         return false;
      }

      @Override
      public RepositoryUri toUri(ExportUriOptions options) {
         throw new UnsupportedCapabilityException(REPOSITORY_URI);
      }

      @Override
      public DocumentUri toUri(DocumentPath documentId, ExportUriOptions options) {
         throw new UnsupportedCapabilityException(DOCUMENT_URI);
      }

      @Override
      public Optional<Document> findDocumentByUri(DocumentUri documentUri) {
         throw new UnsupportedCapabilityException(DOCUMENT_URI);
      }

      @Override
      public Optional<DocumentEntry> findDocumentEntryByUri(DocumentUri documentUri) {
         throw new UnsupportedCapabilityException(DOCUMENT_URI);
      }

   }

   public enum BlobType {
      BLOCK("block"), APPEND("append");

      private static final EnumMapper<BlobType, String> mapper =
            new EnumMapper<>(BlobType.class, BlobType::attributeValue);

      private final String attributeValue;

      BlobType(String attributeValue) {
         this.attributeValue = attributeValue;
      }

      public String attributeValue() {
         return attributeValue;
      }

      public static BlobType ofAttributeValue(String attributeValue) {
         validate(attributeValue, "attributeValue", isNotBlank()).orThrow();

         return mapper.map(attributeValue.toLowerCase());
      }
   }

   /** Generates a hash unique for a given server storage path to identify a similar repository. */
   private int sameRepositoryHash(AzureBlobDocumentConfig azureBlobDocumentConfig) {
      return Objects.hash(azureBlobDocumentConfig.endpoint(),
                          azureBlobDocumentConfig.connectionString(),
                          azureBlobDocumentConfig.containerName(),
                          azureBlobDocumentConfig.containerBasePath());
   }

   private Predicate<Path> directoryFilter(Specification<DocumentEntry> specification) {
      if (specification instanceof DocumentEntrySpecification) {
         DocumentEntrySpecification documentEntrySpecification = (DocumentEntrySpecification) specification;
         return documentEntrySpecification::satisfiedBySubPath;
      } else {
         return __ -> true;
      }
   }

   private Map<String, String> metadata(DocumentMetadata documentMetadata) {
      return map(documentMetadata
                       .attributes()
                       .entrySet()
                       .stream()
                       .map(e -> Pair
                             .of(e.getKey(), e.getValue())
                             .map(AzureBlobDocumentRepository::encodeMetadata,
                                  AzureBlobDocumentRepository::encodeMetadata)));
   }

   private Map<String, String> encodeMetadata(Map<String, String> metadata) {
      return map(metadata
                       .entrySet()
                       .stream()
                       .map(e -> Pair.of(encodeMetadata(e.getKey()), encodeMetadata(e.getValue()))));
   }

   /**
    * @implSpec Azure does not support {@code =} characters for attributes, Base64 encoded attributes
    *       must have ending {@code =} stripped, as they are no use.
    */
   private static String encodeMetadata(String metadata) {
      return StringUtils.stripEnd(new String(BASE64_ENCODER.encode(metadata.getBytes(ISO_8859_1)),
                                             ISO_8859_1), "=");
   }

   private Map<String, String> decodeMetadata(Map<String, String> metadata) {
      return map(metadata
                       .entrySet()
                       .stream()
                       .map(e -> Pair.of(decodeMetadata(e.getKey()), decodeMetadata(e.getValue()))));
   }

   private static String decodeMetadata(String metadata) {
      return new String(BASE64_DECODER.decode(metadata.getBytes(ISO_8859_1)), ISO_8859_1);
   }

   private BlockBlobOutputStreamOptions blockBlobOutputStreamOptions(DocumentMetadata documentMetadata,
                                                                     boolean overwrite) {
      BlobHttpHeaders headers = blobHttpHeaders(documentMetadata);
      BlobRequestConditions blobRequestConditions = blobRequestConditions(overwrite);

      return new BlockBlobOutputStreamOptions()
            .setParallelTransferOptions(parallelTransferOptions())
            .setHeaders(headers)
            .setMetadata(metadata(documentMetadata))
            .setRequestConditions(blobRequestConditions);
   }

   private BlobParallelUploadOptions blockBlobParallelUploadOptions(DocumentMetadata documentMetadata,
                                                                    InputStream inputStream,
                                                                    boolean overwrite) {
      BlobHttpHeaders headers = blobHttpHeaders(documentMetadata);
      BlobRequestConditions blobRequestConditions = blobRequestConditions(overwrite);

      return new BlobParallelUploadOptions(inputStream)
            .setParallelTransferOptions(parallelTransferOptions())
            .setHeaders(headers)
            .setMetadata(metadata(documentMetadata))
            .setRequestConditions(blobRequestConditions);
   }

   /**
    * Configuration used to parallelize data transfer operations.
    *
    * @return parallel transfer options configuration
    */
   private ParallelTransferOptions parallelTransferOptions() {
      return new ParallelTransferOptions().setBlockSizeLong(BLOCK_SIZE).setMaxConcurrency(MAX_CONCURRENCY);
   }

   private AppendBlobCreateOptions appendBlobCreateOptions(DocumentMetadata documentMetadata,
                                                           boolean overwrite) {
      BlobHttpHeaders headers = blobHttpHeaders(documentMetadata);
      BlobRequestConditions blobRequestConditions = blobRequestConditions(overwrite);

      return new AppendBlobCreateOptions()
            .setMetadata(metadata(documentMetadata))
            .setHeaders(headers)
            .setRequestConditions(blobRequestConditions);
   }

   private BlobRequestConditions blobRequestConditions(boolean overwrite) {
      BlobRequestConditions blobRequestConditions = new BlobRequestConditions();
      if (!overwrite) {
         blobRequestConditions.setIfNoneMatch(Constants.HeaderConstants.ETAG_WILDCARD);
      }
      return blobRequestConditions;
   }

   private AppendBlobRequestConditions appendBlobRequestConditions(boolean overwrite) {
      AppendBlobRequestConditions blobRequestConditions = new AppendBlobRequestConditions();
      if (!overwrite) {
         blobRequestConditions.setIfNoneMatch(Constants.HeaderConstants.ETAG_WILDCARD);
      }
      return blobRequestConditions;
   }

   private BlobHttpHeaders blobHttpHeaders(DocumentMetadata documentMetadata) {
      return new BlobHttpHeaders()
            .setContentType(documentMetadata.contentType().map(MimeType::toString).orElse(null))
            .setContentEncoding(documentMetadata.contentEncoding().map(Charset::name).orElse(null));
   }

   /**
    * List document entries from specified base directory. Base directory represents a physical path in the
    * container. It can be relative or absolute, empty or not.
    *
    * @param baseDirectory base directory to search from
    * @param directoryFilter user-space directory filter.Use {@code __ -> false} to disable recursive
    *       search
    *
    * @return filtered document entries
    */
   private Stream<DocumentEntry> listDocumentEntries(Path baseDirectory, Predicate<Path> directoryFilter) {
      notNull(baseDirectory, "baseDirectory");
      notNull(directoryFilter, "directoryFilter");

      String azureBaseDirectory = baseDirectory.toString().isEmpty() ? "" : baseDirectory + "/";

      List<BlobItem> blobItems =
            blobContainerClient.listBlobsByHierarchy(azureBaseDirectory).stream().collect(toList());

      return blobItems.stream().flatMap(blobItem -> {
         Path blobPath = Path.of(blobItem.getName());

         if (isBlobDir(blobItem)) {
            if (directoryFilter.test(blobPath)) {
               return listDocumentEntries(blobPath, directoryFilter);
            } else {
               return stream();
            }
         } else {
            return stream(azureBlobDocumentEntry(Path.of(blobItem.getName()), null));
         }
      });

   }

   private boolean blobAlreadyExists(BlobStorageException e) {
      return e.getErrorCode().toString().equals("BlobAlreadyExists");
   }

   private boolean blobNotFound(BlobStorageException e) {
      return e.getErrorCode().toString().equals("BlobNotFound");
   }

   /**
    * Low-level document entry generation from Azure Blob file.
    *
    * @param blobFile Azure Blob file
    * @param documentId optional document identifier, when known, to optimize reverse document id
    *       mapping
    *
    * @return document entry or {@link Optional#empty} if blob file not found
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   private Optional<DocumentEntry> azureBlobDocumentEntry(Path blobFile, DocumentPath documentId) {
      return azureBlobMetadata(blobClient(blobFile)).map(metadata -> new DocumentEntryBuilder()
            .<DocumentEntryBuilder>reconstitute()
            .documentId(documentId != null ? documentId : documentId(blobFile))
            .metadata(metadata)
            .build());
   }

   private Optional<DocumentEntry> azureBlobDocumentEntry(BlobClientBase blobClient,
                                                          DocumentPath documentId) {
      return azureBlobMetadata(blobClient).map(metadata -> new DocumentEntryBuilder()
            .<DocumentEntryBuilder>reconstitute()
            .documentId(documentId != null ? documentId : documentId(blobClient))
            .metadata(metadata)
            .build());
   }

   /**
    * Low-level document content generation from Azure Blob file.
    *
    * @param blobFile Azure Blob file
    *
    * @return document content or {@link Optional#empty} if blob file not found
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   private Optional<InputStreamDocumentContent> azureBlobDocumentContent(Path blobFile) {
      try {
         BlobClient blobClient = blobClient(blobFile);
         InputStream dataStream = blobClient.downloadContent().toStream();

         return optional(new InputStreamDocumentContentBuilder()
                               .<InputStreamDocumentContentBuilder>reconstitute()
                               .content(dataStream,
                                        nullable(blobClient.getProperties().getContentEncoding())
                                              .map(Charset::forName)
                                              .orElse(null),
                                        blobClient.getProperties().getBlobSize())
                               .build());
      } catch (BlobStorageException e) {
         if (blobNotFound(e)) {
            return optional();
         } else {
            throw new DocumentAccessException(e.getMessage(), e);
         }
      }

   }

   /**
    * Low-level document metadata generation from Azure Blob file.
    *
    * @param blobClient Azure Blob client
    *
    * @return document metadata or {@link Optional#empty} if blob file not found
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   private Optional<DocumentMetadata> azureBlobMetadata(BlobClientBase blobClient) {
      try {
         BlobProperties properties = blobClient.getProperties();

         return optional(new DocumentMetadataBuilder()
                               .<DocumentMetadataBuilder>reconstitute()
                               .documentPath(documentId(blobClient).value())
                               .contentSize(properties.getBlobSize())
                               .contentType(nullable(properties.getContentType())
                                                  .map(MimeTypeFactory::parseMimeType)
                                                  .orElse(null),
                                            nullable(properties.getContentEncoding())
                                                  .map(Charset::forName)
                                                  .orElse(null))
                               .attributes(decodeMetadata(map(properties.getMetadata())))
                               .<DocumentMetadataBuilder, OffsetDateTime>optionalChain(nullable(properties.getCreationTime()),
                                                                                       (b, creationDate) -> b.creationDate(
                                                                                             creationDate.toInstant()))
                               .<DocumentMetadataBuilder, OffsetDateTime>optionalChain(nullable(properties.getLastModified()),
                                                                                       (b, lastUpdateDate) -> b.lastUpdateDate(
                                                                                             lastUpdateDate.toInstant()))
                               .build());
      } catch (BlobStorageException e) {
         if (blobNotFound(e)) {
            return optional();
         } else {
            throw new DocumentAccessException(e.getMessage(), e);
         }
      }
   }

   private Optional<BlobType> blobType(DocumentMetadata documentMetadata) {
      return documentMetadata.attribute(BLOB_TYPE_ATTRIBUTE).map(BlobType::ofAttributeValue);
   }

   private Optional<BlobType> blobType(BlobClientBase blobClient) {
      try {
         return optional(blobType(blobClient.getProperties().getBlobType()));
      } catch (UncheckedIOException e) {
         throw new DocumentAccessException(e);
      } catch (BlobStorageException e) {
         if (blobNotFound(e)) {
            return optional();
         } else {
            throw new DocumentAccessException(e.getMessage(), e);
         }
      }
   }

   private BlobType blobType(com.azure.storage.blob.models.BlobType azureBlobType) {
      BlobType blobType;

      switch (azureBlobType) {
         case APPEND_BLOB:
            blobType = APPEND;
            break;
         case BLOCK_BLOB:
            blobType = BLOCK;
            break;
         default:
            throw new IllegalStateException(String.format("Unsupported '%s' Blob type", azureBlobType));
      }

      return blobType;
   }

   private BlockBlobClient blockBlobClient(Path blobFile) {
      return blobContainerClient.getBlobClient(blobFile.toString()).getBlockBlobClient();
   }

   private BlobClient blobClient(Path blobFile) {
      return blobContainerClient.getBlobClient(blobFile.toString());
   }

   private AppendBlobClient appendBlobClient(Path blobFile) {
      return blobContainerClient.getBlobClient(blobFile.toString()).getAppendBlobClient();
   }

   private boolean isBlobDir(BlobItem blobItem) {
      return Boolean.TRUE.equals(blobItem.isPrefix());
   }

   /**
    * Generates real Azure Blob path for specified document.
    *
    * @param documentId document id
    *
    * @return real Azure Blob path
    */
   public Path blobFile(DocumentPath documentId) {
      return containerBasePath.resolve(documentId.value());
   }

   /**
    * Returns document identifier from Azure Blob file. Reverses real path to generate a
    * logical document path.
    *
    * @param blobFile Azure Blob file
    *
    * @return document identifier from Azure Blob file
    */
   private DocumentPath documentId(Path blobFile) {
      return DocumentPath.of(containerBasePath.relativize(blobFile));
   }

   private DocumentPath documentId(BlobClientBase blobClient) {
      return documentId(Path.of(blobClient.getBlobName()));
   }

   /**
    * Returns the authentication to connect to Azure Blob Service.
    *
    * @param azureBlobDocumentConfig adapter configuration
    *
    * @return Azure authentication
    */
   private static StorageSharedKeyCredential storageCredential(AzureBlobDocumentConfig azureBlobDocumentConfig) {
      return new StorageSharedKeyCredential(azureBlobDocumentConfig.authentication().storageAccountName(),
                                            azureBlobDocumentConfig.authentication().storageAccountKey());
   }

   private static BlobServiceClient blobServiceClient(AzureBlobDocumentConfig azureBlobDocumentConfig,
                                                      Consumer<BlobServiceClientBuilder> clientConfigurer) {
      BlobServiceClientBuilder blobServiceClientBuilder = new BlobServiceClientBuilder();

      if (!StringUtils.isBlank(azureBlobDocumentConfig.connectionString())) {
         blobServiceClientBuilder.connectionString(azureBlobDocumentConfig.connectionString());
      } else {
         blobServiceClientBuilder.endpoint(azureBlobDocumentConfig.endpoint());
         blobServiceClientBuilder.credential(storageCredential(azureBlobDocumentConfig)).buildClient();
      }

      clientConfigurer.accept(blobServiceClientBuilder);

      return blobServiceClientBuilder.buildClient();
   }

   private static BlobContainerClient blobContainerClient(BlobServiceClient blobServiceClient,
                                                          AzureBlobDocumentConfig azureBlobDocumentConfig) {
      String containerName = azureBlobDocumentConfig.containerName();
      BlobContainerClient blobContainerClient = blobServiceClient.getBlobContainerClient(containerName);

      if (!blobContainerClient.exists()) {
         if (azureBlobDocumentConfig.createIfMissingContainer()) {
            blobContainerClient.create();
            log.debug("Created missing '{}' container", containerName);
         } else {
            throw new DocumentAccessException(String.format("Unknown '%s' container", containerName));
         }
      }

      return blobContainerClient;
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", AzureBlobDocumentRepository.class.getSimpleName() + "[", "]").toString();
   }
}
