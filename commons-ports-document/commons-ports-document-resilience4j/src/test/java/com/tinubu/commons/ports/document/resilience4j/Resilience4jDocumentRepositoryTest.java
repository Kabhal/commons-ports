/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.resilience4j;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.clearInvocations;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.nio.file.Path;
import java.time.Duration;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.lang3.time.StopWatch;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.stubbing.Answer;

import com.tinubu.commons.ddd2.domain.event.DomainEvent;
import com.tinubu.commons.ddd2.domain.event.DomainEventListener;
import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.lang.util.UncheckedException;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentEntryCriteria.DocumentEntryCriteriaBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.event.DocumentAccessed;
import com.tinubu.commons.ports.document.domain.event.DocumentDeleted;
import com.tinubu.commons.ports.document.domain.event.DocumentSaved;
import com.tinubu.commons.ports.document.domain.repository.TestDocumentRepository;
import com.tinubu.commons.ports.document.resilience4j.Resilience4jDocumentConfig.Resilience4jDocumentConfigBuilder;
import com.tinubu.commons.ports.resilience4j.Resilience4jConfig;
import com.tinubu.commons.ports.resilience4j.Resilience4jConfig.BulkheadConfig.BulkheadConfigBuilder;
import com.tinubu.commons.ports.resilience4j.Resilience4jConfig.RateLimiterConfig.RateLimiterConfigBuilder;
import com.tinubu.commons.ports.resilience4j.Resilience4jConfig.Resilience4jConfigBuilder;
import com.tinubu.commons.ports.resilience4j.Resilience4jConfig.RetryConfig.RetryConfigBuilder;
import com.tinubu.commons.ports.resilience4j.Resilience4jConfig.ThreadPoolBulkheadConfig.ThreadPoolBulkheadConfigBuilder;
import com.tinubu.commons.ports.resilience4j.Resilience4jConfig.TimeLimiterConfig.TimeLimiterConfigBuilder;
import com.tinubu.commons.ports.resilience4j.event.FailedRetryEvent;
import com.tinubu.commons.ports.resilience4j.event.FinishedCallBulkheadEvent;
import com.tinubu.commons.ports.resilience4j.event.PermittedCallBulkheadEvent;
import com.tinubu.commons.ports.resilience4j.event.PermittedRateLimiterEvent;
import com.tinubu.commons.ports.resilience4j.event.RejectedCallBulkheadEvent;
import com.tinubu.commons.ports.resilience4j.event.RejectedRateLimiterEvent;
import com.tinubu.commons.ports.resilience4j.event.Resilience4jEvent;
import com.tinubu.commons.ports.resilience4j.event.RetriedRetryEvent;
import com.tinubu.commons.ports.resilience4j.event.SucceededRetryEvent;
import com.tinubu.commons.ports.resilience4j.event.TimedOutTimeLimiterEvent;

import io.github.resilience4j.bulkhead.BulkheadFullException;
import io.github.resilience4j.ratelimiter.RequestNotPermitted;

/**
 * Resilience4j test suite.
 */
public class Resilience4jDocumentRepositoryTest {

   @Test
   public void testSameRepositoryAsWhenNominal() {
      try (TestDocumentRepository testDocumentRepository = new TestDocumentRepository()) {

         Resilience4jDocumentRepository documentRepository =
               new Resilience4jDocumentRepository(new Resilience4jDocumentConfigBuilder()
                                                        .resilience(new Resilience4jConfigBuilder().build())
                                                        .build(), testDocumentRepository, "test", true);

         assertThat(documentRepository.sameRepositoryAs(testDocumentRepository)).isTrue();
      }
   }

   @Test
   public void testSameRepositoryAsWhenResilience4jRepository() {
      try (TestDocumentRepository testDocumentRepository = new TestDocumentRepository()) {

         Resilience4jDocumentRepository documentRepository =
               new Resilience4jDocumentRepository(new Resilience4jDocumentConfigBuilder()
                                                        .resilience(new Resilience4jConfigBuilder().build())
                                                        .build(), testDocumentRepository, "test", true);

         assertThat(documentRepository.sameRepositoryAs(new Resilience4jDocumentRepository(new Resilience4jDocumentConfigBuilder()
                                                                                                 .resilience(
                                                                                                       new Resilience4jConfigBuilder().build())
                                                                                                 .build(),
                                                                                           testDocumentRepository,
                                                                                           "test",
                                                                                           true)))
               .as("Only delegates must be taken into account in sameRepositoryAs")
               .isTrue();
         assertThat(documentRepository.sameRepositoryAs(new Resilience4jDocumentRepository(new Resilience4jDocumentConfigBuilder()
                                                                                                 .resilience(
                                                                                                       new Resilience4jConfigBuilder()
                                                                                                             .retry(
                                                                                                                   new RetryConfigBuilder()
                                                                                                                         .maxAttempts(
                                                                                                                               10)
                                                                                                                         .waitDuration(
                                                                                                                               Duration.ofMillis(
                                                                                                                                     500))
                                                                                                                         .build())
                                                                                                             .build())
                                                                                                 .build(),
                                                                                           testDocumentRepository,
                                                                                           "different-name",
                                                                                           false)))
               .as("Different resilience configuration must not interfere with sameRepositoryAs because it does not change the 'storage space'")
               .isTrue();
      }
   }

   /**
    * Common test suite without resilience.
    */
   @Nested
   public class NoResilienceWhenNominal extends CommonResilience4jDocumentRepositoryTest { }

   /**
    * Common test suite without resilience, using eager loading.
    */
   @Nested
   // TODO test that Streams and content stream are effectively pre-loaded ?
   public class NoResilienceWithEagerLoadWhenNominal extends CommonResilience4jDocumentRepositoryTest {
      public NoResilienceWithEagerLoadWhenNominal() {
         super(true);
      }
   }

   /**
    * All resilience patterns configured together : no bulkhead/thread pool bulkhead.
    */
   @Nested
   public class AllResilienceUsingNoBulkheadWhenNominal extends CommonResilience4jDocumentRepositoryTest {
      @Override
      protected Resilience4jConfig resilience4jConfig() {
         return new Resilience4jConfigBuilder()
               .timeLimiter(new TimeLimiterConfigBuilder().timeoutDuration(Duration.ofSeconds(1)).build())
               .rateLimiter(new RateLimiterConfigBuilder()
                                  .limitForPeriod(50)
                                  .limitRefreshPeriod(Duration.ofNanos(500))
                                  .timeoutDuration(Duration.ofSeconds(5))
                                  .build())
               .retry(new RetryConfigBuilder().maxAttempts(3).waitDuration(Duration.ofMillis(500)).build())
               .build();
      }
   }

   /**
    * All resilience patterns configured together : using bulkhead.
    */
   @Nested
   public class AllResilienceUsingBulkheadWhenNominal extends CommonResilience4jDocumentRepositoryTest {
      @Override
      protected Resilience4jConfig resilience4jConfig() {
         return new Resilience4jConfigBuilder()
               .bulkhead(new BulkheadConfigBuilder().maxConcurrentCalls(25).build())
               .timeLimiter(new TimeLimiterConfigBuilder().timeoutDuration(Duration.ofSeconds(1)).build())
               .rateLimiter(new RateLimiterConfigBuilder()
                                  .limitForPeriod(50)
                                  .limitRefreshPeriod(Duration.ofNanos(500))
                                  .timeoutDuration(Duration.ofSeconds(5))
                                  .build())
               .retry(new RetryConfigBuilder().maxAttempts(3).waitDuration(Duration.ofMillis(500)).build())
               .build();
      }
   }

   /**
    * All resilience patterns configured together : using thread pool bulkhead.
    */
   @Nested
   public class AllResilienceUsingThreadPoolBulkheadWhenNominal
         extends CommonResilience4jDocumentRepositoryTest {
      @Override
      protected Resilience4jConfig resilience4jConfig() {
         return new Resilience4jConfigBuilder()
               .threadPoolBulkhead(new ThreadPoolBulkheadConfigBuilder()
                                         .queueCapacity(100)
                                         .keepAliveDuration(Duration.ofMillis(20))
                                         .build())
               .timeLimiter(new TimeLimiterConfigBuilder().timeoutDuration(Duration.ofSeconds(1)).build())
               .rateLimiter(new RateLimiterConfigBuilder()
                                  .limitForPeriod(50)
                                  .limitRefreshPeriod(Duration.ofNanos(500))
                                  .timeoutDuration(Duration.ofSeconds(5))
                                  .build())
               .retry(new RetryConfigBuilder().maxAttempts(3).waitDuration(Duration.ofMillis(500)).build())
               .build();
      }
   }

   /**
    * All resilience patterns configured together : using both bulkhead and thread pool bulkhead (bulkhead is
    * used in priority).
    */
   @Nested
   public class AllResilienceUsingBothBulkheadsWhenNominal extends CommonResilience4jDocumentRepositoryTest {
      @Override
      protected Resilience4jConfig resilience4jConfig() {
         return new Resilience4jConfigBuilder()
               .bulkhead(new BulkheadConfigBuilder().maxConcurrentCalls(25).build())
               .threadPoolBulkhead(new ThreadPoolBulkheadConfigBuilder()
                                         .queueCapacity(100)
                                         .keepAliveDuration(Duration.ofMillis(20))
                                         .build())
               .timeLimiter(new TimeLimiterConfigBuilder().timeoutDuration(Duration.ofSeconds(1)).build())
               .rateLimiter(new RateLimiterConfigBuilder()
                                  .limitForPeriod(50)
                                  .limitRefreshPeriod(Duration.ofNanos(500))
                                  .timeoutDuration(Duration.ofSeconds(5))
                                  .build())
               .retry(new RetryConfigBuilder().maxAttempts(3).waitDuration(Duration.ofMillis(500)).build())
               .build();
      }
   }

   /**
    * Test resilient calls from client code, when consuming stream and content stream externally.
    */
   @Nested
   public class ExternalResilientCall extends BaseResilience4jDocumentRepositoryTest {

      @Override
      protected Resilience4jConfig resilience4jConfig() {
         return new Resilience4jConfigBuilder()
               .retry(new RetryConfigBuilder().maxAttempts(3).waitDuration(Duration.ofMillis(500)).build())
               .build();
      }

      @Test
      public void testFindDocumentByIdWhenExternallyRead() {
         Path documentPath = path("path/pathfile1.pdf");
         DocumentPath testDocument = DocumentPath.of(documentPath);

         DomainEventListener<DomainEvent> eventListener =
               instrumentDocumentRepositoryListeners(DocumentSaved.class,
                                                     DocumentAccessed.class,
                                                     DocumentDeleted.class,
                                                     Resilience4jEvent.class);

         try {
            createDocuments(realDocumentRepository(), documentPath);

            clearInvocations(eventListener);
            clearInvocations(realDocumentRepository());

            Document document =
                  documentRepository().findDocumentById(testDocument).orElseThrow().loadContent();

            verify(realDocumentRepository()).findDocumentById(any());
            verify(eventListener).accept(any(DocumentAccessed.class));
            verifyNoMoreInteractions(eventListener);

            assertThatExceptionOfType(DocumentAccessException.class).isThrownBy(() -> {

               ((Resilience4jDocumentRepository) documentRepository()).resilientCall(() -> {
                  document.content().stringContent();
                  throw new DocumentAccessException("Fake error");
               }, false);

            }).withMessage("Fake error");

         } finally {
            deleteDocuments(realDocumentRepository(), documentPath);
         }
      }
   }

   /**
    * Bulkhead pattern tests.
    */
   @Nested
   public class Bulkhead {

      @Nested
      public class DefaultBulkheadWhenNominal extends CommonResilience4jDocumentRepositoryTest {
         @Override
         protected Resilience4jConfig resilience4jConfig() {
            return new Resilience4jConfigBuilder()
                  .bulkhead(new BulkheadConfigBuilder().maxConcurrentCalls(25).build())
                  .build();
         }

         @Test
         @Override
         public void testFindDocumentByIdWhenNominal() {
            Path documentPath = path("path/pathfile1.pdf");
            DocumentPath testDocument = DocumentPath.of(documentPath);

            DomainEventListener<DomainEvent> eventListener = instrumentDocumentRepositoryListeners(
                  DocumentSaved.class,
                  DocumentAccessed.class,
                  DocumentDeleted.class,
                  Resilience4jEvent.class);

            try {
               createDocuments(realDocumentRepository(), documentPath);

               clearInvocations(eventListener);
               clearInvocations(realDocumentRepository());

               assertThat(documentRepository().findDocumentById(testDocument)).isNotNull();

               verify(realDocumentRepository()).findDocumentById(any());
               verify(eventListener).accept(any(DocumentAccessed.class));
               verify(eventListener).accept(any(PermittedCallBulkheadEvent.class));
               verify(eventListener).accept(any(FinishedCallBulkheadEvent.class));
               verifyNoMoreInteractions(eventListener);
            } finally {
               deleteDocuments(realDocumentRepository(), documentPath);
            }
         }

         @Test
         @Override
         public void testFindDocumentEntriesBySpecificationWhenAbsoluteBaseDirectory() {
            DomainEventListener<DomainEvent> eventListener = instrumentDocumentRepositoryListeners(
                  DocumentSaved.class,
                  DocumentAccessed.class,
                  DocumentDeleted.class,
                  Resilience4jEvent.class);

            clearInvocations(eventListener);

            assertThatExceptionOfType(InvariantValidationException.class)
                  .isThrownBy(() -> documentRepository()
                        .findDocumentEntriesBySpecification(path("/path"),
                                                            new DocumentEntryCriteriaBuilder().build())
                        .count())
                  .withMessage("Invariant validation error > 'basePath=/path' must not be absolute path");

            verify(realDocumentRepository()).findDocumentEntriesBySpecification(any(), any());
            verify(eventListener).accept(any(PermittedCallBulkheadEvent.class));
            verify(eventListener).accept(any(FinishedCallBulkheadEvent.class));
            verifyNoMoreInteractions(eventListener);
         }
      }

      @Nested
      public class DefaultBulkheadWhenFull extends BaseResilience4jDocumentRepositoryTest {
         @Override
         protected Resilience4jConfig resilience4jConfig() {
            return new Resilience4jConfigBuilder()
                  .bulkhead(new BulkheadConfigBuilder().maxConcurrentCalls(0).build())
                  .build();
         }

         @Test
         public void testFindDocumentByIdWhenBulkheadFull() {
            Path documentPath = path("path/pathfile1.pdf");
            DocumentPath testDocument = DocumentPath.of(documentPath);

            DomainEventListener<DomainEvent> eventListener = instrumentDocumentRepositoryListeners(
                  DocumentSaved.class,
                  DocumentAccessed.class,
                  DocumentDeleted.class,
                  Resilience4jEvent.class);

            try {
               createDocuments(realDocumentRepository(), documentPath);

               clearInvocations(eventListener);
               clearInvocations(realDocumentRepository());

               assertThatExceptionOfType(BulkheadFullException.class)
                     .isThrownBy(() -> documentRepository().findDocumentById(testDocument))
                     .withMessage("Bulkhead 'test' is full and does not permit further calls");

               verify(realDocumentRepository(), never()).findDocumentById(any());
               verify(eventListener).accept(any(RejectedCallBulkheadEvent.class));
               verifyNoMoreInteractions(eventListener);
            } finally {
               deleteDocuments(realDocumentRepository(), documentPath);
            }
         }
      }

      @Nested
      public class DefaultBulkheadWithMaxWaitDurationWhenFull extends BaseResilience4jDocumentRepositoryTest {
         @Override
         protected Resilience4jConfig resilience4jConfig() {
            return new Resilience4jConfigBuilder()
                  .bulkhead(new BulkheadConfigBuilder()
                                  .maxConcurrentCalls(0)
                                  .maxWaitDuration(Duration.ofMillis(2000))
                                  .build())
                  .build();
         }

         @Test
         public void testFindDocumentByIdWhenBulkheadFull() {
            Path documentPath = path("path/pathfile1.pdf");
            DocumentPath testDocument = DocumentPath.of(documentPath);

            DomainEventListener<DomainEvent> eventListener = instrumentDocumentRepositoryListeners(
                  DocumentSaved.class,
                  DocumentAccessed.class,
                  DocumentDeleted.class,
                  Resilience4jEvent.class);

            try {
               createDocuments(realDocumentRepository(), documentPath);

               clearInvocations(eventListener);
               clearInvocations(realDocumentRepository());
               StopWatch watch = StopWatch.createStarted();

               assertThatExceptionOfType(BulkheadFullException.class)
                     .isThrownBy(() -> documentRepository().findDocumentById(testDocument))
                     .withMessage("Bulkhead 'test' is full and does not permit further calls");

               assertThat(watch.getTime())
                     .as("bulkhead must wait for room for maxWaitDuration")
                     .isGreaterThanOrEqualTo(2000);

               verify(realDocumentRepository(), never()).findDocumentById(any());
               verify(eventListener).accept(any(RejectedCallBulkheadEvent.class));
               verifyNoMoreInteractions(eventListener);
            } finally {
               deleteDocuments(realDocumentRepository(), documentPath);
            }
         }
      }

      @Nested
      public class DefaultBulkheadWithZeroWaitDuration extends BaseResilience4jDocumentRepositoryTest {
         @Override
         protected Resilience4jConfig resilience4jConfig() {
            return new Resilience4jConfigBuilder()
                  .bulkhead(new BulkheadConfigBuilder()
                                  .maxConcurrentCalls(0)
                                  .maxWaitDuration(Duration.ZERO)
                                  .build())
                  .build();
         }

         @Test
         public void testFindDocumentByIdWhenBulkheadFull() {
            Path documentPath = path("path/pathfile1.pdf");
            DocumentPath testDocument = DocumentPath.of(documentPath);

            DomainEventListener<DomainEvent> eventListener = instrumentDocumentRepositoryListeners(
                  DocumentSaved.class,
                  DocumentAccessed.class,
                  DocumentDeleted.class,
                  Resilience4jEvent.class);

            try {
               createDocuments(realDocumentRepository(), documentPath);

               clearInvocations(eventListener);
               clearInvocations(realDocumentRepository());

               assertThatExceptionOfType(BulkheadFullException.class)
                     .isThrownBy(() -> documentRepository().findDocumentById(testDocument))
                     .withMessage("Bulkhead 'test' is full and does not permit further calls");

               verify(realDocumentRepository(), never()).findDocumentById(any());
               verify(eventListener).accept(any(RejectedCallBulkheadEvent.class));
               verifyNoMoreInteractions(eventListener);
            } finally {
               deleteDocuments(realDocumentRepository(), documentPath);
            }
         }
      }

   }

   /**
    * Thread pool bulkhead pattern tests.
    */
   @Nested
   public class ThreadPoolBulkhead {

      @Nested
      public class DefaultThreadPoolBulkheadWhenNominal extends CommonResilience4jDocumentRepositoryTest {
         @Override
         protected Resilience4jConfig resilience4jConfig() {
            return new Resilience4jConfigBuilder()
                  .threadPoolBulkhead(new ThreadPoolBulkheadConfigBuilder()
                                            .queueCapacity(100)
                                            .keepAliveDuration(Duration.ofMillis(20))
                                            .build())
                  .build();
         }

         @Test
         @Override
         public void testFindDocumentByIdWhenNominal() {
            Path documentPath = path("path/pathfile1.pdf");
            DocumentPath testDocument = DocumentPath.of(documentPath);

            DomainEventListener<DomainEvent> eventListener = instrumentDocumentRepositoryListeners(
                  DocumentSaved.class,
                  DocumentAccessed.class,
                  DocumentDeleted.class,
                  Resilience4jEvent.class);

            try {
               createDocuments(realDocumentRepository(), documentPath);

               clearInvocations(eventListener);
               clearInvocations(realDocumentRepository());

               assertThat(documentRepository().findDocumentById(testDocument)).isNotNull();

               verify(realDocumentRepository()).findDocumentById(any());
               verify(eventListener).accept(any(DocumentAccessed.class));
               verify(eventListener).accept(any(PermittedCallBulkheadEvent.class));
               verify(eventListener).accept(any(FinishedCallBulkheadEvent.class));
               verifyNoMoreInteractions(eventListener);
            } finally {
               deleteDocuments(realDocumentRepository(), documentPath);
            }
         }

         @Test
         @Override
         public void testFindDocumentEntriesBySpecificationWhenAbsoluteBaseDirectory() {
            DomainEventListener<DomainEvent> eventListener = instrumentDocumentRepositoryListeners(
                  DocumentSaved.class,
                  DocumentAccessed.class,
                  DocumentDeleted.class,
                  Resilience4jEvent.class);

            clearInvocations(eventListener);

            assertThatExceptionOfType(InvariantValidationException.class)
                  .isThrownBy(() -> documentRepository()
                        .findDocumentEntriesBySpecification(path("/path"),
                                                            new DocumentEntryCriteriaBuilder().build())
                        .count())
                  .withMessage("Invariant validation error > 'basePath=/path' must not be absolute path");

            verify(realDocumentRepository()).findDocumentEntriesBySpecification(any(), any());
            verify(eventListener).accept(any(PermittedCallBulkheadEvent.class));
            verify(eventListener).accept(any(FinishedCallBulkheadEvent.class));
            verifyNoMoreInteractions(eventListener);
         }

      }

      @Nested
      public class DefaultThreadPoolBulkheadWhenFull extends BaseResilience4jDocumentRepositoryTest {
         @Override
         protected Resilience4jConfig resilience4jConfig() {
            return new Resilience4jConfigBuilder()
                  .threadPoolBulkhead(new ThreadPoolBulkheadConfigBuilder()
                                            .maxThreadPoolSize(1)
                                            .queueCapacity(1)
                                            .keepAliveDuration(Duration.ofMillis(20))
                                            .build())
                  .build();
         }

         /**
          * Depleting bulkhead by launching 2 long calls in separate threads : 1 to deplete the thread pool, 1
          * to deplete the queue.
          */
         @BeforeEach
         public void depletingBulkhead() throws InterruptedException {

            when(realDocumentRepository().findDocumentById(DocumentPath.of("long-call"))).thenAnswer((Answer<Optional<Document>>) invocation -> {
               while (!Thread.currentThread().isInterrupted()) {
                  try {
                     Thread.sleep(100);
                  } catch (InterruptedException e) {
                     Thread.currentThread().interrupt();
                  }
               }
               return (Optional<Document>) invocation.callRealMethod();
            });

            ExecutorService executorService = Executors.newFixedThreadPool(2);

            for (int i = 0; i < 2; i++) {
               executorService.submit(() -> {
                  documentRepository().findDocumentById(DocumentPath.of("long-call"));
               });
            }

            Thread.sleep(1000); // Poorly fix race condition between depleting threads and real test thread
         }

         @Test
         public void testFindDocumentByIdWhenBulkheadFull() {
            Path documentPath = path("path/pathfile1.pdf");
            DocumentPath testDocument = DocumentPath.of(documentPath);

            DomainEventListener<DomainEvent> eventListener = instrumentDocumentRepositoryListeners(
                  DocumentSaved.class,
                  DocumentAccessed.class,
                  DocumentDeleted.class,
                  Resilience4jEvent.class);

            try {
               createDocuments(realDocumentRepository(), documentPath);

               clearInvocations(eventListener);
               clearInvocations(realDocumentRepository());

               assertThatExceptionOfType(BulkheadFullException.class)
                     .isThrownBy(() -> documentRepository().findDocumentById(testDocument))
                     .withMessage("Bulkhead 'test' is full and does not permit further calls");

               verify(realDocumentRepository(), never()).findDocumentById(any());
               verify(eventListener).accept(any(RejectedCallBulkheadEvent.class));
               verifyNoMoreInteractions(eventListener);
            } finally {
               deleteDocuments(realDocumentRepository(), documentPath);
            }
         }
      }

   }

   /**
    * Time limiter pattern tests.
    */
   @Nested
   public class TimeLimiter {

      @Nested
      public class DefaultTimeLimiterWhenNominal extends CommonResilience4jDocumentRepositoryTest {
         @Override
         protected Resilience4jConfig resilience4jConfig() {
            return new Resilience4jConfigBuilder()
                  .timeLimiter(new TimeLimiterConfigBuilder().timeoutDuration(Duration.ofSeconds(1)).build())
                  .build();
         }
      }

      @Nested
      public class DefaultTimeLimiterWhenTimedOutOperation extends CommonResilience4jDocumentRepositoryTest {
         @Override
         protected Resilience4jConfig resilience4jConfig() {
            return new Resilience4jConfigBuilder()
                  .timeLimiter(new TimeLimiterConfigBuilder().timeoutDuration(Duration.ofSeconds(1)).build())
                  .build();
         }

         @Test
         public void testFindDocumentByIdWhenTimedOut() {
            Path documentPath = path("path/pathfile1.pdf");
            DocumentPath testDocument = DocumentPath.of(documentPath);

            when(realDocumentRepository().findDocumentById(testDocument)).thenAnswer((Answer<Optional<Document>>) invocation -> {
               while (!Thread.currentThread().isInterrupted()) {
                  try {
                     Thread.sleep(100);
                  } catch (InterruptedException e) {
                     Thread.currentThread().interrupt();
                  }
               }
               return (Optional<Document>) invocation.callRealMethod();
            });

            DomainEventListener<DomainEvent> eventListener = instrumentDocumentRepositoryListeners(
                  DocumentSaved.class,
                  DocumentAccessed.class,
                  DocumentDeleted.class,
                  Resilience4jEvent.class);

            try {
               createDocuments(realDocumentRepository(), documentPath);

               clearInvocations(eventListener);
               clearInvocations(realDocumentRepository());

               assertThatExceptionOfType(UncheckedException.class)
                     .isThrownBy(() -> documentRepository().findDocumentById(testDocument))
                     .withMessage("TimeLimiter 'test' recorded a timeout exception.")
                     .withCauseExactlyInstanceOf(TimeoutException.class);

               verify(realDocumentRepository()).findDocumentById(any());
               verify(eventListener).accept(any(TimedOutTimeLimiterEvent.class));
               verifyNoMoreInteractions(eventListener);
            } finally {
               deleteDocuments(realDocumentRepository(), documentPath);
            }
         }

      }

   }

   /**
    * Rate limiter pattern tests.
    */
   @Nested
   public class RateLimiter {

      @Nested
      public class DefaultRateLimiterWhenNominal extends CommonResilience4jDocumentRepositoryTest {
         @Override
         protected Resilience4jConfig resilience4jConfig() {
            return new Resilience4jConfigBuilder()
                  .rateLimiter(new RateLimiterConfigBuilder()
                                     .limitForPeriod(50)
                                     .limitRefreshPeriod(Duration.ofNanos(500))
                                     .timeoutDuration(Duration.ofSeconds(5))
                                     .build())
                  .build();
         }

         @Test
         @Override
         public void testFindDocumentByIdWhenNominal() {
            Path documentPath = path("path/pathfile1.pdf");
            DocumentPath testDocument = DocumentPath.of(documentPath);

            DomainEventListener<DomainEvent> eventListener = instrumentDocumentRepositoryListeners(
                  DocumentSaved.class,
                  DocumentAccessed.class,
                  DocumentDeleted.class,
                  Resilience4jEvent.class);

            try {
               createDocuments(realDocumentRepository(), documentPath);

               clearInvocations(eventListener);
               clearInvocations(realDocumentRepository());

               assertThat(documentRepository().findDocumentById(testDocument)).isNotNull();

               verify(realDocumentRepository()).findDocumentById(any());
               verify(eventListener).accept(any(DocumentAccessed.class));
               verify(eventListener).accept(any(PermittedRateLimiterEvent.class));
               verifyNoMoreInteractions(eventListener);
            } finally {
               deleteDocuments(realDocumentRepository(), documentPath);
            }
         }

      }

      @Nested
      public class DefaultRateLimiterWhenRejected extends BaseResilience4jDocumentRepositoryTest {
         @Override
         protected Resilience4jConfig resilience4jConfig() {
            return new Resilience4jConfigBuilder()
                  .rateLimiter(new RateLimiterConfigBuilder()
                                     .limitRefreshPeriod(Duration.ofSeconds(10))
                                     .limitForPeriod(1)
                                     .timeoutDuration(Duration.ofSeconds(0))
                                     .build())
                  .build();
         }

         @Test
         public void testFindDocumentByIdWhenRejected() {
            Path documentPath = path("path/pathfile1.pdf");
            DocumentPath testDocument = DocumentPath.of(documentPath);

            DomainEventListener<DomainEvent> eventListener = instrumentDocumentRepositoryListeners(
                  DocumentSaved.class,
                  DocumentAccessed.class,
                  DocumentDeleted.class,
                  Resilience4jEvent.class);

            try {
               createDocuments(realDocumentRepository(), documentPath);

               assertThat(documentRepository().findDocumentById(testDocument)).isNotNull();

               clearInvocations(eventListener);
               clearInvocations(realDocumentRepository());

               assertThatExceptionOfType(RequestNotPermitted.class)
                     .isThrownBy(() -> documentRepository().findDocumentById(testDocument))
                     .withMessage("RateLimiter 'test' does not permit further calls");

               verify(realDocumentRepository(), never()).findDocumentById(any());
               verify(eventListener).accept(any(RejectedRateLimiterEvent.class));
               verifyNoMoreInteractions(eventListener);
            } finally {
               deleteDocuments(realDocumentRepository(), documentPath);
            }
         }

      }

      @Nested
      public class DefaultRateLimiterWhenRejectedWithTimeout extends BaseResilience4jDocumentRepositoryTest {
         @Override
         protected Resilience4jConfig resilience4jConfig() {
            return new Resilience4jConfigBuilder()
                  .rateLimiter(new RateLimiterConfigBuilder()
                                     .limitRefreshPeriod(Duration.ofSeconds(10))
                                     .limitForPeriod(1)
                                     .timeoutDuration(Duration.ofSeconds(2))
                                     .build())
                  .build();
         }

         @Test
         public void testFindDocumentByIdWhenRejectedWithTimeout() {
            Path documentPath = path("path/pathfile1.pdf");
            DocumentPath testDocument = DocumentPath.of(documentPath);

            DomainEventListener<DomainEvent> eventListener = instrumentDocumentRepositoryListeners(
                  DocumentSaved.class,
                  DocumentAccessed.class,
                  DocumentDeleted.class,
                  Resilience4jEvent.class);

            try {
               createDocuments(realDocumentRepository(), documentPath);

               assertThat(documentRepository().findDocumentById(testDocument)).isNotNull();

               clearInvocations(eventListener);
               clearInvocations(realDocumentRepository());

               StopWatch watch = StopWatch.createStarted();
               assertThatExceptionOfType(RequestNotPermitted.class)
                     .isThrownBy(() -> documentRepository().findDocumentById(testDocument))
                     .withMessage("RateLimiter 'test' does not permit further calls");

               assertThat(watch.getTime()).isGreaterThanOrEqualTo(2000);

               verify(realDocumentRepository(), never()).findDocumentById(any());
               verify(eventListener).accept(any(RejectedRateLimiterEvent.class));
               verifyNoMoreInteractions(eventListener);
            } finally {
               deleteDocuments(realDocumentRepository(), documentPath);
            }
         }

      }
   }

   /**
    * Retry pattern tests.
    */
   @Nested
   public class Retry {

      @Nested
      public class DefaultRetryWhenNominal extends CommonResilience4jDocumentRepositoryTest {

         @Override
         protected Resilience4jConfig resilience4jConfig() {
            return new Resilience4jConfigBuilder()
                  .retry(new RetryConfigBuilder().maxAttempts(3).waitDuration(Duration.ofMillis(500)).build())
                  .build();
         }

         @Test
         @Override
         public void testFindDocumentByIdWhenNominal() {
            Path documentPath = path("path/pathfile1.pdf");
            DocumentPath testDocument = DocumentPath.of(documentPath);

            DomainEventListener<DomainEvent> eventListener = instrumentDocumentRepositoryListeners(
                  DocumentSaved.class,
                  DocumentAccessed.class,
                  DocumentDeleted.class,
                  Resilience4jEvent.class);

            try {
               createDocuments(realDocumentRepository(), documentPath);

               clearInvocations(eventListener);
               clearInvocations(realDocumentRepository());

               assertThat(documentRepository().findDocumentById(testDocument)).isNotNull();

               verify(realDocumentRepository()).findDocumentById(any());
               verify(eventListener).accept(any(DocumentAccessed.class));
               verifyNoMoreInteractions(eventListener);
            } finally {
               deleteDocuments(realDocumentRepository(), documentPath);
            }
         }

      }

      @Nested
      public class DefaultRetryWhenFailedAttempt extends CommonResilience4jDocumentRepositoryTest {
         @Override
         protected Resilience4jConfig resilience4jConfig() {
            return new Resilience4jConfigBuilder()
                  .retry(new RetryConfigBuilder().maxAttempts(3).waitDuration(Duration.ofMillis(500)).build())
                  .build();
         }

         @Test
         public void testFindDocumentByIdWhenFailedAttempt() {
            Path documentPath = path("path/pathfile1.pdf");
            DocumentPath testDocument = DocumentPath.of(documentPath);

            AtomicInteger callCounter = new AtomicInteger(0);
            when(realDocumentRepository().findDocumentById(testDocument)).thenAnswer((Answer<Optional<Document>>) invocation -> {
               if (callCounter.incrementAndGet() <= 2) {
                  throw new DocumentAccessException("Fake error");
               }
               return (Optional<Document>) invocation.callRealMethod();
            });

            DomainEventListener<DomainEvent> eventListener = instrumentDocumentRepositoryListeners(
                  DocumentSaved.class,
                  DocumentAccessed.class,
                  DocumentDeleted.class,
                  Resilience4jEvent.class);

            try {
               createDocuments(realDocumentRepository(), documentPath);

               clearInvocations(eventListener);
               clearInvocations(realDocumentRepository());

               assertThat(documentRepository().findDocumentById(testDocument)).isNotNull();

               verify(realDocumentRepository(), times(3)).findDocumentById(any());
               verify(eventListener).accept(any(DocumentAccessed.class));
               verify(eventListener, times(2)).accept(any(RetriedRetryEvent.class));
               verify(eventListener).accept(any(SucceededRetryEvent.class));
               verifyNoMoreInteractions(eventListener);
            } finally {
               deleteDocuments(realDocumentRepository(), documentPath);
            }
         }

      }

      @Nested
      public class DefaultRetryWhenMaxAttemptFailed extends CommonResilience4jDocumentRepositoryTest {
         @Override
         protected Resilience4jConfig resilience4jConfig() {
            return new Resilience4jConfigBuilder()
                  .retry(new RetryConfigBuilder().maxAttempts(3).waitDuration(Duration.ofMillis(500)).build())
                  .build();
         }

         @Test
         public void testFindDocumentByIdWhenMaxAttemptFailed() {
            Path documentPath = path("path/pathfile1.pdf");
            DocumentPath testDocument = DocumentPath.of(documentPath);

            AtomicInteger callCounter = new AtomicInteger(0);
            when(realDocumentRepository().findDocumentById(testDocument)).thenAnswer((Answer<Optional<Document>>) invocation -> {
               if (callCounter.incrementAndGet() <= 10) {
                  throw new DocumentAccessException("Fake error");
               }
               return (Optional<Document>) invocation.callRealMethod();
            });

            DomainEventListener<DomainEvent> eventListener = instrumentDocumentRepositoryListeners(
                  DocumentSaved.class,
                  DocumentAccessed.class,
                  DocumentDeleted.class,
                  Resilience4jEvent.class);

            try {
               createDocuments(realDocumentRepository(), documentPath);

               clearInvocations(eventListener);
               clearInvocations(realDocumentRepository());

               assertThatExceptionOfType(DocumentAccessException.class)
                     .isThrownBy(() -> documentRepository().findDocumentById(testDocument))
                     .withMessage("Fake error");

               verify(realDocumentRepository(), times(3)).findDocumentById(any());
               verify(eventListener, times(2)).accept(any(RetriedRetryEvent.class));
               verify(eventListener).accept(any(FailedRetryEvent.class));
               verifyNoMoreInteractions(eventListener);
            } finally {
               deleteDocuments(realDocumentRepository(), documentPath);
            }
         }

      }

      @Nested
      public class DefaultRetryWhenUnretryableExceptionFailed
            extends CommonResilience4jDocumentRepositoryTest {
         @Override
         protected Resilience4jConfig resilience4jConfig() {
            return new Resilience4jConfigBuilder()
                  .retry(new RetryConfigBuilder().maxAttempts(3).waitDuration(Duration.ofMillis(500)).build())
                  .build();
         }

         @Test
         public void testFindDocumentByIdWhenUnretryableExceptionFailed() {
            Path documentPath = path("path/pathfile1.pdf");
            DocumentPath testDocument = DocumentPath.of(documentPath);

            AtomicInteger callCounter = new AtomicInteger(0);
            when(realDocumentRepository().findDocumentById(testDocument)).thenAnswer((Answer<Optional<Document>>) invocation -> {
               if (callCounter.incrementAndGet() <= 10) {
                  throw new IllegalStateException("Fake error");
               }
               return (Optional<Document>) invocation.callRealMethod();
            });

            DomainEventListener<DomainEvent> eventListener = instrumentDocumentRepositoryListeners(
                  DocumentSaved.class,
                  DocumentAccessed.class,
                  DocumentDeleted.class,
                  Resilience4jEvent.class);

            try {
               createDocuments(realDocumentRepository(), documentPath);

               clearInvocations(eventListener);
               clearInvocations(realDocumentRepository());

               assertThatExceptionOfType(IllegalStateException.class)
                     .isThrownBy(() -> documentRepository().findDocumentById(testDocument))
                     .withMessage("Fake error");

               verify(realDocumentRepository(), times(1)).findDocumentById(any());
               verify(eventListener).accept(any(FailedRetryEvent.class));
               verifyNoMoreInteractions(eventListener);
            } finally {
               deleteDocuments(realDocumentRepository(), documentPath);
            }
         }

      }

   }
}