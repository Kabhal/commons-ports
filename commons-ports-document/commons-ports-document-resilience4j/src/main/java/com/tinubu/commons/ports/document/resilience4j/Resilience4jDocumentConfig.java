/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.resilience4j;

import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;

import java.util.function.Supplier;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ports.resilience4j.Resilience4jConfig;
import com.tinubu.commons.ports.resilience4j.Resilience4jConfig.Resilience4jConfigBuilder;

/**
 * Resilience4j document repository configuration.
 */
public class Resilience4jDocumentConfig extends AbstractValue {
   private static final Supplier<Resilience4jConfig> DEFAULT_RESILIENCE =
         () -> new Resilience4jConfigBuilder().build();

   private final Resilience4jConfig resilience;

   public Resilience4jDocumentConfig(Resilience4jDocumentConfigBuilder builder) {
      this.resilience = nullable(builder.resilience, DEFAULT_RESILIENCE);
   }

   @Override
   public Fields<? extends Resilience4jDocumentConfig> defineDomainFields() {
      return Fields
            .<Resilience4jDocumentConfig>builder()
            .field("resilience", v -> v.resilience, isNotNull())
            .build();
   }

   /**
    * Resilience configuration.
    *
    * @return resilience configuration
    */
   public Resilience4jConfig resilience() {
      return resilience;
   }

   public static class Resilience4jDocumentConfigBuilder extends DomainBuilder<Resilience4jDocumentConfig> {
      private Resilience4jConfig resilience;

      public Resilience4jDocumentConfigBuilder resilience(Resilience4jConfig resilience) {
         this.resilience = resilience;
         return this;
      }

      @Override
      protected Resilience4jDocumentConfig buildDomainObject() {
         return new Resilience4jDocumentConfig(this);
      }
   }

}
