/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.spring.document;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Duration;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;

import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.spring.autoconfigure.document.CommonsPortsDocumentFsAutoConfiguration;
import com.tinubu.commons.ports.spring.document.SingleDocumentRepositoryDependencyTest.TestConfig;
import com.tinubu.commons.ports.spring.document.fs.FsDocumentConfig;

/**
 * Demonstrates how to inject a {@link DocumentRepository} in an application with resilience.
 */
@SpringBootTest(classes = TestConfig.class)
@DirtiesContext
@TestPropertySource(properties = {
      "commons-ports.document.fs.failFastIfMissingStoragePath=false",

      "commons-ports.document.fs.resilience4j.bulkhead.max-concurrent-calls=25",
      "commons-ports.document.fs.resilience4j.bulkhead.max-wait-duration=10s",
      "commons-ports.document.fs.resilience4j.bulkhead.fair-call-handling-strategy=true",

      "commons-ports.document.fs.resilience4j.thread-pool-bulkhead.keep-alive-duration=20ms",
      "commons-ports.document.fs.resilience4j.thread-pool-bulkhead.core-thread-pool-size=1",
      "commons-ports.document.fs.resilience4j.thread-pool-bulkhead.max-thread-pool-size=4",
      "commons-ports.document.fs.resilience4j.thread-pool-bulkhead.queue-capacity=100",

      "commons-ports.document.fs.resilience4j.time-limiter.timeout-duration=1s",

      "commons-ports.document.fs.resilience4j.rate-limiter.limit-for-period=10",
      "commons-ports.document.fs.resilience4j.rate-limiter.limit-refresh-period=1s",
      "commons-ports.document.fs.resilience4j.rate-limiter.timeout-duration=10s",

      "commons-ports.document.fs.resilience4j.retry.max-attempts=3",
      "commons-ports.document.fs.resilience4j.retry.wait-duration=500ms",

})
public class Resilience4jConfigurationTest {

   /**
    * Simulates a real application Spring context with a single repository on classpath.
    */
   @ImportAutoConfiguration({ CommonsPortsDocumentFsAutoConfiguration.class })
   public static class TestConfig { }

   @Autowired
   FsDocumentConfig fsConfig;

   @Test
   public void testBulkheadConfiguration() {
      assertThat(fsConfig.getResilience4j().getBulkhead()).satisfies(bulkhead -> {
         assertThat(bulkhead.getMaxConcurrentCalls()).isEqualTo(25);
         assertThat(bulkhead.getMaxWaitDuration()).isEqualTo(Duration.ofSeconds(10));
         assertThat(bulkhead.getFairCallHandlingStrategy()).isTrue();
      });
   }

   @Test
   public void testThreadPoolBulkheadConfiguration() {
      assertThat(fsConfig.getResilience4j().getThreadPoolBulkhead()).satisfies(bulkhead -> {
         assertThat(bulkhead.getKeepAliveDuration()).isEqualTo(Duration.ofMillis(20));
         assertThat(bulkhead.getCoreThreadPoolSize()).isEqualTo(1);
         assertThat(bulkhead.getMaxThreadPoolSize()).isEqualTo(4);
         assertThat(bulkhead.getQueueCapacity()).isEqualTo(100);
      });
   }

   @Test
   public void testTimeLimiterConfiguration() {
      assertThat(fsConfig.getResilience4j().getTimeLimiter()).satisfies(limiter -> {
         assertThat(limiter.getTimeoutDuration()).isEqualTo(Duration.ofSeconds(1));
      });
   }

   @Test
   public void testRateLimiterConfiguration() {
      assertThat(fsConfig.getResilience4j().getRateLimiter()).satisfies(limiter -> {
         assertThat(limiter.getLimitForPeriod()).isEqualTo(10);
         assertThat(limiter.getLimitRefreshPeriod()).isEqualTo(Duration.ofSeconds(1));
         assertThat(limiter.getTimeoutDuration()).isEqualTo(Duration.ofSeconds(10));
      });
   }

   @Test
   public void testRetryConfiguration() {
      assertThat(fsConfig.getResilience4j().getRetry()).satisfies(retry -> {
         assertThat(retry.getMaxAttempts()).isEqualTo(3);
         assertThat(retry.getWaitDuration()).isEqualTo(Duration.ofMillis(500));
      });
   }

}
