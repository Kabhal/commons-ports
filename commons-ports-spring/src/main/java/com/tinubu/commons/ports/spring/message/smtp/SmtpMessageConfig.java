/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.spring.message.smtp;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import com.tinubu.commons.ports.spring.resilience4j.Resilience4jConfig;

/**
 * SMTP message service configuration.
 */
@Validated
@ConfigurationProperties(prefix = "commons-ports.message.smtp", ignoreUnknownFields = false)
public class SmtpMessageConfig {

   /**
    * Enable flag for the default repository instance.
    */
   private boolean enabled = true;

   /**
    * Resilience4j configuration. No resilience pattern is configured by default.
    *
    * @implSpec Requires {@code com.tinubu.commons-ports:commons-ports-message-resilience4j} module.
    */
   private Resilience4jConfig resilience4j = new Resilience4jConfig();

   public boolean isEnabled() {
      return enabled;
   }

   public void setEnabled(boolean enabled) {
      this.enabled = enabled;
   }

   public Resilience4jConfig getResilience4j() {
      return resilience4j;
   }

   public void setResilience4j(Resilience4jConfig resilience4j) {
      this.resilience4j = resilience4j;
   }

}
