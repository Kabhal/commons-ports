/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.spring.autoconfigure.document;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.tinubu.commons.ports.document.classpath.ClasspathDocumentRepository;
import com.tinubu.commons.ports.spring.autoconfigure.base.BasePortsDocumentAutoConfiguration;
import com.tinubu.commons.ports.spring.autoconfigure.base.ConditionalOnEnabledDocumentRepositoryBean;
import com.tinubu.commons.ports.spring.document.classpath.ClasspathDocumentConfig;

/**
 * Classpath document adapter auto-configuration.
 * <p>
 * This repository is configured only if {@code commons-ports.document[.classpath].enabled} property undefined
 * or is set to {@code true}.
 * Default bean names for this repository are :
 * <ul>
 *    <li>ClasspathDocumentConfigPoint : {@value CONFIG_BEAN_NAME}</li>
 *    <li>ClasspathDocumentRepository : {@value REPOSITORY_BEAN_NAME}</li>
 * </ul>
 */
@Configuration
@ConditionalOnClass(ClasspathDocumentRepository.class)
public class CommonsPortsDocumentClasspathAutoConfiguration extends BasePortsDocumentAutoConfiguration {

   private static final Logger log =
         LoggerFactory.getLogger(CommonsPortsDocumentClasspathAutoConfiguration.class);

   private static final String QUALIFIER = "classpath";
   private static final String REPOSITORY_BEAN_NAME = "commons-ports.document.classpathDocumentRepository";
   private static final String CONFIG_BEAN_NAME = "commons-ports.document.classpathDocumentConfig";

   @Bean(CONFIG_BEAN_NAME)
   @ConditionalOnMissingBean(ClasspathDocumentConfig.class)
   @ConditionalOnEnabledDocumentRepositoryBean(qualifier = QUALIFIER)
   public ClasspathDocumentConfig classpathDocumentConfig() {
      return new ClasspathDocumentConfig();
   }

   @Bean(REPOSITORY_BEAN_NAME)
   @ConditionalOnMissingBean(ClasspathDocumentRepository.class)
   @ConditionalOnEnabledDocumentRepositoryBean(qualifier = QUALIFIER)
   public ClasspathDocumentRepository classpathDocumentRepository(
         @Qualifier(CONFIG_BEAN_NAME) ClasspathDocumentConfig classpathDocumentConfig) {

      log.info("Using '{}' document repository 'classpathPrefix={}' configuration", QUALIFIER,
               classpathDocumentConfig.getClasspathPrefix());

      return new ClasspathDocumentRepository(classpathDocumentConfig.getClasspathPrefix());
   }

}