/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.spring.autoconfigure.document;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.tinubu.commons.ports.document.memory.MemoryDocumentRepository;
import com.tinubu.commons.ports.spring.autoconfigure.base.BasePortsDocumentAutoConfiguration;
import com.tinubu.commons.ports.spring.autoconfigure.base.ConditionalOnEnabledDocumentRepositoryBean;
import com.tinubu.commons.ports.spring.document.memory.MemoryDocumentConfig;

/**
 * Memory document adapter auto-configuration.
 * <p>
 * This repository is configured only if {@code commons-ports.document[.memory].enabled} property undefined or
 * is set to {@code true}.
 * Default bean names for this repository are :
 * <ul>
 *    <li>MemoryDocumentConfigPoint : {@value CONFIG_BEAN_NAME}</li>
 *    <li>MemoryDocumentRepository : {@value REPOSITORY_BEAN_NAME}</li>
 * </ul>
 */
@Configuration
@ConditionalOnClass(MemoryDocumentRepository.class)
public class CommonsPortsDocumentMemoryAutoConfiguration extends BasePortsDocumentAutoConfiguration {

   private static final Logger log =
         LoggerFactory.getLogger(CommonsPortsDocumentMemoryAutoConfiguration.class);

   private static final String QUALIFIER = "memory";
   private static final String REPOSITORY_BEAN_NAME = "commons-ports.document.memoryDocumentRepository";
   private static final String CONFIG_BEAN_NAME = "commons-ports.document.memoryDocumentConfig";

   @Bean(CONFIG_BEAN_NAME)
   @ConditionalOnMissingBean(MemoryDocumentConfig.class)
   @ConditionalOnEnabledDocumentRepositoryBean(qualifier = QUALIFIER)
   public MemoryDocumentConfig memoryDocumentConfig() {
      return new MemoryDocumentConfig();
   }

   @Bean(REPOSITORY_BEAN_NAME)
   @ConditionalOnMissingBean(MemoryDocumentRepository.class)
   @ConditionalOnEnabledDocumentRepositoryBean(qualifier = QUALIFIER)
   public MemoryDocumentRepository memoryDocumentRepository(
         @Qualifier(CONFIG_BEAN_NAME) MemoryDocumentConfig memoryDocumentConfig) {

      log.info("Using '{}' document repository 'caseInsensitive={}' configuration",
               QUALIFIER, memoryDocumentConfig.isCaseInsensitive());

      return new MemoryDocumentRepository(memoryDocumentConfig.isCaseInsensitive());
   }

}
