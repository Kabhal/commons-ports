/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.spring.document.sftp;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;

import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.springframework.core.io.Resource;

/**
 * SFTP document repository configuration mapper.
 */
public final class SftpDocumentConfigMapper {

   private SftpDocumentConfigMapper() {
   }

   public static com.tinubu.commons.ports.document.sftp.SftpDocumentConfig repositoryConfig(com.tinubu.commons.ports.spring.document.sftp.SftpDocumentConfig config) {
      return new com.tinubu.commons.ports.document.sftp.SftpDocumentConfig.SftpDocumentConfigBuilder()
            .host(config.getHost())
            .port(config.getPort()).allowUnknownHosts(config.isAllowUnknownKeys())
            .basePath(config.getBasePath())
            .username(config.getUsername())
            .password(config.getPassword())
            .privateKey(nullable(config.getPrivateKey())
                              .map(resource -> resourceToArray(resource, false))
                              .orElse(null))
            .privateKeyPassphrase(config.getPrivateKeyPassphrase())
            .sessionCaching(config.isSessionCaching())
            .sessionPoolSize(config.getSessionPoolSize())
            .sessionPoolWaitTimeout(config.getSessionPoolWaitTimeout())
            .keepAliveInterval(config.getKeepAliveInterval()).writeStrategy(config.getWriteStrategyClass())
            .connectTimeout(config.getConnectTimeout())
            .socketTimeout(config.getSocketTimeout())
            .build();
   }

   private static byte[] resourceToArray(Resource resource, boolean ignoreIfNotFound) {
      if (resource.exists()) {
         try (InputStream sshPrivateKeyStream = resource.getInputStream()) {
            return IOUtils.toByteArray(sshPrivateKeyStream);
         } catch (Exception e) {
            throw new IllegalStateException(e);
         }
      } else {
         if (!ignoreIfNotFound) {
            throw new IllegalStateException(String.format("Unknown '%s' resource", resource.getFilename()));
         } else {
            return null;
         }
      }
   }

}
