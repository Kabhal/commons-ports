/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.spring.autoconfigure.base;

import java.util.Map;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * Condition that matches when qualified message service is enabled.
 */
public class EnabledMessageServiceBeanCondition implements Condition {

   protected static final String GLOBAL_ENABLED_PROPERTY = "commons-ports.message.enabled";
   private static final String PORT_ENABLED_PROPERTY = "commons-ports.message.%s.enabled";

   @Override
   public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
      Map<String, Object> attributes =
            metadata.getAnnotationAttributes(ConditionalOnEnabledMessageServiceBean.class.getName());
      String qualifier = (String) attributes.get("qualifier");

      boolean globalEnabled = isEnabled(context, GLOBAL_ENABLED_PROPERTY);
      boolean portEnabled = isEnabled(context, String.format(PORT_ENABLED_PROPERTY, qualifier));

      return globalEnabled && portEnabled;
   }

   private static boolean isEnabled(ConditionContext context, String property) {
      return Boolean.parseBoolean(context.getEnvironment().getProperty(property, "true"));
   }

}
