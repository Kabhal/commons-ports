/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.spring.document.fs;

import java.nio.file.Path;
import java.util.StringJoiner;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import com.tinubu.commons.ports.document.fs.storagestrategy.DirectFsStorageStrategy;
import com.tinubu.commons.ports.document.fs.storagestrategy.FsStorageStrategy;
import com.tinubu.commons.ports.spring.resilience4j.Resilience4jConfig;

/**
 * FS document repository configuration.
 */
@Validated
@ConfigurationProperties(prefix = "commons-ports.document.fs", ignoreUnknownFields = false)
public class FsDocumentConfig {

   /**
    * Enable flag for the default repository instance.
    */
   private boolean enabled = true;

   /**
    * Document filesystem storage root path. This path must be absolute so that storage path does not depend
    * on current application default directory.
    */
   @NotNull
   private Path storagePath = Path.of(System.getProperty("java.io.tmpdir", "/tmp"), "document-fs");

   /**
    * Whether to automatically create missing storage path.
    */
   private boolean createStoragePathIfMissing = false;

   /**
    * Whether to fail fast if storage path is missing and {@code createStoragePathIfMissing} is set to
    * {@code false}, or if an error occurs while creating storage path.
    */
   private boolean failFastIfMissingStoragePath = true;

   /**
    * Document filesystem storage strategy to use. You must not change this configuration if documents already
    * exist, as it changes the way documents are searched.
    */
   @NotNull
   private Class<? extends FsStorageStrategy> storageStrategy = DirectFsStorageStrategy.class;

   private HexTreeStorageStrategyConfig hexTreeStorageStrategy = new HexTreeStorageStrategyConfig();

   /**
    * Whether to automatically delete repository on repository closing.
    */
   private boolean deleteRepositoryOnClose = false;

   /**
    * Resilience4j configuration. No pattern is configured by default.
    *
    * @implSpec Requires {@code com.tinubu.commons-ports:commons-ports-document-resilience4j} module.
    */
   private Resilience4jConfig resilience4j = new Resilience4jConfig();

   public boolean isEnabled() {
      return enabled;
   }

   public void setEnabled(boolean enabled) {
      this.enabled = enabled;
   }

   public Path getStoragePath() {
      return storagePath;
   }

   public void setStoragePath(Path storagePath) {
      this.storagePath = storagePath;
   }

   public boolean isCreateStoragePathIfMissing() {
      return createStoragePathIfMissing;
   }

   public void setCreateStoragePathIfMissing(boolean createStoragePathIfMissing) {
      this.createStoragePathIfMissing = createStoragePathIfMissing;
   }

   public boolean isFailFastIfMissingStoragePath() {
      return failFastIfMissingStoragePath;
   }

   public void setFailFastIfMissingStoragePath(boolean failFastIfMissingStoragePath) {
      this.failFastIfMissingStoragePath = failFastIfMissingStoragePath;
   }

   public Class<? extends FsStorageStrategy> getStorageStrategy() {
      return storageStrategy;
   }

   public void setStorageStrategy(Class<? extends FsStorageStrategy> storageStrategy) {
      this.storageStrategy = storageStrategy;
   }

   public HexTreeStorageStrategyConfig getHexTreeStorageStrategy() {
      return hexTreeStorageStrategy;
   }

   public void setHexTreeStorageStrategy(HexTreeStorageStrategyConfig hexTreeStorageStrategy) {
      this.hexTreeStorageStrategy = hexTreeStorageStrategy;
   }

   public boolean isDeleteRepositoryOnClose() {
      return deleteRepositoryOnClose;
   }

   public void setDeleteRepositoryOnClose(boolean deleteRepositoryOnClose) {
      this.deleteRepositoryOnClose = deleteRepositoryOnClose;
   }

   public Resilience4jConfig getResilience4j() {
      return resilience4j;
   }

   public void setResilience4j(Resilience4jConfig resilience4j) {
      this.resilience4j = resilience4j;
   }

   public static class HexTreeStorageStrategyConfig {

      /**
       * Generated hex tree depth for files, e.g.: a depth of 3 will generate directory tree like {@code
       * 6a/fd/ac}. You must not change this configuration if documents already exist, as it changes the way
       * documents are searched.
       */
      @Min(1)
      @Max(16)
      private int treeDepth = 2;

      public int getTreeDepth() {
         return treeDepth;
      }

      public void setTreeDepth(int treeDepth) {
         this.treeDepth = treeDepth;
      }

      @Override
      public String toString() {
         return new StringJoiner(", ", HexTreeStorageStrategyConfig.class.getSimpleName() + "[", "]")
               .add("treeDepth=" + treeDepth)
               .toString();
      }
   }

}
