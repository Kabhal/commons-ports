/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.spring.autoconfigure.document;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.tinubu.commons.ports.document.resilience4j.Resilience4jDocumentRepository;
import com.tinubu.commons.ports.document.sftp.SftpDocumentRepository;
import com.tinubu.commons.ports.spring.autoconfigure.base.BasePortsDocumentAutoConfiguration;
import com.tinubu.commons.ports.spring.autoconfigure.base.ConditionalOnEnabledDocumentRepositoryBean;
import com.tinubu.commons.ports.spring.autoconfigure.base.ConditionalOnResilience4jDocumentRepositoryBean;
import com.tinubu.commons.ports.spring.document.sftp.SftpDocumentConfig;
import com.tinubu.commons.ports.spring.document.sftp.SftpDocumentConfigMapper;
import com.tinubu.commons.ports.spring.resilience4j.Resilience4jConfigMapper;

/**
 * SFTP document adapter auto-configuration.
 * <p>
 * This repository is configured only if {@code commons-ports.document[.sftp].enabled} property undefined or
 * is set to {@code true}.
 * Default bean names for this repository are :
 * <ul>
 *    <li>SftpDocumentConfigPoint : {@value CONFIG_BEAN_NAME}</li>
 *    <li>SftpDocumentRepository : {@value REPOSITORY_BEAN_NAME}</li>
 * </ul>
 * If {@code com.tinubu.commons-ports:commons-ports-document-resilience4j} artifact is on classpath, bean will be wrapped in a {@link Resilience4jDocumentRepository}.
 */
@Configuration
@ConditionalOnClass(SftpDocumentRepository.class)
public class CommonsPortsDocumentSftpAutoConfiguration extends BasePortsDocumentAutoConfiguration {

   private static final Logger log = LoggerFactory.getLogger(CommonsPortsDocumentSftpAutoConfiguration.class);

   private static final String QUALIFIER = "sftp";
   private static final String REPOSITORY_BEAN_NAME = "commons-ports.document.sftpDocumentRepository";
   private static final String CONFIG_BEAN_NAME = "commons-ports.document.sftpDocumentConfig";

   @Bean(CONFIG_BEAN_NAME)
   @ConditionalOnMissingBean(SftpDocumentConfig.class)
   @ConditionalOnEnabledDocumentRepositoryBean(qualifier = QUALIFIER)
   public SftpDocumentConfig sftpDocumentConfig() {
      return new SftpDocumentConfig();
   }

   @Bean(REPOSITORY_BEAN_NAME)
   @ConditionalOnMissingBean(SftpDocumentRepository.class)
   @ConditionalOnEnabledDocumentRepositoryBean(qualifier = QUALIFIER)
   @ConditionalOnResilience4jDocumentRepositoryBean(qualifier = QUALIFIER, negate = true)
   public SftpDocumentRepository sftpDocumentRepository(
         @Qualifier(CONFIG_BEAN_NAME) SftpDocumentConfig sftpDocumentConfig) {

      com.tinubu.commons.ports.document.sftp.SftpDocumentConfig config =
            SftpDocumentConfigMapper.repositoryConfig(sftpDocumentConfig);

      log.info("Using '{}' document repository '{}' configuration", QUALIFIER, config);

      return new SftpDocumentRepository(config);
   }

   @Configuration
   @ConditionalOnClass(Resilience4jDocumentRepository.class)
   public static class Resilience4jAutoConfiguration {

      @Bean(REPOSITORY_BEAN_NAME)
      @ConditionalOnMissingBean(Resilience4jDocumentRepository.class)
      @ConditionalOnEnabledDocumentRepositoryBean(qualifier = QUALIFIER)
      @ConditionalOnResilience4jDocumentRepositoryBean(qualifier = QUALIFIER)
      public Resilience4jDocumentRepository resilience4jSftpDocumentRepository(
            @Qualifier(CONFIG_BEAN_NAME) SftpDocumentConfig sftpDocumentConfig) {

         com.tinubu.commons.ports.document.resilience4j.Resilience4jDocumentConfig resilience =
               Resilience4jConfigMapper.repositoryConfig(sftpDocumentConfig.getResilience4j());

         com.tinubu.commons.ports.document.sftp.SftpDocumentConfig config =
               SftpDocumentConfigMapper.repositoryConfig(sftpDocumentConfig);

         log.info("Using '{}' document repository '{}' configuration with '{}' resilience",
                  QUALIFIER,
                  config,
                  resilience);

         return new Resilience4jDocumentRepository(resilience,
                                                   new SftpDocumentRepository(config),
                                                   REPOSITORY_BEAN_NAME,
                                                   false);
      }
   }
}
