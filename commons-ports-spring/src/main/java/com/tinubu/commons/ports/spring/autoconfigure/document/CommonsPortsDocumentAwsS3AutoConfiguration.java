/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.spring.autoconfigure.document;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.tinubu.commons.ports.document.awss3.AwsS3DocumentRepository;
import com.tinubu.commons.ports.document.resilience4j.Resilience4jDocumentRepository;
import com.tinubu.commons.ports.spring.autoconfigure.base.BasePortsDocumentAutoConfiguration;
import com.tinubu.commons.ports.spring.autoconfigure.base.ConditionalOnEnabledDocumentRepositoryBean;
import com.tinubu.commons.ports.spring.autoconfigure.base.ConditionalOnResilience4jDocumentRepositoryBean;
import com.tinubu.commons.ports.spring.document.awss3.AwsS3DocumentConfig;
import com.tinubu.commons.ports.spring.document.awss3.AwsS3DocumentConfigMapper;
import com.tinubu.commons.ports.spring.resilience4j.Resilience4jConfigMapper;

/**
 * AWS S3 document adapter auto-configuration.
 * <p>
 * This repository is configured only if {@code commons-ports.document[.aws-s3].enabled} property is undefined
 * or set to
 * {@code true}.
 * Default bean names for this repository are :
 * <ul>
 *    <li>AwsS3DocumentConfigPoint : {@value CONFIG_BEAN_NAME}</li>
 *    <li>AwsS3DocumentRepository : {@value REPOSITORY_BEAN_NAME}</li>
 * </ul>
 */
@Configuration
@ConditionalOnClass(AwsS3DocumentRepository.class)
public class CommonsPortsDocumentAwsS3AutoConfiguration extends BasePortsDocumentAutoConfiguration {

   private static final Logger log =
         LoggerFactory.getLogger(CommonsPortsDocumentAwsS3AutoConfiguration.class);

   private static final String QUALIFIER = "aws-s3";
   private static final String REPOSITORY_BEAN_NAME = "commons-ports.document.awsS3DocumentRepository";
   private static final String CONFIG_BEAN_NAME = "commons-ports.document.awsS3DocumentConfig";

   @Bean(CONFIG_BEAN_NAME)
   @ConditionalOnMissingBean(AwsS3DocumentConfig.class)
   @ConditionalOnEnabledDocumentRepositoryBean(qualifier = QUALIFIER)
   public AwsS3DocumentConfig awsS3DocumentConfig() {
      return new AwsS3DocumentConfig();
   }

   @Bean(REPOSITORY_BEAN_NAME)
   @ConditionalOnMissingBean(AwsS3DocumentRepository.class)
   @ConditionalOnEnabledDocumentRepositoryBean(qualifier = QUALIFIER)
   @ConditionalOnResilience4jDocumentRepositoryBean(qualifier = QUALIFIER, negate = true)
   public AwsS3DocumentRepository awsS3DocumentRepository(
         @Qualifier(CONFIG_BEAN_NAME) AwsS3DocumentConfig awsS3DocumentConfig) {

      com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig config =
            AwsS3DocumentConfigMapper.repositoryConfig(awsS3DocumentConfig);

      log.info("Using '{}' document repository '{}' configuration", QUALIFIER, config);

      return new AwsS3DocumentRepository(config);
   }

   @Configuration
   @ConditionalOnClass(Resilience4jDocumentRepository.class)
   public static class Resilience4jAutoConfiguration {

      @Bean(REPOSITORY_BEAN_NAME)
      @ConditionalOnMissingBean(Resilience4jDocumentRepository.class)
      @ConditionalOnEnabledDocumentRepositoryBean(qualifier = QUALIFIER)
      @ConditionalOnResilience4jDocumentRepositoryBean(qualifier = QUALIFIER)
      public Resilience4jDocumentRepository resilience4jAwsS3DocumentRepository(
            @Qualifier(CONFIG_BEAN_NAME) AwsS3DocumentConfig awsS3DocumentConfig) {

         com.tinubu.commons.ports.document.resilience4j.Resilience4jDocumentConfig resilience =
               Resilience4jConfigMapper.repositoryConfig(awsS3DocumentConfig.getResilience4j());

         com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig config =
               AwsS3DocumentConfigMapper.repositoryConfig(awsS3DocumentConfig);

         log.info("Using '{}' document repository '{}' configuration with '{}' resilience",
                  QUALIFIER,
                  config,
                  resilience);

         return new Resilience4jDocumentRepository(resilience,
                                                   new AwsS3DocumentRepository(config),
                                                   REPOSITORY_BEAN_NAME,
                                                   false);
      }

   }
}