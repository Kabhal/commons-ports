/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.spring.autoconfigure.message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;

import com.tinubu.commons.ports.message.resilience4j.Resilience4jMessageService;
import com.tinubu.commons.ports.message.smtp.SmtpMessageService;
import com.tinubu.commons.ports.spring.autoconfigure.base.BasePortsDocumentAutoConfiguration;
import com.tinubu.commons.ports.spring.autoconfigure.base.ConditionalOnEnabledMessageServiceBean;
import com.tinubu.commons.ports.spring.autoconfigure.base.ConditionalOnResilience4jMessageServiceBean;
import com.tinubu.commons.ports.spring.message.smtp.SmtpMessageConfig;
import com.tinubu.commons.ports.spring.resilience4j.Resilience4jConfigMapper;

/**
 * SMTP message adapter auto-configuration.
 */
@Configuration
@ConditionalOnClass(SmtpMessageService.class)
public class CommonPortsMessageSmtpAutoConfiguration extends BasePortsDocumentAutoConfiguration {

   private static final Logger log = LoggerFactory.getLogger(CommonPortsMessageSmtpAutoConfiguration.class);

   private static final String QUALIFIER = "smtp";
   private static final String SERVICE_BEAN_NAME = "commons-ports.message.smtpMessageService";
   private static final String CONFIG_BEAN_NAME = "commons-ports.message.smtpMessageConfig";

   @Bean(CONFIG_BEAN_NAME)
   @ConditionalOnMissingBean(SmtpMessageConfig.class)
   @ConditionalOnEnabledMessageServiceBean(qualifier = QUALIFIER)
   public SmtpMessageConfig smtpMessageConfig() {
      return new SmtpMessageConfig();
   }

   @Bean(SERVICE_BEAN_NAME)
   @ConditionalOnMissingBean(SmtpMessageService.class)
   @ConditionalOnEnabledMessageServiceBean(qualifier = QUALIFIER)
   @ConditionalOnResilience4jMessageServiceBean(qualifier = QUALIFIER, negate = true)
   public SmtpMessageService smtpMessageService(JavaMailSender javaMailSender) {
      log.info("Using '{}' message service", QUALIFIER);

      return new SmtpMessageService(javaMailSender);
   }

   @Configuration
   @ConditionalOnClass(Resilience4jMessageService.class)
   public static class Resilience4jAutoConfiguration {

      @Bean(SERVICE_BEAN_NAME)
      @ConditionalOnMissingBean(Resilience4jMessageService.class)
      @ConditionalOnEnabledMessageServiceBean(qualifier = QUALIFIER)
      @ConditionalOnResilience4jMessageServiceBean(qualifier = QUALIFIER)
      public Resilience4jMessageService resilience4jSmtpMessageService(
            @Qualifier(CONFIG_BEAN_NAME) SmtpMessageConfig smtpMessageConfig, JavaMailSender javaMailSender) {

         com.tinubu.commons.ports.message.resilience4j.Resilience4jMessageConfig resilience =
               Resilience4jConfigMapper.messageConfig(smtpMessageConfig.getResilience4j());

         log.info("Using '{}' message service with '{}' resilience", QUALIFIER, resilience);

         return new Resilience4jMessageService(resilience,
                                               new SmtpMessageService(javaMailSender),
                                               SERVICE_BEAN_NAME);
      }

   }

}
