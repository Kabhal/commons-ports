/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.spring.document.awss3;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;

import com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig.AwsS3DocumentConfigBuilder;
import com.tinubu.commons.ports.spring.document.awss3.AwsS3DocumentConfig.AwsS3Authentication;
import com.tinubu.commons.ports.spring.document.awss3.AwsS3DocumentConfig.HttpClientConfig;

import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;

/**
 * AWS S3 document repository configuration mapper.
 */
public final class AwsS3DocumentConfigMapper {

   private AwsS3DocumentConfigMapper() {
   }

   public static com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig repositoryConfig(com.tinubu.commons.ports.spring.document.awss3.AwsS3DocumentConfig config) {
      return new AwsS3DocumentConfigBuilder()
            .region(config.getRegion())
            .endpoint(config.getEndpoint()).bucket(config.getBucket())
            .bucketBasePath(config.getBucketBasePath())
            .pathStyleAccess(config.isPathStyleAccess())
            .createIfMissingBucket(config.isCreateBucketIfMissing())
            .listingMaxKeys(config.getListingMaxKeys())
            .listingContentMode(config.getListingContentMode())
            .optionalChain(nullable(config.getAuthentication()),
                           (AwsS3DocumentConfigBuilder b, AwsS3Authentication auth) -> b.credentialsProvider(
                                 StaticCredentialsProvider.create(AwsBasicCredentials.create(auth.getAccessKeyId(),
                                                                                             auth.getAccessKey()))))
            .optionalChain(nullable(config.getHttpClientConfig()),
                           (AwsS3DocumentConfigBuilder b, HttpClientConfig httpClientConfig) -> b.httpClientConfig(
                                 new com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig.HttpClientConfig.Builder()
                                       .maxConnections(httpClientConfig.getMaxConnections())
                                       .build()))

            .build();
   }

}
