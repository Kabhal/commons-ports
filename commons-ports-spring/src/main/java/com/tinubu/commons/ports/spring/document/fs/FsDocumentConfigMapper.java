/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.spring.document.fs;

/**
 * FS document repository configuration mapper.
 */
public final class FsDocumentConfigMapper {

   private FsDocumentConfigMapper() {
   }

   public static com.tinubu.commons.ports.document.fs.FsDocumentConfig repositoryConfig(com.tinubu.commons.ports.spring.document.fs.FsDocumentConfig config) {
      return new com.tinubu.commons.ports.document.fs.FsDocumentConfig.FsDocumentConfigBuilder()
            .storagePath(config.getStoragePath())
            .createStoragePathIfMissing(config.isCreateStoragePathIfMissing())
            .failFastIfMissingStoragePath(config.isFailFastIfMissingStoragePath())
            .storageStrategy(config.getStorageStrategy())
            .hexTreeStorageStrategy(com.tinubu.commons.ports.document.fs.FsDocumentConfig.HexTreeStorageStrategyConfig.of(
                  config.getHexTreeStorageStrategy().getTreeDepth()))
            .deleteRepositoryOnClose(config.isDeleteRepositoryOnClose())
            .build();
   }

}
