/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.spring.resilience4j;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;

/**
 * Resilience4j configuration mapper between Spring and ports configurations.
 */
public final class Resilience4jConfigMapper {

   private Resilience4jConfigMapper() {
   }

   public static com.tinubu.commons.ports.document.resilience4j.Resilience4jDocumentConfig repositoryConfig(
         Resilience4jConfig config) {
      return new com.tinubu.commons.ports.document.resilience4j.Resilience4jDocumentConfig.Resilience4jDocumentConfigBuilder()
            .resilience(new com.tinubu.commons.ports.resilience4j.Resilience4jConfig.Resilience4jConfigBuilder()
                              .bulkhead(nullable(config.getBulkhead())
                                              .map(Resilience4jConfigMapper::bulkheadConfig)
                                              .orElse(null))
                              .threadPoolBulkhead(nullable(config.getThreadPoolBulkhead())
                                                        .map(Resilience4jConfigMapper::threadPoolBulkheadConfig)
                                                        .orElse(null))
                              .rateLimiter(nullable(config.getRateLimiter())
                                                 .map(Resilience4jConfigMapper::rateLimiterConfig)
                                                 .orElse(null))
                              .timeLimiter(nullable(config.getTimeLimiter())
                                                 .map(Resilience4jConfigMapper::timeLimiterConfig)
                                                 .orElse(null))
                              .retry(nullable(config.getRetry())
                                           .map(Resilience4jConfigMapper::retryConfig)
                                           .orElse(null))
                              .build())
            .build();
   }

   public static com.tinubu.commons.ports.message.resilience4j.Resilience4jMessageConfig messageConfig(
         Resilience4jConfig config) {
      return new com.tinubu.commons.ports.message.resilience4j.Resilience4jMessageConfig.Resilience4jMessageConfigBuilder()
            .resilience(new com.tinubu.commons.ports.resilience4j.Resilience4jConfig.Resilience4jConfigBuilder()
                              .bulkhead(nullable(config.getBulkhead())
                                              .map(Resilience4jConfigMapper::bulkheadConfig)
                                              .orElse(null))
                              .threadPoolBulkhead(nullable(config.getThreadPoolBulkhead())
                                                        .map(Resilience4jConfigMapper::threadPoolBulkheadConfig)
                                                        .orElse(null))
                              .rateLimiter(nullable(config.getRateLimiter())
                                                 .map(Resilience4jConfigMapper::rateLimiterConfig)
                                                 .orElse(null))
                              .timeLimiter(nullable(config.getTimeLimiter())
                                                 .map(Resilience4jConfigMapper::timeLimiterConfig)
                                                 .orElse(null))
                              .retry(nullable(config.getRetry())
                                           .map(Resilience4jConfigMapper::retryConfig)
                                           .orElse(null))
                              .build())
            .build();
   }

   public static com.tinubu.commons.ports.resilience4j.Resilience4jConfig.BulkheadConfig bulkheadConfig(
         Resilience4jConfig.BulkheadConfig config) {
      return new com.tinubu.commons.ports.resilience4j.Resilience4jConfig.BulkheadConfig.BulkheadConfigBuilder()
            .maxConcurrentCalls(config.getMaxConcurrentCalls())
            .maxWaitDuration(config.getMaxWaitDuration())
            .fairCallHandlingStrategy(config.getFairCallHandlingStrategy())
            .build();
   }

   public static com.tinubu.commons.ports.resilience4j.Resilience4jConfig.ThreadPoolBulkheadConfig threadPoolBulkheadConfig(
         Resilience4jConfig.ThreadPoolBulkheadConfig config) {
      return new com.tinubu.commons.ports.resilience4j.Resilience4jConfig.ThreadPoolBulkheadConfig.ThreadPoolBulkheadConfigBuilder()
            .coreThreadPoolSize(config.getCoreThreadPoolSize())
            .maxThreadPoolSize(config.getMaxThreadPoolSize())
            .keepAliveDuration(config.getKeepAliveDuration())
            .queueCapacity(config.getQueueCapacity())
            .build();
   }

   public static com.tinubu.commons.ports.resilience4j.Resilience4jConfig.TimeLimiterConfig timeLimiterConfig(
         Resilience4jConfig.TimeLimiterConfig config) {
      return new com.tinubu.commons.ports.resilience4j.Resilience4jConfig.TimeLimiterConfig.TimeLimiterConfigBuilder()
            .timeoutDuration(config.getTimeoutDuration())
            .build();
   }

   public static com.tinubu.commons.ports.resilience4j.Resilience4jConfig.RateLimiterConfig rateLimiterConfig(
         Resilience4jConfig.RateLimiterConfig config) {
      return new com.tinubu.commons.ports.resilience4j.Resilience4jConfig.RateLimiterConfig.RateLimiterConfigBuilder()
            .limitRefreshPeriod(config.getLimitRefreshPeriod())
            .limitForPeriod(config.getLimitForPeriod())
            .timeoutDuration(config.getTimeoutDuration())
            .build();
   }

   public static com.tinubu.commons.ports.resilience4j.Resilience4jConfig.RetryConfig retryConfig(
         Resilience4jConfig.RetryConfig config) {
      return new com.tinubu.commons.ports.resilience4j.Resilience4jConfig.RetryConfig.RetryConfigBuilder()
            .maxAttempts(config.getMaxAttempts())
            .waitDuration(config.getWaitDuration())
            .build();
   }

}
