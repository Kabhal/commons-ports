/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.spring.document.azureblob;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE_USE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.nio.file.Path;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import javax.validation.constraints.NotBlank;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import com.tinubu.commons.ports.spring.document.azureblob.AzureBlobDocumentConfig.ValidConnection;
import com.tinubu.commons.ports.spring.resilience4j.Resilience4jConfig;

/**
 * Azure Blob document repository configuration.
 */
@Validated
@ConfigurationProperties(prefix = "commons-ports.document.azure-blob", ignoreUnknownFields = false)
@ValidConnection
public class AzureBlobDocumentConfig {

   /**
    * Enable flag for the default repository instance.
    */
   private boolean enabled = true;

   /**
    * Azure Blob container name.
    */
   @NotBlank
   private String containerName;

   /**
    * Optional base path, relative to container to store all documents.
    * Path can be empty. Traversal paths are not supported.
    */
   public Path containerBasePath = Path.of("");

   /**
    * Azure Blob connection string. If set, {@link #endpoint} and {@link #authentication} will be ignored.
    * e.g.: {@code
    * DefaultEndpointsProtocol=http;AccountName=devstoreaccount1;AccountKey=Eby8vdM02xNOcqFlqUwJPLlmEtlCDXJ1OUzFT50uSRZ6IFsuFq2UVErCz4I6tq/K1SZFPTOtr/KBHBeksoGMGw==;BlobEndpoint=http://127.0.0.1:10000/devstoreaccount1;}
    */
   private String connectionString;

   /**
    * Azure Blob endpoint. e.g.: {@code http://127.0.0.1:10000/devstoreaccount1}.
    * Must be set if {@link #connectionString} is not set.
    */
   private String endpoint;

   /**
    * Authentication account name/key.
    * Must be set if {@link #connectionString} is not set.
    */
   private AzureAuthentication authentication;

   /**
    * Optional flag to automatically create missing container.
    */
   private boolean createContainerIfMissing = false;

   /**
    * Resilience4j configuration. No pattern is configured by default.
    *
    * @implSpec Requires {@code com.tinubu.commons-ports:commons-ports-document-resilience4j} module.
    */
   private Resilience4jConfig resilience4j = new Resilience4jConfig();

   public boolean isEnabled() {
      return enabled;
   }

   public void setEnabled(boolean enabled) {
      this.enabled = enabled;
   }

   public String getEndpoint() {
      return endpoint;
   }

   public void setEndpoint(String endpoint) {
      this.endpoint = endpoint;
   }

   public String getConnectionString() {
      return connectionString;
   }

   public void setConnectionString(String connectionString) {
      this.connectionString = connectionString;
   }

   public String getContainerName() {
      return containerName;
   }

   public void setContainerName(String containerName) {
      this.containerName = containerName;
   }

   public Path getContainerBasePath() {
      return containerBasePath;
   }

   public void setContainerBasePath(Path containerBasePath) {
      this.containerBasePath = containerBasePath;
   }

   public boolean isCreateContainerIfMissing() {
      return createContainerIfMissing;
   }

   public void setCreateContainerIfMissing(boolean createContainerIfMissing) {
      this.createContainerIfMissing = createContainerIfMissing;
   }

   public AzureAuthentication getAuthentication() {
      return authentication;
   }

   public void setAuthentication(AzureAuthentication authentication) {
      this.authentication = authentication;
   }

   public Resilience4jConfig getResilience4j() {
      return resilience4j;
   }

   public void setResilience4j(Resilience4jConfig resilience4j) {
      this.resilience4j = resilience4j;
   }

   public static class AzureAuthentication {
      /**
       * Azure account name.
       */
      @NotBlank
      private String storageAccountName;

      /**
       * Azure authentication key.
       */
      @NotBlank
      private String storageAccountKey;

      public String getStorageAccountName() {
         return storageAccountName;
      }

      public void setStorageAccountName(String storageAccountName) {
         this.storageAccountName = storageAccountName;
      }

      public String getStorageAccountKey() {
         return storageAccountKey;
      }

      public void setStorageAccountKey(String storageAccountKey) {
         this.storageAccountKey = storageAccountKey;
      }

   }

   @Documented
   @Constraint(validatedBy = ConnectionConstraintValidator.class)
   @Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
   @Retention(RUNTIME)
   public @interface ValidConnection {

      String message() default "Either connectionString, or both endpoint and authentication must be set";

      Class<?>[] groups() default { };

      Class<? extends Payload>[] payload() default { };
   }

   public static class ConnectionConstraintValidator
         implements ConstraintValidator<ValidConnection, AzureBlobDocumentConfig> {

      @Override
      public boolean isValid(AzureBlobDocumentConfig value, ConstraintValidatorContext context) {
         boolean connectionString = value.connectionString != null && !value.connectionString.isEmpty();
         boolean endpoint =
               (value.endpoint != null && !value.endpoint.isEmpty()) && value.authentication != null;
         return connectionString || endpoint;
      }
   }
}
