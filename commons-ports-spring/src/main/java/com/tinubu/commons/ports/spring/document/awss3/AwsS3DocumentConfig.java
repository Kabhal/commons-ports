/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.spring.document.awss3;

import static com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig.S3ListingContentMode.FAST;

import java.net.URI;
import java.nio.file.Path;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig.S3ListingContentMode;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.spring.resilience4j.Resilience4jConfig;

/**
 * AWS S3 document repository configuration.
 */
@Validated
@ConfigurationProperties(prefix = "commons-ports.document.aws-s3", ignoreUnknownFields = false)
public class AwsS3DocumentConfig {

   /**
    * Enable flag for the default repository instance.
    */
   private boolean enabled = true;

   /**
    * S3 region.
    */
   @NotBlank
   private String region;

   /**
    * Optional S3 endpoint override.
    */
   private URI endpoint;

   /**
    * S3 bucket name.
    */
   @NotBlank
   private String bucket;

   /**
    * Optional base path, relative to bucket to store all documents.
    * Path can be empty. Traversal paths are not supported.
    */
   private Path bucketBasePath = Path.of("");

   /**
    * Optional flag to automatically create missing bucket.
    */
   private boolean createBucketIfMissing = false;

   /**
    * Optional flag to use path-style access.
    */
   private boolean pathStyleAccess = false;

   /**
    * Optional max keys (i.e. page size) in objects listing operations.
    */
   private int listingMaxKeys = 1000;

   /**
    * Optional listing content mode to workaround S3 limitations.
    * By default ({@link S3ListingContentMode#FAST}, S3 does not return all metadata for objects, so that
    * {@link DocumentEntry} metadata are  incomplete for both filtering and returned entries. The library can
    * complete the metadata at the cost of one extra API call per object.
    */
   @NotNull
   private com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig.S3ListingContentMode
         listingContentMode = FAST;

   /**
    * Optional S3 Access key authentication. By default, the default AWS SDK authentication chain is used.
    */
   private AwsS3Authentication authentication;

   /**
    * Optional HTTP client configuration. By default the default AWS SDK client configuration is used.
    */
   private HttpClientConfig httpClientConfig;

   /**
    * Resilience4j configuration. No pattern is configured by default.
    *
    * @implSpec Requires {@code com.tinubu.commons-ports:commons-ports-document-resilience4j} module.
    */
   private Resilience4jConfig resilience4j = new Resilience4jConfig();

   public boolean isEnabled() {
      return enabled;
   }

   public void setEnabled(boolean enabled) {
      this.enabled = enabled;
   }

   public String getRegion() {
      return region;
   }

   public void setRegion(String region) {
      this.region = region;
   }

   public URI getEndpoint() {
      return endpoint;
   }

   public void setEndpoint(URI endpoint) {
      this.endpoint = endpoint;
   }

   public String getBucket() {
      return bucket;
   }

   public void setBucket(String bucket) {
      this.bucket = bucket;
   }

   /** @deprecated Back-compatible alias for {@link #getBucket()}. */
   public String getBucketName() {
      return bucket;
   }

   /** @deprecated Back-compatible alias for {@link #setBucket(String)}. */
   public void setBucketName(String bucket) {
      this.bucket = bucket;
   }

   public Path getBucketBasePath() {
      return bucketBasePath;
   }

   public void setBucketBasePath(Path bucketBasePath) {
      this.bucketBasePath = bucketBasePath;
   }

   public boolean isCreateBucketIfMissing() {
      return createBucketIfMissing;
   }

   public void setCreateBucketIfMissing(boolean createBucketIfMissing) {
      this.createBucketIfMissing = createBucketIfMissing;
   }

   public boolean isPathStyleAccess() {
      return pathStyleAccess;
   }

   public void setPathStyleAccess(boolean pathStyleAccess) {
      this.pathStyleAccess = pathStyleAccess;
   }

   public int getListingMaxKeys() {
      return listingMaxKeys;
   }

   public void setListingMaxKeys(int listingMaxKeys) {
      this.listingMaxKeys = listingMaxKeys;
   }

   public S3ListingContentMode getListingContentMode() {
      return listingContentMode;
   }

   public void setListingContentMode(S3ListingContentMode listingContentMode) {
      this.listingContentMode = listingContentMode;
   }

   public AwsS3Authentication getAuthentication() {
      return authentication;
   }

   public void setAuthentication(AwsS3Authentication authentication) {
      this.authentication = authentication;
   }

   public HttpClientConfig getHttpClientConfig() {
      return httpClientConfig;
   }

   public void setHttpClientConfig(HttpClientConfig httpClientConfig) {
      this.httpClientConfig = httpClientConfig;
   }

   public Resilience4jConfig getResilience4j() {
      return resilience4j;
   }

   public void setResilience4j(Resilience4jConfig resilience4j) {
      this.resilience4j = resilience4j;
   }

   /**
    * AWS S3 HTTP client configuration.
    */
   public static class HttpClientConfig {

      /**
       * Max concurrent HTTP connections hold in pool.
       */
      private Integer maxConnections;

      public Integer getMaxConnections() {
         return maxConnections;
      }

      public void setMaxConnections(Integer maxConnections) {
         this.maxConnections = maxConnections;
      }
   }

   /**
    * AWS S3 authentication.
    */
   public static class AwsS3Authentication {
      /**
       * AWS S3 Access key id.
       */
      @NotBlank
      private String accessKeyId;

      /**
       * AWS S3 Access key.
       */
      @NotBlank
      private String accessKey;

      public String getAccessKeyId() {
         return accessKeyId;
      }

      public void setAccessKeyId(String accessKeyId) {
         this.accessKeyId = accessKeyId;
      }

      public String getAccessKey() {
         return accessKey;
      }

      public void setAccessKey(String accessKey) {
         this.accessKey = accessKey;
      }
   }

}
