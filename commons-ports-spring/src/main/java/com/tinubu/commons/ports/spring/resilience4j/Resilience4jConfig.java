/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.spring.resilience4j;

import static java.lang.Math.max;
import static java.lang.Math.min;

import java.time.Duration;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.tinubu.commons.lang.util.MechanicalSympathy;

/**
 * Resilience4j document repository configuration.
 */
public class Resilience4jConfig {

   /**
    * Enable flag for the default repository instance.
    */
   private boolean enabled = true;
   /**
    * Bulkhead configuration. Bulkhead configuration is exclusive with {@link #getThreadPoolBulkhead()} and
    * will be used in priority if both are configured.
    */
   private BulkheadConfig bulkhead;
   /**
    * Thread pool bulkhead configuration. Tread pool bulkhead configuration is exclusive with
    * {@link #getBulkhead()} and will *not* be used in priority if both are configured.
    */
   private ThreadPoolBulkheadConfig threadPoolBulkhead;
   /**
    * Time limiter configuration.
    */
   private TimeLimiterConfig timeLimiter;
   /**
    * Rate limiter configuration.
    */
   private RateLimiterConfig rateLimiter;
   /**
    * Retry configuration.
    */
   private RetryConfig retry;

   public boolean isEnabled() {
      return enabled;
   }

   public void setEnabled(boolean enabled) {
      this.enabled = enabled;
   }

   public BulkheadConfig getBulkhead() {
      return bulkhead;
   }

   public void setBulkhead(BulkheadConfig bulkhead) {
      this.bulkhead = bulkhead;
   }

   public ThreadPoolBulkheadConfig getThreadPoolBulkhead() {
      return threadPoolBulkhead;
   }

   public void setThreadPoolBulkhead(ThreadPoolBulkheadConfig threadPoolBulkhead) {
      this.threadPoolBulkhead = threadPoolBulkhead;
   }

   public TimeLimiterConfig getTimeLimiter() {
      return timeLimiter;
   }

   public void setRateLimiter(RateLimiterConfig rateLimiter) {
      this.rateLimiter = rateLimiter;
   }

   public RateLimiterConfig getRateLimiter() {
      return rateLimiter;
   }

   public void setTimeLimiter(TimeLimiterConfig timeLimiter) {
      this.timeLimiter = timeLimiter;
   }

   public RetryConfig getRetry() {
      return retry;
   }

   public void setRetry(RetryConfig retry) {
      this.retry = retry;
   }

   public static class BulkheadConfig {
      private static final Duration DEFAULT_MAX_WAIT_DURATION = Duration.ZERO;
      private static final boolean DEFAULT_FAIR_CALL_HANDLING_STRATEGY = true;

      /**
       * Bulkhead max concurrent calls. Must be >= 0.
       */
      @NotNull
      @Min(0)
      private Integer maxConcurrentCalls;
      /**
       * Bulkhead max wait duration for available space, when bulkhead has no more space available. Can be
       * set to {@code 0} to disable wait time and immediately fail. Default to
       * {@link #DEFAULT_MAX_WAIT_DURATION}.
       */
      @NotNull
      private Duration maxWaitDuration = DEFAULT_MAX_WAIT_DURATION;
      /**
       * Set to {@code true} to keep operations ordered in wait queue.
       */
      private boolean fairCallHandlingStrategy = DEFAULT_FAIR_CALL_HANDLING_STRATEGY;

      public Integer getMaxConcurrentCalls() {
         return maxConcurrentCalls;
      }

      public void setMaxConcurrentCalls(Integer maxConcurrentCalls) {
         this.maxConcurrentCalls = maxConcurrentCalls;
      }

      public Duration getMaxWaitDuration() {
         return maxWaitDuration;
      }

      public void setMaxWaitDuration(Duration maxWaitDuration) {
         this.maxWaitDuration = maxWaitDuration;
      }

      public boolean getFairCallHandlingStrategy() {
         return fairCallHandlingStrategy;
      }

      public void setFairCallHandlingStrategy(boolean fairCallHandlingStrategy) {
         this.fairCallHandlingStrategy = fairCallHandlingStrategy;
      }

   }

   public static class ThreadPoolBulkheadConfig {
      private static final int DEFAULT_CORE_THREAD_POOL_SIZE =
            max(1, MechanicalSympathy.optimalConcurrency());
      private static final int DEFAULT_MAX_THREAD_POOL_SIZE = MechanicalSympathy.optimalConcurrency();

      /**
       * Bulkhead thread pool max size. Must be > 0. Default to {@link #DEFAULT_MAX_THREAD_POOL_SIZE}.
       */
      @Min(1)
      private int maxThreadPoolSize = DEFAULT_MAX_THREAD_POOL_SIZE;
      /**
       * Bulkhead thread pool core size. Must be > 0. Default to {@link #DEFAULT_CORE_THREAD_POOL_SIZE}.
       */
      @Min(1)
      private int coreThreadPoolSize = DEFAULT_CORE_THREAD_POOL_SIZE;
      /**
       * When the number of threads is greater than the core size, this is the maximum time duration
       * that idle threads will wait for new tasks before terminating. Must be >= 0.
       */
      @NotNull
      private Duration keepAliveDuration;
      /**
       * Working queue capacity. Queue is holding tasks being executed. Must be > 0.
       */
      @NotNull
      @Min(1)
      private Integer queueCapacity;

      public int getCoreThreadPoolSize() {
         return min(coreThreadPoolSize, maxThreadPoolSize);
      }

      public void setMaxThreadPoolSize(int maxThreadPoolSize) {
         this.maxThreadPoolSize = maxThreadPoolSize;
      }

      public int getMaxThreadPoolSize() {
         return maxThreadPoolSize;
      }

      public void setCoreThreadPoolSize(int coreThreadPoolSize) {
         this.coreThreadPoolSize = coreThreadPoolSize;
      }

      public Duration getKeepAliveDuration() {
         return keepAliveDuration;
      }

      public void setKeepAliveDuration(Duration keepAliveDuration) {
         this.keepAliveDuration = keepAliveDuration;
      }

      public Integer getQueueCapacity() {
         return queueCapacity;
      }

      public void setQueueCapacity(Integer queueCapacity) {
         this.queueCapacity = queueCapacity;
      }

   }

   public static class TimeLimiterConfig {
      /**
       * Maximum task duration until timeout. Must be >= 0.
       */
      @NotNull
      private Duration timeoutDuration;

      public Duration getTimeoutDuration() {
         return timeoutDuration;
      }

      public void setTimeoutDuration(Duration timeoutDuration) {
         this.timeoutDuration = timeoutDuration;
      }

   }

   public static class RateLimiterConfig {
      /**
       * Duration after which limit is refreshed. Used accordingly with {@link #getLimitForPeriod()}. Must be
       * >= 1 ns.
       */
      @NotNull
      private Duration limitRefreshPeriod;
      /**
       * Maximum limit value for a period. Used accordingly with {@link #getLimitRefreshPeriod()}. Must be >=
       * 0.
       */
      @NotNull
      @Min(0)
      private Integer limitForPeriod;
      /**
       * Maximum wait duration for permission. Must be >= 0.
       */
      @NotNull
      private Duration timeoutDuration;

      public Duration getLimitRefreshPeriod() {
         return limitRefreshPeriod;
      }

      public void setLimitRefreshPeriod(Duration limitRefreshPeriod) {
         this.limitRefreshPeriod = limitRefreshPeriod;
      }

      public Integer getLimitForPeriod() {
         return limitForPeriod;
      }

      public void setLimitForPeriod(Integer limitForPeriod) {
         this.limitForPeriod = limitForPeriod;
      }

      public Duration getTimeoutDuration() {
         return timeoutDuration;
      }

      public void setTimeoutDuration(Duration timeoutDuration) {
         this.timeoutDuration = timeoutDuration;
      }

   }

   public static class RetryConfig {
      /**
       * Maximum retry attempts. Must be > 0.
       */
      @NotNull
      @Min(1)
      private Integer maxAttempts;

      /**
       * Wait duration between each attempt. Must be >= 0.
       */
      @NotNull
      private Duration waitDuration;

      public Integer getMaxAttempts() {
         return maxAttempts;
      }

      public void setMaxAttempts(Integer maxAttempts) {
         this.maxAttempts = maxAttempts;
      }

      public Duration getWaitDuration() {
         return waitDuration;
      }

      public void setWaitDuration(Duration waitDuration) {
         this.waitDuration = waitDuration;
      }

   }

}
