/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.spring.autoconfigure.base;

import java.util.Map;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

import com.tinubu.commons.lang.libraryselector.LibrarySelector;
import com.tinubu.commons.lang.libraryselector.LibrarySelector.LibraryDefinition;

/**
 * Condition that matches when qualified message service has Resilience4j support enabled, i.e. resilience4j
 * library is on classpath.
 */
public class Resilience4jMessageServiceBeanCondition implements Condition {

   private static final String GLOBAL_RESILIENCE4J_ENABLED_PROPERTY =
         "commons-ports.message.resilience4j.enabled";
   private static final String PORT_RESILIENCE4J_ENABLED_PROPERTY =
         "commons-ports.message.%s.resilience4j.enabled";
   private static final LibraryDefinition MESSAGE_RESILIENCE4J = new LibraryDefinition() {
      @Override
      public String libraryName() {
         return "message-resilience4j";
      }

      @Override
      public String classSample() {
         return "com.tinubu.commons.ports.message.resilience4j.Resilience4jMessageService";
      }
   };
   private static final LibrarySelector<LibraryDefinition> LIBRARY_CHECKER =
         new LibrarySelector<>(MESSAGE_RESILIENCE4J);

   @Override
   public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
      Map<String, Object> attributes =
            metadata.getAnnotationAttributes(ConditionalOnResilience4jMessageServiceBean.class.getName());
      String qualifier = (String) attributes.get("qualifier");
      boolean negate = (boolean) attributes.get("negate");

      boolean hasResilience4j = LIBRARY_CHECKER.hasLibrary(MESSAGE_RESILIENCE4J);
      boolean globalResilience4jEnabled = isEnabled(context, GLOBAL_RESILIENCE4J_ENABLED_PROPERTY);
      boolean portResilience4jEnabled =
            isEnabled(context, String.format(PORT_RESILIENCE4J_ENABLED_PROPERTY, qualifier));

      boolean condition = hasResilience4j && globalResilience4jEnabled && portResilience4jEnabled;

      if (negate) {
         return !condition;
      } else {
         return condition;
      }
   }

   private static boolean isEnabled(ConditionContext context, String property) {
      return Boolean.parseBoolean(context.getEnvironment().getProperty(property, "true"));
   }

}
