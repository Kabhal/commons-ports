/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.resilience4j.event;

import static com.tinubu.commons.ddd2.domain.type.support.DomainObjectSupport.checkInvariants;

import io.github.resilience4j.retry.event.RetryOnSuccessEvent;

/**
 * A retried call attempt succeeded. Event is not sent if first call is successful.
 */
public final class SucceededRetryEvent extends Resilience4jEvent {

   private final int retriedAttempts;

   public SucceededRetryEvent(Object source, RetryOnSuccessEvent event) {
      super(source, event.getCreationTime().toInstant(), event.getName());
      this.retriedAttempts = event.getNumberOfRetryAttempts();

      checkInvariants(this);
   }

   /**
    * Number of retried attempts.
    *
    * @return number of retried attempts
    */
   public int retriedAttempts() {
      return retriedAttempts;
   }

   @Override
   public String toString() {
      return super.toString() + " > Retried call attempt successful";
   }
}
