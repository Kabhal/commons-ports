/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.resilience4j.event;

import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;

import java.time.Instant;

import com.tinubu.commons.ddd2.domain.event.DomainEvent;
import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.Fields;

/**
 * Resilience4j event base class.
 */
public abstract class Resilience4jEvent extends AbstractValue implements DomainEvent {

   protected final Object source;
   protected final Instant eventDate;
   protected final String resilienceName;

   public Resilience4jEvent(Object source, Instant eventDate, String resilienceName) {
      this.source = source;
      this.eventDate = eventDate;
      this.resilienceName = resilienceName;
   }

   @Override
   protected Fields<? extends Resilience4jEvent> defineDomainFields() {
      return Fields
            .<Resilience4jEvent>builder().field("source", v -> v.source, isNotNull())
            .field("eventDate", v -> v.eventDate, isNotNull())
            .field("resilienceName", v -> v.resilienceName, isNotBlank())
            .build();
   }

   public Instant eventDate() {
      return eventDate;
   }

   public String resilienceName() {
      return resilienceName;
   }

   @Override
   public Object source() {
      return source;
   }
}
