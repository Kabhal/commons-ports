/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.resilience4j;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.lang.util.CheckedConsumer.checkedConsumer;
import static com.tinubu.commons.lang.util.CheckedRunnable.checkedRunnable;
import static com.tinubu.commons.lang.util.CheckedRunnable.noop;
import static com.tinubu.commons.lang.util.CheckedSupplier.checkedSupplier;
import static com.tinubu.commons.lang.util.CollectionUtils.immutable;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.ExceptionUtils.sneakyThrow;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.util.StreamUtils.streamConcat;
import static java.util.concurrent.CompletableFuture.supplyAsync;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinubu.commons.ddd2.domain.event.DomainEventService;
import com.tinubu.commons.lang.util.CheckedRunnable;
import com.tinubu.commons.lang.util.CheckedSupplier;
import com.tinubu.commons.ports.resilience4j.event.Resilience4jEvent;
import com.tinubu.commons.ports.resilience4j.event.Resilience4jEventFactory;

import io.github.resilience4j.bulkhead.Bulkhead;
import io.github.resilience4j.bulkhead.BulkheadConfig;
import io.github.resilience4j.bulkhead.ThreadPoolBulkhead;
import io.github.resilience4j.bulkhead.ThreadPoolBulkheadConfig;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.core.EventConsumer;
import io.github.resilience4j.decorators.Decorators;
import io.github.resilience4j.decorators.Decorators.DecorateCallable;
import io.github.resilience4j.decorators.Decorators.DecorateCompletionStage;
import io.github.resilience4j.ratelimiter.RateLimiter;
import io.github.resilience4j.ratelimiter.RateLimiterConfig;
import io.github.resilience4j.retry.Retry;
import io.github.resilience4j.retry.RetryConfig;
import io.github.resilience4j.timelimiter.TimeLimiter;
import io.github.resilience4j.timelimiter.TimeLimiterConfig;

/**
 * Resilience4j decorator for any delegated function.
 * <p>
 * The following resilience patterns are supported :
 * <ul>
 *    <li>Bulkhead (thread-based)</li>
 *    <li>Time limiter (only if Bulkhead is enabled)</li>
 *    <li>Rate limiter</li>
 *    <!--<li>Circuit-breaker</li>-->
 *    <li>Retry</li>
 * </ul>
 * <p>
 * Patterns encapsulation is Retry(Circuit-breaker(Rate limiter(Time limiter(Bulkhead)))).
 */
public class Resilience4jDecorator {

   /** Default thread pool core size for the Resilience4j scheduled pools. */
   private static final int DEFAULT_THREAD_POOL_CORE_SIZE = 1;
   /** Default exceptions to retry on. */
   private static final List<Class<? extends Throwable>> DEFAULT_RETRY_EXCEPTIONS =
         list(IOException.class, UncheckedIOException.class, TimeoutException.class);

   private static final Logger log = LoggerFactory.getLogger(Resilience4jDecorator.class);

   private final DomainEventService eventService;

   private final Bulkhead bulkhead;
   private final ThreadPoolBulkhead threadPoolBulkhead;
   private final RateLimiter rateLimiter;
   private final CircuitBreaker circuitBreaker;
   private final TimeLimiter timeLimiter;
   private ScheduledExecutorService timeLimiterScheduledThreadPool;
   private final Retry retry;
   private ScheduledExecutorService retryScheduledThreadPool;

   /**
    * Creates a Resilience4j enabled decorator.
    *
    * @param config resilience configuration
    * @param eventService event service to publish Resilience4j events
    * @param delegateName unique name for the delegate to identify resilience patterns
    *       against it
    * @param retryExceptions extra applicative exceptions to retry on
    */
   public Resilience4jDecorator(Resilience4jConfig config,
                                DomainEventService eventService,
                                String delegateName,
                                List<Class<? extends Throwable>> retryExceptions) {
      validate(config, "config", isNotNull()).orThrow();
      this.eventService = validate(eventService, "eventService", isNotNull()).orThrow();
      validate(delegateName, "delegateName", isNotBlank()).orThrow();

      this.bulkhead = bulkhead(config.bulkhead(), delegateName);
      this.threadPoolBulkhead = threadPoolBulkhead(config.threadPoolBulkhead(), delegateName);
      this.timeLimiter = timeLimiter(config.timeLimiter(), delegateName);
      this.rateLimiter = rateLimiter(config.rateLimiter(), delegateName);
      this.circuitBreaker = null; // circuitBreaker(delegateName);
      this.retry = retry(config.retry(), delegateName, immutable(retryExceptions));

      int optimalConcurrencyLevel = predictOptimalConcurrencyLevel();

      log.debug("Using the optimal concurrency level : {}", optimalConcurrencyLevel);

      if (timeLimiter != null) {
         timeLimiterScheduledThreadPool = Executors.newScheduledThreadPool(optimalConcurrencyLevel);
      }
      if (retry != null) {
         retryScheduledThreadPool = Executors.newScheduledThreadPool(optimalConcurrencyLevel);
      }
   }

   /**
    * Creates a Resilience4j enabled decorator.
    *
    * @param config resilience configuration
    * @param eventService event service to publish Resilience4j events
    * @param delegateName unique name for the delegate to identify resilience patterns
    *       against it
    */
   public Resilience4jDecorator(Resilience4jConfig config,
                                DomainEventService eventService,
                                String delegateName) {
      this(config, eventService, delegateName, null);
   }

   /**
    * Call delegated operation with resilience patterns.
    * Delegated operation is wrapped with resilience decorators.
    *
    * @param supplier operation
    * @param <T> operation return type
    *
    * @return resilient operation
    */
   public <T> CheckedSupplier<T> resilientCall(CheckedSupplier<T> supplier) {
      return decorate(supplier, false);
   }

   /**
    * Call delegated operation with resilience patterns.
    * Delegated operation is wrapped with resilience decorators.
    *
    * @param supplier operation
    * @param unRetryable whether call is retryable, this disables retry pattern if called operation does
    *       not support it
    * @param <T> operation return type
    *
    * @return resilient operation
    */
   public <T> CheckedSupplier<T> resilientCall(CheckedSupplier<T> supplier, boolean unRetryable) {
      return decorate(supplier, unRetryable);
   }

   /**
    * Call delegated operation with resilience patterns.
    * Delegated operation is wrapped with resilience decorators.
    *
    * @param runnable operation
    *
    * @return resilient operation
    */
   public CheckedRunnable resilientCall(CheckedRunnable runnable) {
      return checkedRunnable(resilientCall(checkedSupplier(runnable)));
   }

   /**
    * Call delegated operation with resilience patterns.
    * Delegated operation is wrapped with resilience decorators.
    *
    * @param runnable operation
    * @param unRetryable whether call is retryable, this disables retry pattern if called operation does
    *       not support it
    *
    * @throws Exception if runnable throws an error, or if a resilience error occurs.
    */
   public CheckedRunnable resilientCall(CheckedRunnable runnable, boolean unRetryable) {
      return checkedRunnable(resilientCall(checkedSupplier(runnable), unRetryable));
   }

   /**
    * Finalize Resilience4j objects.
    *
    * @throws IllegalStateException on error on close
    */
   @SuppressWarnings("unchecked")
   public void close() {
      noop()
            .tryFinally(() -> nullable(retryScheduledThreadPool).map(ScheduledExecutorService::shutdownNow),
                        () -> nullable(timeLimiterScheduledThreadPool).map(ScheduledExecutorService::shutdownNow),
                        () -> nullable(threadPoolBulkhead).ifPresent(checkedConsumer(ThreadPoolBulkhead::close)))
            .run();
   }

   /**
    * Generates a default delegate name for specified object.
    *
    * @param delegate delegate object to generate a name for
    *
    * @return auto-generated delegate name
    */
   public static String delegateName(Object delegate) {
      String delegateId = Integer.toHexString(System.identityHashCode(delegate));

      return delegate.getClass().getSimpleName() + "@" + delegateId;
   }

   /**
    * Predicts the anticipated optimal concurrency level based on configuration to
    * configure the core size of various thread pools.
    *
    * @return anticipated optimal concurrency level
    *
    * @implNote in the case of a semaphore bulkhead, only the maximum concurrency is defined, not the
    *       optimal concurrency
    */
   private int predictOptimalConcurrencyLevel() {
      return nullable(bulkhead)
            .map(b -> DEFAULT_THREAD_POOL_CORE_SIZE)
            .orElseGet(() -> nullable(threadPoolBulkhead)
                  .map(b -> b.getBulkheadConfig().getCoreThreadPoolSize())
                  .orElse(DEFAULT_THREAD_POOL_CORE_SIZE));
   }

   private Bulkhead bulkhead(Resilience4jConfig.BulkheadConfig config, String resilienceName) {
      if (config == null) {
         return null;
      }

      Bulkhead bulkhead = Bulkhead.of(resilienceName,
                                      BulkheadConfig
                                            .custom()
                                            .maxConcurrentCalls(config.maxConcurrentCalls())
                                            .maxWaitDuration(config.maxWaitDuration())
                                            .fairCallHandlingStrategyEnabled(config.fairCallHandlingStrategy())
                                            .build());

      bulkhead.getEventPublisher().onEvent(eventBridge());

      return bulkhead;
   }

   private ThreadPoolBulkhead threadPoolBulkhead(Resilience4jConfig.ThreadPoolBulkheadConfig config,
                                                 String resilienceName) {
      if (config == null) {
         return null;
      }

      ThreadPoolBulkhead bulkhead = ThreadPoolBulkhead.of(resilienceName,
                                                          ThreadPoolBulkheadConfig
                                                                .custom()
                                                                .maxThreadPoolSize(config.maxThreadPoolSize())
                                                                .coreThreadPoolSize(config.coreThreadPoolSize())
                                                                .queueCapacity(config.queueCapacity())
                                                                .keepAliveDuration(config.keepAliveDuration())
                                                                .build());
      bulkhead.getEventPublisher().onEvent(eventBridge());

      return bulkhead;
   }

   private RateLimiter rateLimiter(Resilience4jConfig.RateLimiterConfig config, String resilienceName) {
      if (config == null) {
         return null;
      }

      RateLimiter rateLimiter = RateLimiter.of(resilienceName,
                                               RateLimiterConfig
                                                     .custom()
                                                     .limitForPeriod(config.limitForPeriod())
                                                     .limitRefreshPeriod(config.limitRefreshPeriod())
                                                     .timeoutDuration(config.timeoutDuration())
                                                     .build());
      rateLimiter.getEventPublisher().onEvent(eventBridge());
      return rateLimiter;
   }

   /*private CircuitBreaker circuitBreaker(Resilience4jConfig.CircuitBreakerConfig config, String resilienceName) {
      if (config == null) {
         return null;
      }

      CircuitBreaker circuitBreaker = CircuitBreaker.of(resilienceName, CircuitBreakerConfig.ofDefaults());
      circuitBreaker.getEventPublisher().onEvent(bridgeEvent());
      return circuitBreaker;
   }*/

   private TimeLimiter timeLimiter(Resilience4jConfig.TimeLimiterConfig config, String resilienceName) {
      if (config == null) {
         return null;
      }

      TimeLimiter timeLimiter = TimeLimiter.of(resilienceName,
                                               TimeLimiterConfig
                                                     .custom()
                                                     .cancelRunningFuture(true)
                                                     .timeoutDuration(config.timeoutDuration())
                                                     .build());
      timeLimiter.getEventPublisher().onEvent(eventBridge());
      return timeLimiter;
   }

   private Retry retry(Resilience4jConfig.RetryConfig config,
                       String resilienceName,
                       List<Class<? extends Throwable>> extraRetryExceptions) {
      if (config == null) {
         return null;
      }

      @SuppressWarnings("unchecked")
      Class<? extends Throwable>[] retryExceptions =
            (Class<? extends Throwable>[]) streamConcat(stream(DEFAULT_RETRY_EXCEPTIONS),
                                                        stream(extraRetryExceptions)).toArray(Class<?>[]::new);

      Retry retry = Retry.of(resilienceName,
                             RetryConfig
                                   .custom()
                                   .maxAttempts(config.maxAttempts())
                                   .waitDuration(config.waitDuration())
                                   .retryExceptions(retryExceptions)
                                   .build());
      retry.getEventPublisher().onEvent(eventBridge());
      return retry;
   }

   /**
    * Bridge Resilience4j library event to document repository events.
    *
    * @param <T> event type
    *
    * @return event bridger
    */
   private <T> EventConsumer<T> eventBridge() {
      final Resilience4jEventFactory factory = new Resilience4jEventFactory();

      return resilience4jEvent -> {
         Optional<Resilience4jEvent> event = factory.event(this, resilience4jEvent);

         if (event.isPresent()) {
            event.ifPresent(eventService::publishEvent);
         } else {
            log.warn("Unknown '{}' event is not bridged", resilience4jEvent);
         }
      };
   }

   protected <T> CheckedSupplier<T> decorate(CheckedSupplier<T> supplier, boolean unRetryable) {
      if (bulkhead != null) {
         DecorateCallable<T> decorator = Decorators.ofCallable(supplier).withBulkhead(bulkhead);

         decorator = decorateTimeLimiter(decorator);
         decorator = decorateRateLimiter(decorator);
         decorator = decorateCircuitBreaker(decorator);
         if (!unRetryable) {
            decorator = decorateRetry(decorator);
         }

         return checkedSupplier(decorator.decorate());
      } else if (threadPoolBulkhead != null) {
         DecorateCompletionStage<T> decorator =
               Decorators.ofCallable(supplier).withThreadPoolBulkhead(threadPoolBulkhead);

         decorator = decorateTimeLimiter(decorator);
         decorator = decorateRateLimiter(decorator);
         decorator = decorateCircuitBreaker(decorator);
         if (!unRetryable) {
            decorator = decorateRetry(decorator);
         }

         CompletableFuture<T> completableFuture = decorator.get().toCompletableFuture();
         return () -> unwrapExecutionException(completableFuture);
      } else {
         DecorateCallable<T> decorator = Decorators.ofCallable(supplier);

         decorator = decorateTimeLimiter(decorator);
         decorator = decorateRateLimiter(decorator);
         decorator = decorateCircuitBreaker(decorator);
         if (!unRetryable) {
            decorator = decorateRetry(decorator);
         }

         return checkedSupplier(decorator.decorate());
      }
   }

   /**
    * ThreadPoolBulkhead systematically wraps exception in {@link ExecutionException}. This function unwraps
    * it if possible.
    *
    * @param completionStage completion stage to get
    * @param <T> completion stage return type
    *
    * @return completion stage result
    *
    * @throws InterruptedException
    * @throws ExecutionException
    */
   private <T> T unwrapExecutionException(CompletionStage<T> completionStage)
         throws InterruptedException, ExecutionException {
      try {
         return completionStage.toCompletableFuture().get();
      } catch (ExecutionException e) {
         if (e.getCause() != null) {
            throw sneakyThrow(e.getCause());
         } else {
            throw e;
         }
      }
   }

   private <T> DecorateCallable<T> decorateTimeLimiter(DecorateCallable<T> decorator) {
      if (timeLimiter != null) {
         CheckedSupplier<T> supplier = checkedSupplier(decorator.decorate());
         decorator = Decorators.ofCallable(() -> timeLimiter.executeFutureSupplier(() -> supplyAsync(supplier,
                                                                                                     timeLimiterScheduledThreadPool)));
      }
      return decorator;
   }

   private <T> DecorateCompletionStage<T> decorateTimeLimiter(DecorateCompletionStage<T> decorator) {
      if (timeLimiter != null) {
         decorator = decorator.withTimeLimiter(timeLimiter, timeLimiterScheduledThreadPool);
      }
      return decorator;
   }

   private <T> DecorateCompletionStage<T> decorateRateLimiter(DecorateCompletionStage<T> decorator) {
      if (rateLimiter != null) {
         decorator = decorator.withRateLimiter(rateLimiter);
      }
      return decorator;
   }

   private <T> DecorateCallable<T> decorateRateLimiter(DecorateCallable<T> decorator) {
      if (rateLimiter != null) {
         decorator = decorator.withRateLimiter(rateLimiter);
      }
      return decorator;
   }

   private <T> DecorateCompletionStage<T> decorateCircuitBreaker(DecorateCompletionStage<T> decorator) {
      if (circuitBreaker != null) {
         decorator = decorator.withCircuitBreaker(circuitBreaker);
      }
      return decorator;
   }

   private <T> DecorateCallable<T> decorateCircuitBreaker(DecorateCallable<T> decorator) {
      if (circuitBreaker != null) {
         decorator = decorator.withCircuitBreaker(circuitBreaker);
      }
      return decorator;
   }

   private <T> DecorateCompletionStage<T> decorateRetry(DecorateCompletionStage<T> decorator) {
      if (retry != null) {
         decorator = decorator.withRetry(retry, retryScheduledThreadPool);
      }
      return decorator;
   }

   private <T> DecorateCallable<T> decorateRetry(DecorateCallable<T> decorator) {
      if (retry != null) {
         decorator = decorator.withRetry(retry);
      }
      return decorator;
   }

}