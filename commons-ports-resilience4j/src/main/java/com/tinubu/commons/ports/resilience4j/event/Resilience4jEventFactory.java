/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.resilience4j.event;

import static com.tinubu.commons.lang.util.OptionalUtils.optionalInstanceOf;
import static com.tinubu.commons.lang.util.OptionalUtils.or;

import java.util.Optional;

import io.github.resilience4j.bulkhead.event.BulkheadOnCallFinishedEvent;
import io.github.resilience4j.bulkhead.event.BulkheadOnCallPermittedEvent;
import io.github.resilience4j.bulkhead.event.BulkheadOnCallRejectedEvent;
import io.github.resilience4j.ratelimiter.event.RateLimiterOnDrainedEvent;
import io.github.resilience4j.ratelimiter.event.RateLimiterOnFailureEvent;
import io.github.resilience4j.ratelimiter.event.RateLimiterOnSuccessEvent;
import io.github.resilience4j.retry.event.RetryOnErrorEvent;
import io.github.resilience4j.retry.event.RetryOnIgnoredErrorEvent;
import io.github.resilience4j.retry.event.RetryOnRetryEvent;
import io.github.resilience4j.retry.event.RetryOnSuccessEvent;
import io.github.resilience4j.timelimiter.event.TimeLimiterOnErrorEvent;
import io.github.resilience4j.timelimiter.event.TimeLimiterOnSuccessEvent;
import io.github.resilience4j.timelimiter.event.TimeLimiterOnTimeoutEvent;

public class Resilience4jEventFactory {

   public Optional<Resilience4jEvent> event(Object source, Object event) {
      return or(permittedCallBulkheadEvent(source, event),
                () -> rejectedCallBulkheadEvent(source, event),
                () -> finishedCallBulkheadEvent(source, event),
                () -> successTimeLimiterEvent(source, event),
                () -> errorTimeLimiterEvent(source, event),
                () -> timeoutTimeLimiterEvent(source, event),
                () -> permittedRateLimiterEvent(source, event),
                () -> rejectedRateLimiterEvent(source, event),
                () -> drainedRateLimiterEvent(source, event),
                () -> successRetryEvent(source, event),
                () -> retriedRetryEvent(source, event),
                () -> maxAttemptErrorRetryEvent(source, event),
                () -> nonRetryableExceptionErrorRetryEvent(source, event));
   }

   private static Optional<Resilience4jEvent> permittedCallBulkheadEvent(Object source,
                                                                         Object event) {
      return optionalInstanceOf(event,
                                BulkheadOnCallPermittedEvent.class).map(e -> new PermittedCallBulkheadEvent(
            source,
            e));
   }

   private static Optional<Resilience4jEvent> rejectedCallBulkheadEvent(Object source,
                                                                        Object event) {
      return optionalInstanceOf(event,
                                BulkheadOnCallRejectedEvent.class).map(e -> new RejectedCallBulkheadEvent(
            source,
            e));
   }

   private static Optional<Resilience4jEvent> finishedCallBulkheadEvent(Object source,
                                                                        Object event) {
      return optionalInstanceOf(event,
                                BulkheadOnCallFinishedEvent.class).map(e -> new FinishedCallBulkheadEvent(
            source,
            e));
   }

   private static Optional<SucceededTimeLimiterEvent> successTimeLimiterEvent(Object source,
                                                                              Object event) {
      return optionalInstanceOf(event,
                                TimeLimiterOnSuccessEvent.class).map(e -> new SucceededTimeLimiterEvent(source,
            e));
   }

   private static Optional<FailedTimeLimiterEvent> errorTimeLimiterEvent(Object source,
                                                                         Object event) {
      return optionalInstanceOf(event, TimeLimiterOnErrorEvent.class).map(e -> new FailedTimeLimiterEvent(
            source,
            e));
   }

   private static Optional<TimedOutTimeLimiterEvent> timeoutTimeLimiterEvent(Object source,
                                                                             Object event) {
      return optionalInstanceOf(event, TimeLimiterOnTimeoutEvent.class).map(e -> new TimedOutTimeLimiterEvent(
            source,
            e));
   }

   private static Optional<PermittedRateLimiterEvent> permittedRateLimiterEvent(Object source,
                                                                                Object event) {
      return optionalInstanceOf(event,
                                RateLimiterOnSuccessEvent.class).map(e -> new PermittedRateLimiterEvent(source,
            e));
   }

   private static Optional<RejectedRateLimiterEvent> rejectedRateLimiterEvent(Object source,
                                                                              Object event) {
      return optionalInstanceOf(event, RateLimiterOnFailureEvent.class).map(e -> new RejectedRateLimiterEvent(
            source,
            e));
   }

   private static Optional<DrainedRateLimiterEvent> drainedRateLimiterEvent(Object source,
                                                                            Object event) {
      return optionalInstanceOf(event, RateLimiterOnDrainedEvent.class).map(e -> new DrainedRateLimiterEvent(
            source,
            e));
   }

   private static Optional<SucceededRetryEvent> successRetryEvent(Object source,
                                                                  Object event) {
      return optionalInstanceOf(event, RetryOnSuccessEvent.class).map(e -> new SucceededRetryEvent(source,
            e));
   }

   private static Optional<RetriedRetryEvent> retriedRetryEvent(Object source,
                                                                Object event) {
      return optionalInstanceOf(event, RetryOnRetryEvent.class).map(e -> new RetriedRetryEvent(source,
            e));
   }

   private static Optional<FailedRetryEvent> maxAttemptErrorRetryEvent(Object source,
                                                                       Object event) {
      return optionalInstanceOf(event, RetryOnErrorEvent.class).map(e -> new FailedRetryEvent(source,
            e));
   }

   private static Optional<FailedRetryEvent> nonRetryableExceptionErrorRetryEvent(Object source,
                                                                                  Object event) {
      return optionalInstanceOf(event, RetryOnIgnoredErrorEvent.class).map(e -> new FailedRetryEvent(source,
            e));
   }

}
