#!/bin/sh

set -ex

test -z "$MODULES" && MODULES="commons-ports-document-archive
commons-ports-document-aws-s3
commons-ports-document-azure-blob
commons-ports-document-classpath
commons-ports-document-domain
commons-ports-document-fs
commons-ports-document-git
commons-ports-document-memory
commons-ports-document-resilience4j
commons-ports-document-sftp
commons-ports-document-tika
commons-ports-document-transformer
commons-ports-message-domain
commons-ports-message-resilience4j
commons-ports-message-smtp
commons-ports-resilience4j
commons-ports-spring"

mvn install -DskipTests
for module in $MODULES; do
  mvn -DenableNativeAgent test -pl ":${module}"
done
